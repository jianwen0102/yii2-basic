<?php
namespace app\filter;

use Yii;
use yii\base\ActionFilter;


class AdminFilter extends ActionFilter{
    public function beforeAction($action){
        //判断是否登录
        if (!Yii::$app->user->isGuest){
            return true;
        }else{
            if ($action->getUniqueId() == 'admin/index/index'){
                Yii::$app->user->logout();

                Yii::$app->response->redirect(['admin/index/login']);
            }

            echo Yii::t('app/admin/error', 'no_auth');
            return false;
        }
    }
}