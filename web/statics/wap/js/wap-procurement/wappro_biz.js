$(function(){	
$('#warehouse').select2();
$('#vendor').select2();
var Product_select = '<option value=0>请选择商品...</option>';
(function(){
	$.get('get-product-list', function(data,status){
		if(status =='success'){
			if(data.Ack == 'Success'){
				
				var P = data.Body;
				for(var i in P){

					Product_select += '<option value="'+P[i].product_id+'">'+P[i].product_name+'</option>';
				}
				$(Product_select).appendTo('.product_name');
				// $('.product_name:first').searchableSelect();
				$('.product_name:first').select2();
			}
		} else {
			alertTips('error','网络错误！');
			return false;
		}
	})
})();


//新增产品
 $('#addItem').on('click',function(){
    		$.get('get-item',function(data,status){
    			if(status =="success"){
    				$('#wap_pro_detail').find('.item-info:last').after(data);
    				$('.product_name:last').append(Product_select);
    				// $('.product_name:last').searchableSelect();
    				$('.product_name:last').select2();
    			}
    			
    		})

});
//收起菜单栏
//	$(function(){
//		$('.mainMenuOuterWrapper li').on('click',function(){
//			$('.mainMenuWrapper').hide();
//			$('.mainpanel').show();
//		});
//	});

//删除产品	
$('#removeItem').on('click',function(){
	var pro_len = $('.item-info').length;
	if(pro_len <= 1) {
		return false;
	}
	var ITEM = $('#wap_pro_detail').find('.item-info:first');
	var product_name  = ITEM.find('.product_name').val();
	if(product_name){
		if(confirm("确定要删除商品？")){
		 	ITEM.remove();
		}
	}
})

$('#wap_pro_detail').on('change','.product_name', function(){
	var _TR = $(this);
	$.get('get-pro-unit',{'pid':$(this).val()},function(data, status){
		if(status =='success'){
			if(data.Ack =='Success'){

				var U = data.Body;
				var Unit_select = '';
				for(var i in U){
					Unit_select += '<option value="'+U[i].unit+'">'+U[i].unit_name+'</option>';
				}
				var UNIT = _TR.closest('tr').next('tr').find('.unit');
				UNIT.find('option').remove();
				$(Unit_select).appendTo(UNIT);
			}
		} else {
			alertTips('error','没有单位，请联系管理员！');
			return false;
		}
	})
});

$('#wap_pro_detail').on('keyup','.quantity',function(){
	var _price = $(this).closest('tr').next('tr').find('.price').val();
	if(_price == ''){
		_price = 0;
	}
	this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
	_quantity = $(this).val();
	if(_quantity == ''){
		_quantity = 0;
	}
	var _amount = (_quantity * _price).toFixed(2);
	$(this).closest('tr').next('tr').next('tr').find('.amount').val(_amount);

})
//单位验证 计算价格金额
$("#wap_pro_detail").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').prev('tr').find('.quantity').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _price = $(this).val();

		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').next('tr').find('.amount').val(_amount);
		SumAmount();
});

var SumAmount = function(){
		var total_amount = 0;
		$('#wap_pro_detail').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
			} else {
				total_amount += +_amount;
			}
			
		})
		$('#wap_pro_amount').val(total_amount);
};


(function() {
		// 保存
	$('#savePro').on('click',function(){
			//单头
			var head = pro_header();
			if(!head){
				return false;
			}
			//明细
			var detail = pro_detail();
			if(!detail){
				return false;
			}
			var ii = layer.load();
			$.get('save-wap-pro',{'head':head,'det':detail}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function(){
							 window.location.reload();
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var pro_header = function() {
		
		var warehouse = $('#warehouse').val();
		if (!+warehouse) {
			alertTips('warning', '请选择入库仓库');
			return false;
		}
		
		var handle_man = $('#employee').val();
		if (!+handle_man) {
			alertTips('warning', '请选择采购人员');
			return false;
		}
		var vendor = $('#vendor').val();
		if (!+vendor) {
			alertTips('warning', '请输入供应商');
			return false;
		}
		var remark = $('#head_remark').val();
		var pro_head = {
			'warehouse_id' : warehouse,
			'remark' : remark,
			'procurement_amount' : $('#wap_pro_amount').val(),
			'vendor_id' : vendor,
			'procurement_man':handle_man,
		};
		return pro_head;
	}
	
	//入库单明细
	var pro_detail = function(){
		var product_det=[];
		var product_info = [];
		var _tr = $('#wap_pro_detail').find('.item-info');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.find('.product_name').val();
			if(pid == 0){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var pid = tr.find('.product_name').val();
			if(pid == 0){
				alertTips('warning','请选择商品');
				return false;
			}
			var quantity = tr.find('.quantity').val();
			if(quantity == 0){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var price = tr.find('.price').val();
			if(price == 0){
				alertTips('warning','请填写单价');
				flag = 0;
				return false;
			}
			product_det = {
				'goods_id':tr.find('.product_name').val(),
				'gross_quantity': quantity,
				'gross_unit': tr.find('.unit').val(),
				'price' : tr.find('.price').val(),
				'remark': tr.find('.remark').val(),
				'amount': tr.find('.amount').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		return product_info;
	}


});