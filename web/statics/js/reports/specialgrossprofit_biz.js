$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

	//销售毛利列表
//	listSpecialGrossProfit();
	function listSpecialGrossProfit(pageInfo)
	{
		var global_Acount = 1;
		var global_Bcount = 1;
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#grossProfitlist').find('tr').remove();
		$('#salelist').find('tr').remove();
		$.get('get-special-gross-profit',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body;
					var IN = W.in;
					var OUT = W.out;
					var LOST = W.lost;
					var sum_out_quantity = 0;
					var sum_out_amount = 0;
					var tmp_unit_name = '';
					var sum_lost_quantity = 0;
					var sum_lost_amount = 0;
					var lost_unit_name = '';
					var sum_left_quantity = 0;
					var sum_left_amount = 0;
					var gross_profit = 0;//销售利润
					var RATE  = 0;//利润率
					if(OUT && OUT.length){
						for(var j in OUT){
							$('<tr><td class="text-center" name="ids">'+global_Bcount+'</td>'
									+'<td class="text-center product_id" name="product_id">'+OUT[j].product_id+'</td>'
									+'<td class="text-center product_name" name="product_name">'+OUT[j].product_name+'</td>'
									+'<td class="text-center out_quantity" name="out_quantity">'+OUT[j].sum_quantity+'</td>'
									+'<td class="text-center out_unit" name="out_unit">'+OUT[j].unit_name+'</td>'
									+'<td class="text-center out_amount" name="out_amount">'+OUT[j].sum_amount+'</td>'
									+'</tr>').appendTo('#salelist');
							sum_out_amount +=+OUT[j].sum_amount;
							sum_out_quantity += +OUT[j].sum_quantity;
							tmp_unit_name = OUT[j].unit_name;
							global_Bcount++;
						}
					}
					if(global_Bcount == 1){
						$('<tr><td class="text-center" colspan="16">没有数据....</td></tr>').appendTo('#salelist');
					}
					if(LOST && LOST.length){
						for(var k in LOST){
							sum_lost_quantity += +LOST[k].quantity;
							sum_lost_amount += +LOST[k].amount;
							lost_unit_name = LOST[k].unit_name;
						}
					}
					if(IN && IN.length){
						for(var i in IN){
							sum_left_quantity = IN[i].quantity - sum_out_quantity - sum_lost_quantity;
//							sum_left_amount = IN[i].amount - sum_out_amount - sum_lost_amount;
							gross_profit = parseFloat(sum_out_amount -IN[i].amount - sum_lost_amount);
							RATE = parseFloat(gross_profit / IN[i].amount *100);
							$('<tr><td class="text-center" name="ids">'+global_Acount+'</td>'
									+'<td class="text-center product_id" name="product_id">'+IN[i].goods_id+'</td>'
									+'<td class="text-center product_name" name="product_name">'+IN[i].product_name+'</td>'
									+'<td class="text-center quantity" name="in_quantity">'+IN[i].quantity+'</td>'
									+'<td class="text-center in_unit" name="in_unit">'+ IN[i].unit_name+'</td>'
									+'<td class="text-center in_amount" name="in_amount">'+IN[i].amount+'</td>'
									+'<td class="text-center out_quantity" name="out_quantity">'+sum_out_quantity.toFixed(2)+'</td>'
									+'<td class="text-center out_unit" name="out_unit">'+tmp_unit_name+'</td>'
									+'<td class="text-center out_amount" name="out_amount">'+sum_out_amount.toFixed(2)+'</td>'
									+'<td class="text-center lost_quantity" name="lost_quantity">'+sum_lost_quantity.toFixed(2)+'</td>'
									+'<td class="text-center lost_unit" name="lost_unit">'+lost_unit_name+'</td>'
									+'<td class="text-center lost_amount" name="lost_amount">'+sum_lost_amount.toFixed(2)+'</td>'
									+'<td class="text-center left_quantity" name="left_quantity">'+sum_left_quantity.toFixed(2)+'</td>'
									+'<td class="text-center left_unit" name="left_unit">'+IN[i].unit_name+'</td>'
//									+'<td class="text-center left_amount" name="left_amount">'+sum_left_amount.toFixed(2)+'</td>'
									+'<td class="text-center gross_profit" name="gross_profit">'+gross_profit.toFixed(2)+'</td>'
									+'<td class="text-center rate" name="rate">'+RATE.toFixed(2)+'</td>'
									+'</tr>').appendTo('#grossProfitlist');
							global_Acount++;
						}
					}
					if(global_Acount == 1){
						$('<tr><td class="text-center" colspan="12">没有数据....</td></tr>').appendTo('#grossProfitlist');
					}
					
				} else {
					$('<tr><td colspan="18" class="text-center">没有数据</td></tr>').appendTo('#grossProfitlist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSpecialGrossProfit);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime ==''){
			alertTips('warning','请选择时间！');
			return false;
		}
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
//			'vendor':$('#vendor').val(),
//			'warehouse':$('#warehouse').val(),
//			'customer' : $('#customer').val(),
//			'name' : $('#name').val(),
//			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		var goods_id = $('#special_goods').val();
		if(!+goods_id){
			alertTips('warning','请选择商品！');
			return false;
		}
		global_info.filter = goods_id;
		global_info.page = 1;
		global_info.pageSize =10;
		listSpecialGrossProfit();
	});
	
//	
//	window.onload = function() {
//		
//		var oBox = $("box"), oTop = $("top"), oBottom = $("bottom"), oLine = $("line");
//		alert(121);
//		oLine.onmousedown = function(e) {
//			var disX = (e || event).clientX;
//			oLine.left = oLine.offsetLeft;
//			document.onmousemove = function(e) {
//				var iT = oLine.left + ((e || event).clientX - disX);
//				var e=e||window.event,tarnameb=e.target||e.srcElement;
//				var maxT = oBox.clientWight - oLine.offsetWidth;
//				oLine.style.margin = 0;
//				iT < 0 && (iT = 0);
//				iT > maxT && (iT = maxT);
//				oLine.style.left = oTop.style.width = iT + "px";
//				oBottom.style.width = oBox.clientWidth - iT + "px";
//				$("msg").innerText='top.width:'+oLine.style.width+'---bottom.width:'+oBottom.style.width+'---oLine.offsetLeft:'+oLine.offsetLeft+'---disX:'+disX+'---tarnameb:'+tarnameb.tagName;
//				return false
//			};
//			document.onmouseup = function() {
//				document.onmousemove = null;
//				document.onmouseup = null;
//				oLine.releaseCapture && oLine.releaseCapture()
//			};
//			oLine.setCapture && oLine.setCapture();
//			return false
//		};
//	};
})
$(function(){
	

//window.onload = function() {
//	var oBox = $("#box")[0], oBottom = $("#bottom")[0], oLine = $("#line")[0];
//	console.log(oBox);
//	oLine.onmousedown = function(e) {
//		var disY = (e || event).clientY;
//		oLine.top = oLine.offsetTop;
//		document.onmousemove = function(e) {
//			var iT = oLine.top + ((e || event).clientY - disY);
//			var maxT = oBox.clientHeight - oLine.offsetHeight;
//			oLine.style.margin = 0;
//			iT < 0 && (iT = 0);
//			iT > maxT && (iT = maxT);
//			oLine.style.top = oBottom.style.top = iT + "px";
//			return false
//		};	
//		document.onmouseup = function() {
//			console.log(121);
//			document.onmousemove = null;
//			document.onmouseup = null;	
//			oLine.releaseCapture && oLine.releaseCapture()
//		};
//		oLine.setCapture && oLine.setCapture();
//		return false
//	};
//};
	
//	window.onload = function() {
//		
//		var oBox = $("box"), oTop = $("top"), oBottom = $("bottom"), oLine = $("line");
//		alert(121);
//		oLine.onmousedown = function(e) {
//			var disX = (e || event).clientX;
//			oLine.left = oLine.offsetLeft;
//			document.onmousemove = function(e) {
//				var iT = oLine.left + ((e || event).clientX - disX);
//				var e=e||window.event,tarnameb=e.target||e.srcElement;
//				var maxT = oBox.clientWight - oLine.offsetWidth;
//				oLine.style.margin = 0;
//				iT < 0 && (iT = 0);
//				iT > maxT && (iT = maxT);
//				oLine.style.left = oTop.style.width = iT + "px";
//				oBottom.style.width = oBox.clientWidth - iT + "px";
//				$("msg").innerText='top.width:'+oLine.style.width+'---bottom.width:'+oBottom.style.width+'---oLine.offsetLeft:'+oLine.offsetLeft+'---disX:'+disX+'---tarnameb:'+tarnameb.tagName;
//				return false
//			};
//			document.onmouseup = function() {
//				document.onmousemove = null;
//				document.onmouseup = null;
//				oLine.releaseCapture && oLine.releaseCapture()
//			};
//			oLine.setCapture && oLine.setCapture();
//			return false
//		};
//	};
})