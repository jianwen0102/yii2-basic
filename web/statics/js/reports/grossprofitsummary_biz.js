$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	//销售毛利列表
//	listSaleGrossProfit();
	function listSaleGrossProfit(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#grossProfitlist').find('tr').remove();
		$.post('get-sale-gross-profit-summary',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						var _TOTAL_GOODS_AMOUNT = 0;
						var _TOTAL_BASE_AMOUNT = 0;
						var _TOTAL_GROSS_PROFIT = 0;
						var _RATE = 0;
						var _TOTAL_QUANTITY = 0;
						var _TOTAL_BASE_PRICE = 0;
						for(var i in W){
							var sum_sale_amount = +W[i].sum_sale_amount;
							var sum_cost_amount = +W[i].sum_cost_amount;
							var gross_profit = +(sum_sale_amount - sum_cost_amount).toFixed(2);
							_TOTAL_GOODS_AMOUNT += sum_sale_amount;
							_TOTAL_BASE_AMOUNT += sum_cost_amount;
							_TOTAL_GROSS_PROFIT += gross_profit;
							_TOTAL_QUANTITY += +W[i].sum_quantity;
							
							if(sum_sale_amount) {
								_RATE = +(gross_profit/sum_sale_amount*100).toFixed(2)
							} else {
								_RATE = 0;
							}
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center product_id" name="product_id">'+W[i].product_id+'</td>'
							+'<td class="text-center product_name" name="product_name">'+W[i].product_name+'</td>'
							+'<td class="text-center quantity" name="quantity">'+W[i].sum_quantity+'</td>'
							+'<td class="text-center sum_sale_amount" name="sum_sale_amount">'+W[i].sum_sale_amount+'</td>'
							+'<td class="text-center sum_cost_amount" name="sum_cost_amount">'+W[i].sum_cost_amount+'</td>'
							+'<td class="text-center gross_profit" name="gross_profit">'+gross_profit+'</td>'
							+'<td class="text-center rate" name="rate">'+_RATE+'</td>'
							+'</tr>').appendTo('#grossProfitlist');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSaleGrossProfit);
			                }
						};
						
						$('<tr><td class="text-center">合计</td><td colspan="2"></td><td class="text-center">'+_TOTAL_QUANTITY.toFixed(2)
							+'</td><td class="text-center">'+_TOTAL_GOODS_AMOUNT.toFixed(2)+'</td><td class="text-center">'+_TOTAL_BASE_AMOUNT.toFixed(2)+'</td><td class="text-center">'+_TOTAL_GROSS_PROFIT.toFixed(2)+'</td><td></td></tr>').appendTo('#grossProfitlist');
					}else {
						$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#grossProfitlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleGrossProfit);
					}
				} else {
					$('<tr><td colspan="18" class="text-center">没有数据</td></tr>').appendTo('#grossProfitlist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleGrossProfit);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'warehouse':$('#warehouse').val(),
			'customer' : $('#customer').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		global_info.page = 1;
		global_info.pageSize =10;
		listSaleGrossProfit();
	});
})