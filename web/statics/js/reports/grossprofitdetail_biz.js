$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	//销售毛利列表
	listSaleGrossProfit();
	function listSaleGrossProfit(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#grossProfitlist').find('tr').remove();
		$.get('get-sale-gross-profit-detail',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						var _TOTAL_SELL_AMOUNT = 0;
						var _TOTAL_BASE_AMOUNT = 0;
						var _TOTAL_GROSS_PROFIT = 0;
						var _RATE =0;
						var _TOTAL_BASE_PRICE = 0;
						for(var i in W){
							var sell_amount = +((+W[i].quantity) * (+W[i].sell_price)).toFixed(2);
							var base_amount = +((+W[i].quantity) * (+W[i].base_price)).toFixed(2);
							var gross_profit = (sell_amount - base_amount);
							_TOTAL_SELL_AMOUNT += sell_amount;
							_TOTAL_BASE_AMOUNT += base_amount;
							_TOTAL_GROSS_PROFIT += gross_profit;
							_TOTAL_BASE_PRICE += +W[i].base_price;
							
							if(sell_amount) {
								_RATE = +(gross_profit/sell_amount * 100).toFixed(2)
							} else {
								_RATE = 0;
							}
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center order_time" name="order_time">'+intToLocalDate(W[i].order_time,1)+'</td>'
							+'<td class="text-center order_sn" name="order_sn" data-id="'+W[i].order_id+'">'+W[i].order_sn+'</td>'
							+'<td class="text-center no">'+''+'</td>'
							+'<td class="text-center customer">'+W[i].nickname+'</td>'
							+'<td class="text-center warehouse">'+W[i].warehouse_name+'</td>'
							+'<td class="text-center goods_id" name="goods_id">'+W[i].goods_id+'</td>'
							+'<td class="text-center goods_name" name="goods_name">'+W[i].goods_name+'</td>'
							+'<td class="text-center unit" name="unit">'+W[i].unit_name+'</td>'
							+'<td class="text-center quantity" name="quantity">'+W[i].quantity+'</td>'
							+'<td class="text-center sell_price" name="sell_price">'+W[i].sell_price+'</td>'
							+'<td class="text-center sell_amount" name="sell_amount">'+sell_amount+'</td>'
							+'<td class="text-center base_price" name="base_price">'+W[i].base_price+'</td>'
							+'<td class="text-center base_amount" name="base_amount">'+base_amount+'</td>'
							+'<td class="text-center gross_profit" name="gross_profit">'+gross_profit+'</td>'
							+'<td class="text-center rate" name="rate">'+_RATE+'</td>'
							+'</tr>').appendTo('#grossProfitlist');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSaleGrossProfit);
			                }
						};
						
						$('<tr><td class="text-center">合计</td><td colspan="10"></td><td class="text-center">'+_TOTAL_SELL_AMOUNT.toFixed(2)
							+'</td><td class="text-center">'+_TOTAL_BASE_PRICE+'</td><td class="text-center">'+_TOTAL_BASE_AMOUNT.toFixed(2)+'</td><td class="text-center">'+_TOTAL_GROSS_PROFIT.toFixed(2)+'</td><td></td></tr>').appendTo('#grossProfitlist');
					}else {
						$('<tr><td colspan="18" class="text-center">没有数据</td></tr>').appendTo('#grossProfitlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleGrossProfit);
					}
				} else {
					$('<tr><td colspan="18" class="text-center">没有数据</td></tr>').appendTo('#grossProfitlist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleGrossProfit);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'warehouse':$('#warehouse').val(),
			'customer' : $('#customer').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		global_info.page = 1;
		global_info.pageSize =10;
		listSaleGrossProfit();
	});
})