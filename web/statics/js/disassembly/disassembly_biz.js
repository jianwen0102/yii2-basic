$(function(){
	var _id = parseInt($_GET['id']);
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
	}
	var global_in = 1;
	var global_out =1;
	var global_remove = [];
	var select_units;
	//时间
	dateSelFun($('#disassembly_date'));
	
	/**
	 * @desc 单位//单位要根据product_id 取
	 * 
	 */
	(function(){
		select_units = '<select title="单位" class="form-control unit">'
			+ '<option value="0">单位</option>'
			+ '<option value="6">个</option>'
			+ '<option value="5">支</option>'
			+ '<option value="4">包</option>'
			+ '<option value="2">斤</option>'
			+ '<option value="1">箱</option>' + '</select>';
	})();
	
	if(_id){
		initdisassembly();
	}
	
	
	/**
	 * @初始化页面
	 */
	function initdisassembly() {
		$('#confirm').show();
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-disassembly-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#disassembly_no').val(H.disassembly_no);
					$('#disassembly_date').val(intToLocalDate(H.disassembly_date,1));
					$('#warehouse_out').val(H.warehouse_out);
					$('#warehouse_in').val(H.warehouse_in);
					$('#disassembly_man').val(H.disassembly_man);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					if(+H.confirm_flag){
						$('#save').attr('style','display:none');
						$('#confirm').removeClass().addClass('btn btn-warning btn-sm pull-right').attr('disabled','disabled');
					}
					$('#disassembly_amount').val(H.disassembly_amount);//总金额
				}
				var D = data.Body.det;
				if(D){
					$('#disassemblyOutList').find('tr').remove();
					$('#disassemblyInList').find('tr').remove();
					for(var i in D){
						var TYPE = +D[i].type;
						var tmp_units= '';
						var U = D[i].product_unit;
						tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
						for(var j in U){
							tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
						}
						tmp_units +='</select>';
						if(TYPE){
							//in
							$('<tr>'
							   +'<td class="text-center" name="ids" data-id="'+D[i].disassembly_det_id+'">'+global_in+'</td>'
							   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].product_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
							   +'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
							   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
							   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
							   +'<td class="text-center" name="product_sn">'+D[i].product_sn+'</td>'
							   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
							   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
							   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
							   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
							   +'<td class="text-center" name="unit_relation">'+D[i].unit_content+'</td>'
							   +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
							   +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
							   +'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
							   +'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'">'+D[i].base_name+'</td>'
							   +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].base_quantity+'" disabled></td>'
							   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
							   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
							   +'</tr>').appendTo('#disassemblyInList');
							$('#disassemblyInList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
							$('#disassemblyInList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
							global_in++;
						} else {
							//out
							$('<tr>'
								+'<td class="text-center" name="ids" data-id="'+D[i].disassembly_det_id+'">'+global_out+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].product_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
								+'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
								+'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
								+'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
								+'<td class="text-center" name="product_sn">'+D[i].product_sn+'</td>'
								+'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
								+'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
								+'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
								+'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
								+'<td class="text-center" name="unit_relation">'+D[i].unit_content+'</td>'
								+'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
								+'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
								+'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
								+'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'">'+D[i].base_name+'</td>'
								+'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].base_quantity+'" disabled></td>'
								+'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
								+'</tr>').appendTo('#disassemblyOutList');
							$('#disassemblyOutList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
							$('#disassemblyOutList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
							global_out++;
						}
					}
				}
			}
		});
	};
	
	/**
	 * @desc 选择商品
	 */
	$('#disassembly_out').on('click','.search-product',function(){
		$('#productName').val('');
		global_product.page = 1;
		global_product.name = undefined;
		var pageInfo = undefined;
		var flag = 0;
		getProducts(pageInfo, flag);
		
	})
	
	$('#disassembly_in').on('click','.search-product',function(){
		$('#productName').val('');
		global_product.page = 1;
		global_product.name = undefined;
		var pageInfo = undefined;
		var flag = 1;
		getProducts(pageInfo, flag);
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		global_product.page = 1;
		var pageInfo = undefined;
		var flag  = $(this).attr('data-flag');
		getProducts(pageInfo, flag);
		return;
	})
	
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo, flag){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		if(flag == 0){
			global_product.ware = $('#warehouse_out').val();
			if(global_product.ware == 0){
				alertTips('error','请先选择拆装前仓库');
				return false;
			}
		} else {
			global_product.ware = $('#warehouse_in').val();
			if(global_product.ware == 0){
				alertTips('error','请先选择拆装后仓库');
				return false;
			}
		}
		$('#product_list').find('tr').remove();
		var index = layer.load();
		 $.ajax({
             url: 'get-disassembly-product-by-ware',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 if(data.Ack =='Success'){
	            	 var P = data.Body.list;
						if(P && P.length){
							for(var i in P){
								var tmp_units = ''
								var U = P[i].product_unit;
								tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
								for(var j in U){
									tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
								}
								tmp_units +='</select>';
								$('<tr>'
								+'<td class="text-center">'+(+i+1)+'</td>'
								+'<td class="text-center id">'+P[i].product_id+'</td>'
								+'<td class="text-center name">'+P[i].product_name+'</td>'
								+'<td class="text-center product_sn">'+P[i].product_sn+'</td>'
								+'<td class="text-center quantity">'+P[i].quantity+'</td>'
								+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'" data-rela="'+P[i].unit_content+'">'+(P[i].unit_name == null ? '':P[i].unit_name)+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="product_select" data-unit="'+encodeURI(tmp_units)+'" data-flag="'+flag+'">选择</a></td>'
								+'</tr>').appendTo('#product_list');
								
								$('#searchProduct').attr('data-flag', flag);
				              
							}
							var pageInfo = data.Body.page;
				            if (typeof pageInfo !== 'undefined'){
				              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
				            }
						}else {
							$('<tr><td colspan="7" class="text-center">没有数据</td></tr>').appendTo('#product_list');
							refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
						}
					$('#chooseProduct').modal('show');//选择商品窗口
	             } else{
	            	 
	             }
             }
		 })
	}
	
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var FLAG = +$(this).attr('data-flag');
		if(FLAG){
			//in
			var Object = $('#disassemblyInList');
			var global_count = global_in;
			global_in++;
		} else{
			//out
			var Object = $('#disassemblyOutList');
			var global_count = global_out;
			global_out++;
		}
		var tmp_units = decodeURI($(this).attr('data-unit'));
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var product_sn = tr.find('.product_sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		if(!+unit){
			alertTips('warning','请在商品管理添加单位');
			return false;
		}
		var unit_name = tr.find('.unit').text();
		var unit_content = tr.find('.unit').attr('data-rela');
		if(global_count == 1){
			var pid = Object.find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				Object.find('tr:first').remove();
			} ;
		}
//		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
	
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="product_sn">'+product_sn+'</td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity"></td>'
		  +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price"></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount"></td>'
		  +'<td class="text-center" name="unit_relation" style="width:8%;">'+unit_content+'</td>'
		  +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0"></td>'
		  +'<td class="text-center" name="second_relation"></td>'
		  +'<td class="text-center" name="base_unit" data-id="'+unit+'">'+unit_name+'</td>'
		  +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0" disabled></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo(Object);
		Object.find('tr:last').find('[name="unit"]').find('.units').val(unit);
//		global_count++;
	})
	
	
	//删除数据
	$('#disassemblyOutList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'disassembly_det_id':tmp_id});
		}
		if(global_out == 1){
			return false;
		} else if(global_out == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#disassemblyOutList').find('tr:first').remove();
			global_out--;
			insertOrginHtml($('#disassemblyOutList'),0);
//			SumTransferAmount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#disassemblyOutList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_out--;
//                SumTransferAmount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_out--;
//                SumTransferAmount();
                return false;
            }
		}
		
	});
	
	var insertOrginHtml = function(object, flag){
		if(+flag){
			global_count = global_in;
		} else{
			global_count = global_out;
		}
		$('<tr>'
			+'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
			+'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
			+'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
			+'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
			+'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
			+'<td class="text-center" name="sn"></td>'
			+'<td class="text-center" name="quantity"><input type="text"></td>'
			+'<td class="text-center" name="unit">'+select_units+'</td>'
			+'<td class="text-center" name="price"><input type="text"></td>'
			+'<td class="text-center" name="amount"><input type="text"></td>'
			+'<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_unit">'+select_units+'</td>'
			+'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_relation"></td>'
			+'<td class="text-center" name="base_unit"><input title="基本单位" type="text" value="0.00"></td>'
			+'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0.00" disabled></td>'
			+'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
			+'</tr>').appendTo(object);
	};
	
	
	//删除数据
	$('#disassemblyInList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'disassembly_det_id':tmp_id});
		}
		if(global_in == 1){
			return false;
		} else if(global_in == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#disassemblyInList').find('tr:first').remove();
			global_in--;
			insertOrginHtml($('#disassemblyInList'), 1);
//			SumTransferAmount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#disassemblyInList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_in--;
//                SumTransferAmount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_in--;
//                SumTransferAmount();
                return false;
            }
		}
		
	});
	
	//库存提示
	$('#disassemblyInList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	$('#disassemblyOutList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	
	//数量验证 计算价格金额
	$('#disassemblyOutList,#disassemblyInList').on('keyup','.quantity',function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _quantity = $(this).val();
		
		var tr = $(this).closest('tr');
		var _unit = tr.find('[name="unit"] select').val();
		var base_unit = tr.find('[name="base_unit"]').attr('data-id');
		var _rate = tr.find('[name="unit"]').find('option:selected').attr('data-rate');
		if(_unit == base_unit){
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		} else {
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		}
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = _quantity * _rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(_quantity*_rate/second_rate)+' '+second_name + ' '+(_quantity*_rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		
		var _price = $(this).closest('tr').find('.price').val();
		if(_price == ''){
			_price = 0
		}
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
//		SumAmount();
	});
	
	//单位验证
	$('#disassemblyOutList,#disassemblyInList').on('change','[name="unit"] select', function(){
		var unit = $(this).val();
		var rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		var quantity = tr.find('[name="quantity"] input').val();
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		tr.find('[name="base_quantity"] input').val(quantity * rate);
		
		
	})
	
	//辅助单位
	$('#disassemblyOutList,#disassemblyInList').on('change','[name="second_unit"] select',function(){
		var second_unit = $(this).val();
		var  tr = $(this).closest('tr');
		if(+second_unit){
			var second_rate = $(this).find('option:selected').attr('data-rate');
			var quantity = tr.find('[name="quantity"] input').val();
			var rate = tr.find('[name="unit"]') .find('option:selected').attr('data-rate');
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		} else {
			tr.find('[name="second_quantity"] input').val('');
			tr.find('[name="second_relation"]').text('');
		}
		
	})
	
	//单位验证 计算价格金额
	$("#disassemblyOutList,#disassemblyInList").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').find('[name="quantity"] input').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _price = $(this).val();
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
//		SumAmount();
	});
	//金额验证 计算价格金额
	$('#disassemblyOutList,#disassemblyInList').on('keyup','.amount', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var _quantity = $(this).closest('tr').find('.quantity').val();
		var _amount = +$(this).val();

		var _price = (_amount/ _quantity).toFixed(2);
		$(this).closest('tr').find('.price').val(_price);
//		SumAmount();
	});
	
	
	/**
	 * @var 保存单据
	 */
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = disassembly_header();
			if(!head){
				return false;
			}
			//明细
			var detail_out = disassembly_out_detail();
			if(!detail_out){
				return false;
			}
			var detail_in = disassembly_in_detail();
			if(!detail_in){
				return false;
			}
			var ii = layer.load();
			$.get('save-disassembly',{'head':head,'det_out':detail_out,'det_in':detail_in,'remove': global_remove,'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
 							window.location.reload();
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var disassembly_header = function() {
		var no = $('#disassembly_no').val();
		var disassembly_date = $('#disassembly_date').val();
		if (disassembly_date.length == 0) {
			alertTips('warning', '请输入出库时间');
			return false;
		}
		var disassembly_man = $('#disassembly_man').val();
		if (!+disassembly_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		
		var warehouse_out = $('#warehouse_out').val();
		if (!+warehouse_out) {
			alertTips('warning', '请选择调出仓库');
			return false;
		}
		var warehouse_in = $('#warehouse_in').val();
		if(!+warehouse_in){
			alertTips('warning','请选择调入仓库');
			return false;
		}
		
//		if(warehouse_out == warehouse_in){
//			alertTips('warning','调入仓库不能与调出仓库一样,请重新选择');
//			return false;
//		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var disassembly_head = {
			'disassembly_no' : no,
			'disassembly_date' : disassembly_date,
			'warehouse_out' : warehouse_out,
			'warehouse_in'  : warehouse_in,
			'disassembly_man' : disassembly_man,
			'create_man' : create_man,
			'remark' : remark,
			'disassembly_amount':$('#disassembly_amount').val(),
		};
		return disassembly_head;
	}
	
	//盘盈单明细
	var disassembly_out_detail = function(){
		var product_det=[];
		global_det = [];
		var product_info = [];
		var _tr = $('#disassemblyOutList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var stock = tr.find('.checkStock').attr('data-quantity');
			if(!+stock){
				var name = tr.find('[name="name"] input').val();
				alertTips('warning',name+'是0库存，不能出库。');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			
			product_det = {
				'disassembly_det_id' : line_id,
				'product_id' : tr.find('[name="name"]').attr('data-id'),
				'type' : 0,
				'quantity' :quantity, 
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
//				'discount' : tr.find('[name="discount"] input').val(),
				'second_unit':tr.find('[name="second_unit"] select').val(),
				'second_quantity':tr.find('[name="second_quantity"] input').val(),
				'second_relation':tr.find('[name="second_relation"]').text(),
				'base_unit':tr.find('[name="base_unit"]').attr('data-id'),
				'base_quantity':tr.find('[name="base_quantity"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		
		return product_info;
	}
	
	
	var disassembly_in_detail = function(){
		var product_det=[];
		global_det = [];
		var product_info = [];
		var _tr = $('#disassemblyInList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			
			product_det = {
				'disassembly_det_id' : line_id,
				'product_id' : tr.find('[name="name"]').attr('data-id'),
				'type' : 1,
				'quantity' :quantity, 
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
//				'discount' : tr.find('[name="discount"] input').val(),
				'second_unit':tr.find('[name="second_unit"] select').val(),
				'second_quantity':tr.find('[name="second_quantity"] input').val(),
				'second_relation':tr.find('[name="second_relation"]').text(),
				'base_unit':tr.find('[name="base_unit"]').attr('data-id'),
				'base_quantity':tr.find('[name="base_quantity"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		
		return product_info;
	}
	
	//编辑页面的确认
	$('#confirm').on('click',function(){		
		//检查是否有确认拆装单功能
		$.get('check-confirm-disassembly',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			layer.confirm('点击确认拆装单将无法编辑，是否确认?', function(index) {
				$.get('confirm-disassembly',{'id':_id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						} else if(data.Error="out_quantity lt base_quantity"){
							alertTips('error','拆卸的商品商品大于库存，请调整数量！');
							return false;
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
})