$(function(){
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
	}
	var _id = parseInt($_GET['id']);
	
	var global_count = 1;
	var select_units;
	
	dateSelFun($('#pur_time'));
	
	/**
	 * @desc 单位
	 * 
	 */
	(function(){
		select_units = '<select class="form-control units"><option value="0">请选择单位..</option>';
		var ii = layer.load();
		$.get('../instore/list-units', function(data, status){
			layer.close(ii);
			if(status =='success' && data.Ack =='Success'){
				var U = data.Body;
				for(var i in U){
					select_units +='<option value="'+U[i].unit_id+'">'+U[i].unit_name+'</option>';
				}
			}
			select_units +='</select>';
			initProcurement();
		});
	
	})();
	/**
	 * @desc 初始化采购单页面
	 */
	var initProcurement = function(){
		$('#confirm').show();
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-purchase-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#pur_no').val(H.purchase_no);
					$('#supplier_id').val(H.vendor_id);
					$('#pur_time').val(intToLocalDate(H.purchase_date,1));
					$('#warehouse').val(H.warehouse_id);
					$('#handle_man').val(H.purchase_man);
					$('#vendor').val(H.vendor_id);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					$('#save').attr('data-origin',H.origin_id);
					$('#origin_type').val(H.purchase_type);
					if(+H.confirm_flag){
						$('#save').attr('style','display:none');
						$('#confirm').removeClass().addClass('btn btn-warning btn-sm pull-right').attr('disabled','disabled');
					}
					$('#origin_id').val(H.procurement_no).attr('data-id',H.procurement_id);
					$('#purchase_amount').val(H.purchase_amount);
				}
				var D = data.Body.det;
				if(D){
					$('#purchaseList').find('tr').remove();
					for(var i in D){
						$('<tr>'
						   +'<td class="text-center" name="ids" data-id="'+D[i].purchase_det_id+'" data-proid="'+D[i].procurement_det_id+'">'+(+i+1)+'</td>'
						   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].goods_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
						   +'<td class="text-center" name="name" data-id="'+D[i].goods_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
						   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
						   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
						   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
						   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
						   +'<td class="text-center" name="unit">'+select_units+'</td>'
						   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
						   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
						   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
//						   +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value='+D[i].received_quantity+'></td>'
//						   +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value='+D[i].minus_quantity+'></td>'
						   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
						   +'</tr>').appendTo('#purchaseList');
						$('#purchaseList').find('tr:last').find('.units').val(D[i].quantity_unit).attr('disabled','disabled');
						global_count++;
					}
				}
			}
		});
	};
	
	
	
	// 新增仓库
	$('#addWarehouse').on('click',function(){
		$('#myWarehouse').modal('show');
	})
	//仓库
	$('#warehouse_save').on('click', function() {
		$warehouse = $('#warehouse_name').val();
		if ($warehouse.length == 0) {
			alertTips('warning','请输入仓库名')
			return false;
		}
		var pflag = 0;
		$remark = $('#remark').val();
		$default = $('#default').prop('checked');
		$wid = $('#warehouse_save').attr('data-wid');
		var ii = layer.load();
		$.get('../instore/save-warehouse', {
			'name' : $warehouse,
			'remark' : $remark,
			'def' : $default,
			'wid' : $wid
		}, function(data, status) {
			layer.close(ii);
			if (status === 'success' && data.Ack == 'Success') {
				alertTips('success','保存成功');
				setTimeout(function() {
					$('#myWarehouse').modal('hide');
				}, 1000);
			} else if (data.Error == "warehouse is exists") {
				alertTips('warning','该仓库已经存在，请更改仓库名称');
				return false;
			} else {
				alertTips('error','网络错误');
				return false;
			}
		})
	});
	
		
	/**
	 * @desc 选择商品
	 */
	$('#purchase_detail').on('click','.search-product',function(){
		$('#productName').val('');
		getProducts();
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		getProducts();
		return;
	})
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		$('#product_list').find('tr').remove();
		var index = layer.load();
		 $.ajax({
             url: '../instore/get-products',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 var P = data.Body.list;
					if(P && P.length){
						for(var i in P){
							$('<tr>'
							+'<td class="text-center">'+(+i+1)+'</td>'
							+'<td class="text-center id">'+P[i].product_id+'</td>'
							+'<td class="text-center name">'+P[i].product_name+'</td>'
							+'<td class="text-center sn">'+P[i].product_sn+'</td>'
							+'<td class="text-center quantity">'+P[i].quantity+'</td>'
							+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'">'+P[i].unit_name+'</td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="product_select">选择</a></td>'
							+'</tr>').appendTo('#product_list');
			              
						}
						var pageInfo = data.Body.page;
			            if (typeof pageInfo !== 'undefined'){
			              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
			            }
					}else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#product_list');
						refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
					}
				$('#chooseProduct').modal('show');//选择商品窗口
             }
		 })
	}
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var sn = tr.find('.sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		var unit_name = tr.find('.unit').text();
		if(global_count == 1){
			var pid = $('#purchaseList').find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				$('#purchaseList').find('tr:first').remove();
			} ;
		}
		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="sn">'+sn+'</td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity"></td>'
		  +'<td class="text-center" name="unit">'+units+'</td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price"></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount"></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//		  +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value="0"></td>'
//		  +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value="0"></td>'
		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo('#purchaseList');
		global_count++;
	})
	
	//删除数据
	$('#purchaseList').on('click','.stock-minus-tr',function(){
		if(global_count == 1){
			return false;
		} else if(global_count == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#purchaseList').find('tr:first').remove();
			global_count--;
			insertOrginHtml();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#purchaseList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_count--;
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_count--;
                return false;
            }
		}
		
	});
	
	var insertOrginHtml = function(){
		$('<tr>'
				  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
				  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
				  +'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
				  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
				  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
				  +'<td class="text-center" name="sn"></td>'
				  +'<td class="text-center" name="quantity"><input type="text"></td>'
				  +'<td class="text-center" name="unit">'+select_units+'</td>'
				  +'<td class="text-center" name="price"><input type="text"></td>'
				  +'<td class="text-center" name="amount"><input type="text"></td>'
				  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//				  +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value="0"></td>'
//				  +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value="0"></td>'
				  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock">库存</a></td>'
				  +'</tr>').appendTo('#purchaseList');
	}
	//库存提示
	$('#purchaseList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//名称提示
	$('#purchaseList').on('click','.product_name',function(){
		var name = $(this).val();
		if(name !=undefined){
			layer.tips(name, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	//数量验证 计算价格金额
	$('#purchaseList').on('keyup','.quantity',function(){
		this.value = this.value.replace(/[^\d]/g, '');
		var _quantity = $(this).val();
		var _price = $(this).closest('tr').find('.price').val()
		if(_price == ''){
			_price = 0
		}
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
//		$(this).closest('tr').find('.minus_quantity').val(_quantity);
//		$(this).closest('tr').find('.rec_quantity').val(_quantity);
		SumAmount();
	});
	//单位验证 计算价格金额
	$("#purchaseList").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').find('[name="quantity"] input').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value = this.value.replace(/[^0-9.]/g, '');
		var _price = $(this).val();
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
		SumAmount();
	});
	//金额验证 计算价格金额
	$('#purchaseList').on('keyup','.amount', function(){
		this.value = this.value.replace(/[^0-9.]/g, '');
		
		var _quantity = $(this).closest('tr').find('.quantity').val();
		if(_quantity == ''){
			$(this).closest('tr').find('.price').val(0);
		} else {
			var _amount = $(this).val();
			var _price = (_amount / _quantity).toFixed(2);
			$(this).closest('tr').find('.price').val(_price);
		}
		SumAmount();
	});
	
	/**
	 * @var 计算总金额
	 */
	var SumAmount = function(){
		var total_amount = 0;
		$('#purchaseList tr').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			
		})
		$('#purchase_amount').val(total_amount);
	};
	
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = pur_header();
			if(!head){
				return false;
			}
			//明细
			var detail = pur_detail();
			if(!detail){
				return false;
			}
			var _id = $(this).attr('data-id');
			var ii = layer.load();
			$.get('save-purchase',{'head':head,'det':detail, 'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
 							window.location.reload();
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var pur_header = function() {
		var no = $('#pur_no').val();
		var pur_time = $('#pur_time').val();
		if (pur_time.length == 0) {
			alertTips('warning', '请输入采购入库时间');
			return false;
		}
		var warehouse = $('#warehouse').val();
		if (!+warehouse) {
			alertTips('warning', '请选择入库仓库');
			return false;
		}
		var handle_man = $('#handle_man').val();
		if (!+handle_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		var vendor = $('#supplier_id').val();
		if (!+vendor) {
			alertTips('warning', '请输入供应商');
			return false;
		}
		var origin_id = $('#origin_id').attr('data-id');
		if(origin_id == ''){
			origin_id = 0;
		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var pur_head = {
			'purchase_no' : no,
			'purchase_date' : pur_time,
			'purchase_type' : $('#origin_type').val(),
			'warehouse_id' : warehouse,
			'purchase_man' : handle_man,
			'vendor_id' : vendor,
			'create_man' : create_man,
			'remark' : remark,
			'procurement_id': origin_id,
			'purchase_amount':$('#purchase_amount').val(),
		};
		return pur_head;
	}
	
	//采购单明细
	var pur_detail = function(){
		var product_det=[];
		var product_info = [];
		var _tr = $('#purchaseList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			if(price == '' || price == 0){
				if(!+price){
					if(confirm("商品单价为0,是否继续？")){
						price = 0;
						amount = 0;
					} else {
						flag = 0;
						return false;
					}
				}
			}
			var pro_det_id = tr.find('[name="ids"]').attr('data-proid');
			if(pro_det_id == undefined || pro_det_id ==''){
				pro_det_id = 0;
			}
			product_det = {
				'purchase_det_id' : line_id,
				'goods_id' : tr.find('[name="name"]').attr('data-id'),
				'quantity' : quantity,
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
				'procurement_det_id': pro_det_id,
//				'minus_quantity':tr.find('[name="minus_quantity"] input').val(),//编辑处不用改
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		return product_info;
	}
	
	//编辑页面的确认
	$('#confirm').on('click',function(){
		//检查是否有确认功能
		$.get('check-confirm-pur',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('点击确认入库单将无法编辑，是否确认?', function(index) {
				$.get('confirm-purchase',{'id':_id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
});