$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
		}
	
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	
	global_info.cond ={
			'starTime': $('#starTime').val(),
			'endTime': $('#endTime').val(),
	};
	listPurchaseVendor();
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		if (starTime.length == 0) {
			alertTips('warning', '请输入查询时间');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		
		listPurchaseVendor();
	});
	
	function listPurchaseVendor(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#purVenList').find('tr').remove();
		$.get('get-purchase-count',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						for(var i in W){
							var settle_pay_date = W[i].settle_pay_date?intToLocalDate(W[i].settle_pay_date,1):'';
							var left_amount = +W[i].left_amount?W[i].amount:0;
							var rece_quantity = +W[i].left_amount? 0 : W[i].base_quantity;
							var rece_amount = +W[i].left_amount?0:W[i].amount;
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center pay_time">'+intToLocalDate(W[i].pay_time,1)+'</td>'
							+'<td class="text-center vendor">'+W[i].supplier_name+'</td>'
							+'<td class="text-center product">'+W[i].product_name+'</td>'
							+'<td class="text-center price">'+W[i].price+'</td>'
							+'<td class="text-center unit">'+W[i].unit_name+'</td>'
							+'<td class="text-center quantity">'+W[i].base_quantity+'</td>'
							+'<td class="text-center amount">'+W[i].amount+'</td>'
							+'<td class="text-center receive_quantity">'+rece_quantity+'</td>'
							+'<td class="text-center receive_amount">'+rece_amount+'</td>'
							+'<td class="text-center settle_pay_date">'+settle_pay_date+'</td>'
							+'<td class="text-center left_amount">'+left_amount+'</td>'
//							+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
							+'</tr>').appendTo('#purVenList');
				       var pageInfo = data.Body.page;
				       if (typeof pageInfo !== 'undefined'){
				            refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listPurchaseVendor);
				       }
					  }
				}else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#purVenList');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPurchaseVendor);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#purVenList');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPurchaseVendor);
				}
		} else{
			alertTips('error','网络错误');
		}
	  });
	};
	
	//导出execl
	$('#export').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		if (starTime.length == 0) {
			alertTips('warning', '请输入查询时间');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		var flag = 0;
		$.get('check-purchase-vendor-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag = 1;
				return;
			}
			$url ='export-purchase-vendor?starTime='+global_info.cond.starTime+'&endTime='+global_info.cond.endTime+'&vendor='+global_info.cond.vendor+
				'&id='+global_info.cond.id+'&name='+global_info.cond.name;;
			if(!flag){
				window.location.href = $url;
			}
		})
	})
	
})