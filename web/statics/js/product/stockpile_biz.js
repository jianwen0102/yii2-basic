$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
//	//时间
	
	listStock();
	//获取出入库明细列表
	function listStock(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#stockpilelist').find('tr').remove();
		$.get('get-stock-pile',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var T = data.Body.list;
					if(T && T.length){
						for(var i in T){
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center" name="pid">'+T[i].product_id+'</td>'
							+'<td class="text-center" name="name">'+T[i].product_name+'</td>'
							+'<td class="text-center" name="sn">'+T[i].product_sn+'</td>'
							+'<td class="text-center" name="category">'+T[i].category_name+'</td>'
							+'<td class="text-center" name="gen_qty">'+T[i].genexpectstart_qty+'</td>'
							+'<td class="text-center" name="gen_amount">'+T[i].genexpectstart_amount+'</td>'
							+'<td class="text-center" name="price">'+T[i].price+'</td>'
							+'<td class="text-center" name="quantity">'+T[i].quantity+'</td>'
							+'<td class="text-center" name="unit">'+T[i].unit_name+'</td>'
							+'</tr>').appendTo('#stockpilelist');
				       var pageInfo = data.Body.page;
				       if (typeof pageInfo !== 'undefined'){
				            refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listStock);
				       }
					  }
				}else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#stockpilelist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listStock);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#stockpilelist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listStock);
				}
		} else{
			alertTips('error','网络错误');
		}
	  });
	};
	
	//查询
	$('#search_list').on('click',function(){
		global_info.cond ={
			'name': $('#productname').val(),
			'category':$('#category').val(),
		};
		
		listStock();
	});
	
	//过滤
	$('#filterProduct').on('change',function(){
		global_info.filter = $(this).val();
		global_info.page = 1;
		global_info.pageSize = 10;
		listStock();
	})
	
	// 导出
	$('#export_product').on('click',function(){
		var flag = 0;
		$.get('check-product-stock-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag =1;
				return;
			}
			global_info.cond ={
					'name': $('#productname').val(),
					'category':$('#category').val(),
			};
			global_info.filter = $('#filterProduct').val();
			$url ='export-product-stock?name='+global_info.cond.name+'&category='+global_info.cond.category+'&filter='+global_info.filter;
			if(!flag){
				window.location.href = $url;
			}
		})
	})
})