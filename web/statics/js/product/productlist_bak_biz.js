$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':undefined, //查询条件		
		};
	
		(function() {
			var info = getCookie('global_info');
			if(info== ""){
			} else {
				global_info = JSON.parse(info);
			}
		})();
		
		//产品列表
		listProduct();
		function listProduct(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#product_list').find('tr').remove();
			$.get('get-products',global_info,function(data,status,xhr){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var P = data.Body.list;
						if(P && P.length){
							for(var i in P){
								var unit_content = '';
								if(P[i].unit_content == false){
									unit_content = '';
								}else {
									unit_content = P[i].unit_content;
								}
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+P[i].product_id+'"></td>'
								+'<td class="text-center name">'+P[i].product_name+'</td>'
								+'<td class="text-center sn">'+P[i].product_sn+'</td>'
								+'<td class="text-center cate">'+P[i].category_name+'</td>'
								+'<td class="text-center supplier">'+P[i].supplier_name+'</td>'
								+'<td class="text-center quantity">'+P[i].quantity+'</td>'
								+'<td class="text-center unit">'+P[i].unit_name+'</td>'
								+'<td class="text-center unit_format">'+unit_content+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="product_edit fa fa-pencil-square-o fa-2x" title="编辑"></td>'
								+'</tr>').appendTo('#product_list');
								if(+P[i].ii < 0){
									$('#product_list tr:last').css('color', 'red')
								}
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listProduct);
				                }
							}
						}else {
							$('<tr><td colspan="8" class="text-center">没有数据</td></tr>').appendTo('#product_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listProduct);
						}
					} else {
						$('<tr><td colspan="8" class="text-center">没有数据</td></tr>').appendTo('#product_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listProduct);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
			setCookie('global_info','');
		}
		
		/**
		 * @desc 编辑
		 */
		$('#product_list').on('click', '.product_edit', function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			//检查是否有编辑商品
			$.get('check-edit-product',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				
				if(global_info.page == undefined){
					global_info.page == 1;
				}
				setCookie('global_info',JSON.stringify(global_info));
				location.href="edit-product?id="+id;
			})
		})
		
		/**
		 * @desc  全选
		 */
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		
		/**
		 * @desc 新增
		 */
		$('#add').on('click', function(){
			//检查是否有编辑商品
			$.get('check-add-product',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				location.href="add-product";
			})
		})
		/**
		 * @desc 查询
		 */
		$('#search_list').on('click',function(){
			global_info.cond = {
				'product_name':trim($('#select_name').val()),
				'category_id':$('#select_category').val(),
				'supplier_id':$('#select_supplier').val(),
			}
			global_info.page = 1;
			listProduct();
		});
		
		/**
		 * @desc 删除商品
		 */
		$('#delete').on('click',function(){
			//检查是否有编辑商品
			$.get('check-del-product',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				$('.ids').each(function(i){
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
					}
				})
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框！');
					return false;
				}
				if(confirm("确定要删除商品？")){
					$.get('del-product',{'ids':_all_ids},function(data,status){
						if(status ==='success' && data.Ack ==='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){ 
								listProduct();
							},1500);
						}else {
							alertTips('error','删除失败，请重新再试');
						}
					});
				 }
			})
		});
		
	//导出execl
	$('#explode_list').on('click',function(){
		$.get('check-explode-product',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error','你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		})
		
		var name = trim($('#select_name').val());
		var catid = $('#select_category').val();
		var supid = $('#select_supplier').val(),
		$url ='export-product-list?name='+name+'&category='+catid+'&vendor='+supid;
		
		window.location.href = $url;
	});
	
	//导入execl
//	$('#implode').on('click',function(){
//		$.get('check-implode-product',function(data,status){
//			if(data.Error =='User authentication fails'){
//				alertTips('error','你没有操作权限，如要操作，请向管理员申请');
//				return;
//			}
//		})
//		console.log($('#implodeform').serialize());
//		return false;
//		
////		$.post('implode-product-list',$('#implodeform').serialize(), function(data,status){
//			
//		})
//		
//	})
	
//	(function(){
//		$('#implode').on('click',function(){
//			var form = document.getElementById('implodeform');  
//			var formdata = new FormData(form);  
//			formdata.append('name','fdipzone');  
//			formdata.append('gender','male');  
////			var name = $("#implode").val();
////			formData.append("file",$("#fileUplode")[0].files[0]);
//			console.log(formdata);
//			return false;
//	//		formData.append("name",name);
//			$.ajax({ 
//			url : 'implode-product-list', 
//			type : 'POST', 
//			data : formData, 
//			// 告诉jQuery不要去处理发送的数据
//			processData : false, 
//			// 告诉jQuery不要去设置Content-Type请求头
//			contentType : false,
//			beforeSend:function(){
//			console.log("正在进行，请稍候");
//			},
//			success : function(responseStr) { 
//			if(responseStr.status===0){
//			console.log("成功"+responseStr);
//			}else{
//			console.log("失败");
//			}
//			}, 
//			error : function(responseStr) { 
//			console.log("error");
//			} 
//			});
//		});
//	})();
	
//	function fsubmit(){  
//        var data = new FormData($('#form1')[0]);  
//        $.ajax({  
//            url: 'server.php',  
//            type: 'POST',  
//            data: data,  
//            dataType: 'JSON',  
//            cache: false,  
//            processData: false,  
//            contentType: false  
//        }).done(function(ret){  
//            if(ret['isSuccess']){  
//                var result = '';  
//                result += 'name=' + ret['name'] + '<br>';  
//                result += 'gender=' + ret['gender'] + '<br>';  
//                result += '<img src="' + ret['photo']  + '" width="100">';  
//                $('#result').html(result);  
//            }else{  
//                alert('提交失敗');  
//            }  
//        });  
//        return false;  
//    }  
	
});