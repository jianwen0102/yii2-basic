$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
//			'filter':undefined//过滤条件
		}
	listStock();
	function listStock(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#stocklist').find('tr').remove();
		$('#hhead').find('th').remove();
		$('#hdet').find('th').remove();
		$.get('get-stock-info',global_info,function(data,status){
			if(status =='success'){
				if(data.Ack =='Success'){
					var M = data.Body.list;
					var HHEAD = '<th class="text-center" name="pid" colspan="5">商品编号</th>';
					var HDET = '<th class="text-center">序号</th>'
							+'<th class="text-center" name="pid">商品编号</th>'
	 						+'<th class="text-center" name="name">商品名称</th>'
	 						+'<th class="text-center" name="unit">单位</th>'
	 						+'<th class="text-center" name="price">单价</th>';
					var total_quan
					for(var i in M){
						var W = M[i].warehouse;
						var html = '<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
						  +'<td class="text-center" name="pid">'+M[i].product_id+'</td>'
						  +'<td class="text-center" name="name">'+M[i].product_name+'</td>'
						  +'<td class="text-center" name="unit">'+M[i].unit_name+'</td>'
						  +'<td class="text-center" name="price">'+M[i].price+'</td>';
						for(var j in W){
							html += '<td class="text-center" name="quantity_'+W[j].warehouse_id+'">'+W[j].quantity+'</td>';
							html += '<td class="text-center" name="amount_'+W[j].warehouse_id+'">'+W[j].amount+'</td>';
							if(i==0){
								HHEAD +='<th class="text-center" name="'+W[j].warehouse_id+'" colspan="2" data-id="">'+W[j].warehouse_name+'</th>';
								HDET +='<th class="text-center" name="quantity_'+W[j].warehouse_id+'">数量</th><th class="text-center" name="amount'+W[j].warehouse_id+'">金额</th>';
							}
						}
						html  += '<td class="text-center" name="quantity_total">'+M[i].total_quantity+'</td><td class="text-center" name="amount_total">'+M[i].total_amount+'</td></tr>';
						$(html).appendTo('#stocklist');
						var pageInfo = data.Body.page;
					    if (typeof pageInfo !== 'undefined'){
					        refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listStock);
					    }
					}
					HHEAD += '<th class="text-center" name="total" colspan="2" data-id="">total</th>';
					HDET +='<th class="text-center" name="quantity_total">数量</th>	<th class="text-center" name="quantity_amount">金额</th>';
					$(HHEAD).appendTo('#hhead');
					$(HDET).appendTo('#hdet');
					
					var last_count = 0;
					var TOTAL_QUAN = 0;
					var TOTAL_AMOUNT =0;
					$('#stocklist tr').each(function(index, element){
						$(element).find('td').each(function(ii, ele){
							last_count = ii;
							if($(ele).attr('name') =='quantity_total'){
								var _quantity = $(ele).text();
								TOTAL_QUAN += +_quantity;
							}
							if($(ele).attr('name') =='amount_total'){
								var _amount = $(ele).text();
								TOTAL_AMOUNT += +_amount;
							}
						})

					})
					$('<tr><td class="text-center">total</td><td class="" colspan="'+(last_count - 2)+'">'
						   +'<td class="text-center">'+TOTAL_QUAN+'</td><td class="text-center">'+TOTAL_AMOUNT+'</td></tr>').appendTo('#stocklist');
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#stocklist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listStock);
				}
			} else {
				alertTips('error','网络错误');
				return false;
			}
		});
	}
	
	//查询
	$('#search_list').on('click',function(){
		var name = $('#pname').val();
		global_info.cond ={
			'pname': name,
		};
		global_info.page = 1;
		global_info.pageSize =10;
		listStock();
	});
});