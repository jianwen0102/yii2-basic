$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	
	global_info.cond ={
			'starTime': $('#starTime').val(),
			'endTime': $('#endTime').val(),
	};
	//出库单列表
	listStatistics();
	function listStatistics(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#statisticslist').find('tr').remove();
		$.post('get-transfer-statistics',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						var total_amount = 0;
						for(var i in W){
							total_amount +=(+W[i].quantity);
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center id">'+W[i].product_id+'</td>'
							+'<td class="text-center name">'+W[i].product_name+'</td>'
							+'<td class="text-center warehouse_out">'+W[i].warehouse_out_name+'</td>'
							+'<td class="text-center warehouse_in">'+W[i].warehouse_in_name+'</td>'
							+'<td class="text-center quantity">'+W[i].quantity+'</td>'
//							+'<td class="text-center">'+(+W[i].confirm_flag ? '<a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a>':'<a class="btn btn-success confirm" style="padding: 6px 8px;">确认</a>')
//							+'&nbsp&nbsp&nbsp'+(+W[i].confirm_flag ? '<a href="#" class="btn btn-success invalid" style="background-color:#de6d5b;padding: 6px 8px;">作废</a>':'<a href="#" class="btn btn-info delete" style="padding: 6px 8px;">删除</a>')
							+'</tr>').appendTo('#statisticslist');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listStatistics);
			                }
						};
						$('<tr><td colspan="5" >合计</td><td  class="text-center">'+total_amount+'</td></tr>').appendTo('#statisticslist');
					}else {
						$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#statisticslist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listStatistics);
					}
				} else {
					$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#statisticslist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listStatistics);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		global_info.page =1;
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'warehouse_out':$('#warehouse_out').val(),
			'warehouse_in' : $('#warehouse_in').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		
		listStatistics();
	});
	
	//导出
	$('#explode_search').on('click', function(){
		var flag = 0;
		$.get('check-transfer-statistics-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag =1;
				return;
			}
			global_info.cond ={
					'starTime': $('#starTime').val(),
					'endTime':$('#endTime').val(),
					'outware':$('#warehouse_out').val(),
					'inware':$('#warehouse_in').val(),
					'name' : $('#name').val(),
					'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
			};
			$url ='export-transfer-statistics?stime='+global_info.cond.starTime+'&etime='+global_info.cond.endTime
			      +'&outware='+global_info.cond.outware +'&inware='+global_info.cond.inware+'&id='+global_info.cond.id+'&name='+global_info.cond.name;
			if(!flag){
				window.location.href = $url;
			}
		})
	})
})