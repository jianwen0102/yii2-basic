$(function(){
	var _id = parseInt($_GET['id']);
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
	}
	var global_count = 1;
	var global_remove = [];
	var select_units;
	//时间
	dateSelFun($('#transfer_date'));
	
	/**
	 * @desc 单位//单位要根据product_id 取
	 * 
	 */
	(function(){
		select_units = '<select title="单位" class="form-control unit">'
			+ '<option value="0">单位</option>'
			+ '<option value="6">个</option>'
			+ '<option value="5">支</option>'
			+ '<option value="4">包</option>'
			+ '<option value="2">斤</option>'
			+ '<option value="1">箱</option>' + '</select>';
	})();
	
	if(_id){
		initTransfer();
	}
	/**
	 * @初始化页面
	 */
	function initTransfer() {
		$('#confirm').show();
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-transfer-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#transfer_no').val(H.transfer_no);
					$('#transfer_date').val(intToLocalDate(H.transfer_date,1));
					$('#warehouse_out').val(H.warehouse_out);
					$('#warehouse_in').val(H.warehouse_in);
					$('#transfer_man').val(H.transfer_man);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					$('#damage').val(H.damage);
					$('#transfer_type').val(H.transfer_type);
					if(+H.confirm_flag){
						$('#save').attr('style','display:none');
						$('#confirm').removeClass().addClass('btn btn-warning btn-sm pull-right').attr('disabled','disabled');
					}
					$('#transfer_amount').val(H.transfer_amount);//总金额
				}
				var D = data.Body.det;
				if(D){
					$('#transferList').find('tr').remove();
					for(var i in D){
						var tmp_units= '';
						var U = D[i].product_unit;
						tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
						for(var j in U){
							tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
						}
						tmp_units +='</select>';
						$('<tr>'
						   +'<td class="text-center" name="ids" data-id="'+D[i].transfer_det_id+'">'+(+i+1)+'</td>'
						   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].goods_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
						   +'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
						   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
						   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
						   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
						   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
						   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
						   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
						   +'<td class="text-center" name="unit_relation">'+D[i].unit_content+'</td>'
						   +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
						   +'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
						   +'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'">'+D[i].base_name+'</td>'
						   +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].base_quantity+'" disabled></td>'
						   +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="'+D[i].base_price+'" disabled></td>'
						   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
						   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
						   +'</tr>').appendTo('#transferList');
						$('#transferList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
						$('#transferList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
						global_count++;
					}
				}
			}
		});
	};
	
	
	/**
	 * @desc 选择商品
	 */
	$('#transfer_detail').on('click','.search-product',function(){
		$('#productName').val('');
		global_product.page = 1;
		global_product.name = undefined;
		getProducts();
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		global_product.page = 1;
		getProducts();
		return;
	})
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		global_product.ware = $('#warehouse_out').val();
		if(global_product.ware == 0){
			alertTips('error','请先选择调出仓库');
			return false;
		}
		
		$('#product_list').find('tr').remove();
		var index = layer.load();
		 $.ajax({
             url: '/sale/get-products-by-ware',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 if(data.Ack =='Success'){
	            	 var P = data.Body.list;
						if(P && P.length){
							for(var i in P){
								var tmp_units = ''
								var U = P[i].product_unit;
								tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
								for(var j in U){
									tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
								}
								tmp_units +='</select>';
								$('<tr>'
								+'<td class="text-center">'+(+i+1)+'</td>'
								+'<td class="text-center id">'+P[i].product_id+'</td>'
								+'<td class="text-center name">'+P[i].product_name+'</td>'
								+'<td class="text-center sn">'+P[i].product_sn+'</td>'
								+'<td class="text-center quantity">'+P[i].quantity+'</td>'
								+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'" data-rela="'+P[i].unit_content+'">'+(P[i].unit_name == null ? '':P[i].unit_name)+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="product_select" data-unit="'+encodeURI(tmp_units)+'">选择</a></td>'
								+'</tr>').appendTo('#product_list');
				              
							}
							var pageInfo = data.Body.page;
				            if (typeof pageInfo !== 'undefined'){
				              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
				            }
						}else {
							$('<tr><td colspan="7" class="text-center">没有数据</td></tr>').appendTo('#product_list');
							refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
						}
					$('#chooseProduct').modal('show');//选择商品窗口
	             } else{
	            	 
	             }
             }
		 })
	}
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var tmp_units = decodeURI($(this).attr('data-unit'));
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var sn = tr.find('.sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		if(!+unit){
			alertTips('warning','请在商品管理添加单位');
			return false;
		}
		var unit_name = tr.find('.unit').text();
		var unit_content = tr.find('.unit').attr('data-rela');
		if(global_count == 1){
			var pid = $('#transferList').find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				$('#transferList').find('tr:first').remove();
			} ;
		}
//		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="sn">'+sn+'</td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity"></td>'
		  +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price"></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount"></td>'
		  +'<td class="text-center" name="unit_relation" style="width:8%;">'+unit_content+'</td>'
		  +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0"></td>'
		  +'<td class="text-center" name="second_relation"></td>'
		  +'<td class="text-center" name="base_unit" data-id="'+unit+'">'+unit_name+'</td>'
		  +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0" disabled></td>'
		  +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo('#transferList');
		$('#transferList').find('tr:last').find('[name="unit"]').find('.units').val(unit);
		global_count++;
	})
	
	
	//删除数据
	$('#transferList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'transfer_det_id':tmp_id});
		}
		if(global_count == 1){
			return false;
		} else if(global_count == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#transferList').find('tr:first').remove();
			global_count--;
			insertOrginHtml();
			SumTransferAmount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#transferList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_count--;
                SumTransferAmount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_count--;
                SumTransferAmount();
                return false;
            }
		}
		
	});
	
	/**
	 * @var 计算增减明细后总金额
	 */
	var SumTransferAmount = function(){
		var total_amount = 0;
		var total_discount = 0;
		$('#transferList tr').each(function(index,element){
			var _amount = $(element).find('[name="amount"] input').val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
		})
		$('#transfer_amount').val(total_amount);
	};
	
	var insertOrginHtml = function(){
		$('<tr>'
			+'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
			+'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
			+'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
			+'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
			+'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
			+'<td class="text-center" name="sn"></td>'
			+'<td class="text-center" name="quantity"><input type="text"></td>'
			+'<td class="text-center" name="unit">'+select_units+'</td>'
			+'<td class="text-center" name="price"><input type="text"></td>'
			+'<td class="text-center" name="amount"><input type="text"></td>'
			+'<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_unit">'+select_units+'</td>'
			+'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_relation"></td>'
			+'<td class="text-center" name="base_unit"><input title="基本单位" type="text" value="0.00"></td>'
			+'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0.00" disabled></td>'
			+'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
			+'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
			+'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="">库存</a></td>'
			+'</tr>').appendTo('#transferList');
	};
	
	
	//库存提示
	$('#transferList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//名称提示
	$('#transferList').on('click','.product_name',function(){
		var name = $(this).val();
		if(name !=undefined){
			layer.tips(name, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//数量验证 计算价格金额
	$('#transferList').on('keyup','.quantity',function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _quantity = $(this).val();
		
		var tr = $(this).closest('tr');
		var _unit = tr.find('[name="unit"] select').val();
		var base_unit = tr.find('[name="base_unit"]').attr('data-id');
		var _rate = tr.find('[name="unit"]').find('option:selected').attr('data-rate');
		if(_unit == base_unit){
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		} else {
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		}
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = _quantity * _rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(_quantity*_rate/second_rate)+' '+second_name + ' '+(_quantity*_rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		
		var _price = $(this).closest('tr').find('.price').val();
		if(_price == ''){
			_price = 0
		}
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
		if(_quantity){
			tr.find('[name="base_price"] input').val((_amount / (_quantity * _rate)).toFixed(2));
		}
		SumAmount();
	});
	
	//单位验证
	$('#transferList').on('change','[name="unit"] select', function(){
		var unit = $(this).val();
		var rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		var quantity = tr.find('[name="quantity"] input').val();
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		tr.find('[name="base_quantity"] input').val(quantity * rate);
		var _amount = parseFloat(tr.find('[name="amount"] input').val());
		if(quantity){
			tr.find('[name="base_price"] input').val((_amount / (quantity * rate)).toFixed(2));
		}
		
		
	})
	
	//辅助单位
	$('#transferList').on('change','[name="second_unit"] select',function(){
		var second_unit = $(this).val();
		var  tr = $(this).closest('tr');
		if(+second_unit){
			var second_rate = $(this).find('option:selected').attr('data-rate');
			var quantity = tr.find('[name="quantity"] input').val();
			var rate = tr.find('[name="unit"]') .find('option:selected').attr('data-rate');
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		} else {
			tr.find('[name="second_quantity"] input').val('');
			tr.find('[name="second_relation"]').text('');
		}
		
	})
	
	//单位验证 计算价格金额
	$("#transferList").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').find('[name="quantity"] input').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _price = $(this).val();
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
		if(_quantity){
			var _rate = +$(this).closest('tr').find('[name="unit"]').find('option:selected').attr('data-rate');
			$(this).closest('tr').find('.base_price').val((_amount/(_quantity * _rate)).toFixed(2));
		}
		SumAmount();
	});
	//金额验证 计算价格金额
	$('#transferList').on('keyup','.amount', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var _quantity = $(this).closest('tr').find('.quantity').val();
		var tr = $(this).closest('tr');
		if(_quantity == ''){
			tr.find('.price').val(0);
		} else {
			var _amount = $(this).val();
			var rate = +tr.find('[name="unit"]').find('option:selected').attr('data-rate');
			var _price = (_amount / _quantity).toFixed(2);
			tr.find('.price').val(_price);
			tr.find('.base_price').val((_amount/(_quantity * rate)).toFixed(2));
		}
		SumAmount();
	});
	
	
	/**
	 * @var 计算总金额
	 */
	var SumAmount = function(){
		var total_amount = 0;
		$('#transferList tr').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			
		})
		$('#transfer_amount').val(total_amount);
	};
	
	$('#transfer_type').on('change',function(){
		var type = +$(this).val();
		if(type != 20){
			$('#damage').val(0);
		}
	});
	/**
	 * @var 保存单据
	 */
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = transfer_header();
			if(!head){
				return false;
			}
			//明细
			var detail = transfer_detail();
			if(!detail){
				return false;
			}
			var ii = layer.load();
			$.post('save-transfer',{'head':head,'det':detail,'remove': global_remove,'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
							if(_id){
								window.location.reload();
							} else {
								window.location.href = "list-transfer";
							}
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var transfer_header = function() {
		var no = $('#transfer_no').val();
		var transfer_date = $('#transfer_date').val();
		if (transfer_date.length == 0) {
			alertTips('warning', '请输入出库时间');
			return false;
		}
		var transfer_man = $('#transfer_man').val();
		if (!+transfer_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		
		var warehouse_out = $('#warehouse_out').val();
		if (!+warehouse_out) {
			alertTips('warning', '请选择调出仓库');
			return false;
		}
		var warehouse_in = $('#warehouse_in').val();
		if(!+warehouse_in){
			alertTips('warning','请选择调入仓库');
			return false;
		}
		
		if(warehouse_out == warehouse_in){
			alertTips('warning','调入仓库不能与调出仓库一样,请重新选择');
			return false;
		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var transfer_type = $('#transfer_type').val();
		if(!+transfer_type){
			alertTips('warning','请选择调拨类型！');
			return false;
		}
		
		var damage = +$('#damage').val();
		if(transfer_type == '20' && damage == 0){
			//报废单
			alertTips('warning','请选择报损原因！');
			return false;
		}
		
		
		var transfer_head = {
			'transfer_no' : no,
			'transfer_date' : transfer_date,
			'warehouse_out' : warehouse_out,
			'warehouse_in'  : warehouse_in,
			'transfer_man' : transfer_man,
			'create_man' : create_man,
			'remark' : remark,
			'transfer_amount':$('#transfer_amount').val(),
			'damage':damage,
			'transfer_type':transfer_type,
		};
		return transfer_head;
	}
	
	//盘盈单明细
	var transfer_detail = function(){
		var product_det=[];
		global_det = [];
		var product_info = [];
		var _tr = $('#transferList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var stock = tr.find('.checkStock').attr('data-quantity');
			if(!+stock){
				var name = tr.find('[name="name"] input').val();
				alertTips('warning',name+'是0库存，不能出库。');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			
			product_det = {
				'transfer_det_id' : line_id,
				'product_id' : tr.find('[name="name"]').attr('data-id'),
				'quantity' :quantity, 
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
				'discount' : tr.find('[name="discount"] input').val(),
				'second_unit':tr.find('[name="second_unit"] select').val(),
				'second_quantity':tr.find('[name="second_quantity"] input').val(),
				'second_relation':tr.find('[name="second_relation"]').text(),
				'base_unit':tr.find('[name="base_unit"]').attr('data-id'),
				'base_quantity':tr.find('[name="base_quantity"] input').val(),
				'base_price':tr.find('[name="base_price"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		
		return product_info;
	}
	
	
	//编辑页面的确认
	$('#confirm').on('click',function(){		
		//检查是否有确认其他入库功能
		$.get('check-confirm-transfer',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			layer.confirm('点击确认出库单将无法编辑，是否确认?', function(index) {
				$.get('confirm-transfer',{'id':_id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						} else if(data.Error == 'quantity overflow') {
							alertTips('error','调拨数量超出库存！');
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
})