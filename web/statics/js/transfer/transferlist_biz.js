$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//出库单列表
		listTransfer();
		function listTransfer(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#transferlist').find('tr').remove();
			$.get('get-transfers',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].transfer_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].transfer_id+'">'+W[i].transfer_no+'</td>'
								+'<td class="text-center transfer_date">'+intToLocalDate(W[i].transfer_date,1)+'</td>'
								+'<td class="text-center warehouse_out">'+W[i].warehouse_out_name+'</td>'
								+'<td class="text-center warehouse_in">'+W[i].warehouse_in_name+'</td>'
								+'<td class="text-center transfer_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].remark+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="transfer_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
								+(+W[i].confirm_flag ? '<td class="text-center" data-id="'+W[i].confirm_flag+'"><a href="javascript:void(0)" class="btn btn-warning btn-sm" style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm" data-id="'+W[i].confirm_flag+'"><a class="btn btn-success btn-sm">确认</a></td>')
//								+'<td class="text-center">'+(+W[i].confirm_flag ? '<a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a>':'<a class="btn btn-success confirm" style="padding: 6px 8px;">确认</a>')
//								+'&nbsp&nbsp&nbsp'+(+W[i].confirm_flag ? '<a href="#" class="btn btn-success invalid" style="background-color:#de6d5b;padding: 6px 8px;">作废</a>':'<a href="#" class="btn btn-info delete" style="padding: 6px 8px;">删除</a>')
								+'</tr>').appendTo('#transferlist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listTransfer);
				                }
							};
						}else {
							$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#transferlist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listTransfer);
						}
					} else {
						$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#transferlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listTransfer);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
		//查询
		$('#search_list').on('click',function(){
			var starTime = $('#starTime').val();
			var endTime = $('#endTime').val();
			global_info.page =1;
			if(starTime > endTime){
				alertTips('warning','开始时间不能大于截至时间！');
				return false;
			}
			global_info.cond ={
				'starTime': starTime,
				'endTime': endTime,
				'warehouse_out':$('#warehouse_out').val(),
			};
			
			listTransfer();
		});
		
		//编辑
		$('#transferlist').on('click','.transfer_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			//检查是否有编辑其他入库功能
			$.get('check-edit-transfer',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				
				window.location.href = "/transfer/edit-transfer?id="+ id;
			})
		})
		
			    //确认
	$('#transferlist').on('click','.confirm',function(){
		var tr = $(this).closest('tr');
		//检查是否有确认其他入库功能
		$.get('check-confirm-transfer',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			var id = tr.find('[name="ids"] input').attr('data-id');
			layer.confirm('点击确认调拨单将无法编辑，是否确认?', function(index) {
				$.get('confirm-transfer',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
		
		/**
		 * @desc 点击单号查询商品明细
		 * @author liaojianwen
		 * @date 2017-04-05
		 */
		
		var settings = {
			trigger:'hover',
			title:'商品信息 ',
			content:'<p>没有数据</p>',
			width:320,						
			multi:false,						
			closeable:false,
			style:'',
//			delay:300,
		    cache:true,
			padding:true,
			autoHide:10,
		};
		
		function initPopover(){					
			$('#transferlist').on('click','.show-pop-table',function(){
				var THIS =$(this);
				var iindex = layer.load();
				$.get('get-transfer-det',{'id':$(this).attr('data-id')},function(data,status){
					THIS.webuiPopover('destroy');
					if(status == 'success'){
						if(data.Ack =='Success'){
							$('#webuiList').empty();
							var M = data.Body;
							var _table ='';
							for(var i in M){
								 $('<tr><td data-id="'+M[i].transfer_id+'">'+(+i+1)+'</td>'
									 +'<td>'+M[i].product_id+'</td>'
									 +'<td>'+M[i].product_name+'</td>'
									 +'<td>'+M[i].product_sn+'</td>'
									 +'<td>'+M[i].unit_name+'</td>'
									 +'<td>'+M[i].quantity+'</td>'
									 +'<td>'+M[i].price+'</td>'
									 +'<td>'+M[i].amount+'</td>'
									 +'</tr>').appendTo('#webuiList');
							}
							
						}
						layer.close(iindex);
						
						var tableContent = $('#tableContent').html();
						tableSettings = {content:tableContent,
											width:800
										};
						THIS.webuiPopover($.extend({},settings,tableSettings));
					} else {
						alertTips('error','网络错误');
					}
				})
//				$(this).webuiPopover($.extend({},settings,tableSettings));
//				$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
			})
		}
		
		initPopover();
		
		
	/*删除调拨单*/
		//删除
		$('#delete').on('click',function(){
			//检查是否有删除功能
			$.get('check-del-transfer',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
				$('.ids').each(function(i){
					tmp_count +=i;
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
						
					}
				})
		
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框,选择要删除的调拨单！');
					return false;
				} 
				if(tmp_count == 0){
					if(+global_info.page > 1){
						global_info.page --;
					}
				}
				
				layer.confirm('是否删除调拨单？',function(index){
					$.get('del-transfer',{'ids':_all_ids}, function(data,status){
						if(status =='success' && data.Ack =='Success'){
							alertTips('success','删除成功！');
							setTimeout(function(){ 
								listTransfer();
								return false;
							},1500);
						}else {
							alertTips('error','删除失败！');
							return false;
						}
					})
					layer.close(index);
				})
			})
		})
		
//		/**
//		 * @desc 作废
//		 */
//		$('#transferlist').on('click', '.invalid', function(){
//			var tr = $(this).closest('tr');
//			var id = tr.find('.ids').attr('data-id');
//			//检查是否有确认功能
//			$.get('check-invalid-transfer',function(data,status){
//				if(data.Error =='User authentication fails'){
//					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
//					return;
//				}
//				layer.confirm('点击报废调拨算单将无法无效，是否继续?', function(index) {
//					$.get('invalid-transfer',{'id':id},function(data,status){
//						if(status =='success'){
//							if(data.Ack =='Success'){
//								alertTips('success','操作成功');
//								setTimeout(function(){
//									window.location.reload();
//								},1000);
//							}
//						} else{
//							alertTips('error','网络错误');
//						}
//					})
//					layer.close(index);
//				});
//			})
//		})
	
	dateSelFun($('#exp-star-time'));
	dateSelFun($('#exp-end-time'));
	//查询
	$('#searchPro').on('click', function(){
		var starTime = $('#exp-star-time').val();
		var endTime = $('#exp-end-time').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'name' : $('#good_name').val(),
		};
		global_info.page = 1;
		global_info.pageSize= 10;
		selectProMaterial();
	})
	
	$('#export_list').on('click', function(){
		$('#selectPro').modal('show');
	})
	
	function selectProMaterial(Pages){
		if (Pages !== undefined) {
			global_info.page = Pages.page;
			global_info.pageSize = Pages.pageSize;
		}
		$.get('get-export-transfer', global_info, function(data, status){
			$('#pro_detail').find('tr').remove();
			if(status =='success'){
				if(data.Ack =='Success'){
					var P = data.Body.list;
					
					if(P && P.length) {
						for(var i in P){
							$('<tr><td class="text-center" name="ids" data-id="'+P[i].transfer_id+'">'+(+i+1)+'</td>'
							    +'<td class="text-center">'+intToLocalDate(P[i].transfer_date,1)+'</td>'
							    +'<td class="text-center" data-id="'+P[i].product_id+'">'+P[i].product_name+'</td>'
							    +'<td class="text-center">'+P[i].unit_name+'</td>'
							    +'<td class="text-center">'+P[i].base_quantity+'</td>'
							    +'<td class="text-center">'+P[i].price+'</td>'
							    +'<td class="text-center">'+(P[i].base_quantity*P[i].price)+'</td>'
							    +'<td class="text-center">'+P[i].remark+'</td>'
							    +'</tr>').appendTo('#pro_detail');
						}
						var pageInfo = data.Body.page;
		                if (typeof pageInfo !== 'undefined'){
		                	refreshPaginationNavBar($('.paginationNavBar2'), pageInfo.page, pageInfo.pageSize, data.Body.count, selectProMaterial);
		                }
					} else {
						$('<tr><td colspan="11" class="text-center">没有数据</td></tr>').appendTo('#pro_detail');
						refreshPaginationNavBar($('.paginationNavBar2'), 1, 10, 1, selectProMaterial);
					}
				} else {
					$('<tr><td colspan="11" class="text-center">没有数据</td></tr>').appendTo('#pro_detail');
					refreshPaginationNavBar($('.paginationNavBar2'), 1, 10, 1, selectProMaterial);
				}
				
			} else {
				alertTips('error','网络错误!');
				return false;
			}
		});
	}
	
	/**
	 * @desc 导出execl
	 */
	$("#export_pro").on('click',function(){
		var _FLAG = 0;
		var _id  =0;
		var flag = 0;
		
		$.get('check-transfer-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag =1;
				return;
			}
			$('#pro_detail tr').each(function(index, element){
				_id = +$(element).find('[name="ids"]').attr('data-id');
				if(!_id){
					_FLAG = 1;
				}
			})
			if(_FLAG){
				alertTips('error','请先查询要导出的订单！');
				return false;
			}
			$url ='export-pro-info?name='+global_info.cond.name+'&stime='+global_info.cond.starTime+'&etime='+global_info.cond.endTime;
			if(!flag){
				window.location.href = $url;
			}
		})
		
	})
})