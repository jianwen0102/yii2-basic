/**
 * @desc 供应商列表
 * @author liaojianwen
 * @date 2016-11-10
 */
$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter': undefined //过滤条件
		};
	
	(function() {
		var info = getCookie('global_sup');
		if(info== ""){
		} else {
			global_info = JSON.parse(info);
		}
	})();
		//供应商列表
		listSupplier();
		function listSupplier(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#supplier_list').find('tr').remove();
			$.get('get-suppliers',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var S = data.Body.list;
						if(S && S.length){
							for(var i in S){
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+S[i].supplier_id+'"></td>'
								+'<td class="text-center name">'+S[i].supplier_name+'</td>'
								+'<td class="text-center desc">'+S[i].supplier_desc+'</td>'
								+'<td class="text-center act">'+S[i].region_str+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="supplier_edit fa fa-pencil-square-o fa-2x" title="编辑"></td>'
								+'</tr>').appendTo('#supplier_list');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSupplier);
				                }
							}
						}else {
							$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#supplier_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSupplier);
						}
					} else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#supplier_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSupplier);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		}
	//新增
	$('#add').on('click',function(){
		//检查是否有新增供应商权限
		$.get('check-add-supplier',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			location.href ="create-supplier";
		})
	});
	/**
	 * @desc 编辑
	 */
	$('#supplier_list').on('click', '.supplier_edit', function(){
		//检查是否有新增供应商权限
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		$.get('check-edit-supplier',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		

			if(global_info.page == undefined){
				global_info.page == 1;
			}
			setCookie('global_sup',JSON.stringify(global_info));
			location.href="supplier-edit?id="+id;
		})
	})
	
	//查询
	$('#searchClient').on('click',function(){
		global_info.cond ={
			'name':$('#cname').val(),
		};
		global_info.page =1;
		
		listSupplier();
	});
	
	
	//过滤
	$('#filterClient').on('change',function(){
		global_info.filter = $(this).val();
		global_info.page =1;
		
		listSupplier();
	})
	/**
	 * @desc  全选
	 */
	$('.checkAll').on('click',function(){
		if($(this).prop('checked')){
			$('.ids').prop('checked',true);
		} else {
			$('.ids').prop('checked',false);
		}
	});
	/**
	 * @desc 删除
	 */
	$('#delete').on('click',function(){
		//检查是否有删除供应商权限
		$.get('check-del-supplier',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var _all_ids='';
			$('.ids').each(function(i){
				if($(this).prop('checked')){
					_all_ids += $(this).attr('data-id') + ',';
				}
			})
			_all_ids = _all_ids.substr(0,_all_ids.length -1);
			if (_all_ids.length === 0) {
				alertTips('warning','请勾选复选框！');
				return false;
			}
			if(confirm("确定要删除供应商？")){
				$.get('del-supplier',{'ids':_all_ids},function(data,status){
					if(status ==='success' && data.Ack ==='Success'){
						alertTips('success','删除成功');
						setTimeout(function(){ 
							listSupplier();
						},1500);
					}else {
						alertTips('error','删除失败，请重新再试');
					}
				});
			 }
		})
	});
});