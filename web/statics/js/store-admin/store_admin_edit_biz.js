
var storeAdminEdit = {
    adminid: $("#adminid").val(),
	validateInfo: function(){ //验证
		$('#list_store_admin').validate({
			rules : {
				adminname : {
					required : true,
					remote: '/store-admin/check-user?id='+this.adminid,
				},
				email : {
					required : true,
					email : true,
					remote:'/store-admin/check-user?type=email&id='+this.adminid,
				},
				oldpassword : {
					minlength : 6
				},
				newpassword : {
					minlength : 6
				},
				repassword : {
					minlength : 6,
					equalTo : "#newpassword"
				},

			},
			messages : {
				adminname : {
					required : "请输入用户名",
					minlength : "用户名必需由两个字母组成",
					remote : "用户名已存在",
				},
				oldpassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				newpassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				repassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母",
					equalTo : "两次密码输入不一致"
				},
				email : {
					required : '请填写用户邮箱',
					email : '请填写正确的邮箱',
					remote : '该邮箱已被使用'
				},
			},
			submitHandler: function(form) { 
				storeAdminEdit.changeStoreAdmin();
			} 
		});
	},

	changeStoreAdmin: function(){//修改
		$info = {
			'adminname'   : $('#adminname').val(),
			'email'       : $('#email').val(),
			'oldpassword' : $('#oldpassword').val(),
			'newpassword' : $('#newpassword').val(),
			'repassword'  : $('#repassword').val(),
			'id'          : $('#adminid').val(),
			'delete_flag' : $("#delete_flag").val(),
			'store_id'    : $("#store_id").val()
		};
		
		$.post('/store-admin/change-user-pwd', $info, function(data, status) {
			if (status == 'success') {
				if (data.Ack == 'Success') {
					alertTips('success','更改成功');
					window.location.href= "/store-admin/list-store-admin";
					return false;
				} else {
					if (data.Error == 'oldpassword is not correct') {
						alertTips('error','旧密码不正确！');
						return false;
					} 
					alertTips('error','更改失败！');
					return false;
				}
			}
		});
	},

	
};