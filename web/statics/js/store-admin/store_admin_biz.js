$(function(){
   storeAdmin.storeAdminList();
});
var storeAdmin = {
	'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
	'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页总条数
	'searchData':{},//查询数据
	'filter': {}, //过滤条件
    storeAdminList: function(pageInfo){//获取供应商列表
	    if(pageInfo != undefined){
		   storeAdmin.page     = pageInfo.page;
		   storeAdmin.pageSize = pageInfo.pageSize;
		}
	    var data = {'page':storeAdmin.page, 'pageSize':storeAdmin.pageSize, 'searchData':storeAdmin.searchData, 'filter':storeAdmin.filter};
		$('#store_list').find('tr').remove();
		$.get('get-store-admin-list',data,function(res){
			if(res.Ack ==='Success'){
				var S = res.Body.list;
				if(S && S.length>0){
					for(var i in S){
						var delete_flag_status = S[i].delete_flag==0 ? '<font style="color:green">否</font>' : '<font style="color:red">是</font>';
						$('<tr id="tr_'+S[i].id+'"><td class="text-center"><input type="checkbox"  class="ids" data-id="'+S[i].id+'"></td>'
						+'<td class="text-center">'+S[i].username+'</td>'
						+'<td class="text-center">'+S[i].store_name+'</td>'
						+'<td class="text-center">'+S[i].created_at+'</td>'
						+'<td class="text-center">'+S[i].updated_at+'</td>'
						+'<td class="text-center delete_flag" status="'+S[i].delete_flag+'">'+delete_flag_status+'</td>'
						+'<td class="text-center"><a href="javascript:storeAdmin.editStoreAdminList(\''+S[i].id+'\');" class="supplier_edit fa fa-pencil-square-o fa-2x" title="编辑"/></td>'
						+'</tr>').appendTo('#store_list');
					}
					var pageInfo = res.Body.pageinfo;
				    if (typeof pageInfo !== 'undefined'){
						refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, res.Body.count, storeAdmin.storeAdminList);
					}
				}else {
					$('<tr><td colspan="5" class="text-center">没有数据了</td></tr>').appendTo('#store_list');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, storeAdmin.storeAdminList);
				}
			} else{
				alertTips('error','没有数据了');
			}
		});
	
	},

	addStoreAdminList: function(){//添加
       //检查是否有新增功能
		$.get('check-add-admin',function(storeList){
			//if(data.Error =='User authentication fails'){
			//	alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
			//	return false;
			//}
			if(storeList.length>0){
			   var htl = '<option value="0">请选择门店</option>';
			   for(var i in storeList){
				   htl += '<option value="'+storeList[i].store_id+'">'+storeList[i].store_name+'</option>';
			   }
			   $("#store_id").html(htl);
			}
			$('#myModal').modal('show');
		})
	},
	
	validateInfo: function(){ //验证
		$('#stroeAdminAdd').validate({
			rules : {
				adminname : {
					required : true,
					remote: '/store-admin/check-user',
				},
				email : {
					required : true,
					email : true,
					remote:'/store-admin/check-user?type=email',
				},
				password : {
					required : true,
					minlength : 6
				},
				repassword : {
					required : true,
					minlength : 6,
					equalTo : "#password"
				},

			},
			messages : {
				adminname : {
					required : "请输入用户名",
					minlength : "用户名必需由两个字母组成",
					remote : "用户名已存在",
				},
				password : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				repassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母",
					equalTo : "两次密码输入不一致"
				},
				email : {
					required : '请填写用户邮箱',
					email : '请填写正确的邮箱',
					remote : '该邮箱已被使用'
				},
			},
			submitHandler: function(form) { 
				storeAdmin.createUser();
			} 
		});
	},

	createUser: function(){//新增
		var info = {
				'adminname'   : $('#adminname').val(),
				'email'       : $('#email').val(),
				'store_id'    : $('#store_id').val(),
                'delete_flag' : $('#delete_flag').val(),
				'password'    : $('#password').val(),
				'repassword'  : $('#repassword').val(),
			};
		$.post('/store-admin/create-user', info, function(data, status) {
			if (status == 'success' ){
				if(data.Ack == 'Success') {
					alertTips('success','新增成功');
					window.location.reload();
				}else{
					alertTips('error','新增失败！');
					return false;
				}

			}
		});
	},

	delStoreList: function(){//删除
	  
	},

	editStoreAdminList: function(id){//编辑
		$.post('check-edit-admin',function(data){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			window.location.href = "/store-admin/store-admin-edit?id="+ id;
		});
	},

	changeFilter: function(){//过滤
	   storeAdmin.filter = {
	      'delete_flag': $("#filterClient").val()
	   };
	   storeAdmin.page = 1;
	   storeAdmin.storeAdminList();
	},

	selectAll: function(){//选中
	    if($('.checkAll').prop('checked')){
			$('.ids').prop('checked',true);
		} else {
			$('.ids').prop('checked',false);
		}
	},

	searchAdminList: function(){//查询门店
	    var store_name = $("#sname").val();
		storeAdmin.page = 1;
        storeAdmin.searchData = {
		   'store_name' : store_name
		};
		storeAdmin.storeAdminList();
	}
};