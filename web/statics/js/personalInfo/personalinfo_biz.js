$(function(){
	var _id = $('#adminname').data('id');
	$('#confirm_info').on('click',function(){
		validateInfo();
	})
	var validator;
	/**
	 * @desc 验证数据
	 */
	function validateInfo()
	{
		validator =$('#personalInfo').validate({
			rules : {
				adminname : {
					required : true,
					remote: '/admin/check-user?id='+_id,
				},
				email : {
					required : true,
					email : true,
					remote:'/admin/check-user?type=email&id='+_id,
				},
				oldpassword : {
					minlength : 6
				},
				newpassword : {
					minlength : 6
				},
				repassword : {
					minlength : 6,
					equalTo : "#newpassword"
				},

			},
			messages : {
				adminname : {
					required : "请输入用户名",
					minlength : "用户名必需由两个字母组成",
					remote : "用户名已存在",
				},
				oldpassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				newpassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				repassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母",
					equalTo : "两次密码输入不一致"
				},
				email : {
					required : '请填写用户邮箱',
					email : '请填写正确的邮箱',
					remote : '该邮箱已被使用'
				},
			},
			submitHandler: function(form) { 
				changeinfo();
			} 
		});
	}
	
	function changeinfo(){
		$info = {
			'adminname' : $('#adminname').val(),
			'email' : $('#email').val(),
			'oldpassword' : $('#oldpassword').val(),
			'newpassword' : $('#newpassword').val(),
			'repassword' : $('#repassword').val(),
			'id' : $('#adminname').data('id'),
		};
		

		$.post('change-pwd', $info, function(data, status) {
			if (status == 'success') {
				if (data.Ack == 'Success') {
					alertTips('success','更改成功');
					window.location.reload();
					return false;
				} else {
					if (data.Error == 'oldpassword is not correct') {
						alertTips('error','旧密码不正确！');
						return false;
					} else if(data.Error =='User authentication fails'){
						alertTips('error','你没有操作权限，如要操作，请向管理员申请');
						return false;
					}
					alertTips('error','更改失败！');
					return false;
				}
			}
		});
	}
	
	/**
	 * @desc 重置
	 */
	$('#reset_info').on('click',function(){
		window.location.reload();
		return false;
	});
}); 

