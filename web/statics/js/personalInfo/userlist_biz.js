$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined //页数
		}
		
		//管理员列表
		listUser();
		function listUser(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#user_list').find('tr').remove();
			$.get('get-users',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var U = data.Body.list;
						if(U && U.length){
							for(var i in U){
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+U[i].id+'"></td>'
								+'<td class="text-center name">'+U[i].username+'</td>'
								+'<td class="text-center email">'+U[i].email+'</td>'
								+'<td class="text-center desc">'+intToLocalDate(U[i].created_at,3)+'</td>'
								+'<td class="text-center act">'+intToLocalDate(U[i].last_in,3)+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="user_edit fa fa-pencil-square-o fa-2x" title="编辑"></a> &nbsp;&nbsp;<a href="javascript:void(0)" class="user_assign fa fa-cogs fa-2x" title="分配权限"></td>'
								+'</tr>').appendTo('#user_list');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listUser);
				                }
							}
						}else {
							$('<tr><td colspan="6" class="text-center">没有数据</td></tr>').appendTo('#user_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listUser);
						}
					} else {
						$('<tr><td colspan="6" class="text-center">没有数据</td></tr>').appendTo('#user_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listUser);
					}
				} else{
					alertTips('error','网络错误',1500);
				}
			});
		}
		
		//全选
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		//新增
		$('#add').on('click',function(){
			//检查是否有新增功能
			$.get('check-add-user',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return false;
				}
				$('#unit_name').val('');
				$('#description').val('');
				$('#active').prop('checked',false);
//				$('#user_save').attr('data-uid','');
				$('#myModal').modal('show');
			})
			
		})
	
	/**
	 * @desc 新增用户
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	$('#user_save').on('click',function(){
		validateInfo();
	});	
		
	/**
	 * @desc 验证数据
	 */
	function validateInfo()
	{
		$('#userinfo').validate({
			rules : {
				adminname : {
					required : true,
					remote: '/admin/check-user',
				},
				email : {
					required : true,
					email : true,
					remote:'/admin/check-user?type=email',
				},
				password : {
					required : true,
					minlength : 6
				},
				repassword : {
					required : true,
					minlength : 6,
					equalTo : "#password"
				},

			},
			messages : {
				adminname : {
					required : "请输入用户名",
					minlength : "用户名必需由两个字母组成",
					remote : "用户名已存在",
				},
				password : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母"
				},
				repassword : {
					required : "请输入密码",
					minlength : "密码长度不能小于 6 个字母",
					equalTo : "两次密码输入不一致"
				},
				email : {
					required : '请填写用户邮箱',
					email : '请填写正确的邮箱',
					remote : '该邮箱已被使用'
				},
			},
			submitHandler: function(form) { 
				createUser();
			} 
		});
	}
	
	/**
	 * @desc 新增管理员用户
	 */
	function createUser()
	{
		var info = {
				'adminname' : $('#adminname').val(),
				'email' : $('#email').val(),
				'password' : $('#password').val(),
				'repassword' : $('#repassword').val(),
			};
		$.post('create-user', info, function(data, status) {
			if (status == 'success' ){
				if(data.Ack == 'Success') {
					alertTips('success','新增成功');
					window.location.reload();
				}else{
					alertTips('error','新增失败！');
					return false;
				}

			}
		});
		
	}
	
	//编辑
	$('#user_list').on('click','.user_edit',function(data,status){
		//检查是否有编辑功能
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		$.get('check-edit-user',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			
			window.location.href = "/admin/user-info?id="+ id;
		})
		
	})
	
	
	//删除
	$('#delete').on('click',function(){
		//检查是否有删除功能
		$.get('check-del-user',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var _all_ids='';
			$('.ids').each(function(i){
				if($(this).prop('checked')){
					_all_ids += $(this).attr('data-id') + ',';
				}
			})
			_all_ids = _all_ids.substr(0,_all_ids.length -1);
			if (_all_ids.length === 0) {
				alertTips('warning','请勾选复选框！');
				return false;
			}
			if(confirm("确定要删除用户？")){
				$.get('del-user',{'ids':_all_ids},function(data,status){
					if(status ==='success' && data.Ack ==='Success'){
						alertTips('success','删除成功');
						setTimeout(function(){ 
							listUser();
						},1500);
					}else {
						alertTips('error','删除失败，请重新再试');
					}
				});
			 }
		})
	});
	
	/**
	 * @desc 获取权限分组
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	$('#user_list').on('click', '.user_assign',function(){
		//检查是否有角色分配功能
		var ii = layer.load();
		var id = $(this).closest('tr').find('.ids').attr('data-id');
		$.get('check-assign-user',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			$('#assign_save').attr('data-uid',id);
			$.get('get-auth-role', {'id':id}, function(data,status){
				layer.close(ii);
				$('#assign_body').empty();
				if(status =='success'){
					if(data.Ack =='Success'){
						var A = data.Body;
						var template = '';
						for(var i in A){
							template +='<div class="assign"><input type="checkbox" data-name="'+A[i].name+'"'+(A[i].checked?'checked="checked"':'')+'><label class="control-de">'+A[i].description+'</label></div>';
						}
						$(template).appendTo('#assign_body');
						$('#assignModal').modal('show');
					}
				} else {
					alertTips('error','网络错误');
				}
			})
		})
		
	})
	
	$('#assign_save').on('click',function(){
		var permision = [];
		$('.assign').each(function(index,element){
			var label = $(element).find('label').text();
			var check = $(element).find('input').prop('checked');
			var name = $(element).find('input').attr('data-name');
			permision.push([name,label,check]);
		});
		$.get('save-auth-assign',{'info':permision,'uid':$('#assign_save').attr('data-uid')},function(data,status){
			if(status ==='success' && data.Ack =='Success'){
				alertTips('success', '设置成功');
				setTimeout(function(){
					$('#assignModal').modal('hide');
				},1500);
			} else {
				alertTips('error', '网络错误,请重试!');
				return false;
			}
		});
	})
	
	
});