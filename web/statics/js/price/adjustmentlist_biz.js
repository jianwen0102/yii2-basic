$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//入库单列表
		listAdjustment();
		function listAdjustment(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#adjustmentlist').find('tr').remove();
			$.get('get-adjustments',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].adjustment_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].adjustment_id+'">'+W[i].adjustment_no+'</td>'
								+'<td class="text-center adjustment_date">'+intToLocalDate(W[i].adjustment_date,1)+'</td>'
								+'<td class="text-center handle_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].remark+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="adjustment_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
								+(+W[i].confirm_flag ? '<td class="text-center"><a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>')
//								+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
								+'</tr>').appendTo('#adjustmentlist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listAdjustment);
				                }
							}
						}else {
							$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#adjustmentlist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listAdjustment);
						}
					} else {
						$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#adjustmentlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listAdjustment);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
//			'vendor':$('#vendor').val(),
//			'warehouse':$('#stock').val(),
		};
		
		listAdjustment();
	});
	
	
	//编辑
	$('#adjustmentlist').on('click','.adjustment_edit',function(){
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有编辑其他入库功能
		$.get('check-edit-adjustment',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			
			
			window.location.href = "/price/edit-adjustment?id="+ id;
		})
	})
	
	//确认
	$('#adjustmentlist').on('click','.confirm',function(){
		var tr = $(this).closest('tr');
		//检查是否有确认其他入库功能
		$.get('check-confirm-adjustment',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			var id = tr.find('[name="ids"] input').attr('data-id');
			layer.confirm('点击确认入库单将无法编辑，是否确认?', function(index) {
				$.get('confirm-adjustment',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
	//过滤
	$('#filterInstore').on('change',function(){
		global_info.filter = $(this).val();
		listAdjustment();
	})
	
//	//删除
//	$('#delete').on('click',function(){
//		//检查是否有删除入库功能
//		$.get('check-del-adjustment',function(data,status){
//			if(data.Error =='User authentication fails'){
//				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
//				return;
//			}
//			var _all_ids='';
//			var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
//			$('.ids').each(function(i){
//				tmp_count +=i;
//				if($(this).prop('checked')){
//					_all_ids += $(this).attr('data-id') + ',';
//				}
//			})
//	
//			_all_ids = _all_ids.substr(0,_all_ids.length -1);
//			if (_all_ids.length === 0) {
//				alertTips('warning','请勾选复选框！');
//				return false;
//			} 
//			if(tmp_count == 0){
//				if(+global_info.page > 1){
//					global_info.page --;
//				}
//			}
//			layer.confirm('是否删除其他入库单？',function(index){
//				$.get('del-adjustment',{'ids':_all_ids}, function(data,status){
//					if(status =='success' && data.Ack =='Success'){
//						alertTips('success','删除成功！');
//						setTimeout(function(){ 
//							listAdjustment();
//							return false;
//						},1500);
//					} else {
//						alertTips('error','删除失败！');
//						return false;
//					}
//				})
//				layer.close(index);
//			})
//		})
//	})
	
	/**
	 * @desc 点击单号查询商品明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	
	var settings = {
		trigger:'hover',
		title:'商品信息 ',
		content:'<p>没有数据</p>',
		width:320,						
		multi:false,						
		closeable:false,
		style:'',
//		delay:300,
	    cache:true,
		padding:true,
		autoHide:10,
	};
	
	function initPopover(){					
		$('#adjustmentlist').on('click','.show-pop-table',function(){
			var THIS =$(this);
			var iindex = layer.load();
			$.get('get-adjustment-detail',{'id':$(this).attr('data-id')},function(data,status){
				THIS.webuiPopover('destroy');
				if(status == 'success'){
					if(data.Ack =='Success'){
						$('#webuiList').empty();
						var M = data.Body;
						var _table ='';
						for(var i in M){
							 $('<tr><td data-id="'+M[i].adjustment_id+'">'+(+i+1)+'</td>'
								 +'<td>'+M[i].product_name+'</td>'
								 +'<td>'+M[i].quantity+'</td>'
								 +'<td>'+M[i].unit_name+'</td>'
								 +'<td>'+M[i].price+'</td>'
								 +'<td>'+M[i].adjustment_price+'</td>'
								 +'<td>'+M[i].remark+'</td>'
								 +'</tr>').appendTo('#webuiList');
						}
						
					}
					layer.close(iindex);
					
					var tableContent = $('#tableContent').html();
					tableSettings = {content:tableContent,
										width:800
									};
					THIS.webuiPopover($.extend({},settings,tableSettings));
				} else {
					alertTips('error','网络错误');
				}
			})
//			$(this).webuiPopover($.extend({},settings,tableSettings));
//			$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
		})
	}
	
	initPopover();
})