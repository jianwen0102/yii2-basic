$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//入库单列表
		listAdjustment();
		function listAdjustment(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#adjustmentlist').find('tr').remove();
			$.get('get-adjustment-search',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].adjustment_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].adjustment_id+'">'+W[i].adjustment_no+'</td>'
								+'<td class="text-center adjustment_date">'+intToLocalDate(W[i].adjustment_date,1)+'</td>'
								+'<td class="text-center vendor">'+W[i].supplier_name+'</td>'
								+'<td class="text-center name">'+W[i].product_name+'</td>'
								+'<td class="text-center quantity">'+W[i].quantity+'</td>'
								+'<td class="text-center unit">'+W[i].unit_name+'</td>'
								+'<td class="text-center price">'+W[i].price+'</td>'
								+'<td class="text-center adjustment_price">'+W[i].adjustment_price+'</td>'
								+'<td class="text-center handle_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].det_remark+'</td>'
								+'<td class="text-center">'+(+W[i].confirm_flag ? '<a href="javascript:void(0)" class="" style="padding: 6px 8px;" disabled>已确认</a>':'<a class="" style="padding: 6px 8px;">未确认</a>')
//								+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
								+'</tr>').appendTo('#adjustmentlist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listAdjustment);
				                }
							}
						}else {
							$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#adjustmentlist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listAdjustment);
						}
					} else {
						$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#adjustmentlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listAdjustment);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
		//查询
		$('#search_list').on('click',function(){
			var starTime = $('#starTime').val();
			var endTime = $('#endTime').val();
			if(starTime > endTime){
				alertTips('warning','开始时间不能大于截至时间！');
				return false;
			}
			global_info.cond ={
				'starTime': starTime,
				'endTime': endTime,
				'vendor':$('#vendor').val(),
				'name':$('#product_name').val(),
			};
			
			listAdjustment();
		});
		
})