$(document).ready(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined //页数
		}
		
		//仓库列表
		listUnit();
		function listUnit(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#unit_list').find('tr').remove();
			$.get('get-units',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+W[i].unit_id+'"></td>'
								+'<td class="text-center name">'+W[i].unit_name+'</td>'
								+'<td class="text-center desc">'+W[i].description+'</td>'
								+'<td class="text-center act" data-act="'+W[i].is_active+'">'+(+W[i].is_active ? "生效":"未生效")+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="unit_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
								+'</tr>').appendTo('#unit_list');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listUnit);
				                }
							}
						}else {
							$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#unit_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listUnit);
						}
					} else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#unit_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listUnit);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		}
		//新增
		$('#add').on('click',function(){
			//检查是否有新增单位功能
			$.get('check-add-unit',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				$('#unit').val('');
				$('#description').val('');
				$('#active').prop('checked',false);
				$('#unit_save').attr('data-uid','');
				$('#myModal').modal('show');
			})
		});
		
		
		$('#unit_save').on('click',function(){
			$unit = $('#unit').val();
			if($unit.length == 0){
				alertTips('warning','请输入仓库名');
				return false;
			}
			var pflag = 0;
			$description = $('#description').val();
			$active = $('#active').prop('checked');
			$uid = $('#unit_save').attr('data-uid');
			var index = layer.load();
			$.get('save-unit',{'name':$unit,'desc':$description,'act':$active,'uid':$uid},function(data,status){
				layer.close(index);
				if(status ==='success' && data.Ack =='Success'){
					alertTips('success','保持成功');
					setTimeout(function(){ 
						$('#myModal').modal('hide');
						listUnit();
					},1000);
				} else if(data.Error =="unit is exists"){
					alertTips('warning','该单位已经存在！');
					return false;
				}else {
					alertTips('error','网络错误');
					return false;
				}
			})
		});
		//编辑
		$('#unit_list').on('click','.unit_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			var name = tr.find('.name').text();
			var desc = tr.find('.desc').text();
			var _active = tr.find('.act').data('act');
			//检查是否有编辑单位功能
			$.get('check-edit-unit',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}


				$('#unit_save').attr('data-uid',id);
				$('#unit').val(name);
				$('#description').val(desc);
				$('#active').prop('checked',_active);
				$('#myModal').modal('show');
			})
		});
		
		//全选
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		//删除
		$('#delete').on('click',function(){
			//检查是否有编辑其他出库功能
			$.get('check-del-unit',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
				$('.ids').each(function(i){
					tmp_count +=i;
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
					}
				})
		
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框！');
					return false;
				} 
				if(tmp_count == 0){
					if(+global_info.page > 1){
						global_info.page --;
					}
				}
				if(confirm("确定要删除单位？")){
					$.get('del-unit',{'ids':_all_ids},function(data,status){
						if(status ==='success' && data.Ack ==='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){ 
								listUnit();
							},1500);
						}else {
							alertTips('error','删除失败，请重新再试');
						}
					});
				 }
			})
		});
})