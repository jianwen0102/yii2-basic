$(function(){
   storeList.getStoreList();
});
var storeList = {
	'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
	'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页总条数
	'searchData':{},//查询数据
	'filter': {}, //过滤条件
    getStoreList: function(pageInfo){//获取供应商列表
	    if(pageInfo != undefined){
		   storeList.page     = pageInfo.page;
		   storeList.pageSize = pageInfo.pageSize;
		}
	    var data = {'page':storeList.page, 'pageSize':storeList.pageSize, 'searchData':storeList.searchData, 'filter':storeList.filter};
		$('#store_list').find('tr').remove();
		$.get('get-store-list',data,function(res){
			if(res.Ack ==='Success'){
				var S = res.Body.list;
				if(S && S.length>0){
					for(var i in S){
						var delete_flag_status = S[i].delete_flag==0 ? '<font style="color:green">正常</font>' : '<font style="color:red">屏蔽</font>';
						$('<tr id="tr_'+S[i].store_id+'"><td class="text-center"><input type="checkbox"  class="ids" data-id="'+S[i].store_id+'"></td>'
						+'<td class="text-center store_name">'+S[i].store_name+'</td>'
						+'<td class="text-center">'+S[i].create_time+'</td>'
						+'<td class="text-center delete_flag" status="'+S[i].delete_flag+'">'+delete_flag_status+'</td>'
						+'<td class="text-center">\
							<a href="javascript:storeList.editStoreList(\''+S[i].store_id+'\');" class="supplier_edit fa fa-pencil-square-o fa-2x" title="编辑"/>&nbsp;&nbsp;\
							<a href="store-qrcode?store_id='+S[i].store_id+'" target="_blank" title="门店二维码(充值和消费时验证会员身份)">二维码</a></td>'
						+'</tr>').appendTo('#store_list');
					}
					var pageInfo = res.Body.pageinfo;
				    if (typeof pageInfo !== 'undefined'){
						refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, res.Body.count, storeList.getStoreList);
					}
				}else {
					$('<tr><td colspan="5" class="text-center">没有数据了</td></tr>').appendTo('#store_list');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, storeList.getStoreList);
				}
			} else{
				alertTips('error','没有数据了');
			}
		});
	
	},

	addStoreList: function(){//添加
		$('#store_name').val('');
		$("#myModalLabel").html('门店新增');
		var delete_flag =0;
		$("#delete_flag option").each(function(index,element){
			var this_obj = $(element);              
			if(this_obj.val() == delete_flag){					
				this_obj.attr("selected",true);
			}
		});
	    $('#myModal').modal('show');	
	},

	delStoreList: function(){//删除
	  
	},

	editStoreList: function(store_id){//编辑
		var tr = $("#tr_"+store_id);
		var store_name  = tr.find('.store_name').text();
		var delete_flag = tr.find('.delete_flag').attr('status');
		$.post('check-edit-store',function(data){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
            $("#myModalLabel").html('门店修改');
			$('#store_name').val(store_name);
			$("#store_save").attr('data-id',store_id);

            var html = '<option value="-1">请选择</option>';
            switch(delete_flag){
			   case '0':
				   html += '<option value="0" selected>正常</option><option value="1">屏蔽</option>';
			       break;
			   case '1':
				   html += '<option value="0" >正常</option><option value="1" selected>屏蔽</option>';
			       break;
			}
            $("#delete_flag").html(html);
			$('#myModal').modal('show');
		});
	},

	saveStorList: function(_this){//保存
	    var store_name = $('#store_name').val();
		if(store_name.length == 0){
			alertTips('warning','请输入仓库名');
			return false;
		}
		var store_id  = $(_this).attr('data-id');
		var delete_flag = $('#delete_flag').val();
		
		loading();
		$.post('save-store',{'store_id':store_id,'store_name':store_name,'delete_flag':delete_flag},function(data){
			removeloading();
			if(data.Ack ==='Success'){
				alertTips('success','保存成功');
				setTimeout(function(){ 
					$('#myModal').modal('hide');
					window.location.reload();
				},1000);
			} else if(data.Error =="store_name is exists"){
				alertTips('warning','该门店已经存在');
				return false;
			}else {
				alertTips('error','系统繁忙...');
				return false;
			}
	   })	
	},

	changeFilter: function(){//过滤
	   storeList.filter = {
	      'delete_flag': $("#filterClient").val()
	   };
	   storeList.page = 1;
	   storeList.getStoreList();
	},

	selectAll: function(){//选中
	    if($('.checkAll').prop('checked')){
			$('.ids').prop('checked',true);
		} else {
			$('.ids').prop('checked',false);
		}
	},

	searchStoreList: function(){//查询门店
	    var store_name = $("#sname").val();
		storeList.page = 1;
        storeList.searchData = {
		   'store_name' : store_name
		};
		storeList.getStoreList();
	}
};