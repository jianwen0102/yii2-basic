"use strict";
/**
 * @desc 日期选择方法
 * @param obj(点击的对象)
 * @author linPeiYan
 * @date 2015-04-14
 */
function dateSelFun(obj) {
    obj.datetimepicker({
        format: "yyyy-mm-dd",
        weekStart: 1,
        minView: 2,
        autoclose: true,
        todayBtn: true,
        todayHighlight: true,
        forceParse: true,
        language: 'zh-CN'
    });
}

/**
 * @desc 信息弹框
 * @param type //success,warning,error,info
 * @param message
 * @author liaojianwen
 * @date 2016-11-22
 */
function alertTips(type,message)
{
	if(type =='success'){
		layer.alert(message, {icon: 1});
	} else if(type == 'warning'){
		layer.alert(message, {icon: 0});
	} else if(type =='error'){
		layer.alert(message, {icon: 2});
	} else {
		layer.msg(message);
	}
}
/**
         * @method 信息弹窗
         * @param [string] {type} 提示框类型{'info': 一般信息,'warning': 警告,'error': 错误,'success': 成功提示}
         * @param [string] {msg} 信息内容
         * @param [mixed] {delay} 延时{'fast': 1500,'normal': 2500,'slow': 5000,可自定义}，为0时不自动消失[默认normal]
         * @param [mixed] {margintop} 提示框展示位置{'top': 0%, 'center': 35%,可自定义}
         * @author Weixun Luo
         * @date 2014-11-10
         */
        var showTips = function(type, msg, delay, margintop){
            showTipsX({type: type, msg: msg, delay: delay, margintop: margintop});
        };
        
        /**
         * @method 信息弹窗(加强版)
         * @param [array] {params} 参数
         *  {
         *      type:       提示框类型{'info': 一般信息,'warning': 警告,'error': 错误,'success': 成功提示}
         *      title:      自定义标题
         *      msg:        信息内容
         *      delay:      延时{'fast': 1500,'normal': 2500,'slow': 5000,'stay': 0,可自定义}，为0时不自动消失[默认normal]
         *      margintop:  提示框展示位置{'top': '0%', 'center': '35%',可自定义}
         *      confirm:    是否显示"确认"按钮{true: 显示, false: 不显示}[默认：false]
         *      callback:   确认回调函数(此时默认显示"确认"按钮)
         *      extend:     扩展内容，自定义扩展html内容
         *  }
         * @author Weixun Luo
         * @date 2014-11-10
         */
        var showTipsX = function(params){
            var $shTipsModal = $('#shTipsModal');
            if($shTipsModal.length > 0){
                // 默认info提示
                if(params.type){
                    params.type = params.type.toLowerCase();
                } else {
                    params.type = 'info';
                }

                var title = '';
                var theme = {alertCSS: '', btnCSS: ''};
                switch(params.type){
                    case 'info': title = '信息'; theme = {alertCSS: 'alert-info', btnCSS: 'btn-info'}; break;
                    case 'warning': title = '警告'; theme = {alertCSS: 'alert-warning', btnCSS: 'btn-warning'}; break;
                    case 'error': title = '错误'; theme = {alertCSS: 'alert-danger', btnCSS: 'btn-danger'}; break;
                    case 'success': title = '成功'; theme = {alertCSS: 'alert-success', btnCSS: 'btn-success'}; break;
                    default: title = '信息'; theme = {alertCSS: 'alert-info', btnCSS: 'btn-info'}; break;
                }

                // tips model reset function
                var resetTipsModal = function(ms){
                    $shTipsModal.animate({opacity: 0}, ms, function(){
                        $shTipsModal.removeClass('alert-info alert-success alert-danger alert-warning circle').css({top: '-25%', display: 'none'});
                        $shTipsModal.children('[name=tipsTitle]').text('');
                        $shTipsModal.children('[name=msgBox]').text('');
                        $shTipsModal.children('[name=msgConfirmBtn]').removeClass('btn-info btn-success btn-danger btn-warning').css({'display': 'none'});
                        $shTipsModal.children('[name=msgExtend]').html('');
                    });
                }

                // tips delay hide function
                var delayHide = function(delay){
                    setTimeout(function(){
                        var now = $.now();
                        var hideTime = $shTipsModal.children('[name=hideTime]').val();
                        if(now >= hideTime){
                            resetTipsModal(350);
                        } else{
                            delayHide(hideTime-now);
                        }
                    }, delay);
                }

                // Rest tips modal
                resetTipsModal(0);
                // 如果没有自定义title，怎用系统默认title
                if(!params.title){
                    params.title = title;
                }

                // 提示框停留位置，默认top
                var topBorderRadius = '';
                if(params.margintop === undefined){params.margintop = 'top';}
                switch(params.margintop){
                    case 'top': params.margintop = '0%'; break;
                    case 'center': params.margintop = '35%'; topBorderRadius = 'circle'; break;
                }
                // 提示框停留时间，默认normal
                if(params.delay === undefined){params.delay = 'normal';}
                switch(params.delay){
                    case 'normal': params.delay = 3300; break;
                    case 'fast': params.delay = 1500; break;
                    case 'slow': params.delay = 5000; break;
                    case 'stay': params.delay = 0; break;
                    default:
                        var ms = parseInt(params.delay);
                        if(isNaN(ms)) return false;
                        params.delay = ms;
                        break;
                }

                // 不渐隐而没有设置confirm或设置了回调函数时,默认confirm为true
                if((params.confirm === undefined && params.delay === 0) || typeof(params.callback) === 'function'){
                    params.confirm = true;
                }

                // 添加标题
                $shTipsModal.children('[name=tipsTitle]').text(params.title);
                // 添加提示信息
                if(params.msg){
                    $shTipsModal.children('[name=msgBox]').text(params.msg);
                }
                // 设置"确认"按钮样式和点击事件
                var $msgConfirmBtn = $shTipsModal.children('[name=msgConfirmBtn]');
                $msgConfirmBtn.addClass(theme.btnCSS).css({'display': params.confirm ? 'block' : 'none'});
                $msgConfirmBtn.off('click');
                if(typeof(params.callback) === 'function'){$msgConfirmBtn.on('click', function(event){params.callback();});}
                // 设置扩展内容
                $shTipsModal.children('[name=msgExtend]').css({'display': params.extend ? 'block' : 'none'}).html(params.extend ? params.extend : '');
                // 设置样式,弹出
                $shTipsModal.addClass(theme.alertCSS+' '+topBorderRadius).css({display: 'block'}).animate({opacity: 1, top: params.margintop}, 350);
                if(params.delay != 0){
                    $shTipsModal.children('[name=hideTime]').val($.now()+params.delay);
                    // 设置自动渐隐
                    delayHide(params.delay);
                }
                return true;
            }
            // Tips框模板未加载
            $.get('public/template/_partials/component/tips.html?_201503251138',
                function(component){
                    $('body').append(component);
                    return showTipsX(params);
                });
        };



/**
 * @descr 提示层类型显示
 * @param hintType('字符串') 提示类型  hint_s(操作成功); hint_f(操作失败); hint_w(警告)
 * @param hintText 提示文本
 * @author linPeiYan
 * @date 2015-02-12
 */
$(parent.document).find('.hint').remove();
var hintV = { //提示框判断值 
    'hintOn': false,
    'timeShow': ''
}

function hintShow(hintType, hintText) {
    hintV.hintOn = true;
    var $parDoc = $(parent.document);
    var $newE = $('<div class="hint"><span></span><div class="hintClose">×</div></div>');
    clearTimeout(hintV.timeShow);
    $parDoc.find('.hint').remove();
    $parDoc.find('body').append($newE);
    $newE.addClass(hintType);
    $parDoc.find('.hint span').html(hintText);
    $newE.animate({
        top: 0
    }, 300, function() {
        if (hintV.hintOn) {
            hintV.hintOn = false;
            autoHide();
        }
    });
}
//自动收起提示框（3秒）

function autoHide() {
    hintV.timeShow = setTimeout(function() {
        hintHide();
    }, 3000);
}
//手动收起提示框

function hintHide() {
    $(parent.document).find('.hint').animate({
        top: -100
    }, 300, function() {
        $(this).remove();
    });
}
//点击提示层关闭按钮
$(parent.document).find('body').on('click', '.hint .hintClose', function() {
    //clearTimeout(hintV.timeShow);
    hintHide();
});

/**
 * @descr 状态提示框  stateBoxFun()
 * @param type('字符串') 提示类型  show(显示); hide(移除); 注：如果类型是hide,那么状态提示框会在半秒内自动消失。
 * @param text('字符串') 提示文字
 * @author linPeiYan
 * @date 2015-07-27
 * 如：stateBoxFun('show','请稍等，正在处理中...');
 *     stateBoxFun('hide','已处理完毕');
 */
function stateBoxFun(type, text) {
    var $newE = $('<div class="TCGB stateGB" style="display:block;"><p class="stateBox">' + text + '</p></div>')
    $('body .stateGB').remove();
    $('body').append($newE);
    if (type == 'hide') {
        setTimeout(function() {
            $('body .stateGB').fadeOut('slow', function() {
                $('body .stateGB').remove();
            });
        }, 500)
    }
}

/**
 * @descr 模拟下拉框
 * @author linPeiYan
 * @date 2015-03-19
 */
$('.comboBox').hover(function() {
    $(this).find('.selList').show();
}, function() {
    $(this).find('.selList').hide();
})

/**
 * @descr 模拟确认框
 * @param confirmFun  1.fun1(确定--类型为：function); 2.fun2(取消--类型为：function); 3.确认框文本 类型：字符串;
 * @author linPeiYan
 * @date 2015-03-9
 */
function confirmFun(fun1, fun2, warnText) {
    var conBox = $('<div class="TCGB TC400 confirmBox" style="display:block;">' +
        '<div class="TCBox">' +
        '<h2>' + '警告' + '：<span class="TCClose"><i class="icon-remove" id="close_btn"></i></span></h2>' +
        '<div class="TCBody">' +
        '<p class="warnText">' + warnText + '</p>' +
        '<p><button class="subBtn" id="confirmBtn">' + '确定' + '</button> <button class="subBtn" id="cancelBtn">' + '取消' + '</button>' +
        '</p>' +
        '</div>' +
        '</div>' +
        '</div>');
    $('body').append(conBox);
    $('#confirmBtn').off('click').on('click', function() { //当点击确认按钮时执行fun1
        $('.confirmBox').remove();
        fun1();
    });
    $('#cancelBtn').off('click').on('click', function() { //当点击取消按钮时执行fun2
        $('.confirmBox').remove();
        fun2();
    })
    $('#close_btn').off('click').on('click', function() { //当点击关闭按钮时return
        $('.confirmBox').remove();
        return;
    });
}


/**
 * @desc 输入框默认文字
 * @param obj-表单对象
 * @param text-str-默认显示文本
 * @author linpeiyan
 * @date 2015-09-15
 */
function inputFocusFun(obj, text) {
    obj.css('color', '#999').html(text);
    obj.next('input[type="text"]').on('focus', function() {
        if (obj.html() == text) {
            obj.html('');
        }
    }).on('keydown', function() {
        obj.css('color', '#000');
    }).on('blur', function() {
        if (obj.next('input[type="text"]').val() == '') {
            obj.css('color', '#999').html(text);
        }
    })
};

/**
 * @desc 鼠标经过弹出气泡提示框
 * @param text-obj-节点对象(JQ)
 * @author linpeiyan
 * @date 2015-10-20
 * @例如：tooltip($('td'))   tooltip($('.abc'));
 */
function tooltip(obj) {
    var $tooltipNode = $('<div id="tooltip-y"><div id="tooltip-y-top" class="tooltip-arrows"></div><span></span></div>');
    $('#tooltip-y').remove();
    $('body').append($tooltipNode);
    var $tooltip = $('#tooltip-y');
    $.each(obj, function(index, item) {
        var $item = $(item);
        $item.data('title', $item.attr('title'));
        if ($item.data('title')) {
            $(item).hover(function(e) {
                var objY = $item.offset().top - $(window).scrollTop(),
                    objX = $item.offset().left,
                    nodeHeight = $item.outerHeight(),
                    nodeWidth = $item.outerWidth();
                $tooltip.css('marginTop', nodeHeight).fadeIn(0);
                $tooltipNode.find('span').html('').html($item.data('title'));
                $tooltipNode.css({
                    'top': objY,
                    'left': objX
                });
                var tooltipLeft = $tooltip.position().left + $tooltip.outerWidth(),
                    tooltipTop = $tooltip.offset().top + $tooltip.outerHeight(),
                    bodyWidth = $('body').outerWidth(),
                    bodyHeight = $(window).height();
                if (tooltipLeft > bodyWidth) {
                    var tooltipMarginLeft = tooltipLeft - bodyWidth;
                    $tooltip.css('marginLeft', -tooltipMarginLeft);
                } else {
                    $tooltip.css('marginLeft', 0);
                }
                var mouseX = objX - $tooltip.offset().left + ($item.outerWidth() / 2) - 6;
                $tooltip.find('.tooltip-arrows').stop().animate({
                    'left': mouseX
                }, 200);
                if (tooltipTop > bodyHeight) {
                    var tooltipMarginTop = tooltipTop - bodyHeight;
                    $tooltip.css('marginTop', -$tooltip.outerHeight());
                    $tooltip.find('.tooltip-arrows').attr('id', 'tooltip-y-bottom').css('top', $tooltip.outerHeight() - 2);
                } else {
                    $tooltip.css('marginTop', nodeHeight);
                    $tooltip.find('.tooltip-arrows').attr('id', 'tooltip-y-top');
                    $tooltip.find('.tooltip-arrows').attr('id', 'tooltip-y-top').css('top', -12);
                }
                $item.attr('title', '');
            }, function() {
                $tooltip.fadeOut(0);
            });
        }
    })
    $('body').on('mouseover', '#tooltip-y', function() {
        $tooltip.fadeIn(0);
    });
    $('body').on('mouseout', '#tooltip-y', function() {
        $tooltip.fadeOut(0);
    })
}


/**
 * @desc show loading
 * @param id
 * @author YangLong
 * @return string ''
 */
function loading(id) {
    $("body").append('<div id="loading' + (id ? id : '') + '" class="loading" style="display:none;"></div>');
    $("#loading" + (id ? id : '')).click(function(e) {
        $(this).remove();
    });
    setTimeout(function() {
        $("#loading" + (id ? id : '')).fadeIn("fast");
    }, 200);
    return '';
}

/**
 * @desc hide loading
 * @param id
 * @author YangLong
 * @return void
 */
function removeloading(id) {
    //try{
    $("#loading" + (id ? id : '')).remove();
    //}catch(err){}
}

/*下拉框模拟*/
$('.comboBox li').on('click', function() {
    $(this).parents('.comboBox').find('.defaultOp').text($(this).find('span').text());
});

// 全选/取消全选功能
$("#checkall").click(function(e) {
    if ($(this)[0].checked) {
        $(".ids").each(function(index, element) {
            $(element)[0].checked = true;
        });
    } else {
        $(".ids").each(function(index, element) {
            $(element)[0].checked = false;
        });
    }
});

/**
 * @desc 获取URL参数值，类似php的$_GET
 * @author YangLong
 * @date 2015-02-01
 */
var $_GET = (function() {
    var url = window.document.location.href.toString();
    var u = url.split("?");
    if (typeof(u[1]) == "string") {
        u = u[1].split("&");
        var get = {};
        for (var i in u) {
            var j = u[i].split("=");
            get[j[0]] = j[1];
        }
        return get;
    } else {
        return {};
    }
})();

/**
 * @desc 复选框全选
 * @author liaojianwen
 * @date 2015-03-02
 * @param object obj 事件操作对象（触发者如：全选框）
 * @param object obj1 事件操作对象（被触发者如：明细的复选框）
 */
//【封装】复选框全选-start
function allCheckBox(obj, obj1) {
    obj.click(function(e) {
        if ($(this)[0].checked) {
            obj1.each(function(index, element) {
                $(element)[0].checked = true;
            });
        } else {
            obj1.each(function(index, element) {
                $(element)[0].checked = false;
            });
        }
    });
}

//【封装】获取被选中的复选框当前行的ID封装--start
/**
 * @desc 获取勾选的复选框的id
 * @author liaojianwen
 * @date 2015-03-02
 * @param object obj 被勾选的复选框
 * @return  返回被勾选的复选框的明细的id;例如：1,2,3,4,5
 */
function getAllId(obj) { //参数为被勾选的复选框，即带有checked属性的复选框
    var str_allId = '';
    var $allCheckBox = obj; //例：$('.address-label table tbody tr input[type=checkbox]:checked');
    if ($allCheckBox.length == 0) {
        return '';
    }
    for (var i = 0; i < $allCheckBox.length; i++) {
    	var value  = $allCheckBox.eq(i).attr('data-id');
    	if(value.length === 0){
    		continue;
    	} else {
    		 str_allId += value + ','
    	}
       
    }
    var nStr_allId = str_allId.substr(0, str_allId.length - 1);
    return nStr_allId;
}

/**
 * @desc 刷新分页导航条
 * @param [object] {$paginationNavBar} 需要刷新的导航条对象
 * @param [int] {page} 当前页码
 * @param [int] {pageSize} 当前分页大小
 * @param [int] {total} 查询总记录数
 * @param [function] {eventHandler} 分页导航条的按钮点击时要调用的处理函数
 */
var refreshPaginationNavBar = function($paginationNavBar, page, pageSize, total, eventHandler){
	var ptype = $paginationNavBar.data('ptype');
	if(total === undefined || total === 0){
		$paginationNavBar.empty();return;
	}

	var frameSize = 5; // 定义分页导航条最多显示5页的按钮
	var pageSizeArray = [5,10,15,20,30,50,100,200]; // 定义页面大小选项
	var pageCount =  Math.ceil(total / pageSize); // 总页数
	var currentPage = page; // 当前页码
	var frameFirst = (Math.ceil(currentPage / frameSize) - 1) * frameSize + 1; // 当前frame的第一页
	var frameLast = frameFirst + frameSize - 1;  // 当前frame的最后一页
		frameLast = frameLast > pageCount ? pageCount : frameLast;  // 当前frame最后一页大于总页数，溢出
	var template = '';
	if(currentPage == 1){
		// 在第一页，禁用"首页"按钮,禁用"第一页"按钮
		template += '<ul class="pagination ' + (ptype ? ptype : '') + '"><li class="disabled"><a>首页</a></li>'
			+ '<li class="disabled"><a>&lt;</a></li>';
	} else{
		// 不在第一页，"首页" = "第一页","<" = 当前页的前一页
		template += '<ul class="pagination ' + (ptype ? ptype : '') + '"><li><a data-page="1" data-pagesize="' + pageSize + '">首页</a></li>'
			+ '<li><a title="前1页" data-page="' + (+currentPage - 1) +'" data-pagesize="' + pageSize + '">&lt;</a></li>';
	}
	if (frameFirst != 1){
		// 不在最前frame
		template += '<li><a title="前' + frameSize + '页" data-page="'
			+ (frameFirst - 1) + '" data-pagesize="' + pageSize + '">&hellip;</a></li>';
	}
	for (var pageNumber = frameFirst; pageNumber <= frameLast; pageNumber++) {
		// 页码按钮
		template += '<li ' + (currentPage == pageNumber ? 'class="active"' : '')
			+ '><a data-page="' + pageNumber + '" data-pagesize="' + pageSize + '">' + pageNumber + '</a></li>';
	};
	if (frameLast != pageCount){
		// 不在最后frame
		template += '<li><a title="后' + frameSize + '页" data-page="'
			+ (+frameLast + 1) + '" data-pagesize="' + pageSize + '">&hellip;</a></li>';
	}
	if(currentPage >= pageCount){
		// 到了最后一页，没有下一页
		template += '<li class="disabled"><a>&gt;</a></li>'
			+ '<li class="disabled"><a>尾页</a></li>';
	} else {
		// 还未到最后一页，">" = 当前页的下一页
		template += '<li><a title="后1页" data-page="' + (+currentPage + 1) +'" data-pagesize="' + pageSize + '">&gt;</a></li>'
			+ '<li><a data-page="' + pageCount +'" data-pagesize="' + pageSize + '">尾页</a></li>';
	}
	template += '<li class="select-size"> 每页 <select data-pagesize data-page class="select-page">';
	$.each(pageSizeArray, function(index, aSize){
		template += '<option ' + (aSize == pageSize ? 'selected="selected"' : '') + 'value="' + aSize + '">' + aSize + '</option>';
	});
	template += '</select> 条，共 ';
	template += '<strong>' + pageCount + '</strong> 页 ';
	template += '<strong>' + total + '</strong> 条</li>';
	$paginationNavBar.html(template).show();

	// 绑定按钮提示信息
	// $paginationNavBar.find('[data-toggle=tooltip][data-page][data-pagesize]').tooltip();

	// 页码按钮绑定事件
	$('a[data-page][data-pagesize]').on('click', function(event) {
		var self = $(this);
		var selectedPage = numberClean(self.data('page'));
		var selectedPageSize = numberClean(self.data('pagesize'));
		if(eventHandler != undefined && (typeof eventHandler === 'function')){
			eventHandler({page: selectedPage, pageSize: selectedPageSize});
		}
	});

	// 分页Size按钮绑定事件
	$('select[data-page][data-pagesize]').on('change', function(event) {
		var selectedPage = undefined;
		var selectedPageSize = numberClean($(this).children('option:selected').val());
		if(eventHandler != undefined && (typeof eventHandler === 'function')){
			eventHandler({page: selectedPage, pageSize: selectedPageSize});
		}
	});
};/* <--刷新分页导航条 */

//js过滤用户提交的数字数据
/*function numberClean(source, defaultValue) {
    if (typeof(defaultValue) === 'undefined') {
        // 默认值
        defaultValue = 0;
    }
    var result;
    if (source && source != undefined) {
        if (isNaN(source)) {
            result = source.replace(/[\D]/g, '');
        } else {
            result = source;
        }
    } else {
        result = defaultValue;
    }
    return result;
}*/

/**
 * @desc 刷新分页导航条
 * @param [object] {$paginationNavBar} 需要刷新的导航条对象
 * @param [int] {page} 当前页码
 * @param [int] {pageSize} 当前分页大小
 * @param [int] {total} 查询总记录数
 * @param [function] {eventHandler} 分页导航条的按钮点击时要调用的处理函数
 */
var refreshPaginationNavBar2 = function($paginationNavBar, page, pageSize, total, eventHandler){
	var ptype = $paginationNavBar.data('ptype');
	if(total === undefined || total === 0){
		$paginationNavBar.empty();return;
	}

	var frameSize = 5; // 定义分页导航条最多显示5页的按钮
	var pageSizeArray = [5,10,15,20,30,50,100,200]; // 定义页面大小选项
	var pageCount =  Math.ceil(total / pageSize); // 总页数
	var currentPage = page; // 当前页码
	var frameFirst = (Math.ceil(currentPage / frameSize) - 1) * frameSize + 1; // 当前frame的第一页
	var frameLast = frameFirst + frameSize - 1;  // 当前frame的最后一页
		frameLast = frameLast > pageCount ? pageCount : frameLast;  // 当前frame最后一页大于总页数，溢出
	var template = '';
	if(currentPage == 1){
		// 在第一页，禁用"首页"按钮,禁用"第一页"按钮
		template += '<ul class="pagination ' + (ptype ? ptype : '') + '"><li class="disabled"><a>首页</a></li>'
			+ '<li class="disabled"><a>&lt;</a></li>';
	} else{
		// 不在第一页，"首页" = "第一页","<" = 当前页的前一页
		template += '<ul class="pagination ' + (ptype ? ptype : '') + '"><li><a data-page="1" data-pagesize="' + pageSize + '">首页</a></li>'
			+ '<li><a title="前1页" data-page="' + (+currentPage - 1) +'" data-pagesize="' + pageSize + '">&lt;</a></li>';
	}
//	if (frameFirst != 1){
//		// 不在最前frame
//		template += '<li><a title="前' + frameSize + '页" data-page="'
//			+ (frameFirst - 1) + '" data-pagesize="' + pageSize + '">&hellip;</a></li>';
//	}
	for (var pageNumber = frameFirst; pageNumber <= frameLast; pageNumber++) {
		// 页码按钮
		template += '<li ' + (currentPage == pageNumber ? 'class="active"' : '')
			+ '><a data-page="' + pageNumber + '" data-pagesize="' + pageSize + '">' + pageNumber + '</a></li>';
	};
//	if (frameLast != pageCount){
//		// 不在最后frame
//		template += '<li><a title="后' + frameSize + '页" data-page="'
//			+ (+frameLast + 1) + '" data-pagesize="' + pageSize + '">&hellip;</a></li>';
//	}
	if(currentPage >= pageCount){
		// 到了最后一页，没有下一页
		template += '<li class="disabled"><a>&gt;</a></li>'
			+ '<li class="disabled"><a>尾页</a></li>';
	} else {
		// 还未到最后一页，">" = 当前页的下一页
		template += '<li><a title="后1页" data-page="' + (+currentPage + 1) +'" data-pagesize="' + pageSize + '">&gt;</a></li>'
			+ '<li><a data-page="' + pageCount +'" data-pagesize="' + pageSize + '">尾页</a></li>';
	}
//	template += '<li class="select-size"> 每页 <select data-pagesize data-page class="select-page">';
//	$.each(pageSizeArray, function(index, aSize){
//		template += '<option ' + (aSize == pageSize ? 'selected="selected"' : '') + 'value="' + aSize + '">' + aSize + '</option>';
//	});
//	template += '</select> 条，共 ';
//	template += '<strong>' + pageCount + '</strong> 页 ';
//	template += '<strong>' + total + '</strong> 条</li>';
	$paginationNavBar.html(template).show();

	// 绑定按钮提示信息
	// $paginationNavBar.find('[data-toggle=tooltip][data-page][data-pagesize]').tooltip();

	// 页码按钮绑定事件
	$('a[data-page][data-pagesize]').on('click', function(event) {
		var self = $(this);
		var selectedPage = numberClean(self.data('page'));
		var selectedPageSize = numberClean(self.data('pagesize'));
		if(eventHandler != undefined && (typeof eventHandler === 'function')){
			eventHandler({page: selectedPage, pageSize: selectedPageSize});
		}
	});

	// 分页Size按钮绑定事件
	$('select[data-page][data-pagesize]').on('change', function(event) {
		var selectedPage = undefined;
		var selectedPageSize = numberClean($(this).children('option:selected').val());
		if(eventHandler != undefined && (typeof eventHandler === 'function')){
			eventHandler({page: selectedPage, pageSize: selectedPageSize});
		}
	});
};/* <--刷新分页导航条 */

//js过滤用户提交的数字数据
function numberClean(source, defaultValue) {
    if (typeof(defaultValue) === 'undefined') {
        // 默认值
        defaultValue = 0;
    }
    var result;
    if (source && source != undefined) {
        if (isNaN(source)) {
            result = source.replace(/[\D]/g, '');
        } else {
            result = source;
        }
    } else {
        result = defaultValue;
    }
    return result;
}

/**
 * @desc 将时间戳转为本地日期
 * @author YangLong,liaojianwen
 * @param i 时间戳
 * @param n 返回值类型 1 日期 2 事件 其他 日期+事件
 * @date 2015-03-13
 */
function intToLocalDate(i, n) {
	if(i == 0 || i.length == 1){
		return '';
	}
    var i = parseInt(i) * 1000;
    var d = new Date(i);
    var date = [];
    date['date'] = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).substr(-2, 2) + '-' + ('0' + d.getDate()).substr(-2, 2);
    date['md'] = ('0' + (d.getMonth() + 1)).substr(-2, 2) + '-' + ('0' + d.getDate()).substr(-2, 2);
    date['time'] = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    date['ftime'] = ('0' + d.getHours()).substr(-2, 2) + ':' + ('0' + d.getMinutes()).substr(-2, 2) + ':' + ('0' + d.getSeconds()).substr(-2, 2);
    date['clock'] = ('0' + d.getHours()).substr(-2, 2) + ':' + ('0' + d.getMinutes()).substr(-2, 2);
    switch (n) {
        case 1:
            return date['date'];
            break;
        case 2:
            return date['time'];
            break;
        case 3:
            return date['date'] + '&nbsp;&nbsp;' + date['clock'];
            break;
        case 4:
            return date['date'] + '<br />' + date['time'];
            break;
        case 5:
            return date['date'] + ' ' + date['ftime'];
            break;
        case 6:
            return date['date'] + '<br />' + date['ftime'];
            break;
        case 7:
            return date['date'] + ' ' + date['clock'];
            break;
        case 8:
            return date['date'] + '<br />' + date['clock'];
            break;
        case 9:
            var _nd = new Date();
            if ((_nd.getTime() - d.getTime()) / 1000 < 3600 * 24 * 250) {
                return date['md'];
            } else {
                return date['date'];
            }
            break;
        case 10:
        	var dt2 =new Date();
	    	 var weekDay = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
	    	 return date['md'] + ' ' + weekDay[dt2.getDay()];
	    	 break;
        case 11:
        	return date['md'] + ' ' + date['clock'];
        	break;
        default:
            return date['date'] + ' ' + date['time'];
            break;
    }
}

/**
 * @desc 记住URL
 * @author YangLong
 * @date 2015-03-19
 */
try {
    if (window.top.location != window.self.location) {
        setCookie('remember_url', window.location.href);
    }
} catch (ex) {}

/**
 * @desc 设置cookies
 * @author YangLong
 * @date 2015-03-19
 */
function setCookie(c_name, value, expiredays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + expiredays);
    document.cookie = c_name + "=" + escape(value) + ((expiredays == null) ? "" : ";expires=" + exdate.toGMTString());
}

/**
 * @desc 获取cookies
 * @author YangLong
 * @date 2015-03-19
 */
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        var c_start, c_end;
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

/**
 * @desc 时间格式转为时间戳
 * @author liaojianwen
 * @date 2015-04-20
 * @param dateStr
 * @return
 */
function get_unix_time(dateStr) {
    var newstr = dateStr.replace(/-/g, '/');
    var date = new Date(newstr);
    var time_str = date.getTime().toString();
    return time_str.substr(0, 10);
}

/**
 * @desc HTML解码，非安全
 * @param strEncodeHTML 需要解码的文档
 * @author YangLong
 * @date 2015-06-19
 * @return string 解码后的innerText
 */
function HTMLDecode(strEncodeHTML) {
    var div = document.createElement('div');
    div.innerHTML = strEncodeHTML;
    return div.innerText;
}

/**
 * @desc 首字母大写
 * @param str 需要转换的字符串
 * @author liaojianwen
 * @date 2015-08-10
 * @returns {String}
 */
function ucfirst(str) {
    var str = str.toLowerCase();
    while (str.indexOf("_") != -1) {
        str = str.replace("_", " ");
    }
    var result = str.substring(0, 1).toUpperCase() + str.substring(1);
    return result;
}


/**
 * @desc HTML编码
 * @param string unsafe string
 * @author YangLong
 * @date 2015-10-26
 * @return string safe string
 */
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

// 禁止按钮点击时文本被选中的效果
$('.subBtn').on('click', '', function() {
    window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
});


function trim(str){ //删除左右两端的空格
     return str.replace(/(^\s*)|(\s*$)/g, "");
}

//数字四舍五入
//num 要处理的数，v 预留小数位数
function decimal(num,v){
	var vv = Math.pow(10,v);
	return Math.round(num*vv)/vv;
}

/**
 * @desc  重新算基础数量
 * @param object
 * @returns {Number}
 */
function checkBaseQuantity(object){
	var _rate = object.find('[name="unit"]').find('option:selected').attr('data-rate');
	var _quantity = object.find('[name="quantity"] input').val();
	return _quantity * _rate;
}

