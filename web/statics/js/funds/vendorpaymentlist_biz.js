$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	listPayment();
	function listPayment(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#paymentlist').find('tr').remove();
		$.get('get-vendor-payment',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						for(var i in W){
							$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].vendor_payment_id+'"></td>'
							+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].vendor_payment_id+'">'+W[i].vendor_payment_no+'</td>'
							+'<td class="text-center payment_date">'+intToLocalDate(W[i].payment_date,1)+'</td>'
							+'<td class="text-center summary_date">'+W[i].summary_time+'</td>'
							+'<td class="text-center vendor" data-id="">'+W[i].supplier_name+'</td>'
							+'<td class="text-center handle_man">'+W[i].employee_name+'</td>'
							+'<td class="text-center total_amount">'+W[i].total_amount+'</td>'
							+'<td class="text-center remark">'+W[i].remark+'</td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="settle_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
//							+'<td class="text-center">'+(+W[i].confirm_flag ? '<a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a>':'<a class="btn btn-success confirm" style="padding: 6px 8px;">确认</a>')
							+'<td class="text-center"><a class="btn btn-success explode" style="padding: 6px 8px;">导出</a>'
							+'</td>'+
							+'</tr>').appendTo('#paymentlist');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listPayment);
			                }
						}
					}else {
						$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#paymentlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPayment);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#paymentlist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPayment);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//编辑
	$('#paymentlist').on('click','.settle_edit',function(){
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有编辑其他入库功能
		$.get('check-edit-vendor-payment',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			
			
			window.location.href = "/funds/edit-vendor-payment?id="+ id;
		})
	})
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
		};
		
		listPayment();
	});
	
	//过滤
	$('#filterPay').on('change',function(){
		global_info.filter = $(this).val();
		listPayment();
	})
	
	/**
	 * @desc 点击单号查询商品明细
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	
	var settings = {
		trigger:'hover',
		title:'商品信息 ',
		content:'<p>没有数据</p>',
		width:320,						
		multi:false,						
		closeable:false,
		style:'',
//		delay:300,
	    cache:true,
		padding:true,
		autoHide:10,
	};
	
	function initPopover(){					
		$('#paymentlist').on('click','.show-pop-table',function(){
			var THIS =$(this);
			var iindex = layer.load();
			$.get('get-vendor-payment-det',{'id':$(this).attr('data-id')},function(data,status){
				THIS.webuiPopover('destroy');
				if(status == 'success'){
					if(data.Ack =='Success'){
						$('#webuiList').empty();
						var M = data.Body.list;
						var _table ='';
						
						for(var i in M){
							 $('<tr><td data-id="'+M[i].vendor_payment_det_id+'">'+(+i+1)+'</td>'
								 +'<td>'+M[i].product_name+'</td>'
								 +'<td>'+intToLocalDate(M[i].purchase_time,1)+'</td>'
								 +'<td>'+M[i].quantity+'</td>'
								 +'<td>'+M[i].unit_name+'</td>'
								 +'<td>'+M[i].price+'</td>'
								 +'<td>'+M[i].amount+'</td>'
								 +'<td>'+M[i].remark+'</td>'
								 +'</tr>').appendTo('#webuiList');
						}
						var Total_amount = +data.Body.total_amount;
						$('<tr><td colspan="6">total</td>'
						   +'<td>'+Total_amount+'</td>'
						   +'<td></td>'
						   +'</tr>').appendTo('#webuiList');
						
					}
					layer.close(iindex);
					
					var tableContent = $('#tableContent').html();
					tableSettings = {content:tableContent,
										width:800
									};
					THIS.webuiPopover($.extend({},settings,tableSettings));
				} else {
					alertTips('error','网络错误');
				}
			})
//			$(this).webuiPopover($.extend({},settings,tableSettings));
//			$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
		})
	}
	
	initPopover();
	
	
	//确认
	$('#paymentlist').on('click','.confirm',function(){
		var tr = $(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有确认功能
		$.get('check-confirm-settle-payables',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('点击确认应付结算单将无法编辑，是否确认?', function(index) {
				$.get('confirm-settle-payables',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
	/**
	 * @desc 删除
	 */
	$('#delete').on('click', function(){
		var _all_ids='';
		var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
		$('.ids').each(function(i){
			tmp_count +=i;
			if($(this).prop('checked')){
				_all_ids += $(this).attr('data-id') + ',';
				
			}
		})

		_all_ids = _all_ids.substr(0,_all_ids.length -1);
		if (_all_ids.length === 0) {
			alertTips('warning','请勾选复选框！');
			return false;
		} 
		if(tmp_count == 0){
			if(+global_info.page > 1){
				global_info.page --;
			}
		}
		//检查是否有确认功能
		$.get('check-delete-vendor-payment',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('是否删除请款单！', function(index) {
				$.get('delete-vendor-payment',{'ids':_all_ids},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
		
	});
	
	/**
	 * @desc 导出请款单
	 */
	$('#paymentlist').on('click', '.explode', function(){
		var tr = $(this).closest('tr');
		var _id = tr.find('.ids').attr('data-id');
		$.get('check-explode-vendor-payment',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error','你没有操作权限，如要操作，请向管理员申请');
				return;
			} else {
				$url ='explode-vendor-payment?id='+_id;
				window.location.href = $url;
			}
		})
	});
})