$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//入库单列表
		listPayables();
		function listPayables(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#paylist').find('tr').remove();
			$.get('get-payables',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center"><input name="ids" class="ids" type="checkbox" data-id="'+W[i].pay_id+'"></td>'
								+'<td class="text-center"  data-id="'+W[i].pay_id+'">'+(+i+1)+'</td>'
								+'<td class="text-center">'+W[i].settle_flag+'</td>'
								+'<td class="text-center" data-id="'+W[i].origin_id+'"><a href="###" class="purchase_id">'+W[i].origin_no+'</a></td>'
								+'<td class="text-center pay_date">'+intToLocalDate(W[i].pay_time,1)+'</td>'
								+'<td class="text-center" data-id="'+W[i].vendor_id+'">'+W[i].supplier_name+'</td>'
								+'<td class="text-center" data-id="'+W[i].pay_man+'">'+W[i].employee_name+'</td>'
								+'<td class="text-center">'+W[i].pay_type_name+'</td>'
								+'<td class="text-center">'+W[i].pay_amount+'</td>'
								+'<td class="text-center">'+W[i].settle_amount+'</td>'
								+'<td class="text-center">'+W[i].receive_amount+'</td>'
								+'<td class="text-center">'+W[i].left_amount+'</td>'
								+'<td class="text-center">'+W[i].origin_type_name+'</td>'
								+'<td class="text-center">'+W[i].remark+'</td>'
								+'<td class="text-center">'+W[i].description+'</td>'
								+'</tr>').appendTo('#paylist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listPayables);
				                }
							}
						}else {
							$('<tr><td colspan="14" class="text-center">没有数据</td></tr>').appendTo('#paylist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPayables);
						}
					} else {
						$('<tr><td colspan="14" class="text-center">没有数据</td></tr>').appendTo('#paylist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listPayables);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
		/**
		 * @desc 点击详情
		 */
		$('#paylist').on('click','.purchase_id', function(){
			var target_id = $(this).parent('td').attr('data-id');
			window.open("/purchase/edit-purchase?id="+target_id);   
		})
		
		//查询
		$('#search_list').on('click',function(){
			var starTime = $('#starTime').val();
			var endTime = $('#endTime').val();
			if(starTime > endTime){
				alertTips('warning','开始时间不能大于截至时间！');
				return false;
			}
			global_info.cond ={
				'starTime': starTime,
				'endTime': endTime,
				'vendor':$('#vendor').val(),
//				'warehouse':$('#stock').val(),
			};
			
			listPayables();
		});
		
		//过滤
		$('#filterPay').on('change',function(){
			global_info.filter = $(this).val();
			listPayables();
		})
		
		//全选
		$('#checkall').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		})
		
//		//付款核算
//		$('#account').on('click',function(){
//			var _all_ids ='';
//			$('#paylist').find('tr').each(function(index,element){
//				var flag = $(element).find('[name="ids"]').prop('checked');
//				if(flag){
//					if()
//					_all_ids += $(element).find('[name="ids"]').attr('data-id') + ','
////					$('.ids').each(function(i){
////						tmp_count +=i;
////						if($(this).prop('checked')){
////							_all_ids += $(this).attr('data-id') + ',';
////							
////						}
////					})
//				}
////				console.log($(element).html());
//			})
//		})
		
})