$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	//入库单列表
	listSettle();
	function listSettle(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#settlelist').find('tr').remove();
		$.get('get-settle-payables',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						for(var i in W){
							$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].settle_pay_id+'"></td>'
							+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].settle_pay_id+'">'+W[i].settle_pay_no+'</td>'
							+'<td class="text-center instore_date">'+intToLocalDate(W[i].settle_pay_date,1)+'</td>'
							+'<td class="text-center vendor" data-id="">'+W[i].supplier_name+'</td>'
							+'<td class="text-center settle_man">'+W[i].employee_name+'</td>'
							+'<td class="text-center settle_type">'+W[i].settle_name+'</td>'
							+'<td class="text-center settle_amount">'+W[i].settle_amount+'</td>'
							+'<td class="text-center discount_amount">'+W[i].discount_amount+'</td>'
							+'<td class="text-center remark">'+W[i].remark+'</td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="settle_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
//							+(+W[i].confirm_flag ? '<td class="text-center"><a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>')
							+'<td class="text-center">'+(+W[i].confirm_flag ? '<a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a>':'<a class="btn btn-success confirm" style="padding: 6px 8px;">确认</a>')
							+'&nbsp&nbsp&nbsp'+(+W[i].confirm_flag ? '<a href="#" class="btn btn-success invalid" style="background-color:#de6d5b;padding: 6px 8px;">红冲</a>':'<a href="#" class="btn btn-info delete" style="padding: 6px 8px;">删除</a>')
							+'</td>'+
//							+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
							+'</tr>').appendTo('#settlelist');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSettle);
			                }
						}
					}else {
						$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#settlelist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSettle);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#settlelist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSettle);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	};
	
	//编辑
	$('#settlelist').on('click','.settle_edit',function(){
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有编辑其他入库功能
		$.get('check-edit-settle-payables',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			
			
			window.location.href = "/funds/edit-settle-payables?id="+ id;
		})
	})
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
		};
		
		listSettle();
	});
	
	//过滤
	$('#filterPay').on('change',function(){
		global_info.filter = $(this).val();
		listSettle();
	})
	
	/**
	 * @desc 点击单号查询商品明细
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	
	var settings = {
		trigger:'hover',
		title:'商品信息 ',
		content:'<p>没有数据</p>',
		width:320,						
		multi:false,						
		closeable:false,
		style:'',
//		delay:300,
	    cache:true,
		padding:true,
		autoHide:10,
	};
	
	function initPopover(){					
		$('#settlelist').on('click','.show-pop-table',function(){
			var THIS =$(this);
			var iindex = layer.load();
			$.get('get-settle-payables-det',{'id':$(this).attr('data-id')},function(data,status){
				THIS.webuiPopover('destroy');
				if(status == 'success'){
					if(data.Ack =='Success'){
						$('#webuiList').empty();
						var M = data.Body.list;
						var _table ='';
						for(var i in M){
							 $('<tr><td data-id="'+M[i].settle_pay_id+'">'+(+i+1)+'</td>'
								 +'<td>'+M[i].origin_no+'</td>'
								 +'<td>'+M[i].pay_time+'</td>'
								 +'<td>'+M[i].pay_type_name+'</td>'
								 +'<td>'+M[i].pay_man_name+'</td>'
								 +'<td>'+M[i].pay_amount+'</td>'
								 +'<td>'+M[i].settled_amount+'</td>'
								 +'<td>'+M[i].left_amount+'</td>'
								 +'</tr>').appendTo('#webuiList');
						}
						var T = data.Body;
						$('<tr><td colspan="5">total</td>'
						   +'<td>'+T.total_pay_amount+'</td>'
						   +'<td>'+T.total_received_amount+'</td>'
						   +'<td>'+T.total_left_amount+'</td>'
						   +'</tr>').appendTo('#webuiList');
						
					}
					layer.close(iindex);
					
					var tableContent = $('#tableContent').html();
					tableSettings = {content:tableContent,
										width:800
									};
					THIS.webuiPopover($.extend({},settings,tableSettings));
				} else {
					alertTips('error','网络错误');
				}
			})
//			$(this).webuiPopover($.extend({},settings,tableSettings));
//			$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
		})
	}
	
	initPopover();
	
	
	//确认
	$('#settlelist').on('click','.confirm',function(){
		var tr = $(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有确认功能
		$.get('check-confirm-settle-payables',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('点击确认应付结算单将无法编辑，是否确认?', function(index) {
				$.get('confirm-settle-payables',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
	/**
	 * @desc 删除
	 */
	$('#settlelist').on('click', '.delete', function(){
		var tr = $(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有确认功能
		$.get('check-delete-settle-payables',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('是否删除结算单?', function(index) {
				$.get('delete-settle-payables',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
		
	})
	
	/**
	 * @desc 红冲
	 */
	$('#settlelist').on('click', '.invalid', function(){
		var tr = $(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有确认功能
		$.get('check-invalid-settle-payables',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			layer.confirm('点击红冲结算单将无法无效，是否继续?', function(index) {
				$.get('invalid-settle-payables',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','操作成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
	
})