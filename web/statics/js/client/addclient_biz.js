$(function(){
	var _id = $_GET['id'] ? $_GET['id']:0;
	
	
	/**
	 * @初始化页面
	 */
	(function() {
		$.get('get-client-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var info = data.Body;
				$('#client').val(info.client_name);
				$('#handle_man').val(info.handle_man);
				$('#client_info').val(info.client_desc);
				$('#saveClient').attr('data-id',info.client_id);
				$('#phone').val(info.phone);
				$('#store_id').val(info.store_id);
			}
		})
	})();
	
	/**
	 * @选择省份,添加城市列表
	 */
	$('#re_province').change(function(){
		$('#re_city').find('option:first').nextAll().remove();//清空城市
		$('#re_district').find('option:first').nextAll().remove();//清空地区
		$.get('get-citys',{'province':$(this).val()},function(data,status){
			if(status =='success'){
				if(data.Ack =='Success'){
					var R = data.Body;
					if(R && R.length){
						for(var i in R){
							$('<option value="'+R[i].region_id+'">'+R[i].region_name+'</option>').appendTo($('#re_city'));
						}
					}
				} else {
				}
			} else {
				alertTips('error','网络错误！');
			}
			
		})
		
	});
	
	/**
	 * @desc 选择城市，添加地区
	 */
	$('#re_city').change(function(){
		$('#re_district').find('option:first').nextAll().remove();//清空地区
		$.get('get-district',{'city':$(this).val()},function(data,status){
			if(status == 'success'){
				if(data.Ack =='Success'){
					var R = data.Body;
					if(R && R.length){
						for(var i in R){
							$('<option value="'+R[i].region_id+'">'+R[i].region_name+'</option>').appendTo($('#re_district'));
						}
					}
				} else {
				}
			} else{
				alertTips('error','网络错误！');
			}
		})
	});
	
	
	/**
	 *@desc 验证提交数据
	 */
	var validateInfo = function(){
		$('#newClient').validate({
			rules : {
				client : {
					required : true,
					minlength : 2,
					remote : '/client/check-client?id=' + _id,
				},
				handle_man:{
					required:true,
				},
				re_city:{
					required:true,
				},
//				re_district:{
				//海南地区还没全
//					required:true,
//				}

			},
			messages : {
				client : {
					required : "请输入客户名称",
					minlength : "客户名最少两个字母组成",
					remote : "此客户名称已存在",
				},
				re_city:{
					required:"请选择城市",
				},
//				re_district:{
//					required:"请选择地区",
//				}
			},
			submitHandler : function(form) {
				saveClient();
			}
		});
	}
	
	$('#saveClient').on('click',function(){
		validateInfo();
	});
	
	//保存客户
	function saveClient()
	{
		var dis_count = $('#re_district').find('option').length;
		var dis_select = $('#re_district').find('option:selected').val();
		if(dis_count > 1){
			if(dis_select == undefined || dis_select == 0){
				alertTips('warning','请选择地区');
				return false;
			}
		}
		var client_info = {
			'id' : $('#saveClient').attr('data-id'),
			'sname':$('#client').val(),
			'desc':$('#client_info').val(),
			'man':$('#handle_man').val(),
			'country':$('#re_country').val(),
			'province':$('#re_province').val(),
			'city':$('#re_city').val(),
			'district':$('#re_district').val(),
			'phone':$('#phone').val(),
			'store_id': $('#store_id').val(),
		}
		$.get('save-client',client_info,function(data,status){
			if(status =='success'){
				if(data.Ack =='Success'){
					alertTips('success','保存成功！');
//					window.location.href= "list-client";
					setTimeout(function(){
						 window.location.reload();
					}, 1000);
				} else {
					alertTips('error','保存失败！');
				}
			}
		})
	}
	
	//重置
	$('#reset').on('click',function(){
		window.location.reload();
	});
});