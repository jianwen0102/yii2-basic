$(function(){
	var _id = parseInt($_GET['id']?$_GET['id']: 0);
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
			'ware': undefined,
	}
	var global_count = 1;
	var global_remove = [];
	var select_units;
	var global_ware;
	//时间
	dateSelFun($('#inventory_date'));
	
	/**
	 * @desc 单位
	 * 
	 */
	(function(){
//		select_units = '<select class="form-control units"><option value="0">请选择单位..</option>';
//		var ii = layer.load();
//		$.get('/instore/list-units', function(data, status){
//			layer.close(ii);
//			if(status =='success' && data.Ack =='Success'){
//				var U = data.Body;
//				for(var i in U){
//					select_units +='<option value="'+U[i].unit_id+'">'+U[i].unit_name+'</option>';
//				}
//			}
//			select_units +='</select>';
//			if(_id != 0){
//				//编辑
//				initInventory();
//			}
//		});
		select_units = '<select title="单位" class="form-control unit">'
			+ '<option value="0">单位</option>'
			+ '<option value="6">个</option>'
			+ '<option value="5">支</option>'
			+ '<option value="4">包</option>'
			+ '<option value="2">斤</option>'
			+ '<option value="1">箱</option>' + '</select>';
		
		if(_id != 0){
		//编辑
			initInventory();
		}
	})();
	
	
	/**
	 * @初始化页面
	 */
	function initInventory() {
		$('#confirm').show();
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-inventory-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#inventory_no').val(H.inventory_no);
					$('#inventory_date').val(intToLocalDate(H.inventory_date,1));
					$('#warehouse').val(H.warehouse_id);
					global_ware = H.warehouse_id;
					$('#handle_man').val(H.handle_man);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					$('#total_amount').val(H.total_amount);
					if(+H.confirm_flag){
						$('#save').attr('style','display:none');
						$('#confirm').removeClass().addClass('btn btn-warning btn-sm pull-right').attr('disabled','disabled');
					}
					$('#inventory_amount').val(H.inventory_amount);
				}
				var D = data.Body.det;
				if(D){
					$('#stockList').find('tr').remove();
					for(var i in D){
						var tmp_units= '';
						var U = D[i].product_unit;
						tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
						for(var j in U){
							tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
						}
						tmp_units +='</select>';
						$('<tr>'
						   +'<td class="text-center" name="ids" data-id="'+D[i].inventory_det_id+'">'+(+i+1)+'</td>'
						   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].product_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
						   +'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
						   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
						   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
						   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
						   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="lost_quantity"><input type="text" value="'+D[i].lost_quantity+'" class="lost_quantity" disabled></td>'
						   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity" disabled></td>'
						   +'<td class="text-center" name="inventory_quantity"><input type="text" value="'+D[i].inventory_quantity+'" class="inventory_quantity"></td>'
						  
						   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'" disabled></td>'
						   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'" disabled></td>'
						   +'<td class="text-center" name="unit_relation">'+D[i].unit_content+'</td>'
						   +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
						   +'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
						   +'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'">'+D[i].base_name+'</td>'
						   +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].base_quantity+'" disabled></td>'
						   +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="'+D[i].base_price+'" disabled></td>'
						   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
//						   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
						   +'</tr>').appendTo('#stockList');
//						$('#stockList').find('tr:last').find('.units').val(D[i].quantity_unit).attr('disabled','disabled');
						$('#stockList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit).attr('disabled','disabled');
						$('#stockList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
						global_count++;
					}
				}
			}
		});
	};
	
	//更改仓库提示
	$('#warehouse').change(function(){
		var current_ware = $(this).val();
		var tmp_flag = 0;
		if(global_count == 1){
			var pid = $('#stockList').find('.stock-minus-tr').data['id'];
			if(pid != undefined){
				tmp_flag = 1;
			} ;
		}else{
			tmp_flag =1;
		}
		if(tmp_flag){
			layer.confirm('更换仓库，明细将被清除，是否继续?', function(index) {
                var td = $('#stockList').find('tr').find('td:first');
                $.each(td, function(index, item) {
    				var tmp_id = $(item).attr('data-id');
    				if ($.inArray(tmp_id, global_remove) >= 0) {
    					return false;
    				}
    				if(tmp_id){
    					global_remove.push({'inventory_det_id':tmp_id});
    				}
                })
				$('#stockList').find('tr').remove();
				global_count =1;
				insertOrginHtml();
				global_ware = current_ware;
				
				layer.close(index);
			},function(index){
				$('#warehouse').val(global_ware);
				layer.close(index);
			});
		} else{
			global_ware = current_ware;
		}
	})
	
	/**
	 * @desc 选择商品
	 */
	$('#inventory_detail').on('click','.search-product',function(){
		$('#productName').val('');
		global_product.page = 1;
		global_product.name = undefined;
		getProducts();
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		global_product.page = 1;
		getProducts();
		return;
	})
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		global_product.ware = $('#warehouse').val();
		$('#product_list').find('tr').remove();
		if(global_product.ware == 0){
			alertTips('error','请先选择仓库');
			return false;
		}
		var index = layer.load();
		 $.ajax({
             url: 'get-product-by-ware',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 if(data.Ack =='Success'){
	            	 var P = data.Body.list;
						if(P && P.length){
							for(var i in P){
								var tmp_units = ''
									var U = P[i].product_unit;
									tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
									for(var j in U){
										tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
								}
								tmp_units +='</select>';
								$('<tr>'
								+'<td class="text-center">'+(+i+1)+'</td>'
								+'<td class="text-center id">'+P[i].product_id+'</td>'
								+'<td class="text-center name">'+P[i].product_name+'</td>'
								+'<td class="text-center sn">'+P[i].product_sn+'</td>'
								+'<td class="text-center quantity">'+P[i].quantity+'</td>'
								+'<td class="text-center price">'+P[i].price+'</td>'
								+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'" data-rela="'+P[i].unit_content+'">'+(P[i].unit_name == null ? '':P[i].unit_name)+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="product_select" data-unit="'+encodeURI(tmp_units)+'">选择</a></td>'
								+'</tr>').appendTo('#product_list');
				              
							}
							var pageInfo = data.Body.page;
				            if (typeof pageInfo !== 'undefined'){
				              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
				            }
						}else {
							$('<tr><td colspan="7" class="text-center">没有数据</td></tr>').appendTo('#product_list');
							refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
						}
					$('#chooseProduct').modal('show');//选择商品窗口
	             } else{
	            	 
	             }
             }
		 })
	}
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var tmp_units = decodeURI($(this).attr('data-unit'));
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var sn = tr.find('.sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		if(!+unit){
			alertTips('warning','请在商品管理添加单位');
			return false;
		}
		var unit_name = tr.find('.unit').text();
		var price = tr.find('.price').text();
		var unit_content = tr.find('.unit').attr('data-rela');
		
		if(global_count == 1){
			var pid = $('#stockList').find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				$('#stockList').find('tr:first').remove();
			} ;
		}
//		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="sn">'+sn+'</td>'
		  +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="lost_quantity"><input type="text" class="lost_quantity" value="'+(-quantity)+'" disabled></td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity" value="'+quantity+'" disabled></td>'
		  +'<td class="text-center" name="inventory_quantity"><input type="text" class="inventory_quantity"></td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+price+'" disabled></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" disabled></td>'
		  +'<td class="text-center" name="unit_relation" style="width:8%;">'+unit_content+'</td>'
		  +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0"></td>'
		  +'<td class="text-center" name="second_relation"></td>'
		  +'<td class="text-center" name="base_unit" data-id="'+unit+'">'+unit_name+'</td>'
		  +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0" disabled></td>'
		  +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo('#stockList');
		$('#stockList').find('tr:last').find('[name="unit"]').find('.units').val(unit).attr('disabled','disabled');
		global_count++;
	})
	
	//删除数据
	$('#stockList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'inventory_det_id':tmp_id});
		}
		if(global_count == 1){
			return false;
		} else if(global_count == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#stockList').find('tr:first').remove();
			global_count--;
			insertOrginHtml();
			SumCheckAmount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#stockList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_count--;
                SumCheckAmount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_count--;
                SumCheckAmount();
                return false;
            }
		}
		
	});
	
	/**
	 * @var 计算增减明细后总金额
	 */
	var SumCheckAmount = function(){
		var total_amount = 0;
		$('#stockList tr').each(function(index,element){
			var _amount = $(element).find('[name="amount"] input').val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
		})
		$('#total_amount ').val(total_amount);
	};
	
	var insertOrginHtml = function(){
		$('<tr>'
			+'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
			+'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
			+'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
			+'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
			+'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
			+'<td class="text-center" name="sn"></td>'
			+'<td class="text-center" name="unit">'+select_units+'</td>'
			+'<td class="text-center" name="lost_quantity"><input type="text"></td>'
			+'<td class="text-center" name="quantity"><input type="text"></td>'
			+'<td class="text-center" name="inventory_quantity"><input type="text"></td>'
			+'<td class="text-center" name="price"><input type="text"></td>'
			+'<td class="text-center" name="amount"><input type="text"></td>'
			+'<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_unit">'+select_units+'</td>'
			+'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>'
			+'<td class="text-center" name="second_relation"></td>'
			+'<td class="text-center" name="base_unit"><input title="基本单位" type="text" value="0.00"></td>'
			+'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0.00" disabled></td>'
			+'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
			+'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//			+'<td class="text-center"><a href="javascript:void(0)" class="checkStock">库存</a></td>'
			+'</tr>').appendTo('#stockList');
	}
	
//	//库存提示
//	$('#stockList').on('click','.checkStock',function(){
//		var _quantity = $(this).attr('data-quantity');
//		if(_quantity !=undefined){
//			layer.tips('库存: '+ _quantity, $(this), {
//				  tips: [4, '#78BA32']
//			});
//		}
//	});
//
	//名称提示
	$('#stockList').on('click','.product_name',function(){
		var name = $(this).val();
		if(name !=undefined){
			layer.tips(name, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	$('#stockList').on('keyup','.inventory_quantity',function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var inventory_quantity = $(this).val();
		
		var tr = $(this).closest('tr');
		var _unit = tr.find('[name="unit"] select').val();
		var base_unit = tr.find('[name="base_unit"]').attr('data-id');
		var _rate = tr.find('[name="unit"]').find('option:selected').attr('data-rate');
		if(_unit == base_unit){
			tr.find('[name="base_quantity"] input').val(inventory_quantity * _rate);
		} else {
			tr.find('[name="base_quantity"] input').val(inventory_quantity * _rate);
		}
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = inventory_quantity * _rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(inventory_quantity*_rate/second_rate)+' '+second_name + ' '+(inventory_quantity*_rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		
		var _price = $(this).closest('tr').find('.price').val()
		if(_price == ''){
			_price = 0
		}
		var stock = $(this).closest('tr').find('.quantity').val();
		var lost_quantity = inventory_quantity - stock;
		var _amount = (lost_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.lost_quantity').val(lost_quantity);
		$(this).closest('tr').find('.amount').val(_amount);
		if(lost_quantity){
			tr.find('[name="base_price"] input').val((_amount / (lost_quantity * _rate)).toFixed(2));
		}
		SumAmount();
		
	});
	
	//单位验证
	$('#stockList').on('change','[name="unit"] select', function(){
		var unit = $(this).val();
		var rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		var inventory_quantity = tr.find('[name="inventory_quantity"] input').val();
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = inventory_quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(inventory_quantity*rate/second_rate)+' '+second_name + ' '+(inventory_quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		tr.find('[name="base_quantity"] input').val(inventory_quantity * rate);
		
		
	})
	
	//辅助单位
	$('#stockList').on('change','[name="second_unit"] select',function(){
		var second_unit = $(this).val();
		var second_rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		if(+second_unit){
			var inventory_quantity = tr.find('[name="inventory_quantity"] input').val();
			var rate = tr.find('[name="unit"]') .find('option:selected').attr('data-rate');
			var tmp_quantity = inventory_quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(inventory_quantity*rate/second_rate)+' '+second_name + ' '+(inventory_quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		} else {
			tr.find('[name="second_quantity"] input').val('');
			tr.find('[name="second_relation"]').text('');
		}
	})
	
	
	/**
	 * @var 计算盈亏金额
	 */
	var SumAmount = function(){
		var total_amount = 0;
		$('#stockList tr').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			
		})
		$('#total_amount').val(total_amount);
	};
	
	/**
	 * @var 保存单据
	 */
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = inventory_header();
			if(!head){
				return false;
			}
			//明细
			var detail = inventory_detail();
			if(!detail){
				return false;
			}
			var ii = layer.load();
			$.post('save-inventory',{'head':head,'det':detail,'remove': global_remove,'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
 							if(_id) {
								window.location.href = window.location.href;
							} else {
								window.location.href = "list-inventory";
							}
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var inventory_header = function() {
		var no = $('#inventory_no').val();
		var inventory_date = $('#inventory_date').val();
		if (inventory_date.length == 0) {
			alertTips('warning', '请输入盘点时间');
			return false;
		}
		var warehouse = $('#warehouse').val();
		if (!+warehouse) {
			alertTips('warning', '请选择入库仓库');
			return false;
		}
		var handle_man = $('#handle_man').val();
		if (!+handle_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var inventory_head = {
			'inventory_no' : no,
			'inventory_date' : inventory_date,
			'warehouse_id' : warehouse,
			'handle_man' : handle_man,
			'create_man' : create_man,
			'remark' : remark,
			'total_amount':$('#total_amount').val(),
		};
		return inventory_head;
	}
	
	//盘点单明细
	var inventory_detail = function(){
		var product_det=[];
		var product_info = [];
		var _tr = $('#stockList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var inven_quantity = tr.find('[name="inventory_quantity"] input').val();
			if(inven_quantity == ''){
				alertTips('warning','请填写盘点数量');
				flag = 0;
				return false;
			}
			var lost_quantity = tr.find('[name="lost_quantity"] input').val();
			var quantity = tr.find('[name="quantity"] input').val();
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			product_det = {
				'inventory_det_id' : line_id,
				'product_id' : tr.find('[name="name"]').attr('data-id'),
				'inventory_quantity' :inven_quantity, //盘点数量
				'lost_quantity' : lost_quantity,
				'quantity' : quantity, //库存数量
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
				'second_unit':tr.find('[name="second_unit"] select').val(),
				'second_quantity':tr.find('[name="second_quantity"] input').val(),
				'second_relation':tr.find('[name="second_relation"]').text(),
				'base_unit':tr.find('[name="base_unit"]').attr('data-id'),
				'base_quantity':tr.find('[name="base_quantity"] input').val(),
				'base_price':tr.find('[name="base_price"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		return product_info;
	}
	
	//编辑页面的确认
	$('#confirm').on('click',function(){	
		//检查是否有确认其他入库功能
		$.get('check-confirm-inventory',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			layer.confirm('点击确认盘点单将无法编辑，将生成盘盈亏单，是否确认?', function(index) {
				$.get('confirm-inventory',{'id':_id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
})