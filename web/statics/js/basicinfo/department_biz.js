$(document).ready(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined //页数
		}
		
		//员工列表
		listDepart();
		function listDepart(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#depart_list').find('tr').remove();
			$.get('get-depart',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var D = data.Body.list;
						if(D && D.length){
							for(var i in D){
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+D[i].depart_id+'"></td>'
								+'<td class="text-center name">'+D[i].depart_name+'</td>'
								+'<td class="text-center desc">'+D[i].description+'</td>'
								+'<td class="text-center master" data-id="'+D[i].master+'">'+(D[i].master_man?D[i].master_man:'')+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="depart_edit fa fa-pencil-square-o fa-2x" title="编辑"></td>'
								+'</tr>').appendTo('#depart_list');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listDepart);
				                }
							}
						}else {
							$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#depart_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listDepart);
						}
					} else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#depart_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listDepart);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		}
		//新增
		$('#add').on('click',function(){
			//检查是否有新增权限
			$.get('check-add-depart',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				$('#dname').val('');
				$('#dremark').val('');
				$('#depart_save').attr('data-id','');
				$('#myModal').modal('show');
			});
		});
		
		$('#depart_save').on('click',function(){
			$name = $('#dname').val();
			if($name.length == 0){
				alertTips('warning','请输入部门');
				return false;
			}
			var pflag = 0;
			$remark = $('#dremark').val();
			$master = $('#dmaster').val();
			if(!$name){
				alertTips('warning','请选择负责人！');
				return false;
			}
			
			$did = $('#depart_save').attr('data-id');
			
			var index = layer.load(); 
			$.get('save-depart',{'name':$name,'remark':$remark,'id':$did, 'master':$master},function(data,status){
				layer.close(index);
				if(status ==='success' && data.Ack =='Success'){
					alertTips('success','保持成功');
					setTimeout(function(){ 
						$('#myModal').modal('hide');
						listDepart();
					},1000);
				} else if(data.Error =="department is exists"){
					alertTips('warning','该部门已经存在，请更改部门名称');
					return false;
				}else {
					alertTips('error','网络错误');
					return false;
				}
			})
		});
		
		//编辑
		$('#depart_list').on('click','.depart_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			var name = tr.find('.name').text();
			var desc = tr.find('.desc').text();
			var master = tr.find('.master').attr('data-id');
			//检查是否有新增权限
			$.get('check-edit-depart',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				
				$('#depart_save').attr('data-id',id);
				$('#dname').val(name);
				$('#dremark').val(desc);
				$('#dmaster').val(master);
				$('#myModal').modal('show');
			})
		});
		
		//全选
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		//删除
		$('#delete').on('click',function(){
			//检查是否有新增权限
			$.get('check-del-depart',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
				$('.ids').each(function(i){
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
					}
				})
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框！');
					return false;
				}
				if(tmp_count == 0){
					if(+global_info.page > 1){
						global_info.page --;
					}
				}
				if(confirm("确定要删除部门？")){
					$.get('del-depart',{'ids':_all_ids},function(data,status){
						if(status ==='success' && data.Ack ==='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){ 
								listDepart();
							},1500);
						}else {
							alertTips('error','删除失败，请重新再试');
						}
					});
				 }
			})
		});
})