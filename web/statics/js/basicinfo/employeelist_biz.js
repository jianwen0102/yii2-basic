$(document).ready(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined //页数
		}
		
		//员工列表
		listEmployee();
		function listEmployee(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#employee_list').find('tr').remove();
			$.get('get-employee',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var E = data.Body.list;
						if(E && E.length){
							for(var i in E){
								$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+E[i].employee_id+'"></td>'
								+'<td class="text-center name">'+E[i].employee_name+'</td>'
								+'<td class="text-center depart" data-id="'+E[i].depart_id+'">'+E[i].depart_name+'</td>'
								+'<td class="text-center phone">'+E[i].phone+'</td>'
								+'<td class="text-center remark">'+E[i].remark+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="em_edit fa fa-pencil-square-o fa-2x" title="编辑"></td>'
								+'</tr>').appendTo('#employee_list');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listEmployee);
				                }
							}
						}else {
							$('<tr><td colspan="6" class="text-center">没有数据</td></tr>').appendTo('#employee_list');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listEmployee);
						}
					} else {
						$('<tr><td colspan="6" class="text-center">没有数据</td></tr>').appendTo('#employee_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listEmployee);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		}
		//新增
		$('#add').on('click',function(){
			//检查是否新增员工功能
			$.get('check-add-emp',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				$('#ename').val('');
				$('#edepart').val('');
				$('#ephone').val('');
				$('#eremark').val('');
				$('#em_save').attr('data-id','');
				$('#myModal').modal('show');
			});
		});
		
		$('#em_save').on('click',function(){
			$name = $('#ename').val();
			if($name.length == 0){
				alertTips('warning','请输入员工名');
				return false;
			}
			$description = $('#eremark').val();
			$depart = $('#edepart').val();
			if($depart == null ||$depart.length == 0){
				alertTips('warning','请选择所属部门');
				return false;
			}
			$phone = $('#ephone').val();
			$id = $('#em_save').attr('data-id');
			var index = layer.load();
			$.get('save-employee',{'name':$name,'desc':$description,'depart':$depart,'id':$id,'phone': $phone},function(data,status){
				layer.close(index);
				if(status ==='success' && data.Ack =='Success'){
					alertTips('success','保持成功');
					setTimeout(function(){ 
						$('#myModal').modal('hide');
						listEmployee();
					},1000);
				} else if(data.Error =="employee is exists"){
					alertTips('warning','该员工已经存在！');
					return false;
				}else {
					alertTips('error','网络错误');
					return false;
				}
			})
		});
		
		//编辑
		$('#employee_list').on('click','.em_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			var name = tr.find('.name').text();
			var depart = tr.find('.depart').attr('data-id');
			var desc = tr.find('.desc').text();
			var phone = tr.find('.phone').text();
			//检查是否有编辑用户功能
			$.get('check-edit-emp',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
		

				$('#em_save').attr('data-id',id);
				$('#ename').val(name);
				$('#edepart').val(depart);
				$('#dremark').val(desc);
				$('#ephone').val(phone);
				$('#myModal').modal('show');
			})
		});
		
		//全选
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		//删除
		$('#delete').on('click',function(){
			//检查是否有删除用户功能
			$.get('check-del-emp',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
				$('.ids').each(function(i){
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
					}
				})
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框！');
					return false;
				}
				if(tmp_count == 0){
					if(+global_info.page > 1){
						global_info.page --;
					}
				}
				if(confirm("确定要删除部门？")){
					$.get('del-employee',{'ids':_all_ids},function(data,status){
						if(status ==='success' && data.Ack ==='Success'){
							alertTips('success','删除成功');
							setTimeout(function(){ 
								listEmployee();
							},1500);
						}else {
							alertTips('error','删除失败，请重新再试');
						}
					});
				 }
			})
		});
})