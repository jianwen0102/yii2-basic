$(function(){
   storePosList.getStoreList();
});
var storePosList = {
	'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
	'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页总条数
	'searchData':{},//查询数据
	'filter': {}, //过滤条件
    getStoreList: function(pageInfo){//获取供应商列表
	    if(pageInfo != undefined){
		   storePosList.page     = pageInfo.page;
		   storePosList.pageSize = pageInfo.pageSize;
		}
	    var data = {'page':storePosList.page, 'pageSize':storePosList.pageSize, 'searchData':storePosList.searchData, 'filter':storePosList.filter};
		$('#store_list').find('tr').remove();
		$.get('get-store-pos-list',data,function(res){
			if(res.Ack ==='Success'){
				var S = res.Body.list;
				if(S && S.length>0){
					for(var i in S){
						var delete_flag_status = S[i].delete_flag==0 ? '<font style="color:green">正常</font>' : '<font style="color:red">屏蔽</font>';
						$('<tr id="tr_'+S[i].pos_id+'"><td class="text-center"><input type="checkbox"  class="ids" data-id="'+S[i].pos_id+'"></td>'
						+'<td class="text-center pos_name">'+S[i].pos_name+'</td>'
						+'<td class="text-center">'+S[i].store_name+'</td>'
						+'<td class="text-center">'+S[i].create_time+'</td>'
						+'<td class="text-center delete_flag" status="'+S[i].delete_flag+'">'+delete_flag_status+'</td>'
						+'<td class="text-center"><a href="javascript:storePosList.editStoreList(\''+S[i].pos_id+'\');" class="supplier_edit fa fa-pencil-square-o fa-2x" title="编辑"/></td>'
						+'</tr>').appendTo('#store_list');
					}
					var pageInfo = res.Body.pageinfo;
				    if (typeof pageInfo !== 'undefined'){
						refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, res.Body.count, storePosList.getStoreList);
					}
				}else {
					$('<tr><td colspan="5" class="text-center">没有数据了</td></tr>').appendTo('#store_list');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, storePosList.getStoreList);
				}
			} else{
				alertTips('error','没有数据了');
			}
		});
	
	},

	addStoreList: function(){//添加
		$.post('check-add-pos',function(storeList){
			if(storeList.length>0){
			   var htl = '<option value="0">请选择门店</option>';
			   for(var i in storeList){
				   htl += '<option value="'+storeList[i].store_id+'">'+storeList[i].store_name+'</option>';
			   }
			   $("#store_id").html(htl);
			}
			$('#pos_name').val('');
			$("#myModalLabel").html('pos机新增');
			var delete_flag =0;
			$("#delete_flag option").each(function(index,element){
				var this_obj = $(element);              
				if(this_obj.val() == delete_flag){					
					this_obj.attr("selected",true);
				}
			});
           	$('#myModal').modal('show');
		});
	},

	delStoreList: function(){//删除
	  
	},

	editStoreList: function(pos_id){//编辑
		var tr = $("#tr_"+pos_id);
		var pos_name  = tr.find('.pos_name').text();
		var delete_flag = tr.find('.delete_flag').attr('status');
		$.post('check-edit-pos',{'pos_id':pos_id},function(data){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var posList = data.posList;
			var storeList    = data.store;
            $("#myModalLabel").html('pos机修改');
			$('#pos_name').val(posList.pos_name);
			$("#pos_save").attr('data-id',posList.pos_id);

			if(storeList.length>0){
			   var htl = '<option value="0">请选择门店</option>';
			   for(var i in storeList){
				   if(storeList[i].store_id == posList.store_id){
				      htl += '<option value="'+storeList[i].store_id+'" selected>'+storeList[i].store_name+'</option>';
				   }else{
				      htl += '<option value="'+storeList[i].store_id+'">'+storeList[i].store_name+'</option>';
				   }
			   }
			   $("#store_id").html(htl);
			}

            var html = '<option value="-1">请选择</option>';
            switch(posList.delete_flag){
			   case '0':
				   html += '<option value="0" selected>正常</option><option value="1">屏蔽</option>';
			       break;
			   case '1':
				   html += '<option value="0" >正常</option><option value="1" selected>屏蔽</option>';
			       break;
			}
            $("#delete_flag").html(html);
			$('#myModal').modal('show');
		});
	},

	saveStorList: function(_this){//保存
	    var pos_name = $('#pos_name').val();
		if(pos_name.length == 0){
			alertTips('warning','请输入仓库名');
			return false;
		}
		var pos_id      = $(_this).attr('data-id');
		var store_id    = $("#store_id").val();
		var delete_flag = $('#delete_flag').val();
		
		loading();
		$.post('save-store-pos',{'pos_id':pos_id,'pos_name':pos_name,'store_id':store_id,'delete_flag':delete_flag},function(data){
			removeloading();
			if(data.Ack ==='Success'){
				alertTips('success','保存成功');
				setTimeout(function(){ 
					$('#myModal').modal('hide');
					window.location.reload();
				},1000);
			} else if(data.Error =="pos_name is exists"){
				alertTips('warning','该pos机已经存在');
				return false;
			}else {
				alertTips('error','系统繁忙...');
				return false;
			}
	   })	
	},

	changeFilter: function(){//过滤
	   storePosList.filter = {
	      'delete_flag': $("#filterClient").val()
	   };
	   storePosList.page = 1;
	   storePosList.getStoreList();
	},

	selectAll: function(){//选中
	    if($('.checkAll').prop('checked')){
			$('.ids').prop('checked',true);
		} else {
			$('.ids').prop('checked',false);
		}
	},

	searchStoreList: function(){//查询门店
	    var pos_name = $("#sname").val();
		storePosList.page = 1;
        storePosList.searchData = {
		   'pos_name' : pos_name
		};
		storePosList.getStoreList();
	}
};