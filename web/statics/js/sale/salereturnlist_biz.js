$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//出库退货单列表
		listSaleReturn();
		function listSaleReturn(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#salereturnlist').find('tr').remove();
			$.get('get-sale-return',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].salereturn_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].salereturn_id+'">'+W[i].salereturn_no+'</td>'
								+'<td class="text-center salereturn_date">'+intToLocalDate(W[i].salereturn_date,1)+'</td>'
								+'<td class="text-center no show-pop-table">'+W[i].origin_type+'</td>'
								+'<td class="text-center customer">'+W[i].client_name+'</td>'
								+'<td class="text-center warehouse">'+W[i].warehouse_name+'</td>'
								+'<td class="text-center salereturn_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].remark+'</td>'
								+'<td class="text-center"><a href="javascript:void(0)" class="salereturn_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
								+(+W[i].confirm_flag ? '<td class="text-center" data-id="'+W[i].confirm_flag+'"><a href="javascript:void(0)" class="btn btn-warning"  style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm" data-id="'+W[i].confirm_flag+'"><a class="btn btn-success">确认</a></td>')
								+'</tr>').appendTo('#salereturnlist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSaleReturn);
				                }
							};
						}else {
							$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#salereturnlist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleReturn);
						}
					} else {
						$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#salereturnlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleReturn);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
		//查询
		$('#search_list').on('click',function(){
			var starTime = $('#starTime').val();
			var endTime = $('#endTime').val();
			global_info.page =1;
			if(starTime > endTime){
				alertTips('warning','开始时间不能大于截至时间！');
				return false;
			}
			global_info.cond ={
				'starTime': starTime,
				'endTime': endTime,
				'warehouse':$('#stock').val(),
			};
			
			listSaleReturn();
		});
		
		//编辑
		$('#salereturnlist').on('click','.salereturn_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			//检查是否有编辑其他入库功能
			$.get('check-edit-sale-return',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				
				window.location.href = "/sale/edit-sale-return?id="+ id;
			})
		})
		
			//过滤
		$('#filterInventory').on('change',function(){
			global_info.filter = $(this).val();
			global_info.page =1;
			listSaleReturn();
		})
		
		
	//删除
	$('#delete').on('click',function(){
		//检查是否有删除功能
		$.get('check-del-sale-return',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var _all_ids='';
			var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
			$('.ids').each(function(i){
				tmp_count +=i;
				if($(this).prop('checked')){
					_all_ids += $(this).attr('data-id') + ',';
					
				}
			})
	
			_all_ids = _all_ids.substr(0,_all_ids.length -1);
			if (_all_ids.length === 0) {
				alertTips('warning','请勾选复选框！');
				return false;
			} 
			if(tmp_count == 0){
				if(+global_info.page > 1){
					global_info.page --;
				}
			}
			
			layer.confirm('是否删除退货单？',function(index){
				$.get('del-sale-return',{'ids':_all_ids}, function(data,status){
					if(status =='success' && data.Ack =='Success'){
						alertTips('success','删除成功！');
						setTimeout(function(){ 
							listSaleReturn();
							return false;
						},1500);
					}else {
						alertTips('error','删除失败！');
						return false;
					}
				})
				layer.close(index);
			})
		})
	})
	
	    //确认
	$('#salereturnlist').on('click','.confirm',function(){
		var tr = $(this).closest('tr');
		//检查是否有确认其他入库功能
		$.get('check-confirm-sale-return',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			var id = tr.find('[name="ids"] input').attr('data-id');
			layer.confirm('点击确认退货单将无法编辑，是否确认?', function(index) {
				$.get('confirm-sale-return',{'id':id},function(data,status){
					if(status =='success'){
						if(data.Ack =='Success'){
							alertTips('success','确认成功');
							setTimeout(function(){
								window.location.reload();
							},1000);
						} else if(data.Error =='total quantity is lt shipped_quantity'){
							alertTips('error','出库数量大于订单数量,请修改！');
							return false;
						}
					} else{
						alertTips('error','网络错误');
					}
				})
				layer.close(index);
			});
		})
	})
	
	/**
	 * @desc 点击单号查询商品明细
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	
	var settings = {
		trigger:'hover',
		title:'商品信息 ',
		content:'<p>没有数据</p>',
		width:320,						
		multi:false,						
		closeable:false,
		style:'',
//		delay:300,
	    cache:true,
		padding:true,
		autoHide:10,
	};
	
	function initPopover(){					
		$('#salereturnlist').on('click','.show-pop-table',function(){
			var THIS =$(this);
			var iindex = layer.load();
			$.get('get-sale-return-det',{'id':$(this).attr('data-id')},function(data,status){
				THIS.webuiPopover('destroy');
				if(status == 'success'){
					if(data.Ack =='Success'){
						$('#webuiList').empty();
						var M = data.Body;
						var _table ='';
						for(var i in M){
							 $('<tr><td data-id="'+M[i].salereturn_det_id+'">'+(+i+1)+'</td>'
								 +'<td>'+M[i].product_id+'</td>'
								 +'<td>'+M[i].product_name+'</td>'
								 +'<td>'+M[i].product_sn+'</td>'
								 +'<td>'+M[i].unit_name+'</td>'
								 +'<td>'+M[i].quantity+'</td>'
								 +'<td>'+M[i].price+'</td>'
								 +'<td>'+M[i].amount+'</td>'
								 +'</tr>').appendTo('#webuiList');
						}
						
					}
					layer.close(iindex);
					
					var tableContent = $('#tableContent').html();
					tableSettings = {content:tableContent,
										width:800
									};
					THIS.webuiPopover($.extend({},settings,tableSettings));
				} else {
					alertTips('error','网络错误');
				}
			})
//			$(this).webuiPopover($.extend({},settings,tableSettings));
//			$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
		})
	}
	
	initPopover();
})