$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
//			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//采购订单列表
		listQuotes();
		function listQuotes(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#quoteslist').find('tr').remove();
			$.get('get-quotes',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].quotes_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].quotes_id+'">'+W[i].quotes_no+'</td>'
								+'<td class="text-center quotes_time">'+intToLocalDate(W[i].quotes_time,1)+'</td>'
								+'<td class="text-center vendor">'+W[i].customer+'</td>'
								+'<td class="text-center quotes_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].remark+'</td>'
								+(+W[i].finish_flag ? '<td class="text-center"><a href="javascript:void(0)" class="" style="padding: 6px 8px;" disabled>完成</a></td>':'<td class="text-center"><a class="">未完成</a></td>')
								+'<td class="text-center"><a href="javascript:void(0)" class="quotes_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
//								+(+W[i].confirm_flag ? '<td class="text-center"><a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>')
//								+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
								+'</tr>').appendTo('#quoteslist');
				                var pageInfo = data.Body.page;
				                if (typeof pageInfo !== 'undefined'){
				                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listQuotes);
				                }
							}
						}else {
							$('<tr><td colspan="10" class="text-center">没有数据</td></tr>').appendTo('#quoteslist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listQuotes);
						}
					} else {
						$('<tr><td colspan="10" class="text-center">没有数据</td></tr>').appendTo('#quoteslist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listQuotes);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
		//查询
		$('#search_list').on('click',function(){
			var starTime = $('#starTime').val();
			var endTime = $('#endTime').val();
			if(starTime > endTime){
				alertTips('warning','开始时间不能大于截至时间！');
				return false;
			}
			global_info.cond ={
				'starTime': starTime,
				'endTime': endTime,
				'customer':$('#cust').val(),
			};
			
			listQuotes();
		});	
		
		//编辑
	   $('#quoteslist').on('click','.quotes_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			//检查是否有编辑采购单功能
			$.get('check-edit-quotes',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
			
				window.location.href = "/sale/edit-quotes?id="+ id;
			})
	   })
	   
	 //删除
	  $('#delete').on('click',function(){
			//检查是否有删除采购单功能
			$.get('check-del-quotes',function(data,status){
				if(data.Error =='User authentication fails'){
					alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
					return;
				}
				var _all_ids='';
				var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
				$('.ids').each(function(i){
					tmp_count +=i;
					if($(this).prop('checked')){
						_all_ids += $(this).attr('data-id') + ',';
					}
				})
		
				_all_ids = _all_ids.substr(0,_all_ids.length -1);
				if (_all_ids.length === 0) {
					alertTips('warning','请勾选复选框！');
					return false;
				} 
				if(tmp_count == 0){
					if(+global_info.page > 1){
						global_info.page --;
					}
				}
				layer.confirm('是否删除报价单？',function(index){
					$.get('del-quotes',{'ids':_all_ids}, function(data,status){
						if(status =='success' && data.Ack =='Success'){
							alertTips('success','删除成功！');
							setTimeout(function(){ 
								listQuotes();
								return false;
							},1500);
						}else if(data.Error =='finish order can not be delete'){
							alertTips('error','已完成单据不能删除！');
							return false;
						} else {
							alertTips('error','删除失败！');
							return false;
						}
					})
					layer.close(index);
				})
			})
		})
		
})