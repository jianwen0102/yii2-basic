/**
 * @desc 采购报价单
 */
$(function(){
	var _id = parseInt($_GET['id']);
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
	}
	var global_count = 1;
	var global_remove = [];
	var select_units;
	//时间
	dateSelFun($('#quotes_time'));
	dateSelFun($('#receive_date'));
	
	(function(){
//		select_units = '<select class="form-control units"><option value="0">请选择单位..</option>';
//		loading()
//		$.get('list-units', function(data, status){
//			removeloading();
//			if(status =='success' && data.Ack =='Success'){
//				var U = data.Body;
//				for(var i in U){
//					select_units +='<option value="'+U[i].unit_id+'">'+U[i].unit_name+'</option>';
//				}
//			}
//			select_units +='</select>';
//		});
		select_units = '<select title="单位" class="form-control unit">'
				+ '<option value="0">单位</option>'
				+ '<option value="6">个</option>'
				+ '<option value="5">支</option>'
				+ '<option value="4">包</option>'
				+ '<option value="2">斤</option>'
				+ '<option value="1">箱</option>' + '</select>';
		if(_id){
			initProcurement();
		}
	
	})();
	
	/**
	 * @desc 初始化采购单页面
	 */
	function initProcurement(){
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-quotes-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#quotes_no').val(H.quotes_no);
					$('#supplier_id').val(H.vendor_id);
					$('#quotes_time').val(intToLocalDate(H.quotes_time,1));
//					$('#warehouse').val(H.warehouse_id);
					$('#handle_man').val(H.quotes_man);
					$('#customer').val(H.customer);
					$('#receive_date').val(intToLocalDate(H.receive_date,1));
					$('#total_amount').val(H.total_amount);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					$('#quotes_amount').val(H.quotes_amount);
					$('#total_discount').val(H.total_discount);
					$('#discount_amount').val(H.discount_amount);
					if(+H.finish_flag){
						$('#save').attr('disabled','disabled');
					}
				}
				var D = data.Body.det;
				if(D){
					$('#quotesList').find('tr').remove();
					for(var i in D){
						var tmp_units= '';
						var U = D[i].product_unit;
						tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
						for(var j in U){
							tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
						}
						tmp_units +='</select>';
						$('<tr>'
						   +'<td class="text-center" name="ids" data-id="'+D[i].quotes_det_id+'">'+(+i+1)+'</td>'
						   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].product_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
						   +'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
						   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
						   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
						   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
						   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
						   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
						   +'<td class="text-center" name="discount"><input type="text"  placeholder="0.00" class="discount" value="'+D[i].discount+'"></td>'
						   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
						   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
//						   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
						   +'</tr>').appendTo('#quotesList');
//						$('#quotesList').find('tr:last').find('.units').val(D[i].quantity_unit).attr('disabled','disabled');
						$('#quotesList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
						$('#quotesList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
						global_count++;
					}
				}
			}
		});
	};
	
	// 新增仓库
	$('#addWarehouse').on('click',function(){
		$('#myWarehouse').modal('show');
	})
	//仓库
	$('#warehouse_save').on('click', function() {
		$warehouse = $('#warehouse_name').val();
		if ($warehouse.length == 0) {
			alertTips('warning','请输入仓库名')
			return false;
		}
		var pflag = 0;
		$remark = $('#remark').val();
		$default = $('#default').prop('checked');
		$wid = $('#warehouse_save').attr('data-wid');
		var ii = layer.load();
		$.get('../instore/save-warehouse', {
			'name' : $warehouse,
			'remark' : $remark,
			'def' : $default,
			'wid' : $wid
		}, function(data, status) {
			layer.close(ii);
			if (status === 'success' && data.Ack == 'Success') {
				alertTips('success','保存成功');
				setTimeout(function() {
					$('#myWarehouse').modal('hide');
				}, 1000);
			} else if (data.Error == "warehouse is exists") {
				alertTips('warning','该仓库已经存在，请更改仓库名称');
				return false;
			} else {
				alertTips('error','网络错误');
				return false;
			}
		})
	});
	
	/**
	 * @desc 选择商品
	 */
	$('#quotes_detail').on('click','.search-product',function(){
		$('#productName').val('');
		getProducts();
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		getProducts();
		return;
	})
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		$('#product_list').find('tr').remove();
		var index = layer.load();
		 $.ajax({
             url: '../instore/get-products',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 var P = data.Body.list;
					if(P && P.length){
						for(var i in P){
							var tmp_units = ''
								var U = P[i].product_unit;
								tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
								for(var j in U){
									tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
							}
							tmp_units +='</select>';
							$('<tr>'
							+'<td class="text-center">'+(+i+1)+'</td>'
							+'<td class="text-center id">'+P[i].product_id+'</td>'
							+'<td class="text-center name">'+P[i].product_name+'</td>'
							+'<td class="text-center sn">'+P[i].product_sn+'</td>'
							+'<td class="text-center quantity">'+P[i].quantity+'</td>'
							+'<td class="text-center price">'+P[i].price+'</td>'
							+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'" data-rela="'+P[i].unit_content+'">'+(P[i].unit_name == null ? '':P[i].unit_name)+'</td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="product_select" data-unit="'+encodeURI(tmp_units)+'">选择</a></td>'
							+'</tr>').appendTo('#product_list');
			              
						}
						var pageInfo = data.Body.page;
			            if (typeof pageInfo !== 'undefined'){
			              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
			            }
					}else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#product_list');
						refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
					}
				$('#chooseProduct').modal('show');//选择商品窗口
             }
		 })
	}
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var tmp_units = decodeURI($(this).attr('data-unit'));
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var sn = tr.find('.sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		if(!+unit){
			alertTips('warning','请在商品管理添加单位');
			return false;
		}
		var unit_name = tr.find('.unit').text();
		var price = tr.find('.price').text();
		
		if(global_count == 1){
			var pid = $('#quotesList').find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				$('#quotesList').find('tr:first').remove();
			} ;
		}
//		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="sn">'+sn+'</td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity"></td>'
		  +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+price+'"></td>'
		  +'<td class="text-center" name="discount"><input type="text"  placeholder="0.00" class="discount" value="0.00"></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount"></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo('#quotesList');
		$('#quotesList').find('tr:last').find('[name="unit"]').find('.units').val(unit);
		global_count++;
	})
	
	//删除数据
	$('#quotesList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'quotes_det_id':tmp_id});
		}
		if(global_count == 1){
			return false;
		} else if(global_count == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#quotesList').find('tr:first').remove();
			global_count--;
			insertOrginHtml();
			SumDiscount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#quotesList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_count--;
                SumDiscount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_count--;
                SumDiscount();
                return false;
            }
		}
		
	});
	/**
	 * @var 计算增减明细后总金额
	 */
	var SumDiscount = function(){
		var total_amount = 0;
		var total_discount = 0;
		$('#quotesList tr').each(function(index,element){
			var _amount = $(element).find('[name="amount"] input').val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			var _discount = $(element).find('[name="discount"] input').val();
			if(_discount == ''){
				
			}else {
				total_discount += +_discount; 
			}
		})
		var discount_amount = +$('#discount_amount').val();
		$('#total_amount').val(total_amount);
		$('#quotes_amount').val(total_amount - discount_amount);
		$('#total_discount').val(total_discount);
	};
	
	var insertOrginHtml = function(){
		$('<tr>'
				  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
				  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
				  +'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
				  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
				  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
				  +'<td class="text-center" name="sn"></td>'
				  +'<td class="text-center" name="quantity"><input type="text"></td>'
				  +'<td class="text-center" name="unit">'+select_units+'</td>'
				  +'<td class="text-center" name="price"><input type="text"></td>'
				  +'<td class="text-center" name="amount"><input type="text"></td>'
				  +'<td class="text-center" name="discount"><input type="text"></td>'
				  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
//				  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock">库存</a></td>'
				  +'</tr>').appendTo('#quotesList');
	}
	
	//库存提示
	$('#quotesList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//名称提示
	$('#quotesList').on('click','.product_name',function(){
		var name = $(this).val();
		if(name !=undefined){
			layer.tips(name, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//数量验证 计算价格金额
	$('#quotesList').on('keyup','.quantity',function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _quantity = $(this).val();
		var _price = $(this).closest('tr').find('.price').val()
		if(_price == ''){
			_price = 0
		}
		var _discount = $(this).closest('tr').find('.discount').val();
		if(_discount == ''){
			_discount = 0
		}
		var _amount = (_quantity * _price).toFixed(2) - _discount;
		$(this).closest('tr').find('.amount').val(_amount);
//		$(this).closest('tr').find('.minus_quantity').val(_quantity);//用基本数量记录
		SumAmount();
		
	});
	
	//单位验证 计算价格金额
	$("#quotesList").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').find('[name="quantity"] input').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _price = $(this).val();
		var discount = +$(this).closest('tr').find('.discount').val();
		if(discount == ''){
			discount = 0
		}
		var _amount = (_quantity * _price).toFixed(2) - discount;
		$(this).closest('tr').find('.amount').val(_amount);
		SumAmount();
	});
	//金额验证 计算价格金额
	$('#quotesList').on('keyup','.amount', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var _quantity = $(this).closest('tr').find('.quantity').val();
		var _amount = +$(this).val();
//		var _price = (_amount / _quantity).toFixed(2);
		
		var discount = +$(this).closest('tr').find('.discount').val();
		if(discount == ''){
			discount = 0
		}
		var _price = ((_amount + discount )/ _quantity).toFixed(2);
		$(this).closest('tr').find('.price').val(_price);
		SumAmount();
	});
	
	$('#quotesList').on('keyup','.discount', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _quantity = $(this).closest('tr').find('.quantity').val();
		var _price = $(this).closest('tr').find('.price').val();
		
		var discount = $(this).closest('tr').find('.discount').val();
		var _amount = (_quantity * _price).toFixed(2) - discount;
		
		$(this).closest('tr').find('.amount').val(_amount);
		SumAmount();
		
		//整单折扣金额
		var total_discount = 0;
		$('#quotesList tr').find('.discount').each(function(index,element){
			var _discount = $(element).val();
			if(_discount ==''){
				
			} else {
				total_discount += +_discount;
			}
			
		})
		$('#total_discount').val(total_discount);
	});
	
	/**
	 * @var 计算总金额
	 */
	var SumAmount = function(){
		var total_amount = 0;
		$('#quotesList tr').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			
		})
		var discount_amount = +$('#discount_amount').val();
		$('#total_amount').val(total_amount);
		$('#quotes_amount').val(total_amount - discount_amount);
	};
	
	//整单优化验证 计算价格金额
	$('#discount_amount').on('keyup', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var discount_amount = +$(this).val();
		var total_amount = +$('#total_amount').val();
		if(total_amount == ''){
			total_amount = 0
		}
		var amount = (total_amount - discount_amount).toFixed(2);
		$('#quotes_amount').val(amount);
	});
	
	
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = quotes_header();
			if(!head){
				return false;
			}
			//明细
			var detail = quotes_detail();
			if(!detail){
				return false;
			}

			var _id = $(this).attr('data-id');
			var ii = layer.load();
			$.post('save-quotes',{'head':head, 'det':detail, 'remove': global_remove, 'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
 							window.location.reload();
						}, 1000);
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var quotes_header = function() {
		var no = $('#quotes_no').val();
		var quotes_time = $('#quotes_time').val();
		if (quotes_time.length == 0) {
			alertTips('warning', '请输入报价时间');
			return false;
		}
		var handle_man = $('#handle_man').val();
		if (!+handle_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		var customer = $('#customer').val();
		if (customer.length == 0) {
			alertTips('warning', '请填入客户');
			return false;
		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var quotes_head = {
			'quotes_no' : no,
			'quotes_time' : quotes_time,
			'quotes_man' : handle_man,
			'customer' : customer,
			'create_man' : create_man,
			'remark' : remark,
			'total_amount':$('#total_amount').val(),
			'quotes_amount':$('#quotes_amount').val(),
			'total_discount': $('#total_discount').val(),
			'discount_amount':$('#discount_amount').val(),
		};
		return quotes_head;
	}
	
	//采购单明细
	var quotes_detail = function(){
		var product_det=[];
		var product_info = [];
		var _tr = $('#quotesList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			if(price == '' || price == 0){
				if(!+price){
					if(confirm("商品单价为0,是否继续？")){
						price = 0;
						amount = 0;
					} else {
						flag = 0;
						return false;
					}
				}
			}
			
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			product_det = {
				'quotes_det_id' : line_id,
				'product_id' : tr.find('[name="name"]').attr('data-id'),
				'quantity' : quantity,
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
				'discount' : tr.find('[name="discount"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		return product_info;
	}
	
	
})