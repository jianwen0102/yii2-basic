$(function(){
	
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
		}
	
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	global_info.cond ={
			'starTime': $('#starTime').val(),
			'endTime': $('#endTime').val(),
	};
	
	listSaleoutFree();
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		if (starTime.length == 0) {
			alertTips('warning', '请输入查询时间');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'customer':$('#customer').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		
		listSaleoutFree();
	});
	
	function listSaleoutFree(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#saleFreeList').find('tr').remove();
		$.get('get-sale-out-free',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						for(var i in W){
							var left_amount = +W[i].left_amount?W[i].amount:0;
							var rece_quantity = +W[i].left_amount? 0 : W[i].base_quantity;
							var rece_amount = +W[i].left_amount?0:W[i].amount;
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center no">'+W[i].saleout_no+'</td>'
							+'<td class="text-center pay_time">'+intToLocalDate(W[i].saleout_date,1)+'</td>'
							+'<td class="text-center customer">'+W[i].client_name+'</td>'
							+'<td class="text-center product">'+W[i].product_name+'</td>'
							+'<td class="text-center unit">'+W[i].unit_name+'</td>'
							+'<td class="text-center quantity">'+W[i].base_quantity+'</td>'
							+'<td class="text-center price">'+W[i].price+'</td>'
							+'<td class="text-center amount">'+((W[i].price * W[i].base_quantity).toFixed(2))+'</td>'
							+'<td class="text-center remark">'+W[i].remark+'</td>'
//							+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
							+'</tr>').appendTo('#saleFreeList');
				       var pageInfo = data.Body.page;
				       if (typeof pageInfo !== 'undefined'){
				            refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSaleoutFree);
				       }
					  }
				}else {
					$('<tr><td colspan="10" class="text-center">没有数据</td></tr>').appendTo('#saleFreeList');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleoutFree);
					}
				} else {
					$('<tr><td colspan="10" class="text-center">没有数据</td></tr>').appendTo('#saleFreeList');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSaleoutFree);
				}
		} else{
			alertTips('error','网络错误');
		}
	  });
	};
	
	//导出execl
	$('#export').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		if (starTime.length == 0) {
			alertTips('warning', '请输入查询时间');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'customer':$('#customer').val(),
			'name' : $('#name').val(),
			'id':parseInt($('#id').val())?parseInt($('#id').val()):0,
		};
		
		var flag = 0;
		$.get('check-sale-out-free-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag = 1;
				return;
			}
			$url ='export-sale-out-free?starTime='+global_info.cond.starTime+'&endTime='+global_info.cond.endTime+'&customer='+global_info.cond.customer+
				'&id='+global_info.cond.id+'&name='+global_info.cond.name;
			if(!flag){
				window.location.href = $url;
			}
		})
	})
	
})