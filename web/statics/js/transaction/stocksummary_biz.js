$(function(){
	
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
//			'filter':undefined//过滤条件
		}
	//时间
	console.log(121);
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	
	
	//获取出入库明细列表
	function listSummary(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		console.log(121);
		$('#stocksummarylist').find('tr').remove();
		$.get('get-stock-summary',global_info,function(data,status){
			console.log(12111);
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var T = data.Body.list;
					console.log(T);
					if(T && T.length){
						for(var i in T){
							var bad_rate = +T[i].bad_quantity/(+T[i].init_num + +T[i].in_quantity);
							console.log(bad_rate);
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center" name="pid">'+T[i].goods_id+'</td>'
							+'<td class="text-center" name="name">'+T[i].product_name+'</td>'
							+'<td class="text-center" name="init_num">'+T[i].init_num+'</td>'
							+'<td class="text-center" name="in_quantity">'+T[i].in_quantity+'</td>'
							+'<td class="text-center" name="out_quantity">'+T[i].out_quantity+'</td>'
							+'<td class="text-center" name="eat_quantity">'+T[i].eat_quantity+'</td>'
							+'<td class="text-center" name="bad_quantity">'+T[i].bad_quantity+'</td>'
							+'<td class="text-center" name="bad_rate">'+bad_rate.toFixed(2)+'</td>'
							+'<td class="text-center" name="product_qty">'+T[i].quantity+'</td>'
//							+'<td class="text-center" name="minus">'+ ''+'</td>'
							+'</tr>').appendTo('#stocksummarylist');
				       var pageInfo = data.Body.page;
				       if (typeof pageInfo !== 'undefined'){
				            refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listSummary);
				       }
					  }
				}else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#stocksummarylist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSummary);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#stocksummarylist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listSummary);
				}
		} else{
			alertTips('error','网络错误');
		}
	  });
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
//			'vendor':$('#vendor').val(),
//			'warehouse':$('#warehouse').val(),
//			'customer' : $('#customer').val(),
			'name' : $('#name').val(),
		};
		global_info.page = 1;
		global_info.pageSize =10;
		listSummary();
	});
})