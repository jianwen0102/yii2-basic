$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
//			'filter':undefined//过滤条件
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));
	
	listTrans();
	//获取出入库明细列表
	function listTrans(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#transactionlist').find('tr').remove();
		$.get('get-transactions',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var T = data.Body.list;
					if(T && T.length){
						for(var i in T){
							$('<tr><td class="text-center" name="ids">'+(+i+1)+'</td>'
							+'<td class="text-center" name="origin_id">'+T[i].origin_id+'</td>'
							+'<td class="text-center" name="origin_type">'+T[i].type+'</td>'
							+'<td class="text-center" name="flag">'+(+T[i].flag ? '入库':'出库')+'</td>'
							+'<td class="text-center" name="date">'+intToLocalDate(T[i].origin_time,1)+'</td>'
							+'<td class="text-center" name="pid">'+T[i].goods_id+'</td>'
							+'<td class="text-center" name="name">'+T[i].product_name+'</td>'
							+'<td class="text-center" name="warehouse">'+T[i].warehouse_name+'</td>'
							+'<td class="text-center" name="vendor">'+(T[i].supplier_name == null ? '':T[i].supplier_name)+'</td>'
							+'<td class="text-center" name="in_quantity">'+T[i].in_quantity+'</td>'
							+'<td class="text-center" name="out_quantity">'+T[i].out_quantity+'</td>'
							+'<td class="text-center" name="minus">'+T[i].minus+'</td>'
							+'</tr>').appendTo('#transactionlist');
				       var pageInfo = data.Body.page;
				       if (typeof pageInfo !== 'undefined'){
				            refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listTrans);
				       }
					  }
				}else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#transactionlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listTrans);
					}
				} else {
					$('<tr><td colspan="12" class="text-center">没有数据</td></tr>').appendTo('#transactionlist');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listTrans);
				}
		} else{
			alertTips('error','网络错误');
		}
	  });
	};
	
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'warehouse':$('#warehouse').val(),
			'customer' : $('#customer').val(),
			'name' : $('#name').val(),
		};
		global_info.page = 1;
		global_info.pageSize =10;
		listTrans();
	});
})