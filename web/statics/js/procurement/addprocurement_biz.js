$(function(){
	var _id = parseInt($_GET['id']?$_GET['id']:0);
	/**
	 * @desc 商品信息
	 */
	var global_product = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'name': undefined,//商品名称
	}
	
	
	var global_sell = {
			'page' : undefined,// 页码
			'pageSize' :  undefined, // 页数
			'cond':[],//查询条件
//			'name' : undefined,// 商品名称
	}
	var global_count = 1;
	var global_remove = [];
	var select_units;
	var _COUNT = 1;
	//时间
	dateSelFun($('#pro_time'));
	
	/**
	 * @desc 单位
	 * 
	 */
	(function(){
//		select_units = '<select class="form-control units"><option value="0">请选择单位..</option>';
//		loading()
//		$.get('list-units', function(data, status){
//			removeloading();
//			if(status =='success' && data.Ack =='Success'){
//				var U = data.Body;
//				for(var i in U){
//					select_units +='<option value="'+U[i].unit_id+'">'+U[i].unit_name+'</option>';
//				}
//			}
//			select_units +='</select>';
//		});
		select_units = '<select title="单位" class="form-control unit">'
				+ '<option value="0">单位</option>'
				+ '<option value="6">个</option>'
				+ '<option value="5">支</option>'
				+ '<option value="4">包</option>'
				+ '<option value="2">斤</option>'
				+ '<option value="1">箱</option>' + '</select>';
		if(_id){
			initProcurement();
		}
	
	})();
	

	
	/**
	 * @desc 初始化采购单页面
	 */
	function initProcurement(){
		$('#save').attr('data-id',parseInt(_id));
		$.get('get-procurement-info', {'id' : _id}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
					$('#pro_no').val(H.procurement_no);
					$('#supplier_id').val(H.vendor_id);
					$('#pro_time').val(intToLocalDate(H.procurement_date,1));
					$('#warehouse').val(H.warehouse_id);
					$('#handle_man').val(H.procurement_man);
//					$('#supplier_id').val(H.vendor_id);
					$('#procurement_header').find('[class="searchable-select-holder"]').attr('data-id',H.vendor_id);
					$('#procurement_header').find('[class="searchable-select-holder"]').text(H.vendor_name);
					$('#remark').val(H.remark);
					$('#create_man').val(H.username).attr('data-id', H.create_man);
					$('#procurement_amount').val(H.procurement_amount);
					if(+H.finish_flag){
						$('#save').attr('disabled','disabled');
					}
					$('#total_amount').val(H.total_amount);
					$('#freight').val(H.freight);
				}
				var D = data.Body.det;
				if(D){
					$('#procurementList').find('tr').remove();
					for(var i in D){
						var tmp_units= '';
						var U = D[i].product_unit;
						tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
						for(var j in U){
							tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
						}
						tmp_units +='</select>';
						$('<tr>'
						   +'<td class="text-center" name="ids" data-id="'+D[i].procurement_det_id+'" data-selldetid="'+D[i].sell_id+'" data-sellid="'+D[i].sell_det_id+'">'+(+i+1)+'</td>'
						   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].goods_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
						   +'<td class="text-center" name="name" data-id="'+D[i].goods_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
						   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
						   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
						   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
						   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
						   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
						   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
						   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
						   +'<td class="text-center" name="gross_quantity"><input type="text" value="'+D[i].gross_quantity+'" class="quantity" disabled></td>'
						   +'<td class="text-center" name="gross_unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value='+D[i].received_quantity+'></td>'
						   +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value='+D[i].minus_quantity+'></td>'
						   +'<td class="text-center" name="unit_relation" style="width:5%;">'+D[i].unit_content+'</td>'
						   +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
						   +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
						   +'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
						   +'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'" style="width:5%;">'+D[i].base_name+'</td>'
						   +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].base_quantity+'" disabled></td>'
						   +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="'+D[i].base_price+'" disabled></td>'
						   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
						   +'</tr>').appendTo('#procurementList');
//						$('#procurementList').find('tr:last').find('.units').val(D[i].quantity_unit).attr('disabled','disabled');
						$('#procurementList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
						$('#procurementList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
						$('#procurementList').find('tr:last').find('[name="gross_unit"] select').val(D[i].gross_unit).attr('disabled','disabled');
						global_count++;
					}
				}
			}
		});
	};
	
	// 新增仓库
	$('#addWarehouse').on('click',function(){
		$('#myWarehouse').modal('show');
	})
	//仓库
	$('#warehouse_save').on('click', function() {
		$warehouse = $('#warehouse_name').val();
		if ($warehouse.length == 0) {
			alertTips('warning','请输入仓库名')
			return false;
		}
		var pflag = 0;
		$remark = $('#remark').val();
		$default = $('#default').prop('checked');
		$wid = $('#warehouse_save').attr('data-wid');
		var ii = layer.load();
		$.get('../instore/save-warehouse', {
			'name' : $warehouse,
			'remark' : $remark,
			'def' : $default,
			'wid' : $wid
		}, function(data, status) {
			layer.close(ii);
			if (status === 'success' && data.Ack == 'Success') {
				alertTips('success','保存成功');
				setTimeout(function() {
					$('#myWarehouse').modal('hide');
				}, 1000);
			} else if (data.Error == "warehouse is exists") {
				alertTips('warning','该仓库已经存在，请更改仓库名称');
				return false;
			} else {
				alertTips('error','网络错误');
				return false;
			}
		})
	});
	
	
	
	
//	//添加供应商
//	$('#addSupplier').on('click',function(){
////		$('#myWarehouse').modal('show');
//		//新开窗口
//		window.open("/supplier/create-supplier");       
//	})
	
	/**
	 * @desc 选择商品
	 */
	$('#procurement_detail').on('click','.search-product',function(){
		$('#productName').val('');
		getProducts();
		
	})
	//过滤商品
	$('#searchProduct').on('click', function() {
		var _name = trim($('#productName').val());
		global_product.name = _name;
		getProducts();
		return;
	})
	
	/**
	 * @desc 查询商品
	 */
	function getProducts(pageInfo){
		if (pageInfo !== undefined) {
			global_product.page = pageInfo.page;
			global_product.pageSize = pageInfo.pageSize;
		}
		$('#product_list').find('tr').remove();
		var index = layer.load();
		 $.ajax({
             url: '../instore/get-products',
             type: "GET",
             cache: true,
             data: global_product,
             dataType: "json",
             jsonp: "callback",
             global:false,
             success: function(data){
            	 layer.close(index);
            	 var P = data.Body.list;
					if(P && P.length){
						for(var i in P){
							var tmp_units = ''
								var U = P[i].product_unit;
								tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
								for(var j in U){
									tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
							}
							tmp_units +='</select>';
							$('<tr>'
							+'<td class="text-center">'+(+i+1)+'</td>'
							+'<td class="text-center id">'+P[i].product_id+'</td>'
							+'<td class="text-center name">'+P[i].product_name+'</td>'
							+'<td class="text-center sn">'+P[i].product_sn+'</td>'
							+'<td class="text-center quantity">'+P[i].quantity+'</td>'
							+'<td class="text-center price" style="display:none;">'+P[i].price+'</td>'
							+'<td class="text-center unit" data-unit="'+P[i].quantity_unit+'" data-rela="'+P[i].unit_content+'">'+(P[i].unit_name == null ? '':P[i].unit_name)+'</td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="product_select" data-unit="'+encodeURI(tmp_units)+'">选择</a></td>'
							+'</tr>').appendTo('#product_list');
			              
						}
						var pageInfo = data.Body.page;
			            if (typeof pageInfo !== 'undefined'){
			              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getProducts);
			            }
					}else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#product_list');
						refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getProducts);
					}
				$('#chooseProduct').modal('show');//选择商品窗口
             }
		 })
	}
	
	//添加商品
	$('#product_list').on('click','.product_select',function(){
		var tmp_units = decodeURI($(this).attr('data-unit'));
		var tr =$(this).closest('tr');
		var id = tr.find('.id').text();
		var name = tr.find('.name').text();
		var sn = tr.find('.sn').text();
		var quantity = tr.find('.quantity').text();
		var unit = tr.find('.unit').data('unit');
		if(!+unit){
			alertTips('warning','请在商品管理添加单位');
			return false;
		}
		var unit_name = tr.find('.unit').text();
		var unit_content = tr.find('.unit').attr('data-rela');
		var price = tr.find('.price').text();
		
		if(global_count == 1){
			var pid = $('#procurementList').find('.stock-minus-tr').data['id'];
			if(pid == undefined){
				//刚开始添加数据
				$('#procurementList').find('tr:first').remove();
			} ;
		}
//		var units = '<select class="form-control" disabled><option selected value="'+unit+'">'+unit_name+'</option></select>';
		$('<tr>'
		  +'<td class="text-center" name="ids" data-id="">'+global_count+'</td>'
		  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
		  +'<td class="text-center" name="name" data-id="'+id+'"><input title="商品名称" readonly type="text" value="'+name+'"'
		  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
		  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
		  +'<td class="text-center" name="sn">'+sn+'</td>'
		  +'<td class="text-center" name="quantity"><input type="text" class="quantity"></td>'
		  +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+price+'"></td>'
		  +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount"></td>'
		  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
		  +'<td class="text-center" name="gross_quantity"><input type="text"  class="quantity" disabled></td>'
		  +'<td class="text-center" name="gross_unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value="0"></td>'
		  +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value="0"></td>'
		  +'<td class="text-center" name="unit_relation" style="width:8%;">'+unit_content+'</td>'
		  +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
		  +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0"></td>'
		  +'<td class="text-center" name="second_relation"></td>'
		  +'<td class="text-center" name="base_unit" data-id="'+unit+'" style="width:5%;">'+unit_name+'</td>'
		  +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0" disabled></td>'
		  +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
		  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+quantity+'">库存</a></td>'
		  +'</tr>').appendTo('#procurementList');
		$('#procurementList').find('tr:last').find('[name="unit"]').find('.units').val(unit);
		global_count++;
	})
	
	//删除数据
	$('#procurementList').on('click','.stock-minus-tr',function(){
		var tmp_id = $(this).closest('td').prev('td').attr('data-id');
		if ($.inArray(tmp_id, global_remove) >= 0) {
			return false;
		}
		if(tmp_id){
			global_remove.push({'procurement_det_id':tmp_id});
		}
		if(global_count == 1){
			return false;
		} else if(global_count == 2){
			//添加一条数据要删除数据 后插入一条空数据
			$('#procurementList').find('tr:first').remove();
			global_count--;
			insertOrginHtml();
			SumCheckAmount();
			return false;
		} else {
			var self = $(this);
			//判断当前要删除的行,下面是否还有记录
            var nextTr = self.parent().parent().next('tr');
            if(nextTr.length){
            	//存在下一行，清空当前行数据
                self.closest('tr').remove();
                // 序号重新排列
                var td = $('#procurementList').find('tr').find('td:first');
                $.each(td, function(index, item) {
                    var item = $(item);
                    item.text(index+1);
                })
                global_count--;
                SumCheckAmount();
                return false;
            } else{
            	//删除的为最后一行
                self.closest('tr').remove();
                global_count--;
                SumCheckAmount();
                return false;
            }
		}
		
	});
	
	/**
	 * @var 计算增减明细后总金额
	 */
	var SumCheckAmount = function(){
		var total_amount = 0;
		$('#procurementList tr').each(function(index,element){
			var _amount = $(element).find('[name="amount"] input').val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
		})
		$('#procurement_amount ').val(total_amount);
	};
	
	var insertOrginHtml = function(){
		$('<tr>'
				  +'<td class="text-center" name="ids" data-id="" data-selldetid="" data-sellid="">'+global_count+'</td>'
				  +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id=""> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
				  +'<td class="text-center" name="name" data-id=""><input title="商品名称" readonly type="text" value=""'
				  +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
				  +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
				  +'<td class="text-center" name="sn"></td>'
				  +'<td class="text-center" name="quantity"><input type="text"></td>'
				  +'<td class="text-center" name="unit">'+select_units+'</td>'
				  +'<td class="text-center" name="price"><input type="text"></td>'
				  +'<td class="text-center" name="amount"><input type="text"></td>'
				  +'<td class="text-center" name="remark"><input type="text" style="width:100%;"></td>'
				  +'<td class="text-center" name="gross_quantity"><input type="text" class="quantity" disabled></td>'
				  +'<td class="text-center" name="gross_unit" style="width:8%;">'+select_units+'</td>'
				  +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value="0"></td>'
				  +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value="0"></td>'
				  +'<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>'
				  +'<td class="text-center" name="second_unit">'+select_units+'</td>'
				  +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>'
				  +'<td class="text-center" name="second_relation"></td>'
				  +'<td class="text-center" name="base_unit"><input title="基本单位" type="text" value=""></td>'
				  +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="0.00" disabled></td>'
				  +'<td class="text-center" name="base_price"><input title="基本单价" class ="base_price" type="text" value="0.00" disabled></td>'
				  +'<td class="text-center"><a href="javascript:void(0)" class="checkStock">库存</a></td>'
				  +'</tr>').appendTo('#procurementList');
	}
	//库存提示
	$('#procurementList').on('click','.checkStock',function(){
		var _quantity = $(this).attr('data-quantity');
		if(_quantity !=undefined){
			layer.tips('库存: '+ _quantity, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	
	//名称提示
	$('#procurementList').on('click','.product_name',function(){
		var name = $(this).val();
		if(name !=undefined){
			layer.tips(name, $(this), {
				  tips: [4, '#78BA32']
			});
		}
	});
	//数量验证 计算价格金额
	$('#procurementList').on('keyup','.quantity',function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _quantity = $(this).val();
		
		var tr = $(this).closest('tr');
		var _unit = tr.find('[name="unit"] select').val();
		var base_unit = tr.find('[name="base_unit"]').attr('data-id');
		var _rate = tr.find('[name="unit"]').find('option:selected').attr('data-rate');
		if(_unit == base_unit){
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		} else {
			tr.find('[name="base_quantity"] input').val(_quantity * _rate);
		}
		tr.find('[name="minus_quantity"] input').val(_quantity * _rate);
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = _quantity * _rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(_quantity*_rate/second_rate)+' '+second_name + ' '+(_quantity*_rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		
		var _price = $(this).closest('tr').find('.price').val()
		if(_price == ''){
			_price = 0
		}
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
		if(_quantity){
			tr.find('[name="base_price"] input').val((_amount / (_quantity * _rate)).toFixed(2));
		}
//		$(this).closest('tr').find('.minus_quantity').val(_quantity);//用基本数量记录
		SumAmount();
		
	});
	
	//单位验证
	$('#procurementList').on('change','[name="unit"] select', function(){
		var unit = $(this).val();
		var rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		var quantity = tr.find('[name="quantity"] input').val();
		var second_rate = tr.find('[name="second_unit"]') .find('option:selected').attr('data-rate');
		if(+second_rate){
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		}
		tr.find('[name="base_quantity"] input').val(quantity * rate);
		tr.find('[name="minus_quantity"] input').val(quantity * rate);
		var _amount = parseFloat(tr.find('[name="amount"] input').val());
		if(quantity){
			tr.find('[name="base_price"] input').val((_amount / (quantity * rate)).toFixed(2));
		}
		
		
	})
	
	//辅助单位
	$('#procurementList').on('change','[name="second_unit"] select',function(){
		var second_unit = $(this).val();
		var second_rate = $(this).find('option:selected').attr('data-rate');
		var  tr = $(this).closest('tr');
		if(+second_unit){
			var quantity = tr.find('[name="quantity"] input').val();
			var rate = tr.find('[name="unit"]') .find('option:selected').attr('data-rate');
			var tmp_quantity = quantity * rate /second_rate;
			tr.find('[name="second_quantity"] input').val(tmp_quantity);
			
			var _unit_name = tr.find('[name="unit"]').find('option:selected').text();
			var second_name = tr.find('[name="second_unit"]').find('option:selected').text();
			
			var second_relation = Math.floor(quantity*rate/second_rate)+' '+second_name + ' '+(quantity*rate%second_rate)+_unit_name;
			tr.find('[name="second_relation"]').text(second_relation);
		} else {
			tr.find('[name="second_quantity"] input').val('');
			tr.find('[name="second_relation"]').text('');
		}
	})
	
	//单位验证 计算价格金额
	$("#procurementList").on('keyup','.price',function(){
		var _quantity = $(this).closest('tr').find('[name="quantity"] input').val();
		if(_quantity == ''){
			_quantity = 0;
		}
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		var _price = $(this).val();
		var _amount = (_quantity * _price).toFixed(2);
		$(this).closest('tr').find('.amount').val(_amount);
		if(_quantity){
			var _rate = +$(this).closest('tr').find('[name="unit"]').find('option:selected').attr('data-rate');
			$(this).closest('tr').find('.base_price').val((_amount/(_quantity * _rate)).toFixed(2));
		}
		SumAmount();
	});
	//金额验证 计算价格金额
	$('#procurementList').on('keyup','.amount', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var _quantity = $(this).closest('tr').find('.quantity').val();
		var tr = $(this).closest('tr');
		if(_quantity == ''){
			tr.find('.price').val(0);
		} else {
			var _amount = $(this).val();
			var rate = +tr.find('[name="unit"]').find('option:selected').attr('data-rate');
			var _price = (_amount / _quantity).toFixed(2);
			tr.find('.price').val(_price);
			tr.find('.base_price').val((_amount/(_quantity * rate)).toFixed(2));
		}
		SumAmount();
	});
	
	/**
	 * @var 计算总金额
	 */
	var SumAmount = function(){
		var total_amount = 0;
		$('#procurementList tr').find('.amount').each(function(index,element){
			var _amount = $(element).val();
			if(_amount ==''){
				
			} else {
				total_amount += +_amount;
			}
			
		})
		
		var freight = +$('#freight').val();
		$('#procurement_amount').val(total_amount + freight);
		$('#total_amount').val(total_amount);
	};
	
	$('#freight').on('keyup', function(){
		this.value=this.value.replace(/^(\-)*(\d+)\.(\d\d).*$/,'$1$2.$3');
		
		var _freight = +$(this).val();
		var _total_amount = +$('#total_amount').val();
		
		$('#procurement_amount').val(_total_amount + _freight);
		
	});
	
	(function() {
		// 保存
		$('#save').on('click', function() {
			//单头
			var head = pro_header();
			if(!head){
				return false;
			}
			//明细
			var detail = pro_detail();
			if(!detail){
				return false;
			}
			var _id = $(this).attr('data-id');
			var ii = layer.load();
			$.post('save-procurement',{'head':head, 'det':detail, 'remove': global_remove, 'id':_id}, function(data,status){
				setTimeout(function(){
					  layer.close(ii);
				}, 1000);
				if(status =='success'){
					if(data.Ack =='Success'){
						alertTips('success','保存成功');
						setTimeout(function() {
							if(_id){
								window.location.reload();
							} else {
								window.location.href = "list-procurement";
							}
						}, 1000);
					} else if(data.Error =='exits next order'){
						alertTips('error', '采购入库已引用，不能修改！');
						return false;
					}
				} else {
					alertShows('error','网络错误');
				}
			})
		});

	})();	

	var pro_header = function() {
		var no = $('#pro_no').val();
		var pro_time = $('#pro_time').val();
		if (pro_time.length == 0) {
			alertTips('warning', '请输入采购时间');
			return false;
		}
		var warehouse = $('#warehouse').val();
		if (!+warehouse) {
			alertTips('warning', '请选择入库仓库');
			return false;
		}
		var handle_man = $('#handle_man').val();
		if (!+handle_man) {
			alertTips('warning', '请选择经手人员');
			return false;
		}
		var vendor = $('#supplier_id').val();
		if (!+vendor) {
			alertTips('warning', '请输入供应商');
			return false;
		}
		var remark = $('#remark').val();
		var create_man = $('#create_man').attr('data-id');
		var pro_head = {
			'procurement_no' : no,
			'procurement_date' : pro_time,
			'warehouse_id' : warehouse,
			'procurement_man' : handle_man,
			'vendor_id' : vendor,
			'create_man' : create_man,
			'remark' : remark,
			'procurement_amount':$('#procurement_amount').val(),
			'total_amount' :$('#total_amount').val(),
			'freight':$('#freight').val(),
		};
		return pro_head;
	}
	//采购单明细
	var pro_detail = function(){
		var product_det=[];
		var product_info = [];
		var _tr = $('#procurementList').find('tr');
		var flag = 1;
		if(_tr.length == 1){
			var pid = _tr.first('tr').find('td[name="name"]').attr('data-id');
			if(pid == ''){
				alertTips('warning','请选择商品');
				return false;
			}
		}
		_tr.each(function(index,element){
			var tr = $(this);
			var quantity = tr.find('[name="quantity"] input').val();
			if(quantity == ''){
				alertTips('warning','请填写数量');
				flag = 0;
				return false;
			}
			var line_id = tr.find('[name="ids"]').attr('data-id');
			if(line_id == ''){
				line_id = 0;
			}
			var amount = tr.find('[name="amount"] input').val();
			var price = tr.find('[name="price"] input').val();
			if(price == '' || price == 0){
				if(!+price){
					if(confirm("商品单价为0,是否继续？")){
						price = 0;
						amount = 0;
					} else {
						flag = 0;
						return false;
					}
				}
			}
			
			var unit = tr.find('[name="unit"] select').val();
			if(!+unit){
				alertTips('warning','单位不能为空');
				flag = 0;
				return false;
			}
			var base_quantity = tr.find('[name="base_quantity"] input').val();
			if(base_quantity == 0 || base_quantity == '' ){
				alertTips('warning','基本数量有错，请重新填写数量');
				flag = 0;
				return false;
			}
			var rec_quantity = tr.find('[name="rec_quantity"] input').val();
			if(base_quantity == '' ){
				alertTips('warning','已入数量有错，请联系程序员');
				flag = 0;
				return false;
			}
			var minus_quantity = tr.find('[name="minus_quantity"] input').val();
			if(minus_quantity == '' ){
				alertTips('warning','剩余数量有错，请联系程序员');
				flag = 0;
				return false;
			}
			product_det = {
				'procurement_det_id' : line_id,
				'goods_id' : tr.find('[name="name"]').attr('data-id'),
				'quantity' : quantity,
				'quantity_unit' : tr.find('[name="unit"] select').val(),
				'remark' : tr.find('[name="remark"] input').val(),
				'price' : decimal(price,2),
				'amount' : decimal(amount,2),
				'second_unit':tr.find('[name="second_unit"] select').val(),
				'second_quantity':tr.find('[name="second_quantity"] input').val(),
				'second_relation':tr.find('[name="second_relation"]').text(),
				'base_unit':tr.find('[name="base_unit"]').attr('data-id'),
				'base_quantity':base_quantity,
				'received_quantity': rec_quantity,
				'minus_quantity' : minus_quantity,
				'sell_id': tr.find('[name="ids"]').attr('data-sellid'),
				'sell_det_id':tr.find('[name="ids"]').attr('data-selldetid'),
				'base_price':tr.find('[name="base_price"] input').val(),
			};
			product_info.push(product_det);
			product_det = [];
		});
		if(!flag){
			product_info = [];
			return false;
		}
		if(product_info.length == 0){
			alertTips('warning','请选择产品！');
			return false;
		}
		return product_info;
	}
//	(function(){
//		$('#supplier_id').comboSelect();
//	})()
	
	$('#implode_order').on('click',function(){
		getSellOrder();
//		$('#chooseOrigin').modal('show');
	})
	
	$('#searchOrder').on('click', function(){
		global_sell.cond ={'cust':$('#cust_id').val()}
		global_sell.page = 1;
		global_sell.pageSize= 5;
		getSellOrder();
	})
	
	function getSellOrder(pageInfo)
	{
		if(pageInfo !== undefined){
			global_sell.page = pageInfo.page;
			global_sell.pageSize = pageInfo.pageSize;
		}
		$('#origin_header').find('tr').remove();
		var first_id = 0;
		var ii = layer.load();
		$.ajax({
            url: 'select-sell-order',
            type: "GET",
            cache: true,
            data: global_sell,
            dataType: "json",
            jsonp: "callback",
            global:false,
            success: function(data){
           	 layer.close(ii);
           	 var P = data.Body.list;
				if(P && P.length){
					for(var i in P){
						$('<tr>'
						+'<td class="text-center">'+(+i+1)+'</td>'
						+'<td class="text-center no" data-id="'+P[i].sell_id+'">'+P[i].sell_no+'</td>'
						+'<td class="text-center date">'+intToLocalDate(P[i].sell_date,1)+'</td>'
						+'<td class="text-center vendor">'+P[i].client_name+'</td>'
						+'<td class="text-center amount">'+P[i].sell_amount+'</td>'
						+'<td class="text-center remark">'+P[i].remark+'</td>'
						+'<td class="text-center"><a href="javascript:void(0)" class="sell_select">选择</a></td>'
						+'</tr>').appendTo('#origin_header');
					}
					first_id = P[0].sell_id;//记录第一个id
					var pageInfo = data.Body.page;
		            if (typeof pageInfo !== 'undefined'){
		              	refreshPaginationNavBar2($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, getSellOrder);
		            }
		            
				}else {
					$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#origin_header');
					refreshPaginationNavBar2($('.paginationNavBar'), 1, 10, 1, getSellOrder);
				}
				getSellDet(first_id);
				$('#chooseOrigin').modal('show');//选择订单窗口
					
            }
		})
		
	}
	
	/**
	 * @desc 根据采购订单id 查询明细
	 * @author liaojianwen
	 * @date 2016-12-15
	 */
	function getSellDet(fid)
	{
		if(fid){
			$('#origin_det').find('tr').remove();
			$.get('/sale/get-sell-detail', {'id':fid}, function(data,status){
				if(status =='success'){
					if(data.Ack =='Success'){
						var D = data.Body;
						for(var i in D){
							$('<tr>'
							 +'<td class="text-center"><input type="checkbox" class="ids" data-id="'+D[i].sell_det_id+'" data-pid="'+D[i].sell_id+'" checked></td>'
							 +'<td class="text-center">'+D[i].product_id+'</td>'
							 +'<td class="text-center">'+D[i].product_name+'</td>'
							 +'<td class="text-center">'+D[i].product_sn+'</td>'
							 +'<td class="text-center">'+D[i].quantity+'</td>'
							 +'<td class="text-center">'+D[i].unit_name+'</td>'
							 +'<td class="text-center">'+D[i].shipped_quantity+'</td>'
							 +'<td class="text-center">'+D[i].minus_quantity+'</td>'
							 +'</tr>').appendTo('#origin_det');
						}
					} else {
						alertTips('error','查询明细错误');
					}
				} else {
					alertTips('error','网络错误');
				}
			});
		}
	}
	
	$('#origin_header').on('click','td',function(){
		var _id = $(this).closest('tr').find('.no').attr('data-id');
		getSellDet(_id);
	})
	
	//@todo 冒泡设置
	$('#origin_header').on('click', '.sell_select', function(e) {
		e.stopPropagation();
		var sellId = $(this).closest('tr').find('.no').attr('data-id');// sell_id
		var pid = '';
		var _ids = [];
		$('#origin_det .ids').each(function(i,element){
			if($(this).prop('checked')){
				_ids.push($(this).attr('data-id'));//sell_det_id
			}
			pid = $(this).attr('data-pid');//sell_id
			
		});
		if(sellId != pid){
			_ids =[];
			getSellDet(sellId);
		}
		if(_ids.length == 0){
			alertTips('warning','请选择订单商品');
			return false;
		}
		//判断采购订单是否已被未确认的入库单占用
		$.get('check-sell-used',{'id':sellId}, function(data,status){
			if(status =='success' && data.Ack =='Success'){
				alertTips('error','已有采购单调用了此订单，请选择另外订单商品');
				return false;
			} else {
				imploadeSellOrder(sellId,_ids);
			}
		})
//		imploadeProcurement(sellId,_ids);
	});
	
	//销售订单数据导入销售出库单
	var imploadeSellOrder = function(sellId, ids){
		$.get('/sale/implode-sell-info', {'id' : sellId}, function(data, status) {
			if(status =='success' && data.Ack =='Success'){
				var H = data.Body.head;
				if(H){
//					$('#pur_no').val(H.purchase_no);
					var dt = new Date();
					var NowDay = dt.getFullYear()+'-'+(dt.getMonth()+1)+'-'+dt.getDate();
//					$('#customer').val(H.customer_id);
					$('#pro_time').val(NowDay);
//					$('#origin_id').val(H.sell_no).attr('data-id',H.sell_id);
					$('#warehouse').val(H.warehouse_id);
					$('#handle_man').val(H.handle_man);
//					$('#remark').val('销售订单号:'+ H.sell_no +' 导入');
//					$('#save').attr('data-origin',H.sell_id);
					
					$('#total_discount').val(H.total_discount);//整单折扣
					$('#discount_amount').val(H.discount_amount);//整单优惠金额
					
				}
				var D = data.Body.det;
				if(D){
//					$('#procurementList').find('tr').remove();
					
					for(var i in D){
						for(var j in ids){
							if(D[i].sell_det_id == ids[j]){
								var FLAG = 0;
								$('#procurementList tr').each(function(index,element){
//									console.log($(element).find('[name="ids"]').attr('data-selldetid'));
									var _ID = +$(element).find('[name="name"]').attr("data-id");
									if(!_ID){
										$(element).remove();
									}
									var det_id = +$(element).find('[name="ids"]').attr('data-selldetid');
									if(D[i].sell_det_id == det_id){
										FLAG = 1;
									}
								});
								if(!FLAG){
									var tmp_units= '';
									var U = D[i].product_unit;
									tmp_units = '<select class="form-control units"><option value="0" data-rate="0"> </option>';
									for(var j in U){
										tmp_units +='<option value="'+U[j].unit_id+'" data-rate="'+U[j].rate+'">'+U[j].unit_name+'</option>';
									}
									tmp_units +='</select>';
									$('<tr>'
									   +'<td class="text-center" name="ids" data-id="" data-selldetid="'+D[i].sell_det_id+'" data-sellid="'+D[i].sell_id+'">'+_COUNT+'</td>'
									   +'<td class="text-center"><a href="javascript:void(0)"class="stock-minus-tr" data-id="'+D[i].goods_id+'"> <i class="fa fa-minus-circle" style="color:red;" ></i></a></td>'
									   +'<td class="text-center" name="name" data-id="'+D[i].product_id+'"><input title="商品名称" readonly type="text" value="'+D[i].product_name+'"'
									   +'class="product_name stock-input50"  style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"'
									   +'href="javascript:void(0)"><i class="fa fa-search"></i></a></td>'
									   +'<td class="text-center" name="sn">'+D[i].product_sn+'</td>'
									   +'<td class="text-center" name="quantity"><input type="text" value="'+D[i].quantity+'" class="quantity"></td>'
									   +'<td class="text-center" name="unit" style="width:8%;">'+tmp_units+'</td>'
									   +'<td class="text-center" name="price"><input type="text"  placeholder="0.00" class="price" value="'+D[i].price+'"></td>'
									   +'<td class="text-center" name="amount"><input type="text" placeholder="0.00" class="amount" value="'+D[i].amount+'"></td>'
									   +'<td class="text-center" name="remark"><input type="text" style="width:100%;" value="'+D[i].remark+'"></td>'
									   +'<td class="text-center" name="gross_quantity"><input type="text"  class="quantity" disabled></td>'
									   +'<td class="text-center" name="gross_unit" style="width:8%;">'+tmp_units+'</td>'
									   +'<td class="text-center" name="rec_quantity"><input type="text" class="rec_quantity" disabled value="0"></td>'
									   +'<td class="text-center" name="minus_quantity"><input type="text" class="minus_quantity" disabled value="'+D[i].quantity+'"></td>'
									   +'<td class="text-center" name="unit_relation">'+D[i].unit_content+'</td>'
									   +'<td class="text-center" name="second_unit" style="width:8%;">'+tmp_units+'</td>'
									   +'<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="'+D[i].second_quantity+'"></td>'
									   +'<td class="text-center" name="second_relation">'+D[i].second_relation+'</td>'
									   +'<td class="text-center" name="base_unit" data-id="'+D[i].base_unit+'">'+D[i].base_name+'</td>'
									   +'<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value="'+D[i].left_quantity+'" disabled></td>'
									   +'<td class="text-center"><a href="javascript:void(0)" class="checkStock" data-quantity="'+D[i].product_quantity+'">库存</a></td>'
									   +'</tr>').appendTo('#procurementList');
									  
									$('#procurementList').find('tr:last').find('[name="unit"] select').val(D[i].quantity_unit);
									$('#procurementList').find('tr:last').find('[name="second_unit"] select').val(D[i].second_unit);
									
									global_count++;
									_COUNT++
								}
							}
						}
					}
					SumAmount();
				}
			}
		});
	};
	
	
})

$(function() {
	$('#supplier_id').searchableSelect();
});