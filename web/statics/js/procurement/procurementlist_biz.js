$(function(){
	var global_info = {
			'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
			'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页数
			'cond':[],//查询条件
//			'filter':undefined//过滤条件
		}
	/**
	 * @desc 查询导出采购单
	 */
	var global_pro = {
			'page': undefined,//页码
			'pageSize' :undefined, //页数
			'cond':[],//查询条件
			'filter' : $('#order_status').val(),
		}
	//时间
	dateSelFun($('#starTime'));
	dateSelFun($('#endTime'));

		//采购订单列表
		listProcurement();
		function listProcurement(pageInfo)
		{
			if (pageInfo !== undefined) {
				global_info.page = pageInfo.page;
				global_info.pageSize = pageInfo.pageSize;
			}
			$('#procurementlist').find('tr').remove();
			$.get('get-procurements',global_info,function(data,status){
				if(status ==='success'){
					if(data.Ack ==='Success'){
						var W = data.Body.list;
						if(W && W.length){
							for(var i in W){
								$('<tr><td class="text-center" name="ids"><input type="checkbox"  class="ids" data-id="'+W[i].procurement_id+'"></td>'
								+'<td class="text-center no show-pop-table" style="color:blue;" data-id="'+W[i].procurement_id+'">'+W[i].procurement_no+'</td>'
								+'<td class="text-center procurement_date">'+intToLocalDate(W[i].procurement_date,1)+'</td>'
								+'<td class="text-center vendor">'+W[i].supplier_name+'</td>'
								+'<td class="text-center warehouse">'+W[i].warehouse_name+'</td>'
								+'<td class="text-center procurement_man">'+W[i].employee_name+'</td>'
								+'<td class="text-center remark">'+W[i].remark+'</td>'
								+(+W[i].finish_flag ? '<td class="text-center"><a href="javascript:void(0)" class="" style="padding: 6px 8px;" disabled>完成</a></td>':'<td class="text-center"><a class="">未完成</a></td>')
								+'<td class="text-center"><a href="javascript:void(0)" class="procurement_edit fa fa-pencil-square-o fa-2x" title="编辑"></a></td>'
//								+(+W[i].confirm_flag ? '<td class="text-center"><a href="javascript:void(0)" class="btn btn-warning" style="padding: 6px 8px;" disabled>已确认</a></td>':'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>')
//								+'<td class="text-center confirm"><a class="btn btn-success">确认</a></td>'
								+'</tr>').appendTo('#procurementlist');
				                
							}
							var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listProcurement);
			                }
						}else {
							$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#procurementlist');
							refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listProcurement);
						}
					} else {
						$('<tr><td colspan="9" class="text-center">没有数据</td></tr>').appendTo('#procurementlist');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listProcurement);
					}
				} else{
					alertTips('error','网络错误');
				}
			});
		};
		
	//查询
	$('#search_list').on('click',function(){
		var starTime = $('#starTime').val();
		var endTime = $('#endTime').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_info.cond ={
			'starTime': starTime,
			'endTime': endTime,
			'vendor':$('#vendor').val(),
			'warehouse':$('#warehouse').val(),
		};
		
		listProcurement();
	});
	
	
	//编辑
	$('#procurementlist').on('click','.procurement_edit',function(){
		var tr =$(this).closest('tr');
		var id = tr.find('.ids').attr('data-id');
		//检查是否有编辑采购单功能
		$.get('check-edit-pro',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
		
			
			window.location.href = "/procurement/edit-procurement?id="+ id;
		})
	})
	
//	//确认
//	$('#procurementlist').on('click','.confirm',function(){
//		var tr = $(this).closest('tr');
//		var id = tr.find('[name="ids"] input').attr('data-id');
//		layer.confirm('点击确认入库单将无法编辑，是否确认?', function(index) {
//			$.get('confirm-instore',{'id':id},function(data,status){
//				if(status =='success'){
//					if(data.Ack =='Success'){
//						alertTips('success','确认成功');
//						setTimeout(function(){
//							window.location.reload();
//						},1000);
//					}
//				} else{
//					alertTips('error','网络错误');
//				}
//			})
//			layer.close(index);
//		});
//	})
	
//	//过滤
//	$('#filterInstore').on('change',function(){
//		global_info.filter = $(this).val();
//		listProcurement();
//	})
	
	//删除
	$('#delete').on('click',function(){
		
		//检查是否有删除采购单功能
		$.get('check-del-pro',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var _all_ids='';
			var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
			$('.ids').each(function(i){
				tmp_count +=i;
				if($(this).prop('checked')){
					_all_ids += $(this).attr('data-id') + ',';
				}
			})
	
			_all_ids = _all_ids.substr(0,_all_ids.length -1);
			if (_all_ids.length === 0) {
				alertTips('warning','请勾选复选框！');
				return false;
			} 
			if(tmp_count == 0){
				if(+global_info.page > 1){
					global_info.page --;
				}
			}
			layer.confirm('是否删除采购单？',function(index){
				$.get('del-procurement',{'ids':_all_ids}, function(data,status){
					if(status =='success' && data.Ack =='Success'){
						alertTips('success','删除成功！');
						setTimeout(function(){ 
							listProcurement();
							return false;
						},1500);
					}else if(data.Error =='finish order can not be delete'){
						alertTips('error','已完成单据不能删除！');
						return false;
					} else {
						alertTips('error','删除失败！');
						return false;
					}
				})
				layer.close(index);
			})
		})
	})
	
	
	/**
	 * @desc 点击单号查询商品明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	
	var settings = {
		trigger:'hover',
		title:'商品信息 ',
		content:'<p>没有数据</p>',
		width:320,						
		multi:false,						
		closeable:false,
		style:'',
//		delay:300,
	    cache:true,
		padding:true,
		autoHide:10,
	};
	
	function initPopover(){					
		$('#procurementlist').on('click','.show-pop-table',function(){
			var THIS =$(this);
			var iindex = layer.load();
			$.get('get-procurement-det',{'id':$(this).attr('data-id')},function(data,status){
				THIS.webuiPopover('destroy');
				if(status == 'success'){
					if(data.Ack =='Success'){
						$('#webuiList').empty();
						var M = data.Body;
						var _table ='';
						for(var i in M){
							 $('<tr><td data-id="'+M[i].procurement_id+'">'+(+i+1)+'</td>'
								 +'<td>'+M[i].goods_id+'</td>'
								 +'<td>'+M[i].product_name+'</td>'
								 +'<td>'+M[i].product_sn+'</td>'
								 +'<td>'+M[i].unit_name+'</td>'
								 +'<td>'+M[i].quantity+'</td>'
								 +'<td>'+M[i].price+'</td>'
								 +'<td>'+M[i].amount+'</td>'
								 +'</tr>').appendTo('#webuiList');
						}
						
					}
					layer.close(iindex);
					
					var tableContent = $('#tableContent').html();
					tableSettings = {content:tableContent,
										width:800
									};
					THIS.webuiPopover($.extend({},settings,tableSettings));
				} else {
					alertTips('error','网络错误');
				}
			})
//			$(this).webuiPopover($.extend({},settings,tableSettings));
//			$(this).webuiPopover('destroy').webuiPopover($.extend({},settings,tableSettings));
		})
	}
	
	initPopover();
	
	
	dateSelFun($('#exp-star-time'));
	dateSelFun($('#exp-end-time'));
	//查询
	$('#searchPro').on('click', function(){
		var starTime = $('#exp-star-time').val();
		var endTime = $('#exp-end-time').val();
		if(starTime > endTime){
			alertTips('warning','开始时间不能大于截至时间！');
			return false;
		}
		global_pro.cond ={
			'vendor':$('#exp_vendor').val(),
			'starTime': starTime,
			'endTime': endTime,
			'name' : $('#good_name').val(),
		};
		global_pro.page = 1;
		global_pro.pageSize= 10;
		selectProMaterial();
	})
	
	//过滤
	$('#order_status').on('change',function(){
		global_pro.filter = $(this).val();
		selectProMaterial();
	})
	$('#export_list').on('click', function(){
		$('#selectPro').modal('show');
	})
	
	function selectProMaterial(Pages){
		if (Pages !== undefined) {
			global_pro.page = Pages.page;
			global_pro.pageSize = Pages.pageSize;
		}
		$.get('get-export-procurement', global_pro, function(data, status){
			$('#pro_detail').find('tr').remove();
			if(status =='success'){
				if(data.Ack =='Success'){
					var P = data.Body.list;
					
					if(P && P.length) {
						for(var i in P){
							$('<tr><td class="text-center" name="ids" data-id="'+P[i].procurement_id+'">'+(+i+1)+'</td>'
							    +'<td class="text-center" data-proid="'+P[i].procurement_id+'">'+P[i].procurement_no+'</td>'
							    +'<td class="text-center">'+intToLocalDate(P[i].procurement_date,1)+'</td>'
							    +'<td class="text-center">'+P[i].supplier_name+'</td>'
							    +'<td class="text-center" data-id="'+P[i].product_id+'">'+P[i].product_name+'</td>'
							    +'<td class="text-center">'+P[i].base_quantity+'</td>'
							    +'<td class="text-center">'+P[i].unit_name+'</td>'
							    +'<td class="text-center">'+P[i].price+'</td>'
							    +'</tr>').appendTo('#pro_detail');
						}
						var pageInfo = data.Body.page;
		                if (typeof pageInfo !== 'undefined'){
		                	refreshPaginationNavBar($('.paginationNavBar2'), pageInfo.page, pageInfo.pageSize, data.Body.count, selectProMaterial);
		                }
					} else {
						$('<tr><td colspan="11" class="text-center">没有数据</td></tr>').appendTo('#pro_detail');
						refreshPaginationNavBar($('.paginationNavBar2'), 1, 10, 1, selectProMaterial);
					}
				} else {
					$('<tr><td colspan="11" class="text-center">没有数据</td></tr>').appendTo('#pro_detail');
					refreshPaginationNavBar($('.paginationNavBar2'), 1, 10, 1, selectProMaterial);
				}
				
			} else {
				alertTips('error','网络错误!');
				return false;
			}
		});
	}
	
	/**
	 * @desc 导出execl
	 */
	$("#export_pro").on('click',function(){
		var _FLAG = 0;
		var _id  =0;
		var flag = 0;
		
		$.get('check-procurement-export',function(data,status){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				flag =1;
				return;
			}
			$('#pro_detail tr').each(function(index, element){
				_id = +$(element).find('[name="ids"]').attr('data-id');
				if(!_id){
					_FLAG = 1;
				}
			})
			if(_FLAG){
				alertTips('error','请先查询要导出的订单！');
				return false;
			}
			$url ='export-pro-info?name='+global_pro.cond.name+'&vendor='+global_pro.cond.vendor+'&filter='+global_pro.filter
					+'&stime='+global_pro.cond.starTime+'&etime='+global_pro.cond.endTime;
			if(!flag){
				window.location.href = $url;
			}
		})
		
	})
});