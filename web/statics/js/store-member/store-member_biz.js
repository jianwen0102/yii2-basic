$(function(){
   memberData.getMemberList();
});
var memberData = {
	'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
	'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined, //页总条数
	'searchData':{},//查询数据
	'filter': {}, //过滤条件
    getMemberList: function(pageInfo){//获取供应商列表
	    if(pageInfo != undefined){
		   memberData.page     = pageInfo.page;
		   memberData.pageSize = pageInfo.pageSize;
		}
	    var data = {'page':memberData.page, 'pageSize':memberData.pageSize, 'searchData':memberData.searchData, 'filter':memberData.filter};
		$('#store_member').find('tr').remove();
		$.get('get-member-list',data,function(res){
			if(res.Ack ==='Success'){
				var S = res.Body.list;
				if(S && S.length>0){
					for(var i in S){
						var delete_flag_status = S[i].delete_flag==0 ? '<font style="color:green">正常</font>' : '<font style="color:red">屏蔽</font>';
						$('<tr id="tr_'+S[i].member_id+'"><td class="text-center"><input type="checkbox"  class="ids" data-id="'+S[i].member_id+'">'+S[i].member_id+'</td>'
						+'<td class="text-center">'+S[i].nickname+'</td>'
						+'<td class="text-center">'+S[i].store_name+'</td>'
						+'<td class="text-center">'+S[i].cardno+'</td>'
						+'<td class="text-center">'+S[i].mobile+'</td>'
						+'<td class="text-center">'+S[i].balance+'</td>'
						+'<td class="text-center">'+S[i].consume_points+'</td>'
						+'<td class="text-center">'+S[i].rank_points+'</td>'
						+'<td class="text-center">'+S[i].join_time+'</td>'
						+'<td class="text-center"><a href="javascript:memberData.editMemberList(\''+S[i].member_id+'\');" class="supplier_edit fa fa-pencil-square-o fa-2x" title="编辑"/></td>'
						+'</tr>').appendTo('#store_member');
					}
					var pageInfo = res.Body.pageinfo;
				    if (typeof pageInfo !== 'undefined'){
						refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, res.Body.count, memberData.getMemberList);
					}
				}else {
					$('<tr><td colspan="5" class="text-center">没有数据了</td></tr>').appendTo('#store_member');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, memberData.getMemberList);
				}
			} else{
				alertTips('error','没有数据了');
			}
		});
	
	},

	addMemberList: function(){//添加
		$.post('check-add-member',function(data){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			$("#myModalLabel").html('会员新增');

            if(data.length>0){
			   var htl = '<option value="0">请选择门店</option>';
			   for(var i in data){
				   htl += '<option value="'+data[i].store_id+'">'+data[i].store_name+'</option>';
			   }
			   $("#store_id").html(htl);
			}

			var gender  =0;
			$("#gender option").each(function(index,element){
				var this_obj = $(element);              
				if(this_obj.val() == gender){					
					this_obj.attr("selected",true);
				}
			});
			$('#nickname').val('');
			$('#cardno').val('');
			$('#mobile').val('');
			$("#store_save").attr('data-id','');
			$('#myModal').modal('show');	
		});
	},

	delMemberList: function(){//删除
	  
	},

	editMemberList: function(member_id){//编辑
		$.post('check-edit-member',{'member_id':member_id},function(data){
			if(data.Error =='User authentication fails'){
				alertTips('error', '你没有操作权限，如要操作，请向管理员申请');
				return;
			}
			var member    = data.member;
			var storeList = data.store;
            $("#myModalLabel").html('会员修改');
			$('#nickname').val(member.nickname);
			$('#cardno').val(member.cardno);
			$('#mobile').val(member.mobile);
			$("#store_save").attr('data-id',member.member_id);

			if(storeList.length>0){
			   var htl = '<option value="0">请选择门店</option>';
			   for(var i in storeList){
				   if(storeList[i].store_id == member.store_id){
				      htl += '<option value="'+storeList[i].store_id+'" selected>'+storeList[i].store_name+'</option>';
				   }else{
				      htl += '<option value="'+storeList[i].store_id+'">'+storeList[i].store_name+'</option>';
				   }
			   }
			   $("#store_id").html(htl);
			}

            var html = '<option value="0">未知</option>';
            switch(member.gender){
			   case '1':
				   html += '<option value="1" selected>男</option><option value="2">女</option>';
			       break;
			   case '2':
				   html += '<option value="1" >男</option><option value="2" selected>女</option>';
			       break;
			   default:
                   html += '<option value="1" >男</option><option value="2">女</option>';
			       break;
			}
            $("#gender").html(html);
			$('#myModal').modal('show');
		});
	},

	saveMember: function(_this){//保存
	    var nickname = $('#nickname').val();
		if(nickname.length == 0){
			alertTips('warning','请输入仓库名');
			return false;
		}
		var member_id  = $(_this).attr('data-id');
        var cardno     = $("#cardno").val();
		var mobile     = $("#mobile").val();
		var gender     = $("#gender").val();
		var store_id   = $("#store_id").val();
		
		loading();
		$.post('save-member',{'member_id':member_id, 'nickname':nickname, 'cardno':cardno, 'mobile':mobile, 'gender':gender, 'store_id':store_id},function(data){
			removeloading();
			if(data.Ack ==='Success'){
				alertTips('success','保存成功');
				setTimeout(function(){ 
					$('#myModal').modal('hide');
					window.location.reload();
				},1000);
			} else if(data.Error =="nickname is exists"){
				alertTips('warning','该会员已经存在');
				return false;
			}else {
				alertTips('error','系统繁忙...');
				return false;
			}
	   })	
	},

	changeFilter: function(){//过滤
	   memberData.filter = {
	      'delete_flag': $("#filterClient").val()
	   };
	   memberData.page = 1;
	   memberData.getMemberList();
	},

	selectAll: function(){//选中
	    if($('.checkAll').prop('checked')){
			$('.ids').prop('checked',true);
		} else {
			$('.ids').prop('checked',false);
		}
	},

	searchMemberList: function(){//查询门店
	    var nickname = $("#sname").val();
		var store_id   = $("#storeID").val();
		memberData.page = 1;
        memberData.searchData = {
		   'nickname' : nickname,
		   'store_id' : store_id
		};
		memberData.getMemberList();
	}
};