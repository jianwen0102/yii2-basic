$(document).ready(function(e){
	var global_info = {
		'page': $_GET['page'] ? $_GET['page'] : undefined,//页码
		'pageSize' : $_GET['pageSize'] ? $_GET['pageSize'] : undefined //页数
	}
	
	//仓库列表
	listWarehouse();
	function listWarehouse(pageInfo)
	{
		if (pageInfo !== undefined) {
			global_info.page = pageInfo.page;
			global_info.pageSize = pageInfo.pageSize;
		}
		$('#warehouse_list').find('tr').remove();
		$.get('get-warehouses',global_info,function(data,status){
			if(status ==='success'){
				if(data.Ack ==='Success'){
					var W = data.Body.list;
					if(W && W.length){
						for(var i in W){
							$('<tr><td class="text-center"><input type="checkbox"  class="ids" data-id="'+W[i].warehouse_id+'"></td>'
							+'<td class="text-center name">'+W[i].warehouse_name+'</td>'
							+'<td class="text-center remark">'+W[i].remark+'</td>'
	//						+'<td class="text-center default_set"><input type="checkbox" '+(+W[i].is_default ? 'checked':'')+' disabled></td>'
							+'<td class="text-center"><a href="javascript:void(0)" class="warehouse_edit">编辑</td>'
							+'</tr>').appendTo('#warehouse_list');
			                var pageInfo = data.Body.page;
			                if (typeof pageInfo !== 'undefined'){
			                	refreshPaginationNavBar($('.paginationNavBar'), pageInfo.page, pageInfo.pageSize, data.Body.count, listWarehouse);
			                }
						}
					}else {
						$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#warehouse_list');
						refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listWarehouse);
					}
				} else {
					$('<tr><td colspan="5" class="text-center">没有数据</td></tr>').appendTo('#warehouse_list');
					refreshPaginationNavBar($('.paginationNavBar'), 1, 10, 1, listWarehouse);
				}
			} else{
				alertTips('error','网络错误');
			}
		});
	}
	//新增
	$('#add').on('click',function(){
		$('#warehouse_name').val('');
		$('#remark').val('');
		$('#default').prop('checked',false);
		$('#warehouse_save').attr('data-wid','');
		$('#warehouse_save').attr('data-old','');
		$('#myModal').modal('show');
	});
	
	//新增保存
//	(function(){
		$('#warehouse_save').on('click',function(){
			$warehouse = $('#warehouse_name').val();
			if($warehouse.length == 0){
				alertTips('warning','请输入仓库名');
				return false;
			}
			var pflag = 0;
			$remark = $('#remark').val();
			$default = $('#default').prop('checked');
			$wid = $('#warehouse_save').attr('data-wid');
			loading();
			$.get('save-warehouse',{'name':$warehouse,'remark':$remark,'def':$default,'wid':$wid},function(data,status){
				removeloading();
				if(status ==='success' && data.Ack =='Success'){
					alertTips('success','保持成功');
					setTimeout(function(){ 
						$('#myModal').modal('hide');
						listWarehouse();
					},1000);
				} else if(data.Error =="warehouse is exists"){
					alertTips('warning','该仓库已经存在，请更改仓库名称');
					return false;
				}else {
					alertTips('error','网络错误');
					return false;
				}
			})
		});
//	})();
		
		//编辑
		$('#warehouse_list').on('click','.warehouse_edit',function(){
			var tr =$(this).closest('tr');
			var id = tr.find('.ids').attr('data-id');
			var name = tr.find('.name').text();
			var remark = tr.find('.remark').text();
			var _default = tr.find('.default_set input').prop('checked');
			$('#warehouse_save').attr('data-wid',id);
			$('#warehouse_save').attr('data-old',name);
			$('#warehouse_name').val(name);
			$('#remark').val(remark);
			$('#default').prop('checked',_default);
			$('#myModal').modal('show');

			
		});
		
		//全选
		$('.checkAll').on('click',function(){
			if($(this).prop('checked')){
				$('.ids').prop('checked',true);
			} else {
				$('.ids').prop('checked',false);
			}
		});
		//删除
		$('#delete').on('click',function(){
			var _all_ids='';
			var tmp_count = 0;//列数相加，若是0 则只有一条，删除时，如果global_info.page > 1，page 要减一
			$('.ids').each(function(i){
				if($(this).prop('checked')){
					_all_ids += $(this).attr('data-id') + ',';
				}
			})
			_all_ids = _all_ids.substr(0,_all_ids.length -1);
			if (_all_ids.length === 0) {
				alertTips('warning','请勾选复选框！');
				return false;
			}
			if(tmp_count == 0){
				if(+global_info.page > 1){
					global_info.page --;
				}
			}
			if(confirm("确定要删除仓库？")){
				$.get('del-warehouse',{'ids':_all_ids},function(data,status){
					if(status ==='success' && data.Ack ==='Success'){
						alertTips('success','删除成功');
						setTimeout(function(){ 
							listWarehouse();
						},1500);
					}else {
						alertTips('error','删除失败，请重新再试');
					}
				});
			 }
		});
})    