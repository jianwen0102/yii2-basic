<?php  
use app\enum\EnumOther;

function p($var){
	print_r('<pre>');
	print_r($var);
	print_r('</pre>');
	die;
}
function dd($var){
	print_r('<pre>');
	var_dump($var);
	print_r('</pre>');
	die;
}
/**
 * @desc 获取db\query类执行的sql语句
 * @param  $query // $query = new \yii\db\Query();
 */
function getSql($query){
	$commandQuery = clone $query;
    echo $commandQuery->createCommand()->getRawSql();
//     die;
};


    /**
     * @desc API格式处理,列表格式:'Body'=>array('list'=>'','count'=>'','page'=>array('page'=>'','pageSize'=>'')), 内容格式:'Body'=>array('content'=>'')
     * @param string $ack 状态值;Success:成功;Failure:失败
     * @param array $body 数据主体(除Ack为Failure外)
     * @param string $error 错误信息
     * @author liaojianwen
     * @date 2016-10-28
     * @return array 结果
     */
function handleApiFormat($ack, $body = array(), $error = '') 
{
	if ($ack != EnumOther::ACK_SUCCESS && $ack != EnumOther::ACK_FAILURE && $ack != EnumOther::ACK_WARNING) {
		return false;
	}
	$apiResult = array ();
	$apiResult ['Ack'] = $ack;
	if ($ack === EnumOther::ACK_FAILURE) {
		$apiResult ['Error'] = $error;
	} elseif ($ack === EnumOther::ACK_WARNING) {
		$apiResult ['Body'] = $body;
		$apiResult ['Error'] = $error;
	} else {
		$apiResult ['Body'] = $body;
	}
	$apiResult ['GmtTimeStamp'] = gmdate ( DATE_ISO8601 );
	$apiResult ['LocalTimeStamp'] = date ( DATE_ISO8601 );
	return $apiResult;
}

/**
 * 输出二维码
 * $level = 'L'; //容错级别
 * $size = 6;    //图片大小
 * $margin = 2;  //边框空白区域间距值
 */
function outputQrcode($url, $level = 'L', $size = 6, $margin = 2)
{
	require dirname(__DIR__).'/vendor/phpqrcode/phpqrcode.php';
	
	//生成二维码图片
	QRcode::png($url, false, $level, $size, $margin, true);
	exit();
}

