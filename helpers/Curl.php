<?php
namespace app\helpers;
/**
 * @desc 根据URL获取内容
 * @author liaojianwen
 * @date 2016-11-7
 * @return Curl
 */
class Curl
{

    /**
     * @desc 根据URL获取内容
     * @param string $url
     * @author liaojianwen
 	 * @date 2016-11-7
     * @return string
     */
    static public function get($url)
    {
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $url);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($connection);
        curl_close($connection);
        return $response;
    }
    
    /**
     * @desc 根据URL发送/获取内容
     * @param string $url
     * @param string $postdata
     * @author liaojianwen
     * @date 2016-11-7
     * @return string
     */
    static public function post($url, $postdata)
    {
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_URL, $url);
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($connection, CURLOPT_POST, 1);
        curl_setopt($connection, CURLOPT_POSTFIELDS, $postdata);
        $response = curl_exec($connection);
        curl_close($connection);
        return $response;
    }
	
    /**
     * @
     * @param $type =='GET' $data ="page=1&pageSize=15";
     * 		  $type =='POST' $data = {"goods_name":9999,"supplier_id":2,"goods_unit":3,"cat_id":2,"good_id":1,"attr_id":1212}}
     * @author liaojianwen
     */
    static public function curlOption($url,$type="GET",$data, $key)
    {
    
    	$headers = array(
    			"Authorization:{$key}",
//     			"Content-Type:application/json",
//     			"Content-Length: " . strlen($data),
    
    	);
    
    	$connection = curl_init();
    	if($type ==="GET"){
    		if(!empty($data)){
    			$url .="?{$data}";
    		}
    	} else {
    		array_push($headers, "Content-Type:application/json");
    		array_push($headers, "Content-Length: " . strlen($data));
    	}
    	curl_setopt($connection, CURLOPT_URL, $url);
    	curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
    	curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
    	curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
    	curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
    	if($type ==="POST"){
    		curl_setopt($connection, CURLOPT_POST, 1);
    		if(!empty($data)){
    			curl_setopt($connection, CURLOPT_POSTFIELDS, $data);
    		}
    	}
    	$response = curl_exec($connection);
    	curl_close($connection);
    	return $response;
    
    }
}