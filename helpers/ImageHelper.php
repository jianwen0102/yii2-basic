<?php
namespace app\helpers;

/**
 * @desc 图片处理助手
 * @author lizichuan
 * @date 2018-6-13
 */
class ImageHelper
{
	private $rootPath;
	
	public function __construct()
	{
		$this->rootPath = dirname(__DIR__).'/web/';
	}
	
	/**
	 * @desc 压缩成缩略图
	 * @param mixed $key 在数据库里存储的值
	 * @param array $searchSet 用户定义的转义数组
	 * @return 该数据库存储码实际代表的值
	 * @date 2018-6-13
	 */
	public function thumb($img, $dir = '', $thumb_width = 0, $thumb_height = 0)
	{
         $gd = 2; //获取 GD 版本。0 表示没有 GD 库，1 表示 GD 1.x，2 表示 GD 2.x
         if ($gd == 0)
         {
             return false;
         }

        /* 检查缩略图宽度和高度是否合法 */
		$img = $this->rootPath . $img;
        if ($thumb_width == 0 && $thumb_height == 0)
        {
            return $img;
        }

        /* 检查原始文件是否存在及获得原始文件的信息 */
        $org_info = @getimagesize($img);
		
        if (!$org_info)
        {
            return false;
        }

        switch ($org_info[2])
        {
            case 1:
            case 'image/gif':
                $img_org = imagecreatefromgif($img);
                break;

            case 2:
            case 'image/pjpeg':
            case 'image/jpeg':
                $img_org = imagecreatefromjpeg($img);
                break;

            case 3:
            case 'image/x-png':
            case 'image/png':
                $img_org = imagecreatefrompng($img);
                break;

            default:
                $img_org = false;
        }

        /* 原始图片以及缩略图的尺寸比例 */
        $scale_org      = $org_info[0] / $org_info[1];
        /* 处理只有缩略图宽和高有一个为0的情况，这时背景和缩略图一样大 */
        if ($thumb_width == 0)
        {
            $thumb_width = $thumb_height * $scale_org;
        }
        if ($thumb_height == 0)
        {
            $thumb_height = $thumb_width / $scale_org;
        }

        /* 创建缩略图的标志符 */
        if ($gd == 2)
        {
            $img_thumb  = imagecreatetruecolor($thumb_width, $thumb_height);
        }
        else
        {
            $img_thumb  = imagecreate($thumb_width, $thumb_height);
        }

        if ($org_info[0] / $thumb_width > $org_info[1] / $thumb_height)
        {
            $lessen_width  = $thumb_width;
            $lessen_height  = $thumb_width / $scale_org;
        }
        else
        {
            /* 原始图片比较高，则以高度为准 */
            $lessen_width  = $thumb_height * $scale_org;
            $lessen_height = $thumb_height;
        }

        $dst_x = ($thumb_width  - $lessen_width)  / 2;
        $dst_y = ($thumb_height - $lessen_height) / 2;

        /* 将原始图片进行缩放处理 */
        if ($gd == 2)
        {
            imagecopyresampled($img_thumb, $img_org, $dst_x, $dst_y, 0, 0, $lessen_width, $lessen_height, $org_info[0], $org_info[1]);
        }
        else
        {
            imagecopyresized($img_thumb, $img_org, $dst_x, $dst_y, 0, 0, $lessen_width, $lessen_height, $org_info[0], $org_info[1]);
        }

        /* 如果目标目录不存在，则创建它 */
        if (!file_exists($dir))
        {
            if (!mkdir($dir))
            {
                return false;
            }
        }

        /* 生成文件 */
		$filename = basename($img);
		$suffix = '';
		$index = strrpos($filename, '.');
		if ($index !== false) {
			$suffix = strtolower(substr($filename, $index+1));
		}
		
		$file = $this->rootPath . $dir . $filename;
        if (($suffix == 'jpeg' || $suffix == 'jpg') && function_exists('imagejpeg'))
        {
            imagejpeg($img_thumb, $file);
        }
        elseif ($suffix == 'gif' && function_exists('imagegif'))
        {
            imagegif($img_thumb, $file);
        }
        elseif ($suffix == 'png' && function_exists('imagepng'))
        {
            imagepng($img_thumb, $file);
        }
        elseif ($suffix == 'bmp' && function_exists('imagewbmp'))
        {
            imagewbmp($img_thumb, $file);
        }
        else
        {
            return false;
        }

        imagedestroy($img_thumb);
        imagedestroy($img_org);

        //确认文件是否生成
        if (file_exists($file))
        {
            return $dir . $filename;
        }
        else
        {
            return false;
        }
	}
}
