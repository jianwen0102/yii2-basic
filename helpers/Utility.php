<?php
namespace app\helpers;

/**
 * @desc 常用函数汇总
 * @author liaojianwen
 * @date 2016-11-3
 */
class Utility{
	
	/**
	 * @desc 将数据库存储代码转成实际意义的值
	 * @param mixed $key 在数据库里存储的值
	 * @param array $searchSet 用户定义的转义数组
	 * @return 该数据库存储码实际代表的值
	 * @date 2016-11-3
	 */
	public static function toActual($key, $searchSet, $default = ''){
		if(!isset($key) || empty($searchSet) || !is_array($searchSet)){
			return $default;
		}
		return Utility::getArrayValue($searchSet, $key, $default);
	}
	
	/**
	 * @desc 判断数组里是否存在该键值对应的元素
	 * @param array $searchArray 要查找的数组
	 * @param string $key 要查找的键值
	 * @param $default 不存在时候的默认值
	 * @return mixed，有值则返回，其余返回默认值
	 * @date 2014-11-3
	 */
	public static function getArrayValue($searchArray, $key, $default = ''){
		if(!is_array($searchArray) || empty($searchArray) || !isset($key)){
			return $default;
		}
		if(array_key_exists($key, $searchArray)){
			if(!empty($searchArray[$key]) || is_bool($searchArray[$key])){
				return $searchArray[$key];
			}
		}
		return $default;
	}
}