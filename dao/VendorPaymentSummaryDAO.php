<?php
 namespace app\dao;
 
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
use yii\base\Object;
	 
 class VendorPaymentSummaryDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-10-31
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'vendor_payment_summary';
 		$this->_pKey = 'vendor_payment_summary_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%vendor_payment_detail}}';
 	}
 	
 	/**
 	 * @desc 获取汇总数据
 	 * @author liaojianwen
 	 * @date 2017-10-31
 	 */
 	public function getSummaryDetail($id)
 	{
 		$selects = "d.quantity, d.product_id,d.quantity_unit,p.product_name,u.unit_name,d.price,d.amount,d.remark,d.summary_time";
 		$conditions ="d.vendor_payment_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p","p.product_id = d.product_id")
 		->leftJoin("unit u","u.unit_id = d.quantity_unit")
 		->where($conditions,$params)
 		->all();
 		return $result;
 	}
 }