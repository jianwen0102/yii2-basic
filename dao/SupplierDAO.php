<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;
use Yii;
use app\enum\EnumOriginType;

class SupplierDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'supplier';
		$this->_pKey ='supplier_id';
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%supplier}}';
	}
	
	
	/**
	 * @desc 获取供应商资料
	 * @param [] $cond 查询条件
	 * @param $filter 过滤条件  
	 * @param [] $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-11
	 */
	public function getSupplier($cond, $filter, $pageInfo)
	{
		$limit = $pageInfo['pageSize'];
		$offset = ($pageInfo['page'] - 1) * $limit;
		$selections = "supplier_id,supplier_name,supplier_desc,country,province,city,district";
		$conditions = "delete_flag =:flag and type = :type";
		$params = array (
				':flag' => EnumOther::NO_DELETE,
				':type' => EnumOther::SUPPLIER,
		);
// 		switch ($filter){
// 			case 0 :
// 				break;
// 			case 1:
// 				$conditions .=" and type=:type";
// 				$params[':type'] = EnumOther::SUPPLIER;
// 				break;
// 			case 2:
// 				$conditions .=" and type = :type";
// 				$params[':type'] = EnumOther::CUSTOMER;
// 				break;
// 		}
		
		$userId = Yii::$app->user->id;
		$auth = Yii::$app->authManager;
		$role = $auth->getRolesByUser($userId);
		
		if(array_key_exists(EnumOther::BUY_ROLE, $role)){//采购角色
// 			$conditions .=" and create_man =:man and type=:type";
// 			$params[':man'] = $userId;
// 			$params[':type'] = EnumOther::SUPPLIER;
			// $conditions .=" and create_man =:man";
			// $params[':man'] = $userId;
		}
		
// 		if(array_key_exists(EnumOther::SALE_ROLE, $role)){//销售角色
// 			$conditions .=" and type=:type";
// // 			$params[':man'] = $userId;
// 			$params[':type'] = EnumOther::CUSTOMER;
// 		}
		
		$query = new Query ();
		$query->select ( $selections )->from ( $this->_table )->where ( $conditions,$params )->orderBy ( 'supplier_id ASC' );
		if(isset($cond['name']) && $cond['name']){
			$query->andWhere(['like', 'supplier_name', $cond['name']]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		foreach ( $result ['list'] as &$val ) {
			$region_str = RegionDAO::getInstance ()->getRegionName ( $val ['province'] );
			$region_str .= '-' . RegionDAO::getInstance ()->getRegionName ( $val ['city'] );
			$region_str .= '-' . RegionDAO::getInstance ()->getRegionName ( $val ['district'] );
			if (substr ( $region_str, - 1, 1 ) == '-') {
				$region_str = substr ( $region_str, 0, - 1 );
			}
			$val ['region_str'] = $region_str;
		}
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize'] 
		);
		return $result;
	}
	
	/**
	 * @desc 保存供应商
	 * @param [] $info
	 * @param int $id
	 * @author liaojianwen
	 * @date 2016-11-21
	 */
	public function saveSupplier($info)
	{
		$columns = [
			'supplier_name'=>$info['sname'],
			'supplier_desc'=>$info['desc'],
			'handle_man'=>$info['man'],
			'country'=>$info['country'],
			'province'=>$info['province'],
			'city'=>$info['city'],
			'district'=>$info['district'],
			'phone'=> $info['phone'],
// 			'type'=> $info['client_type'],
			'bank'=>$info['bank'],
			'bank_account' => $info['bank_account'],
			'account_name' => $info['account_name'],
			'create_man' => Yii::$app->user->id,
		];
		$id = isset($info['id'])? $info['id']:0;
		if(empty($columns['district'])){
			unset($columns['district']);
		}
		if(empty($id)){
			$columns['create_time'] = time();
			$result = $this->iinsert($columns);
		} else {
			//更新
			$columns['modify_time']= time();
			$conditions = "supplier_id = :id";
			$params = [
				':id'=>$id,
			];
			$result = $this->iupdate($columns, $conditions,$params);
		}
		return $result;
	}
}