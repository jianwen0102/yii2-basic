<?php
namespace app\dao;
use Yii;
use app\dao\BaseDAO;
use yii\db\Query;

class StoreEmployeeDAO extends BaseDAO
{
     /**
	 * @desc   构造函数
	 * @author hehauwei
	 * @date   2017-7-21
	 */
	public function __construct()
	{
	   parent::__construct();
	   $this->_table = 'store_employee';
	   $this->_pKey  = 'employee_id';
	}
	
	public static function tableName()
	{	
	   return '{{%store_employee}}';	
	}
	
	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */	
	public function getStoreEmployeeListData($pageInfo,$searchData,$filter){
	   $page     = isset($pageInfo['page']) ? $pageInfo['page'] : 1;
	   $pageSize = isset($pageInfo['pageSize']) ? $pageInfo['pageSize'] : 15;
	   $limit    = $pageSize;
	   $offset   = isset($page) ? ($page - 1) * $limit : 0;
	   $query    = new Query();

	   $query->select('employee_id,store_id,employee_name,create_time,delete_flag')->from($this->_table)->where('1')->orderBy('employee_id DESC');

	   if(isset($searchData['employee_name']) && strlen(trim($searchData['employee_name']))>0){//供应商查询
	      $employee_name = trim($searchData['employee_name']);
          $query->andWhere(['like','employee_name',$employee_name]);
	   }

	   if(isset($filter['delete_flag']) && $filter['delete_flag'] != '-1'){
	        $query->andWhere('delete_flag=:delete_flag',[':delete_flag'=>$filter['delete_flag']]);	   
	   }

	   $result['count'] = $query->count();
	   $result['list']  = $query->offset($offset)->limit($limit)->all();	   
	   $result['pageinfo']  = array (
			 'page'     => $page,
			 'pageSize' => $pageSize
	   );
	   return $result;	
	}
	
	/**
	 * @desc   保存添加/编辑门店
	 * @param  $store_id    string 门店id
	 * @param  $store_name  string 门店名称
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStoreEmployeeData($data)
	{
		$employee['employee_name'] = $data['employee_name'];
		$employee['store_id']      = $data['store_id'];
		$employee['delete_flag']   = $data['delete_flag'] < 0 ? 0 : $data['delete_flag'];
		if(empty($data['employee_id']) || $data['employee_id']<1){	   
		   $employee['create_time']   = time();
           $result = $this->iinsert($employee);		
		}else{
		   $employee['modify_time']   = time();
		   $conditions =  " employee_id=:employee_id ";
		   $params     = [':employee_id'=>$data['employee_id']];
		   $result = $this->iupdate($employee,$conditions,$params);
		}
        return $result;
	}

	/**
	 * @desc 检查是否有重名
	 * @param int    $employee_id   门店员工id
	 * @param string $employee_name 门店员工名称
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return boolean
	 */
	public function checkStoreEmployee($employee_id,$employee_name)
	{
		$query = new Query();
		$conditions = " employee_name = '{$employee_name}' ";
		if ($employee_id > 0 ){
			$conditions .= " and employee_id <> {$employee_id}";
		}
		$result = $query->select ( 'employee_id' )->from ( "$this->_table" )->where ( $conditions )->all ();
		if (empty ( $result )) {
			return false;
		}
		return true;
	}


    /**
	 * @desc   获取一条数据
	 * @param  $employee_id  店员id
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStoreEmployeeOne($employee_id)
	{
	   $query = new Query();
       $employeeOne = $query -> select('employee_id,store_id,employee_name,delete_flag')
		              ->from( "$this->_table" ) 
		              -> where(['employee_id'=>$employee_id]) 
		              -> one();
	   return $employeeOne;
	}

}