<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use Yii;
 use yii\db\Query;
 
 class StoreDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author lianjianwen
 	 * @date 2017-07-28
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'store';
 		$this->_pKey = 'store_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%store}}';
 	}
 }
 