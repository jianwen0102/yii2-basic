<?php
namespace app\dao;
/**
 * @desc  分组、权限表
 * @author liaojianwen
 * @date 2017-01-07
 */

use app\dao\BaseDAO;
use app\enum\EnumOther;
use yii\db\Query;
use yii\base\Object;

class AuthItemDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'auth_item';
	}
	
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%auth_item}}';
	}
	
	/**
	 * @desc 查找分组列表信息
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function getGroups($cond, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "name,description";
		$conditions = "type = :type";
		$params = array (
				':type' => EnumOther::ROLE_TYPE,
		);
	
		
		$query = new Query ();
		$query->select ( $selections )
		->from ( "$this->_table")
		->where ( $conditions, $params )
		->orderBy ( 'created_at ASC' );
		if(isset($cond['name']) && $cond['name']){
			$query->andWhere(['like','description',$cond['name']]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	/**
	 * @desc 检查是否有相同的名称
	 * @author liaojianwen
	 * @date 2017-01-11
	 */
	public function checkSameName($name, $group)
	{
		$query = new Query();
		$result = $query->select('name')
				->from($this->_table)
				->where(['!=','name',$group])
				->andWhere(['=','description',$name])
				->one();
		if($result){
			return false;
		}
		return true;
		
	}
}