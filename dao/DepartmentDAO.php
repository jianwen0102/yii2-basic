<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;

class DepartmentDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'department';
		$this->_pKey ='depart_id';
		$this->_name = 'depart_name';
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%department}}';
	}
	
	/**
	 * @desc 获取部门列表
	 * @param  $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function getDepart($pageInfo) {
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "d.depart_id,d.depart_name,description,master,e.employee_name master_man";
		$conditions = "d.delete_flag =:flag";
		$params = array (
				':flag' => 0
		);
	
		$query = new Query ();
		$query->select ( $selections )
		->from ( "$this->_table d")
		->leftJoin("employee e","e.employee_id = d.master")
		->where ( $conditions, $params )
		->orderBy ( 'd.create_time ASC' );
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
}