<?php
namespace app\dao;

use yii\db\Query;
use yii\base\Object;
use app\models\OrderGoods;

class OrderDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-1
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->_table = 'shop_order_info';
		$this->_pKey ='order_id';
	}
	
	
	public function getOrders($pageInfo)
	{
		$limit = $pageInfo['pageSize'];
		$offset = ($pageInfo['page'] -1) * $limit;
		$query = new Query();
		$field = 'o.order_id,o.order_sn,o.user_id,o.order_status,o.shipping_status,o.pay_status,o.consignee,o.goods_amount,
				o.country,o.province,o.city,o.district,o.community,o.address,o.add_time,o.money_paid,o.order_amount,o.take_start_time';
		$query->select($field,'SQL_CALC_FOUND_ROWS')->from ( "$this->_table o" );
// 					->leftJoin('shop_order_goods g','o.order_id = g.order_id')
		$orderCount = $query->count();
		$orderlist = $query->offset($offset)->limit($limit)->orderBy('o.add_time desc')->all();
		
		foreach($orderlist as &$order){
				$goods = OrderGoodsDAO::getInstance()->getGoodsById($order['order_id']);
				$order['goods'] = $goods;
		}
		$result = [ 
				'pageInfo' => [ 
						'page' => $pageInfo['page'],
						'pageSize' => $pageInfo['pageSize'] 
				],
				'count' => $orderCount,
				'list' => $orderlist 
		];
		return  $result;
		
	}
	
}