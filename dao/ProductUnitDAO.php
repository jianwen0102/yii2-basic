<?php
 namespace app\dao;
 /**
  * @desc 商品辅助单位设置
  */
 use Yii;
 use app\enum\EnumOther;
 use app\dao\BaseDAO;
 use yii\db\Query;
 
 class ProductUnitDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2016-11-17
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'product_unit';
 		$this->_pKey ='product_unit_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%product_unit}}';
 	}
 	/**
 	 * @desc 根据id 获取商品辅助单位
 	 * @param $id 商品id
 	 * @author liaojianwen
 	 * @date 2017-02-15
 	 */
 	public function getProductUnitInfo($id)
 	{
 		$selects = "product_unit_id,d.product_id,d.unit_type,d.unit,d.rate,d.format,d.remark,p.product_name,u.unit_name";
 		$conditions ="d.product_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.unit")
 		->where($conditions,$params)
 		->all();
 		return $result;
 	
 	}
 	
 	/**
 	 * @desc 商品列表获取辅助单位的关系式
 	 * @author liaojianwen
 	 * @date 2017-02-16
 	 */
 	public function getUnits($product_id)
 	{
 		$selects = "product_unit_id,product_id,unit_id,rate,u.unit_name";
 		$conditions ="p.product_id = :id and p.delete_flag = :flag";
 		$params = [
 				':id'=>$product_id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table p")
 		->leftJoin("unit u","u.unit_id = p.unit")
 		->where($conditions,$params)
 		->orderBy('rate DESC')
 		->all();
 		return $result;
 		
 	}
 }