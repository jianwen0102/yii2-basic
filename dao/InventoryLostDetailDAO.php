<?php
namespace app\dao;
/**
 * @desc 盘盈表操作类
 * @author liaojianwen
 * @date 2017-02-04
 */
 use app\dao\BaseDAO;
 use Yii;
 use yii\db\Query;
 use app\enum\EnumOther;
		 
 class InventoryLostDetailDAO extends BaseDAO
 {
 /**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-02-04
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'inventory_lost_detail';
		$this->_pKey ='inventory_lost_det_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%inventory_lost_detail}}';
	}
	
	/**
	 * @desc 根据id 获取入库单明细
	 * @param $id 入库单id
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getInventoryLostDet($id)
	{
		$selects = "inventory_lost_det_id,inventory_lost_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,base_price,
				d.quantity_unit,d.price,d.amount,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content,d.product_img";
		$conditions ="d.inventory_lost_id = :id and d.delete_flag = :flag";
		$params = [
				':id'=>$id,
				':flag'=>EnumOther::NO_DELETE,
		];
		$query = new Query();
		$result = $query->select($selects)
		->from("$this->_table d")
		->leftJoin("product p",'d.product_id = p.product_id')
		->leftJoin("unit u","u.unit_id = d.base_unit")
		->where($conditions,$params)
		->all();
		foreach ($result as &$res){
			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
			$res['product_unit'] = $res_units;
		}
		return $result;
	
	}
	
	/**
	 * @desc 报损汇总统计
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function getInventoryLostSummary($cond, $pageInfo)
	{
	
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
	
	
		$sql = "select SUM(d.quantity) quantity,d.product_id,d.quantity_unit,h.warehouse_id, unit_name,product_name, w.warehouse_name from inventory_lost_detail d
				INNER JOIN inventory_lost h on h.inventory_lost_id = d.inventory_lost_id";
	
		if(isset($cond['starTime']) && !empty($cond['starTime'])){
			$sql .= " and h.inventory_lost_date between ".$cond['starTime'] .' and '. $cond['endTime'];
		}
		$sql .=	" LEFT JOIN product p on p.product_id = d.product_id
				LEFT JOIN warehouse w on w.warehouse_id = h.warehouse_id
				LEFT JOIN unit u on u.unit_id = d.quantity_unit where d.delete_flag = ".EnumOther::NO_DELETE;
		if(isset($cond['id']) && !empty($cond['id'])){
			$sql .= " and p.product_id = ".$cond['id'];
		}
	
		if(isset($cond['name']) && !empty($cond['name'])){
			$sql .= " and p.product_name like '%".$cond['name'] ."%'";
		}
			
		if(isset($cond['warehouse_in']) && !empty($cond['warehouse_in'])){
			$sql .= " and h.warehouse_id = ".$cond['warehouse_in'];
		}
		$sql .= " GROUP BY d.product_id,warehouse_id,quantity_unit";
	
	
	
		$command = yii::$app->db;
		$count = $command->createCommand()->setSql($sql)->execute();
	
		$sql .="  limit ".$limit .' offset '. $offset ;
	
		$return = $command->createCommand($sql)->queryAll();
	
		$result ['count'] = $count;
		$result ['list'] = $return;
	
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
	
		return $result;
	}
 }
 