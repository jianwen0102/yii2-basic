<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use yii\db\Query;
 use Yii;
 use app\enum\EnumOther;
 use app\enum\EnumOriginType;
	 
 class PriceTransactionDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2016-11-10
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'price_transaction';
 		$this->_pKey ='transaction_id';
 	}
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%price_transaction}}';
 	}
 	
 	
 	/**
 	 * @desc 获取销售成本信息
 	 * @author liaojianwen
 	 * @date 2018-02-28
 	 */
 	public function getSaleGrossProfitSummary($condition, $pageInfo)
 	{
 		$limit = isset($pageInfo ['pageSize'])?$pageInfo['pageSize']:0;
 		if($limit){
 			$offset = ($pageInfo ['page'] - 1) * $limit;
 		}
 			
 		$selections =['g.product_id','p.product_name','sum(cost_amount) sum_cost_amount','sum(g.quantity) sum_quantity','sum(cost_price) sum_cost_price','sum(g.price) sum_price','sum(g.amount) sum_amout'];
 			
 		$conditions = "g.delete_flag =:dflag and g.origin_type=:type and g.customer_id = :cid";
 			
 		$params = array (
 				':dflag' => EnumOther::NO_DELETE,
 				':type' => EnumOriginType::origin_sale_out_market,
 				':cid'=> EnumOther::DEFAULT_CUSTOMER
 		);
 			
 		$query = new Query();
 			
 		$query->select($selections)
 		->from("$this->_table g")
 		->innerJoin("product p", "g.product_id = p.product_id")
 		->where($conditions, $params)
 		->groupBy("g.product_id");
 			
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','origin_time',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		if(isset($condition['id']) && !empty($condition['id'])){
 			$query->andwhere(['=','g.product_id', $condition['id']]);
 		}
 		if(isset($condition['name']) && !empty($condition['name'])){
 			$query->andwhere(['like','p.product_name', $condition['name']]);
 		}
 			
 		$result ['count'] = $query->count ();
 		if($limit){
 			$query->offset ( $offset )->limit ( $limit );
 		}
 			
 		$result ['list'] = $query->all ();
 		$result ['page'] = array (
 				'page' => isset($pageInfo ['page'])?$pageInfo ['page']:0,
 				'pageSize' => isset($pageInfo ['pageSize'])?$pageInfo ['pageSize']:0,
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取特定商品销售情况
 	 * @author liaojianwen
 	 * @date 2018-04-13
 	 */
 	public function getSpecialOut($condition, $pageInfo, $goods_id)
 	{
//  		$limit = isset($pageInfo ['pageSize'])?$pageInfo['pageSize']:0;
//  		if($limit){
//  			$offset = ($pageInfo ['page'] - 1) * $limit;
//  		}
 		
 		$selections =['g.product_id','p.product_name', 'u.unit_name','sum(cost_amount) sum_cost_amount','sum(g.quantity) sum_quantity','sum(cost_price) sum_cost_price','sum(g.price) sum_price','sum(g.amount) sum_amount'];
 		
 		$conditions = "g.delete_flag =:dflag and g.origin_type=:type and g.customer_id = :cid and e.pid = :pid";
 		
 		$params = array (
 				':dflag' => EnumOther::NO_DELETE,
 				':type' => EnumOriginType::origin_sale_out_market,
 				':cid'=> EnumOther::DEFAULT_CUSTOMER,
 				':pid' => $goods_id
 		);
 		
 		$query = new Query();
 		
 		$query->select($selections)
 		->from("$this->_table g")
 		->innerJoin("product_extend e","e.product_id = g.product_id and e.goods_type != ".EnumOther::BASE_GOODS)
 		->innerJoin("product p", "e.product_id = p.product_id")
 		->innerJoin("unit u","u.unit_id = p.quantity_unit")
 		->where($conditions, $params)
 		->groupBy("g.product_id");
 		
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','origin_time',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		
//  		if(isset($condition['id']) && !empty($condition['id'])){
//  			$query->andwhere(['=','e.pid', $condition['id']]);
//  		}
//  		if(isset($condition['name']) && !empty($condition['name'])){
//  			$query->andwhere(['like','p.product_name', $condition['name']]);
//  		}
//  		dd(getSql($query));
//  		$result ['count'] = $query->count ();
//  		if($limit){
//  			$query->offset ( $offset )->limit ( $limit );
//  		}
 		
 		$result  = $query->all ();
//  		$result ['page'] = array (
//  				'page' => isset($pageInfo ['page'])?$pageInfo ['page']:0,
//  				'pageSize' => isset($pageInfo ['pageSize'])?$pageInfo ['pageSize']:0,
//  		);
//  		dd($result);
 		return $result;
 	}
 }