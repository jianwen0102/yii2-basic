<?php
 namespace app\dao;
 
 /**
  * @desc 请款单功能
  */
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
use yii\base\Object;
		 
 class VendorPaymentDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = "vendor_payment";
 		$this->_pKey = "vendor_payment_id";
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%vendor_payment}}';
 	}
 	
 	
 	/**
 	 * @desc 获取请款单列表
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function getVendorPayment($condition, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections ="v.vendor_payment_id, vendor_payment_no, payment_date,supplier_name,employee_name, v.remark,v.total_amount";
 		
 		$conditions = "v.delete_flag = :flag";
 		$params = [
 				':flag' => EnumOther::NO_DELETE
 		];
 		
 		if(isset($condition['vendor']) && !empty($condition['vendor'])){
 			$conditions .=" and v.vendor_id =:vid";
 			$params[':vid'] =$condition['vendor'];
 		}
 		
 		$query = new Query();
 		
 		$query->select($selections)
 			->from("$this->_table v")
 			->leftJoin("supplier s","s.supplier_id = v.vendor_id")
 			->leftJoin("employee e","e.employee_id = v.handle_man")
 			->where($conditions, $params)
 			->orderBy(['payment_date' => SORT_ASC]);
 		
 		if (isset ( $condition ['starTime'] ) && ! empty ( $condition ['starTime'] )) {
 			$query->andwhere ( ['between','v.payment_date',strtotime ( $condition ['starTime'] ),strtotime ( $condition ['endTime'] )] );
 		}
 		
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$summary = VendorPaymentSummaryDAO::getInstance()->iselect("summary_time", "vendor_payment_id =:id", [':id' => $list['vendor_payment_id']],'one');
 			$list['summary_time'] = isset($summary['summary_time'])?$summary['summary_time']:0;
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		
 		return $result;
 	}
 	
 	/**
 	 * @desc 根据id 获取请款单单头信息
 	 * @param $id //报价单id
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function getVendorPaymentHead($id)
 	{
 		$selections = "vendor_payment_id, vendor_payment_no, payment_date, h.department, vendor_id, h.account_name, account_bank, account, 
 				remark,h.create_man, h.total_amount, username,h.handle_man,depart_name,supplier_name";
 		$conditions = "vendor_payment_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table h")
 		->leftJoin("admin a","a.id= h.create_man")
 		->leftJoin("department d","h.department = d.depart_id")
 		->leftJoin("supplier s","s.supplier_id = h.vendor_id")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 	
 }