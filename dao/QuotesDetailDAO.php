<?php
 namespace app\dao;
 /**
  * @desc 报价单明细处理类
  * @author liaojianwen
  * @date 2017-03-27
  */
 use app\dao\BaseDAO;
 use yii\db\Query;
 use app\enum\EnumOther;
 
 class QuotesDetailDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'quotes_detail';
 		$this->_pKey ='quotes_det_id';
 	}
 	
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%quotes_detail}}';
 	}
 	
 	
 	/**
 	 * @desc 根据id 获取报价单明细
 	 * @param $id 报价单id
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function getQuotesDet($id)
 	{
 		$selects = "quotes_det_id,quotes_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,
				d.quantity_unit,d.price,d.amount,d.discount";
 		$conditions ="d.quotes_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.quantity_unit")
 		->where($conditions,$params)
 		->all();
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
 		}
 		return $result;
 	
 	}
 }