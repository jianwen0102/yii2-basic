<?php
namespace app\dao;

use yii\db\Query;
use app\enum\EnumOther;
class UnitDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-7
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->_table = 'unit';
		$this->_pKey ='unit_id';
		$this->_name = 'unit_name';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%unit}}';
	}
	
	/**
	 * @desc 获取单位列表
	 * @param  $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-7
	 */
	public function getUnits($pageInfo) {
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "unit_id,unit_name,is_active,description";
		$conditions = "delete_flag =:flag";
		$params = array (
				':flag' => EnumOther::NO_DELETE,
		);
		
		$query = new Query ();
		$query->select ( $selections )->from ( $this->_table )->where ( $conditions, $params )->orderBy ( 'create_time ASC' );
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize'] 
		);
		return $result;
	}
	

}