<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use yii\db\Query;
 use Yii;
 use app\enum\EnumOther;
 use yii\base\Object;
	 
 class PriceAdjustmentDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-07-11
 	 */
 	public  function __construct()
 	{
 		parent::__construct();
 		$this->_table ="price_adjustment";
 		$this->_pKey ="adjustment_id";
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%price_adjustment}}';
 	}
 	
 	/**
 	 * @desc 获取成本调整单列表
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustments($condition, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections ="adjustment_id, adjustment_no, adjustment_date,employee_name, a.remark,a.confirm_flag";
 			
 		$conditions = "a.delete_flag = :flag";
 		$params = [
 				':flag' => EnumOther::NO_DELETE
 		];
 			
//  		if(isset($condition['vendor']) && !empty($condition['vendor'])){
//  			$conditions .=" and a.vendor_id =:vid";
//  			$params[':vid'] =$condition['vendor'];
//  		}
 			
 		$query = new Query();
 			
 		$query->select($selections)
 		->from("$this->_table a")
 		->leftJoin("employee e","e.employee_id = a.handle_man")
 		->where($conditions, $params)
 		->orderBy(['adjustment_date' => SORT_ASC]);
 			
 		if (isset ( $condition ['starTime'] ) && ! empty ( $condition ['starTime'] )) {
 			$query->andwhere ( ['between','a.adjustment_date',strtotime ( $condition ['starTime'] ),strtotime ( $condition ['endTime'] )] );
 		}
 			
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 			
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 根据id 获取成本调整单单头信息
 	 * @param $id //报价单id
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustmentHead($id)
 	{
 		$selections = "adjustment_id, adjustment_no, adjustment_date,remark,h.create_man, username,h.handle_man,confirm_flag";
 		$conditions = "adjustment_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table h")
 		->leftJoin("admin a","a.id= h.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }