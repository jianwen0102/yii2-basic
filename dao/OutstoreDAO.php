<?php
namespace app\dao;
/**
 * desc 出库单单头表出来类 outstore
 */

use app\dao\BaseDAO;
use app\enum\EnumOther;
use app\enum\EnumOriginType;
use app\helpers\Utility;
use yii\db\Query;

class OutstoreDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'outstore';
		$this->_pKey ='outstore_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%outstore}}';
	}
	
	/**
	 * @desc 获取出库单列表数据
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function getOutstores($condition, $filter, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "i.outstore_id,outstore_no,outstore_date,vendor_id,i.warehouse_id,warehouse_name,outstore_type,i.remark,
				supplier_name,outstore_man,employee_name,i.confirm_flag";
		$conditions = "i.delete_flag =:flag";
		$params = array (
				':flag' => EnumOther::NO_DELETE
		);
	
// 		if(isset($condition['vendor']) && !empty($condition['vendor'])){
// 			$conditions .=" and i.vendor_id =:vid";
// 			$params[':vid'] =$condition['vendor'];
// 		}
		if(isset($condition['warehouse']) && !empty($condition['warehouse'])){
			$conditions .=" and i.warehouse_id =:wid";
			$params[':wid'] = $condition['warehouse'];
		}
		switch ($filter){
			case 0 :
				break;
			case 1:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::NO_CONFIRM;
				break;
			case 2:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::CONFIRM;
				break;
		}
		$query = new Query ();
		$query->select ( $selections )
		->from ("$this->_table i")
		->leftJoin("warehouse w","i.warehouse_id = w.warehouse_id")
		->leftJoin("supplier s","i.vendor_id = s.supplier_id")
		->leftJoin("employee e","e.employee_id = i.outstore_man")
		->where ( $conditions, $params )
		->orderBy ( ['i.confirm_flag'=>SORT_ASC,'i.create_time'=>SORT_DESC]);
	
		if(isset($condition['starTime']) && !empty($condition['starTime'])){
			$query->andwhere(['between','outstore_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		foreach ($result['list'] as &$list){
			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$outstore_type, $list['outstore_type']);
		}
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	/**
	 * @desc 根据id 获取出库单单头信息
	 * @param $id //出库单id
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getOutstoreHead($id)
	{
		$selections = "outstore_id,outstore_no,outstore_date,vendor_id,warehouse_id,outstore_type,remark,
				vendor_id,outstore_man,confirm_flag,create_man,username,outstore_amount";
		$conditions = "outstore_id =:id";
		$params = array (
				':id' => $id,
		);
		$query = new Query ();
		$result = $query->select ( $selections )
		->from ("$this->_table o")
		->leftJoin("admin a","a.id = o.create_man")
		->where ( $conditions, $params )
		->one();
		return $result;
	}
}