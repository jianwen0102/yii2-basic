<?php
namespace app\dao;

use yii\db\ActiveRecord;

class UsersDao extends ActiveRecord
{
	private static $_instance = null;
	public static function tableName(){
		return '{{%users}}';
	}	
	
	/**
	 * @return \app\models\dao\UsersDao
	 */
	public static function getInstance()
	{
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}
	
		return self::$_instance;
	}

	/**
	 * @desc 检查用户名和密码
	 * @param string $username
	 * @param string $pwd
	 * @author liaojianwen
	 * @date 2016-10-28
	 */
	public function check($username,$pwd)
	{
		$userInfo = $this->find ()->where (['username' =>$username,'password' => $pwd])->asArray()->one ();
		return $userInfo;
	}
}