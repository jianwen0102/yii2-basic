<?php
 namespace app\dao;
 /**
  * @desc 采购退货单头操作类
  * @author liaojianwen
  * @date 2017-04-27
  */
 use app\dao\BaseDAO;
 use yii\db\Query;
 use Yii;
 use app\enum\EnumOriginType;
 use app\enum\EnumOther;
 
 class PurchaseReturnDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-04-27
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'purchase_return';
 		$this->_pKey ='purchase_return_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%purchase_return}}';
 	}
 	
 	
 	/**
 	 * @desc 采购入库单列表
 	 * @param [] $pageInfo
 	 * @params [] $condition
 	 * @author liaojianwen
 	 * @date 2016-12-14
 	 */
 	public function getPurchaseReturn($cond, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selects = "purchase_return_id,purchase_return_no,purchase_return_man,vendor_id,p.warehouse_id,purchase_return_date,
				supplier_name,employee_name,warehouse_name,p.remark,p.confirm_flag";
 	
 		$conditions = "p.delete_flag = :flag";
 		$params  =[
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		if(isset($cond['vendor']) && !empty($cond['vendor'])){
 			$conditions .=" and p.vendor_id =:vid";
 			$params[':vid'] =$cond['vendor'];
 		}
 		if(isset($cond['warehouse']) && !empty($cond['warehouse'])){
 			$conditions .=" and p.warehouse_id =:wid";
 			$params[':wid'] = $cond['warehouse'];
 		}
 	
 		$query = new Query();
 		$query->select($selects)
 		->from("$this->_table p")
 		->leftJoin("employee e", "e.employee_id = p.purchase_return_man")
 		->leftJoin("supplier s", "s.supplier_id = p.vendor_id")
 		->leftJoin("warehouse w", "w.warehouse_id = p.warehouse_id")
 		->where($conditions,$params)
 		->orderBy ( ['p.confirm_flag'=>SORT_ASC,'p.create_time'=>SORT_DESC]);
 	
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','p.purchase_return_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 	
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	
 	}
 	
 	/**
 	 * @desc 根据id 获取采购入库单单头信息
 	 * @param $id //采购单id
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function getPurchaseReturnHead($id)
 	{
 		$selections = "p.purchase_return_id,purchase_return_no,purchase_return_date,p.vendor_id,p.warehouse_id,p.remark,c.purchase_no,p.purchase_id,
				purchase_return_man,p.create_man,username,purchase_return_type,p.confirm_flag,p.purchase_return_amount,s.supplier_name vendor_name";
 		$conditions = "p.purchase_return_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("admin a","a.id= p.create_man")
 		->leftJoin("purchase c","c.purchase_id = p.purchase_id")
 		->leftJoin("supplier s","s.supplier_id = p.vendor_id")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }