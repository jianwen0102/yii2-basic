<?php
namespace app\dao;
/**
 * @desc 采购入库单头表操作
 * @author liaojianwen
 * @date 2016-12-14
 */
use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;
use yii\base\Object;
use app\dao\ProductUnitDAO;

class PurchaseDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'purchase';
		$this->_pKey ='purchase_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%purchase}}';
	}
	
	
	/**
	 * @desc 采购入库单列表
	 * @param [] $pageInfo 
	 * @params [] $condition
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function getPurchases($cond, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selects = "purchase_id,purchase_no,purchase_man,vendor_id,p.warehouse_id,purchase_date,
				supplier_name,employee_name,warehouse_name,p.remark,p.confirm_flag";
		
		$conditions = "p.delete_flag = :flag";
		$params  =[
				':flag'=>EnumOther::NO_DELETE,
		];
		if(isset($cond['vendor']) && !empty($cond['vendor'])){
			$conditions .=" and p.vendor_id =:vid";
			$params[':vid'] =$cond['vendor'];
		}
		if(isset($cond['warehouse']) && !empty($cond['warehouse'])){
			$conditions .=" and p.warehouse_id =:wid";
			$params[':wid'] = $cond['warehouse'];
		}
		
		$query = new Query();
		$query->select($selects)
			->from("$this->_table p")
			->leftJoin("employee e", "e.employee_id = p.purchase_man")
			->leftJoin("supplier s", "s.supplier_id = p.vendor_id")
			->leftJoin("warehouse w", "w.warehouse_id = p.warehouse_id")
			->where($conditions,$params)
			->orderBy ( ['p.confirm_flag'=>SORT_ASC,'p.create_time'=>SORT_DESC]);
		
		if(isset($cond['starTime']) && !empty($cond['starTime'])){
			$query->andwhere(['between','p.purchase_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
		
	}
	
	/**
	 * @desc 根据id 获取采购入库单单头信息
	 * @param $id //采购单id
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function getPurchaseHead($id)
	{
		$selections = "p.purchase_id,purchase_no,purchase_date,p.vendor_id,p.warehouse_id,p.remark,c.procurement_no,
				purchase_man,p.create_man,username,p.procurement_id,purchase_type,confirm_flag,p.purchase_amount,s.supplier_name vendor_name";
		$conditions = "p.purchase_id =:id";
		$params = array (
				':id' => $id,
		);
		$query = new Query ();
		$result = $query->select ( $selections )
		->from ("$this->_table p")
		->leftJoin("admin a","a.id= p.create_man")
		->leftJoin("procurement c","c.procurement_id = p.procurement_id")
		->leftJoin("supplier s","s.supplier_id = p.vendor_id")
		->where ( $conditions, $params )
		->one();
		return $result;
	}
	
	/**
	 * @desc 查询可导入的采购入库
	 * @author liaojianwen
	 * @date 2017-04-27
	 */
	public function selectPurchase($cond, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selects ="purchase_id,purchase_no,purchase_date,
				supplier_name,purchase_amount,p.remark";
		$conditions = "p.delete_flag = :flag and p.confirm_flag = :cflag and p.finish_flag =:fflag";
 		$params  =[
 				':flag'=>EnumOther::NO_DELETE,
 				':cflag'=>EnumOther::CONFIRM,
 				':fflag'=>EnumOther::NO_FINISHED, 
 		];
	
 		if(isset($cond['vendor']) && !empty($cond['vendor'])){
 			$conditions .=" and p.vendor_id =:vid";
 			$params[':vid'] =$cond['vendor'];
 			// 			$query->andwhere(['like','s.supplier_name',$cond['vendor']]);
 		}
		$query = new Query();
		$query ->select($selects)
		->from("$this->_table p")
		->leftJoin("supplier s","s.supplier_id=p.vendor_id")
		->where($conditions,$params)
		->orderBy(['p.create_time'=>SORT_DESC]);
	
		
		
		if(isset($cond['starTime']) && !empty($cond['starTime'])){
			$query->andwhere(['between','p.purchase_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
	
	
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	
	/**
	 * @desc 删除采购退货单更新采购入库单信息
	 * @author liaojianwen
	 * @date 2017-05-04
	 */
	public function updatePurchase($purchase_id, $purchase_det_id, $quantity)
	{
		$flag = $this->findByAttributes('finish_flag','purchase_id = :pid',[':pid'=>$purchase_id]);
		if($flag['finish_flag']){
			$res_hed = $this->updateByPk($purchase_id, ['finish_flag'=> 0]);
			if(!$res_hed){
				return false;
			}
		}
	
		$rest = PurchaseDetailDAO::getInstance ()->updateAllCounters ( [
				'return_quantity' => - $quantity,
				'minus_quantity' => $quantity
		], 'purchase_det_id =:pid ', [
				':pid' => $purchase_det_id,
		] );
		if(!$rest){
			return false;
		}
			
		$det_flag = PurchaseDetailDAO::getInstance()->findByAttributes('finish_flag','purchase_det_id = :did',[':did'=>$purchase_det_id]);
		if($det_flag['finish_flag']){
			$res_det = PurchaseDetailDAO::getInstance()->updateByPk($purchase_det_id, ['finish_flag'=> 0]);
			if(!$res_det){
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @desc 获取入库单明细
	 * @author liaojianwen
	 * @date 2017-11-01
	 */
	public function getPurchaseByVendor($vendor, $condition)
	{
// 		$limit = $pageInfo ['pageSize'];
//  		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "h.purchase_date,d.goods_id,p.product_name,d.base_quantity quantity,d.base_unit quantity_unit,d.amount,d.remark,d.purchase_id,
 				h.purchase_no,u.unit_name,p.product_id,d.purchase_det_id";
 		$conditions = "h.delete_flag =:flag and h.vendor_id = :vendor and h.finish_flag = :fflag and d.pay_finish =:pflag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE,
 				':vendor' => $vendor,
 				':fflag'=> EnumOther::NO_FINISHED,
 				':pflag' => EnumOther::NO_FINISHED,
 		);
 		
 		$query = new Query();
 		$query->select($selections)
 			->from("$this->_table h")
 			->leftJoin("purchase_detail d","h.purchase_id = d.purchase_id")
 			->leftJoin("product p","p.product_id = d.goods_id")
 			->leftJoin("unit u","u.unit_id = d.base_unit")
 			->where($conditions,$params)
 			->orderBy(['d.create_time' => SORT_ASC]);
 		
 		
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','purchase_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		if(isset($condition['name']) && !empty($condition['name'])){
 			$query->andWhere(['like', 'purchase_no',$condition['name']]);
 		}
//  		$result ['count'] = $query->count ();
//  		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		$result ['list'] = $query->all ();
 		foreach ($result['list'] as &$list){
 			$res_unit = ProductUnitDAO::getInstance()->getUnits($list['product_id']);
 			$list['product_unit'] = $res_unit;
 			$list['price'] = round($list['amount'] / $list['quantity']);
 			
 		}
//  		$result ['page'] = array (
//  				'page' => $pageInfo ['page'],
//  				'pageSize' => $pageInfo ['pageSize']
//  		);
 		
 		return $result;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}