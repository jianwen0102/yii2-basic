<?php
namespace app\dao;
use Yii;
use app\dao\BaseDAO;
use yii\db\Query;

class StoreAdminDAO extends BaseDAO
{
     /**
	 * @desc   构造函数
	 * @author hehauwei
	 * @date   2017-7-21
	 */
	public function __construct()
	{
	   parent::__construct();
	   $this->_table = 'store_admin';
	   $this->_pKey  = 'id';
	}
	
	public static function tableName()
	{	
	   return '{{%store_admin}}';	
	}
	
	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */	
	public function getStoreAdminListData($pageInfo,$searchData,$filter){
	   $page     = isset($pageInfo['page']) ? $pageInfo['page'] : 1;
	   $pageSize = isset($pageInfo['pageSize']) ? $pageInfo['pageSize'] : 15;
	   $limit    = $pageSize;
	   $offset   = isset($page) ? ($page - 1) * $limit : 0;
	   $query    = new Query();

	   $query->select('id,username,store_id,email,created_at,updated_at,delete_flag')->from($this->_table)->where("1")->orderBy('id DESC');

	   if(isset($searchData['username']) && strlen(trim($searchData['username']))>0){//供应商查询
	      $username = trim($searchData['username']);
          $query->andWhere(['like','username',$username]);
	   }

	   if(isset($filter['delete_flag']) && $filter['delete_flag'] != '-1'){
	        $query->andWhere('delete_flag=:delete_flag',[':delete_flag'=>$filter['delete_flag']]);	   
	   }

	   $result['count'] = $query->count();
	   $result['list']  = $query->offset($offset)->limit($limit)->all();	   
	   $result['pageinfo']  = array (
			 'page'     => $page,
			 'pageSize' => $pageSize
	   );
	   return $result;	
	}

}