<?php
 namespace app\dao;
 /**
  * @desc 销售订单头操作类
  */
 use app\dao\BaseDAO;
 use yii\db\Query;
 use Yii;
 use app\enum\EnumOther;
 
 class SellDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'sell';
 		$this->_pKey ='sell_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%sell}}';
 	}
 	
 	/**
 	 * @desc 销售定单列表
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function getSells($cond, $pageInfo, $filter)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selects = "sell_id,sell_no,s.handle_man,s.customer_id,s.warehouse_id,sell_date,employee_name,
 				s.remark,s.finish_flag,w.warehouse_name,c.client_name,s.confirm_flag";
 	
 		$conditions = "s.delete_flag = :flag";
 		$params = [
 				':flag' => EnumOther::NO_DELETE
 		];
 		if (isset ( $cond ['customer'] ) && ! empty ( $cond ['customer'] )) {
//  			$query->andwhere(['like','p.customer',$cond['customer']]);
 		}
 		$conditions .=" and s.confirm_flag = :cflag";
 		switch ($filter) {
 			case 2:
 				$params[':cflag'] = EnumOther::CONFIRM;
 			break;
 			default:
 				$params[':cflag'] = EnumOther::NO_CONFIRM;
 			break;
 		}
 		
 		$query = new Query ();
 		$query->select ( $selects )->from ( "$this->_table s" )
 		->leftJoin ( "employee e", "e.employee_id = s.handle_man" )
//  		->leftJoin ( "supplier c", "s.customer_id =c.supplier_id")
		->leftJoin("client c","c.client_id = s.customer_id")
 		->leftJoin ( "warehouse w", "w.warehouse_id = s.warehouse_id")
 		->where ( $conditions, $params )
 		->orderBy ( [
 				's.finish_flag' => SORT_ASC,
 				's.confirm_flag' => SORT_ASC,
 				's.create_time' => SORT_DESC
 		] );
 	
 		if (isset ( $cond ['starTime'] ) && ! empty ( $cond ['starTime'] )) {
 			$query->andwhere ( ['between','p.sell_date',strtotime ( $cond ['starTime'] ),strtotime ( $cond ['endTime'] )] );
 		}
 	
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 	
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 根据id 获取报价单单头信息
 	 * @param $id //报价单id
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function getSellHead($id)
 	{
 		$selections = "p.sell_id,sell_no,sell_date,p.customer_id,p.warehouse_id,p.remark,confirm_flag,
				handle_man,p.create_man,username,sell_amount,finish_flag,discount_amount,total_amount,total_discount";
 		$conditions = "p.sell_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("admin a","a.id= p.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 	
 	
 	/**
 	 * @desc 查询可导入的销售订单
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function selectSell($cond, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selects ="sell_id,sell_no,sell_date,
				client_name,sell_amount,p.remark";
 		$conditions = "p.delete_flag = :flag and p.finish_flag = :fflag and p.confirm_flag =:cflag";
 		$params  =[
 				':flag'=>EnumOther::NO_DELETE,
 				':fflag'=>EnumOther::NO_FINISHED,
 				':cflag' => EnumOther::CONFIRM,
 		];
 	
 		$query = new Query();
 		$query ->select($selects)
 		->from("$this->_table p")
//  		->leftJoin("supplier s","s.supplier_id=p.customer_id")
		->leftJoin("client s","s.client_id = p.customer_id")
 		->where($conditions,$params)
 		->orderBy(['sell_date'=>SORT_DESC]);
 		
 		/*客户*/
 		if(isset($cond['cust']) && !empty($cond['cust'])){
 			$query->andwhere(['like','s.client_name',$cond['cust']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 删除出库单更新采购单信息
 	 * @author liaojianwen
 	 * @date 2017-04-19
 	 */
 	public function updateSell($sell_id, $sell_det_id, $quantity)
 	{
 		$flag = $this->findByAttributes('finish_flag','sell_id = :pid',[':pid'=>$sell_id]);
 		if($flag['finish_flag']){
 			$res_hed = $this->updateByPk($sell_id, ['finish_flag'=> 0]);
 			if(!$res_hed){
 				return false;
 			}
 		}
 		
 		$rest = SellDetailDAO::getInstance ()->updateAllCounters ( [
 				'shipped_quantity' => - $quantity,
 				'minus_quantity' => $quantity
 		], 'sell_det_id =:pid ', [
 				':pid' => $sell_det_id,
 		] );
 		
 		if(!$rest){
 			return false;
 		}
 		$det_flag = SellDetailDAO::getInstance()->findByAttributes('finish_flag','sell_det_id = :did',[':did'=>$sell_det_id]);
 		if($det_flag['finish_flag']){
	 		$res_det = SellDetailDAO::getInstance()->updateByPk($sell_det_id, ['finish_flag'=> 0]);
	 		if(!$res_det){
	 			return false;
	 		}
 		}
 		return true;
 	}
 }