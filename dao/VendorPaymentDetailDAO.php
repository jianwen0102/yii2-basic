<?php
 namespace app\dao;
 
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
use yii\base\Object;
	 
 class VendorPaymentDetailDAO extends BaseDAO 
 {
 	
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		
 		$this->_table = "vendor_payment_detail";
 		$this->_pKey = "vendor_payment_det_id";
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%vendor_payment_detail}}';
 	}
 	
 	/**
 	 * @desc 根据id 获取请款单明细信息
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function getVendorPaymentDetail($id)
 	{
 		$selects = "vendor_payment_det_id, vendor_payment_id, purchase_time, d.product_id, p.product_name,purchase_id,purchase_det_id,d.quantity_unit,
 				u.unit_name, d.price, d.amount, d.quantity, d.remark";
 		$conditions ="d.vendor_payment_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.quantity_unit")
 		->where($conditions,$params)
 		->orderBy(['d.purchase_time' => SORT_ASC])
 		->all();
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
//  			$res_quantity = StockPileDAO::getInstance()->findByAttributes('quantity','product_id = :pid and warehouse_id =:wid',[':pid'=>$res['product_id'],':wid'=>$res['warehouse_id']]);
//  			$res['product_quantity'] = isset($res_quantity['quantity']) ? $res_quantity['quantity']:0;
 		}
 		return $result;
 	}
 	
 }