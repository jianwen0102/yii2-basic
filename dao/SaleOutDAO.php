<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
 use yii\db\Query;
 use app\helpers\Utility;
 use app\enum\EnumOriginType;

 class SaleOutDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'saleout';
 		$this->_pKey ='saleout_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%saleout}}';
 	}
 	
 	/**
 	 * @desc 获取销售出库列表信息
 	 * @param $condition [] 查询条件
 	 * @param $filter string 过滤条件
 	 * @param $pageInfo [] 页面信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function getSaleOut($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "i.saleout_id,saleout_no,i.saleout_date,i.warehouse_id,warehouse_name,i.remark,i.handle_man,employee_name,i.confirm_flag,customer_id,
 				client_name,saleout_type";
 		$conditions = "i.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE
 		);
 	
 		if(isset($condition['warehouse']) && !empty($condition['warehouse'])){
 			$conditions .=" and i.warehouse_id =:wid";
 			$params[':wid'] = $condition['warehouse'];
 		}
 		switch ($filter){
 			case 0 :
 				break;
 			case 1:
 				$conditions .=" and i.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::NO_CONFIRM;
 				break;
 			case 2:
 				$conditions .=" and i.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::CONFIRM;
 				break;
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("warehouse w","i.warehouse_id = w.warehouse_id")
 		->leftJoin("employee e","e.employee_id = i.handle_man")
//  		->leftJoin("supplier s","s.supplier_id = i.customer_id")
		->leftJoin("client c","c.client_id = i.customer_id")
 		->where ( $conditions, $params )
 		->orderBy ( ['i.confirm_flag'=>SORT_ASC,'i.create_time'=>SORT_DESC]);
 	
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','saleout_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$sale_type, $list['saleout_type']);
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取销售出库单信息
 	 * @author liaojianwen
 	 * @date 2017-02-06
 	 */
 	public function getSaleOutHead($id)
 	{
 		$selections = "i.saleout_id,saleout_no,saleout_date,i.warehouse_id,i.remark,saleout_type,i.customer_id,s.client_name,i.sell_id,c.sell_no,
				i.handle_man,i.confirm_flag,i.create_man,username,i.total_amount,i.discount_amount,i.total_discount,i.saleout_amount";
 		$conditions = "i.saleout_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("admin a","a.id= i.create_man")
//  		->leftJoin("supplier s","s.supplier_id = i.customer_id")
		->leftJoin("client s","s.client_id = i.customer_id")
 		->leftJoin("sell c","c.sell_id = i.sell_id")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 	
 	
 	/**
 	 * @desc 赠送明细
 	 * @author liaojianwen
 	 * @date 2017-03-29
 	 */
 	public function getSaleoutFree($cond,$pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "h.saleout_date,d.product_id,h.customer_id,h.saleout_id,d.minus_quantity base_quantity,d.base_unit,
 				s.client_name,g.price,u.unit_name,g.product_name,h.remark,h.saleout_no";
 			
 		$conditions = "d.delete_flag = :flag and h.confirm_flag = :cflag and d.minus_quantity > :quan";
 		$params =[
 				':flag'=>EnumOther::NO_DELETE,
 				':cflag' => EnumOther::CONFIRM,
 				':quan' => 0,
 		];
 			
 		if(isset($cond['customer']) && !empty($cond['customer'])){
 			$conditions .=" and h.customer_id =:vid";
 			$params[':vid'] =$cond['customer'];
 		}
 		
 		if(isset($cond['id']) && $cond['id']){
 			$conditions .=" and g.product_id =:id";
 			$params[':id'] = $cond['id'];
 		}
 		
 		$query = new Query();
 		$query->select($selections)
 		->from("$this->_table h")
 		->leftJoin("saleout_detail d","h.saleout_id = d.saleout_id")
 		->leftJoin("product g","d.product_id = g.product_id")
//  		->leftJoin("supplier s","s.supplier_id = h.customer_id")
		->leftJoin("client s","s.client_id = h.customer_id")
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params);
 			
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','saleout_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		
 		if(isset($cond['name']) && !empty($cond['name'])){
 			$query->andWhere(['like','g.product_name', $cond['name']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 			
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 			
 	}
 	
 	/**
 	 * @desc 赠送明细导出数据
 	 * @author liaojianwen
 	 * @date 2017-03-30
 	 */
 	public function getSaleoutExport($cond)
 	{
 		$selections = "h.saleout_date,d.product_id,h.customer_id,h.saleout_id,d.minus_quantity base_quantity,d.base_unit,
 				s.client_name,g.price,u.unit_name,g.product_name,h.remark, (d.minus_quantity * g.price) gamount";
 		
 		$conditions = "d.delete_flag = :flag and d.minus_quantity > :quan";
 		$params =[
 				':flag'=>EnumOther::NO_DELETE,
 				':quan'=>0
 		];
 		
 		if(isset($cond['customer']) && !empty($cond['customer'])){
 			$conditions .=" and h.customer_id =:vid";
 			$params[':vid'] =$cond['customer'];
 		}
 		if(isset($cond['id']) && !empty($cond['id'])){
 			$conditions .=" and g.product_id =:pid";
 			$params[':pid'] =$cond['id'];
 		}
 		$query = new Query();
 		$query->select($selections)
 		->from("$this->_table h")
 		->leftJoin("saleout_detail d","h.saleout_id = d.saleout_id")
 		->leftJoin("product g","d.product_id = g.product_id")
//  		->leftJoin("supplier s","s.supplier_id = h.customer_id")
		->leftJoin("client s","s.client_id = h.customer_id")
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params);
 		
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','saleout_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		
 		if(isset($cond['name']) && !empty($cond['name'])){
 			$query->andwhere(['like','g.product_name',$cond['name']]);
 		}
 		$result['list'] = $query->all ();
 		
 		$total_amount = 0;
 		foreach ($result['list'] as $res){
 			$total_amount += round($res['gamount'],2);
 		}
 		$result['total_amount'] = $total_amount;
 		return $result;
 	}
 	
  	/**
 	 * @desc 赠送明细
 	 * @author liaojianwen
 	 * @date 2017-03-29
 	 */
 	public function getSaleoutCount($cond,$pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "h.saleout_date,d.product_id,h.customer_id,h.saleout_id,d.base_quantity,d.base_unit,
 				s.client_name_name,d.price,u.unit_name,g.product_name,h.remark,h.saleout_no,d.amount";
 			
 		$conditions = "d.delete_flag = :flag";
 		$params =[
 				':flag'=>EnumOther::NO_DELETE,
 		];
 			
 		if(isset($cond['customer']) && !empty($cond['customer'])){
 			$conditions .=" and h.customer_id =:vid";
 			$params[':vid'] =$cond['customer'];
 		}
 		$query = new Query();
 		$query->select($selections)
 		->from("$this->_table h")
 		->leftJoin("saleout_detail d","h.saleout_id = d.saleout_id")
 		->leftJoin("product g","d.product_id = g.product_id")
//  		->leftJoin("supplier s","s.supplier_id = h.customer_id")
		->leftJoin("client s","s.client_id = h.customer_id")
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params);
 			
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','saleout_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 			
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 			
 	}
 	
 	/**
 	 * @desc 查询可导入的销售出库单(退货单)
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function selectSaleOut($cond, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selects ="saleout_id,saleout_no,saleout_date,
				client_name,saleout_amount,p.remark";
 		$conditions = "p.delete_flag = :flag and p.confirm_flag = :cflag and p.finish_flag =:fflag";
 		$params  =[
 				':flag'=>EnumOther::NO_DELETE,
 				':cflag'=>EnumOther::CONFIRM,
 				':fflag'=>EnumOther::NO_FINISHED, 
 		];
 	
 		if(isset($cond['cust']) && !empty($cond['cust'])){
 			$conditions .=" and p.customer_id =:vid";
 			$params[':vid'] =$cond['cust'];
 		}
 		$query = new Query();
 		$query ->select($selects)
 		->from("$this->_table p")
//  		->leftJoin("supplier s","s.supplier_id=p.customer_id")
		->leftJoin("client s","s.client_id = p.customer_id")
 		->where($conditions,$params);
//  		if(isset($cond['cust']) && !empty($cond['cust'])){
//  			$query->andWhere(['like','s.supplier_name',$cond['cust']]);
//  		}
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','saleout_date',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 	
 	
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 删除退货单更新出库单信息
 	 * @author liaojianwen
 	 * @date 2017-04-26
 	 */
 	public function updateSaleOut($saleout_id, $saleout_det_id, $quantity)
 	{
 		$flag = $this->findByAttributes('finish_flag','saleout_id = :pid',[':pid'=>$saleout_id]);
 		if($flag['finish_flag']){
 			$res_hed = $this->updateByPk($saleout_id, ['finish_flag'=> 0]);
 			if(!$res_hed){
 				return false;
 			}
 		}
 			
 		$rest = SaleOutDetailDAO::getInstance ()->updateAllCounters ( [
 				'return_quantity' => - $quantity,
 				'minus_quantity' => $quantity
 		], 'saleout_det_id =:pid ', [
 				':pid' => $saleout_det_id,
 		] );
 			
 		if(!$rest){
 			return false;
 		}
 		$det_flag = SaleOutDetailDAO::getInstance()->findByAttributes('finish_flag','saleout_det_id = :did',[':did'=>$saleout_det_id]);
 		if($det_flag['finish_flag']){
 			$res_det = SaleOutDetailDAO::getInstance()->updateByPk($saleout_det_id, ['finish_flag'=> 0]);
 			if(!$res_det){
 				return false;
 			}
 		}
 		return true;
 	}
 }