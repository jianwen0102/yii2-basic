<?php
namespace app\dao;
/**
 * @desc 用户分组对应关系表
 * @author liaojianwen
 * @date 2017-01-07
 */
use app\dao\BaseDAO;

class AuthAssignmentDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'auth_assignment';
	}
	
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%auth_assignment}}';
	}
	
	
}