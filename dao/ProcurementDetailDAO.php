<?php
namespace app\dao;
/**
 * @desc 采购明细表操作类
 * @author liaojianwen
 * @date 2016-12-09
 */
use yii\db\Query;
use app\dao\BaseDAO;
use app\enum\EnumOther;

class ProcurementDetailDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'procurement_detail';
		$this->_pKey ='procurement_det_id';
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%procurement_detail}}';
	}
	
	/**
	 * @desc 根据id 获取采购单明细
	 * @param $id 采购单id
	 * @author liaojianwen
	 * @date 2016-12-12
	 */
	public function getProcurementDet($id)
	{
		$selects = "procurement_det_id,procurement_id,goods_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,sell_id,sell_det_id,base_price,
				d.quantity_unit,d.price,d.amount,d.received_quantity,d.minus_quantity,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content,gross_unit,gross_quantity";
		$conditions ="d.procurement_id = :id and d.delete_flag = :flag";
		$params = [
				':id'=>$id,
				':flag'=>EnumOther::NO_DELETE,
		];
		$query = new Query();
		$result = $query->select($selects)
		->from("$this->_table d")
		->leftJoin("product p",'d.goods_id = p.product_id')
		->leftJoin("unit u","u.unit_id = d.base_unit")
		->where($conditions,$params)
		->all();
		foreach ($result as &$res){
			$res_units = ProductUnitDAO::getInstance()->getUnits($res['goods_id'] );
			$res['product_unit'] = $res_units;
		}
		return $result;
	
	}
	
	/**
	 * @desc 确认采购订单明细已收到数量，确认是否完成
	 * @param $proid 采购订单id
	 * @param  [ 'purchase_no' => string '20161219090249' (length=14)
	 *			 'goods_id' => string '8' (length=1)
	 *			 'quantity' => string '25' (length=2)
	 *			 'purchase_type' => string '1' (length=1)
	 *			 'purchase_det_id' => string '10' (length=2)
	 *			 'warehouse_id' => string '1' (length=1)
	 *			 'procurement_det_id' => string '6' (length=1)
	 *			 'minus_quantity' => string '25' (length=2)
	 *			 'received_quantity' => string '25' (length=2)
	 *		  ]
	 * @author liaojianwen
	 * @date 2016-12-19
	 */
	public function updateFinish($proid, $det)
	{
		$pro_det = ProcurementDetailDAO::getInstance ()->findByAttributes ( "minus_quantity,received_quantity,base_quantity,finish_flag", "procurement_det_id =:detid", [ 
				':detid' => $det ['procurement_det_id'] 
		] );
	
		$total_quantity = $pro_det ['received_quantity'] + $det ['base_quantity'];
		if ($total_quantity > $pro_det ['base_quantity']) {
			//现在的值加上原来单据上的超过了未收的数量
			$result ['Ack'] = 'error';
			$result ['msg'] = 'total quantity is lt received_quantity';
			return $result;
		} else if ($total_quantity == $pro_det ['base_quantity']) {
			//现在数量加上单据上的数量 刚好等于未收数量//标记完成
			$detail_flag = ProcurementDetailDAO::getInstance ()->iupdate ( [ 
					'finish_flag' => EnumOther::FINISHED 
			], "procurement_det_id=:pid", [ 
					':pid' => $det ['procurement_det_id']
			] ); 
			if (! $detail_flag) {
				$result ['Ack'] = 'error';
				$result ['msg'] = 'update detail finish_flag fail';
				return $result;
			}
		}
		$condition = "procurement_det_id =:detid";
		$params = [ 
				':detid' => $det ['procurement_det_id'] 
		];
		$received_quantity = $this->updateAllCounters ( [ 
				"received_quantity" => $det ['base_quantity'],
				"minus_quantity"=> -$det['base_quantity'],//未收数量变更为原来单据数量- 已收数量
		], $condition, $params );
		if (! $received_quantity) {
			$result ['Ack'] = 'error';
			$result ['msg'] = 'update received quantity fail';
			return $result;
		}
		
		//查找是否还有未完成的明细，如果有就不用给head 更新finish_flag,如果没有就给head 的finish_flag 打上完成
		$exit_unfinish = ProcurementDetailDAO::getInstance ()->findByAttributes ( "procurement_det_id", "procurement_id=:pid and finish_flag=:flag and delete_flag =:dflag", [ 
				':pid' => $proid,
				':flag' => EnumOther::NO_FINISHED,
				':dflag' => EnumOther::NO_DELETE,
		] );
		if (!$exit_unfinish) {
			$head_flag = ProcurementDAO::getInstance ()->iupdate ( [ 
					'finish_flag' => EnumOther::FINISHED 
			], "procurement_id=:pid", [ 
					':pid' => $proid 
			] ); 
			if(empty($head_flag)) {
				$result ['Ack'] = 'error';
				$result ['msg'] = 'update head finish_flag fail';
				return $result;
			}
		}
		return [ 
				'Ack' => 'success' 
		];
	}
} 