<?php
namespace  app\dao;

use yii\db\Query;
use yii\base\Object;
use app\enum\EnumOther;
class WarehouseDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-3
	 */
	public function __construct() 
	{
		parent::__construct();
		$this->_table = 'warehouse';
		$this->_pKey ='warehouse_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%warehouse}}';
	}
	
	/**
	 * @desc 获取仓库信息
	 * @param [] $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-3
	 */
	public function getWarehouses($pageInfo)
	{
		$limit = $pageInfo['pageSize'];
        $offset = ($pageInfo['page'] - 1) * $limit;
        $selections = "warehouse_id,warehouse_name,is_default,remark,is_scrapped";
        $conditions = "delete_flag =:flag";
        $params = array(
            ':flag' => 0,
        );
        
        $query = new Query();
        $query->select($selections)
            ->from($this->_table)
            ->where($conditions, $params)
            ->orderBy('create_time ASC');
        $result['count'] =  $query->count ();
        $result['list'] = $query->offset($offset)->limit($limit)->all();
        
        $result['page'] = array(
            'page' => $pageInfo['page'],
            'pageSize' => $pageInfo['pageSize'],
        );
        return $result;
	}
	
	/**
	 * @desc 检查是否有重名
	 * @param string $name 仓库名称
	 * @param int $id  仓库代号
	 * @author liaojianwen
	 * @date 2016-11-03
	 * @return boolean
	 */
	public function checkSameName($name,$id)
	{
		$query = new Query();
		$conditions ="warehouse_name = '{$name}' and delete_flag =".EnumOther::NO_DELETE;
		if ($id !== 0 ){
			$conditions .= " and warehouse_id <> {$id}";
		}
		$result = $query->select ( 'warehouse_id' )->from ( "$this->_table" )->where ( $conditions )->all ();
		if (empty ( $result )) {
			return false;
		}
		return true;
	}
}