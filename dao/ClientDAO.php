<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;
use Yii;

class ClientDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'client';
		$this->_pKey ='client_id';
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%client}}';
	}
	
	
	/**
	 * @desc 获取客户资料
	 * @param [] $cond 查询条件
	 * @param [] $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-11
	 */
	public function getClients($cond, $pageInfo)
	{
		$limit = $pageInfo['pageSize'];
		$offset = ($pageInfo['page'] - 1) * $limit;
		$selections = "client_id,client_name,client_desc,country,province,city,district";
		$conditions = "delete_flag =:flag";
		$params = array (
				':flag' => EnumOther::NO_DELETE,
		);
		
		$userId = Yii::$app->user->id;
		$auth = Yii::$app->authManager;
		$role = $auth->getRolesByUser($userId);
		
// 		if(array_key_exists(EnumOther::BUY_ROLE, $role)){//采购角色
// 			$conditions .=" and create_man =:man and type=:type";
// 			$params[':man'] = $userId;
// 			$params[':type'] = EnumOther::SUPPLIER;
// 		}
		if(array_key_exists(EnumOther::SALE_ROLE, $role)){//销售角色
			$conditions .=" and create_man =:man";
			$params[':man'] = $userId;
		}
		
		$query = new Query ();
		$query->select ( $selections )->from ( $this->_table )->where ( $conditions,$params )->orderBy ( 'client_id ASC' );
		if(isset($cond['name']) && $cond['name']){
			$query->andWhere(['like', 'client_name', $cond['name']]);
		}
		
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		foreach ( $result ['list'] as &$val ) {
			$region_str = RegionDAO::getInstance ()->getRegionName ( $val ['province'] );
			$region_str .= '-' . RegionDAO::getInstance ()->getRegionName ( $val ['city'] );
			$region_str .= '-' . RegionDAO::getInstance ()->getRegionName ( $val ['district'] );
			if (substr ( $region_str, - 1, 1 ) == '-') {
				$region_str = substr ( $region_str, 0, - 1 );
			}
			$val ['region_str'] = $region_str;
		}
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize'] 
		);
		return $result;
	}
	
	/**
	 * @desc 保存客户
	 * @param [] $info
	 * @param int $id
	 * @author liaojianwen
	 * @date 2016-11-21
	 */
	public function saveClient($info)
	{
		/*$columns = [
			'client_name'=>$info['sname'],
			'client_desc'=>$info['desc'],
			'handle_man'=>$info['man'],
			'country'=>$info['country'],
			'province'=>$info['province'],
			'city'=>$info['city'],
			'district'=>$info['district'],
			'phone'=> $info['phone'],
			'create_man' => Yii::$app->user->id,
			'store_id' => $info['store_id'],

		];*/
// 		dd($info);
		$columns = [
			'client_name' => $info['sname'],
			'client_desc' => $info['desc'],
			'handle_man'  => isset($info['man']) ? $info['man'] : 0,
			'country'     => isset($info['country']) ? $info['country'] : 0,
			'province'    => isset($info['province']) ? $info['province'] : 0,
			'city'        => isset($info['city']) ? $info['city'] : 0,
			'district'    => isset($info['district']) ? $info['district'] : 0,
			'phone'       => isset($info['phone']) ? $info['phone'] : '',
			'create_man'  => Yii::$app->user->id,
		];
		$id = isset($info['id'])? $info['id']:0;
		if(empty($columns['district'])){
			unset($columns['district']);
		}
		if(empty($id)){
			if(isset($info['store_id'])){
			   $columns['store_id'] = $info['store_id'];
			}
			$columns['create_time'] = time();
			$result = $this->iinsert($columns);
		} else {
			//更新
			$columns['store_id'] = $info['store_id'];
			$columns['modify_time']= time();
			$conditions = "client_id = :id";
			$params = [
				':id'=>$id,
			];
// 			dd($columns);
			$result = $this->iupdate($columns, $conditions,$params);
		}
		return $result;
	}
}