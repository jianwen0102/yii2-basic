<?php
 namespace app\dao;
 /**
  * @desc 应付款结算单操作类
  */
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
 use app\helpers\Utility;
 use app\enum\EnumOriginType;
 
 class SettlePayablesDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-03-03
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'settle_payables';
 		$this->_pKey ='settle_pay_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%settle_payables}}';
 	}
 	
 	/**
 	 * @desc 获取应付结算单列表
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function getSettlePayables($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
//  		$selections = "p.pay_id,pay_time,pay_man,pay_type,vendor_id,origin_no,origin_id,origin_type,finish_flag,pay_amount,
//  				settle_amount,receive_amount,left_amount,p.remark,description,supplier_name,e.employee_name";
		$selections = "p.settle_pay_id,p.settle_pay_no,p.settle_pay_type,p.settle_pay_date,p.vendor_id,p.settle_amount,p.discount_amount,
				p.remark,p.confirm_flag,p.settle_man,s.supplier_name,e.employee_name";
//  		$conditions = "p.delete_flag =:flag";
//  		$params = array (
//  				':flag' => EnumOther::NO_DELETE
//  		);
	 	$conditions = '';
	 	$params =[];
 		if(isset($condition['vendor']) && !empty($condition['vendor'])){
 			$conditions .="p.vendor_id =:vid";
 			$params[':vid'] =$condition['vendor'];
 		}
 		switch ($filter){
			case 0 :
				break;
			case 1:
				$conditions .="p.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::NO_CONFIRM;
				break;
			case 2:
				$conditions .="p.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::CONFIRM;
				break;
		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("supplier s","p.vendor_id = s.supplier_id")
 		->leftJoin("employee e","e.employee_id = p.settle_man")
 		->where ( ["in", 'p.delete_flag',[EnumOther::NO_DELETE]])
 		->orderBy ( ['p.confirm_flag'=>SORT_ASC,'p.create_time'=>SORT_DESC]);
 		
 		
 		$query->andWhere($conditions, $params);
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','settle_pay_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$list['settle_name'] = Utility::getArrayValue(EnumOriginType::$payables_type, $list['settle_pay_type']);
//  			$list['settle_flag'] = Utility::getArrayValue(EnumOther::$settle, $list['finish_flag']);
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 根据id 获取应付结算单头信息
 	 * @param $id //应付结算单头id
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function getSettlePayablesHead($id)
 	{
 		$selections = "i.settle_pay_id,settle_pay_no,settle_pay_date,vendor_id,settle_pay_type,i.remark,
 				settle_man,confirm_flag,create_man,username,settle_amount,discount_amount";
 		$conditions = "i.settle_pay_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("admin a","a.id= i.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }