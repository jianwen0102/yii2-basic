<?php
namespace app\dao;
use Yii;
use app\dao\BaseDAO;
use yii\db\Query;

class StoreMemberDAO extends BaseDAO
{
     /**
	 * @desc   构造函数
	 * @author hehauwei
	 * @date   2017-7-21
	 */
	public function __construct()
	{
	   parent::__construct();
	   $this->_table = 'store_member';
	   $this->_pKey  = 'member_id';
	}
	
	public static function tableName()
	{	
	   return '{{%store_member}}';	
	}
	
	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */	
	public function getMemberListData($pageInfo,$searchData,$filter)
	{
	   $page     = isset($pageInfo['page']) ? $pageInfo['page'] : 1;
	   $pageSize = isset($pageInfo['pageSize']) ? $pageInfo['pageSize'] : 15;
	   $limit    = $pageSize;
	   $offset   = isset($page) ? ($page - 1) * $limit : 0;
	   $query    = new Query();

	   $query->select('member_id,store_id,nickname,cardno,gender,mobile,balance,consume_points,rank_points,join_time')->from($this->_table)->orderBy('member_id DESC');

	   if(isset($searchData['nickname']) && strlen(trim($searchData['nickname']))>0){//供应商查询
	      $nickname = trim($searchData['nickname']);
          $query->andWhere(['like','nickname',$nickname]);
	   }
	   if(isset($searchData['store_id']) && $searchData['store_id']>0){//供应商查询
          $query->andWhere(['=','store_id',$searchData['store_id']]);
	   }

	   $result['count'] = $query->count();
	   $result['list']  = $query->offset($offset)->limit($limit)->all();	   
	   $result['pageinfo']  = array (
			 'page'     => $page,
			 'pageSize' => $pageSize
	   );
	   return $result;	
	}
	
	/**
	 * @desc   保存添加/编辑会员
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveMemberData($data)
	{
		$member['nickname'] = $data['nickname'];
        $member['cardno']   = $data['cardno'];
		$member['mobile']   = $data['mobile'];
		$member['gender']   = $data['gender'];
		$member['store_id'] = $data['store_id'];

		if(empty($data['member_id']) || $data['member_id']<1){	   
		   $this->_created = false;
		   $member['join_time'] = time();
           $result = $this->iinsert($member);

		}else{
		   $this->_updated = false;
		   $conditions =  " member_id=:member_id";
		   $params     = [':member_id'=>$data['member_id']];
		   $result     = $this->iupdate($member,$conditions,$params);
		}
        return $result;
	}

	/**
	 * @desc 检查是否有重名
	 * @param int    $member_id 会员id
	 * @param string $nickname  会员名称
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return boolean
	 */
	public function checkMember($member_id,$nickname)
	{
		$query = new Query();
		$conditions = " nickname = '{$nickname}'";
		if ($member_id > 0 ){
			$conditions .= " and member_id <> {$member_id}";
		}
		$result = $query->select ( 'member_id' )->from ( "$this->_table" )->where ( $conditions )->all ();
		if (empty ( $result )) {
			return false;
		}
		return true;
	}

	/**
	 * @desc 查询会员
	 * @param int|array    $member_id   会员id
	 * @date 2017-7-21
	 * @return array
	 */
	public function getMemberRs($member_id)
	{	
		if(empty($member_id)){
		    return false;
		}
		$query = new Query();
		$query->select('member_id,store_id,nickname,cardno,gender,mobile')->from($this->_table);
		$result = array();
		if(is_array($member_id)){
		   $query->andWhere(['in','member_id',array_filter($member_id)]);
		   $result = $query->all();
		}else{
		   $query->andWhere(['member_id'=>$member_id]);
		   $result = $query->one();
		}
		
	    return $result;
	}

}