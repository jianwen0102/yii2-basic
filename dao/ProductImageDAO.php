<?php
namespace app\dao;

use app\dao\BaseDAO;

class ProductImageDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-17
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'product_image';
		$this->_pKey ='image_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%product_image}}';
	}
}