<?php
 namespace app\dao;
 /**
  * @desc 采购退货明细操作类
  * @author liaojianwen
  * @date 2017-04-27
  */
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOriginType;
 use app\enum\EnumOther;
 use yii\db\Query;
 
 class PurchaseReturnDetailDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-04-27
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'purchase_return_detail';
 		$this->_pKey ='purchase_return_det_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%purchase_return_detail}}';
 	}
 	
 	/**
 	 * @desc 根据id 获取采购退货单明细
 	 * @param $id 采购退货单id
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function getPurchaseReturnDet($id)
 	{
 		$selects = "purchase_return_det_id,purchase_return_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,base_price,
				d.quantity_unit,d.price,d.amount,d.purchase_det_id,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content";
 		$conditions ="d.purchase_return_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params)
 		->all();
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
 		}
 		return $result;
 	
 	}
 	
 	
 
 }