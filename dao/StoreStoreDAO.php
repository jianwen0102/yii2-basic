<?php
namespace app\dao;
use Yii;
use app\dao\BaseDAO;
use yii\db\Query;
use app\dao\ClientDAO;

class StoreStoreDAO extends BaseDAO
{
     /**
	 * @desc   构造函数
	 * @author hehauwei
	 * @date   2017-7-21
	 */
	public function __construct()
	{
	   parent::__construct();
	   $this->_table = 'store';
	   $this->_pKey  = 'store_id';
	}
	
	public static function tableName()
	{	
	   return '{{%store}}';	
	}
	
	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */	
	public function getStoreListData($pageInfo,$searchData,$filter){
	   $page     = isset($pageInfo['page']) ? $pageInfo['page'] : 1;
	   $pageSize = isset($pageInfo['pageSize']) ? $pageInfo['pageSize'] : 15;
	   $limit    = $pageSize;
	   $offset   = isset($page) ? ($page - 1) * $limit : 0;
	   $query    = new Query();

	   $query->select('store_id,store_name,create_time,delete_flag')->from($this->_table)->orderBy('store_id DESC');

	   if(isset($searchData['store_name']) && strlen(trim($searchData['store_name']))>0){//供应商查询
	      $store_name = trim($searchData['store_name']);
          $query->andWhere(['like','store_name',$store_name]);
	   }

	   if(isset($filter['delete_flag']) && $filter['delete_flag'] != '-1'){
	        $query->andWhere('delete_flag=:delete_flag',[':delete_flag'=>$filter['delete_flag']]);	   
	   }

	   $result['count'] = $query->count();
	   $result['list']  = $query->offset($offset)->limit($limit)->all();	   
	   $result['pageinfo']  = array (
			 'page'     => $page,
			 'pageSize' => $pageSize
	   );
	   return $result;	
	}
	
	/**
	 * @desc   保存添加/编辑门店
	 * @param  $store_id    string 门店id
	 * @param  $store_name  string 门店名称
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStoreData($data)
	{
		$store['store_name']  = $data['store_name'];
		$store['delete_flag'] = $data['delete_flag'] < 0 ? 0 : $data['delete_flag'];
		if(empty($data['store_id']) || $data['store_id']<1){	   
		   $store['create_time']   = time();
           $result = $this->iinsert($store,true);
		    if($result){
			   $client = array(
				  'sname'       => $data['store_name'],
			      'store_id'    => $result,
				  'desc' => $data['store_name']
			   );
		       ClientDAO::getInstance()->saveClient($client);		   
		   }
		}else{
		   $store['modify_time']   = time();
		   $conditions =  " store_id=:store_id ";
		   $params     = [':store_id'=>$data['store_id']];
		   $result = $this->iupdate($store,$conditions,$params);
		}
        return $result;
	}

	/**
	 * @desc 检查是否有重名
	 * @param int    $store_id   门店id
	 * @param string $store_name 门店名称
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return boolean
	 */
	public function checkStore($store_id,$store_name)
	{
		$query = new Query();
		$conditions = " store_name = '{$store_name}' ";
		if ($store_id > 0 ){
			$conditions .= " and store_id <> {$store_id}";
		}
		$result = $query->select ( 'store_id' )->from ( "$this->_table" )->where ( $conditions )->all ();
		if (empty ( $result )) {
			return false;
		}
		return true;
	}

	/**
	 * @desc 查询门店
	 * @param array|int    $store_ids   门店id
	 * @author hehuawei    $delete_flag 状态
	 * @date 2017-7-21
	 * @return array
	 */
	public function getStoreRs($store_ids,$delete_flag='')
	{
		$query = new Query();
		$where = '1';
		if(!empty($store_ids)){
		    $where = ['in','store_id',array_filter($store_ids)];
		}
		$query->select('store_id,store_name')->from($this->_table)->where($where);
		if(strlen($delete_flag)>0){
		   $query->andWhere(['delete_flag'=>$delete_flag]);
		}

		$result = $query->all();
		//$query->createCommand()->sql;//打印sql
	    return $result;
	}

}