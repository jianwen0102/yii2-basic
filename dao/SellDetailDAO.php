<?php
 namespace app\dao;
 /**
  * @desc 销售明细表操作类
  */
 use app\dao\BaseDAO;
 use Yii;
 use yii\db\Query;
 use app\enum\EnumOther;
 
 class SellDetailDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'sell_detail';
 		$this->_pKey ='sell_det_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%sell_detail}}';
 	}
 		
 	/**
 	 * @desc 根据id 获取报价单明细
 	 * @param $id 报价单id
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function getSellDet($id)
 	{
 		$selects = "sell_det_id,sell_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,shipped_quantity,minus_quantity,base_price,
				d.quantity_unit,d.price,d.amount,d.discount,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content,
 				p.supply_price,p.guide_price";
 		$conditions ="d.sell_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params)
 		->all();
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
 		}
 		return $result;
 	
 	}
 	
 	
 	/**
 	 * @desc 确认采购订单明细已收到数量，确认是否完成
 	 * @param $proid 采购订单id
 	 * @param  [ 'purchase_no' => string '20161219090249' (length=14)
 	 *			 'goods_id' => string '8' (length=1)
 	 *			 'quantity' => string '25' (length=2)
 	 *			 'purchase_type' => string '1' (length=1)
 	 *			 'purchase_det_id' => string '10' (length=2)
 	 *			 'warehouse_id' => string '1' (length=1)
 	 *			 'procurement_det_id' => string '6' (length=1)
 	 *			 'minus_quantity' => string '25' (length=2)
 	 *			 'received_quantity' => string '25' (length=2)
 	 *		  ]
 	 * @author liaojianwen
 	 * @date 2016-12-19
 	 */
 	public function updateFinish($proid, $det)
 	{
 		$pro_det = SellDetailDAO::getInstance ()->findByAttributes ( "minus_quantity,shipped_quantity,base_quantity,finish_flag", "sell_det_id =:detid", [
 				':detid' => $det ['sell_det_id']
 		] );
 		$total_quantity = $pro_det ['shipped_quantity'] + $det ['base_quantity'];
 		if ($total_quantity > $pro_det ['base_quantity']) {
 			//现在的值加上原来单据上的超过了未收的数量
 			$result ['Ack'] = 'error';
 			$result ['msg'] = 'total quantity is lt shipped_quantity';
 			return $result;
 		} else if ($total_quantity == $pro_det ['base_quantity']) {
 			//现在数量加上单据上的数量 刚好等于未收数量//标记完成
 			$detail_flag = SellDetailDAO::getInstance ()->iupdate ( [
 					'finish_flag' => EnumOther::FINISHED
 			], "sell_det_id=:pid", [
 					':pid' => $det ['sell_det_id']
 			] );
 			if (! $detail_flag) {
 				$result ['Ack'] = 'error';
 				$result ['msg'] = 'update detail finish_flag fail';
 				return $result;
 			}
 		}
 		$condition = "sell_det_id =:detid";
 		$params = [
 				':detid' => $det ['sell_det_id']
 		];
 		$received_quantity = $this->updateAllCounters ( [
 				"shipped_quantity" => $det ['base_quantity'],
 				"minus_quantity"=> -$det['base_quantity'],//未收数量变更为原来单据数量- 已收数量
 		], $condition, $params );
 		if (! $received_quantity) {
 			$result ['Ack'] = 'error';
 			$result ['msg'] = 'update shipped quantity fail';
 			return $result;
 		}
 	
 		//查找是否还有未完成的明细，如果有就不用给head 更新finish_flag,如果没有就给head 的finish_flag 打上完成
 		$exit_unfinish = SellDetailDAO::getInstance ()->findByAttributes ( "sell_det_id", "sell_id=:pid and finish_flag=:flag and delete_flag =:dflag", [
 				':pid' => $proid,
 				':flag' => EnumOther::NO_FINISHED,
 				':dflag' => EnumOther::NO_DELETE,
 		] );
 		if (!$exit_unfinish) {
 			$head_flag = SellDAO::getInstance ()->iupdate ( [
 					'finish_flag' => EnumOther::FINISHED
 			], "sell_id=:pid", [
 					':pid' => $proid
 			] );
 			if(empty($head_flag)) {
 				$result ['Ack'] = 'error';
 				$result ['msg'] = 'update head finish_flag fail';
 				return $result;
 			}
 		}
 		return [
 				'Ack' => 'success'
 		];
 	}
 	
 }