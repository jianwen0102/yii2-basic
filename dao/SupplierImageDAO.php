<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;
use Yii;
use app\enum\EnumOriginType;

class SupplierImageDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-06-30
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'supplier_image';
		$this->_pKey ='image_id';
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%supplier_image}}';
	}
	
	
}