<?php
namespace  app\dao;
use yii\db\Query;
use yii\base\Object;
class OrderGoodsDAO extends BaseDAO
{
	public function __construct() 
	{
		parent::__construct();
		
		$this->_table = 'shop_order_goods';
		$this->_pKey ='rec_id';
	}
	
	public function getGoodsById($id){
		$query = new Query();
		return $query->select('goods_name,goods_number')->from($this->_table)
		->where('order_id=:id',[':id'=>$id])->all();
	}
}