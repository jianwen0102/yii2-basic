<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\helpers\Utility;
use app\enum\EnumOriginType;
use app\enum\EnumOther;

/**
 * @desc instore 表操作类
 * @author liaojianwen
 * @date 2016年11月22日下午4:48:56
 */
class InstoreDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-22
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'instore';
		$this->_pKey ='instore_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%instore}}';
	}
	
	/**
	 * @desc 获取入库单列表数据
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getInstores($condition, $filter, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "i.instore_id,instore_no,instore_date,vendor_id,i.warehouse_id,warehouse_name,instore_type,i.remark,
				supplier_name,instore_man,employee_name,i.confirm_flag";
		$conditions = "i.delete_flag =:flag";
		$params = array (
			':flag' => EnumOther::NO_DELETE
		);
		
		if(isset($condition['vendor']) && !empty($condition['vendor'])){
			$conditions .=" and i.vendor_id =:vid";
			$params[':vid'] =$condition['vendor'];
		}
		if(isset($condition['warehouse']) && !empty($condition['warehouse'])){
			$conditions .=" and i.warehouse_id =:wid";
			$params[':wid'] = $condition['warehouse'];
		}
		switch ($filter){
			case 0 :
				break;
			case 1:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::NO_CONFIRM;
				break;
			case 2:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::CONFIRM;
				break;
		}
		$query = new Query ();
		$query->select ( $selections )
			  ->from ("$this->_table i")
			  ->leftJoin("warehouse w","i.warehouse_id = w.warehouse_id")
			  ->leftJoin("supplier s","i.vendor_id = s.supplier_id")
			  ->leftJoin("employee e","e.employee_id = i.instore_man")
			  ->where ( $conditions, $params )
			  ->orderBy ( ['i.confirm_flag'=>SORT_ASC,'i.create_time'=>SORT_DESC]);
		
		if(isset($condition['starTime']) && !empty($condition['starTime'])){
			$query->andwhere(['between','instore_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		foreach ($result['list'] as &$list){
			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$instore_type, $list['instore_type']);
		}
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	/**
	 * @desc 根据id 获取入库单单头信息
	 * @param $id //入库单id
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getInstoreHead($id)
	{
		$selections = "i.instore_id,instore_no,instore_date,vendor_id,i.warehouse_id,instore_type,i.remark,
				vendor_id,instore_man,confirm_flag,create_man,username,instore_amount";
		$conditions = "i.instore_id =:id";
		$params = array (
				':id' => $id,
		);
		$query = new Query ();
		$result = $query->select ( $selections )
				->from ("$this->_table i")
				->leftJoin("admin a","a.id= i.create_man")
				->where ( $conditions, $params )
				->one();
		return $result;
	}
	
}