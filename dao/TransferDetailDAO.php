<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use yii\db\Query;
 use app\enum\EnumOther;
use app\enum\EnumOriginType;
	 
 class TransferDetailDAO extends BaseDAO 
 {
 	/**
 	* @desc 构造函数
 	* @author liaojianwen
 	* @date 2017-03-31
 	*/
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'transfer_detail';
 		$this->_pKey ='transfer_det_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%transfer_detail}}';
 	}
 	
 	/**
 	 * @desc 编辑页面调拨单明细数据
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function getTransferDetail($id)
 	{
 		$selects = "transfer_det_id,d.transfer_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,s.warehouse_out,base_price,
				d.quantity_unit,d.price,d.amount,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content";
 		$conditions ="d.transfer_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->leftJoin("transfer s","s.transfer_id = d.transfer_id")
 		->where($conditions,$params)
 		->all();
 		// 		dd(getSql($query));
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
 			$res_quantity = StockPileDAO::getInstance()->findByAttributes('quantity','product_id = :pid and warehouse_id =:wid',[':pid'=>$res['product_id'],':wid'=>$res['warehouse_out']]);
 			$res['product_quantity'] = isset($res_quantity['quantity']) ? $res_quantity['quantity']:0;
 		}
 		return $result;
 	
 	}
 	
 	/**
 	 * @desc 获取特定商品销售情况
 	 * @author liaojianwen
 	 * @date 2018-04-13
 	 */
 	public function getSpecialLost($condition, $goods_id)
 	{
 			
 		$selections = ['d.product_id','sum(base_quantity) quantity','sum(base_price) price','ROUND(sum(base_quantity)*sum(base_price),2) amount','g.product_name','u.unit_name'];
		$conditions ="d.delete_flag = :flag and p.transfer_type = :type";
		$params = [
				':flag'=>EnumOther::NO_DELETE,
				':type'=> EnumOriginType::origin_scrapped
		];
		$query = new Query();
		
		 $query->select($selections)
			->from("$this->_table d")
			->innerJoin("transfer p","p.transfer_id = d.transfer_id")
			->innerJoin("product_extend e","e.product_id = d.product_id and goods_type != ".EnumOther::BASE_GOODS ." and pid = ".$goods_id)
			->innerJoin("product g","g.product_id = d.product_id")
			->innerJoin("unit u","g.quantity_unit = u.unit_id")
			->where($conditions, $params);
		
		if(isset($condition['starTime']) && !empty($condition['starTime'])){
			$query->andwhere(['between','p.transfer_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
		}
		
		$result= $query->groupBy("d.product_id")->all();
		return $result;
 	}
 }