<?php
 namespace app\dao;
 
 /**
  * @desc 拆装单单头
  */
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
 use yii\db\Query;
 
 class DisassemblyDAO extends BaseDAO
 {
	 
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-10-17
 	 * @return DisassemblyDAO
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'disassembly';
 		$this->_pKey ='disassembly_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%disassembly}}';
 	}
 	
 	
 	/**
 	 * @desc 获取拆装列表信息
 	 * @param $condition [] 查询条件
 	 * @param $filter string 过滤条件
 	 * @param $pageInfo [] 页面信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function getDisassembly($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 	
 		$selections = "t.disassembly_id,disassembly_no,t.disassembly_date,t.warehouse_out,w.warehouse_name warehouse_out_name, u.warehouse_name warehouse_in_name,t.remark,t.disassembly_man,employee_name,t.confirm_flag";
 		$conditions = "t.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE,
 		);
 	
 		if(isset($condition['warehouse_out']) && !empty($condition['warehouse_out'])){
 			$conditions .=" and t.warehouse_out =:owid";
 			$params[':owid'] = $condition['warehouse_out'];
 		}
 		switch ($filter){
 			case 0 :
 				break;
 			case 1:
 				$conditions .=" and t.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::NO_CONFIRM;
 				break;
 			case 2:
 				$conditions .=" and t.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::CONFIRM;
 				break;
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table t")
 		->leftJoin("warehouse w","t.warehouse_out = w.warehouse_id")
 		->leftJoin("warehouse u","t.warehouse_in = u.warehouse_id")
 		->leftJoin("employee e","e.employee_id = t.disassembly_man")
 		->where ( $conditions, $params )
 		->orderBy ( ['t.confirm_flag'=>SORT_ASC,'t.create_time'=>SORT_DESC]);
 	
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','disassembly_date',strtotime($condition['starTime']), nextDay($condition['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		//  		foreach ($result['list'] as &$list){
 		// //  			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$sale_type, $list['disassembly_type']);
 		//  		}
 	
 		$result ['page'] = array (
 		'page' => $pageInfo ['page'],
 		'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 编辑页面拆装单头数据
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function getDisassemblyHead($id)
 	{
 		$selections = "i.disassembly_id,disassembly_no,disassembly_date,i.warehouse_out,i.warehouse_in,i.remark,
				i.disassembly_man,confirm_flag,create_man,username,disassembly_amount";
 		$conditions = "i.disassembly_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("admin a","a.id= i.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }