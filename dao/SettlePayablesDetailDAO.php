<?php
 namespace app\dao;
 /**
  * @desc 应付款结算明细操作类
  */
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
 use app\helpers\Utility;
 use app\enum\EnumOriginType;
 
 class SettlePayablesDetailDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-03-03
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'settle_payables_detail';
 		$this->_pKey ='settle_pay_det_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%settle_payables_detail}}';
 	}
 	
 	/**
 	 * @desc 根据id 获取入库单明细
 	 * @param $id 入库单id
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function getSettlePayablesDet($id)
 	{
 		$selects = "settle_pay_det_id,settle_pay_id,d.pay_id,d.pay_amount,d.settled_amount,d.left_amount,d.current_amount,d.remark,
 				p.pay_type,p.pay_man,p.pay_time,p.origin_no,e.employee_name pay_man_name";
 		$conditions ="d.settle_pay_id = :id";
 		$params = [
 				':id'=>$id,
 		];
 		$query = new Query();
 		$query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("payables p","p.pay_id = d.pay_id")
 		->leftJoin("employee e","e.employee_id = p.pay_man")
 		->where(['in','d.delete_flag',[EnumOther::NO_DELETE, EnumOther::INVALID]]);
 		
 		$result['list'] = $query->andWhere($conditions, $params)->all();
 		$total_pay_amount = 0;
 		$total_receive_amount = 0;
 		$total_left_amount = 0;
 		$total_cur_amount = 0;
 		foreach ($result['list'] as &$list){
 			$list['pay_type_name'] = Utility::getArrayValue(EnumOriginType::$pay_type, $list['pay_type']);
//  			$list['origin_type_name'] = Utility::getArrayValue(EnumOriginType::$purchase_type, $list['origin_type']);
//  			$list['settle_flag'] = Utility::getArrayValue(EnumOther::$settle, $list['finish_flag']);
			$total_pay_amount +=$list['pay_amount'];
			$total_cur_amount += $list['current_amount'];
			$total_receive_amount += $list['settled_amount'];
			$total_left_amount += $list['left_amount'];
			
 		}
 		
 		$result['total_pay_amount'] = $total_pay_amount;
 		$result['total_received_amount'] = $total_receive_amount;
 		$result['total_left_amount'] = $total_left_amount;
 		$result['total_cur_amount'] = $total_cur_amount;
 		
 		return $result;
 	
 	}
 }