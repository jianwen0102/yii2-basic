<?php
 namespace app\dao;
 /**
  * @desc 应付款单操作类
  */
 use yii\db\Query;
 use app\dao\BaseDAO;
 use Yii;
 use app\enum\EnumOther;
 use app\helpers\Utility;
 use app\enum\EnumOriginType;
 use yii\base\Object;
 use app\dao\SettlePayablesDetailDAO;
 
 class PayablesDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-03-01
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'payables';
 		$this->_pKey ='pay_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%payables}}';
 	}
 	
 	/**
 	 * @desc 获取应付账单
 	 * @author liaojianwen
 	 * @date 2017-03-01
 	 */
 	public function getPayables($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "p.pay_id,pay_time,pay_man,pay_type,vendor_id,origin_no,origin_id,origin_type,finish_flag,pay_amount,
 				settle_amount,receive_amount,left_amount,p.remark,description,supplier_name,e.employee_name";
 		$conditions = "p.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE
 		);
 		
 		if(isset($condition['vendor']) && !empty($condition['vendor'])){
 			$conditions .=" and p.vendor_id =:vid";
 			$params[':vid'] =$condition['vendor'];
 		}
 		switch ($filter){
 			case 0 :
 				break;
 			case 1:
 				$conditions .=" and p.finish_flag = :cflag";
 				$params[':cflag'] = EnumOther::NO_SETTELE;
 				break;
 			case 2:
 				$conditions .=" and p.finish_flag = :cflag";
 				$params[':cflag'] = EnumOther::SETTLE;
 				break;
 			case 3:
 				$conditions .= " and p.finish_flag= :cflag";
 				$params[':cflag'] = EnumOther::PART_SETTLE;
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("supplier s","p.vendor_id = s.supplier_id")
 		->leftJoin("employee e","e.employee_id = p.pay_man")
 		->where ( $conditions, $params )
 		->orderBy ( ['finish_flag'=> SORT_ASC,'p.create_time'=>SORT_DESC]);
 		
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','pay_time',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$list['pay_type_name'] = Utility::getArrayValue(EnumOriginType::$pay_type, $list['pay_type']);
 			$list['origin_type_name'] = Utility::getArrayValue(EnumOriginType::$payable_orgin, $list['origin_type']);
 			$list['settle_flag'] = Utility::getArrayValue(EnumOther::$settle, $list['finish_flag']);
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	
 	/**
 	 * @desc 根据往来单位获取应付账单
 	 * @author liaojianwen
 	 * @date 2017-03-02
 	 */
 	public function getPayablesByVendor($vendor, $pageInfo, $condition)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "p.pay_id,pay_time,pay_man,pay_type,vendor_id,origin_no,origin_id,origin_type,finish_flag,pay_amount,
 				settle_amount,receive_amount,left_amount,p.remark,description,supplier_name,e.employee_name";
 		$conditions = "p.delete_flag =:flag and p.vendor_id = :vendor and finish_flag != :fflag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE,
 				':vendor' => $vendor,
 				':fflag'=> EnumOther::SETTLE,
 		);
 		
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("supplier s","p.vendor_id = s.supplier_id")
 		->leftJoin("employee e","e.employee_id = p.pay_man")
 		->where ( $conditions, $params )
 		->orderBy ( ['p.create_time'=>SORT_DESC]);
 		
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','pay_time',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		if(isset($condition['name']) && !empty($condition['name'])){
 			$query->andWhere(['like', 'origin_no',$condition['name']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$list['pay_type_name'] = Utility::getArrayValue(EnumOriginType::$pay_type, $list['pay_type']);
 			$list['origin_type_name'] = Utility::getArrayValue(EnumOriginType::$purchase_type, $list['origin_type']);
 			$list['settle_flag'] = Utility::getArrayValue(EnumOther::$settle, $list['finish_flag']);
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @ 统计采购单付款情况
 	 * @author liaojianwen
 	 * @date 2017-03-10
 	 */
 	public function getPurchaseCount($cond,$pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "p.pay_time,d.goods_id,p.vendor_id,p.pay_id,p.left_amount,d.base_quantity,d.base_unit,h.purchase_no,
 				s.supplier_name,d.amount,d.price,u.unit_name,g.product_name";
 		
 		$conditions = "p.delete_flag = :flag";
 		$params =[
 			':flag'=>EnumOther::NO_DELETE,
 		];
 		
 		if(isset($cond['vendor']) && !empty($cond['vendor'])){
 			$conditions .=" and p.vendor_id =:vid";
 			$params[':vid'] =$cond['vendor'];
 		}
 		
 		if(isset($cond['id']) && $cond['id']){
 			$conditions .=" and g.product_id =:id";
 			$params[':id'] = $cond['id'];
 		}
 		
 		$query = new Query();
 		$query->select($selections)
	 		->from("$this->_table p")
	 		->leftJoin("purchase h","p.origin_id = h.purchase_id")
	 		->leftJoin("purchase_detail d","h.purchase_id = d.purchase_id")
	 		->leftJoin("product g","d.goods_id = g.product_id")
	 		->leftJoin("supplier s","s.supplier_id = p.vendor_id")
	 		->leftJoin("unit u","u.unit_id = d.base_unit")
	 		->where($conditions,$params);
 		
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','pay_time',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		
 		if(isset($cond['name']) && !empty($cond['name'])){
 			$query->andWhere(['like','g.product_name', $cond['name']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$det){
 			$con ="d.pay_id = :pid";
 			$pad =[
 				':pid'=>$det['pay_id'],
 			];
 			$joinArray = [
 				[
 					'settle_payables h',
					'd.settle_pay_id = h.settle_pay_id', 
 					'left'=>''
 				]
 			];
 			$res = SettlePayablesDetailDAO::getInstance()->iselect("settle_pay_date", $con, $pad,'one','d.settle_pay_id ASC',$joinArray,'d');
 			$det['settle_pay_date'] = $res['settle_pay_date'];
 		}
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 		
 	}
 	
 	
 	public function getPurchaseVendor($cond)
 	{
//  		$limit = $pageInfo ['pageSize'];
//  		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "p.pay_time,d.goods_id,p.vendor_id,p.pay_id,p.left_amount last_amount,d.base_quantity,d.base_unit,h.purchase_no,
 				s.supplier_name,d.amount,d.price,u.unit_name,g.product_name";
 			
 		$conditions = "p.delete_flag = :flag";
 		$params =[
 				':flag'=>EnumOther::NO_DELETE,
 		];
 			
 		if(isset($cond['vendor']) && !empty($cond['vendor'])){
 			$conditions .=" and p.vendor_id =:vid";
 			$params[':vid'] =$cond['vendor'];
 		}
 		if(isset($cond['id']) && !empty($cond['id'])){
 			$conditions .=" and g.product_id =:pid";
 			$params[':pid'] =$cond['id'];
 		}
 		$query = new Query();
 		$query->select($selections)
 		->from("$this->_table p")
 		->leftJoin("purchase h","p.origin_id = h.purchase_id")
 		->leftJoin("purchase_detail d","h.purchase_id = d.purchase_id")
 		->leftJoin("product g","d.goods_id = g.product_id")
 		->leftJoin("supplier s","s.supplier_id = p.vendor_id")
 		->leftJoin("unit u","u.unit_id = d.base_unit")
 		->where($conditions,$params);
 			
 		if(isset($cond['starTime']) && !empty($cond['starTime'])){
 			$query->andwhere(['between','pay_time',strtotime($cond['starTime']), strtotime($cond['endTime'])]);
 		}
 		
 		if(isset($cond['name']) && !empty($cond['name'])){
 			$query->andWhere(['like','g.product_name', $cond['name']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->all ();
 		$total_quantity = 0;
 		$total_amount = 0;
 		$total_receive_quantity = 0;
 		$total_receive_amount = 0;
 		$total_left_amount = 0;
 		foreach ($result['list'] as &$det){
 			$con ="d.pay_id = :pid";
 			$pad =[
 					':pid'=>$det['pay_id'],
 			];
 			$joinArray = [
 					[
 							'settle_payables h',
 							'd.settle_pay_id = h.settle_pay_id',
 							'left'=>''
 					]
 			];
 			$res = SettlePayablesDetailDAO::getInstance()->iselect("settle_pay_date", $con, $pad,'one','d.settle_pay_id ASC',$joinArray,'d');
 			$det['settle_pay_date'] = $res['settle_pay_date'];
 			if($det['last_amount'] > 0){
 				$det['receive_quantity'] = 0;
 				$det['receive_amount'] = 0;
 				$det['left_amount'] = $det['amount'];
 			} else {
 				$det['receive_quantity'] = $det['base_quantity'];
 				$det['receive_amount'] = $det['amount'];
 				$det['left_amount'] = 0;
 			}
 			
 			$total_quantity += $det['base_quantity'];
 			$total_amount += $det['amount'];
 			$total_receive_quantity += $det['receive_quantity'];
 			$total_receive_amount += $det['receive_amount'];
 			$total_left_amount += $det['left_amount'];
 			
 			
 		}
 		$result['total_quantity'] = $total_quantity;
 		$result['total_amount'] = $total_amount;
 		$result['total_receive_quantity'] = $total_receive_quantity;
 		$result['total_receive_amount'] = $total_receive_amount;
 		$result['total_left_amount'] = $total_left_amount;
 		
 			
//  		$result ['page'] = array (
//  				'page' => $pageInfo ['page'],
//  				'pageSize' => $pageInfo ['pageSize']
//  		);
 		return $result;
 	}
 }