<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;
use Yii;
/**
 * @desc instore_detail 表操作类
 * @author liaojianwen
 * @date 2016年11月22日下午4:48:11
 */
class InstoreDetailDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-22
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'instore_detail';
		$this->_pKey ='instore_det_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%instore_detail}}';
	}
	
	/**
	 * @desc 根据id 获取入库单明细
	 * @param $id 入库单id
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getInstoreDet($id)
	{
		$selects = "instore_det_id,instore_id,goods_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,base_price,
				unit,d.price,d.amount,d.batch_num,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content";
		$conditions ="d.instore_id = :id and d.delete_flag = :flag";
		$params = [
			':id'=>$id,
			':flag'=>EnumOther::NO_DELETE,
		];
		$query = new Query();
		$result = $query->select($selects)
			->from("$this->_table d")
			->leftJoin("product p",'d.goods_id = p.product_id')
			->leftJoin("unit u","u.unit_id = d.base_unit")
			->where($conditions,$params)
			->all();
		foreach ($result as &$res){
			$res_units = ProductUnitDAO::getInstance()->getUnits($res['goods_id'] );
			$res['product_unit'] = $res_units;
		}
		return $result;
		
	}
	
	/**
	 * @desc 其他入库汇总统计
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function getInstoreSummary($cond, $pageInfo)
	{
	
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
	
		$sql = "select SUM(d.quantity) quantity,goods_id,unit,h.warehouse_id, unit_name,product_name, w.warehouse_name from instore_detail d
				INNER JOIN instore h on h.instore_id = d.instore_id";
		
		if(isset($cond['starTime']) && !empty($cond['starTime'])){
			$sql .= " and h.instore_date between ".$cond['starTime'] .' and '. $cond['endTime'];
		}	
		$sql .=	" LEFT JOIN product p on p.product_id = d.goods_id
				LEFT JOIN warehouse w on w.warehouse_id = h.warehouse_id
				LEFT JOIN unit u on u.unit_id = d.unit where d.delete_flag = ".EnumOther::NO_DELETE;
		if(isset($cond['id']) && !empty($cond['id'])){
			$sql .= " and p.product_id = ".$cond['id'];
		}
		
		if(isset($cond['name']) && !empty($cond['name'])){
			$sql .= " and p.product_name like '%".$cond['name'] ."%'";
		}		
			
		if(isset($cond['warehouse_in']) && !empty($cond['warehouse_in'])){
			$sql .= " and h.warehouse_id = ".$cond['warehouse_in'];
		}
		$sql .= " GROUP BY goods_id,warehouse_id,unit";
	
	
	
		$command = yii::$app->db;
		$count = $command->createCommand()->setSql($sql)->execute();
	
		$sql .="  limit ".$limit .' offset '. $offset ;
	
		$return = $command->createCommand($sql)->queryAll();
	
		$result ['count'] = $count;
		$result ['list'] = $return;
	
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
	
		return $result;
	}
}