<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;

class AdminDAO extends BaseDAO{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'admin';
		$this->_pKey ='id';
	}
	
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%admin}}';
	}
	
	/**
	 * @desc 获取管理员列表
	 * @param $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	public function getUserList($pageInfo) {
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "id,username,email,created_at,last_in";
		$conditions = "delete_flag =:flag";
		$params = array (
				':flag' => 0
		);
	
		$query = new Query();
		$query->select ( $selections )->from ( $this->_table )->where ( $conditions, $params )->orderBy ( 'created_at ASC' );
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
}