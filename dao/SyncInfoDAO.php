<?php
 namespace app\dao;
 
 use yii\db\Query;
 
 class SyncInfoDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2018-02-26
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'sync_info';
 		$this->_pKey ='id';
 	}
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%sync_info}}';
 	}
 }