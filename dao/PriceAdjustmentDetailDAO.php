<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use yii\db\Query;
 use Yii;
 use app\enum\EnumOther;
use yii\base\Object;
	 
 class PriceAdjustMentDetailDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-07-11
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table ="price_adjustment_detail";
 		$this->_pKey  = "adjustment_det_id";
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%price_adjustment_detail}}';
 	}
 	
 	/**
 	 * @desc 根据id 获取成本调整单明细信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustmentDetail($id)
 	{
 		$selects = "adjustment_det_id, adjustment_id, d.product_id, p.product_name,d.quantity_unit,
 				u.unit_name, d.price, d.adjustment_price, d.quantity, d.remark";
 		$conditions ="d.adjustment_id = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->leftJoin("unit u","u.unit_id = d.quantity_unit")
 		->where($conditions,$params)
 		->all();
 		foreach ($result as &$res){
 			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
 			$res['product_unit'] = $res_units;
 		}
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取成本调整单查询列表
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function getAdjustmentSearch($condition, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections ="h.adjustment_id, h.adjustment_no, h.adjustment_date,e.employee_name, h.remark head_remark,
 				d.adjustment_det_id, d.product_id, d.quantity,d.quantity_unit,d.price,d.adjustment_price,d.remark det_remark,
 				p.supplier_id, u.unit_name, s.supplier_name,p.product_name,h.confirm_flag";
 		$query= new Query();
 		
 		$conditions = "d.delete_flag = :flag";
 		$params = [
 				':flag' => EnumOther::NO_DELETE
 		];
 		
 		if(isset($condition['vendor']) && !empty($condition['vendor'])){
 			$conditions .=" and p.supplier_id =:vid";
 			$params[':vid'] =$condition['vendor'];
 		}
 		
 		$query->select($selections)
 			-> from("$this->_table d")
 			->leftJoin("price_adjustment h","d.adjustment_id = h.adjustment_id")
 			->leftJoin("product p","p.product_id = d.product_id")
 			->leftJoin("supplier s","s.supplier_id = p.supplier_id")
 			->leftJoin("unit u","u.unit_id = d.quantity_unit")
 			->leftJoin("employee e","h.handle_man = e.employee_id")
 			->where($conditions, $params)
 			->orderBy(["h.adjustment_date" => SORT_DESC]);
 		
 		if (isset ( $condition ['starTime'] ) && ! empty ( $condition ['starTime'] )) {
 			$query->andwhere ( ['between','h.adjustment_date',strtotime ( $condition ['starTime'] ),strtotime ( $condition ['endTime'] )] );
 		}
 		
 		if(isset($condition['name']) && !empty($condition['name'])){
 			$query->andWhere(['like', 'p.product_name', $condition['name']]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 }