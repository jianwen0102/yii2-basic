<?php
namespace app\dao;

use app\dao\BaseDAO;
use Yii;
use app\enum\EnumOther;
use yii\db\Query;

class SaleOutSyncDetailDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-02-07
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'saleout_sync_detail';
		$this->_pKey ='saleout_det_id';
	}

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%saleout_sync_detail}}';
	}
	
	/**
	 * @desc 汇总商品销售价
	 * @author liaojianwen
	 * @date 2018-02-27
	 */
	public function getSalePriceSummary($starTime, $endTime, $product_id)
	{
		$selections =['g.product_id','p.product_name','sum(g.quantity) sum_quantity','sum(g.price) sum_price','sum(g.amount) sum_amount'];
			
		$conditions = "g.product_id =:pid";
			
		$params = array (
				':pid' => $product_id,
		);
			
		$query = new Query();
			
		$query->select($selections)
		->from("$this->_table g")
		->innerJoin("product p", "g.product_id = p.product_id")
		->where($conditions, $params)
		->groupBy("g.product_id");
			
		if(!empty($starTime)){
			$query->andwhere(['between','g.create_time',strtotime($starTime), strtotime($endTime)]);
		}
			
			
		$result = $query->one();
		return $result;
	}
}
