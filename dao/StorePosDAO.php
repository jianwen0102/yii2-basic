<?php
namespace app\dao;
use Yii;
use app\dao\BaseDAO;
use yii\db\Query;

class StorePosDAO extends BaseDAO
{
     /**
	 * @desc   构造函数
	 * @author hehauwei
	 * @date   2017-7-21
	 */
	public function __construct()
	{
	   parent::__construct();
	   $this->_table = 'store_pos';
	   $this->_pKey  = 'pos_id';
	}
	
	public static function tableName()
	{	
	   return '{{%store_pos}}';	
	}
	
	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */	
	public function getStorePosListData($pageInfo,$searchData,$filter){
	   $page     = isset($pageInfo['page']) ? $pageInfo['page'] : 1;
	   $pageSize = isset($pageInfo['pageSize']) ? $pageInfo['pageSize'] : 15;
	   $limit    = $pageSize;
	   $offset   = isset($page) ? ($page - 1) * $limit : 0;
	   $query    = new Query();

	   $query->select('pos_id,pos_name,store_id,create_time,delete_flag')->from($this->_table)->orderBy('pos_id DESC');

	   if(isset($searchData['pos_name']) && strlen(trim($searchData['pos_name']))>0){//供应商查询
	      $pos_name = trim($searchData['pos_name']);
          $query->andWhere(['like','pos_name',$pos_name]);
	   }

	   if(isset($filter['delete_flag']) && $filter['delete_flag'] != '-1'){
	        $query->andWhere('delete_flag=:delete_flag',[':delete_flag'=>$filter['delete_flag']]);	   
	   }

	   $result['count'] = $query->count();
	   $result['list']  = $query->offset($offset)->limit($limit)->all();	   
	   $result['pageinfo']  = array (
			 'page'     => $page,
			 'pageSize' => $pageSize
	   );
	   return $result;	
	}
	
	/**
	 * @desc   保存添加/编辑
	 * @param  $data    array pos机数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStorePosData($data)
	{
		$store['pos_name']  = $data['pos_name'];
		$store['store_id']  = $data['store_id'];
		$store['delete_flag'] = $data['delete_flag'] < 0 ? 0 : $data['delete_flag'];
		if(empty($data['pos_id']) || $data['pos_id']<1){	   
		   $store['create_time']   = time();
           $result = $this->iinsert($store);		
		}else{
		   $store['modify_time']   = time();
		   $conditions =  " pos_id=:pos_id ";
		   $params     = [':pos_id'=>$data['pos_id']];
		   $result = $this->iupdate($store,$conditions,$params);
		}
        return $result;
	}

	/**
	 * @desc 检查是否有重名
	 * @param int    $pos_id     pos机id
	 * @param string $pos_name   pos机名称
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return boolean
	 */
	public function checkStorePos($pos_id,$pos_name)
	{
		
		$query = new Query();
		$conditions = " pos_name = '{$pos_name}' ";
		if ($pos_id > 0 ){
			$conditions .= " and pos_id <> {$pos_id}";
		}
		$result = $query->select ( 'pos_id' )->from ( "$this->_table" )->where ( $conditions )->all ();
		if (empty ( $result )) {
			return false;
		}
		return true;
	}

    /**
	 * @desc   获取一条数据
	 * @param  $employee_id  店员id
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStorePosOne($pos_id)
	{
	   $query = new Query();
       $employeeOne = $query -> select('pos_id,pos_name,store_id,delete_flag')
		              ->from( "$this->_table" ) 
		              -> where(['pos_id'=>$pos_id]) 
		              -> one();
	   return $employeeOne;
	}

}