<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use yii\base\Object;

/**
 * @desc 地区操作类
 * @author liaojianwen
 * @date 2016年11月17日
 */
class RegionDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-11
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'region';
		$this->_pKey ='region_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%region}}';
	}
	
	/**
	 * @desc 获取地区名称
	 * @param  $id
	 * @author liaojianwen
	 * @date 2016-11-11
	 */
	public function getRegionName($id)
	{
		$query = new Query();
		$condition ="region_id=:id";
		$params = [
			':id'=>$id,
		];
		$result = $query->select ( 'region_name' )->from ( $this->_table )->where ( $condition, $params )->scalar();
		return $result;
	}
	
	/**
	 * @desc 获取地区名称
	 * @param  $type //0 ：国家; 1：省份;2:城市;3：地区;
	 * @param unknown $item
	 */
	public function getRegion($parent = 0)
	{
		$query = new Query();
		$condition = "parent_id = :pid";
		$params = [
				':pid'=> $parent,
		];
		$result = $query->select ( 'region_id,region_name' )->from ( $this->_table )->where ( $condition, $params )->all ();
		return $result;
	}
	
}