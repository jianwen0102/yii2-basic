<?php
namespace app\dao;
/**
 * @desc 盘点单明细
 */
use app\dao\BaseDAO;
use Yii;
use app\enum\EnumOther;
use yii\db\Query;
use app\dao\ProductUnitDAO;

class InventoryDetailDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'inventory_detail';
		$this->_pKey ='inventory_det_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%inventory_detail}}';
	}
	
	/**
	 * @desc 根据id 获取盘点单明细
	 * @param $id 盘点单id
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function getInventoryDet($id)
	{
		$selects = "inventory_det_id,inventory_id,d.product_id,d.quantity,d.remark,p.product_name,product_sn,p.quantity product_quantity,d.lost_quantity,d.inventory_quantity,
				d.quantity_unit,d.price,d.amount,base_quantity,base_unit,u.unit_name base_name,second_quantity,second_unit,second_relation,p.unit_content,base_price";
		$conditions ="d.inventory_id = :id and d.delete_flag = :flag";
		$params = [
				':id'=>$id,
				':flag'=>EnumOther::NO_DELETE,
		];
		$query = new Query();
		$result = $query->select($selects)
		->from("$this->_table d")
		->leftJoin("product p",'d.product_id = p.product_id')
		->leftJoin("unit u","u.unit_id = d.base_unit")
		->where($conditions,$params)
		->all();
		foreach ($result as &$res){
			$res_units = ProductUnitDAO::getInstance()->getUnits($res['product_id'] );
			$res['product_unit'] = $res_units;
		}
		return $result;
	
	}
}
