<?php
namespace app\dao;

use app\dao\BaseDAO;
use yii\db\Query;
use app\enum\EnumOther;

class EmployeeDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'employee';
		$this->_pKey ='employee_id';
		$this->_name = "employee_name";
	}
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%employee}}';
	}
	/**
	 * @desc 获取单位列表
	 * @param  $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-7
	 */
	public function getEmployee($pageInfo) {
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "employee_id,employee_name,phone,remark,e.depart_id,depart_name";
		$conditions = "e.delete_flag =:flag";
		$params = array (
				':flag' => 0
		);
	
		$query = new Query ();
		$query->select ( $selections )
			  ->from ( "$this->_table e")
			  ->leftJoin("department d","e.depart_id = d.depart_id")
			  ->where ( $conditions, $params )
			  ->orderBy ( 'e.create_time ASC' );
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
}