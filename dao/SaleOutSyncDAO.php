<?php
namespace app\dao;

use app\dao\BaseDAO;
use Yii;
use app\enum\EnumOther;
use yii\db\Query;
use app\helpers\Utility;
use app\enum\EnumOriginType;

class SaleOutSyncDAO extends BaseDAO 
{
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'saleout_sync';
 		$this->_pKey ='saleout_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%saleout_sync}}';
 	}
 	
}
