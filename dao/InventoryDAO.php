<?php
namespace app\dao;

/**
 * @desc 盘点单头
 */
use app\dao\BaseDAO;
use Yii;
use app\enum\EnumOther;
use yii\db\Query;
use app\helpers\Utility;

class InventoryDAO extends BaseDAO
{
	/**
	 * @desc 构造函数
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function __construct()
	{
		parent::__construct();
		$this->_table = 'inventory';
		$this->_pKey ='inventory_id';
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%inventory}}';
	}
	
	/**
	 * @desc 获取盘点信息
	 * @param $condition [] 查询信息
	 * @param $filter 查询条件
	 * @param $pageInfo [] 页面信息
	 * @author liaojianwen
	 */
	public function getInventorys($condition, $filter, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selections = "i.inventory_id,inventory_no,i.inventory_date,i.warehouse_id,warehouse_name,i.remark,handle_man,employee_name,i.confirm_flag";
		$conditions = "i.delete_flag =:flag";
		$params = array (
				':flag' => EnumOther::NO_DELETE
		);
		
		if(isset($condition['warehouse']) && !empty($condition['warehouse'])){
			$conditions .=" and i.warehouse_id =:wid";
			$params[':wid'] = $condition['warehouse'];
		}
		switch ($filter){
			case 0 :
				break;
			case 1:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::NO_CONFIRM;
				break;
			case 2:
				$conditions .=" and i.confirm_flag = :cflag";
				$params[':cflag'] = EnumOther::CONFIRM;
				break;
		}
		$query = new Query ();
		$query->select ( $selections )
		->from ("$this->_table i")
		->leftJoin("warehouse w","i.warehouse_id = w.warehouse_id")
		->leftJoin("employee e","e.employee_id = i.handle_man")
		->where ( $conditions, $params )
		->orderBy ( ['i.confirm_flag'=>SORT_ASC,'i.create_time'=>SORT_DESC]);
		
		if(isset($condition['starTime']) && !empty($condition['starTime'])){
			$query->andwhere(['between','inventory_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	/**
	 * @desc 根据id 获取盘点单单头信息
	 * @param $id //盘点单id
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function getInventoryHead($id)
	{
		$selections = "i.inventory_id,inventory_no,inventory_date,i.warehouse_id,i.remark,
				handle_man,confirm_flag,create_man,username,total_amount";
		$conditions = "i.inventory_id =:id";
		$params = array (
				':id' => $id,
		);
		$query = new Query ();
		$result = $query->select ( $selections )
		->from ("$this->_table i")
		->leftJoin("admin a","a.id= i.create_man")
		->where ( $conditions, $params )
		->one();
		return $result;
	}
}
