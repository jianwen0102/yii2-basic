<?php
 namespace app\dao;
 
 use Yii;
 use app\dao\BaseDAO;
 use yii\db\Query;
 
 class AccountPriceLogDAO extends BaseDAO 
 {
	 /**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2018-02-09
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'account_price_log';
 		$this->_pKey ='log_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%account_price_log}}';
 	}
 }