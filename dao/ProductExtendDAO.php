<?php
 namespace app\dao;
 /**
  * @desc 商品分拆
  */
 use Yii;
 use app\enum\EnumOther;
 use app\dao\BaseDAO;
 use yii\db\Query;
 
 class ProductExtendDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2018-04-09
 	 * @return ProductExtendDAO
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'product_extend';
 		$this->_pKey ='product_extend_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%product_extend}}';
 	}
 	/**
 	 * @desc 根据id 获取商品辅助单位
 	 * @param $id 商品id
 	 * @author liaojianwen
 	 * @date 2018-04-09
 	 */
 	public function getProductExtendInfo($id)
 	{
 		$selects = "product_extend_id,d.product_id,d.goods_type,p.product_name";
 		$conditions ="d.pid = :id and d.delete_flag = :flag";
 		$params = [
 				':id'=>$id,
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$query = new Query();
 		$result = $query->select($selects)
 		->from("$this->_table d")
 		->leftJoin("product p",'d.product_id = p.product_id')
 		->where($conditions,$params)
 		->all();
 		return $result;
 	
 	}
 	
 }