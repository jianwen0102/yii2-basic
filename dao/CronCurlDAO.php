<?php
 namespace app\dao;
 
 use Yii;
 use app\dao\BaseDAO;
 use app\helpers\storeTokenTool;
			 
 class CronCurlDAO extends BaseDAO
 {
 	
	public function httpCurl($url, $postdata)
	{

		$key = storeTokenTool::getInstance()->getToken();
		$headers = array(
			"Authorization:{$key}",
			//"Content-Type:application/json",
			//"Content-Length: " . strlen($postdata),
		);				 
		$connection = curl_init();
		curl_setopt($connection, CURLOPT_URL, $url);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($connection, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($connection, CURLOPT_POST, 1);
		curl_setopt($connection, CURLOPT_POSTFIELDS, $postdata);
		$response = curl_exec($connection);
		curl_close($connection);
	    return $response;
	}
}
