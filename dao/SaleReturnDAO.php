<?php
 namespace app\dao;
 /**
  * @desc 销售退货单头操作类
  */
 use yii\db\Query;
 use app\enum\EnumOther;
 use Yii;
 use app\dao\BaseDAO;
 use app\helpers\Utility;
 use app\enum\EnumOriginType;
 
 class SaleReturnDAO extends BaseDAO
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'salereturn';
 		$this->_pKey ='salereturn_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%salereturn}}';
 	}
 	
 	
 	/**
 	 * @desc 获取销售退货列表信息
 	 * @param $condition [] 查询条件
 	 * @param $filter string 过滤条件
 	 * @param $pageInfo [] 页面信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function getSaleReturn($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "i.salereturn_id,salereturn_no,i.salereturn_date,i.warehouse_id,warehouse_name,i.remark,i.handle_man,employee_name,i.confirm_flag,customer_id,
 				client_name,salereturn_type";
 		$conditions = "i.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE
 		);
 	
 		if(isset($condition['warehouse']) && !empty($condition['warehouse'])){
 			$conditions .=" and i.warehouse_id =:wid";
 			$params[':wid'] = $condition['warehouse'];
 		}
 		switch ($filter){
 			case 0 :
 				break;
 			case 1:
 				$conditions .=" and i.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::NO_CONFIRM;
 				break;
 			case 2:
 				$conditions .=" and i.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::CONFIRM;
 				break;
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("warehouse w","i.warehouse_id = w.warehouse_id")
 		->leftJoin("employee e","e.employee_id = i.handle_man")
//  		->leftJoin("supplier s","s.supplier_id = i.customer_id")
		->leftJoin("client c","c.client_id = i.customer_id")
 		->where ( $conditions, $params )
 		->orderBy ( ['i.confirm_flag'=>SORT_ASC,'i.create_time'=>SORT_DESC]);
 	
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','salereturn_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 			
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
 		foreach ($result['list'] as &$list){
 			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$sale_return_type, $list['salereturn_type']);
 		}
 			
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取销售退货单信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function getSaleReturnHead($id)
 	{
 		$selections = "i.salereturn_id,salereturn_no,salereturn_date,i.warehouse_id,i.remark,salereturn_type,i.customer_id,s.client_name,i.saleout_id,c.saleout_no,
				i.handle_man,i.confirm_flag,i.create_man,username,i.total_amount,i.discount_amount,i.total_discount,i.salereturn_amount";
 		$conditions = "i.salereturn_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("admin a","a.id= i.create_man")
//  		->leftJoin("supplier s","s.supplier_id = i.customer_id")
		->leftJoin("client s","s.client_id = i.customer_id")
 		->leftJoin("saleout c","c.saleout_id = i.saleout_id")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }