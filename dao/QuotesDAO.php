<?php
 namespace app\dao;
 /**
  * @desc 报价单单头处理类
  * @author liaojianwen
  * @date 2017-03-27
  */
 use app\dao\BaseDAO;
 use yii\db\Query;
 use app\enum\EnumOther;
 
 class QuotesDAO extends BaseDAO 
 {
 	/**
 	 * @desc 构造函数
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'quotes';
 		$this->_pKey ='quotes_id';
 	}
 	
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%quotes}}';
 	}
 	
 	/**
 	 * @desc 报价单列表
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function getQuotes($cond, $pageInfo)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selects = "quotes_id,quotes_no,quotes_man,customer,quotes_time,receive_date,employee_name,p.remark,p.finish_flag";
		
		$conditions = "p.delete_flag = :flag";
		$params = [ 
				':flag' => EnumOther::NO_DELETE 
		];

		
		$query = new Query ();
		$query->select ( $selects )->from ( "$this->_table p" )
			->leftJoin ( "employee e", "e.employee_id = p.quotes_man" )
			->where ( $conditions, $params )->orderBy ( [ 
				'p.finish_flag' => SORT_ASC,
				'p.create_time' => SORT_DESC 
		] );
		
		if (isset ( $cond ['starTime'] ) && ! empty ( $cond ['starTime'] )) {
			$query->andwhere ( ['between','p.quotes_time',strtotime ( $cond ['starTime'] ),strtotime ( $cond ['endTime'] )] );
		}
		if (isset ( $cond ['customer'] ) && ! empty ( $cond ['customer'] )) {
			$query->andwhere(['like','p.customer',$cond['customer']]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
 			);
 		return $result;
 	}
 	
 	
 	/**
 	 * @desc 根据id 获取报价单单头信息
 	 * @param $id //报价单id
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function getQuotesHead($id)
 	{
 		$selections = "p.quotes_id,quotes_no,quotes_time,customer,p.remark,receive_date,
				quotes_man,p.create_man,username,quotes_amount,finish_flag,discount_amount,total_amount,total_discount";
 		$conditions = "p.quotes_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table p")
 		->leftJoin("admin a","a.id= p.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 }