<?php
 namespace app\dao;
 
 use app\dao\BaseDAO;
 use yii\db\Query;
 use app\enum\EnumOther;
 
 class TransferDAO extends BaseDAO
 {
 	/**
 	* @desc 构造函数
 	* @author liaojianwen
 	* @date 2017-02-07
 	*/
 	public function __construct()
 	{
 		parent::__construct();
 		$this->_table = 'transfer';
 		$this->_pKey ='transfer_id';
 	}
 	
 	/**
 	 * @inheritdoc
 	 */
 	public static function tableName()
 	{
 		return '{{%transfer}}';
 	}
 	
 	
 	/**
 	 * @desc 获取销售出库列表信息
 	 * @param $condition [] 查询条件
 	 * @param $filter string 过滤条件
 	 * @param $pageInfo [] 页面信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function getTransfers($condition, $filter, $pageInfo)
 	{
 		$limit = $pageInfo ['pageSize'];
 		$offset = ($pageInfo ['page'] - 1) * $limit;
 		$selections = "t.transfer_id,transfer_no,t.transfer_date,t.warehouse_out,w.warehouse_name warehouse_out_name, u.warehouse_name warehouse_in_name,t.remark,t.transfer_man,employee_name,t.confirm_flag";
 		$conditions = "t.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE
 		);
 	
 		if(isset($condition['warehouse_out']) && !empty($condition['warehouse_out'])){
 			$conditions .=" and t.warehouse_out =:owid";
 			$params[':owid'] = $condition['warehouse_out'];
 		}
 		switch ($filter){
 			case 0 :
 				break;
 			case 1:
 				$conditions .=" and t.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::NO_CONFIRM;
 				break;
 			case 2:
 				$conditions .=" and t.confirm_flag = :cflag";
 				$params[':cflag'] = EnumOther::CONFIRM;
 				break;
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table t")
 		->leftJoin("warehouse w","t.warehouse_out = w.warehouse_id")
 		->leftJoin("warehouse u","t.warehouse_in = u.warehouse_id")
 		->leftJoin("employee e","e.employee_id = t.transfer_man")
 		->where ( $conditions, $params )
 		->orderBy ( ['t.confirm_flag'=>SORT_ASC,'t.create_time'=>SORT_DESC]);
 	
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','transfer_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		$result ['count'] = $query->count ();
 		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
//  		foreach ($result['list'] as &$list){
// //  			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$sale_type, $list['transfer_type']);
//  		}
 			
 		$result ['page'] = array (
 				'page' => $pageInfo ['page'],
 				'pageSize' => $pageInfo ['pageSize']
 		);
 		return $result;
 	}
 	
 	/**
 	 * @desc 编辑页面调拨单头数据
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function getTransferHead($id)
 	{
 		$selections = "i.transfer_id,transfer_no,transfer_date,i.warehouse_out,i.warehouse_in,i.remark,transfer_type,
				i.transfer_man,confirm_flag,create_man,username,transfer_amount,damage";
 		$conditions = "i.transfer_id =:id";
 		$params = array (
 				':id' => $id,
 		);
 		$query = new Query ();
 		$result = $query->select ( $selections )
 		->from ("$this->_table i")
 		->leftJoin("admin a","a.id= i.create_man")
 		->where ( $conditions, $params )
 		->one();
 		return $result;
 	}
 	
 	
 	/**
 	 * @desc 获取销售出库列表信息
 	 * @param $condition [] 查询条件
 	 * @param $filter string 过滤条件
 	 * @param $pageInfo [] 页面信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function getTransferStatistics($condition, $filter, $pageInfo)
 	{
 		$limit = isset($pageInfo ['pageSize'])?$pageInfo['pageSize']:0;
 		if($limit){
 			$offset = ($pageInfo ['page'] - 1) * $limit;
 		}
 		$selections = "d.product_id,p.product_name,sum(d.quantity) quantity,v.unit_name,u.warehouse_name warehouse_in_name, w.warehouse_name warehouse_out_name";
 		$conditions = "d.delete_flag =:flag";
 		$params = array (
 				':flag' => EnumOther::NO_DELETE
 		);
 	
 		if(isset($condition['warehouse_out']) && !empty($condition['warehouse_out'])){
 			$conditions .=" and t.warehouse_out =:owid";
 			$params[':owid'] = $condition['warehouse_out'];
 		}
 		if(isset($condition['warehouse_in']) && !empty($condition['warehouse_in'])){
 			$conditions .=" and t.warehouse_in =:iwid";
 			$params[':iwid'] = $condition['warehouse_in'];
 		}
 		
 		if(isset($condition['id']) && !empty($condition['id'])){
 			$conditions .=" and p.product_id =:pid";
 			$params[':pid'] =$condition['id'];
 		}
 		$query = new Query ();
 		$query->select ( $selections )
 		->from ("$this->_table t")
 		->leftJoin("transfer_detail d","d.transfer_id = t.transfer_id")
 		->leftJoin("product p","d.product_id = p.product_id")
 		->leftJoin("unit v","v.unit_id = p.quantity_unit")
 		->leftJoin("warehouse w","t.warehouse_out = w.warehouse_id")
 		->leftJoin("warehouse u","t.warehouse_in = u.warehouse_id")
 		->where ( $conditions, $params )
 		->groupBy("d.product_id,t.warehouse_in,t.warehouse_out")
 		->orderBy ( ['d.product_id'=>SORT_ASC]);
 	
 		if(isset($condition['starTime']) && !empty($condition['starTime'])){
 			$query->andwhere(['between','transfer_date',strtotime($condition['starTime']), strtotime($condition['endTime'])]);
 		}
 		
 		if(isset($condition['name']) && !empty($condition['name'])){
 			$query->andWhere(['like','p.product_name', $condition['name']]);
 		}
 		$result ['count'] = $query->count ();
 		if($limit){
 			$query->offset ( $offset )->limit ( $limit );
 		}
 		
 		$result ['list'] = $query->all ();
 		//  		foreach ($result['list'] as &$list){
 		// //  			$list['origin_type'] = Utility::getArrayValue(EnumOriginType::$sale_type, $list['transfer_type']);
 		//  		}
 	
 		$result ['page'] = array (
 		 'page' => isset($pageInfo ['page'])?$pageInfo ['page']:0,
 		 'pageSize' => isset($pageInfo ['pageSize'])?$pageInfo ['pageSize']:0,
 		);
 		return $result;
 	}
 	
	/**
	 * @desc 调拨单明细
	 * @author liaojianwen
	 * @date 2017-09-27
	 */
	public function getExportTransfer($cond, $pageInfo, $filter)
	{
		$limit = $pageInfo ['pageSize'];
		$offset = ($pageInfo ['page'] - 1) * $limit;
		$selects ="p.transfer_id,transfer_no,transfer_date,d.product_id,g.product_name,u.unit_id,u.unit_name,d.base_quantity,transfer_amount,p.remark,g.price";
		$conditions = "d.delete_flag = :flag AND p.warehouse_out!=18 AND p.warehouse_in=18";
		$params  =[
				':flag'=>EnumOther::NO_DELETE,
		];
		
		$query = new Query();
		$query ->select($selects)
		->from("$this->_table p")
		->leftJoin("transfer_detail d", "d.transfer_id = p.transfer_id")
		->leftJoin("product g","g.product_id = d.product_id")
		->leftJoin("unit u", "u.unit_id = d.base_unit")
		->where($conditions,$params)
		->orderBy(['p.transfer_date'=>SORT_DESC,'p.transfer_id'=>SORT_DESC]);
		
		if(!empty($cond['starTime']) && !empty($cond['endTime'])){
			$query->andwhere(['between','transfer_date',strtotime($cond['starTime']), strtotime($cond['endTime'])+86400-1]);
		}
		
		if(isset($cond['name']) && $cond['name']){
			$query->andwhere(['like','g.product_name',$cond['name']]);
		}
		$result ['count'] = $query->count ();
		$result ['list'] = $query->offset ( $offset )->limit ( $limit )->all ();
		
		$result ['page'] = array (
				'page' => $pageInfo ['page'],
				'pageSize' => $pageInfo ['pageSize']
		);
		return $result;
	}
	
	/**
	 * @desc 导出调拨单明细
	 * @author liaojianwen
	 * @date 2017-09-28
	 */
	public function getExproInfo($cond)
	{
		$selects ="d.product_id,transfer_no,transfer_date,g.product_name,u.unit_id,u.unit_name,d.base_quantity,transfer_amount,p.remark,g.price";
		$conditions = "d.delete_flag = :flag AND p.warehouse_out!=18 AND p.warehouse_in=18";
		$params  =[
				':flag'=>EnumOther::NO_DELETE,
		];
		
		$query = new Query();
		$query ->select($selects)
		->from("$this->_table p")
		->leftJoin("transfer_detail d", "d.transfer_id = p.transfer_id")
		->leftJoin("product g","g.product_id = d.product_id")
		->leftJoin("unit u", "u.unit_id = d.base_unit")
		->where($conditions,$params)
		->orderBy(['p.transfer_date'=>SORT_ASC,'p.transfer_id'=>SORT_ASC]);
		// ->groupBy("d.product_id,u.unit_id");
		
		if(!empty($cond['stime']) && !empty($cond['etime'])){
			$query->andwhere(['between','transfer_date',strtotime($cond['stime']), strtotime($cond['etime'])+86400-1]);
		}
		
		if(isset($cond['name']) && $cond['name']){
			$query->andwhere(['like','g.product_name',$cond['name']]);
		}
		$result = $query->all ();
		
		return $result;
	}
 }