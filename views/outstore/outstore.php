<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/outstore/outstore_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '其他出库单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create_outstore">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>