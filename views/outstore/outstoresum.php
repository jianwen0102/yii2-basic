<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/outstore/outstoresum_biz.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/webui-popover/jquery.webui-popover.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerCssFile('@web/statics/webui-popover/jquery.webui-popover.css',['depends'=>['app\assets\AppAsset']]);
	$this->title = '其他出库汇总';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime" value="<?= date('Y-m-d',time());?>"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime" value="<?= date('Y-m-d',time());?>"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品编号" style="margin-right:10px;" id="id"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="name"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="warehouse_in" >
						<option value="0">请选择入库仓库..</option>
						<?php foreach ($stock as $st):?>
							<option value="<?= $st['warehouse_id']?>"><?=$st['warehouse_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
				<!-- 	<button class="btn btn-primary" type="button" id="explode_search" style="margin-left:50px;">导出</button>  -->
				</tr>

			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterOutstore"> -->
<!-- 						<option value="0">显示所有出库单</option> -->
<!-- 						<option value="1">只显示未确认的出库单</option> -->
<!-- 						<option value="2">只显示已确认的出库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr>
						<th class="text-center" name="ids">序号</th>
						<th class="text-center" name="id">商品id</th>
						<th class="text-center" name="name">商品名称</th>
						<th class="text-center" name="unit">单位</th>
						<th class="text-center" name="warehouse_out">出库仓库</th>
						<th class="text-center" name="quantity">数量</th>
					</tr>
				</thead>
				<tbody id="outstoreSummaryList">
					<td colspan="7" class="text-center">.....请选择查询区间.....</td>
				</tbody>
				<tfoot>
					<tr>
						<td class="paginationNavBar" colspan="8"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>