<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/outstore/outstorelist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/webui-popover/jquery.webui-popover.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerCssFile('@web/statics/webui-popover/jquery.webui-popover.css',['depends'=>['app\assets\AppAsset']]);
	$this->title = '出库单列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="stock" >
						<option value="0">请选择仓库..</option>
						<?php foreach ($stock as $st):?>
							<option value="<?= $st['warehouse_id']?>"><?=$st['warehouse_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>

				</tr>

			</table>
			</div>
			<div class="search input-group pull-right">
					<span class="input-group-addon input-former">过滤</span> 
					<select class="form-control" id="filterOutstore">
						<option value="0">显示所有出库单</option>
						<option value="1">只显示未确认的出库单</option>
						<option value="2">只显示已确认的出库单</option>
			<!-- 			<option value="3">不显示库存为负的材料</option>
						<option value="4">显示库存为负的材料</option> -->
					</select>
				</div>
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<col width="1%" />
				<thead>
					<tr>
						<th class="text-center"><input type="checkbox" name=""	id="checkall" /></th>
						<th class="text-center" name="no">出库单号</th>
						<th class="text-center" name="date">出库时间</th>
						<th class="text-center" name="type">类型</th>
						<th class="text-center" name="warehouse">仓库</th>
						<th class="text-center" name="man">出库人员</th>
						<th class="text-center" name="remark">备注</th>
						<th class="text-center">操作</th>
						<th></th>
					</tr>
				</thead>
				<tbody id="outstorelist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
							<button class="btn btn-danger btn-sm" id="delete">删除</button>
						</td>
						<td class="paginationNavBar" colspan="8"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<?=\app\widgets\productInfo\ProductInfo::widget()?>
</div>