<?php
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '编辑成本调整单';
$this->params['breadcrumbs'][] = ['label' => '成本调整单列表', 'url' => ['list-adjustment']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_adjustment">
	<?= $this->render('price_adjustment_form',[ 
				'admin' => $admin,
				'NO' =>$NO,
		])?>
</div>