<?php
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '新增成本调整单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create_adjustment">
	<?= $this->render('price_adjustment_form',[ 
				'admin' => $admin,
				'NO' =>$NO,
		])?>
</div>