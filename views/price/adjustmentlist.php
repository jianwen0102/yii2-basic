<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/price/adjustmentlist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/webui-popover/jquery.webui-popover.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerCssFile('@web/statics/webui-popover/jquery.webui-popover.css',['depends'=>['app\assets\AppAsset']]);
	$this->title = '成本调整单列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
	 			<div class="form-group">
		 			<table class="table table-condensed">
		 				<tr>
							<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
							<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
							<button class="btn btn-info" type="button" id="search_list">查找</button>
						</tr>
		
					</table>
				</div>
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<col width="1%" />
				<thead>
					<tr>
						<th class="text-center"><input type="checkbox" id="checkall"></td>
						<th class="text-center" name="no">调整单号</th>
						<th class="text-center" name="date">调整日期</th>
						<th class="text-center" name="handle_man">调整人员</th>
						<th class="text-center" name="remark">备注</th>
						<th class="text-center">操作</th>
						<th></th>
					</tr>
				</thead>
				<tbody id="adjustmentlist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;"> 
<!-- 						<button class="btn btn-danger btn-sm" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="6"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div id="tableContent" style="display: none;">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>商品名称</th>
					<th>数量</th>
					<th>单位</th>
					<th>单价</th>
					<th>调后单价</th>
					<th>备注</th>
				</tr>
			</thead>
			<tbody id="webuiList">
				<tr>
					<td colspan="7" class="text-center">没有找到数据，请联系程序员</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</div>