<?php
$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/store-admin/store_admin_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '门店列表';
$this->params['breadcrumbs'][] = $this->title;



?>
<div id="listStoreAdmin">
	<div class="col-md-11 col-lg-11" style="margin-bottom:10px;">
		<form class="form-search form-inline">
				<input  class="form-control" type="text" placeholder="门店名称" id="sname">
				<button class="btn btn-info" type="button" onclick="javascript:storeAdmin.searchAdminList();">查找</button>
				<div class="search input-group pull-right">
					<span class="input-group-addon input-former">是否删除</span> 
					<select class="" style="height:35px;border: 1px solid #bdc3d1;" id="filterClient" onchange="javascript:storeAdmin.changeFilter();">
						<option value="-1">全部</option>
						<option value="0">否</option>
						<option value="1">是</option>
					</select>
				</div>
		</form>
	</div>
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center">全选 <input type="checkbox" class="checkAll" onclick="javascript:storeAdmin.selectAll();"></th>
					<th class="text-center">管理员名称</th>
					<th class="text-center">所属门店</th>
					<th class="text-center">添加时间</th>
					<th class="text-center">更新时间</th>
					<th class="text-center">是否已删除</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="store_list">			

			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<a onclick="javascript:storeAdmin.addStoreAdminList();" class="btn btn-primary btn-sm" id="add" href="javascript:;">新增</a>
					<button style="display:none;" onclick="javascript:;" class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="5" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>

	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">新增门店管理员</h4>
				</div>
				<div class="modal-body">
					<form id="stroeAdminAdd">
						<div class="form-group">
							<table class="table table-border-null">
								<tbody>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former"><b
													style="color: red;">*</b>用户名</span> <input type="text"
													class="form-control" aria-describedby="basic-addon1"
													id="adminname" name="adminname" value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">email</span>
												<input type="text" class="form-control"
													aria-describedby="basic-addon1" id="email" name="email"
													value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">									   
												<span class="input-group-addon input-former">所属门店</span>
												<select class="form-control" id="store_id" name="store_id">
													 
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">									   
												<span class="input-group-addon input-former">是否删除</span>
												<select class="form-control" id="delete_flag" name="delete_flag">
													 <option value="-1">请选择</option>
													 <option value="0" selected>否</option>
													 <option value="1">是</option>
												</select>
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">密
													&#12288;&nbsp;码</span> <input type="password"
													class="form-control" aria-describedby="basic-addon1"
													id="password" name="password" value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">确认密码</span> <input
													type="password" class="form-control"
													aria-describedby="basic-addon1" id="repassword"
													name="repassword" value="">
											</div>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<div>
										<td colspan="2" style="text-align: center;">
											<input class="submit btn btn-success" type="submit"
											value="确定" id="user_save" onclick="javascript:storeAdmin.validateInfo();">
											<button type="button" class="btn btn-success" id="reset_info"
												data-dismiss="modal" >取消</button>
										</td>
									</div>
								</tfoot>
							</table>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>