<?php 
$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/store-admin/store_admin_edit_biz.js',['depends'=>['app\assets\AppAsset']]); 
$this->title = '管理员信息';
$this->params['breadcrumbs'][] = ['label' => '门店管理员列表', 'url' => ['list-store-admin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="" class="col-md-12 col-lg-12 col-md-offset-2">
	<form id="list_store_admin" class="col-md-5">
	    <input id="adminid" type="hidden" value="<?=$info->id?>"/>
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tbody>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>&nbsp;&nbsp;用户名&nbsp;&nbsp;</span> 
								<input type="text" class="form-control" value="<?=$info->username?>" id="adminname" name="adminname">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>&nbsp;&nbsp;&nbsp;email&nbsp;&nbsp;&nbsp;</span>
								<input type="text" class="form-control" id="email" value="<?= $info->email?>" name="email">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>所属门店</span>
								<select class="form-control" id="store_id">
								   <option value="">请选择门店</option>							   
                                   <?php foreach($storeList as $v):?>
                                         <option value="<?=$v['store_id']?>" <?php if($info->store_id == $v['store_id']):?>selected<?php endif ?>><?=$v['store_name']?></option>		
								   <?php endforeach;?>
								<select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>是否删除</span>
								<select class="form-control" id="delete_flag">
								   <option value="">请选择</option>							   
								   <option value="0" <?php if($info->delete_flag == 0):?>selected<?php endif ?>>否</option>
								   <option value="1" <?php if($info->delete_flag == 1):?>selected<?php endif ?>>是</option>
								<select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">&nbsp;&nbsp;&nbsp;原密码&nbsp;&nbsp;&nbsp;</span> 
								<input type="password" class="form-control" id="oldpassword" name="oldpassword">
							</div>
							<div class="input-group">
								<label class="col-md-12 col-md-offset-2" style="margin-bottom: 0px;">如果要修改密码,请先填写旧密码,如留空,密码保持不变</label>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">&nbsp;&nbsp;&nbsp;新密码&nbsp;&nbsp;&nbsp;</span> 
								<input type="password" class="form-control" id="newpassword" name="newpassword">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">&nbsp;确认密码&nbsp;</span> 
								<input type="password" class="form-control" id="repassword" name="repassword">
							</div>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<div>
						<td  colspan="2" style="text-align:center;">
							<input class="submit btn btn-success" type="submit" value="更改" onclick="javascript:storeAdminEdit.validateInfo();" id="confirm_info">
							<button type="button" class="btn btn-success" id="reset_info" onclick="javascript:window.location.reload();return false;">重置</button>
						</td>
					</div>
				</tfoot>
			</table>
		</div>
	</form>
</div>
