<?php 
	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/client/addclient_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新增客户';
	$this->params['breadcrumbs'][] = ['label' => '客户列表', 'url' => ['list-client']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add_client">
    <?= $this->render('client_form', [
    		'country'=>$country,
    		'province'=>$province,
    		'city'=>$city,
    		'district'=>$district,
    		'admin'=>$admin,
    		'stores'=>$stores,
    ])?>
</div>