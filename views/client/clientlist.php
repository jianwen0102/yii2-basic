<?php
	$this->registerJsFile('@web/statics/js/client/clientlist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '客户列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div id="listclient">
	<div class="col-md-11 col-lg-11" style="margin-bottom:10px;">
		<form class="form-search form-inline">
				<input  class="form-control" type="text" placeholder="单位名称" id="cname">
				<button class="btn btn-info" type="button" id="searchClient">查找</button>
			</form>
	</div>
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center"><input type="checkbox" class="checkAll"></th>
					<th class="text-center">单位名称</th>
					<th class="text-center">描述</th>
					<th class="text-center">所在地址</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="client_list">
			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<a class="btn btn-primary btn-sm" id="add" href="#">新增</a>
					<button class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="3" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>
</div>