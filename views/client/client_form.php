<?php
?>
<div>
	<a href="javascript:void(0);" id="backUrl">返回上一页</a>
</div>
<div id="addClient" class="col-md-12 col-lg-12 col-md-offset-1">
	<form id="newClient" class="col-md-8">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tbody>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
							<span class="input-group-addon input-former">客户</span> <input
							type="text" class="form-control" value="" id="client" name="client">
							<span class="input-group-addon"  style="color:red;">*</span>
							</div>
							<div class="input-group col-md-12"
									style="margin: 20px 5px 10px 0px;">
									<span class="input-group-addon input-former">负责人</span> <select
									class="form-control" name="handle_man" id="handle_man" title="请选择负责人">
									<option value="">请选择负责人</option>
									<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id'] ?>"><?= $user['employee_name']?></option>
									<?php endforeach;?>
									</select>
							</div>
						</td>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former">客户描述</span>
								<textarea class="form-control" aria-describedby="basic-addon1"
								id="client_info" style="height: 100px;" nameClienter_info"></textarea>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former">联系方式</span>
								<input class="form-control" aria-describedby="basic-addon1"
								id="phone" style="" name="">
							</div>
						</td>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former">所属店铺</span>
								<select
									class="form-control" name="handle_man" id="store_id" title="请选择店铺">
									<option value="0"></option>
									<?php foreach ($stores as $st):?>
									<option value="<?=$st['store_id'] ?>"><?= $st['store_name']?></option>
									<?php endforeach;?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="2">
							<from class="form-inline">
								<div class="input-group">
									<span class="" >地址：</span>
								</div>
								<div class="input-group">
									<select class="form-control region" size="2" style="display: none;" id="re_country">
									<?php foreach ($country as $count): ?>  
									<option value="<?=$count['region_id']?>" 
									<?php if (isset($count['selected'])): ?>
										  selected="selected"
									<?php else: ?>
									<?php endif; ?>
									><?=$count['region_name']?></option>
									<?php endforeach; ?>  
				
								   </select>
								</div>
								<div class="input-group" style="margin-left: 50px;" >
									<select	class="form-control region" size="2" id="re_province" name="re_province">
									<?php foreach ($province as $pro): ?>  
									<option value="<?=$pro['region_id']?>"
									<?php if (isset($pro['selected'])): ?>
									 selected="selected"
									<?php else: ?>
									<?php endif; ?>
									><?=$pro['region_name']?></option>
									<?php endforeach; ?>  
				
								   </select>
								</div>
								<div class="input-group" style="margin-left: 50px;">
									<select	class="form-control region" size="2" id="re_city" name="re_city">
									<option value="">请选择...</option>
									<?php foreach ($city as $ct): ?>  
									<option value="<?=$ct['region_id']?>"
									<?php if (isset($ct['selected'])): ?>
									 selected="selected"
									<?php else: ?>
									<?php endif; ?>
									><?=$ct['region_name']?></option>
									<?php endforeach; ?>  
					
									</select>
								</div>
								<div class="input-group" style="margin-left: 50px;">
									<select	class="form-control region" size="2" id="re_district" name="re_district">
									<option value=0>请选择...</option>
									<?php foreach ($district as $dis): ?>  
									<option value="<?=$dis['region_id']?>"
									<?php if (isset($dis['selected'])): ?>
									selected="selected"
									<?php else: ?>
									<?php endif; ?>
									><?=$dis['region_name']?></option>
									<?php endforeach; ?>  
					
									</select>
								</div>
							</from>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<div>
						<td  colspan="2" style="text-align:right;">
							<button class="btn btn-success"  id="saveClient" data-id="">确定</button>
							<button class="btn btn-success" type="button" id="reset">重置</button>
						</td>
					</div>
				</tfoot>
			</table>
		</div>
	</form>
</div>