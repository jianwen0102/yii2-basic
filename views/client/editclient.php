<?php
	$this->title = '编辑客户';
	$this->params['breadcrumbs'][] = ['label' => '客户列表', 'url' => ['list-client']];
	$this->params['breadcrumbs'][] = $this->title;
	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/client/addclient_biz.js',['depends'=>['app\assets\AppAsset']]);
?>
<div class="edit_client">
    <?= $this->render('client_form', [
    		'country'=>$country,
    		'province'=>$province,
    		'city'=>$city,
    		'district'=>$district,
    		'admin'=>$admin,
    		'stores'=>$stores,
    ]) ?>
</div>