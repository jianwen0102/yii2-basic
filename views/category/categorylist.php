<?php 
	$this->registerCssFile('@web/statics/css/zTree/zTreeStyle.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/zTree/jquery.ztree.core.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/zTree/jquery.ztree.exedit.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/category/category_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '类目管理';
// 	$this->params['breadcrumbs'][] = ['label' => '出库单列表', 'url' => ['list-outstore']];
	$this->params['breadcrumbs'][] = $this->title;
	use yii\helpers\Url;
?>

<div class="reason_list row">
	<div class="col-md-3 col-lg-3" style="padding-left: 0px; height:100%">
		<ul id="tree" class="ztree" style="width:560px; overflow:auto;"></ul>
	</div>
	<div class="col-md-5 col-lg-5" style="max-width: 85%;background-color:#f0f1f4;max-height:1000px;border:1px solid #ddd;">
		<form id="instore_header" class="col-md-12">
			<div class="form-group">
				<table class="table table-border-null">
					<tbody>
					  <tr>
						<td class="">
							<div class="input-group">
								<span class="input-group-addon input-former">类目名称</span>
								<input class="form-control" aria-describedby="basic-addon1" id="cname" value="" type="text">
							</div>
						</td>
					 </tr>
					 <tr>
						<td class="">
							<div class="input-group">
								<span class="input-group-addon input-former">父级类目</span>
								<input class="form-control" aria-describedby="basic-addon1" id="cpid" value="" type="text" data-id="">
							</div>
						</td>
					 </tr>
					<tr>
					    <td class="">
						    <div class="input-group">
                                 <span class="input-group-addon">备注</span>
                                 <input name="remark" class="form-control" id="cremark" type="text"> 
                             </div>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
					<td colspan="2" style="text-align:right;">
							<button id="save_cate" class="btn btn-primary" type="button" data-id="">保存</button>
							<button class="btn btn-success" type="button" id="del_cate" data-id="" >删除 </button>
					</td>
					
					</tr>
				</tfoot>
			</table>
			<div class="form-group" style="text-align: right;">
<!-- 				<button id="save_cate" class="btn btn-primary" type="button" data-id="">保存</button> -->
<!-- 				<button class="btn btn-success" type="button" id="del_cate" data-id="" >删除 </button> -->
			</div>
		</div>
	</form>
	</div>
</div>