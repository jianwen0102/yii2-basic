<?php 
// 	$this->registerCssFile('@web/statics/css/jstree/style.min.css',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/js/jstree.min.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/js/category/category_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '类目管理';
// 	$this->params['breadcrumbs'][] = ['label' => '出库单列表', 'url' => ['list-outstore']];
	$this->params['breadcrumbs'][] = $this->title;
	use yii\helpers\Url;
?>

<div class="reason_list">
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th>ID</th>
					<th>类别名称</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
				<?php
                   function genHtml($proCatTree, $width = 0){
                      if (!empty($proCatTree)){
                         foreach ($proCatTree as $proCat){
                            echo '<tr role="row">';
                            echo '<td>'.$proCat['value']['category_id'].'</td>';
                            echo '<td>';

                            if ($width != 0){
                               echo '<div style="width:'.$width.'px; height:20px; float:left;"></div><div style="background:url(admin/lib/ke01/img/File1.gif) 1px center no-repeat;padding-left:20px; float:left;">';
                            }

                            echo $proCat['value']['category_name'].'</td>';
                            echo '<td>';

//                             if ($proCat['value']['status'] == 1){
//                                echo '<a href="'. Url::toRoute(['admin/product/convercat', 'id'=>$proCat['value']['cat_id'], 'field'=>'status', 'value'=>0]) .'">';
//                                echo '<span class="label label-success">生效</span>';
//                             }else{
//                                echo '<a href="'. Url::toRoute(['admin/product/convercat', 'id'=>$proCat['value']['cat_id'], 'field'=>'status', 'value'=>1]) .'">';
//                                echo '<span class="label label-default">未生效</span>';
//                             }

                            echo '<td>';
                            echo '<a href="'.Url::toRoute(['admin/product/addcat', 'pid'=>$proCat['value']['category_id']]).'" class="btn btn-primary" title="添加子级"><i class="fa fa-plus"></i> 添加子级</a>';
                            echo '<a href="'.Url::toRoute(['admin/product/list', 'catid'=>$proCat['value']['category_id']]).'" class="btn btn-success"><i class="fa fa-search-plus"></i></a>';
                            echo '<a href="'.Url::toRoute(['admin/product/modifycat', 'id'=>$proCat['value']['category_id']]).'" class="btn btn-info" title="修改"><i class="fa fa-edit"></i></a>';
                            echo '<a href="javascript:ConfirmDelSort(\''.Url::toRoute(['admin/product/delcat', 'id'=>$proCat['value']['category_id']]).'\', \'\')" class="btn btn-danger" title="删除"><i class="fa fa-trash-o "></i></a>';
                            echo '</td>';

                            echo '</a>';
                            echo '</td>';
                            echo '</tr>';

                            if (!empty($proCat['list'])){
                               genHtml($proCat['list'], $width + 25);
                            }
                          }
                      }
                  }

                  genHtml($proCatTree);
             ?>
			</tbody>
		</table>
	</div>
</div>