<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/procurement/addprocurement_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新建采购单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create_procurement">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>