<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/procurement/procurementlist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/webui-popover/jquery.webui-popover.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerCssFile('@web/statics/webui-popover/jquery.webui-popover.css',['depends'=>['app\assets\AppAsset']]);
	$this->title = '采购单列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="vendor" >
						<option value="0">请选择供应商..</option>
						<?php foreach ($vendor as $ve):?>
							<option value="<?= $ve['supplier_id']?>"><?=$ve['supplier_name']?></option>
						<?php endforeach;?>
					</select>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="warehouse">
						<option value="0">请选择仓库..</option>
						<?php foreach ($stock as $st):?>
							<option value="<?= $st['warehouse_id']?>"><?=$st['warehouse_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
					<button class="btn btn-success" type="button" id="export_list" style="margin-left:10px;">导出采购商品</button>
				</tr>

			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<col width="1%" />
				<thead>
					<tr>
						<th class="text-center"><input type="checkbox" name=""	id="checkall" /></th>
						<th class="text-center" name="no">采购单号</th>
						<th class="text-center" name="date">采购时间</th>
 						<th class="text-center" name="vendor">供应商</th>
						<th class="text-center" name="warehouse">仓库</th>
						<th class="text-center" name="man">经手人员</th>
						<th class="text-center" name="remark">备注</th>
						<th class="text-center" name="status">状态</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody id="procurementlist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
							<button class="btn btn-danger btn-sm" id="delete">删除</button>
						</td>
						<td class="paginationNavBar" colspan="7"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<?=\app\widgets\productInfo\ProductInfo::widget()?>
</div>

<!-- 上级单据选择 -->
	<div class="modal fade" id="selectPro" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:60%; height:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="">选择订单</h4>
				</div>
				<div class="modal-body"  style="padding-top: 0px;">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;" id="pro_search_field">
						<div class="col-md-1 col-sm-2  input-group" >
							<input class="form-control" placeholder="开始时间" type="text" id="exp-star-time">
						</div>
						<div class="col-md-1  input-group" >
							<input class="form-control" placeholder="截至时间" type="text" id="exp-end-time">
						</div>
						<div class="col-md-2  input-group" >
							<input class="form-control" placeholder="商品名称" type="text" id="good_name">
							
						</div>
						<div class="col-md-2  input-group" >
<!-- 							<input class="form-control vendor" placeholder="商品名称" type="text" id=""> -->
							<select class="input-medium search-query form-control" style="padding-top: 0px;padding-bottom: 0px;" id="exp_vendor" >
								<option value="0">请选择供应商..</option>
								<?php foreach ($vendor as $ve):?>
									<option value="<?= $ve['supplier_id']?>"><?=$ve['supplier_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
				 		<div>
							<button type="button" class="btn btn-success btn-sm" id="searchPro" style=" min-height: 20px;margin-left:10px;">搜索</button>
				 		</div> 
				 		<div style="float:right;">
							<button type="button" class="btn btn-primary btn-sm" id="export_pro" style=" min-height: 20px;margin-left:10px;">导出execl</button>
				 		</div> 
						<div class="col-md-2  input-group" style="float:right;">
							<select class="input-medium search-query form-control" id="order_status">
								<option value="0">全部订单</option>
								<option value="1" selected="selected">未完成订单</option>
								<option value="2">已完成订单</option>
							</select>
						</div> 
					</div>
					<div>
						<table class="table table-bordered" id="pro_head">
							<thead>
								<tr>
<!-- 									<th class="text-center"><input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">单据编号</th>
									<th class="text-center">单据日期</th>
									<th class="text-center">供应商</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">单价</th>
								</tr>
							</thead>
							<tbody id="pro_detail" style="">
								<?php for($i =1; $i< 5; $i++):?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<?php endfor;?>
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar2" colspan="11" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>