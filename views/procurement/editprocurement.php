<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/procurement/addprocurement_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '采购单编辑';
	$this->params['breadcrumbs'][] = ['label' => '采购单列表', 'url' => ['list-procurement']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_procurement">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>