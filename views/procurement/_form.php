<?php
	$this->registerCssFile('@web/statics/searchableselect/jquery.searchableSelect.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/searchableselect/jquery.searchableSelect.js',['depends'=>['app\assets\AppAsset']]);
?>
<div id="procurement">
	<div id="">
	<form id="procurement_header" class="col-md-12">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">采购单号</span> <input
								type="text" class="form-control" value="<?=$NO ?>" id="pro_no"
								disabled>
						</div>
					</td>
 					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">供应商</span>
							<select class="form-control" id="supplier_id">
								<option value="0">请选择供应商...</option>
								<?php foreach ($supplier as $sup):?>
									<option value="<?= $sup['supplier_id']?>"><?= $sup['supplier_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">采购时间</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								value="" id="pro_time">
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">仓库</span>
							<!-- <input 	type="text" class="form-control" aria-describedby="basic-addon1"
								id="warehouse"> -->
							<select class="form-control" id="warehouse">
							<option value="0">请选择仓库...</option>
							<?php foreach($warehouse as $ware) :?>
								<option value="<?= $ware['warehouse_id']?>"><?=$ware['warehouse_name'] ?></option>
							<?php endforeach;?>
							</select>
							<span class="input-group-addon fa fa-plus" title="添加仓库" id="addWarehouse"></span>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">经手人</span> <select
								id="handle_man" class="form-control">
								<option value="0">请选择经手人员...</option>
								<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">制单人</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="create_man" value='<?=Yii::$app->user->identity->username?>' data-id="<?= Yii::$app->user->identity->id?>"> 
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3" colspan="1">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">总计金额</span> <input
								type="text" class="form-control" value="0" aria-describedby="basic-addon1" disabled
								id="total_amount">
						</div>
					</td>
					<td class="col-md-3" colspan="1">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">运费及其他</span> <input
								type="text" class="form-control" value="0" aria-describedby="basic-addon1"
								id="freight">
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">订单金额</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="procurement_amount" value='0' data-id=""> 
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3" colspan="2">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">备注</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								id="remark">
						</div>
					</td>
					<td class="col-md-3">
						<div class="">
							<button id="implode_order" class="btn btn-default" type="button" style="padding-left: 20px; padding-right:20px;">调用销售订单</button>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12 table-responsive"" style="padding-left: 12px;position:inherit; overflow:auto;" id="procurement_detail" >
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable table-responsive"
			cellspacing="0" width="100%" style="width:2200px;">
<!-- 			<col width="1%" /> -->
<!-- 			<col width="1%"/> -->
<!-- 			<col width="8%"/> -->
<!-- 			<col width="5%"/> -->
<!-- 			<col width="4%"/> -->
<!-- 			<col width="6%"/> -->
<!-- 			<col width="4%"/> -->
<!-- 			<col width="5%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
<!-- 			<col width="7%"/> -->
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">商品名称</th>
					<th class="text-center">货号</th>
					<th class="text-center">数量</th>
					<th class="text-center">单位</th>
					<th class="text-center">单价</th>
					<th class="text-center">金额</th>
					<th class="text-center">备注</th>
					<th class="text-center">毛重</th>
					<th class="text-center">毛重单位</th>
					<th class="text-center">已到货数量</th>
					<th class="text-center">未到货数量</th>
					<th class="text-center">单位关系</th>
					<th class="text-center">辅助单位</th>
					<th class="text-center">辅助单位数量</th>
					<th class="text-center">辅助单位关系</th>
					<th class="text-center">基本单位</th>
					<th class="text-center">基本单位数量</th>
					<th class="text-center">基本单价</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="procurementList">
				<tr>
					<td class="text-center" name="ids" data-id="">1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle" style="color:red;"></i>
					</a></td>
					<td class="text-center" name="name" data-id=""><input title="商品名称" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="sn"></td>
					<td class="text-center" name="quantity"><input title="数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="unit"><select title="单位" class="form-control unit">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>
					<td class="text-center" name="price"><input title="单价" type="text" value="0.00"></td>
					<td class="text-center" name="amount"><input title="金额" type="text" value="0.00"></td>
					<td class="text-center" name="remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
					<td class="text-center" name="gross_quantity"><input title="毛重数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="gross_unit"><select title="毛重单位" class="form-control unit">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>	
					<td class="text-center" name="rec_quantity"><input title="已到货数量" type="text" value="0" disabled
						class="num stock-input"></td>	
					<td class="text-center" name="minus_quantity"><input title="未到货数量" type="text" value="0" disabled
						class="num stock-input"></td>
					<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>
					<td class="text-center" name="second_unit"><input title="辅助单位" type="text" value=""></td>
					<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>
					<td class="text-center" name="second_relation"><input title="辅助单位关系" type="text" value="0.00"></td>
					<td class="text-center" name="base_unit"><input title="基本单位" type="text" value="0.00"></td>
					<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value=""></td>
					<td class="text-center" name="base_price"><input title="基本单价" type="text" value="0.00"></td>
					<td class="text-center"><a class="stock-list-view" href="javascript:void(0)"
						data-id="0">库存</a></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<!--  <button class="btn btn-primary btn-sm pull-right" id="confirm" style="margin-left: 10px;display:none">确认</button>-->
		<button class="btn btn-success btn-sm pull-right" id="save"  data-id="">保存</button>
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myWarehouse" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">新建仓库</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b>*</b>仓库</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_name" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former">备注</span> <input
											type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_remark"
											value="">
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<!-- 		<div class="pull-left">
						<span>默认:</span> <input type="checkbox" id="default">
					</div> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="warehouse_save"
						data-wid="" data-old="">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:40%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-4 col-sm-4 col-lg-4 col-xs-4  input-group">
							<input class="form-control name" placeholder="商品名称 / 商品ID " type="text" id="productName" onkeyup="if(event.keyCode == 13) $('#searchProduct').click()" />
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center" style="display: none;">成本价</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	<!-- 上级单据选择 -->
	<div class="modal fade" id="chooseOrigin" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%; height:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择销售订单</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3  input-group">
							<input class="form-control vendor" placeholder="客户" type="text" id="cust_id" style="padding-top: 1px; height: 31px; padding-bottom: 1px; padding-right: 1px;">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchOrder" style=" min-height: 30px;">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-bordered" id="origin_head">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">单据编号</th>
									<th class="text-center">单据日期</th>
									<th class="text-center">客户名称</th>
									<th class="text-center">单据金额</th>
									<th class="text-center">备注</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="origin_header" style="">
								<?php for($i =1; $i< 5; $i++):?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<?php endfor;?>
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
					<div style="height: 300px;overflow:scroll;">
						<table class="table table-bordered" id="origin_detail">
							<thead>
								<tr>
									<th class="text-center"><input type="checkbox" id="checkAll" checked></th>
<!-- 									<th class="text-center">序号</th> -->
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">已发数量</th>
									<th class="text-center">未发数量</th>
<!-- 									<th class="text-center">操作</th> -->
								</tr>
							</thead>
							<tbody id="origin_det">
								<?php for($i =1; $i< 5; $i++):?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<?php endfor;?>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>