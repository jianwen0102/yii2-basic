<?php
// 	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/instore/instore_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑入库单';
	$this->params['breadcrumbs'][] = ['label' => '入库单列表', 'url' => ['list-instore']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_instore">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
// 				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>