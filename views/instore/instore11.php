<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/instore/instore_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新建入库单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div id="instore">
	<div id="">
	<form id="instore_header" class="col-md-12">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">入库单号</span> <input
								type="text" class="form-control" value="<?=$NO ?>" id="instore_no"
								disabled>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">单据类型</span>
							<select class="form-control" id="instore_type">
								<?php foreach ($type as $k=>$t):?>
									<option value="<?= $k?>"><?= $t?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">入库时间</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								value="" id="instore_time">
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">仓库</span>
							<!-- <input 	type="text" class="form-control" aria-describedby="basic-addon1"
								id="warehouse"> -->
							<select class="form-control" id="warehouse">
							<option value="0">请选择仓库...</option>
							<?php foreach($warehouse as $ware) :?>
								<option value="<?= $ware['warehouse_id']?>"><?=$ware['warehouse_name'] ?></option>
							<?php endforeach;?>
							</select>
							<span class="input-group-addon fa fa-plus" title="添加仓库" id="addWarehouse"></span>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">入库人员</span> <select
								id="handle_man" class="form-control">
								<option value="0">请选择入库人员...</option>
								<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">制单人</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="create_man" value='<?=Yii::$app->user->identity->username?>' data-id="<?= Yii::$app->user->identity->id?>"> 
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">供应商</span>
							<!-- <input type="text" class="form-control"  value=""	id="vendor"> -->
							<select id="vendor" class="form-control">
							<option value="0">请选择供应商...</option>
							<?php foreach ($supplier as $sup):?>
								<option value="<?=$sup['supplier_id']?>"><?=$sup['supplier_name']?></option>
							<?php endforeach;?>
							</select>
							<span class="input-group-addon fa fa-plus" title="添加供应商" id="addSupplier"></span>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">备注</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								id="remark">
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
	</div>
	<div class="col-md-12 col-lg-12" style="padding-left: 12px;" id="instore_detail">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">商品名称</th>
					<th class="text-center">货号</th>
					<th class="text-center">数量</th>
					<th class="text-center">单位</th>
					<th class="text-center">备注</th>
					<th></th>
				</tr>
			</thead>
			<tbody id="stockList">
				<tr>
					<td>1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle" style="color:red;"></i>
					</a></td>
					<td class="text-center" name="name" data-id=""><input title="商品名称" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="sn"></td>
					<td class="text-center" name="quantity"><input title="数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="unit"><select title="单位" class="form-control unit">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>
					<td class="text-center" name="remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
					<td class="text-center"><a class="stock-list-view" href="javascript:void(0)"
						data-id="0">库存</a></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12">
		<button class="btn btn-success btn-sm pull-right" id="save">入库</button>
<!-- 		<button class="btn btn-danger btn-sm" id="cancel">取消</button> -->
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myWarehouse" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">新建仓库</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b>*</b>仓库</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_name" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former">备注</span> <input
											type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_remark"
											value="">
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<!-- 		<div class="pull-left">
						<span>默认:</span> <input type="checkbox" id="default">
					</div> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="warehouse_save"
						data-wid="" data-old="">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<!-- 模态框（Modal） -->
	<!-- 添加供应商 -->
	<div class="modal fade" id="mySupplier" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">添加供应商</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b>*</b>仓库</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_name" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former">备注</span> <input
											type="text" class="form-control"
											aria-describedby="basic-addon1" id="warehouse_remark"
											value="">
									</div>
								</td>
							</tr>

						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<!-- 		<div class="pull-left">
						<span>默认:</span> <input type="checkbox" id="default">
					</div> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="warehouse_save"
						data-wid="" data-old="">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3  input-group">
							<input class="form-control name" placeholder="商品名称" type="text" id="productName">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

</div>