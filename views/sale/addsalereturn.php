<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/sale/salereturn_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '销售退货单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add_salereturn">
	<?= $this->render('salereturn_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'customer'=>$customer,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>