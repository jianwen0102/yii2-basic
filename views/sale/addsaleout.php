<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/sale/saleout_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '销售出库单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add_saleout">
	<?= $this->render('saleout_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'customer'=>$customer,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>