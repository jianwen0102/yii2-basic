<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/sale/sell_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑销售出库单';
	$this->params['breadcrumbs'][] = ['label' => '销售出库单列表', 'url' => ['list-sale-out']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_sell">
	<?= $this->render('sell_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'customer'=>$customer,
				'NO' => $NO 
		])?>
</div>