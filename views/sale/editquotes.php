<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/sale/quotesmanages_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑采购报价单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_quotes">
	<?= $this->render('quotes_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
// 				'type' => $type,
				'NO' => $NO 
		])?>
</div>