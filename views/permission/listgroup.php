<?php 
	$this->registerJsFile('@web/statics/js/permission/listgroup_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '权限分组';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid"" id="groupManger">
	<div class="row-fluid" id="">
		<div class="span12 col-md-8 col-md-offset-1">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="分组名称" style="margin-right:10px;" id="groupName"/>
					<button class="btn btn-info" type="button" id="search_list">查找</button>

				</tr>

			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
			<div>
				<table class="table table-striped table-hover table-bordered" cellpadding="0"
					cellspacing="0">
					<col width="1%" />
					<thead>
						<tr>
							<th class="text-center"><input type="checkbox" name=""	id="checkall" /></th>
							<th class="text-center" name="name">分组名</th>
							<th class="text-center" >操作</th>
						</tr>
					</thead>
					<tbody id="grouplist">
					</tbody>
					<tfoot>
						<tr>
							<td id="" colspan="2" style="text-align: left;">
								<button class="btn btn-primary btn-sm" id="add">新增</button>
								<button class="btn btn-danger btn-sm" id="delete">删除</button>
							</td>
							<td class="paginationNavBar" colspan="7"
								data-ptype="pagination-sm" style="text-align: right;"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
			<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="">权限分组新增/编辑</h4>
				</div>
				<div class="modal-body">
				<form id="groupinfo">
				<div class="form-group">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former">分组名</span> 
										<input type="text" class="form-control" aria-describedby="basic-addon1"
											id="groupname" name="groupname" value="">
									</div>
								</td>
							</tr>
						</tbody>
						<tfoot>
							<div>
								<td  colspan="2" style="text-align:center;">
<!-- 									<button type="button" class="btn btn-success" id="user_save">确定</button> -->
									<input class="submit btn btn-success" type="submit" value="确定" id="group_save" data-group="">
									<button type="button" class="btn btn-success" id="reset_info" data-dismiss="modal">重置</button>
								</td>
							</div>
						</tfoot>
					</table>
					</div>
				</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	
				<!-- 模态框（Modal） -->
	<div class="modal fade" id="permissionAssign" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="">权限分组</h4>
				</div>
				<div class="modal-body">
<!-- 				<form id="groupinfo"> -->
				<div class="form-group" id="groupBody">
<!-- 					<table>
					<tbody>
						<tr>
							<td><select multiple="multiple" class="authSelect"
								id="leftSelect" style="height: 300px;"><optgroup
										label="商店管理">
										<option class="leftOption" data-auth="admin/manage/dellog">删除员工登录日志</option>
										<option class="leftOption" data-auth="admin/manage/deluser">删除员工</option>
										<option class="leftOption" data-auth="admin/manage/loginlog">查看员工登录日志</option>
										<option class="leftOption" data-auth="admin/manage/modifyuser">编辑员工</option>
										<option class="leftOption" data-auth="admin/shop/get-shop">商店信息</option>
										<option class="leftOption" data-auth="admin/user/invite">邀请用户</option>
									</optgroup>
									<optgroup label="商品管理">
										<option class="leftOption" data-auth="admin/product/add">添加商品</option>
										<option class="leftOption" data-auth="admin/product/addcat">添加商品分类</option>
										<option class="leftOption" data-auth="admin/product/catlist">查看商品分类</option>
										<option class="leftOption" data-auth="admin/product/colorlist">查看商品颜色</option>
										<option class="leftOption"
											data-auth="admin/product/cuttinglist">查看商品切工</option>
										<option class="leftOption" data-auth="admin/product/del">删除产品信息</option>
										<option class="leftOption" data-auth="admin/product/delcat">删除产品分类</option>
										<option class="leftOption" data-auth="admin/product/delsitem">删除产品属性</option>
										<option class="leftOption"
											data-auth="admin/product/download-model">下载商品导入模板</option>
										<option class="leftOption" data-auth="admin/product/edit">编辑产品信息</option>
										<option class="leftOption" data-auth="admin/product/editcat">修改产品分类</option>
										<option class="leftOption" data-auth="admin/product/editsitem">编辑商品其他属性</option>
										<option class="leftOption" data-auth="admin/product/explod">商品导出</option>
										<option class="leftOption"
											data-auth="admin/product/import-page">商品导入</option>
										<option class="leftOption" data-auth="admin/product/list">查看商品</option>
										<option class="leftOption" data-auth="admin/product/modify">编辑商品</option>
										<option class="leftOption"
											data-auth="admin/product/product-in-out-log">查看产品出入库记录</option>
										<option class="leftOption" data-auth="admin/product/ranklist">查看商品等级</option>
										<option class="leftOption" data-auth="admin/product/sharplist">查看商品形状</option>
									</optgroup>
									<optgroup label="报表中心">
										<option class="leftOption"
											data-auth="admin/report/in-update-log">查看入库单修改记录</option>
										<option class="leftOption"
											data-auth="admin/report/out-update-log">查看出库单修改记录</option>
										<option class="leftOption"
											data-auth="admin/report/stock-in-report">入库报表</option>
										<option class="leftOption"
											data-auth="admin/report/stock-out-report">出库报表</option>
									</optgroup>
									<optgroup label="仓库管理">
										<option class="leftOption" data-auth="admin/stock/stock-in">新建入库单</option>
										<option class="leftOption" data-auth="admin/stock/stock-log">出入库明细</option>
										<option class="leftOption" data-auth="admin/stock/stock-out">新建出库单</option>
										<option class="leftOption" data-auth="admin/unit/add">添加单位</option>
										<option class="leftOption" data-auth="admin/unit/conversion">单位状态转换</option>
										<option class="leftOption" data-auth="admin/unit/del">删除单位</option>
										<option class="leftOption" data-auth="admin/unit/edit">添加或修改单位资料</option>
										<option class="leftOption" data-auth="admin/unit/list">查看单位</option>
										<option class="leftOption" data-auth="admin/unit/modify">修改单位</option>
										<option class="leftOption" data-auth="admin/warehouse/add">添加仓库</option>
										<option class="leftOption" data-auth="admin/warehouse/del">删除仓库</option>
										<option class="leftOption"
											data-auth="admin/warehouse/explod-pro">仓库导出</option>
										<option class="leftOption" data-auth="admin/warehouse/list">查看仓库</option>
										<option class="leftOption" data-auth="admin/warehouse/modify">编辑仓库</option>
									</optgroup>
									<optgroup label="权限管理">
										<option class="leftOption"
											data-auth="admin/usergroup/group-list">自定义分组</option>
									</optgroup></select></td>
							<td><div style="margin-bottom: 10px;"><i class="fa fa-angle-double-right  btn btn-success" ></i></div><div><i class="fa fa-angle-double-left  btn btn-success"></i></div></td>
							<td><select multiple="multiple" class="authSelect"
								id="rightSelect" style="height: 300px;"><option
										class="rightOption" data-auth="admin/product/download-model">下载商品导入模板</option>
									<option class="rightOption" data-auth="admin/product/list">查看商品</option>
									<option class="rightOption"
										data-auth="admin/product/import-page">商品导入</option>
									<option class="rightOption"
										data-auth="admin/report/stock-in-report">入库报表</option>
									<option class="rightOption"
										data-auth="admin/report/stock-out-report">出库报表</option>
									<option class="rightOption" data-auth="admin/stock/stock-in">新建入库单</option>
									<option class="rightOption" data-auth="admin/stock/stock-out">新建出库单</option></select></td>
						</tr>
					</tbody>
				</table>-->
				</div>
				<div class="TFOOT text-center" >
					<span id="saveAssign" data-group=""><a class="btn btn-success" style="margin: 0;">确定</a></span>
					<span id="cancleAssign"><a class="btn btn-success" style="margin: 0;"  data-dismiss="modal">取消</a></span>
				</div>
<!-- 				</form> -->
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
</div>