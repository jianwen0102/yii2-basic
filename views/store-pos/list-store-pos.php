<?php

$this->registerJsFile('@web/statics/js/store-pos/store_pos_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = 'pos机列表';
$this->params['breadcrumbs'][] = $this->title;



?>
<div id="listsupplier">
	<div class="col-md-11 col-lg-11" style="margin-bottom:10px;">
		<form class="form-search form-inline">
				<input  class="form-control" type="text" placeholder="pos名称" id="sname">
				<button class="btn btn-info" type="button" onclick="javascript:storePosList.searchStoreList();">查找</button>
				<div class="search input-group pull-right">
					<span class="input-group-addon input-former">状态</span> 
					<select class="" style="height:35px;border: 1px solid #bdc3d1;" id="filterClient" onchange="javascript:storePosList.changeFilter();">
						<option value="-1">全部</option>
						<option value="0">正常</option>
						<option value="1">屏蔽</option>
					</select>
				</div>
		</form>
	</div>
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center">全选 <input type="checkbox" class="checkAll" onclick="javascript:storePosList.selectAll();"></th>
					<th class="text-center">pos机名称</th>
					<th class="text-center">所属门店</th>
					<th class="text-center">添加时间</th>
					<th class="text-center">状态</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="store_list">			

			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<a onclick="javascript:storePosList.addStoreList();" class="btn btn-primary btn-sm" id="add" href="javascript:;">新增</a>
					<button style="display:none;" onclick="javascript:;" class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="5" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>

	<!-- 修改模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">pos机修改</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b style="color:red;">* </b>pos机名称</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="pos_name" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">									   
	                                    <span class="input-group-addon input-former"><b style="color:red;">* </b> 所属门店</span>
										<select class="form-control" id="store_id" name="store_id">
										</select>
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">									   
	                                    <span class="input-group-addon input-former"><b style="color:red;">* </b> 状&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;态</span>
										<select class="form-control" id="delete_flag" name="delete_flag">
												<option value="-1">请选择</option>
												<option value="0">正常</option>
												<option value="1">屏蔽</option>
										</select>
									</div>
								</td>
							</tr>	
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="pos_save"  data-id="" onclick="javascript:storePosList.saveStorList(this);">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>