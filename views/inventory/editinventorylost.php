<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑盘亏单';
	$this->params['breadcrumbs'][] = ['label' => '盘亏列表', 'url' => ['list-inventory-lost']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_inventory_full">
	<?= $this->render('lost_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $NO 
		])?>
</div>
