<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/inventory/inventory_biz.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->title = '新建盘点单';
// 	$this->params['breadcrumbs'][] = $this->title;
?>
	<div id="">
	<form id="inventory_header" class="col-sm-12 col-md-12 col-lg-12">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tr>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">盘点单号</span> <input
								type="text" class="form-control" value="<?=$NO ?>" id="inventory_no"
								disabled>
						</div>
					</td>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">盘点时间</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								value="" id="inventory_date">
						</div>
					</td>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">制单人</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="create_man" value='<?=Yii::$app->user->identity->username?>' data-id="<?= Yii::$app->user->identity->id?>"> 
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">仓库</span>
							<select class="form-control" id="warehouse">
							<option value="0">请选择仓库...</option>
							<?php foreach($warehouse as $ware) :?>
								<option value="<?= $ware['warehouse_id']?>"><?=$ware['warehouse_name'] ?></option>
							<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">经手人</span> <select
								id="handle_man" class="form-control">
								<option value="0">请选择经手人...</option>
								<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3 col-sm-3 col-lg-2">
						<div class="input-group">
							<span class="input-group-addon input-former">盈亏总金额</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1" disabled="disabled"
								id="total_amount">
						</div>
					</td>

				</tr>
				<tr>
					<td class="col-md-3 col-sm-3 col-lg-2" colspan="2">
						<div class="input-group">
							<span class="input-group-addon input-former">备注</span> 
							<input type="text" class="form-control" aria-describedby="basic-addon1"  id="remark">
						</div>
					</td>
	
				</tr>
			</table>
		</div>
	</form>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12" style="padding-left: 12px;overflow:auto;" id="inventory_detail">
		<table id="" class="table table-striped table-bordered bootstrap-datatable" cellspacing="0" width="100%" style="min-width:1920px;">
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">商品名称</th>
					<th class="text-center">货号</th>
					<th class="text-center">单位</th>
					<th class="text-center">盈亏数量</th>
					<th class="text-center">库存数量</th>
					<th class="text-center">盘点数量</th>
					<th class="text-center">成本价</th>
					<th class="text-center">盈亏金额</th>
					<th class="text-center">单位关系</th>
					<th class="text-center">辅助单位</th>
					<th class="text-center">辅助单位数量</th>
					<th class="text-center">辅助单位关系</th>
					<th class="text-center">基本单位</th>
					<th class="text-center">基本单位数量</th>
					<th class="text-center">基本单价</th>
					<th class="text-center">备注</th>
<!-- 					<th></th> -->
				</tr>
			</thead>
			<tbody id="stockList">
				<tr>
					<td>1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle" style="color:red;"></i>
					</a></td>
					<td class="text-center" name="name" data-id=""><input title="商品名称" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="sn"></td>
					<td class="text-center" name="unit"><select title="单位" class="form-control unit" style="">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>
					<td class="text-center" name="lost_quantity"><input title="盈亏数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="quantity"><input title="库存数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="inventory_quantity"><input title="盘点数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="price"><input title="成本价" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="lost_amount"><input title="盈亏金额" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="unit_relation"><input title="单位关系" type="text" value="0.00"></td>
					<td class="text-center" name="second_unit"><input title="辅助单位" type="text" value=""></td>
					<td class="text-center" name="second_quantity"><input title="辅助单位数量" type="text" value="0.00"></td>
					<td class="text-center" name="second_relation"><input title="辅助单位关系" type="text" value="0.00"></td>
					<td class="text-center" name="base_unit"><input title="基本单位" type="text" value="0.00"></td>
					<td class="text-center" name="base_quantity"><input title="基本单位数量" type="text" value=""></td>
					<td class="text-center" name="base_price"><input title="基本单价" type="text" value="0.00"></td>
					<td class="text-center" name="remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
			<!-- 		<td class="text-center"><a class="stock-list-view" href="javascript:void(0)"
						data-id="0">库存</a></td> -->
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12" style="text-align: right;">
		<button class="btn btn-success btn-sm " id="save">保存</button>
		<button class="btn btn-success btn-sm" id="confirm" style="display: none;">确认</button>
	</div>
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3  input-group">
							<input class="form-control name" placeholder="商品名称 / 商品ID " type="text" id="productName" onkeyup="if(event.keyCode == 13) $('#searchProduct').click()" />
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">成本价</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

