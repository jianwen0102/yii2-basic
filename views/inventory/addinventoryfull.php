<?php
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '新建盘盈单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="add_inventory_full">
	<?= $this->render('full_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $NO 
		])?>
</div>
