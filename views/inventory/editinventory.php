<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑盘点单';
	$this->params['breadcrumbs'][] = ['label' => '盘点列表', 'url' => ['list-inventory']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_inventory">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $NO 
		])?>
</div>
