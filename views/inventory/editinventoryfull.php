<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑盘盈单';
	$this->params['breadcrumbs'][] = ['label' => '盘盈列表', 'url' => ['list-inventory-full']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_inventory_full">
	<?= $this->render('full_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $NO 
		])?>
</div>
