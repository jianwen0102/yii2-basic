<?php
	$this->registerJsFile('@web/statics/js/funds/vendorpay_biz.js',['depends'=>['app\assets\AppAsset']]);
	
	$cssConfig = ".table > thead > tr > th, .table > tfoot > tr > th {background-color:#f0f1f4}";
	$this->registerCss($cssConfig,['depends'=>['app\assets\AppAsset']]);
?>
<div id="payment_pay">
	<div id="">
	<form id="payment_header" class="col-md-12">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">请款单号</span> <input
								type="text" class="form-control" value="<?= $NO?>" id="payment_no"
								disabled>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">申请时间</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								value="" id="payment_date">
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">请款部门</span>
							<select class="form-control" id="department">
								<option value="0">请选择请款部门...</option>
								<?php foreach ($depart as $dep):?>
									<option value="<?= $dep['depart_id']?>"><?= $dep['depart_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">供应商</span> <select
								id="vendor" class="form-control">
								<option value="0">请选择供应商...</option>
								<?php foreach ($supplier as $sup):?>
									<option value="<?=$sup['supplier_id']?>"><?=$sup['supplier_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">收款账户名称</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1"
								id="account_name" value='' data-id=""> 
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">开户银行</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" 
								id="account_bank" value='' data-id=""> 
						</div>
					</td>
				
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">银行账号</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								id="account">
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">经手人员</span> <select
								id="handle_man" class="form-control">
								<option value="0">请选择人员...</option>
								<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">总金额</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="total_amount">
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3" colspan="2">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">备注</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								id="remark">
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">制单人</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="create_man" value='<?=Yii::$app->user->identity->username?>' data-id="<?= Yii::$app->user->identity->id?>"> 
						</div>
					</td>
				</tr>

			</table>
		</div>
	</form>
	</div>
	<div style="margin-left:10px;">
	<ul id="myTab" class="nav nav-tabs col-md-12" style="min-width: ;">
		<li class="active" id="aa"><a href="#detail" data-toggle="tab">明细</a>
		</li>
		<li id="bb" style="display: none;"><a href="#summary" data-toggle="tab">汇总</a></li>
	</ul>
	</div>
<div id="myTabContent" class="tab-content">	
	<div class="tab-pane fade in active" id="detail">
	<div class="col-md-12 col-lg-12 col-sm-12" style="margin-top:10px;" id="payment_detail">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">商品名称</th>
					<th class="text-center">进货日期</th>
					<th class="text-center">数量</th>
					<th class="text-center">单位</th>
					<th class="text-center">单价</th>
					<th class="text-center">金额</th>
					<th class="text-center">备注</th>
				</tr>
			</thead>
			<tbody id="stockList">
				<tr>
					<td class="text-center" name="ids" data-payid="" data-id="">1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle" style="color:red;"></i>
					</a></td>
					<td class="text-center" name="product" data-id=""><input title="商品名称" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="payment_date"></td>
					<td class="text-center" name="quantity"><input title="数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="unit"><select title="单位" class="form-control unit">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>
					<td class="text-center" name="price"><input title="单价" type="text" value="0.00"></td>
					<td class="text-center" name="amount"><input title="金额" type="text" value="0.00"></td>
					<td class="text-center" name="remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="text-centor" style="font-size: 15px;">total</td>
					<td colspan="6"></td>
					<td class=""><input id="total_payamount" type="text" value="0.00" disabled></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<!-- <button class="btn btn-primary btn-sm pull-right" id="confirm" style="margin-left: 10px;display:none">确认</button>-->
		<button class="btn btn-info btn-sm pull-right" id="explode" style="margin-left: 10px;display:none;">导出</button>
		<button class="btn btn-success btn-sm pull-right" id="save"  data-id="" data-sum-time="" >保存</button>
	</div>
	
	</div>
	<div class="tab-pane fade" id="summary">
		<div class="col-md-12 col-lg-12 col-sm-12" style="margin-top:10px;" id="summary_payment_detail">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th class="text-center">商品名称</th>
					<th class="text-center">进货日期</th>
					<th class="text-center">数量</th>
					<th class="text-center">单位</th>
					<th class="text-center">单价</th>
					<th class="text-center">金额</th>
					<th class="text-center">备注</th>
				</tr>
			</thead>
			<tbody id="summaryList">
				<tr>
					<td class="text-center" name="sum_ids" data-payid="" data-id="">1</td>
					<td class="text-center" name="sum_product" data-id=""><input title="商品名称" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="sum_payment_date"></td>
					<td class="text-center" name="sum_quantity"><input title="数量" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="sum_unit"><select title="单位" class="form-control unit">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select></td>
					<td class="text-center" name="sum_price"><input title="单价" type="text" value="0.00"></td>
					<td class="text-center" name="sum_amount"><input title="金额" type="text" value="0.00"></td>
					<td class="text-center" name="sum_remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="text-centor" style="font-size: 15px;">total</td>
					<td colspan="5"></td>
					<td class=""><input id="summary_amount" type="text" value="0.00" disabled></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<button class="btn btn-info btn-sm pull-right" id="explodeSum" style="margin-left: 10px;">导出汇总</button>
<!-- 		<button class="btn btn-success btn-sm pull-right" id="sum_save"  data-id="">保存</button> -->
	</div>
	</div>
</div>
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12 col-sm-12 col-lg-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3 col-sm-12 col-lg-2  input-group">
							<input class="form-control name" placeholder="商品名称" type="text" id="productName">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	
		<div class="modal fade" id="chooseOrigin" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择应付单</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="searchQuery"  input-group">
							<input class="input-medium search-query form-control" type="text" placeholder="开始时间"  id="starTime"/>
							<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间"  id="endTime"/>
							<input class=" input-medium  search-query  form-control" placeholder="单据编号" type="text" id="origin_id">
							<span class="input-group-btn" style="margin-left:10px;display:inline-flex;">
								<button type="button" class="btn btn-primary btn-sm" id="searchPay" style=" min-height:25px;padding-top: 0px;padding-bottom: 0px;">搜索</button>
								<button type="button" class="btn btn-info btn-sm pull-right" id="choosiePay" style=" min-height:25px;padding-top: 0px;padding-bottom: 0px;margin-left:50px;">选择</button>
							</span>
							
						</div>
					</div>
					<div  style="min-width:1000px; min-height:150px;max-height:550px; overflow-y:scroll;">
						<table class="table table-bordered" id="origin">
							<thead>
								<tr>
									<th class="text-center"><input type="checkbox" id="checkAll" checked></th>
									<th class="text-center">序号</th>
									<th class="text-center">进货日期</th>
									<th class="text-center">单据编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">单位</th>
									<th class="text-center">数量</th>
									<th class="text-center">单价</th>
									<th class="text-center">金额</th>
									<th class="text-center">备注</th>
								</tr>
							</thead>
							<tbody id="origin_header" style="">
								<?php for($i =1; $i< 5; $i++):?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<?php endfor;?>
							</tbody>
<!-- 							<tfoot> -->
<!-- 								<tr> -->
									<td class="paginationNavBar" colspan="10" data-ptype="pagination-sm"	style="text-align: right;"></td>
<!-- 								</tr> -->
<!-- 							</tfoot> -->
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>