<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/funds/settlepaylistlist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/webui-popover/jquery.webui-popover.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerCssFile('@web/statics/webui-popover/jquery.webui-popover.css',['depends'=>['app\assets\AppAsset']]);
	$this->title = '应付结算单列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="vendor" >
						<option value="0">请选择往来单位..</option>
						<?php foreach ($supplier as $su):?>
							<option value="<?= $su['supplier_id']?>"><?=$su['supplier_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>

				</tr>

			</table>
			</div>
			<div class="search input-group pull-right">
					<span class="input-group-addon input-former">过滤</span> 
					<select class="form-control" id="filterPay">
						<option value="0">显示所有入库单</option>
						<option value="1">只显示未确认的入库单</option>
						<option value="2">只显示已确认的入库单</option>
					</select>
				</div>
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<col width="1%" />
				<thead>
					<tr>
						<th class="text-center"><input type="checkbox" id="checkall"></td>
						<th class="text-center" name="no">结算单单号</th>
						<th class="text-center" name="date">结算日期</th>
						<th class="text-center" name="vendor">往来单位</th>
						<th class="text-center" name="settle_man">经手人</th>
						<th class="text-center" name="settle_type">应付款类型</th>
						<th class="text-center" name="settle_amount">付款金额</th>
						<th class="text-center" name="discount_amount">优惠金额</th>
<!-- 						<th class="text-center" name="settle_amount">已付款</th> -->
<!-- 						<th class="text-center" name="settle_amount">余额</th> -->
<!-- 						<th class="text-center" name="origin_type">相关单据类型</th> -->
						<th class="text-center" name="remark">备注</th>
<!-- 						<th class="text-center" name="state">结销状态</th> -->
						<th class="text-center">操作</th>
						<th></th>
					</tr>
				</thead>
				<tbody id="settlelist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
<!-- 							<button class="btn btn-info btn-sm" id="account">付款核销</button> -->
						</td>
						<td class="paginationNavBar" colspan="9"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div id="tableContent" style="display: none;">
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>应付款单号</th>
					<th>发生日期</th>
					<th>付款类型</th>
					<th>经手人</th>
					<th>应付金额</th>
					<th>已结销金额</th>
					<th>剩余金额</th>
				</tr>
			</thead>
			<tbody id="webuiList">
				<tr>
					<td colspan="8" class="text-center">没有找到数据，请联系程序员</td>
				</tr>
			</tbody>
		</table>
	</div>
	
</div>