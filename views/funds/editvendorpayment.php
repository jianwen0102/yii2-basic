<?php
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '编辑请款单';
$this->params['breadcrumbs'][] = ['label' => '请款单列表', 'url' => ['list-vendor-payment']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_payment">
	<?= $this->render('vendor_payment_form',[ 
				'admin' => $admin,
				'supplier' => $supplier,
				'NO' =>$NO,
				'depart' => $depart,
		])?>
</div>