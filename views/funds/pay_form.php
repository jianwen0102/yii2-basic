<?php
	$this->registerJsFile('@web/statics/js/funds/settlepay_biz.js',['depends'=>['app\assets\AppAsset']]);
?>
<div id="settle_pay">
	<div id="">
	<form id="settle_header" class="col-md-12">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">结算单号</span> <input
								type="text" class="form-control" value="<?=$NO ?>" id="settle_no"
								disabled>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">单据类型</span>
							<select class="form-control" id="settle_type">
								<?php foreach ($type as $k=>$t):?>
									<option value="<?= $k?>"><?= $t?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">结算时间</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								value="" id="settle_time">
						</div>
					</td>
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">往来单位</span> <select
								id="vendor" class="form-control">
								<option value="0">请选择往来单位...</option>
								<?php foreach ($supplier as $sup):?>
									<option value="<?=$sup['supplier_id']?>"><?=$sup['supplier_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">付款金额</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1"
								id="settle_amount" value='0' data-id=""> 
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">优惠金额</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" 
								id="discount_amount" value='0' data-id=""> 
						</div>
					</td>
				
				</tr>
				<tr>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">经手人员</span> <select
								id="handle_man" class="form-control">
								<option value="0">请选择人员...</option>
								<?php foreach ($admin as $user):?>
									<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
								<?php endforeach;?>
							</select>
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group col-md-12">
							<span class="input-group-addon input-former">备注</span> <input
								type="text" class="form-control" aria-describedby="basic-addon1"
								id="remark">
						</div>
					</td>
					<td class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon input-former">制单人</span>
							<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
								id="create_man" value='<?=Yii::$app->user->identity->username?>' data-id="<?= Yii::$app->user->identity->id?>"> 
						</div>
					</td>
				</tr>
			</table>
		</div>
	</form>
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12" style="padding-left: 12px;" id="settle_detail">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">应付款单单号</th>
					<th class="text-center">应付款发生日期</th>
<!-- 					<th class="text-center">原单据号</th> -->
					<th class="text-center">应付款类型</th>
					<th class="text-center">经手人</th>
					<th class="text-center">应付金额</th>
					<th class="text-center">已结销金额</th>
					<th class="text-center">当前剩余金额</th>
					<th class="text-center">本次付款</th>
					<th class="text-center">备注</th>
				</tr>
			</thead>
			<tbody id="stockList">
				<tr>
					<td class="text-center" name="ids" data-payid="" data-id="">1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle" style="color:red;"></i>
					</a></td>
					<td class="text-center" name="name" data-id=""><input title="应付单号" type="text" 
						class="product-name stock-input50" disabled=""
						style="height: 30px;width:80%"> <a class="search-btn search-product" title="搜索"
						href="javascript:void(0)"><i class="fa fa-search"></i></a></td>
					<td class="text-center" name="settle_pay_time"><input title="应付款发生日期" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="settle_pay_type"><input title="应付款类型" type="text" 
						class="num stock-input"></td>
					<td class="text-center" name="handle_man"><input title="经手人" type="text" value="0.00"></td>
					<td class="text-center" name="settle_pay_amount"><input title="应付金额" type="text" value="0.00"></td>
					<td class="text-center" name="settle_receive_amount"><input title="已结销金额" type="text" value="0.00"></td>
					<td class="text-center" name="settle_left_amount"><input title="当前剩余金额" type="text" value="0.00"></td>
					<td class="text-center" name="settle_cur_amount"><input title="本次付款金额" type="text" value="0.00"></td>
					<td class="text-center" name="settle_remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="text-centor" style="font-size: 15px;">total</td>
					<td colspan="5"></td>
					<td class=""><input id="total_payamount" type="text" value="0.00" disabled></td>
					<td class=""><input id="total_receiveamount" type="text" value="0.00" disabled></td>
					<td class=""><input id="total_leftamount" type="text" value="0.00" disabled></td>
					<td class=""><input id="total_curamount" type="text" value="0.00" disabled></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<button class="btn btn-primary btn-sm pull-right" id="confirm" style="margin-left: 10px;display:none">确认</button>
		<button class="btn btn-success btn-sm pull-right" id="save"  data-id="">保存</button>
	</div>
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12 col-sm-12 col-lg-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3 col-sm-12 col-lg-2  input-group">
							<input class="form-control name" placeholder="商品名称" type="text" id="productName">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

	
		<div class="modal fade" id="chooseOrigin" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:60%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择应付单</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class=""  input-group">
							<input class="input-medium search-query  col-md-2" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
							<input 	class="input-medium search-query  col-md-2" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
							<input class=" input-medium  search-query  col-md-2" placeholder="单据编号" type="text" id="origin_id" style="margin-right:10px;">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchPay" style=" min-height:25px;padding-top: 0px;padding-bottom: 0px;">搜索</button>
								<button type="button" class="btn btn-info btn-sm pull-right" id="choosiePay" style=" min-height:25px;padding-top: 0px;padding-bottom: 0px;">选择</button>
							</span>
							
						</div>
					</div>
					<div >
						<table class="table table-bordered" id="origin">
							<thead>
								<tr>
									<th class="text-center"><input type="checkbox" id="checkAll" checked></th>
									<th class="text-center">序号</th>
									<th class="text-center">发生日期</th>
									<th class="text-center">单据编号</th>
									<th class="text-center">应付款类型</th>
									<th class="text-center">经手人</th>
									<th class="text-center">应付金额</th>
									<th class="text-center">当前已结销</th>
									<th class="text-center">余额</th>
									<th class="text-center">摘要</th>
									<th class="text-center">备注</th>
								</tr>
							</thead>
							<tbody id="origin_header" style="">
								<?php for($i =1; $i< 5; $i++):?>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<?php endfor;?>
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="11" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>