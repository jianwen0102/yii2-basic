<?php 
	$this->registerJsFile('@web/statics/js/basicinfo/department_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '部门列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div id="listdepartment">
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center"><input type="checkbox" class="checkAll"></th>
					<th class="text-center">部门</th>
					<th class="text-center">描述</th>
					<th class="text-center">负责人</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="depart_list">
			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<button class="btn btn-primary btn-sm" id="add" >新增</button>
					<button class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="3" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">部门编辑/修改</h4>
				</div>
				<div class="modal-body">
				<form id="newdepart" class="col-md-12">
					<div class="form-group">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b style="color: red;">*</b>部门</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="dname" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
                                    	<span class="input-group-addon">描述</span>
                                    	<input name="dremark" class="form-control" type="text" id="dremark"> 
                                	</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
                                    	<span class="input-group-addon">负责人</span>
<!--                                     	<input name="dremark" class="form-control" type="text" id="dmaster">  -->
                                    	<select
											id="dmaster" class="form-control">
											<option value="0">请选择经手人员...</option>
											<?php foreach ($admin as $user):?>
												<option value="<?=$user['employee_id']?>"><?=$user['employee_name']?></option>
											<?php endforeach;?>
										</select>
                                	</div>
								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</form>
				</div>
				<div class="modal-footer">
				<!-- 	<div class="pull-left">
						<span>默认:</span> <input type="checkbox" id="default">
					</div> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="depart_save" data-id="" data-old="">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>