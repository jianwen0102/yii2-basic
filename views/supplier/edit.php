<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserModel */

	$this->title = '编辑供应商';
	$this->params['breadcrumbs'][] = ['label' => '供应商列表', 'url' => ['list-supplier']];
	$this->params['breadcrumbs'][] = $this->title;
	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/supplier/editsupplier_biz.js',['depends'=>['app\assets\AppAsset']]);
?>
<div class="supplier_edit">


    <?= $this->render('_form', ['country'=>$country,'province'=>$province,'city'=>$city,'district'=>$district,'admin'=>$admin]) ?>

</div>
