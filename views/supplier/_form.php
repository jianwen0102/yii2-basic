<?php
$this->registerJsFile('@web/statics/js/dmuploader.js',['depends'=>['app\assets\AppAsset']]);
$this->registerCssFile('@web/statics/lightbox/css/lightbox.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/lightbox/js/lightbox.min.js',['depends'=>['app\assets\AppAsset']]);
?>
<div class="TCGB TC600 addImgTC" style="z-index: 80;">
	<div class="TCBox">
		<h2>
			<span title="TCtitle">上传图片</span> <span class="TCClose"
				id="cancelUserPic"><i class="glyphicon glyphicon-remove"
				id="CloseAdd" title="点击关闭"></i></span>
		</h2>
		<div class="TCBody" user-id="" data-type="add">
			<div id="drop-area-div">
				<p>将图片拖至此处上传</p>
				<div class="uploadBtnBox">
					<span>选择文件...</span> <input type="file" name="files[]"
					accept="image/*" multiple title='Click to add Images'
					id="choose_pic">
				</div>
			</div>
			<div class="clear"></div>
			<p>最多可添加5张图片(支持拖拽上传)</p>
			<p>(注：仅支持JPG、GIF、PNG、BMP、TIF格式图片文件、文件最大1M)</p>
			<ul class="imglist_addwindow" id="imglistli">
				<!--上传图片显示区-->
			</ul>
		</div>
		<input type="hidden" value="1215154" name="tmpdir" id="id_file">
		<div class="TCFoot">
			<span id="determine"><a class="subBtn" style="margin: 0;">确定</a></span>
		</div>
	</div>
</div>

<ul id="myTab" class="nav nav-tabs" style="min-width: 1620px;">
	<li class="active"><a href="#supply_info" data-toggle="tab">基本信息</a>
	</li>
	<li><a href="#photoInfo" data-toggle="tab">附件管理</a></li>
	
</ul>
<div id="myTabContent" class="tab-content">
	<div class="tab-pane fade in active" id ="supply_info">
		<div>
			<a href="javascript:void(0);" id="backUrl">返回上一页</a>
		</div>
		<div id="addSupplier" class="col-md-12 col-lg-12 col-md-offset-1">
			<form id="newSupplier" class="col-md-8">
				<div class="form-group">
					<table class="table table-condensed table-border-null" id="">
						<tbody>
							<tr>
								<td class="col-md-3">
									<div class="input-group col-md-12">
									<span class="input-group-addon input-former">供应商</span> <input
									type="text" class="form-control" value="" id="vendor" name="vendor">
									<span class="input-group-addon"  style="color:red;">*</span>
									</div>
									<div class="input-group col-md-12"
											style="margin: 20px 5px 10px 0px;">
											<span class="input-group-addon input-former">负责人</span> <select
											class="form-control" name="handle_man" id="handle_man" title="请选择负责人">
											<option value="">请选择负责人</option>
											<?php foreach ($admin as $user):?>
											<option value="<?=$user['employee_id'] ?>"><?= $user['employee_name']?></option>
											<?php endforeach;?>
											</select>
									</div>
								</td>
								<td class="col-md-3" colspan="1">
									<div class="input-group" style="">
										<span class="input-group-addon input-former">供应商描述</span>
										<textarea class="form-control" aria-describedby="basic-addon1"
										id="supplier_info" style="height: 100px;" name="supplier_info"></textarea>
									</div>
								</td>
							</tr>
							<tr>
								<td class="col-md-3" colspan="1">
									<div class="input-group" style="">
										<span class="input-group-addon input-former">联系方式</span>
										<input class="form-control" aria-describedby="basic-addon1"
										id="phone" style="" name="">
									</div>
								</td>
								<td class="col-md-3">
									<div class="input-group" style="">
										<span class="input-group-addon input-former">开户银行</span>
										<input class="form-control" aria-describedby="basic-addon1"
										id="bank" style="" name="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon input-former">银行账户名称</span>
										<input class="form-control" aria-describedby="basic-addon1" id="account_name" >
									</div>
								</td>
								<td class="col-md-3">
									<div class="input-group">
										<span class="input-group-addon input-former">银行帐号</span>
										<input class="form-control" aria-describedby="basic-addon1" id="bank_account" >
									</div>
								</td>
							</tr>
							<tr>
								<td class="col-md-3" colspan="2">
									<from class="form-inline">
										<div class="input-group">
											<span class="" >地址：</span>
										</div>
										<div class="input-group">
											<select class="form-control region" size="2" style="display: none;" id="re_country">
											<?php foreach ($country as $count): ?>  
											<option value="<?=$count['region_id']?>" 
											<?php if (isset($count['selected'])): ?>
												  selected="selected"
											<?php else: ?>
											<?php endif; ?>
											><?=$count['region_name']?></option>
											<?php endforeach; ?>  
						
										   </select>
										</div>
										<div class="input-group" style="margin-left: 50px;" >
											<select	class="form-control region" size="2" id="re_province" name="re_province">
											<?php foreach ($province as $pro): ?>  
											<option value="<?=$pro['region_id']?>"
											<?php if (isset($pro['selected'])): ?>
											 selected="selected"
											<?php else: ?>
											<?php endif; ?>
											><?=$pro['region_name']?></option>
											<?php endforeach; ?>  
						
										   </select>
										</div>
										<div class="input-group" style="margin-left: 50px;">
											<select	class="form-control region" size="2" id="re_city" name="re_city">
											<option value="">请选择...</option>
											<?php foreach ($city as $ct): ?>  
											<option value="<?=$ct['region_id']?>"
											<?php if (isset($ct['selected'])): ?>
											 selected="selected"
											<?php else: ?>
											<?php endif; ?>
											><?=$ct['region_name']?></option>
											<?php endforeach; ?>  
							
											</select>
										</div>
										<div class="input-group" style="margin-left: 50px;">
											<select	class="form-control region" size="2" id="re_district" name="re_district">
											<option value=0>请选择...</option>
											<?php foreach ($district as $dis): ?>  
											<option value="<?=$dis['region_id']?>"
											<?php if (isset($dis['selected'])): ?>
											selected="selected"
											<?php else: ?>
											<?php endif; ?>
											><?=$dis['region_name']?></option>
											<?php endforeach; ?>  
							
											</select>
										</div>
									</from>
								</td>
							</tr>
						</tbody>
						<tfoot>
							<div>
								<td  colspan="2" style="text-align:right;">
									<button class="btn btn-success"  id="saveSupplier" data-id="">确定</button>
									<button class="btn btn-success" type="button" id="reset">重置</button>
								</td>
							</div>
						</tfoot>
					</table>
				</div>
			</form>
		</div>
	</div>
	<div class="tab-pane fade" id="photoInfo">
		<div class="col-md-8 col-lg-8 col-sm-8 col-md-offset-1" style="padding-left: 12px; border: 3px dotted;height:420px;overflow:auto;" id="">
			<ul class="imglist_addwindow1">
	                    <!--上传图片显示区-->
	          <label  id="uploadPic"><img src="../statics/images/a11.png" style="width: 190px; height:180px;margin-bottom:10px;"></label>
	       </ul>
		</div>
		<div class="col-md-8 col-sm-8 col-lg-8 col-md-offset-1" style="margin-top: 20px;">
			<button class="btn btn-success btn-sm pull-right" id="saveFile"  data-id="">保存</button>
			<button class="btn btn-primary btn-sm pull-right" id="eidtFile" style="margin-right: 10px;">编辑</button>
			
		</div>
	</div>
</div>