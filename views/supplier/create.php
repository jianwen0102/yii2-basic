<?php 
	$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/supplier/addsupplier_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新增供应商';
	$this->params['breadcrumbs'][] = ['label' => '供应商列表', 'url' => ['list-supplier']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier_create">


    <?= $this->render('_form', ['country'=>$country,'province'=>$province,'city'=>$city,'district'=>$district,'admin'=>$admin]) ?>

</div>