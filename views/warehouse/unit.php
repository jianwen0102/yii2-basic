<?php 
	$this->registerJsFile('@web/statics/js/warehouse/unit_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '单位设置';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div id="listwarehouse">
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center">全选 <input type="checkbox" class="checkAll"></th>
					<th class="text-center">单位</th>
					<th class="text-center">备注</th>
					<th class="text-center">状态</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="unit_list">
			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<button class="btn btn-primary btn-sm" id="add" >新增</button>
					<button class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="3" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">单位编辑/修改</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b>*</b>单位</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="unit" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former">备注</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="description" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
                                    	<span class="input-group-addon"><input name="checkbox"  type="checkbox" id="active"> &#12288;</span>
                                    	<span class="form-control ">是否生效</span>
                                	</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				<!-- 	<div class="pull-left">
						<span>默认:</span> <input type="checkbox" id="default">
					</div> -->
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="unit_save" data-uid="" data-old="">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>