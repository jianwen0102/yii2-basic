<?php 
// 	$this->registerJsFile('@web/statics/js/unit_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '入库单列表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
 	<h2>石料入库列表</h2>
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
				<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
				<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
				<!-- <input class="input-medium search-query form-control" type="text" placeholder="供应商" style="margin-right:10px;" id="vendor"/> -->
				<span class="input-group-addon form-control" style=" border-radius:5px;">供应商</span><select class="input-medium search-query form-control" style="margin-right:10px;" id="vendor" ></select>
				<button class="btn btn-info" type="button" id="search_list">查找</button>
			</form>
			<table class="table table-striped table-hover" cellpadding="0"
				cellspacing="0">
				<col width="1%" />
				<thead>
		<!-- 			<tr class="inBoxConTop">
						<th colspan="9">
							<div class="search input-group pull-right">
								<input type="text" name="" class="form-control" id="searchName"
									placeholder="请输入生产单号" height="64px" /> <span
									class="input-group-btn">
									<button class="btn btn-primary" id="search_btn" title="">搜索</button>
								</span>
							</div>
						</th>
					</tr> -->
					<tr>
						<th class="text-center"><input type="checkbox" name=""	id="checkall" /></th>
						<th class="text-center tablesorter-header" name="no">入库单号</th>
						<th class="text-center tablesorter-header" name="date">入库时间</th>
						<th class="text-center tablesorter-header" name="type">类型</th>
						<th class="text-center tablesorter-header" name="vendor">供应商</th>
						<th class="text-center tablesorter-header" name="warehouse">仓库</th>
						<th class="text-center tablesorter-header" name="man">入库人员</th>
						<th class="text-center tablesorter-header" name="remark">备注</th>
						<th class="text-center">操作</th>
					</tr>
				</thead>
				<tbody id="instorelist">
				</tbody>
				<tfoot>
					<tr>
						<td style="text-align: left;">
							<!-- <button class="btn btn-danger btn-xs" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="9"
							data-ptype="pagination-sm" style="text-align: right;"></td>
						<td id="paginationNavBar" colspan="9"
							style="text-align: right; padding-right: 40px;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="row-fluid" id="bottom">
		<div class="span12" id="expander">
			<table class="table table-striped table-hover">
				<col width="2%" />
				<thead>
					<tr>
						<th class="text-center">序号</th>
						<th class="text-center">商品代码</th>
						<th class="text-center">商品全称</th>
						<th class="text-center">形状</th>
						<th class="text-center">尺寸</th>
						<th class="text-center">等级</th>
						<th class="text-center">颜色</th>
						<th class="text-center">数量(pcs)</th>
						<th class="text-center">重量(ct)</th>
						<th class="text-center">备注</th> 
						<th class="text-center">状态</th> 
					</tr>
				</thead>
				<tbody id="detList">
				</tbody>
				<tfoot>
					<td class="paginationNavBar1" colspan="11"
							data-ptype="pagination-sm" style="text-align: right;"></td>
			<!-- 		<td id="paginationNavBar1" colspan="9"
							style="text-align: right; padding-right: 40px;"></td> -->
				</tfoot>
			</table>
		</div>
	</div>
	 <div id="line"></div>
</div>