<?php
$this->registerCssFile ( '@web/statics/KEditor/themes/default/default.css', ['depends' => ['app\assets\AppAsset']]);
$this->registerJsFile ( '@web/statics/KEditor/kindeditor.js', ['depends' => ['app\assets\AppAsset']]);
$this->registerJsFile ( '@web/statics/KEditor/lang/zh_CN.js', ['depends' => ['app\assets\AppAsset']]);
$this->registerCssFile('@web/statics/lightbox/css/lightbox.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/lightbox/js/lightbox.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/product/addproduct_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '编辑商品';
$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['list-product']];
$this->params['breadcrumbs'][] = $this->title;
function genSelect($info, $spe = '│')
{
	if (! empty ( $info )) {
		foreach ( $info as $item ) {
			if (isset ( $item ['value'] ['id'] )) {
				echo '<option value="' . $item ['value'] ['id'] . '"';
				
				if (isset ( $item ['selected'] ) && $item ['selected'] == TRUE) {
					echo ' selected ';
				}
				
				echo '>' . $spe . $item ['value'] ['name'] . '</option>';
			}
			
			if (! empty ( $item ['list'] )) {
				genSelect ( $item ['list'], $spe . '├' );
			}
		}
	}
}
// dd($supplier);
$sup_select = '';
foreach($supplier as $sup){
	$sup_select .= '<option value="'.$sup['supplier_id'].'"';
	if(isset($sup['selected'])){
		$sup_select .= 'selected = "selected"';
	}
	$sup_select .= '>'.$sup['supplier_name'].'</option>';
}
$unit_select = '';
if(isset($unit['Body'])){
	foreach ($unit['Body'] as $ut){
		$unit_select .= '<option value="'. $ut['unit_id'].'"';
		if(isset($ut['selected'])){
			$unit_select .='selected ="selected"';
		}		
		$unit_select .= '>'.$ut['unit_name'].'</option>';
	}
}


// dd($product);
// $category
$product_name = '';
$big_img ='';
$pics = '';
$pics_a = '';
$pics_first = '';
$warn_quantity = 0;
$description = '';
$product_id = '';
if($product){
	$product_name = $product['product_name'];
	$big_img = $product['product_img'];
	if(isset($product['img']) && !empty($product['img'])){
		foreach ($product['img'] as $k => $img){
			if($k == 0){
				$pics_first = $img['origin_img'];
				$pics .= $img['origin_img'];
			} else{
				$pics .=' , '.$img['origin_img'];
				$pics_a .= '<a href="'.$img['origin_img'].'" data-lightbox="example-set" data-id="'.$img['image_id'].'"></a>';
			}
			
		}
	}
	$warn_quantity =$product['warn_quantity'];
	$description = $product['description'];
	$product_id = $product['product_id'];
}
?>
<ul id="myTab" class="nav nav-tabs" style="width: 2500px; display: none">
	<li class="active" id="aa"><a href="#order" data-toggle="tab">订单信息</a>
	</li>
	<li id="bb"><a href="#shipdet" data-toggle="tab">出货表明细</a></li>
</ul>
<div id="myTabContent" class="tab-content">
	<div class="tab-pane fade in active" id="order">
		<div id="addProduct" class="col-md-12 col-lg-12">
			<form id="productForm" class="form-horizontal" name="editForm">
				<fieldset>
					<div class="form-group">
						<label class="col-md-2 control-label">产品名称：</label>
						<div class="col-md-4">
							<input title="产品名称" name="pname" type="text" id="pname"
								class="form-control" value="<?=$product_name ?>"> <span class="help-block"
								style="color: red;"></span>
						</div>
						<br />
					</div>

					<!-- 				<div class="form-group"> 
 					<label class="col-md-2 control-label">货 号：</label>
					<div class="col-md-4">
						<input title="sn" name="sn" type="text" id="sn"
							class="form-control" value="" placeholder="如果您不输入商品货号，系统将自动生成一个唯一的货号">
							<span class="help-block" style="color: red;"></span>
 					</div> 
				</div> -->

					<div class="form-group">
						<label class="col-md-2 control-label">产品类别：</label>
						<div class="col-md-5">
							<select title="产品类别" size="1" class="form-control" name="catid"
								id="catid">
								<option value="0">请选择...</option>
							<?php
							if (isset ( $category )) {
								genSelect ( $category );
							}
							?>
						</select> <span class="help-block" style="color: red;"> </span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">供应商：</label>
						<div class="col-md-5">
							<select title="供应商" size="1" class="form-control" name="supplier"
								id="supplier">
								<option value="0">请选择...</option>
								<?=$sup_select ?>
						</select> <span class="help-block" style="color: red;"> </span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">基本单位：</label>
						<div class="col-md-5">
							<select title="基本单位" size="1" class="form-control" name="unit"
								id="unit">
								<option value="0">请选择...</option>
								<?=$unit_select?>
						</select> <span class="help-block" style="color: red;"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">产品大图：</label>
						<div class="col-md-5">
							<div class="input-group">
								<input title="产品大图" type="text" id="thumb" name="thumb"
									class="form-control" value="<?=$big_img?>" readonly> <span
									class="input-group-btn">
									<button type="button" id="BigImage" class="btn btn-default">上传图片</button>
								</span> <span class="input-group-btn" style="<?php if($big_img) {echo '';} else{ echo 'display:none;';}  ?> "> <a
									href="<?=$big_img?>" type="button" id="lightBig" class="btn btn-default" data-lightbox="example-1">查看图片</a>
								</span>
							</div>
							<span class="help-block">产品大图主要是做内页显示使用</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">更多产品图片：</label>
						<div class="col-md-5">
							<textarea title="更多产品图片" rows="5" cols="80" name="pics" id="pics"
								class="form-control"><?=$pics ?></textarea>
							<span class="input-group-btn" style="">
								<button type="button" id="J_selectImage" class="btn btn-default">批量上传</button>
								<a type="button" id="J_image" class="btn btn-default" href="<?=$pics_first ?>"
								style="margin-left: 10px; <?php if($pics_first){echo '';} else{ echo 'display:none;';} ?>" data-lightbox="example-set">查看</a>
							</span> <span class="help-block">上传多张案例图片</span>
							<div style="display: none;">
								<?= $pics_a ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-group">
							<label class="col-md-2 control-label">商品预警库存：</label>
							<div class="col-md-5">
								<input type='text' title="商品预警库存" rows="5" cols="80"
									name="warnquan" id="warnquan" class="form-control"
									placeholder="0" value="<?=$warn_quantity ?>" /> <span class="help-block"
									style="color: red;"></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">产品详细介绍：</label>
						<div class="col-md-6">
							<textarea title="产品详细介绍" name="content" id="description"
								style="width: 95%; height: 400px; visibility: hidden;"
								class="form-control"><?= $description?></textarea>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2 control-label"></div>
						<div class="col-md-10">
							<!-- <button type="button" id="save" class="btn btn-danger btn-lg">保存产品信息</button> -->
							<span class="btn btn-danger btn-lg" id="saveProduct" data-id="<?=$product_id?>"
								type="">确定</span>
							<button type="button" class="btn btn-danger btn-lg" value="返回上一页"
								onClick="history.back(-1)">返回上一页</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
	<div class="tab-pane fade" id="shipdet"></div>
</div>