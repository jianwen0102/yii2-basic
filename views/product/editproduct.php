<?php 
// 	$this->registerCssFile('@web/statics/lightbox/css/lightbox.min.css',['depends'=>['app\assets\AppAsset']]);	
// 	$this->registerJsFile('@web/statics/lightbox/js/lightbox.min.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/js/product/editproduct_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑商品';
	$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['list-product']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product_edit">
     <?= $this->render('_form', ['category'=>$category,'supplier'=>$supplier,'unit'=>$unit,'warehouse'=> $warehouse,'product'=>$product]) ?>
</div>