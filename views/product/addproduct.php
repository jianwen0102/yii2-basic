<?php 
// 	$this->registerJsFile('@web/statics/js/product/addproduct_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新增商品';
	$this->params['breadcrumbs'][] = ['label' => '商品列表', 'url' => ['list-product']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product_create">
    <?= $this->render('_form', ['category'=>$category,'supplier'=>$supplier,'unit'=>$unit,'warehouse'=> $warehouse,'product'=>$product]) ?>
</div>