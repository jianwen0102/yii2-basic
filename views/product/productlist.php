<?php
	$this->registerJsFile('@web/statics/js/product/productlist_biz.js?t=20180206',['depends'=>['app\assets\AppAsset']]);
	$this->title = '商品列表';
	$this->params['breadcrumbs'][] = $this->title;
	
	function genSelect($info, $spe = '│')
	{
		if (! empty ( $info )) {
			foreach ( $info as $item ) {
				if (isset ( $item ['value'] ['id'] )) {
					echo '<option value="' . $item ['value'] ['id'] . '"';
	
					if (isset ( $item ['selected'] ) && $item ['selected'] == TRUE) {
						echo ' selected ';
					}
	
					echo '>' . $spe . $item ['value'] ['name'] . '</option>';
				}
					
				if (! empty ( $item ['list'] )) {
					genSelect ( $item ['list'], $spe . '├' );
				}
			}
		}
	}
	$sup_select = '';
	foreach($supplier as $sup){
		$sup_select .= '<option value="'.$sup['supplier_id'].'"';
		if(isset($sup['selected'])){
			$sup_select .= 'selected = "selected"';
		}
		$sup_select .= '>'.$sup['supplier_name'].'</option>';
	}
	
	//检查authitem 是否有permission
	$price_flag = 0;
	$auth = Yii::$app->authManager;
	$actions = 'product/check-implode-product';
	$userId = Yii::$app->user->id;
	if($auth->getPermission($actions)){//只判断在auth_item 的权限
		$res = $auth->checkAccess($userId, $actions);
		if(!$res){
			$price_flag = 1;
		}
	}
?>
<div id="listproduct">
	<div class="col-md-12 col-lg-12" style="padding-left: 0px;">
		<form class="form-search form-inline" onsubmit="$('#search_list').click();return false;">
 			<div class="form-group pull-left">
 			<input class="input-medium search-query form-control" placeholder="产品名称" style="margin-right:10px;" id="select_name" type="text">
 			<select class="input-medium search-query form-control"  style="margin-right:10px;" id="select_category">
 				<option value="0">请选择类目..</option>
 				<?php
					if (isset ( $category )) {
						genSelect ( $category );
					}
				?>
 			</select>
 			<select class="input-medium search-query form-control" style="margin-right:10px;" id="select_supplier">
				<option value="0">请选择供应商..</option>
				<?=$sup_select ?>
			</select>
			<button class="btn btn-info btn-sm" type="button" id="search_list">查找</button>&nbsp;&nbsp;&nbsp;
			<button class="btn btn-warning btn-sm" type="button" id="explode_list">导出</button>
<!-- 			<button class="btn btn-success btn-sm" type="button" id="implode_list" title="更新供货价">导入</button> -->
		
			<table class="table table-condensed">
 			<tbody>
 				<tr>
				</tr>

			</tbody>
			</table>
			</div>
			<div class="search input-group pull-right" style="<?php if($price_flag) {echo 'display:none;';} else {echo '';} ?>">
				<form name="form" id="formm" >  
				     <input type="file" name="execl" id="file_stu" style="float:left;">
				     <input type="button"   style="width:50px; height:25px; display:none; float:left;" id="updateExecl" data-path="" data-name="" value="更新">
				     <input type="button" name="b1" value="导入" onclick="fsubmit()"  style="width:50px; height:25px; float:left;">
				 </form>  
		  	</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 			</div> -->
		</form>
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center"><input type="checkbox" class="checkAll"></th>
					<th class="text-center">产品名称</th>
					<th class="text-center">产品货号</th>
					<th class="text-center">产品类目</th>
					<th class="text-center">供应商</th>
					<th class="text-center">库存</th>
					<th class="text-center">单位</th>
					<th class="text-center">辅助单位</th>
					<th class="text-center">生产日期</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="product_list">
			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<button class="btn btn-primary btn-sm" id="add" >新增</button>
					<button class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>
</div>
<?php 
// 	$this->registerJs('
// 			$(document).ready(function(){
// 	function fsubmit(){  
//         var data = new FormData($("#form1")[0]);  
//         $.ajax({  
//             url: "server.php",  
//             type: "POST",  
//             data: data,  
//             dataType: "JSON",  
//             cache: false,  
//             processData: false,  
//             contentType: false  
//         }).done(function(ret){  
//             if(ret["isSuccess"]){  
//                 var result = "";  
//                 result += "name=" + ret["name"] + "<br>";  
//                 result += "gender=" + ret["gender"] + "<br>";  
//                 result += "<img src="" + ret["photo"]  + "" width="100">";  
//                 $("#result").html(result);  
//             }else{  
//                 alert("提交失敗");  
//             }  
//         });  
//         return false;  
//     }  })');
	?>
	<?php $this->beginBlock('test') ?>
		function fsubmit() {
			var file = $('#file_stu').val();
			if(file.length == 0){
				alertTips('warning','请选择execl');
				return false;
			}
            var formData=new FormData();
            formData.append('file_stu',document.getElementById('file_stu').files[0]);
            var oReq = new XMLHttpRequest();
            oReq.onreadystatechange=function(){
            	
              if(oReq.readyState==4){
                if(oReq.status==200){
               		 var json=JSON.parse(oReq.responseText);
               		 if(json['Ack'] =='Success'){
	                    $('#updateExecl').attr('data-path',json['path']).attr('data-name',json['filename']).show();
	                    
	                    alertTips('success',json['msg']);
                     } else {
                     	alertTips('error',json['msg'])
                     }
                } else {
              		 var json=JSON.parse(oReq.responseText);
                	alertTips('error','失败');
                }
              }
            }
            oReq.open("POST", "implode-product-list");
            oReq.send(formData); 
            return false;
        } 
	<?php $this->endBlock() ?>  
	<?php $this->registerJs($this->blocks["test"], \yii\web\View::POS_END); ?>
