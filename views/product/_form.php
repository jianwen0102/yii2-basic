<?php
$this->registerCssFile ( '@web/statics/KEditor/themes/default/default.css', ['depends' => ['app\assets\AppAsset']]);
$this->registerJsFile ( '@web/statics/KEditor/kindeditor.js', ['depends' => ['app\assets\AppAsset']]);
$this->registerJsFile ( '@web/statics/KEditor/lang/zh_CN.js', ['depends' => ['app\assets\AppAsset']]);
$this->registerCssFile('@web/statics/lightbox/css/lightbox.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/lightbox/js/lightbox.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/product/addproduct_biz.js?t=20171218',['depends'=>['app\assets\AppAsset']]);
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
function genSelect($info, $spe = '│')
{
	if (! empty ( $info )) {
		foreach ( $info as $item ) {
			if (isset ( $item ['value'] ['id'] )) {
				echo '<option value="' . $item ['value'] ['id'] . '"';
				
				if (isset ( $item ['selected'] ) && $item ['selected'] == TRUE) {
					echo ' selected ';
				}
				
				echo '>' . $spe . $item ['value'] ['name'] . '</option>';
			}
			
			if (! empty ( $item ['list'] )) {
				genSelect ( $item ['list'], $spe . '├' );
			}
		}
	}
}
$sup_select = '';
foreach($supplier as $sup){
	$sup_select .= '<option value="'.$sup['supplier_id'].'"';
	if(isset($sup['selected'])){
		$sup_select .= 'selected = "selected"';
	}
	$sup_select .= '>'.$sup['supplier_name'].'</option>';
}
$unit_select = '';
if(isset($unit['Body'])){
	foreach ($unit['Body'] as $ut){
		$unit_select .= '<option value="'. $ut['unit_id'].'"';
		if(isset($ut['selected'])){
			$unit_select .='selected ="selected"';
		}		
		$unit_select .= '>'.$ut['unit_name'].'</option>';
	}
}
$ware_select = '';
foreach($warehouse as $ware){
	$ware_select .= '<option value="'.$ware['warehouse_id'].'"';
	if(isset($ware['selected'])){
		$ware_select .= 'selected = "selected"';
	}
	$ware_select .= '>'.$ware['warehouse_name'].'</option>';
}

$product_name = '';
$big_img ='';
$pics = '';
$pics_a = '';
$pics_first = '';
$warn_quantity = 0;
$description = '';
$product_id = '';
$price = 0;
$supply_price = 0;
$guide_price = 0;
$guide_price_jin = 0;
$barcode = '';
$produced_time = '';//生产日期
$quality_time  = '';//保质期
$sync_store = 1;
$sync_shop = 0;
$goods_id = 0;
if($product){
	$product_name = $product['product_name'];
	$big_img = $product['product_img'];
	if(isset($product['img']) && !empty($product['img'])){
		foreach ($product['img'] as $k => $img){
			if($k == 0){
				$pics_first = $img['origin_img'];
				$pics .= $img['origin_img'];
			} else{
				$pics .=' , '.$img['origin_img'];
				$pics_a .= '<a href="'.$img['origin_img'].'" data-lightbox="example-set" data-id="'.$img['image_id'].'"></a>';
			}
			
		}
	}
	$warn_quantity =$product['warn_quantity'];
	$description = $product['description'];
	$product_id = $product['product_id'];
	$price =$product['price'];
	$supply_price = $product['supply_price'];
	$guide_price = $product['guide_price'];
	$guide_price_jin = round($guide_price / 2, 2);
	$barcode = $product['barcode'];
	$produced_time = $product['produced_time'] > 0 ? date('Y-m-d',$product['produced_time']) : '';
	$quality_time  = $product['quality_time'] > 0 ? date('Y-m-d',$product['quality_time']) : '';
	$sync_store = $product['sync_store'];
	$sync_shop = $product['sync_shop'];
	$goods_id = $product['good_id'];
}

//控制成本单价显示
//检查authitem 是否有permission
$price_flag = 0;
$auth = Yii::$app->authManager;
$actions = 'product/check-product-price';
$userId = Yii::$app->user->id;
if($auth->getPermission($actions)){//只判断在auth_item 的权限
	$res = $auth->checkAccess($userId, $actions);
	if(!$res){
		$price_flag = 1;
	}
}
?>
<ul id="myTab" class="nav nav-tabs" style="width: 2500px;">
	<li class="active" id="aa"><a href="#order" data-toggle="tab">基本信息</a>
	</li>
	<li id="bb"><a href="#shipdet" data-toggle="tab">多单位设置</a></li>
	<li id="cc"><a href="#productdet" data-toggle="tab">分拆设置</a></li>
</ul>
<div id="myTabContent" class="tab-content">
	<div class="tab-pane fade in active" id="order">
		<div id="addProduct" class="col-md-12 col-lg-12">
			<a href="javascript:void(0);" id="backUrl">返回上一页</a>
			<form id="productForm" class="form-horizontal" name="editForm">
				<fieldset>
					<div class="form-group">
						<label class="col-md-2 control-label">产品名称：</label>
						<div class="col-md-4">
							<input title="产品名称" name="pname" type="text" id="pname"
								class="form-control" value="<?=$product_name ?>"> <span class="help-block"
								style="color: red;"></span>
						</div>
						<br />
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">条形码：</label>
						<div class="col-md-4">
							<input title="条形码" name="barcode" type="text" id="barcode"
								class="form-control" value="<?=$barcode ?>"> <span class="help-block"></span>
						</div>
						<br/>
					</div>

					<!-- 				<div class="form-group"> 
 					<label class="col-md-2 control-label">货 号：</label>
					<div class="col-md-4">
						<input title="sn" name="sn" type="text" id="sn"
							class="form-control" value="" placeholder="如果您不输入商品货号，系统将自动生成一个唯一的货号">
							<span class="help-block" style="color: red;"></span>
 					</div> 
				</div> -->

					<div class="form-group">
						<label class="col-md-2 control-label">产品类别：</label>
						<div class="col-md-4">
							<select title="产品类别" size="1" class="form-control" name="catid"
								id="catid">
								<option value="0">请选择...</option>
							<?php
							if (isset ( $category )) {
								genSelect ( $category );
							}
							?>
						</select> <span class="help-block" style="color: red;"> </span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">供应商：</label>
						<div class="col-md-4">
							<select title="供应商" size="1" class="form-control" name="supplier"
								id="supplier">
								<option value="0">请选择...</option>
								<?=$sup_select ?>
						</select> <span class="help-block" style="color: red;"> </span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">基本单位：</label>
						<div class="col-md-4">
							<select title="基本单位" size="1" class="form-control" name="unit"
								id="unit">
								<option value="0">请选择...</option>
								<?=$unit_select?>
						</select> <span class="help-block" style="color: red;"></span>
						</div>
					</div>
					<div class="form-group" style="<?php if($price_flag) {echo 'display:none;';} else {echo '';} ?>">
						<label class="col-md-2 control-label">成本单价：</label>
						<div class="col-md-4">
							<input title="成本单价" name="price" type="text" id="price"
								class="form-control" value="<?=$price ?>"> <span class="help-block"
								style="color: red;"></span>
						</div>
						<br />
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">供货单价：</label>
						<div class="col-md-4">
							<input title="供货单价" name="supply_price" type="text" id="supply_price"
								class="form-control" value="<?=$supply_price ?>"> <span class="help-block"
								style="color: red;"></span>
						</div>
						<br />
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">指导价格：</label>
						<div class="col-md-4">
							<span class="guide_price_jin">公斤</span><input title="指导价格" name="guide_price" type="text" id="guide_price" class="form-control" style="display:inline-block;width:40%;" value="<?=$guide_price ?>">
							<span class="guide_price_jin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;斤</span><input title="每斤指导价" name="guide_price_jin" type="text" id="guide_price_jin" class="form-control guide_price_jin" style="display:inline-block;width:40%;" value="<?=$guide_price_jin ?>">
							<span class="help-block" style="color: red;"></span>
						</div>
						<br />
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">仓库</label>
						<div class="col-md-4">
							<select title="仓库" size="1" class="form-control" name="warehouse"
									id="warehouse">
									<?=$ware_select ?>
							</select> <span class="help-block" style="color: red;"> </span>
						</div>
						<br />
					</div>

                   <div class="form-group">
						<label class="col-md-2 control-label">生产日期：</label>
						<div class="col-md-4 ">
							<input style="background:white;"  name="produced_time" type="text" id="produced_time"
								class="form-control" readonly placeholder="生产日期"  value="<?php echo $produced_time;?>">
						</div>
						<br />
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">保质期：</label>
						<div class="col-md-4 ">
							<input style="background:white;"  name="quality_time" type="text" id="quality_time"
								class="form-control" readonly placeholder="保质期"  value="<?php echo $quality_time;?>">
						</div>
						<br />
					</div>

					<div class="form-group" style="display:none;">
						<label class="col-md-2 control-label">产品大图：</label>
						<div class="col-md-4">
							<div class="input-group">
								<input title="产品大图" type="text" id="thumb" name="thumb"
									class="form-control" value="<?=$big_img?>" readonly> <span
									class="input-group-btn">
									<button type="button" id="BigImage" class="btn btn-default">上传图片</button>
								</span> <span class="input-group-btn" style="<?php if($big_img) {echo '';} else{ echo 'display:none;';}  ?> "> <a
									href="<?=$big_img?>" type="button" id="lightBig" class="btn btn-default" data-lightbox="example-1">查看图片</a>
								</span>
							</div>
							<span class="help-block">产品大图主要是做内页显示使用</span>
						</div>
					</div>

					<div class="form-group" style="display:none;">
						<label class="col-md-2 control-label">更多产品图片：</label>
						<div class="col-md-4">
							<textarea title="更多产品图片" rows="5" cols="80" name="pics" id="pics"
								class="form-control"><?=$pics ?></textarea>
							<span class="input-group-btn" style="">
								<button type="button" id="J_selectImage" class="btn btn-default">批量上传</button>
								<a type="button" id="J_image" class="btn btn-default" href="<?=$pics_first ?>"
								style="margin-left: 10px; <?php if($pics_first){echo '';} else{ echo 'display:none;';} ?>" data-lightbox="example-set">查看</a>
							</span> <span class="help-block">上传多张案例图片</span>
							<div style="display: none;">
								<?= $pics_a ?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-group">
							<label class="col-md-2 control-label">商品预警库存：</label>
							<div class="col-md-4">
								<input type='text' title="商品预警库存" rows="5" cols="80"
									name="warnquan" id="warnquan" class="form-control"
									placeholder="0" value="<?=$warn_quantity ?>" /> <span class="help-block"
									style="color: red;"></span>
							</div>
						</div>
					</div>
					<div class="form-group" style="display:none;">
						<label class="col-md-2 control-label">产品详细介绍：</label>
						<div class="col-md-6">
							<textarea title="产品详细介绍" name="content" id="description"
								style="width: 95%; height: 400px; visibility: hidden;"
								class="form-control"><?= $description?></textarea>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">成本核算方法：</label>
						<div class="col-md-4 ">
							<select id="account_method" class="form-control" disabled>
								<option value="1">移动加权平均法</option>
							</select>
						</div>
						<br />
					</div>
					<div class="form-group">
						<div class="form-group">
							<label class="col-md-2 control-label">是否同步：</label>
							<div class="col-md-5">
								<label style="width:100px"><input type="checkbox" style="width:20px" id="sync_store" name="sync_store"<?=$sync_store ? ' checked="checked"' : '';?>/>同步到门店</label>&nbsp;&nbsp;&nbsp;
								<label style="width:100px"><input type="checkbox" style="width:20px" id="sync_shop" name="sync_shop"<?=$sync_shop ? ' checked="checked"' : '';?>/>同步到商城</label>&nbsp;&nbsp;&nbsp;
								<?=$goods_id ? '<a href="http://www.yiliving.com/a/goods.php?act=edit&goods_id='.$goods_id.'" target="_blank">商城商品</a>' : '';?>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-2 control-label"></div>
						<div class="col-md-10">
							<!-- <button type="button" id="save" class="btn btn-danger btn-lg">保存产品信息</button> -->
							<span class="btn btn-danger btn-lg" id="saveProduct" data-id="<?=$product_id?>"
								type="">确定</span>
							<button type="button" class="btn btn-danger btn-lg" value="返回上一页"
								onClick="history.back(-1)">返回上一页</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
	<div class="tab-pane fade" id="shipdet">
		<div class="col-md-12 col-lg-12 col-sm-12" style="padding-left: 12px;" id="unit_detail">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<col width="20px" />
			<col width="100px" />
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">单位类别</th>
					<th class="text-center">单位</th>
					<th class="text-center">换算比率</th>
					<th class="text-center">规格</th>
					<th class="text-center">备注</th>
				</tr>
			</thead>
			<tbody id="productUnitList">
				<tr>
					<td class="text-center" name="ids" data-id="">1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr"> <i class="fa fa-minus-circle fa-lg" style="color: red;margin-top:10px;"></i>
					</a></td>
					<td class="text-center" name="unit_type" data-id="0">基本单位</td>
					<td class="text-center" name="name" data-id="">
			<!-- 		<input title="商品名称" type="text" class="product-name stock-input50" disabled="" style="height: 30px;width:80%"> -->
					<select title="单位" class="form-control unit stock-input50" style="height: 30px;width:80%;float:left;padding:1px;">
							<option value="0">单位</option>
							<option value="6">个</option>
							<option value="5">支</option>
							<option value="4">包</option>
							<option value="2">斤</option>
							<option value="1">箱</option>
					</select>
					<a class="add-btn add-unit" title="新增" style="float:left;margin:8px 10px 10px 10px;"
						href="javascript:void(0)"><i class="fa fa-plus-circle fa-lg"></i></a></td>
					<td class="text-center" name="rate"><input title="换算比率" type="text" 
						class="rate stock-input"></td>
					<td class="text-center" name="format"><input title="数量" type="text" 
						class="format stock-input"></td>
					<td class="text-center" name="remark"><input title="备注" type="text" 
						class="remark stock-input" style="width: 100%;"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<!-- <button class="btn btn-primary btn-sm pull-right" id="confirm" style="margin-left: 10px;display:none">确认</button>-->
		<button class="btn btn-success btn-sm pull-right" id="save"  data-id="">保存</button>
	</div>
	</div>
	<div class="tab-pane fade" id="productdet">
		<div class="col-md-12 col-lg-12 col-sm-12" style="padding-left: 120px;" id="goods_detail">
		
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<col width="20px" />
			<col width="80px" />
			<col width="100px" />
			<col width="20%" />
			<col width="30%" />
			<thead>
				<tr role="row">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">商品类型</th>
					<th class="text-center">商品id</th>
					<th class="text-center">商品名称</th>
<!-- 					<th class="text-center">备注</th> -->
				</tr>
			</thead>
			<tbody id="goodsList">
				<tr>
					<td class="text-center" name="ids" data-id="">1</td>
					<td class="text-center"><a href="javascript:void(0)"
						class="stock-minus-tr" data-id="<?=$product_id ?>"> <i class="fa fa-minus-circle fa-lg" style="color: red;margin-top:10px;"></i>
					</a></td>
					<td class="text-center" name="goods_type" data-id="0">原始商品</td>
					<td class="text-center" name="goods_id">
						<input title="商品ID" type="text" class="product-id stock-input50" disabled="" value="<?= $product_id ?>" style="height: 30px;width:70%;float:left;">
						<a class="add-btn add-goods" title="新增" style="float:left;margin:8px 10px 10px 10px;"
						href="javascript:void(0)"><i class="fa fa-plus-circle fa-lg"></i></a> 
					</td>
					<td class="text-center" name="name" data-id="">
						<input title="商品名称" type="text" class="product-name stock-input50" disabled="" value="<?= $product_name ?>" style="height: 30px;width:100%;float:left;"> 					
					</td>
<!-- 					<td class="text-center" name="remark"><input title="备注" type="text"  
						class="remark stock-input" style="width: 100%;height:30px;"></td>-->
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<button class="btn btn-success btn-sm pull-right" id="extend-save"  data-id="">保存</button>
	</div>
	</div>
	<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;height:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12 col-sm-12 col-lg-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-4 col-sm-4 col-lg-4  input-group">
							<input class="form-control name" placeholder="商品名称 / 商品ID " type="text" id="productName" onkeyup="if(event.keyCode == 13) $('#searchProduct').click()" />
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
	
</div>