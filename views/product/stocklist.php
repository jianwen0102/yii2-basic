<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/product/stockpile_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '商品库存信息';
// 	$this->params['breadcrumbs'][] = ['label' => '采购单列表', 'url' => ['list-procurement']];
	$this->params['breadcrumbs'][] = $this->title;
	
	//类目形成树结构
	function genSelect($info, $spe = '│')
	{
		if (! empty ( $info )) {
			foreach ( $info as $item ) {
				if (isset ( $item ['value'] ['id'] )) {
					echo '<option value="' . $item ['value'] ['id'] . '"';
	
					if (isset ( $item ['selected'] ) && $item ['selected'] == TRUE) {
						echo ' selected ';
					}
	
					echo '>' . $spe . $item ['value'] ['name'] . '</option>';
				}
					
				if (! empty ( $item ['list'] )) {
					genSelect ( $item ['list'], $spe . '├' );
				}
			}
		}
	}
?>
<div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="productname"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="category" >
						<option value="0">请选择商品类目..</option>
						<?php
							if (isset ( $category )) {
								genSelect ( $category );
							}
						?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
					<button class="btn btn-info" type="button" id="export_product" style="margin-left: 100px;">导出</button>
				</tr>
			</table>
			</div>
			<div class="search input-group pull-right">
					<span class="input-group-addon input-former">过滤</span> 
					<select class="form-control" id="filterProduct">
						<option value="0">显示所有商品</option>
						<option value="1">显示库存为0的商品</option>
						<option value="2">不显示库存为0的商品</option>
					</select>
				</div>
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr>
						<th class="text-center" colspan="5">商品信息</th>
						<th class="text-center" colspan="2">期初库存</th>
						<th class="text-center" colspan="3">当前库存</th>
					</tr>
					<tr>
						<th class="text-center">序号</th>
 						<th class="text-center" name="pid">商品编号</th>
 						<th class="text-center" name="name">商品名称</th>
 						<th class="text-center" name="sn">商品货码</th>
 						<th class="text-center" name="category">类目</th>
 						<th class="text-center" name="gen_qty">期初数量</th>
 						<th class="text-center" name="gen_amount">期初金额</th>
 						<th class="text-center" name="price">成本单价</th>
 						<th class="text-center" name="quantity">数量</th>
 						<th class="text-center" name="unit">单位</th>
 						
<!-- 						<th class="text-center" name="remark">备注</th> -->
					</tr>
				</thead>
				<tbody id="stockpilelist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
<!-- 							<button class="btn btn-danger btn-sm" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="10"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>