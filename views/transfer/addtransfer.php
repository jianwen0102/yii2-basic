<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/transfer/transfer_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新增调拨单';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create_transfer">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
// 				'supplier' => $supplier,
				'NO' => $NO,
				'damage' => $damage,
				'type' => $type,
			
		])?>
</div>
