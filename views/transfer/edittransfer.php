<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/transfer/transfer_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '编辑调拨单';
	$this->params['breadcrumbs'][] = ['label' => '调拨单列表', 'url' => ['list-transfer']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_transfer">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $NO,
				'damage' => $damage,
				'type' => $type,
		])?>
</div>
