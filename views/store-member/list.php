<?php

$this->registerJsFile('@web/statics/js/store-member/store-member_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '会员列表';
$this->params['breadcrumbs'][] = $this->title;

?>
<div id="listmember">
	<div class="col-md-11 col-lg-11" style="margin-bottom:10px;">
		<form class="form-search form-inline">
				<input  class="form-control" type="text" placeholder="会员名称" id="sname">

				<select class="form-control" style="margin:0 10px" id="storeID">
					<option value="0">全部门店</option>
					<?php foreach($store as $val):?>
						 <option value="<?=$val['store_id']?>"><?=$val['store_name']?></option>
					<?php endforeach;?>
				</select>
				<button class="btn btn-info" type="button" onclick="javascript:memberData.searchMemberList();">查找</button>
				<div class="search input-group pull-right" style="display:none;">
					<span class="input-group-addon input-former">状态</span> 
					<select class="" style="height:35px;border: 1px solid #bdc3d1;" id="filterClient" onchange="javascript:memberData.changeFilter();">
						<option value="-1">全部</option>
						<option value="0">正常</option>
						<option value="1">屏蔽</option>
					</select>
				</div>
		</form>
	</div>
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center">全选 <input type="checkbox" class="checkAll" onclick="javascript:memberData.selectAll();"></th>
					<th class="text-center">会员昵称</th>
					<th class="text-center">所属门店</th>
					<th class="text-center">会员卡号</th>
					<th class="text-center">手机号</th>
					<th class="text-center">余额</th>
					<th class="text-center">消费积分</th>
					<th class="text-center">等级积分</th>
					<th class="text-center">添加时间</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="store_member">			

			</tbody>	
			<tfoot>
			
				<td id="" colspan="2" style="text-align: left;">
					<a onclick="javascript:memberData.addMemberList();" class="btn btn-primary btn-sm" id="add" href="javascript:;">新增</a>
					<button style="display:none;" onclick="javascript:;" class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm" style="text-align:right;"></td>
			</tfoot>
		</table>
	</div>

	<!-- 修改模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">会员修改</h4>
				</div>
				<div class="modal-body">
					<table class="table table-border-null">
						<tbody>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b style="color:red;">* </b>会员昵称</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="nickname" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">									   
	                                    <span class="input-group-addon input-former"><b style="color:red;">* </b>所属门店</span>
										<select class="form-control" id="store_id">
										</select>
									</div>
								</td>
							</tr>	
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b style="color:red;">* </b>会员卡号</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="cardno" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">
										<span class="input-group-addon input-former"><b style="color:red;">* </b>手&nbsp;&nbsp;机&nbsp;&nbsp;号</span>
										<input type="text" class="form-control"
											aria-describedby="basic-addon1" id="mobile" value="">
									</div>
								</td>
							</tr>
							<tr>
								<td class="">
									<div class="input-group">									   
	                                    <span class="input-group-addon input-former"><b style="color:red;">* </b> 性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别</span>
										<select class="form-control" id="gender">
												<option value="0">未知</option>
												<option value="1">男</option>
												<option value="2">女</option>
										</select>
									</div>
								</td>
							</tr>	
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭
					</button>
					<button type="button" class="btn btn-primary" id="store_save"  data-id="" onclick="javascript:memberData.saveMember(this);">提交更改</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>