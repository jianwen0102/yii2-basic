<?php
$this->registerCssFile('@web/statics/bootstrap-table/css/bootstrap-table.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/bootstrap-table/js/bootstrap-table.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/bootstrap-table/js/bootstrap-table-export.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/bootstrap-table/js/jquery.base64.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/bootstrap-table/js/tableExport.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/transaction/stocksummary_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '出入库汇总';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid class="" id="box">
	<div id="content-body">
		<div id="reportTableDiv" class="span10">
			<table id="reportTable">
			</table>
		</div>
	</div>
</div>