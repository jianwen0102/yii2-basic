<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/transaction/transactionlist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '出入库明细';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12 col-md-12 col-sm-12 col-lg-12">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="name"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="vendor" >
						<option value="0">请选择供应商..</option>
						<?php foreach ($vendor as $ve):?>
							<option value="<?= $ve['supplier_id']?>"><?=$ve['supplier_name']?></option>
						<?php endforeach;?>
					</select>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="customer" >
						<option value="0">请选择客户..</option>
						<?php foreach ($cust as $cut):?>
							<option value="<?= $cut['client_id']?>"><?=$cut['client_name']?></option>
						<?php endforeach;?>
					</select>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="warehouse">
						<option value="0">请选择仓库..</option>
						<?php foreach ($warehouse as $st):?>
							<option value="<?= $st['warehouse_id']?>"><?=$st['warehouse_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
				</tr>
			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>-->
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr>
						<th class="text-center">序号</th>
						<th class="text-center" name="origin_id">单号</th>
						<th class="text-center" name="origin_type">单据类型</th>
						<th class="text-center" name="flag">状态</th>
 						<th class="text-center" name="date">日期</th>
 						<th class="text-center" name="pid">商品编号</th>
 						<th class="text-center" name="name">商品名称</th>
 						<th class="text-center" name="warehouse">仓库</th>
 						<th class="text-center" name="vendor">供应商</th>
 						<th class="text-center" name="in_quantity">入库数量</th>
 						<th class="text-center" name="out_quantity">出库数量</th>
						<th class="text-center" name="minus">数量差值</th>
<!-- 						<th class="text-center" name="remark">备注</th> -->
					</tr>
				</thead>
				<tbody id="transactionlist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
<!-- 							<button class="btn btn-danger btn-sm" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="10"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>