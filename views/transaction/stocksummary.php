<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/transaction/stocksummary_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '出入库汇总统计';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12 col-md-12 col-sm-12 col-lg-12">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="name"/>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
				</tr>
			</table>
			</div>
			</form>
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr>
						<th class="text-center">序号</th>
 						<th class="text-center" name="pid">商品编号</th>
 						<th class="text-center" name="name">商品名称</th>
 						<th class="text-center" name="init_num">期初数量</th>
 						<th class="text-center" name="in_quantity">入库数量</th>
 						<th class="text-center" name="out_quantity">出库数量</th>
 						<th class="text-center" name="taste">试吃</th>
 						<th class="text-center" name="destory">损耗</th>
 						<th class="text-center" name="bad_rate">损耗率</th>
 						<th class="text-center" name="product_qty">现库存数量</th>
						
<!-- 						<th class="text-center" name="remark">备注</th> -->
					</tr>
				</thead>
				<tbody id="stocksummarylist">
				</tbody>
				<tfoot>
					<tr>
						<td id="" colspan="2" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
<!-- 							<button class="btn btn-danger btn-sm" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="10"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>