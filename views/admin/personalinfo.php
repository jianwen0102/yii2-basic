<?php 
$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/personalInfo/personalinfo_biz.js',['depends'=>['app\assets\AppAsset']]); 
$this->title = '个人信息';
$this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['list-supplier']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="" class="col-md-12 col-lg-12 col-md-offset-2">
	<form id="personalInfo" class="col-md-5">
		<div class="form-group">
			<table class="table table-condensed table-border-null" id="">
				<tbody>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>用户名</span> <input
									type="text" class="form-control" value="<?=$info->username?>" data-id="<?=$info->id?>" id="adminname" name="adminname">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3" colspan="1">
							<div class="input-group" style="">
								<span class="input-group-addon input-former"><b style="color:red;">*</b>email</span>
								<input type="text" class="form-control" id="email" value="<?= $info->email?>" name="email">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">原密码</span> 
								<input type="password" class="form-control" id="oldpassword" name="oldpassword">
							</div>
							<div class="input-group">
								<label class="col-md-12 col-md-offset-2" style="margin-bottom: 0px;">如果要修改密码,请先填写旧密码,如留空,密码保持不变</label>
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">新密码</span> 
								<input type="password" class="form-control" id="newpassword" name="newpassword">
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-3">
							<div class="input-group col-md-12">
								<span class="input-group-addon input-former">确认密码</span> 
								<input type="password" class="form-control" id="repassword" name="repassword">
							</div>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<div>
						<td  colspan="2" style="text-align:center;">
<!-- 							<button  type="button" class="btn btn-success" id="confirm_info">确定</button> -->
							<input class="submit btn btn-success" type="submit" value="更改" id="confirm_info">
							<button  type="button" class="btn btn-success" id="reset_info">重置</button>
						</td>
					</div>
				</tfoot>
			</table>
		</div>
	</form>
</div>