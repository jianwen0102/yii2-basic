<?php
$this->registerJsFile('@web/statics/js/jquery-validation-1.14.0/jquery.validate.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/personalInfo/userlist_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '管理员列表';
// $this->params['breadcrumbs'][] = ['label' => '系统设置', 'url' => ['list-supplier']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="listuser">
	<div class="col-md-11 col-lg-11" style="padding-left: 0px;">
		<table class="table table-hover table-datatables table-bordered">
			<thead>
				<tr>
					<th class="text-center"><input type="checkbox" class="checkAll"></th>
					<th class="text-center">用户名</th>
					<th class="text-center">email</th>
					<th class="text-center">加入时间</th>
					<th class="text-center">上次登录时间</th>
					<th class="text-center">操作</th>
				</tr>
			</thead>
			<tbody id="user_list">
			</tbody>
			<tfoot>

				<td id="" colspan="2" style="text-align: left;">
					<button type="button" class="btn btn-primary btn-sm" id="add">新增</button>
					<button type="button" class="btn btn-danger btn-sm" id="delete">删除</button>
				</td>
				<td class="paginationNavBar" colspan="4" data-ptype="pagination-sm"
					style="text-align: right;"></td>
			</tfoot>
		</table>
	</div>
	<!-- 模态框（Modal） -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">用户新增</h4>
				</div>
				<div class="modal-body">
					<form id="userinfo">
						<div class="form-group">
							<table class="table table-border-null">
								<tbody>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former"><b
													style="color: red;">*</b>用户名</span> <input type="text"
													class="form-control" aria-describedby="basic-addon1"
													id="adminname" name="adminname" value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">email&#12288;</span>
												<input type="text" class="form-control"
													aria-describedby="basic-addon1" id="email" name="email"
													value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">密
													&#12288;&nbsp;码</span> <input type="password"
													class="form-control" aria-describedby="basic-addon1"
													id="password" name="password" value="">
											</div>
										</td>
									</tr>
									<tr>
										<td class="">
											<div class="input-group">
												<span class="input-group-addon input-former">确认密码</span> <input
													type="password" class="form-control"
													aria-describedby="basic-addon1" id="repassword"
													name="repassword" value="">
											</div>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<div>
										<td colspan="2" style="text-align: center;">
											<!-- 									<button type="button" class="btn btn-success" id="user_save">确定</button> -->
											<input class="submit btn btn-success" type="submit"
											value="确定" id="user_save">
											<button type="button" class="btn btn-success" id="reset_info"
												data-dismiss="modal">重置</button>
										</td>
									</div>
								</tfoot>
							</table>
						</div>
					</form>
				</div>
				<!-- 				<div class="modal-footer"> -->
				<!-- 	<div class="pull-left">
<!-- 						<span>默认:</span> <input type="checkbox" id="default"> -->
				<!-- 					</div> -->
				<!-- 					<button type="button" class="btn btn-default" data-dismiss="modal">关闭 -->
				<!-- 					</button> -->
				<!-- 					<button type="button" class="btn btn-primary" id="user_save" -->
				<!-- 						data-uid="" data-old="">提交更改</button> -->
				<!-- 				</div> -->
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>


	<!-- 模态框（Modal） -->
	<div class="modal fade" id="assignModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="">权限设置</h4>
				</div>
				<div class="modal-body" id="assign_body">
					<!-- content -->
				</div>
				<div class="modal-footer pull-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="assign_save" data-uid="">提交更改</button>
			</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>
</div>