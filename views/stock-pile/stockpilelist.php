<?php
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/stock-pile/stockpilelist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '分仓库存查询';
// 	$this->params['breadcrumbs'][] = ['label' => '采购单列表', 'url' => ['list-procurement']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group ">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="pname"/>
					<button class="btn btn-info" type="button" id="search_list">查找</button>
				</tr>
			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>-->
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
			<div style="max-width:2800px; max-height:900px; overflow:scroll;">
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr id="hhead">
 						<th class="text-center" name="pid" colspan="5">商品编号</th>
 						<?php foreach ($warehouse as $ware):?>
 							<th class="text-center" name="" colspan="2" data-id="<?= $ware['warehouse_id']?>"><?=$ware['warehouse_name']?></th>
 						<?php endforeach;?>
 						<th class="text-center" name="remark" colspan="2">total</th>
						
					</tr>
					<tr id="hdet">
						<th class="text-center">序号</th>
						<th class="text-center" name="pid">商品编号</th>
 						<th class="text-center" name="name">商品名称</th>
 						<th class="text-center" name="unit">单位</th>
 						<th class="text-center" name="price">单价</th>
 						<?php for($i=0; $i < count($warehouse)+1; $i++):?>
	 						<th class="text-center" name="">数量</th>
	 						<th class="text-center" name="">金额</th>
 						<?php endfor;?>
					</tr>
				</thead>
				<tbody id="stocklist">
				</tbody>
				<tfoot id="stockFoot">
					<tr>
						<td id="" colspan="1" style="text-align: left;">
<!-- 							<button class="btn btn-primary btn-sm" id="add">新增</button> -->
<!-- 							<button class="btn btn-danger btn-sm" id="delete">删除</button> -->
						</td>
						<td class="paginationNavBar" colspan="100"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
			</div>
		</div>
	</div>
</div>