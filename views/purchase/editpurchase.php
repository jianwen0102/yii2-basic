<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/purchase/addpurchase_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '采购入库编辑';
	$this->params['breadcrumbs'][] = ['label' => '采购入库列表', 'url' => ['list-purchase']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_purchase">
	<?= $this->render('_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>