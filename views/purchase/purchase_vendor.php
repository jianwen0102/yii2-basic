<?php
$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
$this->registerJsFile('@web/statics/js/purchase/purven_biz.js',['depends'=>['app\assets\AppAsset']]);
$this->title = '采购统计';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="purchase_vendor">
	<div class="col-md-12 col-lg-12 col-sm-12 search_query" style="padding-left: 12px;">
			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime" value="<?= date('Y-m-d',time());?>"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime" value="<?= date('Y-m-d',time());?>"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品编号" style="margin-right:10px;" id="id"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="name"/>
					<select class="input-medium search-query form-control" style="margin-right:10px;" id="vendor" >
						<option value="0">请选择供应商..</option>
						<?php foreach ($vendor as $ve):?>
							<option value="<?= $ve['supplier_id']?>"><?=$ve['supplier_name']?></option>
						<?php endforeach;?>
					</select>
					<button class="btn btn-info" type="button" id="search_list">查询</button>

				</tr>

			</table>
			</div>
<!-- 			<div class="search input-group pull-right"> -->
<!-- 					<span class="input-group-addon input-former">过滤</span>  -->
<!-- 					<select class="form-control" id="filterInstore"> -->
<!-- 						<option value="0">显示所有入库单</option> -->
<!-- 						<option value="1">只显示未确认的入库单</option> -->
<!-- 						<option value="2">只显示已确认的入库单</option> -->
			<!-- 			<option value="3">不显示库存为负的材料</option>
<!-- 						<option value="4">显示库存为负的材料</option> -->
<!-- 					</select> -->
<!-- 				</div> -->
			</form>
	</div>
	<div class="col-md-12 col-lg-12 col-sm-12" style="padding-left: 12px;" id="">
		<table id="datatable"
			class="table table-striped table-bordered bootstrap-datatable"
			cellspacing="0" width="100%">
			<thead>
				<tr role="row">
					<th class="text-center">序号</th>
					<th class="text-center">日期</th>
					<th class="text-center">供应商</th>
					<th class="text-center">产品</th>
					<th class="text-center">单价</th>
					<th class="text-center">单位</th>
					<th class="text-center">总数量</th>
					<th class="text-center">应付总金额</th>
					<th class="text-center">已付数量</th>
					<th class="text-center">已付总金额</th>
					<th class="text-center">付款日期</th>
					<th class="text-center">未付款金额</th>
				</tr>
			</thead>
			<tbody id="purVenList">
				<tr>
					<td colspan="12" class="text-center">没有数据</td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
				<td class="paginationNavBar" colspan="12" data-ptype="pagination-sm" style="text-align: right;"></td>
			  </tr>
		   </tfoot>
		</table>
	</div>
	<div class="col-md-12 col-sm-12 col-lg-12">
		<button class="btn btn-primary btn-sm pull-right" id="confirm" style="margin-left: 10px;display:none">确认</button>
		<button class="btn btn-success btn-sm pull-right" id="export"  data-id="" data-origin="">导出</button>
	</div>
</div>
