<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/js/inventory/inventorylist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/reports/grossprofitdetail_biz.js',['depends' => ['app\assets\AppAsset']]);
	
	$this->title = '销售毛利明细表';
	$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline">
 			<div class="form-group pull-right">
 			<table class="table table-condensed">
 				<tr>
					<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品编号" style="margin-right:10px;" id="id"/>
					<input 	class="input-medium search-query form-control" type="text" placeholder="商品名称" style="margin-right:10px;" id="name"/>
					<!-- <select class="input-medium search-query form-control" style="margin-right:10px;" id="stock" >
						<option value="0">请选择仓库..</option>
						<?php foreach ($stock as $st):?>
							<option value="<?= $st['warehouse_id']?>"><?=$st['warehouse_name']?></option>
						<?php endforeach;?>
					</select> -->
					<button class="btn btn-info" type="button" id="search_list">查找</button>
		<!-- 			<button class="btn btn-primary" type="button" id="explode_search" style="margin-left:50px;">导出</button> -->
				</tr>

			</table>
			</div>
			</form>
			
			
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
				cellspacing="0">
				<thead>
					<tr>
						<th class="text-center">序号</th>
						<th class="text-center" name="order_time">单据日期</th>
						<th class="text-center" name="order_id">单据编号</th>
						<th class="text-center" name="order_type">单据类型</th>
						<th class="text-center" name="customer">客户</th>
						<th class="text-center" name="warehouse">仓库</th>
						<th class="text-center" name="goods_id">商品编号</th>
						<th class="text-center" name="goods_name">商品名称</th>
						<th class="text-center" name="unit">单位</th>
						<th class="text-center" name="quantity">数量</th>
						<th class="text-center" name="price">销售单价</th>
						<th class="text-center" name="amount">销售金额</th>
						<th class="text-center" name="base_price">成本单价</th>
						<th class="text-center" name="base_amount">成本金额</th>
						<th class="text-center" name="sale_profit">销售毛利</th>
						<th class="text-center" name="gross_profit_rate">毛利率(%)</th>
<!-- 						<th class="text-center" name="remark">备注</th> -->
					</tr>
				</thead>
				<tbody id="grossProfitlist">
				</tbody>
				<tfoot>
					<tr>
						<td class="paginationNavBar" colspan="18"
							data-ptype="pagination-sm" style="text-align: right;"></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	
</div>