<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
// 	$this->registerJsFile('@web/statics/js/inventory/inventorylist_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/reports/specialgrossprofit_biz.js',['depends' => ['app\assets\AppAsset']]);
	
	$this->title = '特定商品销售毛利汇总表';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div id="special_gross">
 <div class="container-fluid class="" id="box">
	<div class="row-fluid" id="top">
		<div class="span12">
 			<form class="form-search form-inline pull-right" style="margin-right: 10px; margin-bottom:5px;">
				<input class="input-medium search-query form-control" type="text" placeholder="开始时间" style="margin-right:10px;" id="starTime" value="<?= date('Y-m-d',time())?>"/>
				<input 	class="input-medium search-query form-control" type="text" placeholder="截至时间" style="margin-right:10px;" id="endTime" value="<?= date('Y-m-d',time())?>"/>
				<select class="input-medium search-query form-control" style="margin-right:10px;" id="special_goods" >
					<option value="0">请选择商品..</option>
					<?php foreach ($goods as $gd):?>
						<option value="<?= $gd['product_id']?>"><?=$gd['product_name']?></option>
					<?php endforeach;?>
				</select> 
				<button class="btn btn-info" type="button" id="search_list">查找</button>
			</form>
			<table class="table table-striped table-hover table-bordered" cellpadding="0"
					cellspacing="0">
					<thead>
						<tr>
							<th class="text-center" rowspan="2">序号</th>
							<th colspan="2" class="text-center">商品信息</th>
							<th colspan="3"  class="text-center">入库信息</th>
							<th colspan="3"  class="text-center">销售信息</th>
							<th colspan="3" class="text-center">报损信息</th>
							<th colspan="2" class="text-center">剩余信息</th>
							
							<th class="text-center" name="sale_profit" rowspan="2">销售毛利</th>
							<th class="text-center" name="gross_profit_rate"  rowspan="2">毛利率(%)</th>
							
						</tr>
						<tr>
							
							<th class="text-center" name="goods_id">商品编号</th>
							<th class="text-center" name="goods_name">商品名称</th>
							<th class="text-center" name="in_quantity">入库数量</th>
							<th class="text-center" name="in_unit">单位</th>
							<th class="text-center" name="base_amount">成本金额</th>
							<th class="text-center" name="sale_quantity">销售总数量</th>
							<th class="text-center" name="sale_unit">单位</th>
							<th class="text-center" name="sale_amount">销售总金额</th>
							<th class="text-center" name="lost_quantity">报损数量</th>
							<th class="text-center" name="lost_unit">单位</th>
							<th class="text-center" name="lost_amount">报损总金额</th>
							<th class="text-center" name="left_quantity">剩余数量</th>
							<th class="text-center" name="left_unit">单位</th>
<!-- 							<th class="text-center" name="left_amount">剩余总金额</th> -->
							
	<!-- 						<th class="text-center" name="sale_profit">销售毛利</th> -->
	<!-- 						<th class="text-center" name="gross_profit_rate">毛利率(%)</th> -->
						</tr>
					</thead>
					<tbody id="grossProfitlist">
					<tr><td colspan="16" class="text-center">...请选择商品查询...</td></tr>
					</tbody>
					<tfoot>
						<tr>
							<td class="paginationNavBar" colspan="12"
								data-ptype="pagination-sm" style="text-align: right;"></td>
						</tr>
					</tfoot>
				</table>
		</div>
			
			<div id="bottom" class="row-fluid"> 
				<table class="table table-striped table-hover table-bordered" cellpadding="0"
					cellspacing="0">
					<thead>
						<tr>
							<th class="text-center" rowspan="2">序号</th>
							<th colspan="2" class="text-center">商品信息</th>
<!-- 							<th colspan="3"  class="text-center">入库信息</th> -->
							<th colspan="3"  class="text-center">销售信息</th>
<!-- 							<th class="text-center" name="sale_profit" rowspan="2">销售毛利</th> -->
<!-- 							<th class="text-center" name="gross_profit_rate"  rowspan="2">毛利率(%)</th> -->
							
						</tr>
						<tr>
							
							<th class="text-center" name="goods_id">商品编号</th>
							<th class="text-center" name="goods_name">商品名称</th>
<!-- 							<th class="text-center" name="in_quantity">入库数量</th> -->
<!-- 							<th class="text-center" name="in_unit">入库单位</th> -->
<!-- 							<th class="text-center" name="base_amount">成本金额</th> -->
							<th class="text-center" name="sale_quantity">销售数量</th>
							<th class="text-center" name="sale_unit">销售单位</th>
							<th class="text-center" name="amount">销售金额</th>
	<!-- 						<th class="text-center" name="sale_profit">销售毛利</th> -->
	<!-- 						<th class="text-center" name="gross_profit_rate">毛利率(%)</th> -->
						</tr>
					</thead>
					<tbody id="salelist">
					<tr><td colspan="8" class="text-center">...请选择商品查询...</td></tr>
					</tbody>
					<tfoot>
						<tr>
							<td class="paginationNavBar" colspan="12"
								data-ptype="pagination-sm" style="text-align: right;"></td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div id="line"></div>
		</div>
	</div>
	
<!-- </div>
</div> -->
<?php $this->beginBlock('test') ?>  
window.onload = function() {
	var oBox = $("#box")[0], oBottom = $("#bottom")[0], oLine = $("#line")[0];
	oLine.onmousedown = function(e) {
		var disY = (e || event).clientY;
		oLine.top = oLine.offsetTop;
		document.onmousemove = function(e) {
			var iT = oLine.top + ((e || event).clientY - disY);
			var maxT = oBox.clientHeight - oLine.offsetHeight;
			oLine.style.margin = 0;
			iT < 0 && (iT = 0);
			iT > maxT && (iT = maxT);
			oLine.style.top = oBottom.style.top = iT + "px";
			return false
		};	
		document.onmouseup = function() {
			document.onmousemove = null;
			document.onmouseup = null;	
			oLine.releaseCapture && oLine.releaseCapture()
		};
		oLine.setCapture && oLine.setCapture();
		return false
	};
};
<!-- </script> -->
<?php $this->endBlock() ?>  
<?php $this->registerJs($this->blocks['test'], \yii\web\View::POS_END); ?>  