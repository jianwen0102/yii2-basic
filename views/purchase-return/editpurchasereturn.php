<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/purchase-return/addpurchasereturn_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '采购退货编辑';
	$this->params['breadcrumbs'][] = ['label' => '采购退货列表', 'url' => ['list-purchase-return']];
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="edit_purchase_return">
	<?= $this->render('return_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>