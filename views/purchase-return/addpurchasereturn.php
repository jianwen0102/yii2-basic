<?php 
	$this->registerCssFile('@web/statics/css/bootstrap-datetimepicker.min.css',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/bootstrap-datetimepicker/bootstrap-datetimepicker.zh-CN.js',['depends'=>['app\assets\AppAsset']]);
	$this->registerJsFile('@web/statics/js/purchase-return/addpurchasereturn_biz.js',['depends'=>['app\assets\AppAsset']]);
	$this->title = '新建采购退货';
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="create_purchase_return">
	<?= $this->render('return_form',[ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $NO 
		])?>
</div>