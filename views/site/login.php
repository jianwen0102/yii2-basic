<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '登录';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sign-overlay"></div>
<div class="signpanel"></div>

<div class="panel signin">
    <div class="panel-heading">
        <h4 class="panel-title">欢迎使用供应链系统</h4>
    </div>
    <div class="panel-body">
      <!--<button class="btn btn-primary btn-quirk btn-fb btn-block">联系我们</button>
      <div class="or">or</div>-->
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username',[
                    'inputOptions'=>[
                        'placeholder'=>'请输入账户',
                    ],
                    'inputTemplate'=>
                        '<div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>{input}
                        </div>',
                ])->textInput(['autofocus' => true])->label(false) ?>

                <?= $form->field($model, 'password',[
                    'inputOptions'=>[
                        'placeholder'=>'请输入密码',
                    ],
                    'inputTemplate'=>
                        '<div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>{input}
                        </div>',
                ])->passwordInput()->label(false) ?>
				
				<?= $form->field($model,'verifyCode')->widget(yii\captcha\Captcha::className(),[
//                                         'captchaAction'=>'site/captcha', //默认可以不用填
//                                           'template'=>'{input}{image}',
										  'template'=>  '<div class="input-group" id="captcha">
					                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>{input}{image}
					                        </div>',
                                          'options'=>[
                                        	  'class'=>'input verifycode form-control ',
                                          	  'placeholder'=>'请输入验证码',
                                        	  'id'=>'verifyCode',
                                        	],'imageOptions'=>[
	                                        	  'class'=>'imagecode',
	                                        	  'id'=>'verifyCode-image',
                                        		  'alt'=>"点击更换图片",
                                        		  'title'=>'点击换图', 
                                        		  'style'=>'cursor:pointer'
                                        	],
                 ])->label(false);?>
                                        
				<?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton('登录', ['class' => 'btn btn-primary btn-success btn-quirk btn-block', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
            <hr class="invisible">
    </div>
</div><!-- panel -->


