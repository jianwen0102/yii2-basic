<?php
 namespace app\wap\models;
 
 use app\models\BaseModel;
 use app\enum\EnumOther;
 use Yii;
use app\dao\ProductDAO;
use app\dao\ProductUnitDAO;
use app\dao\ProcurementDAO;
use app\enum\EnumOriginType;
use app\dao\ProcurementDetailDAO;
					 
 class WapProcurementModel extends BaseModel
 {
  	/**
 	 * @desc覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-07-18
 	 * @return WapProcurementModel
 	 */
 	public static function model($className = __CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	/**
 	 * @desc 获取产品列表
 	 * @author liaojianwen
 	 * @date 2017-07-18
 	 */
 	public function getProductList()
 	{
 		$fields ="product_id,product_name";
 		$conditions ="delete_flag = :flag";
 		$params =[
 			':flag'=>EnumOther::NO_DELETE,
 		];
 		$result = ProductDAO::getInstance()->iselect($fields, $conditions, $params);
 		if(!$result){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,  '', 'no data found ');
 		}

 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 获取商品单位
 	 * @author liaojianwen
 	 * @date 2017-07-19
 	 */
 	public function getProUnit($pid)
 	{
 		if(!$pid){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','product_id can not be null');
 		}
 		
 		$fields = "product_unit_id,P.unit,u.unit_name";
 		$conditions = "p.delete_flag= :flag and product_id =:pid";
 		$params =[
 				':flag'=>EnumOther::NO_DELETE,
 				':pid' => $pid
 		];
 		$joinArray = [
 			[
 				'unit u',
 				'u.unit_id = p.unit'
 			],
 		];
 		$units = ProductUnitDAO::getInstance()->iselect($fields, $conditions, $params,'all',"product_unit_id ASC",$joinArray,'p');
 		if(!$units){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,  '', 'no data found ');
 		}
 		
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $units);
 	}
 	
 	/**
 	 * @desc  保存采购单
 	 * @author liaojianwen
 	 * @date 2017-07-19
 	 */
 	public function saveWapPro($head, $detail)
 	{
 		if(empty($head)){
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'head is empty' );
 		}
 		if (empty ( $detail )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'detail is empty' );
 		}
 		//清掉为空的元素
 		foreach ($head as $k =>$hed){
 			if(empty($hed)){
 				unset($head[$k]);
 			}
 		}
 		foreach ($detail as $k=> $det){
 			foreach ($det as $j => $deet){
 				if(empty($deet)){
 					unset($detail[$k][$j]);
 				}
 			}
 		}
 		// 表头信息
 		$tr = Yii::$app->db->beginTransaction();
 		try{
 			$head ['procurement_date'] = time();
 			$head['procurement_no'] = EnumOriginType::PROCUREMENT.date('YmdHis');
 			$head['create_man'] = Yii::$app->user->id;
 			$head['total_amount'] = $head['procurement_amount'];
 			$Iid = ProcurementDAO::getInstance()->iinsert($head,true);
 			if (! $Iid) {
 				$tr->rollBack();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save procurement_head failure' );
 			}
 			//明细
 			foreach ( $detail as &$det ) {
 				$det ['procurement_id'] = $Iid;
 				$res_unit = ProductDAO::getInstance()->iselect("quantity_unit", "product_id =:id", [':id' => $det['goods_id']],'one');
 				if($res_unit){
 					$det['base_unit'] = $res_unit['quantity_unit'];
 				}	
 		
				$res_det = ProcurementDetailDAO::getInstance()->iinsert($det, true);
 				if (! $res_det) {
 					$tr->rollBack();
 					return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save procurement_detail failure' );
 				}
 			}
 			$tr->commit();
 			return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, '' );
 		}catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
 		}
 	}
 }

