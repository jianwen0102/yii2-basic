<?php
namespace app\wap\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class WapAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'statics/css/font-awesome-4.4.0/css/font-awesome.min.css',
//     	'statics/css/layout.css',
//     	'statics/css/site.css',
		'statics/css/waphome.css',
    	'statics/wap/css/framework.css',
    	'statics/wap/css/style.css',
    	'statics/wap/css/hidpi.css',
    	

    ];
    public $js = [
        'statics/js/toggles.js',
        // 'statics/js/layout.js',
        'statics/js/site.js',
    	'statics/js/common.js',
    	'statics/layer/layer.js',
    	'statics/wap/js/effects.jquery-ui.min.js',
    	'statics/wap/js/jquery.nivo-slider.min.js',
    	'statics/wap/js/jquery.colorbox.min.js',
    	'statics/wap/js/custom.js',
    	'statics/js/jquery-ui.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
