<?php

namespace app\wap\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class WapLoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    	'statics/css/styles.css',
    ];
    public $js = [
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
