<?php
 namespace app\wap\controllers;
 
 use app\controllers\BaseController;
 use Yii;
 use yii\web\Controller;
 use app\models\WapLoginForm;
	 
 class HomeController extends Controller
 {
 	public $layout = 'wapmain';
 	
 	public function actionIndex()
 	{
//  		 return $this->render('index');
		$this->redirect(['wap-procurement/index']);
 	}
 	
 	//登录
 	public function actionLogin()
 	{
 		$this->layout='waplogin';
 		$model = new WapLoginForm();
 		if ($model->load(Yii::$app->request->post()) && $model->login()) {
 			// return $this->goBack();
			$this->redirect(['home/index']);
 		} else {
 			return $this->render('waplogin',['model' => $model,]);
 		}
 	}
 	
 	/**
 	 * Logout action.
 	 *
 	 * @return string
 	 */
 	public function actionLogout()
 	{
 		Yii::$app->user->logout();
//  				$this->redirect(['site/index']);
 		return $this->goHome();
 	}
 }
 