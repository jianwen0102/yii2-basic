<?php
  namespace app\wap\controllers;
 
 use app\controllers\BaseController;
 use app\enum\EnumOther;
 use Yii;
use app\models\SupplierModel;
use app\models\WarehouseModel;
use app\wap\models\WapProcurementModel;
use app\helpers\CInputFilter;
use app\models\EmployeeModel;
					
 class WapProcurementController extends BaseController
 {
 	public $layout = 'wapmain';
 	
 	public function actionIndex()
 	{
 		$supplier = SupplierModel::model()->listSuppliers();
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$employee = EmployeeModel::model()->listEmployee();
 		return $this->render('index',['warehouse' => $warehouse,'supplier'=>$supplier,'employee' => $employee]);
 	}
 	
 	public function actionGetItem()
 	{
 		return $this->renderPartial('iteminfo');
 	}
 	
 	/**
 	 * @desc 获取产生列表
 	 * @author liaojianwen
 	 * @date 2017-07-18
 	 */
 	public function actionGetProductList()
 	{
 		$result = WapProcurementModel::model()->getProductList();
 		$this->renderJson($result);
 	}

 	/**
 	 * @desc 获取产品的单位
 	 * @author liaojianwen
 	 * @date 2017-07-19
 	 */
 	public function actionGetProUnit()
 	{
 		$pid = CInputFilter::getInt('pid',0);
 		$result = WapProcurementModel::model()->getProUnit($pid);
 		$this->renderJson($result);
 	}
 	
 	public function actionSaveWapPro()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
//  		$id = CInputFilter::getInt('id',0);
//  		$remove = CInputFilter::getArray('remove','');
 		$result = WapProcurementModel::model ()->saveWapPro ( $head, $detail);
 		$this->renderJson ( $result );
 	}
 }
 

 