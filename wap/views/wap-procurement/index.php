<?php
$this->registerJsFile('@web/statics/wap/js/wap-procurement/wappro_biz.js',['depends'=>['app\wap\assets\WapAppAsset']]);
$this->registerCssFile('@web/statics/searchableselect/jquery.searchableSelect.css',['depends'=>['app\wap\assets\WapAppAsset']]);
$this->registerJsFile('@web/statics/searchableselect/jquery.searchableSelect.js',['depends'=>['app\wap\assets\WapAppAsset']]);

$this->registerJsFile('@web/statics/select2/js/bootstrap-select2.min.js',['depends'=>['app\wap\assets\WapAppAsset']]);
$this->registerCssFile('@web/statics/select2/css/bootstrap-select2.min.css',['depends'=>['app\wap\assets\WapAppAsset']]);
$this->registerJsFile('@web/statics/select2/js/select2-i18n/zh-CN.js',['depends'=>['app\wap\assets\WapAppAsset']]);
$this->title = '市场采购单';
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="wap_pro">
	<div id="">
		<form id="wap_pro_header" class="col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<table class="table table-condensed table-border-null" id="">
					<tr>
						<td class="col-md-6 col-sm-6 col-xs-6 col-xs-offset-2">
							<div class="input-group">
								<span class="input-group-addon input-former">仓库</span>
								<select class="bootstrap-select form-control warehouse" data-name="warehouse" id="warehouse">
								<option value="0">请选择仓库...</option>
								<?php foreach ($warehouse as $ware):?>
									<option value="<?= $ware['warehouse_id']?>"><?= $ware['warehouse_name']?></option>
								<?php endforeach;?>
								</select>
							</div>
						</td>
						<td class="col-md-4 col-sm-4 col-xs-4">
							<div class="input-group">
								<span class="input-group-addon input-former">总金额</span>
								<input 	type="text" class="form-control" aria-describedby="basic-addon1" disabled
									id="wap_pro_amount" value='' data-id=""> 
							</div>
						</td>
					</tr>
					<tr>
						<td class="col-md-6 col-sm-6 col-xs-6 col-xs-offset-2">
							<div class="input-group">
								<span class="input-group-addon input-former">供应商</span>
								<select class="bootstrap-select form-control vendor" data-name="vendor" id="vendor">
								<option value="0">请选择供应商...</option>
								<?php foreach ($supplier as $sup):?>
									<option value="<?= $sup['supplier_id']?>"><?= $sup['supplier_name']?></option>
								<?php endforeach;?>
								</select>
							</div>
						</td>
						<td class="col-md-4 col-sm-4 col-xs-4">
							<div class="input-group">
								<span class="input-group-addon input-former">采购人</span>
								<select class="bootstrap-select form-control employee" data-name="employee" id="employee">
								<option value="0">请选择采购人...</option>
								<?php foreach ($employee as $emp):?>
									<option value="<?= $emp['employee_id']?>"><?= $emp['employee_name']?></option>
								<?php endforeach;?>
								</select>
							</div>
						</td>
					</tr>
					
					 <tr>
						<td class="col-md-6 col-sm-6 col-xs-6" colspan="2" style="width:100%;">
							<div class="input-group col-md-12 col-sm-112 col-xs-12">
								<span class="input-group-addon input-former">备注</span> 
								<input	type="text" class="form-control" aria-describedby="basic-addon1" id="head_remark">
								
							</div>
						</td>
					</tr> 
				</table>
			</div>
		</form>
	</div>
	<!-- <hr class="darken" style="clear: both;"> -->
	<div style="margin-bottom:10px;float:right;">
			<span class="label label-danger pull-right removeProduct" style="margin-right:10px;" id="removeItem"><small><span class="glyphicon glyphicon-minus"></span>移除商品</small></span> 
		   <span class="label label-success pull-right addProduct" style="margin-right:10px;" id="addItem"><small><span class="glyphicon glyphicon-plus"></span>新增商品</small></span>
	</div>
	<div id="wap_pro_detail">
		 <?= $this->render('iteminfo');?>
	</div>
	<div style="margin-bottom:100px;">
		<!--  <span class="label label-danger pull-left removeProduct" style="margin-left:10px;" id="removeItem"><small><span class="glyphicon glyphicon-minus"></span>移除商品</small></span> --> 
		<!--<span class="label label-success pull-right addProduct" style="margin-right:10px;" id="addItem"><small><span class="glyphicon glyphicon-plus"></span>新增商品</small></span>-->
		<span class="label label-info pull-right savePro" style="margin-right:10px;" id="savePro"><small><span class="fa fa-save"></span>&nbsp;保存</small></span> 
	</div>
	
</div>
<!-- 选择产品 -->
	<div class="modal fade" id="chooseProduct" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:50%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">选择商品</h4>
				</div>
				<div class="modal-body">
					<div class="row col-md-12 col-sm-12 col-lg-12" style="margin-top: 10px;margin-bottom: 15px;">

						<div class="col-md-3 col-sm-12 col-lg-2  input-group">
							<input class="form-control name" placeholder="商品名称" type="text" id="productName">
							<span class="input-group-btn" style="margin-left:10px;">
								<button type="button" class="btn btn-primary btn-sm" id="searchProduct">搜索</button>
							</span>
						</div>
					</div>
					<div>
						<table class="table table-border-null" id="products">
							<thead>
								<tr>
<!-- 									<th class="text-center">全选<input type="checkbox" class="checkAll"></th> -->
									<th class="text-center">序号</th>
									<th class="text-center">商品编号</th>
									<th class="text-center">商品名称</th>
									<th class="text-center">货号</th>
									<th class="text-center">数量</th>
									<th class="text-center">单位</th>
									<th class="text-center">操作</th>
								</tr>
							</thead>
							<tbody id="product_list">
	
							</tbody>
							<tfoot>
								<tr>
									<td class="paginationNavBar" colspan="8" data-ptype="pagination-sm"	style="text-align: right;"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal -->
	</div>

<?php $this->beginBlock('Item') ?>  

<?php $this->endBlock() ?>  
<?php $this->registerJs($this->blocks['Item'], \yii\web\View::POS_END); ?>  

