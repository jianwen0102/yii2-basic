<?php
?>
	<div class="item-info" style="padding-left:10px;padding-right:10px;">
		<table class="table table-condensed table-border-null order-info ">
			<tr>
				<td class="col-md-8 col-sm-7 col-xs-8">
					<div class="input-group">
						<span class="input-group-addon input-former" style="padding-left:1px;padding-right:3px;">商品名称</span>
						<!-- <input type="text" class="form-control product_name" aria-describedby = "basic-addon1" data-orderinfo="name"> -->
						<select class="form-control product_name" aria-describedby = "basic-addon1">
						</select>
					</div>
				</td>
				<td class="col-md-4 col-sm-4 col-xs-4">
					<div class="input-group">
						<span class="input-group-addon input-former">数量</span>
						<input type="text" class="form-control quantity" aria-describedby = "basic-addon1"  data-orderinfo="quantity">
					</div>
				</td>
			</tr>
			<tr>
				<td class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-group">
						<span class="input-group-addon input-former">单位</span>
						<select class="form-control unit" aria-describedby = "basic-addon1" >
									
						</select>
					</div>
				</td>
				<td class="col-md-5 col-sm-5 col-xs-5">
					<div class="input-group">
						<span class="input-group-addon input-former">单价</span>
						<input type="text" class="form-control price" aria-describedby = "basic-addon1" value="0.00" data-orderinfo="price">
					</div>
				</td>
			</tr>
			<tr>
				<td class="col-md-7 col-sm-7 col-xs-7">
					<div class="input-group">
						<span class="input-group-addon input-former">备注</span>
						<input type="text" class="form-control remark" aria-describedby = "basic-addon1">
					</div>
				</td>
				<td class="col-md-5 col-sm-5 col-xs-5">
					<div class="input-group">
						<span class="input-group-addon input-former">金额</span>
						<input type="text" class="form-control amount" aria-describedby = "basic-addon1" value="0.00" disabled data-orderinfo="amount">
					</div>
				</td>
	
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>