<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\wap\assets\WapAppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use yii\bootstrap\Modal;
use yii\helpers\Url;
// use app\widgets\sidebar\SidebarWidget;
use app\widgets\wappage\WapPage;

WapAppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="Keywords" content="blinds, premium, mobile, template, HTML, Css" />
	<meta name="Description" content="Premium mobile HTML/CSS template." />
<!--     <meta name="viewport" content="width=device-width, initial-scale=1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="websiteWrapper"> 
  
  <!-- header outer wrapper starts -->
  <div class="headerOuterWrapper">
    <!-- header outer wrapper starts -->
	<div class="headerOuterWrapper">
	  <div class="headerWrapper"> <a href="/site/index" class="mainLogo"></a> <a href="" class="mainMenuButton"></a></div>
	</div>
	<!-- header outer wrapper ends --> 
  </div>
  <!-- header outer wrapper ends --> 
	
  <!-- main menu outer wrapper starts -->
  <div class="mainMenuOuterWrapper mainMenuHidden" style="display:block;">
    <?=WapPage::widget()?>
  </div> 
  <!-- main menu outer wrapper ends --> 

</div>
<!-- website wrapper ends -->
		
<!-- preloader starts -->
<div class="preloader"></div>
<!-- preloader ends -->
<div class="mainpanel">
    <div class="contentpanel">
        <?= Breadcrumbs::widget([
            'homeLink'=>[
                'label' => '<i class="fa fa-home mr5"></i> '.Yii::t('yii', 'Home'),
                'url' => 'javascript:void(0)',
                'encode' => false,
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'tag'=>'ol',
            'options' => ['class' => 'breadcrumb breadcrumb-quirk']
        ]) ?>                
        <hr class="darken"> 
        <?= Alert::widget() ?>       
        <?=$content?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php $this->beginBlock('test1') ?>
	$(function(){
		alert(1222222);
	})
<?php $this->endBlock() ?>  
