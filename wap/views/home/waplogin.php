<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<div class="htmleaf-container">
<div class="wrapper">
<div class="container">
<h1>Welcome</h1>

<!-- <form class="form">
<input type="text" placeholder="Username">
<input type="password" placeholder="Password">
<button type="submit" id="login-button">Login</button>
</form> -->
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username',[
                    'inputOptions'=>[
                        'placeholder'=>'请输入账户',
                    	'style'=>"width:80%;"
                    ],
                    'inputTemplate'=>
                        '{input}',
                ])->textInput(['autofocus' => true])->label(false) ?>

                <?= $form->field($model, 'password',[
                    'inputOptions'=>[
                        'placeholder'=>'请输入密码',
                    	'style'=>"width:80%;"
                    ],
                    'inputTemplate'=>
                        '{input}',
                ])->passwordInput()->label(false) ?>
                                        
            <?= Html::submitButton('登录', ['class' => 'btn btn-primary btn-success btn-quirk btn-block', 'style'=>'width:80%;margin-left: 10%;','name' => 'login-button']) ?>

     <?php ActiveForm::end(); ?>

</div>
	
<ul class="bg-bubbles">
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
<li></li>
</ul>
</div>
</div>

<?php $this->beginBlock('test') ?>
$('#login-button').click(function (event) {
	event.preventDefault();
	$('form').fadeOut(500);
	$('.wrapper').addClass('form-success');
});
<?php $this->endBlock() ?>  
<?php $this->registerJs($this->blocks['test'], \yii\web\View::POS_END); ?>  
