<?php
/**
 * @desc   pos机列表模型
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\models;
use Yii;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\StorePosDAO;

class StorePosModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return ListStoreStoreModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStorePosList($pageInfo,$searchData,$filter)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$storeList = StorePosDAO::getInstance()->getStorePosListData($pageInfo,$searchData,$filter);

		if (empty ( $storeList )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $storeList );
	}

	/**
	 * @desc   保存添加/编辑门店
	 * @param  $data  array 门店数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStorePos($data)
	{
	    if(empty($data['pos_name'])){
		   return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','pos_name is null');
		}
		//检查是否有重名
        $result = StorePosDAO::getInstance()->checkStorePos($data['pos_id'],$data['pos_name']);	
		if($result){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','pos_name is exists');
		}
        $result = StorePosDAO::getInstance()->saveStorePosData($data);				
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}

	/**
	 * @desc   获取一条数据
	 * @param  $employee_id  pos机id
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	 public function getStorePosOne($pos_id)
	 {
		 if($pos_id<1){
		    return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','pos_id is null');
		 }
         $result = StorePosDAO::getInstance()->getStorePosOne($pos_id);
	     if(empty($result)){
		    return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','select failure');
		 }
		 return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
	 }
}

