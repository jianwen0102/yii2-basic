<?php
/**
 * @desc   门店后台管理员模型
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\models;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\enum\EnumOther;
use app\dao\StoreAdminDAO;

class StoreAdminModel extends ActiveRecord implements IdentityInterface
{
	const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    public static function tableName()
    {
        return '{{%store_admin}}';
    }
    /**
     * 行为
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * 验证规则
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        	//['delete_flag','default','value'=>EnumOther::NO_DELETE]
        ];
    }

    /**
     * 通过给定的ID找到一个标识
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * 通过给定的令牌找到一个标识
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     * @modify liaojianwen
     */
    public static function findByUsername($username)
    {		
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        //return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'delete_flag'=>EnumOther::NO_DELETE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * 返回一个惟一标识用户标识的ID
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * 返回一个可以用来检查给定标识符的有效性的键
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * 验证给定的auth密钥
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


	/**
	 * @desc   获取门店后台管理员列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStoreAdminList($pageInfo,$searchData,$filter)
	{
		if(empty($pageInfo)){
			return handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$storeAdminList = StoreAdminDAO::getInstance()->getStoreAdminListData($pageInfo,$searchData,$filter);

		if (empty ( $storeAdminList )) {
			return handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return handleApiFormat ( EnumOther::ACK_SUCCESS, $storeAdminList );
	}

    /**
     * @desc   新增管理员用户
     * @param  $userInfo object 用户数据
     * @author hehuawei
     * @date   2017-7-29
     * @return multitype
     */
    public function createUser($userInfo)
    {
    	if(empty($userInfo)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','user info empty');
    	}
    	$this->username    = $userInfo['adminname'];
		$this->store_id    = $userInfo['store_id'];
    	$this->email       = $userInfo['email'];
		$this->delete_flag = $userInfo['delete_flag'];
    	$this->setPassword($userInfo['password']);
    	$this->generateAuthKey();
    	
    	$result=  $this->save() ? true : false;
    	if (empty($result)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
    	}
    	return handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
    	
    }
	
}

