<?php
namespace app\models;

use app\models\BaseModel;
use app\dao\WarehouseDAO;
use app\enum\EnumOther;
use app\dao\UnitDAO;
use Yii;
use app\helpers\storeTokenTool;
use app\helpers\Curl;

class WarehouseModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author liaojianwen
	 * @date 2016-11-3
	 * @return WarehouseModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @desc 获取仓库列表
	 * @param [] $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-3
	 */
	public function getWarehouses($pageInfo)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','missing params');
		}
		$result = WarehouseDAO::getInstance()->getWarehouses($pageInfo);
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
	
	}
	
	/**
	 * @desc 新增/编辑保存
	 * @param string $name 仓库名
	 * @param string $remark 备注
	 * @param int $default 是否默认主仓库
	 * @param $int $id
	 * @author liaojianwen
	 * @date 2016-11-03
	 */
	public function saveWarehouse($name, $remark, $default, $id, $scrapped)
	{
		if(empty($name)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','name is null');
		}
		if(empty($id)){
			$id = 0;
		}
		$wareInfo = WarehouseDAO::getInstance()->checkSameName($name,$id);
		if($wareInfo){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','warehouse is exists');
		}
		$scrapped = ($scrapped == 'true' ? 1: 0);
		$columns = array(
				'warehouse_name' => $name,
				'remark' => $remark,
// 				'is_default' => $default,
				'is_scrapped' => $scrapped,
		);
		
// 		foreach($columns as $key=> $column){
// 			if(empty($column)){
// 				unset($columns[$key]);
// 			}
// 		}
		if($id == 0){
			$columns['create_time'] = time();
		} else {
			$columns['modify_time'] = time();
		}
		$conditions = "warehouse_id = :id and delete_flag = :flag ";
		$params = array(
				':id' => $id,
				':flag' => EnumOther::NO_DELETE,
		);
		$res = WarehouseDAO::getInstance()->iselect(array('warehouse_id'),$conditions,$params,'one');
		if(empty($res)){
			//新增
			$result = WarehouseDAO::getInstance()->iinsert($columns);
		} else {
			//更新
			$result = WarehouseDAO::getInstance()->updateByPk($id, $columns);
		}
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 单据库存列表
	 * @author  liaojianwen
	 * @date 2016-11-4
	 */
	public function listWarehouse($is_selected = 0)
	{
		$fields = [
			'warehouse_id',
			'warehouse_name',
		];
		$conditions ="delete_flag = :flag";
		$params = [
			':flag'=>EnumOther::NO_DELETE,
		];
		$result = WarehouseDAO::getInstance()->iselect($fields, $conditions, $params,'all','warehouse_id ASC');
		if($is_selected){
			foreach ($result as &$res){
				if($res['warehouse_id'] == $is_selected){
					$res['selected'] = true;
				}
			}
		}
		return $result;
	}
	
	/**
	 * @desc 删除仓库
	 * @param string  $ids //1,2,3,4...
	 * @author liaojianwen
	 * @date 2016-11-04
	 * @return multitype:
	 */
	public function delWarehouse($ids)
	{
		if(empty($ids)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','ids is missing');
		}
		$_ids = explode(',',$ids);
		$params = array(
				'delete_flag'=>1
		);
		foreach($_ids as $id){
			$result = WarehouseDAO::getInstance()->updateByPk($id, $params);
		}
		if($result == false){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del failed');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 获取单位列表数据
	 * @author liaojianwen
	 * @date 2016-11-04
	 */
	public function getUnits($pageInfo)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params empty');
		}
		$result = UnitDAO::getInstance()->getUnits($pageInfo);
		if($result == false){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
	}
	
	/**
	 * @desc  保存单位
	 * @param string $name
	 * @param string $desc
	 * @param string $active
	 * @param int $id
	 * @author liaojianwen
	 * @date 2016-11-05
	 */
	public function saveUnit($name,$desc,$active,$id)
	{
		if(empty($name)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','name is null');
		}
		if(empty($id)){
			$id = 0;
		}
		$tr = Yii::$app->db->beginTransaction();
		try{
			$unitInfo = UnitDAO::getInstance()->checkSameName($name, $id);
			if($unitInfo){
				return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','unit is exists');
			}
			if($active == 'false'){
				$active = 0;
			} else {
				$active = 1;
			}
			$columns = array(
					'unit_name' => $name,
					'description' => $desc?:'',
					'is_active' => $active,
			);
// 			if($id == 0){
// 				$columns['create_time'] = time();
// 			} 
			$conditions = "unit_id = :id and delete_flag = :flag ";
			$params = array(
					':id' => $id,
					':flag' => EnumOther::NO_DELETE,
			);
			$result = UnitDAO::getInstance()->ireplaceinto($columns, $conditions, $params,true);
// 			$res = UnitDAO::getInstance()->iselect(array('unit_id'),$conditions,$params,'one');
// 			if(empty($res)){
// 				//新增
// 				$result = UnitDAO::getInstance()->iinsert($columns,true);
// 			} else {
// 				//更新
// 				$result = UnitDAO::getInstance()->updateByPk($id, $columns);
// 			}
			if(empty($result)){
				$tr->rollBack();
				return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
			}
			// 同步供应链到店铺
			$res_unit = $this->syncStoreUnit($result);
			if($res_unit != 'true'){
				$tr->rollBack();
				return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'store unit sync fail');
			}
			
			$tr->commit();
			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
		}catch (\Exception $e){
			$tr->rollBack();
			return false;
		}
	}
	
	/**
	 * @desc 删除单位
	 * @param string  $ids //1,2,3,4...
	 * @author liaojianwen
	 * @date 2016-11-07
	 * @return multitype:
	 */
	public function delUnit($ids)
	{
		if(empty($ids)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','ids is missing');
		}
		$_ids = explode(',',$ids);
		$params = array(
				'delete_flag'=>EnumOther::DELETED
		);
		foreach($_ids as $id){
			$result = UnitDAO::getInstance()->updateByPk($id, $params);
		}
		if($result == false){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del failed');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	
	/**
	 * @desc 获取单位信息 用于下列表
	 * @author liaojianwen
	 * @date 2016-11-18
	 */
	public function listUnits($is_selected = 0)
	{
		$fields = [
			'unit_id',
			'unit_name',
		];
		$conditions = "delete_flag = :flag and is_active=:active";
		$params = [
			':flag'=>EnumOther::NO_DELETE,
			':active'=> EnumOther::ACTIVE,
		];
		$result = UnitDAO::getInstance()->iselect($fields, $conditions, $params, 'all', ['create_time'=>SORT_ASC, 'unit_id'=>SORT_ASC]);
		if($is_selected){
			foreach ($result as &$res){
				if($res['unit_id'] == $is_selected){
					$res['selected'] = true;
				}
			}
		}
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'', 'no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
	}
	
	
	/**
	 * @desc 单位同步到店铺
	 * @author liaojianwen
	 * @date 2017-08-05
	 */
	public function syncStoreUnit($unit_id)
	{
		if(!$unit_id){
			return false;
		}
		try{
			$unit_fields =[
				'unit_id',
				'unit_name',
				'is_active',
				'description',
				'create_time',
			];
			$conditions="unit_id=:uid";
			$params= [
				':uid'=>$unit_id
			];
			$unit_select = UnitDAO::getInstance()->iselect($unit_fields, $conditions, $params,'one');
			if(!$unit_select){
				return false;
			}
			$storeUrl = Yii::$app->params ['storeUrl'];
				
			$key = storeTokenTool::getInstance()->getToken();
			$url = "{$storeUrl}/scm/api/unit-update";
				
			$postdata = json_encode($unit_select);
			$result = Curl::curlOption($url, 'POST', $postdata, $key);
			return  $result;
			
		} catch (\Exception $e){
			return false;
		}
		
	}
	
}