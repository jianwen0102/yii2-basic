<?php
 namespace app\models;
 /**
  * @desc 客户操作model
  */
 use app\models\BaseModel;
 use app\enum\EnumOther;
 use app\dao\ClientDAO;
 use app\dao\RegionDAO;
 use Yii;
 use app\dao\StoreDAO;
			 
 class ClientModel extends BaseModel 
 {
 	/**
 	 * @desc覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 * @return ClientModel
 	 */
 	public static function model($className = __CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	/**
 	 * @desc 获取客户列表
 	 * @param [] $cond 查询条件
 	 * @param [] $pageInfo
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function getClients($cond, $pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
 		}
 	
 		$result = ClientDAO::getInstance ()->getClients ($cond, $pageInfo );
 		if (empty ( $result )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
 		}
 		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
 	}
 	
 	/**
 	 * @desc 获取默认地址
 	 * @param  $parent//父id
 	 * @param  $selected //选中
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function getDefaultRegion($parent, $selected)
 	{
 	
 		$result = RegionDAO::getInstance()->getRegion($parent);
 		foreach($result as &$res){
 			if($res['region_id'] == $selected){
 				$res['selected'] = 1;
 			}
 		}
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取下一级地址
 	 * @param  $pid
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function getRegions($pid)
 	{
 		if(empty($pid)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no child data found');
 		}
 		$result = RegionDAO::getInstance()->getRegion($pid);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	
 	}
 	
//  	/**
//  	 * @desc 供应商列表
//  	 * @param [] $pageInfo
//  	 * @author  liaojianwen
//  	 * @date 2016-11-18
//  	 */
//  	public function listClients()
//  	{
//  		$fields = [
//  				'supplier_id',
//  				'supplier_name',
//  		];
//  		$conditions ="delete_flag = :flag and type= :type";
//  		$params = [
//  				':flag'=>EnumOther::NO_DELETE,
//  				':type'=>EnumOther::SUPPLIER,
//  		];
//  		$userId = Yii::$app->user->id;
//  		$auth = Yii::$app->authManager;
//  		$role = $auth->getRolesByUser($userId);
 	
//  		if(array_key_exists(EnumOther::BUY_ROLE, $role)){//采购角色
//  			$conditions .=" and create_man =:man and type=:type";
//  			$params[':man'] = $userId;
//  			$params[':type'] = EnumOther::SUPPLIER;
//  		}
//  		$result = SupplierDAO::getInstance()->iselect($fields, $conditions, $params, 'all', ['supplier_id'=>SORT_ASC]);
//  		return $result;
//  	}

 	/**
 	 * @desc 保存供应商
 	 * @param [] $supplier_info
 	 * @author liaojianwen
 	 * @date 2016-11-21
 	 */
 	public function saveClient($client_info)
 	{
 		if(empty($client_info)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
 		}
 		$result = ClientDAO::getInstance()->saveClient($client_info);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
 	}
 	
 	
 	/**
 	 * @desc 根据id 获取供应商信息
 	 * @param int $id
 	 * @return multitype:
 	 * @author liaojianwen
 	 * @date 2017-05-21
 	 */
 	public function getClientInfo($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
 		}
 		$fields = [
 				'client_id',
 				'client_name',
 				'client_desc',
 				'handle_man',
 				'phone',
 				'store_id',
 		];
 		$conditions = "client_id =:id";
 		$params = [
 				':id'=>$id
 		];
 		$result = ClientDAO::getInstance()->iselect($fields, $conditions, $params, 'one');
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	
 	}
 	
 	/**
 	 * @desc 删除客户
 	 * @params string $ids //1,2,3,4...
 	 * @author liaojianwen
 	 * @date 2017-05-21
 	 */
 	public function delClient($ids)
 	{
 		if(empty($ids)){
 			$this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
 		}
 		$tr = Yii::$app->db->beginTransaction();
 		try{
 			$_ids = explode(',',$ids);
 			$columns = [
 					'delete_flag'=>EnumOther::DELETED,
 					'modify_time'=>time(),
 			];
 			foreach ($_ids as $id){
 				$conditions ="client_id=:id";
 				$params=[
 						':id'=>$id,
 				];
 				$result = ClientDAO::getInstance()->iupdate($columns, $conditions, $params);
 				if(empty($result)){
 					$tr->rollBack();
 				}
 			}
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
 		}catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','return false');
 		}
 	}
 	
 	/**
 	 * @desc 客户列表
 	 * @author  liaojianwen
 	 * @date 2017-05-22
 	 */
 	public function getCustomer()
 	{
 		$fields = [
 				'client_id',
 				'client_name',
 		];
 		$conditions ="delete_flag = :flag";
 		$params = [
 				':flag'=>EnumOther::NO_DELETE,
 		];
 		$result = ClientDAO::getInstance()->iselect($fields, $conditions, $params, 'all', ['client_id'=>SORT_ASC]);
 		return $result;
 	}
 	
 	/**
 	 * @desc 获取店铺列表
 	 * @author liaojianwen
 	 * @date 2017-07-30
 	 */
 	public function listStores()
 	{
 		$fields = [
 			'store_id',
 			'store_name',
 		];
 		$conditions="delete_flag=:flag";
 		$params=[
 			':flag'=>EnumOther::NO_DELETE,
 		];
 		$result = StoreDAO::getInstance()->iselect($fields, $conditions, $params);
 		return $result;
 	}
 }