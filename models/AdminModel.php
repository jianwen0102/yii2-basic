<?php
namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\enum\EnumOther;
use app\dao\AdminDAO;

/**
 * AdminModel
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property integer $delete_flag
 * @property integer $last_in
 */
class AdminModel extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admin}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        	['delete_flag','default','value'=>EnumOther::NO_DELETE]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     * @modify liaojianwen
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE, 'delete_flag'=>EnumOther::NO_DELETE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    /**
     * @desc 获取管理员列表
     * @param  $pageInfo
     * @author liaojianwen
     * @date 2016-11-16
     */
    public function getUserList($pageInfo)
    {
    	if(empty($pageInfo)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
    	}
    	$result = AdminDAO::getInstance()->getUserList($pageInfo);
    	if(empty($result)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
    	}
    	return  handleApiFormat(EnumOther::ACK_SUCCESS,$result);
    }
    
    /**
     * @desc 新增管理员用户
     * @param [] $userInfo
     * @author liaojianwen
     * @date 2016-11-16
     * @return multitype:
     */
    public function createUser($userInfo)
    {
    	if(empty($userInfo)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','user info empty');
    	}
    	$this->username = $userInfo['adminname'];
    	$this->email = $userInfo['email'];
    	$this->setPassword($userInfo['password']);
    	$this->generateAuthKey();
    	
    	$result=  $this->save() ? true : false;
    	if (empty($result)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
    	}
    	return handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
    	
    }
    
    /**
     * @desc 删除用户
     * @param  $ids//1,2,3
     * @author liaojianwen
     * @date 2016-11-16
     * @return multitype:
     */
    public function delUser($ids)
    {
    	if(empty($ids)){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'' ,'params is empty');
    	}
    	$_ids = explode(',',$ids);
    	$params = array(
    			'delete_flag'=>1
    	);
    	foreach($_ids as $id){
    		$result =AdminDAO::getInstance()->updateByPk($id, $params);
    	}
    	if($result == false){
    		return handleApiFormat(EnumOther::ACK_FAILURE,'','del failed');
    	}
    	return handleApiFormat(EnumOther::ACK_SUCCESS,'');
    }
    
    
    /**
     * @desc 用户列表
     * @param [] $pageInfo
     * @author  liaojianwen
     * @date 2016-11-18
     */
    public function listAdmins()
    {
    	$fields = [
    			'id',
    			'username',
    	];
    	$conditions ="delete_flag = :flag";
    	$params = [
    			':flag'=>EnumOther::NO_DELETE,
    	];
    	$result = AdminDAO::getInstance()->iselect($fields, $conditions, $params);
    	return $result;
    }
    
    /**
     * @desc 保存分组分配
     * @param [] $info 分配给用户的角色
     * @param $user_id 用户id
     * @author liaojianwen
     * @date 2017-01-13
     */
    public function saveAuthAssign($info, $user_id)
    {
    	if(empty($info) || empty($user_id)){
    		return handleApiFormat(EnumOther::ACK_FAILURE, '', 'param is empty');
    	}
    	$tr = Yii::$app->db->beginTransaction();
    	try{
	    	$auth = Yii::$app->authManager;
	    	$auth->revokeAll($user_id);
	    	foreach($info as $assign)
	    	{
	    		if($assign[2] == 'true'){
					$oneRole = $auth->getRole($assign[0]);
					$result = $auth->assign($oneRole, $user_id);
					if(!$result){
						$tr->rollBack();
						return handleApiFormat(EnumOther::ACK_FAILURE, '', 'assign failed');
					}
	    		}
	    	}
	    	$tr->commit();
	    	return handleApiFormat(EnumOther::ACK_SUCCESS, '');
    	}catch (\Exception $e){
    		$tr->rollBack();
    		return handleApiFormat(EnumOther::ACK_FAILURE,'', $e->getMessage());
    	}
    }
}
