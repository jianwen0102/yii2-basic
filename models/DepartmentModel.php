<?php
namespace app\models;

use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\DepartmentDAO;
class DepartmentModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author liaojianwen
	 * @date 2016-11-23
	 * @return DepartmentModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @desc 获取部门列表数据
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function getDepart($pageInfo)
	{
		if (empty ( $pageInfo )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'param is empty' );
		}
		$result = DepartmentDAO::getInstance ()->getDepart ( $pageInfo );
		if (empty ( $result )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
	}
	
	/**
	 * @desc 保存部门
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function saveDepart ( $name, $remark, $id, $master)
	{
		if(empty($name)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'', 'depart_name is empty');
		}
		if(empty($master)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'', 'master is empty');
		}
		if(empty($id)){
			$id = 0;
		}
		$res_same = DepartmentDAO::getInstance()->checkSameName($name, $id);
		if($res_same){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','department is exists');
		}
		
		$columns = array(
				'depart_name' => $name,
				'description' => $remark,
				'master' => $master
		);
		foreach($columns as $key=> $column){
			if(empty($column)){
				unset($columns[$key]);
			}
		}
		if($id == 0){
			$columns['create_time'] = time();
		} else {
			$columns['modify_time'] = time();
		}
		$conditions = "depart_id = :id and delete_flag = :flag ";
		$params = array(
				':id' => $id,
				':flag' => EnumOther::NO_DELETE,
		);
		$res = DepartmentDAO::getInstance()->iselect(array('depart_id'),$conditions,$params,'one');
		if(empty($res)){
			//新增
			$result = DepartmentDAO::getInstance()->iinsert($columns);
		} else {
			//更新
			$result = DepartmentDAO::getInstance()->updateByPk($id, $columns);
		}
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 删除部门信息
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function delDepart($ids)
	{
		if(empty($ids)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
		}
		$_ids = explode(',', $ids);
		$params = [
			'delete_flag'=>EnumOther::DELETED
		];
		foreach ($_ids as $id){
			$result = DepartmentDAO::getInstance()->updateByPk($id, $params);
		}
		if($result == false){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del failed');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
		
	}
	
	/**
	 * @desc 部门下拉列表
	 * @author  liaojianwen
	 * @date 2017-07-05
	 */
	public function listDepartment()
	{
		$fields = [
				'depart_id',
				'depart_name',
		];
		$conditions ="delete_flag = :flag";
		$params = [
				':flag'=>EnumOther::NO_DELETE,
		];
		$result = DepartmentDAO::getInstance()->iselect($fields, $conditions, $params);
		return $result;
	}
}