<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\AdminModel;


/**
 * WapLogin form
 */
class WapLoginForm extends Model
{
    public $username;
    public $password;
    private  $rememberMe = false;
    
// 	public $verifyCode;
    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            // [['username', 'password'], 'required','message'=>'用户名或密码不能为空'],
            ['username', 'required','message'=>'用户名不能为空'],
        	//验证验证码
//         	['verifyCode','captcha','captchaAction'=>'site/captcha', 'message'=>'验证码不正确'],
        		
            ['password', 'required','message'=>'密码不能为空'],
            // rememberMe must be a boolean value
//             ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username'=>'用户名',
            'password'=>'密码',
//         	'verifyCode'=>'验证码',
        ];
    }
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, '用户名或密码不正确.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
        	//登录时间 start
        	$admin = $this->getUser();
        	$admin->last_in = time();
        	$admin->save();
        	//end
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = AdminModel::findByUsername($this->username);
        }

        return $this->_user;
    }
}