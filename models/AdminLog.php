<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admin_log".
 *
 * @property integer $id
 * @property string $route
 * @property string $description
 * @property integer $create_time
 * @property integer $admin_id
 * @property string $admin_name
 * @property string $sql
 */
class AdminLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_log';
    }

//     /**
//      * @inheritdoc
//      */
//     public function rules()
//     {
//         return [
//             [['description'], 'string'],
//             [['created_at'], 'required'],
//             [['created_at', 'user_id'], 'integer'],
//             [['route'], 'string', 'max' => 255],
//         ];
//     }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route' => 'Route',
            'description' => 'Description',
            'created_at' => 'Created At',
            'user_id' => 'User ID',
        ];
    }
    
    /**
     * @desc 保存操作记录
     * @author liaojianwen
     * @date 2017-02-13
     */
    public static function saveLog($controller ,$action, $route, $sql, $result=NULL){
    	$model = new self;
    	$model->admin_ip = Yii::$app->request->userIP;
    	$model->create_time = time();
    	if($controller =='shop/api'){
    		$model->admin_id = 0;
    		$model->admin_name = 'shop';
    	} else if($controller =='tore/api'){
    		$model->admin_id = 0;
    		$model->admin_name = 'store';
    	}else {
    		$model->admin_id = Yii::$app->user->identity->id;
    		$model->admin_name = Yii::$app->user->identity->username;
    	}
  		
    	$controllers = ['admin','category', 'client','department','employee', 'funds','instore','inventory','outstore','permission','price','procurement','product','product-log','purchase','purchase_return','sale','stock-pile','supplier','transaction','transfer','warehouse','shop/api'];
    	if(!in_array(strtolower($controller),$controllers)) $controller = '';
    
    	$model->controller = $controller;
    	$model->action = $action;
    	$model->route = $route;
    	$model->sql = $sql; 
    	
    	$model->description =  $model->admin_name.' '.$model->action.' '.$model->controller;
    	
    	$dd = $model->save(false);
    	return $dd;
    }
}
