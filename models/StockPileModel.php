<?php
namespace app\models;
/**
 * @desc 分仓表模型
 */
use app\models\BaseModel;
use app\dao\StockPileDAO;
use app\enum\EnumOther;
use Yii;

class StockPileModel  extends BaseModel
{
	/**
	 * @desc 覆盖父方法返回对象
	 * @author liaojianwen
	 * @date 2016-11-24
	 * @return StockPileModel
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @desc 获取分仓数据信息
	 * @cond [] 查询条件
	 * @pageInfo [] 页面信息
	 * @author liaojianwen
	 */
	public function getStockInfo($cond, $pageInfo)
	{
		$product = StockPileDAO::getInstance()->findByProduct($cond, $pageInfo);

		foreach ($product['list'] as &$pp){
			$quan  = StockPileDAO::getInstance()->quantity($pp['product_id']);
			
			$auth = Yii::$app->authManager;
			$userId = Yii::$app->user->id;
			$res = $auth->checkAccess($userId, EnumOther::CHECK_PRODUCT_PRICE);
			if(!$res){
				$pp['price'] = 0;
			}
// 			dd($res);
// 			dd($auth->getPermission('product/check-product-price'));
// 			$auth->get
// 			product/check-product-price
			
			$total_quantity = 0;
			$total_amount = 0.00;
			foreach ($quan as &$stock){
				$total_quantity += (int)$stock['quantity'];
				$total_amount += (int)($stock['quantity'] * $pp['price']);
				$stock['amount'] = (int)($stock['quantity'] * $pp['price']);
			}
			$pp['warehouse'] = $quan;
			$pp['total_quantity'] = $total_quantity;
			$pp['total_amount'] = $total_amount;
		}
		if(empty($product)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $product);
		
	}
	
}