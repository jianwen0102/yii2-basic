<?php
 namespace app\models;
 
 /**
  * @desc 报表模型
  */
 use app\models\BaseModel;
 use Yii;
 use app\enum\EnumOther;
 use app\dao\OrderTransactionDAO;
use app\dao\OrderGoodsDAO;
use app\dao\PriceTransactionDAO;
use app\dao\SaleoutSyncDAO;
use app\dao\SaleOutSyncDetailDAO;
use app\dao\ProductExtendDAO;
use app\dao\PurchaseDetailDAO;
use app\dao\InventoryLostDetailDAO;
use app\dao\TransferDetailDAO;
									 
 class ReportsModel extends BaseModel
 {
 	/**
 	 * @desc 覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-02-01
 	 * @return ReportsModel
 	 */
 	public static function model($className = __CLASS__)
 	{
 		return parent::model($className);
 	}
 	
//  	/**
//  	 * @desc 销售毛利明细表
//  	 * @param $pageInfo 页面信息
//  	 * @param $condition 查询条件
//  	 * @date 2018-02-01
//  	 * @author liaojianwen
//  	 */
//  	public function getSaleGrossProfitDetail ( $pageInfo, $condition)
//  	{
//  		if(empty($pageInfo)){
//  			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
//  		}
//  		$result = OrderGoodsDAO::getInstance()->getSaleGrossProfitDetail($condition, $pageInfo);
//  		if(empty($result)){
//  			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
//  		}
//  		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
//  	}
 	
 	/**
 	 * @desc 销售毛利汇总数据（按商品）
 	 * @param $pageInfo 页面信息
 	 * @param $condition 查询条件
 	 * @date 2018-02-05
 	 * @author liaojianwen
 	 */
 	public function getSaleGrossProfitSummary ( $pageInfo, $condition)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$trans_res = PriceTransactionDAO::getInstance()->getSaleGrossProfitSummary($condition, $pageInfo);
 		if(empty($trans_res['list'])){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		foreach ($trans_res['list'] as &$res){
 			$res_sale = SaleOutSyncDetailDAO::getInstance()->getSalePriceSummary($condition['starTime'],$condition['endTime'], $res['product_id']);
 			if($res_sale){
 				$res['sum_sale_amount'] = $res_sale['sum_amount'];
 			} else {
 				$res['sum_sale_amount'] = 0;
 			}
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$trans_res);
 	}
 	
 	/**
 	 * @desc 获取特殊需分拆商品
 	 * @author liaojianwen
 	 * @date 2018-04-12
 	 */
 	public function getSpecialPro()
 	{
 		$conditions = "goods_type =:type";
 		$params = [
 			':type' => EnumOther::BASE_GOODS
 		];
 		$joinArray = [
 			[
 				'product p',
 				'p.product_id = e.product_id',
 				'left'=>'',
 			]
 		];
 		$goods_select= ProductExtendDAO::getInstance()->iselect("p.product_id,product_name", $conditions, $params,'all','goods_type',$joinArray,'e');
 		return $goods_select;
 	}
 	
 	
 	/**
 	 * @desc 特定商品销售毛利汇总数据（按商品）
 	 * @param $pageInfo 页面信息
 	 * @param $condition 查询条件
 	 * @date 2018-04-12
 	 * @author liaojianwen
 	 */
 	public function getSpecialGrossProfitSummary ( $pageInfo, $condition, $goods_id)
 	{
 		if(empty($condition['starTime'])){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','time is empty');
 		}
 		
 		if(!$goods_id){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'goods_id is empty');
 		}
 		
 		
 		$in_goods = PurchaseDetailDAO::getInstance()->getSpecialIn($condition, $goods_id);
 		$out_goods = PriceTransactionDAO::getInstance()->getSpecialOut($condition, $pageInfo, $goods_id);
 		$lost_goods = TransferDetailDAO::getInstance()->getSpecialLost($condition, $goods_id);
 		
 		$special_arr = [
 			'in'=>$in_goods,
 			'out'=> $out_goods,
 			'lost' => $lost_goods
 		];
 		
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $special_arr);
 		
 	}
 }