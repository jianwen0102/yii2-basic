<?php
namespace app\models;
/**
 * @desc product_update_log model
 * @author liaojianwen
 * @date 2017-01-03
 */
 use app\enum\EnumOther;
 use app\models\BaseModel;
 use app\dao\ProductUpdateLogDAO;
 
 class ProductLogModel extends BaseModel
 {
 	/**
 	 * @desc 覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-01-03
 	 * @return ProductLogModel
 	 */
 	public static function model($className=__CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	/**
 	 * @desc 获取商品更新log 
 	 * @param [] $cond 查询条件
 	 * @param $filter 过滤条件
 	 * @param [] $pageInfo 页面信息
 	 * @author liaojianwen
 	 */
 	public function getProductLog($cond, $filter, $pageInfo)
 	{
 		if(!$pageInfo){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'pageinfo is empty');
 		}
 		
 		$result = ProductUpdateLogDAO::getInstance()->getProductLog($cond, $filter, $pageInfo);
 		if(!$result){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 }