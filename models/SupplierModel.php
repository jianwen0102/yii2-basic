<?php
namespace app\models;

use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\SupplierDAO;
use app\dao\RegionDAO;
use app\enum\EnumOriginType;
use Yii;
use app\dao\SupplierImageDAO;

class SupplierModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author liaojianwen
	 * @date 2016-11-10
	 * @return SupplierModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @desc 获取供应商列表
	 * @param [] $cond 查询条件
	 * @param $filter 过滤条件  
	 * @param [] $pageInfo
	 * @author liaojianwen
	 * @date 2016-11-11
	 */
	public function getSuppliers($cond, $filter, $pageInfo)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$result = SupplierDAO::getInstance ()->getSupplier ($cond, $filter, $pageInfo );
		if (empty ( $result )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
	}
	
	/**
	 * @desc 获取默认地址
	 * @param  $parent//父id
	 * @param  $selected //选中
	 * @author liaojianwen
	 * @date 2016-11-14
	 */
	public function getDefaultRegion($parent, $selected)
	{
		
		$result = RegionDAO::getInstance()->getRegion($parent);
		foreach($result as &$res){
			if($res['region_id'] == $selected){
				$res['selected'] = 1;
			}
		}
		return $result;
	}
	
	/**
	 * @desc 获取下一级地址
	 * @param  $pid
	 * @author liaojianwen
	 * @date 2016-11-14
	 */
	public function getRegions($pid)
	{
		if(empty($pid)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no child data found');
		}
		$result = RegionDAO::getInstance()->getRegion($pid);
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
		
	}
	
	/**
	 * @desc 供应商列表
	 * @param [] $pageInfo
	 * @author  liaojianwen
	 * @date 2016-11-18
	 */
	public function listSuppliers()
	{
		$fields = [
				'supplier_id',
				'supplier_name',
		];
		$conditions ="delete_flag = :flag and type= :type";
		$params = [
				':flag'=>EnumOther::NO_DELETE,
				':type'=>EnumOther::SUPPLIER,
		];
		$userId = Yii::$app->user->id;
		$auth = Yii::$app->authManager;
		$role = $auth->getRolesByUser($userId);
		
		if(array_key_exists(EnumOther::BUY_ROLE, $role)){//采购角色
			// $conditions .=" and create_man =:man and type=:type";
			// $params[':man'] = $userId;
			// $params[':type'] = EnumOther::SUPPLIER;
		}
		$result = SupplierDAO::getInstance()->iselect($fields, $conditions, $params, 'all', ['supplier_id'=>SORT_ASC]);
		return $result;
	}
	
	/**
	 * @desc 客户列表
	 * @author  liaojianwen
	 * @date 2017-02-07
	 */
	public function getCustomer()
	{
		$fields = [
				'supplier_id',
				'supplier_name',
		];
		$conditions ="delete_flag = :flag and type= :type";
		$params = [
				':flag'=>EnumOther::NO_DELETE,
				':type'=>EnumOther::CUSTOMER,
		];
		$result = SupplierDAO::getInstance()->iselect($fields, $conditions, $params, 'all', ['supplier_id'=>SORT_ASC]);
		return $result;
	}
	/**
	 * @desc 保存供应商 
	 * @param [] $supplier_info
	 * @author liaojianwen
	 * @date 2016-11-21
	 */
	public function saveSupplier($supplier_info)
	{
		if(empty($supplier_info)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
		}
		$result = SupplierDAO::getInstance()->saveSupplier($supplier_info);
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 根据id 获取供应商信息 
	 * @param int $id
	 * @return multitype:
	 * @author liaojianwen
	 * @date 2016-11-21
	 */	
	public function getSupplierInfo($id)
	{
		if(empty($id)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
		}
		$fields = [
			'supplier_id',
			'supplier_name',
			'supplier_desc',
			'handle_man',
			'phone',
			'type',
			'bank',
			'bank_account',
			'account_name',
		];
		$conditions = "supplier_id =:id";
		$params = [
			':id'=>$id
		];
		$result['list'] = SupplierDAO::getInstance()->iselect($fields, $conditions, $params, 'one');
		$img_field = "supplier_img";
		$img_columns = "supplier_id = :supid and delete_flag = :flag";
		$img_params = [
				':supid' => $id,
				':flag' => EnumOther::NO_DELETE,
		];
		$result['imgs'] = SupplierImageDAO::getInstance()->iselect($img_field, $img_columns, $img_params);
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
		
	}
	
	/**
	 * @desc 删除供应商
	 * @params string $ids //1,2,3,4...
	 * @author liaojianwen
	 * @date 2016-11-22
	 */
	public function delSupplier($ids)
	{
		if(empty($ids)){
			$this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
		}
		$tr = Yii::$app->db->beginTransaction();
		try{
			$_ids = explode(',',$ids);
			$columns = [
				'delete_flag'=>EnumOther::DELETED,
				'modify_time'=>time(),
			];
			foreach ($_ids as $id){
				$conditions ="supplier_id=:id";
				$params=[
					':id'=>$id,
				];
				$result = SupplierDAO::getInstance()->iupdate($columns, $conditions, $params);
				if(empty($result)){
					$tr->rollBack();
				}
			}
			$tr->commit();
			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
		}catch (\Exception $e){
			$tr->rollBack();
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','return false');
		}
	}
	
	/**
	 * @desc 获取供应商下拉列表
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function getSupplierList($is_selected = 0)
	{
		$fields = [
			'supplier_id',
			'supplier_name',
		];
		$conditions = "delete_flag=:flag and type =:type";
		$params = [
			':flag'=>EnumOther::NO_DELETE,
			':type'=>EnumOther::SUPPLIER
		];
		$result = SupplierDAO::getInstance()->iselect($fields, $conditions, $params);
		if($is_selected){
			foreach($result as &$res){
				if($res['supplier_id'] == $is_selected){
					$res['selected'] = true;
				}
			}
		}
		if(empty($result)){
			return [];
		}
		return $result;
	}
	
	/**
	 * @desc 保存供应商附件
	 * @author liaojianwen
	 * @date 2017-06-30
	 */
	public function saveImg($imgs, $removeImg, $supid)
	{
		if(!$supid){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'supid is  empty');
		}
		$tr = Yii::$app->db->beginTransaction();
		try{
			foreach ($imgs as $img){
				$fields = 'image_id';
				$conditions ="supplier_img =:img";
				$params =[
					':img' =>$img
				];
				$res = SupplierImageDAO::getInstance()->iselect($fields, $conditions, $params);
				if($res){
					continue;
				}
				$columns =[
					'supplier_id' => $supid,
					'supplier_img' => $img
				];
				$res_save = SupplierImageDAO::getInstance()->iinsert($columns);
				if(!$res_save){
					$tr->rollBack();
					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'save img fail');
				}
			}
			
			foreach($removeImg as $rmimg){
				$del_columns = [
					'delete_flag' =>EnumOther::DELETED,	
				];
				$del_cond = "supplier_img = :del_img";
				$del_params = [
					':del_img' => $rmimg
				];
				$res_del = SupplierImageDAO::getInstance()->iupdate($del_columns, $del_cond, $del_params);
				if(!$res_del){
					$tr->rollBack();
					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'del img fail');
				}
			}
			$tr->commit();
			return $this->handleApiFormat(EnumOther::ACK_SUCCESS);
		} catch (\Exception $e){
			$tr->rollBack();
			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
		}
	}
	
	/**
	 * @desc 根据供应商获取供应商的账户信息
	 * @author liaojianwen
	 * @date 2017-07-06
	 */
	public function getAccountByVendor($vendor)
	{
		if(!$vendor){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'', 'vendor id can not be null');
		}
		$fields = ['account_name','bank','bank_account'];
		$conditions = "supplier_id = :id";
		$params =[
				':id'=>$vendor
		];
		
		$result = SupplierDAO::getInstance()->iselect($fields, $conditions, $params, 'one');
		if(!$result){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
		}
		
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
	}
}