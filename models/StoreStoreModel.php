<?php
/**
 * @desc   门店列表模型
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\models;
use Yii;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\StoreStoreDAO;

class StoreStoreModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return ListStoreStoreModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStoreList($pageInfo,$searchData,$filter)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$storeList = StoreStoreDAO::getInstance()->getStoreListData($pageInfo,$searchData,$filter);

		if (empty ( $storeList )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $storeList );
	}

	/**
	 * @desc   保存添加/编辑门店
	 * @param  $data  array 门店数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStore($data)
	{
	    if(empty($data['store_name'])){
		   return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','store_name is null');
		}
		//检查是否有重名
        $result = StoreStoreDAO::getInstance()->checkStore($data['store_id'],$data['store_name']);	
		if($result){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','store_name is exists');
		}
        $result = StoreStoreDAO::getInstance()->saveStoreData($data);				
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}	
}

