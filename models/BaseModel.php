<?php
namespace  app\models;
/**
 * @desc table model base class
 * @author liaojianwen
 * @date 2016-10-28
 */
use app\enum\EnumOther;
abstract class BaseModel
{
	private static $_models = array();  // class name => model

	/**
	 * @desc Returns the static model of the specified table model class.
	 * @author liaojianwen
	 * @param string $className table model class name.
	 * @return BaseModel
	 * @date 2016-10-28
	 */
	public static function model($className = __CLASS__)
	{	
		if(isset(self::$_models[$className])){
			return self::$_models[$className];
		} else{
			$model = self::$_models[$className] = new $className(null);
			return $model;
		}
	}
	
    /**
     * @desc API格式处理,列表格式:'Body'=>array('list'=>'','count'=>'','page'=>array('page'=>'','pageSize'=>'')), 内容格式:'Body'=>array('content'=>'')
     * @param string $ack 状态值;Success:成功;Failure:失败
     * @param array $body 数据主体(除Ack为Failure外)
     * @param string $error 错误信息
     * @author liaojianwen
     * @date 2016-10-28
     * @return array 结果
     */
    public function handleApiFormat($ack, $body = array(), $error = '')
    {
        if ($ack != EnumOther::ACK_SUCCESS && $ack != EnumOther::ACK_FAILURE && $ack != EnumOther::ACK_WARNING) {
            return false;
        }
        $apiResult = array();
        $apiResult['Ack'] = $ack;
        if ($ack === EnumOther::ACK_FAILURE) {
            $apiResult['Error'] = $error;
            $apiResult['Msg'] = $body;
        } elseif ($ack === EnumOther::ACK_WARNING) {
            $apiResult['Body'] = $body;
            $apiResult['Error'] = $error;
        } else {
            $apiResult['Body'] = $body;
        }
        $apiResult['GmtTimeStamp'] = gmdate(DATE_ISO8601);
        $apiResult['LocalTimeStamp'] = date(DATE_ISO8601);
        return $apiResult;
    }
    
    public function _genExcelFont(&$objActSheet, $row, $styleRow, $fontText = ' ', $meger = false)
    {
    	$objActSheet->setCellValue($row, $fontText);
    
    	if ($meger){
    		$objActSheet->mergeCells($meger);
    	}
    	$styleThinBlackBorderOutline = array(
    			'borders' => array(
    					'outline' => array(
    							'style' => \PHPExcel_Style_Border::BORDER_THIN
    					) // 设置border样式 'color' => array ('argb' => 'FF000000'), //设置border颜色
    
    			)
    	);
    	$objActSheet->getStyle( $styleRow)->applyFromArray($styleThinBlackBorderOutline);
    }
    
    /**
     * @desc 数字转成中文数字
     * @author liaojianwen
     * @date 2017-07-11
     */
    public function NumToCNMoney($num,$mode = true,$sim = true){
    	if(!is_numeric($num)) return '含有非数字非小数点字符！';
    	$char    = $sim ? array('零','一','二','三','四','五','六','七','八','九')
    	: array('零','壹','贰','叁','肆','伍','陆','柒','捌','玖');
    	$unit    = $sim ? array('','十','百','千','','万','亿','兆')
    	: array('','拾','佰','仟','','萬','億','兆');
    	$retval  = $mode ? '元':'点';
    	//小数部分
    	if(strpos($num, '.')){
    		list($num,$dec) = explode('.', $num);
    		$dec = strval(round($dec,2));
    		if($mode){
    			$retval .= "{$char[$dec['0']]}角{$char[$dec['1']]}分";
    		}else{
    			for($i = 0,$c = strlen($dec);$i < $c;$i++) {
    				$retval .= $char[$dec[$i]];
    			}
    		}
    	}
    	//整数部分
    	$str = $mode ? strrev(intval($num)) : strrev($num);
    	for($i = 0,$c = strlen($str);$i < $c;$i++) {
    		$out[$i] = $char[$str[$i]];
    		if($mode){
    			$out[$i] .= $str[$i] != '0'? $unit[$i%4] : '';
    			if($i>1 and $str[$i]+$str[$i-1] == 0){
    				$out[$i] = '';
    			}
    			if($i%4 == 0){
    				$out[$i] .= $unit[4+floor($i/4)];
    			}
    		}
    	}
    	$retval = join('',array_reverse($out)) . $retval;
    	return $retval;
    }
}