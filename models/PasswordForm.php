<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\AdminModel;
use app\enum\EnumOther;
use app\models\BaseModel;


/**
 * Login form
 */
class PasswordForm extends Model
{
    public $adminname;
    public $email;
    public $oldpassword;
	public $newpassword;
	public $repassword;
	public $id;


	public function __construct($data){
		$this->adminname = isset($data['adminname']) ? $data['adminname'] :'';
		$this->email = isset($data['email']) ? $data['email'] :'';
		$this->oldpassword = isset($data['oldpassword']) ? $data['oldpassword']:'';
		$this->newpassword = isset($data['newpassword'])? $data['newpassword']:'';
		$this->repassword = isset($data['repassword']) ? $data['repassword'] : '';
		$this->id = isset($data['id']) ? $data['id'] :'';
	}
	
	/**
	 * @desc 更改个人信息
	 * @author liaojianwen
	 * @date 2016-11-15
	 */
	public function changeInfo()
	{
		$this->validate();
// 		$id = Yii::$app->user->id;
		$id = $this->id;
		$admin=  AdminModel::findIdentity($id);
		$password = $admin->password_hash;
		if($this->adminname == $admin->username){
		} else{
			$admin->username = $this->adminname;//更改用户名
		}
		if($this->email == $admin->email){
		} else{
			$admin->email = $this->email;//更改用户名
		}
		if(!empty($this->oldpassword)){
			//有填写旧密码 即更改密码
			if (Yii::$app->getSecurity ()->validatePassword ( $this->oldpassword, $password )) {
				if ($this->newpassword == $this->repassword) {
					$newPass = Yii::$app->security->generatePasswordHash ( $this->newpassword );
					$admin->password_reset_token = '';
					$admin->password_hash = $newPass;
				} else {
					return handleApiFormat(EnumOther::ACK_FAILURE,'','newpassword do not eq to repassword');
				}
			} else {
				return handleApiFormat(EnumOther::ACK_FAILURE,'','oldpassword is not correct');
			}
		}
		if($admin->save()){
			return handleApiFormat(EnumOther::ACK_SUCCESS,true);
		}else{
			return handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
	}

}
