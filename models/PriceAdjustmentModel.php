<?php
 namespace app\models;
 
 use app\models\BaseModel;
 use Yii;
 use app\enum\EnumOther;
 use app\dao\ProductDAO;
 use app\dao\PriceAdjustmentDAO;
 use app\dao\PriceAdjustMentDetailDAO;
				 
 class PriceAdjustmentModel extends BaseModel
 {
 	/**
 	 * @desc覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-07-11
 	 * @return PriceAdjustmentModel
 	 */
 	public static function model($className = __CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	
 	/**
 	 * @desc 查询商品信息
 	 * @param string $name
 	 * @param [] $pageInfo
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getProducts($pageInfo, $name)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','params is empty');
 		}
 		$result = ProductDAO::getInstance()->getProducts($pageInfo, $name);
 		if(empty ( $result )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	/**
 	 * @desc 保存成本调整单
 	 * @param [] $head 调整单头
 	 * @param [] $detail 调整单明细
 	 * @param $id //调整单id //有就是编辑,没有就是新增
 	 * @param $remove 删除的明细
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function saveAdjustment($head, $detail,$remove,$id)
 	{
 		if(empty($head)){
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'head is empty' );
 		}
 		if (empty ( $detail )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'detail is empty' );
 		}
 	
 		//清掉为空的元素
 		foreach ($head as $k =>$hed){
 			if(empty($hed)){
 				unset($head[$k]);
 			}
 		}
 		foreach ($detail as $k=> $det){
 			foreach ($det as $j => $deet){
 				if(empty($deet)){
 					unset($detail[$k][$j]);
 				}
 			}
 		}
 		$tr = Yii::$app->db->beginTransaction();
 		try{
	 		// 表头信息
	 		$head ['adjustment_date'] = strtotime ( $head ['adjustment_date'] );
	 		$cond_head = "adjustment_id =:id";
	 		$param_head = [
	 				':id' => $id
	 		];
	 		$Iid = PriceAdjustmentDAO::getInstance ()->ireplaceinto ( $head, $cond_head, $param_head, true );
	 		if (! $Iid) {
	 			$tr->rollBack();
	 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save adjustment_head failure' );
	 		}
	 		// 表单明细
	 		//删除的明细
	 		foreach ($remove as $move){
	 			$res_remove = PriceAdjustMentDetailDAO::getInstance()->iupdate(['delete_flag'=>EnumOther::DELETED], "adjustment_det_id = :det_id", [':det_id'=>$move['adjustment_det_id']]);
	 			if(!$res_remove){
	 				$tr->rollBack();
	 				$this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'remove adjustment_det_id:'+$move['adjustment_det_id'] +'failed');
	 			}
	 		}
	 		foreach ( $detail as &$det ) {
	 			$det ['adjustment_id'] = $Iid;
	 			$det ['delete_flag'] = EnumOther::NO_DELETE;
	 			$cond_det = "adjustment_id = :id and adjustment_det_id = :lid";
	 			$param_det = [
	 					':id' => $Iid,
	 					':lid' => isset($det ['adjustment_det_id'])?$det['adjustment_det_id']:0,
	 			];
	 	
	 			$res_det = PriceAdjustMentDetailDAO::getInstance ()->ireplaceinto ( $det, $cond_det, $param_det, true );
	 			if (! $res_det) {
	 				$tr->rollBack();
	 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save adjustment_detail failure' );
	 			}
	 		}
	 		
	 		$tr->commit();
	 		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, '' );
 		} catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 获取成本调整单列表
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustments ($condition,$pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$result = PriceAdjustmentDAO::getInstance()->getAdjustments($condition, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	
 	/**
 	 * @desc 获取成本调整单明细（列表）
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustmentDetail($id)
 	{
 		if (! $id) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'param is empty' );
 		}
 		$fields = [
 				'adjustment_det_id',
 				'adjustment_id',
 				'd.product_id',
 				'p.product_name',
 				'd.quantity',
 				'd.quantity_unit',
 				'u.unit_name',
 				'd.price',
 				'd.adjustment_price',
 				'd.remark',
 		];
 			
 		$joinArr = [
 				[
 						"product p",
 						"p.product_id = d.product_id",
 						'left' => '',
 				],
 				[
 						"unit u",
 						"u.unit_id = d.quantity_unit",
 						'left' => '',
 				],
 					
 		];
 		$conditions = "d.adjustment_id = :id and d.delete_flag = :dflag";
 		$params = [
 				':id' => $id,
 				':dflag' => EnumOther::NO_DELETE,
 		];
 		$result = PriceAdjustMentDetailDAO::getInstance()->iselect($fields, $conditions, $params, 'all',"adjustment_det_id ASC", $joinArr,'d');
 			
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 获取成本调整编辑页面信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function getAdjustmentInfo($id)
 	{
 		if(!$id){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id can not be null');
 		}
 			
 		$in_head = PriceAdjustmentDAO::getInstance()->getAdjustmentHead($id);
 		if(empty($in_head)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','head data found');
 		}
 		$in_det = PriceAdjustMentDetailDAO::getInstance()->getAdjustmentDetail($id);
 		if(empty($in_det)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','detail data found');
 		}
 		$result['head'] = $in_head;
 		$result['det'] = $in_det;
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	
 	/**
 	 * @desc 确认入库单
 	 * @author liaojianwen
 	 * @date 2016-11-25
 	 */
 	public function confirmAdjustment($id)
 	{
 		if (! $id) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'param is empty' );
 		}
 		$id = (int)$id;
 		$tr = Yii::$app->db->beginTransaction ();
 		try {
 			$re_confirm = PriceAdjustmentDAO::getInstance ()->updateByPk ( $id, [
 					'confirm_flag' => EnumOther::CONFIRM
 			] );
 			if (! $re_confirm) {
 				$tr->rollBack ();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'confirm fail' );
 			}
 			$fields = [
 					'product_id',
 					'adjustment_price',
 			];
 			$conditions = "adjustment_id =:id and delete_flag = :flag";
 			$params = [
 					':id' => $id,
 					':flag' => EnumOther::NO_DELETE
 			];
 			$adjustmt_det = PriceAdjustMentDetailDAO::getInstance ()->iselect ( $fields, $conditions, $params, 'all');
 			if (! $adjustmt_det) {
 				$tr->rollBack ();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'select det fail' );
 			}
 			// 调整单明细
 			foreach ( $adjustmt_det as $det ) {
 				$columns = [
 					'price' => $det['adjustment_price']
 				];
 				$conditions ="product_id = :pid";
 				$params = [
 						':pid' => $det['product_id'],
 				];
 				$res_product = ProductDAO::getInstance()->iupdate($columns, $conditions, $params);
 				
 				if(!$res_product){
 					$tr->rollBack();
 					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'update product price fail');
 				}
 			}
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
 		} catch ( \Exception $e ) {
 			$tr->rollBack ();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 获取成本调整单查询列表
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function getAdjustmentSearch ($condition,$pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$result = PriceAdjustMentDetailDAO::getInstance()->getAdjustmentSearch($condition, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 }
 