<?php

namespace app\models;

use Yii;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\EmployeeDAO;
use app\enum\EnumOriginType;
use app\dao\DepartmentDAO;

class EmployeeModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author liaojianwen
	 * @date 2016-11-23
	 * @return EmployeeModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @desc 获取部门信息
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function getDeparts()
	{
		$fields = ['depart_id','depart_name'];
		$conditions= "delete_flag = :flag";
		$params = [
			':flag'=>EnumOther::NO_DELETE
		];
		$result = DepartmentDAO::getInstance()->iselect($fields, $conditions, $params);
		return $result;
	}
	
	/**
	 * @desc 获取员工列表数据
	 * @param [] $pageInfo 
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function getEmployee($pageInfo)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'params is empty');
		}
		$result = EmployeeDAO::getInstance()->getEmployee($pageInfo);
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
	}
	
	/**
	 * @desc 保存员工
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function saveEmployee ($name, $desc,$depart,$phone,$id)
	{
		if(empty($name)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
		}
		if(empty($id)){
			$id = 0;
		}
		$emp_info = EmployeeDAO::getInstance()->checkSameName($name, $id);
		if($emp_info){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','employee is exists');
		}
		
		$columns = array(
				'employee_name' => $name,
				'depart_id' => $depart,
				'phone'=>$phone,
				'remark' => $desc,
		);
		foreach($columns as $key=> $column){
			if(empty($column)){
				unset($columns[$key]);
			}
		}
		if($id == 0){
			$columns['create_time'] = time();
		} else {
			$columns['modify_time'] = time();
		}
		$conditions = "employee_id = :id and delete_flag = :flag ";
		$params = array(
				':id' => $id,
				':flag' => EnumOther::NO_DELETE,
		);
		$res = EmployeeDAO::getInstance()->iselect(array('employee_id'),$conditions,$params,'one');
		if(empty($res)){
			//新增
			$result = EmployeeDAO::getInstance()->iinsert($columns);
		} else {
			//更新
			$result = EmployeeDAO::getInstance()->updateByPk($id, $columns);
		}
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 删除员工
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function delEmployee($ids)
	{
		if(empty($ids)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
		}
		$_ids = explode(',', $ids);
		$params =[
			'delete_flag'=>EnumOther::DELETED,
		];
		foreach ($_ids as $id){
			$result = EmployeeDAO::getInstance()->updateByPk($id, $params);
		}
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del fail');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc 员工列表
	 * @param [] $pageInfo
	 * @author  liaojianwen
	 * @date 2016-11-24
	 */
	public function listEmployee()
	{
		$fields = [
				'employee_id',
				'employee_name',
		];
		$conditions ="delete_flag = :flag";
		$params = [
				':flag'=>EnumOther::NO_DELETE,
		];
		$result = EmployeeDAO::getInstance()->iselect($fields, $conditions, $params);
		return $result;
	}
	

}
