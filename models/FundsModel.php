<?php
 namespace app\models;
 
 use app\models\BaseModel;
 use Yii;
 use app\dao\PayablesDAO; 
 use app\enum\EnumOther;
use app\dao\SettlePayablesDetailDAO;
use app\dao\SettlePayablesDAO;
use app\enum\EnumOriginType;
use app\dao\PurchaseDAO;
use app\dao\VendorPaymentDAO;
use app\dao\VendorPaymentDetailDAO;
use app\dao\PurchaseDetailDAO;
use app\dao\VendorPaymentSummaryDAO;
									 
 class FundsModel extends BaseModel 
 {
 	/**
 	 * @desc 覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2017-03-01
 	 * @return FundsModel
 	 */
 	public static function model($className=__CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	/**
 	 * @desc 获取应付账单列表
 	 * @author liaojianwen
 	 * @date 2017-03-01
 	 */
 	public function getPayables($condition, $filter, $pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$result = PayablesDAO::getInstance()->getPayables($condition, $filter, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	
 	/**
 	 * @desc 根据往来单位获取应付账单
 	 * @author liaojianwen
 	 * @date 2017-03-02
 	 */
 	public function getPayableByVendor($vendor, $pageInfo, $condition)
 	{
 		if(empty($vendor)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor can not be null');
 		}
 		$result = PayablesDAO::getInstance()->getPayablesByVendor($vendor, $pageInfo, $condition);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 保存采购入库结算单
 	 * @author liaojianwen
 	 * @date 2017-03-03
 	 */
 	public function saveSettlePayables ( $head, $detail, $remove, $id )
 	{
 		if(empty($head)){
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'head is empty' );
 		}
 		if (empty ( $detail )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'detail is empty' );
 		}
 		
 		//清掉为空的元素
 		foreach ($head as $k =>$hed){
 			if(empty($hed)){
 				unset($head[$k]);
 			}
 		}
 		foreach ($detail as $k=> $det){
 			foreach ($det as $j => $deet){
 				if(empty($deet)){
 					unset($detail[$k][$j]);
 				}
 			}
 		}
 		// 表头信息
 		$head ['settle_pay_date'] = strtotime ( $head ['settle_pay_date'] );
 		$cond_head = "settle_pay_id =:id";
 		$param_head = [
 				':id' => $id
 		];
 		$Iid = SettlePayablesDAO::getInstance ()->ireplaceinto ( $head, $cond_head, $param_head, true );
 		if (! $Iid) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save settle_payables_head failure' );
 		}
 		// 表单明细
 		// 		InstoreDetailDAO::getInstance()->iupdate(['delete_flag' => EnumOther::DELETED], "instore_id =:did", [':did'=>$Iid]);// 先删除后没有删除的数据更新回来
 		//删除的明细
 		foreach ($remove as $move){
 			$res_remove = SettlePayablesDetailDAO::getInstance()->iupdate(['delete_flag'=>EnumOther::DELETED], "settle_pay_det_id = :det_id", [':det_id'=>$move['settle_pay_det_id']]);
 			if(!$res_remove){
 				$tr->rollBack();
 				$this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'remove settle_pay_det_id:'+$move['settle_pay_det_id'] +'failed');
 			}
 		}
 		foreach ( $detail as &$det ) {
 			$det ['settle_pay_id'] = $Iid;
 			$det ['delete_flag'] = EnumOther::NO_DELETE;
 			$cond_det = "settle_pay_id = :id and settle_pay_det_id = :lid";
 			$param_det = [
 					':id' => $Iid,
 					':lid' => isset($det ['settle_pay_det_id'])?$det['settle_pay_det_id']:0,
 			];
 		
 			$res_det = SettlePayablesDetailDAO::getInstance ()->ireplaceinto ( $det, $cond_det, $param_det, true );
 			if (! $res_det) {
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save settle_payables_detail failure' );
 			}
 		}
 		
 		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, '' );
 	}
 	
 	
 	/**
 	 * @desc 获取应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function getSettlesPayables ($condition, $filter,$pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$result = SettlePayablesDAO::getInstance()->getSettlePayables($condition, $filter, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	/**
 	 * @desc 获取应付结算单明细（编辑）
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function getSettlePayablesInfo($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$in_head = SettlePayablesDAO::getInstance()->getSettlePayablesHead($id);
 		if(empty($in_head)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','head data found');
 		}
 		$in_det = SettlePayablesDetailDAO::getInstance()->getSettlePayablesDet($id);
 		if(empty($in_det)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','detail data found');
 		}
 		$result['head'] = $in_head;
 		$result['det'] = $in_det;
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	/**
 	 * @desc 检查应付单是否被引用
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function checkPayablesById($id)
 	{
 		if(!$id){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'',' id can not be null');
 		}
 		$fields = "pay_id";
 		$conditions = "d.pay_id = :pid and h.confirm_flag = :cflag and d.delete_flag = :flag";
 		$params =[
 			':pid'=>$id,
 			':cflag' => EnumOther::NO_CONFIRM,
 			':flag' => EnumOther::NO_DELETE,
 		];
		$joinArray = [ 
				[ 
						'settle_payables h',
						'd.settle_pay_id = h.settle_pay_id' 
				] ,
		];
 		$result = SettlePayablesDetailDAO::getInstance()->iselect($fields, $conditions, $params,'all','pay_id ASC',$joinArray,'d');
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return  $this->handleApiFormat(EnumOther::ACK_SUCCESS, '');
 		
 	}
 	
 	
 	/**
 	 * @desc 确认应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function confirmSettlePayables($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id is null');
 		}
 		$tr = Yii::$app->db->beginTransaction ();
 		try {
 			$re_confirm = SettlePayablesDAO::getInstance ()->updateByPk ( $id, [
 					'confirm_flag' => EnumOther::CONFIRM
 			] );
 			if (! $re_confirm) {
 				$tr->rollBack ();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'confirm fail' );
 			}
 			$fields = [
 				'pay_id',
 				'current_amount',
 				'left_amount',
 			];
 			$conditions = "settle_pay_id = :pid and delete_flag = :flag";
 			$params = [
 				':pid'=>$id,
 				':flag'=> EnumOther::NO_DELETE,
 			];
 			$res_det = SettlePayablesDetailDAO::getInstance()->iselect($fields, $conditions, $params);
 			if (! $res_det) {
 				$tr->rollBack ();
				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'select det fail' );
			}
 			
 			foreach($res_det as $det){
				$res_count = PayablesDAO::getInstance ()->updateAllCounters ( [ 
						'receive_amount' => $det ['current_amount'],
						'left_amount' => - $det ['current_amount'] 
				], "pay_id = :pid", [ 
						':pid' => $det ['pay_id'] 
				] );
 				if(!$res_count){
 					$tr->rollBack();
 					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '','update payable amount fail');
 				}
				
 				$condition = "pay_id = :ppid";
 				$param = [
 					':ppid' => $det['pay_id'],
 				];
 				$res_flag = '';
 				if($det['left_amount'] == $det['current_amount']){
					$columns = [ 
							'finish_flag' => EnumOther::SETTLE,
					];
 					$res_flag = PayablesDAO::getInstance()->iupdate($columns, $condition, $param);

 				} else {
 					$columns = [
 							'finish_flag' => EnumOther::PART_SETTLE,
 					];
 					$res_flag = PayablesDAO::getInstance()->iupdate($columns, $condition, $param);
 				}
 				if(!$res_flag){
 					$tr->rollBack();
 					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '','');
 				}
 			}
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS, '');
 			
 		} catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '',$e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 获取应付结算单明细
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function getSettlePayablesDet($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','id is null');
 		}
 		$result = SettlePayablesDetailDAO::getInstance()->getSettlePayablesDet($id);
 		if(!$result){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 删除应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function deleteSettlePayables($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id is null');
 		}
 		$tr= Yii::$app->db->beginTransaction();
 		try{
 			$res_head = SettlePayablesDAO::getInstance ()->updateByPk ( $id, ['delete_flag' => EnumOther::DELETED] );
 			if(!$res_head){
 				$tr->rollBack();
 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'del settle_payables fail');
 			}
 			//settle_payables_detail
 			$res_det = SettlePayablesDetailDAO::getInstance()->iupdate(['delete_flag'=>EnumOther::DELETED], "settle_pay_id=:id", [':id'=>$id]);
 			if(!$res_det){
 				$tr->rollBack();
 				return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del_settle_payables_det fail');
 			}
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
 		} catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'',$e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 应付账单单据红冲
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function invalidSettlePayables($id)
 	{
 		if(empty($id)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id is null');
 		}
 		$tr = Yii::$app->db->beginTransaction();
 		try{
 			$re_confirm = SettlePayablesDAO::getInstance ()->updateByPk ( $id, [
 					'confirm_flag' => EnumOther::NO_CONFIRM
 			] );
 			if (! $re_confirm) {
 				$tr->rollBack ();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'confirm fail' );
 			}
 			
 		 	$res_head = SettlePayablesDAO::getInstance ()->updateByPk ( $id, ['delete_flag' => EnumOther::INVALID] );
 			if(!$res_head){
 				$tr->rollBack();
 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'invalid settle_payables fail');
 			}
 			
 			$fields = [
 					'settle_pay_det_id',
 					'pay_id',
 					'current_amount',
 					'left_amount',
 					'pay_amount',
 					'settled_amount'
 			];
 			$conditions = "settle_pay_id = :pid and delete_flag = :flag";
 			$params = [
 					':pid'=>$id,
 					':flag'=> EnumOther::NO_DELETE,
 			];
 			$res_det = SettlePayablesDetailDAO::getInstance()->iselect($fields, $conditions, $params);
 			if (! $res_det) {
 				$tr->rollBack ();
 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'select det fail' );
 			}
 			
 			foreach($res_det as $det){
 				$res_count = PayablesDAO::getInstance ()->updateAllCounters ( [
 						'receive_amount' => -$det ['current_amount'],
 						'left_amount' => $det ['current_amount']
 				], "pay_id = :pid", [
 						':pid' => $det ['pay_id']
 				] );
 				if(!$res_count){
 					$tr->rollBack();
 					return $this->handleApiFormat(EnumOther::ACK_FAILURE, '','update payable amount fail');
 				}
 				$columns = [
 					'pay_amount',
 					'settle_amount',
 					'receive_amount',
 					'left_amount'
 				];
 				
 				$condition = "pay_id = :ppid";
 				$param = [
 					':ppid'=> $det['pay_id']
 				];
 				$ddet = PayablesDAO::getInstance()->iselect($columns, $condition, $param, 'one');
				if ($ddet ['receive_amount'] > 0) {
					$columns = [ 
							'finish_flag' => EnumOther::PART_SETTLE 
					];
					$res_flag = PayablesDAO::getInstance ()->iupdate ( $columns, $condition, $param );
				} else {
					$columns = [ 
							'finish_flag' => EnumOther::NO_SETTELE 
					];
					$res_flag = PayablesDAO::getInstance ()->iupdate ( $columns, $condition, $param );
				}
				if (! $res_flag) {
					$tr->rollBack ();
					return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', '' );
				}
				
				
					
//  				$condition = "pay_id = :ppid";
//  				$param = [
//  						':ppid' => $det['pay_id'],
//  				];
//  				$res_flag = '';
//  				if($det['current_amount'] == $det['pay_amount']){
//  					$columns = [
//  							'finish_flag' => EnumOther::NO_SETTELE,
//  					];
//  					$res_flag = PayablesDAO::getInstance()->iupdate($columns, $condition, $param);
 			
//  				} else {
//  					$columns = [
//  							'finish_flag' => EnumOther::PART_SETTLE,
//  					];
//  					$res_flag = PayablesDAO::getInstance()->iupdate($columns, $condition, $param );
// 				}
// 				if (! $res_flag) {
// 					$tr->rollBack ();
// 					return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', '' );
// 				}
					
				// settle_payables_detail
				$res_detail = SettlePayablesDetailDAO::getInstance ()->iupdate ( [ 
						'delete_flag' => EnumOther::INVALID 
				], "settle_pay_det_id=:id", [ 
						':id' => $det['settle_pay_det_id'],
				] );
				if (! $res_detail) {
					$tr->rollBack ();
					return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'invalid_settle_payables_det fail' );
				}
 			}
 			
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS, '');
 		} catch(\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
 		}
 	}
 	
 	
 	/**
 	 * @desc 根据供应商获取采购入库明细
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function getPurchaseByVendor($vendor, $condition)
 	{
 		if(empty($vendor)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor can not be null');
 		}
 		$result = PurchaseDAO::getInstance()->getPurchaseByVendor($vendor, $condition);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 保存供应商货款申请单
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function saveVendorPayment ( $head, $detail, $remove, $id, $sumtime)
 	{
 		if(empty($head)){
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'head is empty' );
 		}
 		if (empty ( $detail )) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'detail is empty' );
 		}
 			
 		//清掉为空的元素
 		foreach ($head as $k =>$hed){
 			if(empty($hed)){
 				unset($head[$k]);
 			}
 		}
 		foreach ($detail as $k=> $det){
 			foreach ($det as $j => $deet){
 				if(empty($deet)){
 					unset($detail[$k][$j]);
 				}
 			}
 		}
 		
 		$tr = Yii::$app->db->beginTransaction();
 		try {
	 		// 表头信息
	 		$head ['payment_date'] = strtotime ( $head ['payment_date'] );
	 		$cond_head = "vendor_payment_id =:id";
	 		$param_head = [
	 				':id' => $id
	 		];
	 		
	 		$Iid = VendorPaymentDAO::getInstance ()->ireplaceinto ( $head, $cond_head, $param_head, true );
	 		if (! $Iid) {
	 			$tr->rollBack();
	 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save vendor_payment_head failure' );
	 		}
	 		// 表单明细
	 		//删除的明细
	 		foreach ($remove as $move){
	 			$res_remove = VendorPaymentDetailDAO::getInstance()->iupdate(['delete_flag'=>EnumOther::DELETED], "vendor_payment_det_id = :det_id", [':det_id'=>$move['vendor_payment_det_id']]);
	 			if(!$res_remove){
	 				$tr->rollBack();
	 				$this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'remove vendor_payment_det_id:'+$move['vendor_payment_det_id'] +'failed');
	 			}
	 			
	 			$det_select = VendorPaymentDetailDAO::getInstance()->iselect("purchase_id,purchase_det_id", "vendor_payment_det_id = :det_id", [':det_id'=>$move['vendor_payment_det_id']],'one');
	 			if($det_select){
	 				$pay_column = [
		 					'pay_finish' => EnumOther::NO_FINISHED,
		 			];
		 			$pay_condition = "purchase_id = :id and purchase_det_id = :did";
		 			$pay_params = [
		 					':id' => $det_select['purchase_id'],
		 					':did' => $det_select['purchase_det_id'],
		 			];
		 			$pay_finish = PurchaseDetailDAO::getInstance()->iupdate($pay_column, $pay_condition, $pay_params);
		 			if(! $pay_finish){
		 				$tr->rollBack();
		 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, ' ', 'update pay_finish fail');
		 			}
	 			}
	 		}
	 		foreach ( $detail as &$det ) {
	 			$det ['vendor_payment_id'] = $Iid;
	 			$det ['purchase_time'] = strtotime ( $det ['purchase_time'] );
	 			$det ['delete_flag'] = EnumOther::NO_DELETE;
	 			$cond_det = "vendor_payment_id = :id and vendor_payment_det_id = :lid";
	 			$param_det = [
	 					':id' => $Iid,
	 					':lid' => isset($det ['vendor_payment_det_id'])?$det['vendor_payment_det_id']:0,
	 			];
	 				
	 			$res_det = VendorPaymentDetailDAO::getInstance ()->ireplaceinto ( $det, $cond_det, $param_det, true );
	 			if (! $res_det) {
	 				$tr->rollBack();
	 				return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'save vendor_payment_detail failure' );
	 			}
	 			
	 			$pay_column = [
	 					'pay_finish' => EnumOther::FINISHED,
	 			];
	 			$pay_condition = "purchase_id = :id and purchase_det_id = :did";
	 			$pay_params = [
	 					':id' => $det['purchase_id'],
	 					':did' => $det['purchase_det_id'],
	 			];
	 			$pay_finish = PurchaseDetailDAO::getInstance()->iupdate($pay_column, $pay_condition, $pay_params);
	 			if(! $pay_finish){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, ' ', 'update pay_finish fail');
	 			}
	 			
	 		}
	 		//汇总
	 		$item = [];
	 		foreach ($detail as $k=>$v){
	 			if(!isset($item[$v['product_id']])){
	 				$item[$v['product_id']] = $v;
	 			} else {
	 				if($item[$v['product_id']]['product_id'] == $v['product_id']){
	 					$item[$v['product_id']]['quantity'] += $v['quantity'];
	 					$item[$v['product_id']]['amount'] += $v['amount'];
	 				}
	 			}
	 		}
	 		//先把summary 数据全部删除
	 		$sum_select = VendorPaymentSummaryDAO::getInstance()->iselect("vendor_payment_summary_id", "vendor_payment_id=:id", [':id' => $Iid],'one');
	 		if($sum_select){
	 			$sum_del = VendorPaymentSummaryDAO::getInstance()->iupdate(['delete_flag' => EnumOther::DELETED], "vendor_payment_id =:id", [':id'=>$Iid]);
	 			if(! $sum_del){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, '','del summary failure');
	 			}
	 		}
	 		
	 		
	 		foreach ($item as $it){
	 			$columns = [
	 				'vendor_payment_id'	=> $Iid,
	 				'summary_time' => $sumtime,
	 				'product_id' => $it['product_id'],
	 				'quantity' => $it['quantity'],
	 				'quantity_unit' => $it['quantity_unit'],
	 				'amount' => $it['amount'],
	 				'price' => round(($it['amount'] /$it['quantity']),2),
	 				'delete_flag' => EnumOther::NO_DELETE
	 			];
	 			$conditions ="vendor_payment_id = :id and product_id =:pid";
	 			$params = [
	 				':id' => $Iid,
	 				':pid' => $it['product_id'],
	 			];
	 			$sum_res = VendorPaymentSummaryDAO::getInstance()->ireplaceinto($columns, $conditions, $params,true);
	 			if(!$sum_res){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, ' ', 'save summary failure');
	 			}
	 			
	 		}
	 		
	 		
	 		$tr->commit();
	 		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, '' );
 		}catch(\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', $e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 获取应付结算单
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function getVendorPayment ($condition,$pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','param is empty');
 		}
 		$result = VendorPaymentDAO::getInstance()->getVendorPayment($condition, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	/**
 	 * @desc 获取请款单明细（列表）
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function getVendorPaymentDet($id)
 	{
 		if (! $id) {
 			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'param is empty' );
 		}
 		$fields = [
 				'vendor_payment_det_id',
 				'vendor_payment_id',
 				'd.purchase_time',
 				'd.product_id',
 				'p.product_name',
 				'd.quantity',
 				'd.quantity_unit',
 				'u.unit_name',
 				'd.price',
 				'd.amount',
 				'd.remark',
 		];
 		
 		$joinArr = [
 				[
 						"product p",
 						"p.product_id = d.product_id",
 						'left' => '',
 				],
 				[
 						"unit u",
 						"u.unit_id = d.quantity_unit",
 						'left' => '',
 				],
 				
 		];
 		$conditions = "d.vendor_payment_id = :id and d.delete_flag = :dflag";
 		$params = [
 				':id' => $id,
 				':dflag' => EnumOther::NO_DELETE,
 		];
 		$result['list'] = VendorPaymentDetailDAO::getInstance()->iselect($fields, $conditions, $params, 'all',"vendor_payment_det_id ASC", $joinArr,'d');
 		
 		if(empty($result['list'])){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		$total_amount = 0;
 		foreach ($result['list'] as $key => $ret){
 			$total_amount  += $ret['amount'];
 		}
 		$result['total_amount'] = $total_amount;
 		
 		
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 获取请款单编辑页面信息
 	 * @author liaojianwen
 	 * @date 2017-07-07 
 	 */
 	public function getVendorPaymentInfo($id)
 	{
 		if(!$id){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id can not be null');
 		}
 		
 		$in_head = VendorPaymentDAO::getInstance()->getVendorPaymentHead($id);
 		if(empty($in_head)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','head data found');
 		}
 		$in_det = VendorPaymentDetailDAO::getInstance()->getVendorPaymentDetail($id);
 		if(empty($in_det)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','detail data found');
 		}
 		$summary_det = VendorPaymentSummaryDAO::getInstance()->getSummaryDetail($id);
 		if(empty($summary_det)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','detail data found');
 		}
 		
 		foreach ($summary_det as &$det){
 			$starTime = strtotime(substr($det['summary_time'], 0,10));
 			$endTime = strtotime(substr($det['summary_time'], 11));
 			$det['time'] = date('n月d日',$starTime) .'-'.date('n月d日',$endTime);
 		}
 		$result['head'] = $in_head;
 		$result['det'] = $in_det;
 		$result['sum'] = $summary_det;
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,$result);
 	}
 	
 	
 	/**
 	 * @desc 删除请款单
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function deleteVendorPayment($ids)
 	{
 		if(!$ids){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'ids can not be null');
 		}
 		
 		$tr= Yii::$app->db->beginTransaction();
 		try{
 			$_ids = explode ( ',', $ids );
 			foreach ($_ids as $id){
	 			$res_head = VendorPaymentDAO::getInstance ()->updateByPk ( $id, ['delete_flag' => EnumOther::DELETED] );
	 			if(!$res_head){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'del vendor_payment fail');
	 			}
	 			
	 			//更新purchase_detail pay_finish 
	 			$fields = "purchase_id, purchase_det_id";
	 			$conditions = "vendor_payment_id = :id and delete_flag =:flag";
	 			$params = [
	 					':id' => $id,
	 					':flag' => EnumOther::NO_DELETE,
	 			];
	 			$det_select = VendorPaymentDetailDAO::getInstance()->iselect($fields, $conditions, $params,'all');
	 			//vendor_payment_detail
	 			$res_det = VendorPaymentDetailDAO::getInstance()->iupdate(['delete_flag'=>EnumOther::DELETED], "vendor_payment_id=:id", [':id'=>$id]);
	 			if(!$res_det){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del_vendor_payment_detail fail');
	 			}
	 			//vendor_payment_summary
	 			$sum_del = VendorPaymentSummaryDAO::getInstance()->iupdate(['delete_flag' => EnumOther::DELETED], "vendor_payment_id =:id", [':id'=>$id]);
	 			if(!$sum_del){
	 				$tr->rollBack();
	 				return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','del_vendor_payment_summary fail');
	 			}
	 			foreach ($det_select as $det){
		 			$pay_column = [
		 					'pay_finish' => EnumOther::NO_FINISHED,
		 			];
		 			$pay_condition = "purchase_id = :id and purchase_det_id = :did";
		 			$pay_params = [
		 					':id' => $det['purchase_id'],
		 					':did' => $det['purchase_det_id'],
		 			];
		 			$pay_finish = PurchaseDetailDAO::getInstance()->iupdate($pay_column, $pay_condition, $pay_params);
		 			if(! $pay_finish){
		 				$tr->rollBack();
		 				return $this->handleApiFormat(EnumOther::ACK_FAILURE, ' ', 'update pay_finish fail');
		 			}
	 			}
 			}
 			$tr->commit();
 			return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
 		} catch (\Exception $e){
 			$tr->rollBack();
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'',$e->getMessage());
 		}
 	}
 	
 	/**
 	 * @desc 根据id 获取请款单
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function getVendorPaymentList($id)
 	{
 		if(!$id){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id can not be null');
 		}
 		$res_head = VendorPaymentDAO::getInstance()->getVendorPaymentHead($id);
 		if(! $res_head){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor_payment_head can not be found');
 		}
 		$res_det = VendorPaymentDetailDAO::getInstance()->getVendorPaymentDetail($id);
 		if(! $res_det){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor_payment_detail can not be found');
 		}
 		
 		$result['head'] = $res_head;
 		$result['detail'] = $res_det;
 		
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	
 	/**
 	 * @desc 导出采购统计（供应商）
 	 * @author liaojianwen
 	 * @date 2017-03-10
 	 */
 	public function explodeVendorPayment($paymentInfo, $filename, $title, $excVer = "xls")
 	{
 		$data = $paymentInfo;
 		$head = $paymentInfo['head'];
 		$detail = $paymentInfo['detail'];
//  		dd($detail);
 		$objPHPExecl = new \PHPExcel();
 		$objSheet = $objPHPExecl->getActiveSheet();
 		$objWriter = NULL;
 		// 创建文件格式写入对象实例
 		switch ($excVer)
		{
			case 'xlsx' :
				$objWriter = new \PHPExcel_Writer_Excel2007 ( $objPHPExecl );
				break;
			case 'xls' :
				$objWriter = new \PHPExcel_Writer_Excel5 ( $objPHPExecl );
				break;
			default:
 				$objWriter = new \PHPExcel_Writer_Excel5($objPHPExecl);
 		}
			// 设置宽度
		$objSheet->mergeCells ( "A1:H1" );
		$objSheet->setCellValue ( "A1", $title );
		$objSheet->getStyle ( 'A1' )->getFont ()->setSize ( 20 );
		$objSheet->getStyle ( 'A1' )->getFont ()->setBold ( true );
		$objSheet->getStyle ( 'A1' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
		
		
		$objSheet->mergeCells("A2:H2")->setCellValue("A2", "请款日期: ".date("Y-m-d",$head['payment_date']))->getStyle('A2')->getFont()->setSize(14);
		$objSheet->getStyle ( 'A2' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
		$objSheet->mergeCells("A3:B3")->setCellValue("A3", "请款部门:")->getStyle('A3')->getFont()->setSize(14);
		$objSheet->mergeCells("C3:H3")->setCellValue("C3", $head['depart_name']?:'')->getStyle('C3')->getFont()->setSize(14);
		$objSheet->mergeCells("A4:B4")->setCellValue("A4", "供应商名称:")->getStyle('A4')->getFont()->setSize(14);
		$objSheet->mergeCells("C4:H4")->setCellValue("C4", $head['supplier_name']?:'')->getStyle('C4')->getFont()->setSize(14);
		$objSheet->mergeCells("A5:B5")->setCellValue("A5", "收款帐户名称:")->getStyle('A5')->getFont()->setSize(14);
		$objSheet->mergeCells("C5:H5")->setCellValue("C5", $head['account_name']?:'')->getStyle('C5')->getFont()->setSize(14);
		$objSheet->mergeCells("A6:B6")->setCellValue("A6","开户银行:")->getStyle('A6')->getFont()->setSize(14);
		$objSheet->mergeCells("C6:H6")->setCellValue("C6", $head['account_bank']?:'')->getStyle('C6')->getFont()->setSize(14);
		$objSheet->mergeCells("A7:B7")->setCellValue("A7", "银行帐号:")->getStyle('A7')->getFont()->setSize(14);
		$objSheet->mergeCells("C7:F7")->setCellValue("C7", $head['account'])->getStyle('C7')->getFont()->setSize(14);
		$objSheet->getStyle ( 'C7' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_LEFT );
		$objSheet->setCellValue("G7", "单位：");
		$objSheet->setCellValue("H7", '元');
		
		
 		$objSheet->getColumnDimension('A')->setWidth(12);
 		$objSheet->getColumnDimension('B')->setWidth(14);
 		$objSheet->getColumnDimension('C')->setWidth(23);
 		$objSheet->getColumnDimension('D')->setWidth(12);
 		$objSheet->getColumnDimension('E')->setWidth(15);
 		$objSheet->getColumnDimension('F')->setWidth(12);
 		$objSheet->getColumnDimension('G')->setWidth(12);
 		$objSheet->getColumnDimension('H')->setWidth(23);
 	
 		$objSheet->setCellValue('A8', '序号');
 		$objSheet->getStyle('A8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('B8', '进货日期');
 		$objSheet->getStyle('B8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('C8', '商品明细');
 		$objSheet->getStyle('C8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('D8', '单位');
 		$objSheet->getStyle('D8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('E8', '数量');
 		$objSheet->getStyle('E8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('F8', '单价');
 		$objSheet->getStyle('F8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('G8', '金额');
 		$objSheet->getStyle('G8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('H8', '备注');
 		$objSheet->getStyle('H8')->getFont()->setBold(true)->setSize(14);
 		
//  		$objSheet->getStyle("A8:H8")->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
 	
 	
 		$rowNum = 9;
 		$total_amount = 0;
 		foreach($detail as $key => $det){
 			$this->_genExcelFont($objSheet, "A".$rowNum, "A".$rowNum, $key+1 , false);
 			$this->_genExcelFont($objSheet, "B".$rowNum, "B".$rowNum, date('Y-m-d',$det['purchase_time']), false);
 			$this->_genExcelFont($objSheet, "C".$rowNum, "C".$rowNum, $det['product_name']?:'', false);
 			$this->_genExcelFont($objSheet, "D".$rowNum, "D".$rowNum, $det['unit_name']?:'', false);
 			$this->_genExcelFont($objSheet, "E".$rowNum, "E".$rowNum, $det['quantity']?:'', false);
 			$this->_genExcelFont($objSheet, "F".$rowNum, "F".$rowNum, $det['price']?:'', false);
 			$this->_genExcelFont($objSheet, "G".$rowNum, "G".$rowNum, $det['amount']?:'', false);
 			$this->_genExcelFont($objSheet, "H".$rowNum, "H".$rowNum, $det['remark']?:'', false);
 			$total_amount += intval($det['amount']);
 			$rowNum++;
 		}
 		
 		$_amount = $this->NumToCNMoney($total_amount,1,0);
 		$this->_genExcelFont($objSheet, "A".$rowNum, "A".$rowNum,'合计', false);
 		$objSheet->getStyle ( "A".$rowNum )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
//  		$this->_genExcelFont($objSheet, "B".$rowNum, "F".$rowNum, $_amount, "B".$rowNum.":F".$rowNum);
 		$objSheet->mergeCells("B".$rowNum.":F".$rowNum)->setCellValue("B".$rowNum, $_amount);
 		$objSheet->getStyle ( "B".$rowNum )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
 		$this->_genExcelFont($objSheet, "G".$rowNum, "G".$rowNum, $total_amount, false);
 		
 		$objSheet->getStyle("A8:H".$rowNum)->getBorders()->getOutline()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
 		
 		
 		ob_end_clean(); // 去掉缓存
 		header("Content-Type: application/force-download");
 		header("Content-Type: application/octet-stream");
 		header("Content-Type: application/download");
 		header('Content-Disposition:inline;filename="' . $filename . '.xls"');
 		header("Content-Transfer-Encoding: binary");
 		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
 		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 		header("Pragma: no-cache");
 		$objWriter->save('php://output');
 	}
 	
 	
 	/**
 	 * @desc 获取汇总信息
 	 * @author liaojianwen
 	 * @date 2017-11-01
 	 */
 	public function getPaymentSummaryList($id)
 	{
	 	if(!$id){
	 		return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'id can not be null');
	 	}
	 	$res_head = VendorPaymentDAO::getInstance()->getVendorPaymentHead($id);
	 	if(! $res_head){
	 		return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor_payment_head can not be found');
	 	}
		$res_sum = VendorPaymentSummaryDAO::getInstance()->getSummaryDetail($id);
	 	if(! $res_sum){
	 		return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'vendor_payment_summary can not be found');
	 	}
	 	foreach ($res_sum as &$det){
	 		$starTime = strtotime(substr($det['summary_time'], 0,10));
	 		$endTime = strtotime(substr($det['summary_time'], 11));
	 		$det['time'] = date('n月d日',$starTime) .'-'.date('n月d日',$endTime);
	 	}	
	 	$result['head'] = $res_head;
	 	$result['sum'] = $res_sum;
	 	
	 	return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 导出采购统计（供应商）
 	 * @author liaojianwen
 	 * @date 2017-03-10
 	 */
 	public function explodePaymentSummary($paymentInfo, $filename, $title, $excVer = "xls")
 	{
 		$data = $paymentInfo;
 		$head = $paymentInfo['head'];
 		$detail = $paymentInfo['sum'];
 		$objPHPExecl = new \PHPExcel();
 		$objSheet = $objPHPExecl->getActiveSheet();
 		$objWriter = NULL;
 		// 创建文件格式写入对象实例
 		switch ($excVer)
 		{
 			case 'xlsx' :
 				$objWriter = new \PHPExcel_Writer_Excel2007 ( $objPHPExecl );
 				break;
 			case 'xls' :
 				$objWriter = new \PHPExcel_Writer_Excel5 ( $objPHPExecl );
 				break;
 			default:
 				$objWriter = new \PHPExcel_Writer_Excel5($objPHPExecl);
 		}
 		// 设置宽度
 		$filename = $head['supplier_name'] .'付款审批单' .date("Y-m-d",$head['payment_date']);
 		$objSheet->mergeCells ( "A1:H1" );
 		$objSheet->setCellValue ( "A1", $title );
 		$objSheet->getStyle ( 'A1' )->getFont ()->setSize ( 20 );
 		$objSheet->getStyle ( 'A1' )->getFont ()->setBold ( true );
 		$objSheet->getStyle ( 'A1' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
 	
 	
 		$objSheet->mergeCells("A2:H2")->setCellValue("A2", "请款日期: ".date("Y-m-d",$head['payment_date']))->getStyle('A2')->getFont()->setSize(14);
 		$objSheet->getStyle ( 'A2' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
 		$objSheet->mergeCells("A3:B3")->setCellValue("A3", "请款部门:")->getStyle('A3')->getFont()->setSize(14);
 		$objSheet->mergeCells("C3:H3")->setCellValue("C3", $head['depart_name']?:'')->getStyle('C3')->getFont()->setSize(14);
 		$objSheet->mergeCells("A4:B4")->setCellValue("A4", "供应商名称:")->getStyle('A4')->getFont()->setSize(14);
 		$objSheet->mergeCells("C4:H4")->setCellValue("C4", $head['supplier_name']?:'')->getStyle('C4')->getFont()->setSize(14);
 		$objSheet->mergeCells("A5:B5")->setCellValue("A5", "收款帐户名称:")->getStyle('A5')->getFont()->setSize(14);
 		$objSheet->mergeCells("C5:H5")->setCellValue("C5", $head['account_name']?:'')->getStyle('C5')->getFont()->setSize(14);
 		$objSheet->mergeCells("A6:B6")->setCellValue("A6","开户银行:")->getStyle('A6')->getFont()->setSize(14);
 		$objSheet->mergeCells("C6:H6")->setCellValue("C6", $head['account_bank']?:'')->getStyle('C6')->getFont()->setSize(14);
 		$objSheet->mergeCells("A7:B7")->setCellValue("A7", "银行帐号:")->getStyle('A7')->getFont()->setSize(14);
 		$objSheet->mergeCells("C7:F7")->setCellValue("C7", $head['account'])->getStyle('C7')->getFont()->setSize(14);
 		$objSheet->getStyle ( 'C7' )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_LEFT );
 		$objSheet->setCellValue("G7", "单位：");
 		$objSheet->setCellValue("H7", '元');
 	
 	
 		$objSheet->getColumnDimension('A')->setWidth(12);
 		$objSheet->getColumnDimension('B')->setWidth(20);
 		$objSheet->getColumnDimension('C')->setWidth(23);
 		$objSheet->getColumnDimension('D')->setWidth(12);
 		$objSheet->getColumnDimension('E')->setWidth(15);
 		$objSheet->getColumnDimension('F')->setWidth(12);
 		$objSheet->getColumnDimension('G')->setWidth(12);
 		$objSheet->getColumnDimension('H')->setWidth(23);
 	
 		$objSheet->setCellValue('A8', '序号');
 		$objSheet->getStyle('A8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('B8', '进货日期');
 		$objSheet->getStyle('B8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('C8', '商品明细');
 		$objSheet->getStyle('C8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('D8', '单位');
 		$objSheet->getStyle('D8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('E8', '数量');
 		$objSheet->getStyle('E8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('F8', '单价');
 		$objSheet->getStyle('F8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('G8', '金额');
 		$objSheet->getStyle('G8')->getFont()->setBold(true)->setSize(14);
 		$objSheet->setCellValue('H8', '备注');
 		$objSheet->getStyle('H8')->getFont()->setBold(true)->setSize(14);
 			
 		//  		$objSheet->getStyle("A8:H8")->getBorders()->getAllBorders()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
 	
 	
 		$rowNum = 9;
 		$total_amount = 0;
 		foreach($detail as $key => $det){
 			$this->_genExcelFont($objSheet, "A".$rowNum, "A".$rowNum, $key+1 , false);
 			$this->_genExcelFont($objSheet, "B".$rowNum, "B".$rowNum, $det['time'], false);
 			$this->_genExcelFont($objSheet, "C".$rowNum, "C".$rowNum, $det['product_name']?:'', false);
 			$this->_genExcelFont($objSheet, "D".$rowNum, "D".$rowNum, $det['unit_name']?:'', false);
 			$this->_genExcelFont($objSheet, "E".$rowNum, "E".$rowNum, $det['quantity']?:'', false);
 			$this->_genExcelFont($objSheet, "F".$rowNum, "F".$rowNum, $det['price']?:'', false);
 			$this->_genExcelFont($objSheet, "G".$rowNum, "G".$rowNum, $det['amount']?:'', false);
 			$this->_genExcelFont($objSheet, "H".$rowNum, "H".$rowNum, $det['remark']?:'', false);
 			$total_amount += intval($det['amount']);
 			$rowNum++;
 		}
 			
 		$_amount = $this->NumToCNMoney($total_amount,1,0);
 		$this->_genExcelFont($objSheet, "A".$rowNum, "A".$rowNum,'合计', false);
 		$objSheet->getStyle ( "A".$rowNum )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
 		//  		$this->_genExcelFont($objSheet, "B".$rowNum, "F".$rowNum, $_amount, "B".$rowNum.":F".$rowNum);
 		$objSheet->mergeCells("B".$rowNum.":F".$rowNum)->setCellValue("B".$rowNum, $_amount);
 		$objSheet->getStyle ( "B".$rowNum )->getAlignment ()->setHorizontal ( \PHPExcel_Style_Alignment::HORIZONTAL_CENTER );
 		$this->_genExcelFont($objSheet, "G".$rowNum, "G".$rowNum, $total_amount, false);
 		
 		
 		
 		$objSheet->getStyle("A8:H".$rowNum)->getBorders()->getOutline()->setBorderStyle(\PHPExcel_Style_Border::BORDER_MEDIUM);
 		
 		
 		$rowNum++;
 		$objSheet->setCellValue ( "A".$rowNum, '总经理核准：' );
 		$this->_genExcelFont($objSheet, "A".$rowNum, "A".$rowNum, '总经理核准：' , false);
 		$this->_genExcelFont($objSheet, "C".$rowNum, "C".$rowNum, '财务审批：' , false);
 		$this->_genExcelFont($objSheet, "E".$rowNum, "E".$rowNum, '部门主管审批：' , false);
 		$this->_genExcelFont($objSheet, "G".$rowNum, "G".$rowNum, '经办人签名：' , false);
 		
 		
 		ob_end_clean(); // 去掉缓存
 		header("Content-Type: application/force-download");
 		header("Content-Type: application/octet-stream");
 		header("Content-Type: application/download");
 		header('Content-Disposition:inline;filename="' . $filename . '.xls"');
 		header("Content-Transfer-Encoding: binary");
 		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
 		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 		header("Pragma: no-cache");
 		$objWriter->save('php://output');
 	}
 	
 }