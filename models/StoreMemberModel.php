<?php
/**
 * @desc   会员列表模型
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\models;
use Yii;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\StoreMemberDAO;

class StoreMemberModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return ListStoreStoreModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @desc   获取门店列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getMemberList($pageInfo,$searchData,$filter)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$memberList = StoreMemberDAO::getInstance()->getMemberListData($pageInfo,$searchData,$filter);

		if (empty ( $memberList )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $memberList );
	}

	/**
	 * @desc   保存添加/编辑门店
	 * @param  $data  array 门店数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveMember($data)
	{
	    if(empty($data['nickname'])){
		   return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','nickname is null');
		}
		//检查是否有重名
        $result = StoreMemberDAO::getInstance()->checkMember($data['member_id'],$data['nickname']);	
		if($result){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','nickname is exists');
		}
        $result = StoreMemberDAO::getInstance()->saveMemberData($data);				
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc   获取一条会员数据
	 * @param  $data  member_id 会员id
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getMemberOne($member_id)
	{
	    if($member_id<1){		
		   return false;
		}
        $reslut = StoreMemberDao::getInstance()->getMemberRs($member_id);
		if(empty($reslut)){
		   return false;
		} 
	    return $reslut;
	}
}

