<?php
/**
 * @desc   店员列表模型
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\models;
use Yii;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\StoreEmployeeDAO;

class StoreEmployeeModel extends BaseModel
{
	/**
	 * @desc覆盖父方法返回对象
	 * @author hehuawei
	 * @date 2017-7-21
	 * @return ListStoreStoreModel
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @desc   获取店员列表
	 * @param  $searchData object 查询条件
	 * @param  $filter     object 过滤条件  
	 * @param  $pageInfo   object 分页相关数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function getStoreEmployeeList($pageInfo,$searchData,$filter)
	{
		if(empty($pageInfo)){
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'params can not be null' );
		}
		
		$storeEmployeeList = StoreEmployeeDAO::getInstance()->getStoreEmployeeListData($pageInfo,$searchData,$filter);

		if (empty ( $storeEmployeeList )) {
			return $this->handleApiFormat ( EnumOther::ACK_FAILURE, '', 'no data found' );
		}
		return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $storeEmployeeList );
	}

	/**
	 * @desc   保存添加/编辑店员
	 * @param  $data  array 店员数据
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	public function saveStoreEmployee($data)
	{
	    if(empty($data['employee_name'])){
		   return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','employee_name is null');
		}
		//检查是否有重名
        $result = StoreEmployeeDAO::getInstance()->checkStoreEmployee($data['employee_id'],$data['employee_name']);	
		if($result){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','employee_name is exists');
		}
        $result = StoreEmployeeDAO::getInstance()->saveStoreEmployeeData($data);				
		if(empty($result)){
			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','save failure');
		}
		return $this->handleApiFormat(EnumOther::ACK_SUCCESS,'');
	}
	
	/**
	 * @desc   获取一条数据
	 * @param  $employee_id  店员id
	 * @author hehuawei
	 * @date   2017-7-21
     * @return array
	 */
	 public function getStoreEmployeeOne($employee_id)
	 {
		 if($employee_id<1){
		    return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','employee_id is null');
		 }
         $result = StoreEmployeeDAO::getInstance()->getStoreEmployeeOne($employee_id);
	     if(empty($result)){
		    return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','select failure');
		 }
		 return $this->handleApiFormat ( EnumOther::ACK_SUCCESS, $result );
	 }
}

