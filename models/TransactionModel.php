<?php
namespace app\models;
/**
 * @desc transactionModel
 * @author liaojianwen
 * @date 2016-12-21
 */
use app\models\BaseModel;
use app\enum\EnumOther;
use app\dao\TransactionDAO;
	 
 class TransactionModel extends BaseModel
 {
 	
 	/**
 	 * @desc 覆盖父方法返回对象
 	 * @author liaojianwen
 	 * @date 2016-12-14
 	 * @return TransactionModel
 	 */
 	public static function model($className=__CLASS__)
 	{
 		return parent::model($className);
 	}
 	
 	/**
 	 * @desc 获取出入库明细列表数据
 	 * @param $cond 查询条件
	 * @param $pageInfo 页面数据
 	 * @author liaojianwen
 	 * @date 2016-12-22
 	 */
 	public function getTransactions($cond, $pageInfo)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','pageinfo is empty');
 		}
 		$result = TransactionDAO::getInstance()->getTransactions($cond, $pageInfo);
 		if(empty($result)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE,'','no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 	
 	/**
 	 * @desc 获取出入库统计数据
 	 * @author liaojianwen
 	 * @date 2017-04-19
 	 */
 	public function getStockSummary($pageInfo, $cond)
 	{
 		if(empty($pageInfo)){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'pageInfo is empty');
 		}
 		if(!isset($cond['starTime']) || empty($cond['starTime'])){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'search params time is empty');
 		}
 		$cond['starTime'] = strtotime($cond['starTime']);
 		$cond['endTime'] = strtotime($cond['endTime']);
 		$result =TransactionDAO::getInstance()->getStockSum($cond, $pageInfo);
 		if(!$result){
 			return $this->handleApiFormat(EnumOther::ACK_FAILURE, '', 'no data found');
 		}
 		return $this->handleApiFormat(EnumOther::ACK_SUCCESS, $result);
 	}
 }
