<?php 
namespace app\controllers;
/**
 * @desc 出库单控制器
 */
use app\controllers\BaseController;
use app\models\SupplierModel;
use app\models\WarehouseModel;
use app\models\EmployeeModel;
use app\enum\EnumOriginType;
use app\models\InstoreModel;
use app\models\OutstoreModel;
use Yii;
use app\helpers\CInputFilter;
use app\models\InventoryModel;

class OutstoreController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-28
	 */
	public  function actionIndex()
	{
		$this->redirect('outstore/list-outstore');
	}
	
	/**
	 * @desc 出库单列表
	 * @author liaojianwen
	 * @date 2016-11-28
	 */
	public function actionListOutstore()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('outstorelist', ['stock'=>$stock]);
	}
	
	/**
	 * @desc 出库单页面
	 * @author liaojianwen
	 * @date 2016-11-28
	 */
	public function actionOutstore()
	{
		$instoreNo = EnumOriginType::OTHER_OUT . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
// 		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$outstore_type;
		return $this->render ( 'outstore', [
				'warehouse' => $warehouse,
				'admin' => $admin,
// 				'supplier' => $supplier,
				'type' => $type,
				'NO' => $instoreNo
		] );
	}
	
	/**
	 * @desc 获取产品信息
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function actionGetProducts()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize'=>$request->get('pageSize',5),
		];
		$name = $request->get('name');
		$result = InstoreModel::model()->getProducts($pageInfo,$name);
		$this->renderJson($result);
	}
	
	/**
	 * @保存出库单
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function actionSaveOutstore()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
		$result = OutstoreModel::model ()->saveOutstore ( $head, $detail, $remove, $id);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 获取出库单列表数据
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function actionGetOutstores()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter');
	
		$result = OutstoreModel::model ()->getOutstores ( $pageInfo, $condition, $filter);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc  编辑出库单
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionOutstoreDetail()
	{
		$instoreNo =  EnumOriginType::OTHER_OUT . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
// 		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$outstore_type;
		return $this->render ( 'editoutstore', [
				'warehouse' => $warehouse,
				'admin' => $admin,
// 				'supplier' => $supplier,
				'type' => $type,
				'NO' => $instoreNo
		] );
	}
	
	/**
	 * @desc 获取单位
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function actionListUnits()
	{
		$result = WarehouseModel::model()->listUnits();
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据id 获取出库单单信息
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionGetOutstoreInfo()
	{
		$id = Yii::$app->request->get('id');
		$result = OutstoreModel::model()->getOutstoreInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 确认出库单
	 * @author liaojianwen
	 * @date 2016-11-25
	 */
	public function actionConfirmOutstore()
	{
		$id = Yii::$app->request->get('id',0);
		$result = OutstoreModel::model()->confirmOutstore($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除出库单
	 * @author liaojianwen
	 * @date 2016-11-29
	 */
	public function actionDelOutstore()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
	
		$result = OutstoreModel::model()->delOutstore($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取出库单的明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	public function actionGetOutstoreDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = OutstoreModel::model()->getOutstoreDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有删除其他出库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelOutstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑其他出库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditOutstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有确认其他出库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckConfirmOutstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 根据仓库获取仓库下面的商品
	 * @author liaojianwen
	 * @date 2017-06-22
	 */
	public function actionGetProductsByWare()
	{
		$request = Yii::$app->request;
		$ware_id = $request->get('ware');
	
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize'=>$request->get('pageSize',5),
		];
		$name = $request->get('name');
		$result = InventoryModel::model()->getProductByWare($ware_id, $name, $pageInfo);
		$this->renderJson($result);
	
	}
	
	/**
	 * @desc 其他出库单汇总
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionOutstoreSummary()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('outstoresum', ['stock'=> $stock]);
	}
	
	/**
	 * @desc 获取其他出库汇总数据
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionGetOutstoreSummary()
	{
		$pageInfo = [
				'page'=>CInputFilter::getInt('page',1),
				'pageSize' => CInputFilter::getInt('pageSize',10),
		];
		$condition = CInputFilter::getArray('cond', '');
		$result = OutstoreModel::model()->getOutstoreSummary($pageInfo, $condition);
		$this->renderJson($result);
	}
}