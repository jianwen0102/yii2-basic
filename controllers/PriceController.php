<?php
 namespace app\controllers;
 
 use app\controllers\BaseController;
 use Yii;
 use app\enum\EnumOther;
use app\enum\EnumOriginType;
use app\models\EmployeeModel;
use app\models\PriceAdjustmentModel;
use app\helpers\CInputFilter;
use app\models\SupplierModel;
					 
 class PriceController extends BaseController
 {
 	/**
 	 * @desc 默认控制器
 	 * @author liaojianwen
 	 */
 	public function actionIndex()
 	{
 		$this->redirect('list-adjustment');
 	}
 	
 	/**
 	 * @desc 成本调整单列表
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function actionListAdjustment()
 	{
 		return $this->render('adjustmentlist');
 	}
 	
 	/**
 	 * @desc 新增调整单
 	 * @author liaojianwen
 	 * @date 2017-07-11
 	 */
 	public function actionAddAdjustment()
 	{
 		$No = EnumOriginType::PRICE_ADJUSTMENT.date('YmdHis');
 		$admin = EmployeeModel::model()->listEmployee();
 		return $this->render ( 'addadjustment', [
 				'admin' => $admin,
 				'NO' => $No,
 		] );
 	}
 	
 	/**
 	 * @desc 编辑调整单
 	 * @author liaojianwen
 	 * @date 2017-07-11
 	 */
 	public function actionEditAdjustment()
 	{
 		$No = EnumOriginType::PRICE_ADJUSTMENT.date('YmdHis');
 		$admin = EmployeeModel::model()->listEmployee();
 		return $this->render ( 'editadjustment', [
 				'admin' => $admin,
 				'NO' => $No,
 		] );
 	}
 	
 	/**
 	 * @desc 成本调整单查询
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function actionListAdjustmentInfo()
 	{
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render('listadjustmentinfo',['supplier'=>$supplier]);
 	}
 	
 	/**
 	 * @desc 获取产品信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionGetProducts()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize'=>$request->get('pageSize',5),
 		];
 		$name = $request->get('name');
 		$result = PriceAdjustmentModel::model()->getProducts($pageInfo,$name);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 保存成本调整单
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionSaveAdjustment()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$result = PriceAdjustmentModel::model ()->saveAdjustment( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	
 	/**
 	 * @desc 获取货款申请单信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionGetAdjustments()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 			
 		$result = PriceAdjustmentModel::model ()->getAdjustments($condition,$pageInfo);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有编辑权限
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionCheckEditAdjustment()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 弹框获取调整单明细
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionGetAdjustmentDetail()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = PriceAdjustmentModel::model()->getAdjustmentDetail($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 获取请款单编辑页面信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionGetAdjustmentInfo()
 	{
 		$id = CInputFilter::getInt('id', 0);
 			
 		$result = PriceAdjustmentModel::model()->getAdjustmentInfo($id);
 		$this->renderJson($result);
 			
 	}
 	
 	/**
 	 * @desc 检查是否有确认调整单权限
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function actionCheckConfirmAdjustment()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 确认成本调整单
 	 * @author liaojianwen
 	 * @date 2017-07-13
 	 */
 	public function actionConfirmAdjustment()
 	{
		$id = CInputFilter::getInt('id', 0);
 		$result = PriceAdjustmentModel::model()->confirmAdjustment($id);
 		$this->renderJson($result);
 	
 	}
 	
 	/**
 	 * @desc 获取货款申请单查询信息
 	 * @author liaojianwen
 	 * @date 2017-07-12
 	 */
 	public function actionGetAdjustmentSearch()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 	
 		$result = PriceAdjustmentModel::model ()->getAdjustmentSearch($condition,$pageInfo);
 		$this->renderJson ( $result );
 	}
 	
 }