<?php
namespace app\controllers;

use app\controllers\BaseController;
use app\models\WarehouseModel;
use yii;
use app\models\AdminModel;
use app\models\SupplierModel;
use app\enum\EnumOriginType;
use app\helpers\CInputFilter;

class WarehouseController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-3
	 */
	public function actionIndex()
	{
		$this->redirect('?r=warehouse/list-warehouse');
	}
	
	/**
	 * @desc 仓库列表页面
	 * @author liaojianwen
	 * @date 2016-11-3
	 */
	public function actionListWarehouse() 
	{
		return $this->render('warehousesetting');
	}
	
	/**
	 * @desc 单位列表
	 * @author liaojianwen
	 * @date 2016-11-07
	 */
	public function actionListUnit()
	{
		return $this->render('unit');
		
	}
	
	/**
	 * @desc 获取仓库信息
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionGetWarehouses()
	{
		$request = Yii::$app->request;
		$page = $request->get('page',1);
		$pageSize = $request->get('pageSize',10);
		$pageInfo = [
				'page'=>$page,
				'pageSize'=>$pageSize,
		];
		$result = WarehouseModel::model()->getWarehouses($pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存仓库
	 * @author liaojianwen
	 * @date 2016-11-03
	 */
	public function actionSaveWarehouse()
	{
		$request = Yii::$app->request;
		$name = $request->get('name');
		$remark = $request->get('remark');
		$default = $request->get('def');
		$wid = $request->get('wid',0);
		$scrapped = $request->get('scrapped');
		$result = WarehouseModel::model()->saveWarehouse($name,$remark,$default,$wid,$scrapped);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除仓库
	 * @author liaojianwen
	 * @date 2106-11-04
	 */
	public function actionDelWarehouse()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		$result = WarehouseModel::model()->delWarehouse($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 单位信息
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionGetUnits()
	{
		$request = Yii::$app->request;
		$pageInfo = [
			'page'=>$request->get('page',1),
			'pageSize'=>$request->get('pageSize',10),
		];
		$result = WarehouseModel::model()->getUnits($pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存单位
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionSaveUnit()
	{
		$request = Yii::$app->request;
		$name = $request->get('name');
		$desc = $request->get('desc');
		$active = $request->get('act');
		$uid = $request->get('uid',0);
		$result = WarehouseModel::model()->saveUnit($name, $desc, $active, $uid);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除单位
	 * @author liaojianwen
	 * @date 2106-11-07
	 */
	public function actionDelUnit()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		$result = WarehouseModel::model()->delUnit($ids);
		$this->renderJson($result);
	}

	/**
	 * @desc 检验是否有新增仓库权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddWare()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑仓库权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditWare()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有删除仓库权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelWare()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有新增单位权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddUnit()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑单位权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditUnit()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有删除单位
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelUnit()
	{
		$this->renderJson('');
	}
}