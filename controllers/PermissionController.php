<?php
namespace app\controllers;
/**
 * @desc 权限控制器
 * @author liaojianwen
 * @date 2017-01-07
 */
 use app\controllers\BaseController;
 use Yii;
use app\enum\EnumOther;
use yii\rbac\Permission;
use app\models\PermissionModel;
use app\dao\AuthItemDAO;
 
class PermissionController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function actionIndex()
	{
		$this->redirect('list-group');
	}
	
	/**
	 * @desc 权限分组
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function actionListGroup()
	{
		return $this->render('listgroup');
	}
	
	/**
	 * @desc 检查分组是否已存在
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function actionCheckGroup()
	{
		$request = Yii::$app->request;
		$name = $request->get('groupname');
		$group = $request->get('group');
		
		if($group){
			//编辑
			$result = AuthItemDAO::getInstance()->checkSameName($name, $group);
			if($result){
				echo 'true';
				exit;
			}
			echo 'false';
			exit;
		}
		//新增
		$auth =Yii::$app->authManager;
		$groupname = EnumOther::PRE_GROUP.$name;
		//检查是否有分组（role）
		$oneRole = $auth->getRole($groupname);
		if($oneRole){
			echo 'false';
			exit;
		}
		echo 'true';
		exit;
		
	}
	
	/**
	 * @desc 保存分组
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function actionSaveGroup()
	{
		$request = Yii::$app->request;
		$name = $request->get('groupname');
		$group = $request->get('group');
		
		$result = PermissionModel::model()->saveGroup($name, $group);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取权限分组信息
	 * @author liaojianwen
	 * @date 2017-01-07
	 */
	public function actionGetGroups()
	{
		$request = Yii::$app->request;
		$pageInfo = [
			'page'=> $request->get('page',1),
			'pageSize'=> $request->get('pageSize',10),
		];
		$cond = $request->get('cond');
		$result = PermissionModel::model()->getGroups($cond, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有分配权限
	 * @author liaojianwen
	 * @date 2017
	 */
	public function actionCheckAddGroup()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有分配权限
	 * @author liaojianwen
	 * @date 2017
	 */
	public function actionCheckEditGroup()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有分配权限
	 * @author liaojianwen
	 * @date 2017
	 */
	public function actionCheckDelGroup()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有分配权限
	 * @author liaojianwen
	 * @date 2017
	 */
	public function actionCheckAssignGroup()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 获取权限列表
	 * @author liaojianwen
	 * @date 2017-01-10
	 */
	public function actionListAuth()
	{
		$request = Yii::$app->request;
		$gname = $request->get('group');
	
		$result = PermissionModel::model()->listAuth($gname);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存分组权限
	 * @author liaojianwen
	 * @date 2017-01-11
	 */
	public function actionSaveAuth()
	{
		$request = Yii::$app->request;
		$group = $request->get('group');
		$remove = $request->get('remove','');
		$auth = $request->get('auth','');
		$result = PermissionModel::model()->saveAuth($group,$remove,$auth);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除分组
	 * @author liaojianwen
	 * @date 2017-01-12
	 */
	public function actionDelGroup()
	{
		$request = Yii::$app->request;
		$names= $request->get('names');
		
		$result = PermissionModel::model()->delGroup($names);
		$this->renderJson($result);
	}
}