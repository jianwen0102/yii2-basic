<?php
namespace app\controllers;

use app\controllers\BaseController;
use app\models\AdminModel;
use yii\base\Object;
use yii;
use app\models\PasswordForm;
use app\models\BaseModel;
use app\enum\EnumOther;
use app\models\PermissionModel;

class AdminController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
	public function actionIndex()
	{
		$this->redirect(['Admin/personal-info']);
	}
	
	/**
	 * @desc 个人信息页面
	 * @author liaojianwen
	 * @date 2016-11-15
	 */
	public function actionPersonalInfo()
	{
		$info = (new AdminModel())->findOne(Yii::$app->user->identity->id);
		return $this->render('personalinfo',['info'=>$info]);
	}
	
	/**
	 * @desc 更改个人信息
	 * @author liaojianwen
	 * @date 2016-11-15
	 */
	public function actionChangePwd()
	{
		$model = new PasswordForm(Yii::$app->request->post());
		$result = $model->changeInfo();
		$this->renderJson($result);
	}
	
	/**
	 * @desc 更改用户信息
	 * @author liaojianwen
	 * @date 2016-11-15
	 */
	public function actionChangeUserPwd()
	{
		$model = new PasswordForm(Yii::$app->request->post());
		$result = $model->changeInfo();
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查用户名是否存在
	 * @author liaojianwen
	 * @date 2016-11-15
	 */
	public function actionCheckUser(){
		$request = Yii::$app->request;
	
		$type = $request->get('type', 'adminname');
		$val = $request->get($type, '');
		$id = $request->get('id','');
		
		if ($val){
			switch ($type){
				case 'adminname':
					if(empty($id)){
						$userInfo = AdminModel::find()->where('username=:name',[':name'=>$val])->one();
					} else {
						$userInfo = AdminModel::find()->where('username=:name and id != :id',[':name'=>$val,':id'=>$id])->one();
					}
					break;
				case 'email':
					if(empty($id)){
						$userInfo = AdminModel::find()->where('email=:email',[':email'=>$val])->one();
					} else{
						$userInfo = AdminModel::find()->where('email=:email and id != :id',[':email'=>$val,':id'=>$id])->one();
					}
					break;
			}
			if (!empty($userInfo)){
				echo 'false';
				exit;
			}
		}
		echo 'true';
		exit;
	}
	
	/**
	 * @desc 管理员列表
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	public function actionListUsers()
	{
		return $this->render('userlist');
	}
	
	/**
	 * @desc 获取管理员list
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	public function actionGetUsers()
	{
		$request = Yii::$app->request;
		$pageInfo = [
			'page'=>$request->get('page',1),
			'pageSize'=> $request->get('pageSize',10),
		];
		$admin = new AdminModel();
		$result = $admin->getUserList($pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 创建新用户管理员
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	public function actionCreateUser()
	{
		$userInfo = Yii::$app->request->post();
		$model = new AdminModel();
		$result = $model->createUser($userInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 用户信息更改
	 * @author liaojianwen
	 * @date 2016-11-16
	 */
	public function actionUserInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$info = (new AdminModel())->find()->where(['id'=>$id,'delete_flag'=>EnumOther::NO_DELETE])->one();
		if(empty($info)){
			return false;
		}
		return $this->render('userinfo',['info'=>$info]);
	}
	
	/**
	 * @desc 删除用户
	 * @author liaojianwen
	 * @date 2106-11-16
	 */
	public function actionDelUser()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		$result = (new AdminModel())->delUser($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有新增用户权限
	 * @author liaojianwen
	 * @date 2017-01-10
	 */
	public function actionCheckAddUser()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有编辑权限
	 * @author liaojianwen
	 * @date 2017-01-10
	 */
	public function actionCheckEditUser()
	{
		$this->renderJson('');	
	}
	
	/**
	 * @desc 检查是否删除权限
	 * @author liaojianwen
	 * @date 2017-01-10
	 */
	public function actionCheckDelUser()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有分配角色权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAssignUser()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 获取权限分组
	 * @author liaojianwen
	 * @date 2017-01-12
	 */
	public function actionGetAuthRole()
	{
		$request = Yii::$app->request;
		$id = $request->get('id','');
		
		$result = PermissionModel::model()->getAuthRole($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存角色分配给用户
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionSaveAuthAssign()
	{
		$request = Yii::$app->request;
		
		$info = $request->get('info');
		$user_id = $request->get('uid');
		
		$admin = new AdminModel(); 
		$result = $admin->saveAuthAssign($info, $user_id);
		$this->renderJson($result);
		
		
	}
	
}