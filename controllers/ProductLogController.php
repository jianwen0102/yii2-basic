<?php
namespace app\controllers;
/**
 * @desc 商品更新日志
 * @author liaojianwen
 * @date 2017-01-03
 */
 use app\controllers\BaseController;
 use Yii;
use app\models\ProductLogModel;
	 
 class ProductLogController extends BaseController
 {
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2017-01-03
	 */ 	
 	public function actionIndex()
 	{
 		$this->redirect('list-log');
 	}
	
	/**
	 * @desc 商品更新log
	 * @author liaojianwen
	 * @date 2017-01-03
	 */
 	public function actionListLog()
 	{
 		return $this->render('productlog');
 	}
 	
 	/**
 	 * @desc 获取商品更新log
 	 * @author liaojianwen
 	 * @date 2017-01-03
 	 */
 	public function actionGetProductLog()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 			'page'=> $request->get('page',1),
 			'pageSize'=> $request->get('pageSize',10),
 		];
 		$cond = $request->get('cond');
 		$filter = $request->get('filter');
 		
 		$result = ProductLogModel::model()->getProductLog($cond, $filter, $pageInfo);
 		$this->renderJson($result);
 	}
 }