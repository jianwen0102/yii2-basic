<?php
 namespace app\controllers;
 /**
  * @desc 客户控制器
  */
 
 use app\controllers\BaseController;
 use app\enum\EnumOther;
 use app\enum\EnumOriginType;
 use Yii;
 use app\models\ClientModel;
 use app\models\EmployeeModel;
 use app\dao\ClientDAO;
use app\dao\StoreDAO;
			 
 class ClientController extends BaseController
 {
 	/**
 	 * @desc 默认控制器
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function actionIndex()
 	{
 		$this->redirect('list-client');
 	}
 	
 	/**
 	 * @desc 客户列表
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function actionListClient()
 	{
 		return $this->render('clientlist');
 	}
 	
 	
 	/**
 	 * @desc 获取供应商信息
 	 * @author liaojianwen
 	 * @date 2016-11-10
 	 */
 	public function actionGetClients()
 	{
 		$request = Yii::$app->request;
 		$pageInfo= [
 				'page'=>$request->get('page',1),
 				'pageSize'=>$request->get('pageSize',10),
 		];
 		$cond = $request->get('cond');
 		$result = ClientModel::model()->getClients($cond, $pageInfo);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查新增客户权限
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function actionCheckAddClient()
 	{
 		$this->renderJson ( '' );
	}
	
	/**
	 * @desc 创建客户
	 * @author liaojianwen
	 * @date 2017-05-19
	 */
	public function actionAddClient()
	{
		$country = ClientModel::model ()->getDefaultRegion ( EnumOther::RE_COUNTRY, EnumOther::DEFAULT_COUNTRY );
		$province = ClientModel::model ()->getDefaultRegion ( EnumOther::DEFAULT_COUNTRY, EnumOther::DEFAULT_PROVINCE );
		$city = ClientModel::model ()->getDefaultRegion ( EnumOther::DEFAULT_PROVINCE, EnumOther::DEFAULT_CITY );
		$district = ClientModel::model ()->getDefaultRegion ( EnumOther::DEFAULT_CITY, EnumOther::DEFAULT_DISTINCT );
		$admin = EmployeeModel::model ()->listEmployee ();
		$stores = ClientModel::model()->listStores();
		return $this->render ( 'addclient', [ 
				'country' => $country,
				'province' => $province,
				'city' => $city,
				'district' => $district,
				'admin' => $admin,
				'stores' => $stores,
		]);
 	}
 	
 	/**
 	 * @desc 编辑页面
 	 * @author liaojianwen
 	 * @date 2017-01-16
 	 */
 	public function actionClientEdit()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get ( 'id', 0 );
 		$sinfo = ClientDAO::find ()->where ( 'client_id =:id', [
 				':id' => $id
 		] )->one ();
 		if(!empty($sinfo)){
	 		$country = ClientModel::model ()->getDefaultRegion ( EnumOther::RE_COUNTRY, $sinfo->country );
	 		$province = ClientModel::model ()->getDefaultRegion ( $sinfo->country, $sinfo->province );
	 		$city = ClientModel::model ()->getDefaultRegion ( $sinfo->province, $sinfo->city );
	 		$district = ClientModel::model ()->getDefaultRegion ( $sinfo->city, $sinfo->district );
	 		$admin = EmployeeModel::model()->listEmployee();
	 		$stores = ClientModel::model()->listStores();
	 		return $this->render ( 'editclient', [
	 				'country' => $country,
	 				'province' => $province,
	 				'city' => $city,
	 				'district' => $district,
	 				'admin' => $admin ,
	 				'stores' => $stores,
	 		] );
 		}
 	}
 	
 	/**
 	 * @desc 保存客户
 	 * @author liaojianwen
 	 * @date 2017-05-19
 	 */
 	public function actionSaveClient()
 	{
 		$request = Yii::$app->request;
 		$result = ClientModel::model()->saveClient($request->get());
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查供应商是否存在
 	 * @author liaojianwen
 	 * @date 2016-11-21
 	 */
 	public function actionCheckClient(){
		$request = Yii::$app->request;
		$id = $request->get ( 'id', 0);
		$val = $request->get('client','');
		if(empty($id)){
			$clientInfo = ClientDAO::find ()->where ( 'client_name=:name and delete_flag =:flag', [
					':name' => $val,
					':flag' => EnumOther::NO_DELETE
			] )->one ();
		} else {
			$clientInfo = ClientDAO::find ()->where ( 'client_name=:name and delete_flag =:flag and client_id != :id', [
					':name' => $val,
					':flag' => EnumOther::NO_DELETE,
					':id' => $id
			] ) ->one();
		}
		if (! empty ( $clientInfo )) {
			echo 'false';
			exit ();
		}
		
		echo 'true';
		exit ();
	}
	
	
	/**
	 * @desc 检查是否有编辑权限
	 * @author liaojianwen
	 * @date 2017-05-21
	 */
	public function actionCheckEditClient()
	{
		$this->renderJson('');
	}
 	
	
	/**
	 * @desc 根据id获取客户信息
	 * @author liaojianwen
	 * @date 2017-05-21
	 */
	public function actionGetClientInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get ( 'id', 0 );
		$result = ClientModel::model()->getClientInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据省id 获取城市list
	 * @author liaojianwen
	 * @date 2017-05-21
	 */
	public function actionGetCitys()
	{
		$request = Yii::$app->request;
		$province_id = $request->get('province');
		$result = ClientModel::model()->getRegions($province_id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据city_id 获取 地区列表
	 * @author liaojianwen
	 * @date 2017-05-21
	 */
	public function actionGetDistrict()
	{
		$request = Yii::$app->request;
		$cit = $request->get('city');
		$result = ClientModel::model()->getRegions($cit);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有删除客户权限
	 * @author liaojianwen
	 * @达特2017-05-21
	 */
	public function actionCheckDelClient()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 删除客户
	 * @author liaojianwen
	 * @date 2017-05-21
	 */
	public function actionDelClient()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids','');
		$result = ClientModel::model()->delClient($ids);
		$this->renderJson($result);
	}
 }
 
 
 
 