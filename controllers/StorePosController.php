<?php
/**
 * @desc    pos机列表控制器
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\controllers;
use Yii;
use app\controllers\BaseController;
use app\models\StorePosModel;
use app\dao\StoreStoreDAO;

class StorePosController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
    public function actionIndex()
	{
	    $this->redirect('store-pos/list-store-pos');	
	}
    
    public function actionListStorePos()
	{
	   return $this->render('list-store-pos');
	}

	/**
	 * @desc   获取列表数据
	 * @author hehuawei
	 * @date   2017-7-21
	 */
    public function actionGetStorePosList()
	{
	    $request = Yii::$app->request;
	    $pageInfo = [
	      'page'     => $request->get('page',1), 
		  'pageSize' => $request->get('pageSize',10)
	    ];
		$searchData = $request->get('searchData');
		$filter     = $request->get('filter');
        $storePosList  = StorePosModel::model()->getStorePosList($pageInfo,$searchData,$filter);
		if(count($storePosList['Body']['list']>0)){
		   $store_ids = $storeRs = array();
		   foreach($storePosList['Body']['list'] as &$v) $store_ids[$v['store_id']] = $v['store_id'];		   
		   if(count($store_ids)>0){
		        $storeList = StoreStoreDao::getInstance()->getStoreRs($store_ids);
				if(count($storeList)>0) foreach($storeList as $st) $storeRs[$st['store_id']] = $st['store_name'];
		   } 
		   foreach($storePosList['Body']['list'] as &$val){		   
		      $val['create_time'] = $val['create_time']>0 ? date('Y-m-d H:i:s',$val['create_time']) : '';
			  $val['store_name']  = isset($storeRs[$val['store_id']]) ?$storeRs[$val['store_id']] : '';
		   }	      
		}
	    $this->renderJson($storePosList);
	}

	/**
	 * @desc 保存添加/编辑
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionSaveStorePos()
	{
	    $request             = Yii::$app->request;
		$data['pos_id']      = $request->post('pos_id',0);
		$data['store_id']    = $request->post('store_id',0);
		$data['pos_name']    = $request->post('pos_name');
		$data['delete_flag'] = $request->post('delete_flag');
		$storeList  = StorePosModel::model()->saveStorePos($data);
        $this->renderJson($storeList);
	}
		

	/**
	 * @desc 编辑
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionCheckEditPos()
	{
		$request = Yii::$app->request;
		$pos_id  = $request->post('pos_id',0);
		$posData = StorePosModel::model()->getStorePosOne($pos_id);
		$store   = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson(array('posList'=>$posData['Body'],'store'=>$store));
	}


	/**
	 * @desc 检验是否有添加员工权限
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionCheckAddPos()
	{
		$store = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson($store);
	}





}