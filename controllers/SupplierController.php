<?php
namespace app\controllers;

use app\controllers\BaseController;
use Yii;
use app\models\SupplierModel;
use app\dao\SupplierDAO;
use app\dao\RegionDAO;
use app\enum\EnumOther;
use app\models\AdminModel;
use app\models\EmployeeModel;
use app\helpers\CInputFilter;
use app\models\UploadModel;

class SupplierController extends BaseController
{
	public $enableCsrfValidation = false;
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionIndex()
	{
		$this->redirect(['supplier/list-supplier']);
	}
	
	/**
	 * @desc 供应商列表列表
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionListSupplier()
	{
		return $this->render('supplierlist');
	}
	
	/**
	 * @desc 供应商明细
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionSupplierDet()
	{
		return $this->render('supplierdet');
	}
	
	/**
	 * @desc 创建供应商 
	 * @author liaojianwen
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionCreateSupplier()
	{
		
// 		$country = SupplierModel::model()->getRegion(0, 0);
// 		$country = SupplierModel::model()->getDefaultRegion(EnumOther::RE_COUNTRY,EnumOther::RE_COUNTRY,EnumOther::DEFAULT_COUNTRY);
// 		$province = SupplierModel::model()->getDefaultRegion(EnumOther::RE_PROVINCE,EnumOther::DEFAULT_COUNTRY,EnumOther::DEFAULT_PROVINCE);
// 		$city = SupplierModel::model()->getDefaultRegion(EnumOther::RE_CITY,EnumOther::DEFAULT_PROVINCE,EnumOther::DEFAULT_CITY);
// 		$district = SupplierModel::model()->getDefaultRegion(EnumOther::RE_DISTINCT,EnumOther::DEFAULT_CITY,EnumOther::DEFAULT_DISTINCT);
		$country = SupplierModel::model()->getDefaultRegion(EnumOther::RE_COUNTRY,EnumOther::DEFAULT_COUNTRY);
		$province = SupplierModel::model()->getDefaultRegion(EnumOther::DEFAULT_COUNTRY,EnumOther::DEFAULT_PROVINCE);
		$city = SupplierModel::model()->getDefaultRegion(EnumOther::DEFAULT_PROVINCE,EnumOther::DEFAULT_CITY);
		$district = SupplierModel::model()->getDefaultRegion(EnumOther::DEFAULT_CITY,EnumOther::DEFAULT_DISTINCT);
// 		$admin = (new AdminModel())->listAdmins();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render('create',['country'=>$country,'province'=>$province,'city'=>$city,'district'=>$district,'admin'=>$admin]);
	}
	
	/**
	 * @desc 检查供应商是否存在
	 * @author liaojianwen
	 * @date 2016-11-21
	 */
	public function actionCheckSupplier(){
		$request = Yii::$app->request;
	
		$type = $request->get('type', 'vendor');
		$val = $request->get($type, '');
		$id = $request->get('id','');
		if ($val){
			if($type =='vendor'){
				if(empty($id)){
					$supplierInfo = SupplierDAO::find ()->where ( 'supplier_name=:name and delete_flag =:flag', [ 
							':name' => $val,
							':flag' => EnumOther::NO_DELETE 
					] )->one ();
				} else {
					$supplierInfo = SupplierDAO::find ()->where ( 'supplier_name=:name and delete_flag =:flag and supplier_id != :id', [ 
							':name' => $val,
							':flag' => EnumOther::NO_DELETE,
							':id' => $id 
					] ) ->one();
				}
			}
			if (!empty($supplierInfo)){
				echo 'false';
				exit;
			}
		}
		echo 'true';
		exit;
	}
	
	/**
	 * @desc 获取供应商信息
	 * @author liaojianwen
	 * @date 2016-11-10
	 */
	public function actionGetSuppliers()
	{
		$request = Yii::$app->request;
		$pageInfo= [
			'page'=>$request->get('page',1),
			'pageSize'=>$request->get('pageSize',10),
		];
		$cond = $request->get('cond');
		$filter = $request->get('filter');
		$result = SupplierModel::model()->getSuppliers($cond, $filter, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据省id 获取城市list
	 * @author liaojianwen
	 * @date 2016-11-14
	 */
	public function actionGetCitys()
	{
		$request = Yii::$app->request;
		$province_id = $request->get('province');
		$result = SupplierModel::model()->getRegions($province_id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据city_id 获取 地区列表
	 * @author liaojianwen
	 * @date 2016-11-14
	 */
	public function actionGetDistrict()
	{
		$request = Yii::$app->request;
		$cit = $request->get('city');
		$result = SupplierModel::model()->getRegions($cit);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存供应商
	 * @author liaojianwen
	 * @date 2016-11-21
	 */
	public function actionSaveSupplier()
	{
		$request = Yii::$app->request;
		$result = SupplierModel::model()->saveSupplier($request->get());
		$this->renderJson($result);
	}
	
	/**
	 * @desc 编辑页面
	 * @author liaojianwen
	 * @date 2017-01-16
	 */
	public function actionSupplierEdit()
	{
		$request = Yii::$app->request;
		$id = $request->get ( 'id', 0 );
		$sinfo = SupplierDAO::find ()->where ( 'supplier_id =:id', [ 
				':id' => $id 
		] )->one ();
		$country = SupplierModel::model ()->getDefaultRegion ( EnumOther::RE_COUNTRY, $sinfo->country );
		$province = SupplierModel::model ()->getDefaultRegion ( $sinfo->country, $sinfo->province );
		$city = SupplierModel::model ()->getDefaultRegion ( $sinfo->province, $sinfo->city );
		$district = SupplierModel::model ()->getDefaultRegion ( $sinfo->city, $sinfo->district );
// 		$admin = (new AdminModel ())->listAdmins ();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'edit', [ 
				'country' => $country,
				'province' => $province,
				'city' => $city,
				'district' => $district,
				'admin' => $admin ,
		] );
	}
	
	/**
	 * @desc 根据id获取供应商信息
	 * @author liaojianwen
	 * @date 2016-11-22
	 */
	public function actionGetSupplierInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get ( 'id', 0 );
		$result = SupplierModel::model()->getSupplierInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除供应商
	 * @author liaojianwen
	 * @date 2016-11-22
	 */
	public function actionDelSupplier()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids','');
		$result = SupplierModel::model()->delSupplier($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有新增供应商权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddSupplier()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑供应商权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditSupplier()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有删除供应商
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelSupplier()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc上传文件后台处理
	 * @author liaojianwen
	 * @date 2017-06-29
	 */
	public function actionUploadPic()
	{
		$path = Yii::$app->params['upload_url'];//'statics/supplierImg/';
		$src = CInputFilter::getString('src');
		$result = UploadModel::model()->uploadImage($path, "", $src);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存供应商附件
	 * @author liaojianwen
	 * @date 2017-06-30
	 */
	public function actionSaveImg()
	{
		$imgs = CInputFilter::getArray('img', '');
		$removeImg = CInputFilter::getArray('rmimg', '');
		$supid = CInputFilter::getInt('supid',0);
		$result = SupplierModel::model()->saveImg($imgs, $removeImg, $supid);
		$this->renderJson($result);
	}
	
}