<?php

/**
 * @desc   商品保质期发消息控制器
 * @author hehuawei
 * @date   2017-11-20
 */
namespace app\controllers;
use Yii;
use yii\web\Controller;
use app\dao\ProductDAO;
use app\dao\CronCurlDAO;

class CronProductPastController extends Controller
{   
    public function actionSend()
	{
		$timeTemp = strtotime(date('Y-m-d',time()));
        $product = ProductDAO::getInstance()->getGoodsData($timeTemp);

		if(!empty($product)){
			$goods = $goods_ids = $goodsRs = $goodsRes = [];
		    foreach($product as $v){
			   // （保质期 - 当前时间）<（保质期 - 生产期）/3 
			   if( ($v['quality_time'] - $timeTemp) < ($v['quality_time'] - $v['produced_time'])/3 ){
			        $goods[] = $v;			    
			   }					
			}

			if(!empty($goods)){
				 $endRes = [];
				 foreach($goods as $info){
				    $endRes[] = $info['produced_time'].','.$info['quality_time'].','.$info['product_name'];
				 }
			     $url = Yii::$app->params ['shopUrl'] ."/api/send_wx_message.php?act=productPast";
                 //$url = 'http://www.yicailan.com/api/send_wx_message.php?act=productPast';
			     $result = CronCurlDAO::getInstance()->httpCurl($url, $endRes);
			}
				
		}	
	}	
}
