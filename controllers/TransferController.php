<?php
 namespace app\controllers;
 
 /**
  * @desc 调拨控制器
  */
 use app\controllers\BaseController;
 use app\enum\EnumOther;
 use app\enum\EnumOriginType;
 use app\models\EmployeeModel;
 use app\models\SupplierModel;
 use app\models\WarehouseModel;
 use app\models\TransferModel;
 use app\helpers\CInputFilter;
 use Yii;
		 
 class TransferController extends BaseController
 {
 	/**
 	 * @desc 默认控制器
 	 * @author liaojianwen
 	 * @date 2017-03-31
 	 */
 	public function actionIndex()
 	{
 		$this->redirect('transfer/list-transfer');
 	}
 	
 	/**
 	 * @desc 新增调拨单
 	 * @author liaojianwen
 	 * @date 2017-03-31
 	 */
 	public function actionAddTransfer()
 	{
 		$transferNo = EnumOriginType::TRANSFER.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$damage = EnumOther::$damage_type;
 		$transfer_type = EnumOriginType::$transfer_type;
 		return $this->render ( 'addtransfer', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $transferNo,
 				'damage'=> $damage,
 				'type' => $transfer_type,
 		] );
 	}
 	
 	/**
 	 * @desc 编辑调拨单
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionEditTransfer()
 	{
 		$transferNo = EnumOriginType::TRANSFER.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$damage = EnumOther::$damage_type;
 		$transfer_type = EnumOriginType::$transfer_type;
 		return $this->render ( 'edittransfer', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $transferNo,
 				'damage'=> $damage,
 				'type' => $transfer_type,
 		] );
 	}
 	
 	/**
 	 * @desc 调拨单列表
 	 * @author liaojianwen
 	 * @date 2017-03-31
 	 */
 	public function actionListTransfer()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('listtransfer', ['stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 保存调拨单
 	 * @author liaojianwen
 	 * @date 2017-03-31
 	 */
 	public function actionSaveTransfer()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$result = TransferModel::model ()->saveTransfer ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 获取调拨单列表数据
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionGetTransfers()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 10 )
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$filter = $request->get('filter');
 		
 		$result = TransferModel::model ()->getTransfers ( $pageInfo, $condition, $filter);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有编辑调拨单的权限
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionCheckEditTransfer()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检验是否有确认调拨单权限
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionCheckConfirmTransfer()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检验是否有删除调拨单权限
 	 * @author liaojianwen
 	 * @date 20174-04-01
 	 */
 	public function actionCheckDelTransfer()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检验是否有报废调拨单的权限
 	 * @author liaojianwen
 	 * @date 2017-04-07
 	 */
 	public function actionCheckInvalidTransfer()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 编辑页面调拨单数据
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionGetTransferInfo()
 	{
 		$id = Yii::$app->request->get('id');
 		$result = TransferModel::model()->getTransferInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 确认调拨单
 	 * @author liaojianwen
 	 * @date 2017-04-01
 	 */
 	public function actionConfirmTransfer()
 	{
 		$id = Yii::$app->request->get('id',0);
 		$result = TransferModel::model()->confirmTransfer($id);
 		$this->renderJson($result);
 	}

 	/**
 	 * @desc 获取调拨单明细（弹窗框）
 	 * @author liaojianwen
 	 * @date 2017-04-05
 	 */
 	public function actionGetTransferDet()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = TransferModel::model()->getTransferDet($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 删除调拨单（未确认）
 	 * @author liaojianwen
 	 * @date 2017-04-07
 	 */
 	public function actionDelTransfer()
 	{
 		$ids = CInputFilter::getnorepeatInts('ids');
 		
 		$result = TransferModel::model()->delTransfer($ids);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 调拨单统计
 	 * @author liaojianwen
 	 * @date 2017-11-23
 	 */
 	public function actionTransferStatistics()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('statistics', ['stock'=> $stock]);
 	}
 	
 	/**
 	 * @desc 获取统计信息
 	 * @author liaojianwen
 	 * @date 2017-11-23
 	 */
 	public function actionGetTransferStatistics()
 	{
//  		$request = Yii::$app->request;
		
 		$pageInfo = [
 				'page' => CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond','');
 		$filter = CInputFilter::getInt('filter');
 		$result = TransferModel::model ()->getTransferStatistics ( $pageInfo, $condition, $filter);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有导出调拨统计信息功能
 	 * @author liaojianwen
 	 * @date 2017-12-14
 	 */
 	public function actionCheckTransferStatisticsExport()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 调拨统计信息导出execl
 	 * @author liaojianwen
 	 * @date 2017-12-14
 	 */
 	public function actionExportTransferStatistics()
 	{
 		$condition = [
 				'starTime'=>CInputFilter::getString('stime',''),
 				'endTime' => CInputFilter::getString('etime',''),
 				'warehouse_out' => CInputFilter::getInt('outware',0),
 				'warehouse_in' => CInputFilter::getInt('inware',0),
 				'id'=> CInputFilter::getInt('id'),
 				'name' => CInputFilter::getString('name'),
 		];
 		
 		$pageInfo = TransferModel::model()->getTransferStatistics('',$condition);
 		if(empty($pageInfo['Body']['list'])){
 			$this->renderJson('error,no data found');
 		}
 		$title = '调拨统计报表';
 		$fileName = '调拨统计报表'.rand(0, 1000);
 		
 		$dataInfo = [
 			'list' =>$pageInfo['Body']['list'],
 			'starTime' => $condition['starTime'],
 			'endTime' => $condition['endTime'],
 		];
 		TransferModel::model()->exportTransferStatistics($dataInfo,$fileName,$title);
 		
 		exit;
 	}
	
	/**
	 * @desc 需导出调拨单
	 * @author liaojianwen
	 * @date 2017-09-27
	 */
	public function actionGetExportTransfer()
	{
		$pageInfo = [
			'page' => CInputFilter::getInt( 'page', 1),
			'pageSize' => CInputFilter::getInt('pageSize', 10),
		];
		$cond = CInputFilter::getArray('cond', '');
		
		$filter = CInputFilter::getInt('filter', 0);
		$result = TransferModel::model()->getExportTransfer($cond, $pageInfo, $filter);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有导出权限
	 * @author liaojianwen
	 * @date 2017-09-28
	 */
	public function actionCheckTransferExport()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出订单
	 * @author liaojianwen
	 * @date 2017-09-28
	 */
	public function actionExportProInfo()
	{
		$condition = [
				'name'=>CInputFilter::getString('name',''),
				'stime' => CInputFilter::getString('stime'),
				'etime'=> CInputFilter::getString('etime'),
		];
		$proInfos = TransferModel::model()->getExproInfo($condition);
		if(empty($proInfos)){
			$this->renderJson('error,no data found');
		}
		$title = '损耗商品明细表';
		$fileName = '损耗商品明细表'.$condition['stime'];
		TransferModel::model()->exportProInfo($proInfos,$fileName,$title);
		exit;
	}
 }