<?php
namespace app\controllers;
/**
 * @desc 采购单控制器
 * @author liaojianwen
 * @date 2016-12-09
 */
use Yii;
use app\controllers\BaseController;
use app\models\WarehouseModel;
use app\models\SupplierModel;
use app\enum\EnumOriginType;
use app\models\EmployeeModel;
use app\models\ProcurementModel;
use app\helpers\CInputFilter;

class ProcurementController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function actionIndex()
	{
		$this->redirect('list-procurement');
	}
	
	/**
	 * @desc 采购单列表
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function actionListProcurement()
	{
		$vendor = SupplierModel::model()->getSupplierList();
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('procurementlist',['vendor'=>$vendor, 'stock'=>$stock]);
	}
	
	/**
	 * @desc 新增采购单
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function actionAddProcurement()
	{
		$proNo = EnumOriginType::PROCUREMENT.date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$instore_type;
		return $this->render ( 'addprocurement', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $proNo
		] );
	}
	
	/**
	 * @保存采购单
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function actionSaveProcurement()
	{
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id');
		$remove = CInputFilter::getArray('remove','');
		$result = ProcurementModel::model ()->saveProcureMent ( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 获取采购单列表数据
	 * @author liaojianwen
	 * @date 2016-12-12
	 */
	public function actionGetProcurements()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		
		$result = ProcurementModel::model()->getProcurements($pageInfo, $condition);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 编辑页面
	 * @author liaojianwen
	 * @date 2016-12-12
	 */
	public function actionEditProcurement()
	{
		$instoreNo = EnumOriginType::PROCUREMENT. date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$instore_type;
		return $this->render ( 'editprocurement', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $instoreNo
		]);
	}
	
	/**
	 * @desc 获取采购单明细信息
	 * @author liaojianwen
	 * @date 2016-12-12
	 */
	public function actionGetProcurementInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = ProcurementModel::model()->getProcurementInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除采购单
	 * @author liaojianwen
	 * @date 2016-12-12
	 */
	public function actionDelProcurement()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		
		$result = ProcurementModel::model()->delProcurement($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取采购单的明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	public function actionGetProcurementDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = ProcurementModel::model()->getProcurementDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有编辑采购单功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditPro()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有删除采购单功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelPro()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 获取销售订单
	 * @author liaojianwen
	 * @date 2017-09-06
	 */
	public function actionSelectSellOrder()
	{
		$pageInfo = [
				'page' => CInputFilter::getInt( 'page', 1 ),
				'pageSize' => CInputFilter::getInt( 'pageSize', 5)
		];
		$condition = CInputFilter::getArray( 'cond', '');
		$result = ProcurementModel::model()->selectSellOrder($condition, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查销售订单是否被占用
	 * @author liaojianwen
	 * @date 2017-09-26
	 */
	public function actionCheckSellUsed()
	{
		$id = CInputFilter::getInt('id',0);
		$result = ProcurementModel::model()->checkSellUsed($id);
		$this->renderJson($result);
	}
	
	
	/**
	 * @desc 销售出库导入销售订单
	 * @author liaojianwen
	 * @date 2017-05-03
	 */
	public function actionImplodeSellInfo()
	{
		$id = CInputFilter::getInt('id');
		$result = SaleModel::model()->implodeSellInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 需导出采购订单
	 * @author liaojianwen
	 * @date 2017-09-27
	 */
	public function actionGetExportProcurement()
	{
		$pageInfo = [
			'page' => CInputFilter::getInt( 'page', 1),
			'pageSize' => CInputFilter::getInt('pageSize', 10),
		];
		$cond = CInputFilter::getArray('cond', '');
		
		$filter = CInputFilter::getInt('filter', 0);
		$result = ProcurementModel::model()->getExportProcurement($cond, $pageInfo, $filter);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有导出权限
	 * @author liaojianwen
	 * @date 2017-09-28
	 */
	public function actionCheckProcurementExport()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出订单
	 * @author liaojianwen
	 * @date 2017-09-28
	 */
	public function actionExportProInfo()
	{
		$condition = [
				'name'=>CInputFilter::getString('name',''),
				'vendor' => CInputFilter::getInt('vendor',''),
				'stime' => CInputFilter::getString('stime'),
				'etime'=> CInputFilter::getString('etime'),
				'filter' => CInputFilter::getInt('filter'),
		];
		$proInfos = ProcurementModel::model()->getExproInfo($condition);
		if(empty($proInfos)){
			$this->renderJson('error,no data found');
		}
		$title = '订单商品';
		$fileName = '采购订单'.rand();
		ProcurementModel::model()->exportProInfo($proInfos,$fileName,$title);
		exit;
	}
}
