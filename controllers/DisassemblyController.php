<?php
 namespace app\controllers;
 
 /**
  * @desc 拆装单
  */
 use Yii;
 use app\controllers\BaseController;
 use app\enum\EnumOther;
 use app\enum\EnumOriginType;
 use app\models\WarehouseModel;
 use app\models\EmployeeModel;
 use app\models\SupplierModel;
use app\models\DisassemblyModel;
use app\helpers\CInputFilter;
		 
 class DisassemblyController extends BaseController
 {
 	public function actionIndex()
 	{
 		
 	}
 	
	 /**
 	 * @desc 新增拆装单
 	 * @author liaojianwen
 	 * @date 2017-10-09
 	 */
 	public function actionAddDisassembly()
 	{
 		$disassemblyNo = EnumOriginType::DISASSEMBLY.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render ( 'adddisassembly', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $disassemblyNo
 		] );
 	}
 	
 	/**
 	 * @desc 编辑拆装单
 	 * @author liaojianwen
 	 * @date 2017-10-09
 	 */
 	public function actionEditDisassembly()
 	{
 		$disassemblyNo = EnumOriginType::DISASSEMBLY.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render ( 'editdisassembly', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $disassemblyNo
 		] );
 	}
 	
 	/**
 	 * @desc 拆装单列表
 	 * @author liaojianwen
 	 * @date 2017-10-09
 	 */
 	public function actionListDisassembly()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('listdisassembly', ['stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 保存拆装单
 	 * @author liaojianwen
 	 * @date 2017-10-17
 	 */
 	public function actionSaveDisassembly()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail_out = CInputFilter::getArray('det_out', '');
 		$detail_in = CInputFilter::getArray('det_in', '');
 		$detail = (array_merge($detail_out, $detail_in));
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$result = DisassemblyModel::model ()->saveDisassembly ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 获取拆卸单列表
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function actionGetDisassembly()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$filter = CInputFilter::getInt('filter');
 			
 		$result = DisassemblyModel::model ()->getDisassembly ( $pageInfo, $condition, $filter);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有编辑功能
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function actionCheckEditDisassembly()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 编辑页面明细
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function actionGetDisassemblyInfo()
 	{
 		$id = CInputFilter::getInt('id',0);
 		$result = DisassemblyModel::model()->getDisassemblyInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有确认权限
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function actionCheckConfirmDisassembly()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 确认拆装单
 	 * @author liaojianwen
 	 * @date 2017-10-19
 	 */
 	public function actionConfirmDisassembly()
 	{
 		$id = CInputFilter::getInt('id',0);
 		$result = DisassemblyModel::model()->confirmDisassembly($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有删除权限
 	 * @author liaojianwen
 	 * @date 2017-10-20
 	 */
 	
 	public function actionCheckDelDisassembly()
 	{
 		$this->renderJson('');
 	}
	
 	/**
 	 * @desc 删除拆装单
 	 * @author liaojianwen
 	 * @date 2017-10-20
 	 */
 	public function actionDelDisassembly()
 	{
 		$ids = CInputFilter::getnorepeatInts('ids');
 			
 		$result = DisassemblyModel::model()->delDisassembly($ids);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 根据仓库获取仓库下面的商品
 	 * @author liaojianwen
 	 * @date 2017-01-19
 	 */
 	public function actionGetDisassemblyProductByWare()
 	{
 		$request = Yii::$app->request;
 		$ware_id = $request->get('ware');
 	
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize'=>$request->get('pageSize',5),
 		];
 		$name = $request->get('name');
 		$result = DisassemblyModel::model()->getDisassemblyProductByWare($ware_id, $name, $pageInfo);
 		$this->renderJson($result);
 	
 	}
 }