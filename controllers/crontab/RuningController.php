<?php
 namespace app\controllers\crontab;
 
 
 use yii\base\Controller;
 use app\helpers\Curl;
 use Yii;
use app\helpers\storeTokenTool;
use app\dao\StoreDAO;
use app\dao\SyncInfoDAO;
use app\enum\EnumOther;
				 
 class RuningController extends Controller
 {
 	public function actionIndex()
 	{
 		
 	}
 	
 	public function actionScmJob001()
 	{
 		$this->syncStoreInventoryLost();
 	}
 	
 	/**
 	 * @desc 同步store 盘亏单到scm
 	 * @author liaojianwen
 	 * @date 2018-02-26
 	 */
 	private function syncStoreInventoryLost()
 	{
 		
 		$storeUrl = Yii::$app->params ['storeUrl'];
 		$key = storeTokenTool::getInstance()->getToken();
 		$url = "{$storeUrl}/scm/api/sync-inventory-lost";
 		
 		$res = SyncInfoDAO::getInstance()->iselect("inventory_lost_sync_time", "1=:num", [':num'=>1], 'one');
 		
 		 
//  		$arr = [
//  				'data' => [
//  						'goods_name' => 9999,
//  						'supplier_id' => 2,
//  						'goods_unit' => 3,
//  						'cat_id' => 2,
//  						'good_id' => 1,
//  						'attr_id' => 1212
//  				]
//  		];
// //  		    	dd($url);
//  		$postdata = json_encode($arr);
		if(!$res){
			$startTime = time() - EnumOther::INVENTORY_MAX_SYNC_DATE;
		} else{
			if(time() - $res['inventory_lost_sync_time'] > 2*3600){
				$startTime = $res['inventory_lost_sync_time'] -EnumOther::OVARLAP_TIME;
			}else {
				return;
			}
		}
			
			$endTime = time();
			$urlParam = "startTime={$startTime}&endTime={$endTime}";
	 		$data = Curl::curlOption($url, 'GET', $urlParam, $key);
// 	 		dd($data);
	 		$inv_array = json_decode($data, true);
	 		if($inv_array['ack'] =='success'){
	 			//插入供应链 store_inventory_lost_transaction
	 			
	 		}
 	}
 }