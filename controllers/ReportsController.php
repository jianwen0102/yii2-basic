<?php
 namespace app\controllers;
 
 /**
  * @desc 报表控制器
  */
 use app\controllers\BaseController;
 use Yii;
 use app\models\WarehouseModel;
 use app\models\ReportsModel;
 use app\helpers\CInputFilter;
 use yii\log\FileTarget;
				 
 class ReportsController extends BaseController
 {
 	
//  	/**
//  	 * @desc 销售毛利明细表
//  	 * @author liaojianwen
//  	 * @date 2018-02-05
//  	 */
//  	public function actionSaleGrossProfitDetails()
//  	{
//  		$warehouse = WarehouseModel::model()->listWarehouse();
//  		return $this->render('gross_profit_details',['stock' => $warehouse]);
//  	}
 	
//  	/**
//  	 * @desc 销售毛利明细
//  	 * @author liaojianwen
//  	 * @date 2018-02-05
//  	 */
//  	public function actionGetSaleGrossProfitDetail()
//  	{
//  		$pageInfo = [
//  				'page' => CInputFilter::getInt( 'page', 1 ),
//  				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
//  		];
//  		$condition = CInputFilter::getArray( 'cond', '');
//  		$result = ReportsModel::model()->getSaleGrossProfitDetail ( $pageInfo, $condition);
//  		$this->renderJson ( $result );
 		
//  	}
 	
 	/**
 	 * @desc 销售毛利汇总表
 	 * @author liaojianwen
 	 * @date 2018-02-05
 	 */
 	public function actionSaleGrossProfitSummary()
 	{
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		return $this->render('gross_profit_summary',['stock' => $warehouse]);
 	}
 	
 	/**
 	 * @desc 销售毛利汇总数据
 	 * @author liaojianwen
 	 * @date 2018-02-05
 	 */
 	public function actionGetSaleGrossProfitSummary()
 	{
 		$pageInfo = [
 				'page' => CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$result = ReportsModel::model()->getSaleGrossProfitSummary ( $pageInfo, $condition);
 		$this->renderJson ( $result );
 	}
 	
 	
 	/**
 	 * @desc 特定商品毛利汇总表（需分拆的商品）
 	 * @author liaojianwen
 	 * @date 2018-04-12
 	 */
 	public function actionSpecialGrossProfitSummary()
 	{
		$goods = ReportsModel::model()->getSpecialPro();
 		return $this->render('special_gross_profit',['goods' => $goods]);
 	}
 	
 	/**
 	 * @desc 特定商品销售毛利汇总数据
 	 * @author liaojianwen
 	 * @date 2018-04-12
 	 */
 	public function actionGetSpecialGrossProfit()
 	{
 		$pageInfo = [
 				'page' => CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$goods_id = CInputFilter::getInt( 'filter');
 		$result = ReportsModel::model()->getSpecialGrossProfitSummary ( $pageInfo, $condition, $goods_id);
 		$this->renderJson ( $result );
 	}
 }