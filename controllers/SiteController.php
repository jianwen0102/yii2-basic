<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\controllers\BaseController;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use app\enum\RoleEnum;
use app\dao\ProductDAO;
use app\enum\EnumOther;
use app\dao\ProductUnitDAO;
use app\helpers\scmTokenTool;
use app\helpers\Curl;
use app\dao\CategoryDAO;
use yii\rbac\Role;
use app\dao\StockPileDAO;
use yii\base\Object;
use app\models\WapLoginForm;
use yii\debug\models\search\Log;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','init','add-items','init-unit-content','test','shop','cat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
    */
    public function actions()
    {
    	return [
    		'captcha' => [
    				'class' => 'yii\captcha\CaptchaAction',
    				'maxLength' => 4,
    				'minLength' => 4,
    				'width' => 80,
    				'height' => 40,
//     				'backColor'=>'#696c74',
    		],
    		'error'=>[
    				'class' => 'yii\web\ErrorAction',
    		],
    	];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
    	if(Yii::$app->deviceDetect->isMobile()){
    		$this->redirect("/wap/home/login/");
    	}
        $this->layout='login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$redirect = Yii::$app->request->get('redirect', '');
			if (!empty($redirect))
			{
				return Yii::$app->getResponse()->redirect($redirect);
			}
			else
			{
				return $this->goBack();
			}
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
// 		$this->redirect(['site/index']);
        return $this->goHome();
    }
    
    
    /**
     * @desc 初始化权限列表
     * @author liaojianwen
     */
    public function actionInit()
    {
//     	dd($this->route);
		//初始化权限 设置管理员权限
		
		$auth = Yii::$app->authManager;
		
		$admin = $auth->createRole('admin');
		$admin->description = '管理员权限';
		$auth->add($admin);
		$auth->assign($admin, 1);//
		
		echo "创建“管理员”角色\n";
		
		/*类目设置 */
		$cateList = $auth->createPermission(RoleEnum::ROLE_CATEGORY_LIST);
		$cateList->description = '类目管理';
		$auth->add($cateList);
		$auth->addChild($admin, $cateList);
		
		
		echo "创建类目列表权限\n";
		
		$cateUpdate = $auth->createPermission(RoleEnum::ROLE_CATEGORY_SAVE);
		$cateUpdate->description = '新增/更新类目';
		$auth->add($cateUpdate);
		$auth->addChild($admin, $cateUpdate);
		
		echo "创建类目新增权限\n";
		
		$cateDel = $auth->createPermission(RoleEnum::ROLE_CATEGORY_DEL);
		$cateDel->description = '删除类目';
		$auth->add($cateDel);
		$auth->addChild($admin, $cateDel);
		
		echo "创建类目删除权限\n";
		
		/* 商品管理*/
		$productList = $auth->createPermission(RoleEnum::ROLE_PRODUCT_LIST);
		$productList->description = '商品管理';
		$auth->add($productList);
		$auth->addChild($admin, $productList);
		
		echo "创建商品列表权限\n";
		
		$productAdd = $auth->createPermission(RoleEnum::ROLE_PRODUCT_ADD);
		$productAdd->description = '新增商品';
		$auth->add($productAdd);
		$auth->addChild($admin, $productAdd);
		
		echo "创建新增商品权限\n";
		$productEdit = $auth->createPermission(RoleEnum::ROLE_PRODUCT_EDIT);
		$productEdit->description = '编辑商品';
		$auth->add($productEdit);
		$auth->addChild($admin, $productEdit);
		
		echo "创建商品编辑权限\n";
		
		$productDel = $auth->createPermission(RoleEnum::ROLE_PRODUCT_DEL);
		$productDel->description = '删除商品';
		$auth->add($productDel);
		$auth->addChild($admin, $productDel);
		
		echo "创建商品删除权限\n";
		
		//部门管理
		$departmentList = $auth->createPermission(RoleEnum::ROLE_DEPARTMENT_LIST);
		$departmentList->description = '部门管理';
		$auth->add($departmentList);
		$auth->addChild($admin, $departmentList);
		
		echo "创建部门列表权限\n";
		
		$departmentAdd = $auth->createPermission(RoleEnum::ROLE_DEPAETMENT_ADD);
		$departmentAdd->description = '新增部门';
		$auth->add($departmentAdd);
		$auth->addChild($admin, $departmentAdd);
		
		echo "创建新增部门权限\n";
		
		$departmentEdit = $auth->createPermission(RoleEnum::ROLE_DEPARTMENT_EDIT);
		$departmentEdit->description = '编辑部门';
		$auth->add($departmentEdit);
		$auth->addChild($admin, $departmentEdit);
		
		echo "创建编辑部门权限\n";
		
		$departmentDel = $auth->createPermission(RoleEnum::ROLE_DEPARTMENT_DEL);
		$departmentDel->description = '删除部门';
		$auth->add($departmentDel);
		$auth->addChild($admin, $departmentDel);
		
		echo "创建删除部门权限\n";
		
		//员工管理
		
		$employeeList = $auth->createPermission(RoleEnum::ROLE_EMPLOYEE_LIST);
		$employeeList->description = '员工管理';
		$auth->add($employeeList);
		$auth->addChild($admin, $employeeList);
		
		echo "创建员工列表权限\n";
		
		$employeeAdd = $auth->createPermission(RoleEnum::ROLE_EMPLOYEE_ADD);
		$employeeAdd->description = '新增员工';
		$auth->add($employeeAdd);
		$auth->addChild($admin, $employeeAdd);
		
		echo "创建新增员工权限\n";
		
		$employeeEdit = $auth->createPermission(RoleEnum::ROLE_EMPLOYEE_EDIT);
		$employeeEdit->description = '编辑员工';
		$auth->add($employeeEdit);
		$auth->addChild($admin, $employeeEdit);
		
		echo "创建编辑员工权限\n";
		
		$employeeDel = $auth->createPermission(RoleEnum::ROLE_EMPLOYEE_DEL);
		$employeeDel->description = '删除员工';
		$auth->add($employeeDel);
		$auth->addChild($admin, $employeeDel);
		
		echo "创建删除员工权限\n";
		
		//供应商管理
		$vendorList = $auth->createPermission(RoleEnum::ROLE_VENDOR_LIST);
		$vendorList->description = '供应商管理';
		$auth->add($vendorList);
		$auth->addChild($admin, $vendorList);
		
		echo "创建供应商列表权限\n";
		
		$vendorAdd = $auth->createPermission(RoleEnum::ROLE_VENDOR_ADD);
		$vendorAdd->description = '新增供应商';
		$auth->add($vendorAdd);
		$auth->addChild($admin, $vendorAdd);
		
		echo "创建新增供应商权限\n";
		
		$vendorEdit = $auth->createPermission(RoleEnum::ROLE_VENDOR_EDIT);
		$vendorEdit->description = '编辑供应商';
		$auth->add($vendorEdit);
		$auth->addChild($admin, $vendorEdit);
		
		echo "创建编辑供应商权限\n";
		
		$vendorDel = $auth->createPermission(RoleEnum::ROLE_VENDOR_DEL);
		$vendorDel->description = '删除供应商';
		$auth->add($vendorDel);
		$auth->addChild($admin, $vendorDel);
		
		echo "创建删除供应商权限\n";
		
		//商品更新日志
		$productLog = $auth->createPermission(RoleEnum::ROLE_PRODUCTLOG_LIST);
		$productLog->description = '商品更新日志';
		$auth->add($productLog);
		$auth->addChild($admin, $productLog);
		
		echo "创建商品更新日志权限\n";
		
		//采购单管理
		$procurementADD = $auth->createPermission(RoleEnum::ROLE_PROCUREMENT_ADD);
		$procurementADD->description = '新增采购单';
		$auth->add($procurementADD);
		$auth->addChild($admin, $procurementADD);
		
		echo "创建新增采购单权限\n";
		

		$procurementList = $auth->createPermission(RoleEnum::ROLE_PROCUREMENT_LIST);
		$procurementList->description = '采购单列表';
		$auth->add($procurementList);
		$auth->addChild($admin, $procurementList);
		
		echo "创建采购单列表权限\n";
		

		$procurementEdit = $auth->createPermission(RoleEnum::ROLE_PROCUREMENT_EDIT);
		$procurementEdit->description = '编辑采购单';
		$auth->add($procurementEdit);
		$auth->addChild($admin, $procurementEdit);
		
		echo "创建编辑采购单权限\n";
		

		$procurementDel = $auth->createPermission(RoleEnum::ROLE_PROCUREMENT_DEL);
		$procurementDel->description = '删除采购单';
		$auth->add($procurementDel);
		$auth->addChild($admin, $procurementDel);
		
		echo "创建删除采购单权限\n";
		
		//采购入库单
		$purchaseList = $auth->createPermission(RoleEnum::ROLE_PURCHASE_LIST);
		$purchaseList->description = '采购入库列表';
		$auth->add($purchaseList);
		$auth->addChild($admin, $purchaseList);
		
		echo "创建采购入库管理权限\n";
		
		$purchaseAdd = $auth->createPermission(RoleEnum::ROLE_PURCHASE_ADD);
		$purchaseAdd->description = '新增采购入库';
		$auth->add($purchaseAdd);
		$auth->addChild($admin, $purchaseAdd);
		
		echo "创建新增采购入库权限\n";
		
		$purchaseEdit = $auth->createPermission(RoleEnum::ROLE_PURCHASE_EDIT);
		$purchaseEdit->description = '编辑采购入库';
		$auth->add($purchaseEdit);
		$auth->addChild($admin, $purchaseEdit);
		
		echo "创建编辑采购入库权限\n";
		
		$purchaseDel = $auth->createPermission(RoleEnum::ROLE_PURCHASE_DEL);
		$purchaseDel->description = '删除采购入库';
		$auth->add($purchaseDel);
		$auth->addChild($admin, $purchaseDel);
		
		echo "创建删除采购入库权限\n";
		
		$purchaseConfirm = $auth->createPermission(RoleEnum::ROLE_PURCHASE_CONFIRM);
		$purchaseConfirm->description = '确认采购入库';
		$auth->add($purchaseConfirm);
		$auth->addChild($admin, $purchaseConfirm);
		
		echo "创建确认采购入库权限\n";
		
		//其他入库
		$instoreList = $auth->createPermission(RoleEnum::ROLE_INSTORE_LIST);
		$instoreList->description = '其他入库列表';
		$auth->add($instoreList);
		$auth->addChild($admin, $instoreList);
		
		echo "创建其他入库列表权限\n";
		
		$instoreAdd = $auth->createPermission(RoleEnum::ROLE_INSTORE_ADD);
		$instoreAdd->description = '新增其他入库';
		$auth->add($instoreAdd);
		$auth->addChild($admin, $instoreAdd);
		
		echo "创建新增其他入库权限\n";
		
		$instoreEdit = $auth->createPermission(RoleEnum::ROLE_INSTORE_EDIT);
		$instoreEdit->description = '编辑其他入库';
		$auth->add($instoreEdit);
		$auth->addChild($admin, $instoreEdit);
		
		echo "创建编辑其他入库权限\n";
		
		$instoreDel = $auth->createPermission(RoleEnum::ROLE_INSTORE_DEL);
		$instoreDel->description = '删除其他入库';
		$auth->add($instoreDel);
		$auth->addChild($admin, $instoreDel);
		
		echo "创建删除其他入库权限\n";
		
		$instoreConfirm = $auth->createPermission(RoleEnum::ROLE_INSTORE_CONFIRM);
		$instoreConfirm->description = '确认其他入库';
		$auth->add($instoreConfirm);
		$auth->addChild($admin, $instoreConfirm);
		
		echo "创建确认其他入库权限\n";
		
		//其他出库
		$outstoreList = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_LIST);
		$outstoreList->description = '其他出库列表';
		$auth->add($outstoreList);
		$auth->addChild($admin, $outstoreList);
		
		echo "创建其他出库列表权限\n";
		
		$outstoreAdd = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_ADD);
		$outstoreAdd->description = '新增其他出库';
		$auth->add($outstoreAdd);
		$auth->addChild($admin, $outstoreAdd);
		
		echo "创建新增其他出库权限\n";
		
		$outstoreEdit = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_EDIT);
		$outstoreEdit->description = '编辑其他出库';
		$auth->add($outstoreEdit);
		$auth->addChild($admin, $outstoreEdit);
		
		echo "创建编辑其他出库权限\n";
		
		$outstoreDel = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_DEL);
		$outstoreDel->description = '删除其他出库';
		$auth->add($outstoreDel);
		$auth->addChild($admin, $outstoreDel);
		
		echo "创建删除其他出库权限\n";
		
		$outstoreConfirm = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_CONFIRM);
		$outstoreConfirm->description = '确认其他出库';
		$auth->add($outstoreConfirm);
		$auth->addChild($admin, $outstoreConfirm);
		
		echo "创建确认其他出库权限\n";
		
		//仓库信息
		$warehouseList = $auth->createPermission(RoleEnum::ROLE_WAREHOUSE_LIST);
		$warehouseList->description = '仓库信息';
		$auth->add($warehouseList);
		$auth->addChild($admin, $warehouseList);
		
		echo "创建仓库信息权限\n";
		
		$warehouseAdd = $auth->createPermission(RoleEnum::ROLE_WAREHOUSE_ADD);
		$warehouseAdd->description = '新增仓库';
		$auth->add($warehouseAdd);
		$auth->addChild($admin, $warehouseAdd);
		
		echo "创建新增仓库权限\n";
		
		$warehouseEdit = $auth->createPermission(RoleEnum::ROLE_WAREHOUSE_EDIT);
		$warehouseEdit->description = '编辑仓库';
		$auth->add($warehouseEdit);
		$auth->addChild($admin, $warehouseEdit);
		
		echo "创建编辑仓库权限\n";
		
		$warehouseDel = $auth->createPermission(RoleEnum::ROLE_WAREHOUSE_DEL);
		$warehouseDel->description = '删除仓库';
		$auth->add($warehouseDel);
		$auth->addChild($admin, $warehouseDel);
		
		echo "创建删除仓库权限\n";
		
		//单位信息
		$unitList = $auth->createPermission(RoleEnum::ROLE_UNIT_LIST);
		$unitList->description = '单位信息';
		$auth->add($unitList);
		$auth->addChild($admin, $unitList);
		
		echo "创建单位信息权限\n";
		
		$unitAdd = $auth->createPermission(RoleEnum::ROLE_UNIT_ADD);
		$unitAdd->description = '新增单位';
		$auth->add($unitAdd);
		$auth->addChild($admin, $unitAdd);
		
		echo "创建新增单位权限\n";
		
		$unitEdit = $auth->createPermission(RoleEnum::ROLE_UNIT_EDIT);
		$unitEdit->description = '编辑单位';
		$auth->add($unitEdit);
		$auth->addChild($admin, $unitEdit);
		
		echo "创建编辑单位权限\n";
		
		$unitDel = $auth->createPermission(RoleEnum::ROLE_UNIT_DEL);
		$unitDel->description = '删除单位';
		$auth->add($unitDel);
		$auth->addChild($admin, $unitDel);
		
		echo "创建删除单位权限\n";
		
		//库存信息
		$productStock = $auth->createPermission(RoleEnum::ROLE_PRODUCTSTOCK_LIST);
		$productStock->description = '库存信息';
		$auth->add($productStock);
		$auth->addChild($admin, $productStock);
		
		echo "创建库存信息权限\n";
		
		//出入库明细
		$transactionList = $auth->createPermission(RoleEnum::ROLE_TRANSACTION_LIST);
		$transactionList->description = '出入库明细';
		$auth->add($transactionList);
		$auth->addChild($admin, $transactionList);
		
		echo "创建出入库明细权限\n";
		
		//分仓库存查询
		$stockPile = $auth->createPermission(RoleEnum::ROLE_STOCKPILE_LIST);
		$stockPile->description = '分仓库存查询';
		$auth->add($stockPile);
		$auth->addChild($admin, $stockPile);
		
		echo "创建分仓库存查询权限\n";
		
		//个人信息
		$personalInfo = $auth->createPermission(RoleEnum::ROLE_PERSONAL_LIST);
		$personalInfo->description = '个人信息';
		$auth->add($personalInfo);
		$auth->addChild($admin, $personalInfo);
		
		echo "创建个人信息权限\n";
		
		$personalEdit = $auth->createPermission(RoleEnum::ROLE_PERSONAL_CHANGEPWD);
		$personalEdit->description = '编辑个人信息';
		$auth->add($personalEdit);
		$auth->addChild($admin, $personalEdit);
		
		echo "创建编辑个人信息权限\n";
		
		//用户管理
		$userList = $auth->createPermission(RoleEnum::ROLE_ADMIN_LIST);
		$userList->description = '管理员列表';
		$auth->add($userList);
		$auth->addChild($admin, $userList);
		
		echo "创建管理员列表权限\n";
		
		$userAdd = $auth->createPermission(RoleEnum::ROLE_ADMIN_ADD);
		$userAdd->description = '新增管理员';
		$auth->add($userAdd);
		$auth->addChild($admin, $userAdd);
		
		echo "创建新增管理员权限\n";
		
		$userEdit = $auth->createPermission(RoleEnum::ROLE_ADMIN_EDIT);
		$userEdit->description = '编辑管理员';
		$auth->add($userEdit);
		$auth->addChild($admin, $userEdit);
		
		echo "创建编辑管理员权限\n";
		
		$userDel = $auth->createPermission(RoleEnum::ROLE_ADMIN_DEL);
		$userDel->description = '删除管理员';
		$auth->add($userDel);
		$auth->addChild($admin, $userDel);
		
		echo "创建删除管理员权限\n";
		
		$userAssign = $auth->createPermission(RoleEnum::ROLE_ADMIN_ASSIGN);
		$userAssign->description = '管理员角色分配';
		$auth->add($userAssign);
		$auth->addChild($admin, $userAssign);
		
		echo "创建删除管理员权限\n";
		
		//权限分组
		$permissionList = $auth->createPermission(RoleEnum::ROLE_PERMISSION_LIST);
		$permissionList->description = '权限分组列表';
		$auth->add($permissionList);
		$auth->addChild($admin, $permissionList);
		
		echo "创建权限分组权限\n";
		
		$permissionAdd = $auth->createPermission(RoleEnum::ROLE_PERMISSION_ADD);
		$permissionAdd->description = '新增权限分组';
		$auth->add($permissionAdd);
		$auth->addChild($admin, $permissionAdd);
		
		echo "创建新增权限分组权限\n";
		
		$permissionEdit = $auth->createPermission(RoleEnum::ROLE_PERMISSION_EDIT);
		$permissionEdit->description = '编辑权限分组';
		$auth->add($permissionEdit);
		$auth->addChild($admin, $permissionEdit);
		
		echo "创建编辑权限分组权限\n";
		
		$permissionDel = $auth->createPermission(RoleEnum::ROLE_PERMISSION_DEL);
		$permissionDel->description = '删除权限分组';
		$auth->add($permissionDel);
		$auth->addChild($admin, $permissionDel);
		
		echo "创建删除权限分组权限\n";
		
		$permissionAssign = $auth->createPermission(RoleEnum::ROLE_PERMISSION_ASSIGN);
		$permissionAssign->description = '权限分配';
		$auth->add($permissionAssign);
		$auth->addChild($admin, $permissionAssign);
		
		echo "创建分配权限权限\n";
		
		//盘点功能
		//盘点单
		$inventoryList = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LIST);
		$inventoryList->description = '盘点单列表';
		$auth->add($inventoryList);
		$auth->addChild($admin, $inventoryList);
		
		echo "创建盘点单列表权限\n";
		
		$inventoryAdd = $auth->createPermission(RoleEnum::ROLE_INVENTORY_ADD);
		$inventoryAdd->description = '新增盘点单';
		$auth->add($inventoryAdd);
		$auth->addChild($admin, $inventoryAdd);
		
		echo "创建新增盘点单权限\n";
		
		$inventoryEdit = $auth->createPermission(RoleEnum::ROLE_INVENTORY_EDIT);
		$inventoryEdit->description = '编辑盘点单';
		$auth->add($inventoryEdit);
		$auth->addChild($admin, $inventoryEdit);
		
		echo "创建编辑盘点单权限\n";
		
		$inventoryDel = $auth->createPermission(RoleEnum::ROLE_INVENTORY_DEL);
		$inventoryDel->description = '删除盘点单';
		$auth->add($inventoryDel);
		$auth->addChild($admin, $inventoryDel);
		
		echo "创建删除盘点单权限\n";
		
		$inventoryConfirm = $auth->createPermission(RoleEnum::ROLE_INVENTORY_CONFIRM);
		$inventoryConfirm->description = '确认盘点单';
		$auth->add($inventoryConfirm);
		$auth->addChild($admin, $inventoryConfirm);
		
		echo "创建确认盘点单权限\n";
		
		//盘盈单
		$inventoryFullList = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_LIST);
		$inventoryFullList->description = '盘盈单列表';
		$auth->add($inventoryFullList);
		$auth->addChild($admin, $inventoryFullList);
		
		echo "创建盘盈单列表权限\n";
		
		$inventoryFullAdd = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_ADD);
		$inventoryFullAdd->description = '新增盘盈单';
		$auth->add($inventoryFullAdd);
		$auth->addChild($admin, $inventoryFullAdd);
		
		echo "创建新增盘盈单权限\n";
		
		$inventoryFullEdit = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_EDIT);
		$inventoryFullEdit->description = '编辑盘盈单';
		$auth->add($inventoryFullEdit);
		$auth->addChild($admin, $inventoryFullEdit);
		
		echo "创建编辑盘盈单权限\n";
		
		$inventoryFullDel = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_DEL);
		$inventoryFullDel->description = '删除盘盈单';
		$auth->add($inventoryFullDel);
		$auth->addChild($admin, $inventoryFullDel);
		
		echo "创建删除盘盈单权限\n";
		
		$inventoryFullConfirm = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_CONFIRM);
		$inventoryFullConfirm->description = '确认盘盈单';
		$auth->add($inventoryFullConfirm);
		$auth->addChild($admin, $inventoryFullConfirm);
		
		echo "创建确认盘盈单权限\n";
		
		//盘亏单
		$inventoryLostList = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_LIST);
		$inventoryLostList->description = '盘亏单列表';
		$auth->add($inventoryLostList);
		$auth->addChild($admin, $inventoryLostList);
		
		echo "创建盘亏单列表权限\n";
		
		$inventoryLostAdd = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_ADD);
		$inventoryLostAdd->description = '新增盘亏单';
		$auth->add($inventoryLostAdd);
		$auth->addChild($admin, $inventoryLostAdd);
		
		echo "创建新增盘亏单权限\n";
		
		$inventoryLostEdit = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_EDIT);
		$inventoryLostEdit->description = '编辑盘亏单';
		$auth->add($inventoryLostEdit);
		$auth->addChild($admin, $inventoryLostEdit);
		
		echo "创建编辑盘亏单权限\n";
		
		$inventoryLostDel = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_DEL);
		$inventoryLostDel->description = '删除盘亏单';
		$auth->add($inventoryLostDel);
		$auth->addChild($admin, $inventoryLostDel);
		
		echo "创建删除盘亏单权限\n";
		
		$inventoryLostConfirm = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_CONFIRM);
		$inventoryLostConfirm->description = '确认盘亏单';
		$auth->add($inventoryLostConfirm);
		$auth->addChild($admin, $inventoryLostConfirm);
		
		echo "创建确认盘亏单权限\n";
		
    }
    /**
     * @desc 新增功能添加到权限控制表
     * @author liaojianwen
     * @date 2017-02-08
     */
    public function actionAddItems()
    {
    	$auth = Yii::$app->authManager;
    	$admin = $auth->getRole('admin');
    	//销售出库单 
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_LIST)){
	    	$saleOutList = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_LIST);
	    	$saleOutList->description = '销售出库单列表';
	    	$auth->add($saleOutList);
	    	$auth->addChild($admin, $saleOutList);
	    	
	    	echo "创建销售出库单列表权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_ADD)){
	    	$saleOutAdd = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_ADD);
	    	$saleOutAdd->description = '新增销售出库单';
	    	$auth->add($saleOutAdd);
	    	$auth->addChild($admin, $saleOutAdd);
	    	
	    	echo "创建新增销售出库单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_EDIT)){
	    	$saleOutEdit = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_EDIT);
	    	$saleOutEdit->description = '编辑销售出库单';
	    	$auth->add($saleOutEdit);
	    	$auth->addChild($admin, $saleOutEdit);
	    	
	    	echo "创建编辑销售出库单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_DEL)){
	    	$saleOutDel = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_DEL);
	    	$saleOutDel->description = '删除销售出库单';
	    	$auth->add($saleOutDel);
	    	$auth->addChild($admin, $saleOutDel);
	    	
	    	echo "创建删除销售出库单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_CONFIRM)){
	    	$saleOutConfirm = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_CONFIRM);
	    	$saleOutConfirm->description = '确认销售出库单';
	    	$auth->add($saleOutConfirm);
	    	$auth->addChild($admin, $saleOutConfirm);
	    	
	    	echo "创建确认销售出库单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PAYABLES_LIST)){
    		$payablesList = $auth->createPermission(RoleEnum::ROLE_PAYABLES_LIST);
    		$payablesList->description = '应付单管理';
    		$auth->add($payablesList);
    		$auth->addChild($admin, $payablesList);
    		
    		echo "创建应付单管理权限\n";
    	}
    	/*应付结算单 */
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_LIST)){
    		$settleList = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_LIST);
    		$settleList->description = '应付结算单列表';
    		$auth->add($settleList);
    		$auth->addChild($admin, $settleList);
    		
    		echo "创建应付结算单列表权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_ADD)){
    		$settleAdd = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_ADD);
    		$settleAdd->description = '新增应付结算单';
    		$auth->add($settleAdd);
    		$auth->addChild($admin, $settleAdd);
    		
    		echo "创建新增应付结算单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_EDIT)){
    		$settleEdit = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_EDIT);
    		$settleEdit->description = '编辑应付结算单';
    		$auth->add($settleEdit);
    		$auth->addChild($admin, $settleEdit);
    		
    		echo "创建编辑应付结算单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_DEL)){
    		$settleDel = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_DEL);
    		$settleDel->description = '删除应付结算单';
    		$auth->add($settleDel);
    		$auth->addChild($admin, $settleDel);
    	
    		echo "创建删除应付结算单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_CONFIRM)){
    		$settleConfirm = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_CONFIRM);
    		$settleConfirm->description = '确认应付结算单';
    		$auth->add($settleConfirm);
    		$auth->addChild($admin, $settleConfirm);
    		
    		echo "创建确认应付结算单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SETTLE_PAYABLES_INVALID)){
    		$settleInvalid = $auth->createPermission(RoleEnum::ROLE_SETTLE_PAYABLES_INVALID);
    		$settleInvalid->description = '红冲应付结算单';
    		$auth->add($settleInvalid);
    		$auth->addChild($admin, $settleInvalid);
    		
    		echo "创建红冲应付结算单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_VENDOR)){
    		$purchaseVen = $auth ->createPermission(RoleEnum::ROLE_PURCHASE_VENDOR);
    		$purchaseVen->description = '采购统计';
    		$auth->add($purchaseVen);
    		$auth->addChild($admin, $purchaseVen);
    		
    		echo "创建采购统计权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_VENDOR_EXPORT)){
    		$purchaseExp = $auth ->createPermission(RoleEnum::ROLE_PURCHASE_VENDOR_EXPORT);
    		$purchaseExp->description ='采购统计导出';
    		$auth->add($purchaseExp);
    		$auth->addChild($admin, $purchaseExp);
    		
    		echo "创建采购统计导出权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PRODUCTSTOCK_EXPORT)){
    		$productExp = $auth->createPermission(RoleEnum::ROLE_PRODUCTSTOCK_EXPORT);
    		$productExp->description = '库存信息导出';
    		$auth->add($productExp);
    		$auth->addChild($admin, $productExp);
    		
    		echo "创建库存信息导出权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PRODUCT_PRICE)){
    		$productPri = $auth->createPermission(RoleEnum::ROLE_PRODUCT_PRICE);
    		$productPri->description = "商品成本显示";
    		$auth->add($productPri);
    		$auth->addChild($admin, $productPri);
    		
    		echo "创建商品成本显示\n";
    	}
    	
    	/* 报价单*/
    	if(!$auth->getPermission(RoleEnum::ROLE_QUOTES_ADD)){
    		$quotesAdd = $auth->createPermission(RoleEnum::ROLE_QUOTES_ADD);
    		$quotesAdd->description = '新增报价单';
    		$auth->add($quotesAdd);
    		$auth->addChild($admin, $quotesAdd);
    		
    		echo "创建报价单新增权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_QUOTES_EDIT)){
    		$quotesEdit = $auth->createPermission(RoleEnum::ROLE_QUOTES_EDIT);
    		$quotesEdit->description = '编辑报价单';
    		$auth->add($quotesEdit);
    		$auth->addChild($admin, $quotesEdit);
    	
    		echo "创建报价单编辑权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_QUOTES_LIST)){
    		$quotesList = $auth->createPermission(RoleEnum::ROLE_QUOTES_LIST);
    		$quotesList->description = '报价单列表';
    		$auth->add($quotesList);
    		$auth->addChild($admin, $quotesList);
    	
    		echo "创建报价单列表权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_QUOTES_DEL)){
    		$quotesDel = $auth->createPermission(RoleEnum::ROLE_QUOTES_DEL);
    		$quotesDel->description = '删除报价单';
    		$auth->add($quotesDel);
    		$auth->addChild($admin, $quotesDel);
    	
    		echo "创建报价单删除权限\n";
    	}
    	
    	/*赠送统计（销售）*/
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_FREE)){
    		$saleFree = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_FREE);
    		$saleFree->description = '赠送统计';
    		$auth->add($saleFree);
    		$auth->addChild($admin, $saleFree);
    		 
    		echo "创建赠送统计权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_OUT_FREE_EXP)){
    		$saleExp = $auth->createPermission(RoleEnum::ROLE_SALE_OUT_FREE_EXP);
    		$saleExp->description = '导出赠送统计';
    		$auth->add($saleExp);
    		$auth->addChild($admin, $saleExp);
    		 
    		echo "创建导出赠送统计权限\n";
    	}
    	
    	/* 调拨单 */
    	if(!$auth->getPermission(RoleEnum::ROLE_TRANSFER_LIST)){
    		$transferList = $auth->createPermission(RoleEnum::ROLE_TRANSFER_LIST);
    		$transferList->description = '调拨单列表';
    		$auth->add($transferList);
    		$auth->addChild($admin, $transferList);
    		 
    		echo "创建调拨单列表权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_TRANSFER_ADD)){
    		$transferAdd = $auth->createPermission(RoleEnum::ROLE_TRANSFER_ADD);
    		$transferAdd->description = '新增调拨单';
    		$auth->add($transferAdd);
    		$auth->addChild($admin, $transferAdd);
    		 
    		echo "创建新增调拨单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_TRANSFER_EDIT)){
    		$transferEdit = $auth->createPermission(RoleEnum::ROLE_TRANSFER_EDIT);
    		$transferEdit->description = '编辑调拨单';
    		$auth->add($transferEdit);
    		$auth->addChild($admin, $transferEdit);
    		 
    		echo "创建编辑调拨单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_TRANSFER_DEL)){
    		$transferDel = $auth->createPermission(RoleEnum::ROLE_TRANSFER_DEL);
    		$transferDel->description = '删除调拨单';
    		$auth->add($transferDel);
    		$auth->addChild($admin, $transferDel);
    		 
    		echo "创建删除调拨单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_TRANSFER_CONFIRM)){
    		$transferConfirm = $auth->createPermission(RoleEnum::ROLE_TRANSFER_CONFIRM);
    		$transferConfirm->description = '确认调拨单';
    		$auth->add($transferConfirm);
    		$auth->addChild($admin, $transferConfirm);
    		 
    		echo "创建确认调拨单权限\n";
    	}
    	
    	if(! $auth->getPermission(RoleEnum::ROLE_TRANSFER_STATISTICS)){
    		$transferStatistics = $auth->createPermission(RoleEnum::ROLE_TRANSFER_STATISTICS);
    		$transferStatistics->description = '调拨统计';
    		$auth->add($transferStatistics);
    		$auth->addChild($admin, $transferStatistics);
    		 
    		echo "创建调拨统计权限\n";
    	}
    	
    	if(! $auth->getPermission(RoleEnum::ROLE_EXPORT_TRANSFER_STATISTICS)){
    		$transferExp = $auth->createPermission(RoleEnum::ROLE_EXPORT_TRANSFER_STATISTICS);
    		$transferExp->description = '导出调拨统计';
    		$auth->add($transferExp);
    		$auth->addChild($admin, $transferExp);
    		 
    		echo "创建导出调拨统计权限\n";
    	}
    	
    	
    	//销售订单
    	if(!$auth->getPermission(RoleEnum::ROLE_SELL_LIST)){
    		$sellList = $auth->createPermission(RoleEnum::ROLE_SELL_LIST);
    		$sellList->description = '销售订单列表';
    		$auth->add($sellList);
    		$auth->addChild($admin, $sellList);
    		
    		echo "创建列表销售定单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SELL_ADD)){
    		$sellAdd = $auth->createPermission(RoleEnum::ROLE_SELL_ADD);
    		$sellAdd->description = '新增销售订单';
    		$auth->add($sellAdd);
    		$auth->addChild($admin, $sellAdd);
    	
    		echo "创建新增销售定单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SELL_EDIT)){
    		$sellEdit = $auth->createPermission(RoleEnum::ROLE_SELL_EDIT);
    		$sellEdit->description = '编辑销售订单';
    		$auth->add($sellEdit);
    		$auth->addChild($admin, $sellEdit);
    	
    		echo "创建编辑销售定单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SELL_DEL)){
    		$sellDel = $auth->createPermission(RoleEnum::ROLE_SELL_DEL);
    		$sellDel->description = '删除销售订单';
    		$auth->add($sellDel);
    		$auth->addChild($admin, $sellDel);
    	
    		echo "创建删除销售定单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_SELL_CONFIRM)){
    		$sellConfirm = $auth->createPermission(RoleEnum::ROLE_SELL_CONFIRM);
    		$sellConfirm->description = '确认销售订单';
    		$auth->add($sellConfirm);
    		$auth->addChild($admin, $sellConfirm);
    		 
    		echo "创建确认销售定单权限\n";
    	}
    	
    	
    	/* 销售退货*/
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_RETURN_ADD)){
    		$saleReturnAdd = $auth->createPermission(RoleEnum::ROLE_SALE_RETURN_ADD);
    		$saleReturnAdd->description ='新增销售退货单';
    		$auth->add($saleReturnAdd);
    		$auth->addChild($admin, $saleReturnAdd);
    		
    		echo "创建新增销售退货单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_RETURN_LIST)){
    		$saleReturnList = $auth->createPermission(RoleEnum::ROLE_SALE_RETURN_LIST);
    		$saleReturnList->description ='销售退货单列表';
    		$auth->add($saleReturnList);
    		$auth->addChild($admin, $saleReturnList);
    	
    		echo "创建销售退货单列表权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_RETURN_EDIT)){
    		$saleReturnEdit = $auth->createPermission(RoleEnum::ROLE_SALE_RETURN_EDIT);
    		$saleReturnEdit->description ='编辑销售退货单';
    		$auth->add($saleReturnEdit);
    		$auth->addChild($admin, $saleReturnEdit);
    	
    		echo "创建编辑销售退货单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_RETURN_DEL)){
    		$saleReturnDel = $auth->createPermission(RoleEnum::ROLE_SALE_RETURN_DEL);
    		$saleReturnDel->description ='新增销售退货单';
    		$auth->add($saleReturnDel);
    		$auth->addChild($admin, $saleReturnDel);
    	
    		echo "创建删除销售退货单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_RETURN_CONFIRM)){
    		$saleReturnCon = $auth->createPermission(RoleEnum::ROLE_SALE_RETURN_CONFIRM);
    		$saleReturnCon->description ='确认销售退货单';
    		$auth->add($saleReturnCon);
    		$auth->addChild($admin, $saleReturnCon);
    	
    		echo "创建确认销售退货单权限\n";
    	}
    	
    	/* 采购退货*/
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_RETURN_ADD)){
    		$purReturnAdd = $auth->createPermission(RoleEnum::ROLE_PURCHASE_RETURN_ADD);
    		$purReturnAdd->description ='新增采购退货单';
    		$auth->add($purReturnAdd);
    		$auth->addChild($admin, $purReturnAdd);
    	
    		echo "创建新增采购退货单权限\n";
    	}
    	 
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_RETURN_LIST)){
    		$purReturnList = $auth->createPermission(RoleEnum::ROLE_PURCHASE_RETURN_LIST);
    		$purReturnList->description ='采购退货单列表';
    		$auth->add($purReturnList);
    		$auth->addChild($admin, $purReturnList);
    		 
    		echo "创建采购退货单列表权限\n";
    	}
    	 
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_RETURN_EDIT)){
    		$purReturnEdit = $auth->createPermission(RoleEnum::ROLE_PURCHASE_RETURN_EDIT);
    		$purReturnEdit->description ='编辑采购退货单';
    		$auth->add($purReturnEdit);
    		$auth->addChild($admin, $purReturnEdit);
    		 
    		echo "创建编辑采购退货单权限\n";
    	}
    	 
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_RETURN_DEL)){
    		$purReturnDel = $auth->createPermission(RoleEnum::ROLE_PURCHASE_RETURN_DEL);
    		$purReturnDel->description ='新增采购退货单';
    		$auth->add($purReturnDel);
    		$auth->addChild($admin, $purReturnDel);
    		 
    		echo "创建删除采购退货单权限\n";
    	}
    	 
    	if(!$auth->getPermission(RoleEnum::ROLE_PURCHASE_RETURN_CONFIRM)){
    		$purReturnCon = $auth->createPermission(RoleEnum::ROLE_PURCHASE_RETURN_CONFIRM);
    		$purReturnCon->description ='确认采购退货单';
    		$auth->add($purReturnCon);
    		$auth->addChild($admin, $purReturnCon);
    		 
    		echo "创建确认采购退货单权限\n";
    	}
    	
    	//库存汇总单
    	if(!$auth->getPermission(RoleEnum::ROLE_STOCK_PILE_SUM)){
    		$stockSum = $auth->createPermission(RoleEnum::ROLE_STOCK_PILE_SUM);
    		$stockSum->description ='库存汇总';
    		$auth->add($stockSum);
    		$auth->addChild($admin, $stockSum);
    		 
    		echo "创建库存汇总权限\n";
    	}
    	
    	//更新商品供货
    	if(!$auth->getPermission(RoleEnum::ROLE_PRODUCT_IMPLOADE_EXECL)){
    		$implodeExecl = $auth->createPermission(RoleEnum::ROLE_PRODUCT_IMPLOADE_EXECL);
    		$implodeExecl->description ='导入execl';
    		$auth->add($implodeExecl);
    		$auth->addChild($admin, $implodeExecl);
    		 
    		echo "创建商品导入更新供货价功能\n";
    	}
    	
    	//商品导出execl 
    	if(!$auth->getPermission(RoleEnum::ROLE_PRODUCT_EXPORT_EXECL)){
    		$explodeExecl = $auth->createPermission(RoleEnum::ROLE_PRODUCT_EXPORT_EXECL);
    		$explodeExecl->description ='商品导出execl';
    		$auth->add($explodeExecl);
    		$auth->addChild($admin, $explodeExecl);
    		 
    		echo "创建商品导出execl\n";
    	}
    	//客户管理
    	if(!$auth->getPermission(RoleEnum::ROLE_CLIENT_LIST)){
    		$clientList = $auth->createPermission(RoleEnum::ROLE_CLIENT_LIST);
    		$clientList->description ='客户列表';
    		$auth->add($clientList);
    		$auth->addChild($admin, $clientList);
    		 
    		echo "创建客户列表权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_CLIENT_ADD)){
    		$clientAdd = $auth->createPermission(RoleEnum::ROLE_CLIENT_ADD);
    		$clientAdd->description ='新增客户';
    		$auth->add($clientAdd);
    		$auth->addChild($admin, $clientAdd);
    		 
    		echo "创建新增客户权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_CLIENT_EDIT)){
    		$clientEdit = $auth->createPermission(RoleEnum::ROLE_CLIENT_EDIT);
    		$clientEdit->description ='编辑客户';
    		$auth->add($clientEdit);
    		$auth->addChild($admin, $clientEdit);
    		 
    		echo "创建编辑客户权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_CLIENT_DEL)){
    		$clientDel = $auth->createPermission(RoleEnum::ROLE_CLIENT_DEL);
    		$clientDel->description ='删除客户';
    		$auth->add($clientDel);
    		$auth->addChild($admin, $clientDel);
    		 
    		echo "创建删除客户权限\n";
    	}
    	/*成本调整单*/
    	if(!$auth->getPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_LIST)){
    		$adjustmentList = $auth->createPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_LIST);
    		$adjustmentList->description ='成本调整单列表';
    		$auth->add($adjustmentList);
    		$auth->addChild($admin, $adjustmentList);
    		 
    		echo "创建成本调整单列表权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_ADD)){
    		$adjustmentAdd = $auth->createPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_ADD);
    		$adjustmentAdd->description ='新增成本调整单';
    		$auth->add($adjustmentAdd);
    		$auth->addChild($admin, $adjustmentAdd);
    		 
    		echo "创建新增成本调整单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_EDIT)){
    		$adjustmentEdit = $auth->createPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_EDIT);
    		$adjustmentEdit->description ='编辑成本调整单';
    		$auth->add($adjustmentEdit);
    		$auth->addChild($admin, $adjustmentEdit);
    		 
    		echo "创建编辑成本调整单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_CONFIRM)){
    		$adjustmentConfirm = $auth->createPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_CONFIRM);
    		$adjustmentConfirm->description ='确认成本调整单';
    		$auth->add($adjustmentConfirm);
    		$auth->addChild($admin, $adjustmentConfirm);
    		 
    		echo "创建确认成本调整单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_SEARCH)){
    		$adjustmentSearch = $auth->createPermission(RoleEnum::ROLE_PRICE_ADJUSTMENT_SEARCH);
    		$adjustmentSearch->description ='成本调整查询';
    		$auth->add($adjustmentSearch);
    		$auth->addChild($admin, $adjustmentSearch);
    		 
    		echo "创建成本调整查询权限\n";
    	}
    	//请款单
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_LIST)){
    		$paymentList = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_LIST);
    		$paymentList->description ='请款单列表';
    		$auth->add($paymentList);
    		$auth->addChild($admin, $paymentList);
    		 
    		echo "创建请款单列表权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_ADD)){
    		$paymentAdd = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_ADD);
    		$paymentAdd->description ='新增请款单';
    		$auth->add($paymentAdd);
    		$auth->addChild($admin, $paymentAdd);
    		 
    		echo "创建新增请款单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_EDIT)){
    		$paymentEdit = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_EDIT);
    		$paymentEdit->description ='编辑请款单';
    		$auth->add($paymentEdit);
    		$auth->addChild($admin, $paymentEdit);
    		 
    		echo "创建编辑请款单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_DEL)){
    		$paymentDel = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_DEL);
    		$paymentDel->description ='删除请款单';
    		$auth->add($paymentDel);
    		$auth->addChild($admin, $paymentDel);
    		 
    		echo "创建删除请款单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_EXPLODE)){
    		$paymentExp = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_EXPLODE);
    		$paymentExp->description ='导出请款单';
    		$auth->add($paymentExp);
    		$auth->addChild($admin, $paymentExp);
    		 
    		echo "创建导出请款单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_VENDOR_PAYMENT_SUMMARY_EXPLODE)){
    		$paymentSumExp = $auth->createPermission(RoleEnum::ROLE_VENDOR_PAYMENT_SUMMARY_EXPLODE);
    		$paymentSumExp->description ='导出请款汇总单';
    		$auth->add($paymentSumExp);
    		$auth->addChild($admin, $paymentSumExp);
    		 
    		echo "创建导出请款汇总单权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_PROCUREMENT_EXPORT)){
    		$proExp = $auth->createPermission(RoleEnum::ROLE_PROCUREMENT_EXPORT);
    		$proExp->description ='导出采购单';
    		$auth->add($proExp);
    		$auth->addChild($admin, $proExp);
    		 
    		echo "创建导出采购单权限\n";
    	}
    	//拆装单
    	if(!$auth->getPermission(RoleEnum::ROLE_DISASSEMBLY_LIST)){
    		$disassemblyList = $auth->createPermission(RoleEnum::ROLE_DISASSEMBLY_LIST);
    		$disassemblyList->description ='拆装单列表';
    		$auth->add($disassemblyList);
    		$auth->addChild($admin, $disassemblyList);
    		 
    		echo "创建拆装单列表权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_DISASSEMBLY_ADD)){
    		$disassemblyAdd = $auth->createPermission(RoleEnum::ROLE_DISASSEMBLY_ADD);
    		$disassemblyAdd->description ='新增拆装单';
    		$auth->add($disassemblyAdd);
    		$auth->addChild($admin, $disassemblyAdd);
    		 
    		echo "创建新增拆装单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_DISASSEMBLY_EDIT)){
    		$disassemblyEdit = $auth->createPermission(RoleEnum::ROLE_DISASSEMBLY_EDIT);
    		$disassemblyEdit->description ='编辑拆装单';
    		$auth->add($disassemblyEdit);
    		$auth->addChild($admin, $disassemblyEdit);
    		 
    		echo "创建编辑拆装单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_DISASSEMBLY_DEL)){
    		$disassemblyDel = $auth->createPermission(RoleEnum::ROLE_DISASSEMBLY_DEL);
    		$disassemblyDel->description ='删除拆装单';
    		$auth->add($disassemblyDel);
    		$auth->addChild($admin, $disassemblyDel);
    		 
    		echo "创建删除拆装单权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_DISASSEMBLY_CONFIRM)){
    		$disassemblyConfirm = $auth->createPermission(RoleEnum::ROLE_DISASSEMBLY_CONFIRM);
    		$disassemblyConfirm->description ='确认拆装单';
    		$auth->add($disassemblyConfirm);
    		$auth->addChild($admin, $disassemblyConfirm);
    		 
    		echo "创建确认拆装单权限\n";
    	}
    	//统计
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_INSTORE_SUMMARY)){
    		$instoreSummary = $auth->createPermission(RoleEnum::ROLE_INSTORE_SUMMARY);
    		$instoreSummary->description ='其他入库汇总';
    		$auth->add($instoreSummary);
    		$auth->addChild($admin, $instoreSummary);
    		 
    		echo "创建其他入库汇总权限\n";
    	}
    	if(!$auth->getPermission(RoleEnum::ROLE_OUTSTORE_SUMMARY)){
    		$outstoreSummary = $auth->createPermission(RoleEnum::ROLE_OUTSTORE_SUMMARY);
    		$outstoreSummary->description ='其他出库汇总';
    		$auth->add($outstoreSummary);
    		$auth->addChild($admin, $outstoreSummary);
    		 
    		echo "创建其他出库汇总权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_INVENTORY_FULL_SUMMARY)){
    		$inventoryFullSummary = $auth->createPermission(RoleEnum::ROLE_INVENTORY_FULL_SUMMARY);
    		$inventoryFullSummary->description ='报盈汇总';
    		$auth->add($inventoryFullSummary);
    		$auth->addChild($admin, $inventoryFullSummary);
    		 
    		echo "创建报盈汇总权限\n";
    	}
    	
    	if(!$auth->getPermission(RoleEnum::ROLE_INVENTORY_LOST_SUMMARY)){
    		$inventoryLostSummary = $auth->createPermission(RoleEnum::ROLE_INVENTORY_LOST_SUMMARY);
    		$inventoryLostSummary->description ='报损汇总';
    		$auth->add($inventoryLostSummary);
    		$auth->addChild($admin, $inventoryLostSummary);
    		 
    		echo "创建报损汇总权限\n";
    	}
    	
//     	if(!$auth->getPermission(RoleEnum::ROLE_SALE_GROSS_PROFIT_DETAIL)){
//     		$grossProfitDet = $auth->createPermission(RoleEnum::ROLE_SALE_GROSS_PROFIT_DETAIL);
//     		$grossProfitDet->description = '销售毛利明细表';
//     		$auth->add($grossProfitDet);
//     		$auth->addChild($admin, $grossProfitDet);
    		 
//     		echo "创建销售毛利明细表权限\n";
//     	}
    	if(!$auth->getPermission(RoleEnum::ROLE_SALE_GROSS_PROFIT_SUMMARY)){
    		$grossProfitSum = $auth->createPermission(RoleEnum::ROLE_SALE_GROSS_PROFIT_SUMMARY);
    		$grossProfitSum->description = '销售毛利汇总表';
    		$auth->add($grossProfitSum);
    		$auth->addChild($admin, $grossProfitSum);
    		 
    		echo "创建销售毛利汇总表权限\n";
    	}
    	 
    	//报表
    	if(!$auth->getPermission(RoleEnum::ROLE_STORE_GROSS_PROFIT_SUMMARY)){
    		$storeGrossProfitSum = $auth->createPermission(RoleEnum::ROLE_STORE_GROSS_PROFIT_SUMMARY);
    		$storeGrossProfitSum->description = '门店利润汇总表';
    		$auth->add($storeGrossProfitSum);
    		$auth->addChild($admin, $storeGrossProfitSum);
    		 
    		echo "创建门店利润汇总表权限\n";
    	}
    	
    	//特定商品销售利润报表
    	if(!$auth->getPermission(RoleEnum::ROLE_STORE_SPECIAL_GROSS_PROFIT_SUMMARY)){
    		$storeSpecialGrossProfitSum = $auth->createPermission(RoleEnum::ROLE_STORE_SPECIAL_GROSS_PROFIT_SUMMARY);
    		$storeSpecialGrossProfitSum->description = '特定商品利润报表';
    		$auth->add($storeSpecialGrossProfitSum);
    		$auth->addChild($admin, $storeSpecialGrossProfitSum);
    		 
    		echo "创建特定商品利润报表权限\n";
    	}
    }
    
    /**
     * @desc version --1958 初始化单位关系
     * @author liaojianwen
     * @date 2017-02-24
     */
    public function actionInitUnitContent()
    {
    	$res_product = ProductDAO::getInstance()->findAllByAttributes('product_id','delete_flag=:flag',[':flag'=>EnumOther::NO_DELETE]);
    	foreach ($res_product['list'] as $product){
    		$fields =[
    			'product_id',
    			'rate',
    			'unit_name',
    		];
    			$conditions = "p.delete_flag = :flag and p.product_id = :pid";
 				$params = [
 						':flag'=>EnumOther::NO_DELETE,
 						':pid'=> $product['product_id'],
 				];
 				$joinArray = [
 						[ 
							'unit u',
							'u.unit_id =p.unit',
							'left' => '' 
					] 
			];
			$res_units = ProductUnitDAO::getInstance ()->iselect ( $fields, $conditions, $params, 'all', "rate DESC", $joinArray, 'p' );
			$pre_unit = '';
			$units_content = '';
			foreach ( $res_units as $key => $unit ) {
				if ($key == 0) {
					$pre_unit = $unit ['rate'];
					$units_content .= '1' . $unit ['unit_name'] . '=';
				} else {
					$units_content .= $pre_unit / $unit ['rate'] . $unit ['unit_name'] . '=';
				}
			}
			$units_content = substr ( $units_content, 0, strlen ( $units_content ) - 1 );
			
			ProductDAO::getInstance()->iupdate(['unit_content'=>$units_content], 'product_id=:pid', [':pid'=>$product['product_id']]);
    	}
    }
    
    /**
     * @desc 更新线上的数据
     * @author liaojianwen
     * @date 2017-03-16
     */
    public function actionTest()
    {
			
			// product_id < 745 是第一次倒进来的
			// $res_product = ProductDAO::getInstance()->findAllByAttributes('product_id','product_id <:pid',[':pid'=>745]);
			// $i = 0;
			// foreach ($res_product['list'] as $product){
			// // print_r($i++);
			// ProductDAO::getInstance()->iupdate(['good_id'=>$product['product_id']
			// ], 'product_id=:pid', [
			// ':pid' => $product ['product_id']
			// ] );
			// }
			
		//
			// $res_cat = CategoryDAO::getInstance()->findAllByAttributes('category_id', "1=:num",[':num'=>1]);
			// // dd($res_cat);
			// foreach ($res_cat['list'] as $category){
			// $res = CategoryDAO::getInstance()->iupdate(['shop_cat_id'=>$category['category_id']], "category_id =:cid", [':cid'=>$category['category_id']]);
			// }
			
    	//初始化id
		$test = ProductDAO::getInstance ()->iselect ( [ 
				'product_id',
				'quantity_unit',
				'warehouse_id' 
		], "delete_flag =:flag",[':flag'=>0]);
    	foreach ($test as $tet){
//     		INSERT INTO stock_pile (warehouse_id,product_id,quantity_unit,create_time) VALUES (pre_warehouse,pre_product,pre_unit,UNIX_TIMESTAMP(NOW()));
			$var = StockPileDAO::getInstance ()->findByAttributes ( "stock_pile_id", "product_id =:id and warehouse_id =:wid", [ 
					':id' => $tet ['product_id'],
					':wid' => $tet ['warehouse_id'] 
			] );
			if(!$var){
				$columns = [
					'product_id' => $tet['product_id'],
					'warehouse_id' => $tet['warehouse_id'],
					'quantity_unit' => $tet['quantity_unit'],
				];
				StockPileDAO::getInstance()->iinsert($columns);
			}
    	}
    	dd($test);
    	
	}
	
	//
	public function actionShop()
	{
		$homeUrl = Yii::$app->params ['homeUrl'];
    	$url = Yii::$app->params['storeUrl'];
    	$key = scmTokenTool::getInstance()->getToken();
    	$url = "{$homeUrl}shop/api/test";
// 		$url = "{$url}/client/test";
// 		$url ="{$url}/scm/api/test";
    	
    	
    	$arr = [ 
				'data' => [ 
						'goods_name' => 9999,
						'supplier_id' => 2,
						'goods_unit' => 3,
						'cat_id' => 2,
						'good_id' => 1,
						'attr_id' => 1212 
				] 
		];
//     	dd($url);
    	$postdata = json_encode($arr);
    	$data = Curl::curlOption($url, 'post', $postdata, $key);
    	echo($data);
    }
    
    public function actionCat()
    {
    	// dd(1212);
    	// $homeUrl = Yii::$app->params ['homeUrl'];
    	
    	// $key = scmTokenTool::getInstance()->getToken();
    	// $url = "{$homeUrl}/shop/api/category-insert";
    	// $arr = [
    	// 		'data' => [
    	// 				'cat_id' => 43,
    	// 				'cat_name' => 9999,
    	// 				'parent_id' => 3,
    	// 		]
    	// ];
    	// $postdata = json_encode($arr);
    	// $data = Curl::curlOption($url, 'POST', $postdata, $key);
    	// echo($data);

    	// $url = "http://store01.com/api/out-store/out-store";
    	// $arr = [
    	// 		'outstore_type'=> 1,
    	// 		'outstore_man' => 2,
    	// 		'warehouse_id' => 3,
    	// 		'store_id' => 3,
    			// 'total_amount'=>9,
    	// 		'goods_detail' => [
    	// 			[
    	// 				'goods_id' => 1,
    	// 				'quantity' => 3,
    	// 				'unit' => 2,
    					// 'price' => 1,
    					// 'amount' =>3
    	// 			],
    	// 			[
    	// 				'goods_id' => 2,
    	// 				'quantity' => 3,
    	// 				'unit' => 3,
    					// 'price' => 1,
    					// 'amount' =>3
    	// 			],
    	// 			[
    	// 				'goods_id' => 3,
    	// 				'quantity' => 3,
    	// 				'unit' => 3,
    					// 'price' => 1,
    					// 'amount' =>3
    	// 			]
    	// 		]
    	// ];

// 		$url = "http://store01.com/api/inventory/inventory";
// 		$arr = [
// 			'warehouse_id'=>3,
// 			'handle_man' => 2,
// 			'store_id' =>3,
// 			'total_amount' => 9,
// 			'goods_detail'=>[
// 				[
//     				'goods_id' => 1,
//     				'quantity' => 3,//库存数
//     				'unit' => 2,
//     				'price' => 1,
//     				'amount' =>3,
//     				'lost_quantity'=> 1,//盈亏数量
//     				'inventory_quantity' => 4, //lost_quantity = Invendtor_quantity - quantity
//     			],
//     			[
//     				'goods_id' => 2,
//     				'quantity' => 3,
//     				'unit' => 3,
//     				'price' => 1,
//     				'amount' =>3,
//     				'lost_quantity'=> 1,//盈亏数量
//     				'inventory_quantity' => 4,
//     			],
//     			[
//     				'goods_id' => 3,
//     				'quantity' => 3,
//     				'unit' => 3,
//     				'price' => 1,
//     				'amount' =>3,
//     				'lost_quantity'=> 1,//盈亏数量
//     				'inventory_quantity' => 4,
//     			]
// 			]
// 		];
//     	$postdata = json_encode($arr);
// 		dd($postdata);
    	$url = "http://store01.com/api/order/create ";
    	$postdata = '{"order_sn":"201808101141966","store_id":"3","pos_id":"1","employee_id":"1","member_id":"1","payment_id":"1","money_paid":"10.00","payment_id2":"1","money_paid2":"10.00","order_amount":"10.00","discount":"100","sell_amount":"10.00","note":"优惠信息","pay_id":"1","pay_sn":"123456","is_refund":"0","refund_time":"0","order_goods":[{"goods_id":"1041","goods_name":"黑土猪精肋排","goods_price":"10.00","unit_name":"牛奶","unit_price":"10.00","sell_price":"10.00","goods_amount":"10.00","quantity":"0.01","is_gift":"0","is_refund":"0","refund_time":"0"}],"promotions":[{"promotion_type":"1","promotion_id":"1","goods_id":"0","discount_amount":"10.00","gift_goods_id":"1","gift_goods_number":"1"}]}';
// 		$postdata = '{"order_sn":"201708101041965","store_id":"3","pos_id":"1","employee_id":"1","member_id":"1","payment_id":"1","money_paid":"10.00","payment_id2":"1","money_paid2":"10.00","order_amount":"10.00","discount":"100","sell_amount":"10.00","note":"优惠信息","order_goods":[{"goods_id":"1064","goods_name":"黑土猪猪俐","goods_price":"10.00","unit_name":"牛奶","unit_price":"10.00","sell_price":"10.00","goods_amount":"10.00","quantity":"1","is_gift":"0"}],"promotions":[{"promotion_type":"1","promotion_id":"1","goods_id":"0","discount_amount":"10.00","gift_goods_id":"1","gift_goods_number":"1"}]}';
// 		$postdata = '{"order_sn":"LY19053100092","store_id":"3","pos_id":"3","employee_id":"10","member_id":"0","payment_id":"2","money_paid":"36.9","payment_id2":"0","money_paid2":"0","order_amount":"36.89","discount":"100","sell_amount":"36.89","note":"\u975e\u4f1a\u5458","pay_id":"1","pay_sn":"123456","is_refund":"0","refund_time":"0","order_goods":[{"goods_id":"1043","goods_name":"\u9ed1\u571f\u732a\u7626\u8089","goods_price":"61.44","unit_name":"Kg","unit_price":"87.8","sell_price":"61.46","goods_amount":"15.36","quantity":"0.25","is_gift":"0","is_refund":"0","refund_time":"0"},{"goods_id":"1103","goods_name":"\u56db\u5b63\u8c46","goods_price":"12","unit_name":"Kg","unit_price":"12","sell_price":"12","goods_amount":"3.12","quantity":"0.26","is_gift":"0","is_refund":"0","refund_time":"0"},{"goods_id":"1086","goods_name":"\u672c\u5730\u4e1d\u74dc","goods_price":"8.4","unit_name":"Kg","unit_price":"8.4","sell_price":"8.4","goods_amount":"3.95","quantity":"0.47","is_gift":"0","is_refund":"0","refund_time":"0"},{"goods_id":"1779","goods_name":"\u9c88\u9c7c\uff08\u6d3b\uff09","goods_price":"39.62","unit_name":"Kg","unit_price":"39.6","sell_price":"39.6","goods_amount":"14.46","quantity":"0.365","is_gift":"0","is_refund":"0","refund_time":"0"}]}';
    	$data = Curl::curlOption($url, 'POST', $postdata, 12);
    	// $data = Curl::post($url, $postdata);
    	echo($data);
    }


    
}
