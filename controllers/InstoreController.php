<?php
namespace app\controllers;
/**
 * @desc 入库单控制器
 */
use app\controllers\BaseController;
use app\models\WarehouseModel;
use Yii;
use app\models\SupplierModel;
use app\models\AdminModel;
use app\enum\EnumOriginType;
use app\models\InstoreModel;
use app\models\EmployeeModel;
use app\dao\InstoreDAO;
use app\helpers\CInputFilter;

class InstoreController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function  actionIndex()
	{
		$this->redirect('instore/list-instore');
	}
	
	/**
	 * @desc 入库单页面
	 * @author liaojianwen
	 * @date 2016-11-7
	 */
	public function actionInstore()
	{
		$instoreNo = EnumOriginType::OTHER_IN.date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$instore_type;
		return $this->render ( 'instore', [ 
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $instoreNo 
		] );
	}
	
	/**
	 * @desc 入库单列表
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionListInstore()
	{
		$vendor = SupplierModel::model()->getSupplierList();
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('instorelist',['vendor'=>$vendor,'stock'=>$stock]);
	}
	
	/**
	 * @desc 获取产品信息
	 * @author liaojianwen
	 * @date 2016-11-18
	 */
	public function actionGetProducts()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize'=>$request->get('pageSize',5),
		];
		$name = $request->get('name');
		$result = InstoreModel::model()->getProducts($pageInfo,$name);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取单位
	 * @author liaojianwen
	 * @date 2016-11-18
	 */
	public function actionListUnits()
	{
		$result = WarehouseModel::model()->listUnits();
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存仓库
	 * @author liaojianwen
	 * @date 2016-11-03
	 */
	public function actionSaveWarehouse()
	{
		$request = Yii::$app->request;
		$name = $request->get('name');
		$remark = $request->get('remark');
		$default = $request->get('def');
		$wid = $request->get('wid',0);
		$result = WarehouseModel::model ()->saveWarehouse ( $name, $remark, $default, $wid );
		$this->renderJson ( $result );
	}
	
	/**
	 * @保存入库单
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionSaveInstore()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
		$result = InstoreModel::model ()->saveInstore ( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	
	
	/**
	 * @desc 获取入库单列表数据
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionGetInstores()
	{
		$request = Yii::$app->request;
		$pageInfo = [ 
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 ) 
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter');
		
		$result = InstoreModel::model ()->getInstores ( $pageInfo, $condition, $filter);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc  编辑入库单
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionInstoreDetail()
	{
		$instoreNo = EnumOriginType::OTHER_IN.date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$instore_type;
		return $this->render ( 'editinstore', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $instoreNo
		] );
	}
	
	/**
	 * @desc 根据id 获取入库单单信息
	 * @author liaojianwen
	 * @date 2016-11-24
	 */
	public function actionGetInstoreInfo()
	{
		$id = Yii::$app->request->get('id');
		$result = InstoreModel::model()->getInstoreInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 确认入库单
	 * @author liaojianwen
	 * @date 2016-11-25
	 */
	public function actionConfirmInstore()
	{
		$id = Yii::$app->request->get('id',0);
		$result = InstoreModel::model()->confirmInstore($id);
		$this->renderJson($result);
		
	}
	
	/**
	 * @desc 删除入库单
	 * @author liaojianwen
	 * @date 2016-11-28
	 */
	public function actionDelInstore()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		
		$result = InstoreModel::model()->delInstore($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取入库单的明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	public function actionGetInstoreDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = InstoreModel::model()->getInstoreDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有删除其他入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelInstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑其他入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditInstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有确认其他入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckConfirmInstore()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 其他入库单汇总
	 * @author liaojianwen
	 * @date 2018-01-29
	 */
	public function actionInstoreSummary()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('instoresum', ['stock'=> $stock]);
	}
	
	/**
	 * @desc 获取其他入库汇总数据
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionGetInstoreSummary()
	{
		$pageInfo = [
				'page'=>CInputFilter::getInt('page',1),
				'pageSize' => CInputFilter::getInt('pageSize',10),
		];
		$condition = CInputFilter::getArray('cond', '');
		$result = InstoreModel::model()->getInstoreSummary($pageInfo, $condition);
		$this->renderJson($result);
	}
}