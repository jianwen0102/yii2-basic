<?php

/**
 * @desc   门店列表控制器
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\controllers;
use Yii;
use app\controllers\BaseController;
use app\models\StoreStoreModel;

class StoreStoreController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
    public function actionIndex()
	{
	    $this->redirect('store-store/list-store-store');	
	}
    
    public function actionListStoreStore()
	{
	   return $this->render('list-store-store');
	}

	/**
	 * @desc   获取列表数据
	 * @author hehuawei
	 * @date   2017-7-21
	 */
    public function actionGetStoreList()
	{
	    $request = Yii::$app->request;
	    $pageInfo = [
	      'page'     => $request->get('page',1), 
		  'pageSize' => $request->get('pageSize',10)
	    ];
		$searchData = $request->get('searchData');
		$filter     = $request->get('filter');
        $storeList  = StoreStoreModel::model()->getStoreList($pageInfo,$searchData,$filter);
		if(count($storeList['Body']['list']>0)){
		   foreach($storeList['Body']['list'] as &$v) $v['create_time'] = $v['create_time']>0 ? date('Y-m-d H:i:s',$v['create_time']) : '';		      
		}
	    $this->renderJson($storeList);
	}

	/**
	 * @desc 保存添加/编辑门店
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionSaveStore()
	{
	    $request    = Yii::$app->request;
		$data['store_id']    = $request->post('store_id',0);
		$data['store_name']  = $request->post('store_name');
		$data['delete_flag'] = $request->post('delete_flag');
		$storeList  = StoreStoreModel::model()->saveStore($data);
        $this->renderJson($storeList);
	}

	/**
	 * @desc 门店二维码
	 * @author lizichuan
	 * @date 2017-08-16
	 */
	public function actionStoreQrcode()
	{
		$store_id = Yii::$app->request->get('store_id', 0);
		$url = 'http://m.yiliving.com/store/member.php?store_id='.$store_id;
	    outputQrcode($url);
	}
		
	/**
	 * @desc 检验是否有编辑门店权限
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionCheckEditStore()
	{
		$this->renderJson('');
	}

	/**
	 * @desc 检验是否有编辑门店权限
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionAddEditStore()
	{
		$this->renderJson('');
	}





}