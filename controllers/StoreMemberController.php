<?php

/**
 * @desc   门店会员列表控制器
 * @author hehuawei
 * @date   2017-8-4
 */
namespace app\controllers;
use Yii;
use app\controllers\BaseController;
use app\models\StoreMemberModel;
use app\dao\StoreStoreDAO;

class StoreMemberController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
    public function actionIndex()
	{
	    $this->redirect('list-member/list');	
	}
    
    public function actionListMember()
	{
	   $store = StoreStoreDao::getInstance()->getStoreRs('',0);
	   return $this->render('list',['store'=>$store]);
	}

	/**
	 * @desc   获取列表数据
	 * @author hehuawei
	 * @date   2017-8-4
	 */
    public function actionGetMemberList()
	{
	    $request = Yii::$app->request;
	    $pageInfo = [
	      'page'     => $request->get('page',1), 
		  'pageSize' => $request->get('pageSize',10)
	    ];
		$searchData = $request->get('searchData');
		$filter     = $request->get('filter');
        $memberList  = StoreMemberModel::model()->getMemberList($pageInfo,$searchData,$filter);
		if(count($memberList['Body']['list']>0)){
		   $store_ids = $storeRs = array();
		   foreach($memberList['Body']['list'] as &$v) $store_ids[$v['store_id']] = $v['store_id'];		   
		   if(count($store_ids)>0){
		        $storeList = StoreStoreDao::getInstance()->getStoreRs($store_ids);
				if(count($storeList)>0) foreach($storeList as $st) $storeRs[$st['store_id']] = $st['store_name'];
		   } 
		   foreach($memberList['Body']['list'] as &$val){		    
		      $val['join_time'] = $val['join_time']>0 ? date('Y-m-d H:i:s',$val['join_time']) : '';
			  $val['store_name']  = isset($storeRs[$val['store_id']]) ?$storeRs[$val['store_id']] : '';
		   } 		      
		}		
	    $this->renderJson($memberList);
	}



	/**
	 * @desc 保存添加/编辑门店
	 * @author hehuawei
	 * @date 2017-8-4
	 */
	public function actionSaveMember()
	{
	    $request           = Yii::$app->request;
		$data['member_id'] = $request->post('member_id',0);
		$data['nickname']  = $request->post('nickname');
		$data['cardno']    = $request->post('cardno');
		$data['mobile']    = $request->post('mobile');
		$data['gender']    = $request->post('gender');
		$data['store_id']  = $request->post('store_id');
		$memberList        = StoreMemberModel::model()->saveMember($data);
        $this->renderJson($memberList);
	}
		
	/**
	 * @desc 编辑
	 * @author hehuawei
	 * @date 2017-8-4
	 */
	public function actionCheckEditMember()
	{
        $request = Yii::$app->request;
		$member_id = $request->post('member_id',0);
        $member = StoreMemberModel::model()->getMemberOne($member_id);
		$store = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson(['member'=>$member,'store'=>$store]);
	}


	/**
	 * @desc 检验是否有编辑门店权限
	 * @author hehuawei
	 * @date 2017-8-4
	 */
	public function actionCheckAddMember()
	{
		$store = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson($store);
	}





}