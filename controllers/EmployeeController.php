<?php

namespace app\controllers;

use Yii;
use app\controllers\BaseController;
use app\models\EmployeeModel;
class EmployeeController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionIndex()
	{
		$this->redirect(['employee/list-employee']);
	}
	
	/**
	 * @desc 员工列表
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionListEmployee()
	{
		$depart = EmployeeModel::model()->getDeparts();
		return $this->render('employeelist',['depart'=>$depart]);
	}
	
	
	/**
	 * @desc 获取员工列表数据
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionGetEmployee()
	{
		$request = Yii::$app->request;
		$pageInfo = [
			'page'=>$request->get('page',1),
			'pageSize'=>$request->get('pageSize',10),
		];
		$result = EmployeeModel::model()->getEmployee($pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存部门
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionSaveEmployee()
	{
		$request = Yii::$app->request;
		$name= $request->get('name','');
		$desc = $request->get('desc', '' );
		$depart = $request->get('depart');
		$id = $request->get ( 'id', 0 );
		$phone = $request->get ( 'phone' );
		$result = EmployeeModel::model ()->saveEmployee ( $name, $desc,$depart,$phone,$id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除员工
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionDelEmployee()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		$result = EmployeeModel::model()->delEmployee($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有新增员工权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddEmp()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有编辑员工权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditEmp()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有删除员工权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelEmp()
	{
		$this->renderJson('');
	}
}
