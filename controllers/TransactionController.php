<?php
namespace app\controllers;
/**
 * @desc 出入库明细控制器
 * @author liaojianwen
 * @date 2016-12-21
 */
use app\controllers\BaseController;
use Yii;
use app\models\TransactionModel;
use app\models\WarehouseModel;
use app\models\SupplierModel;
use app\helpers\CInputFilter;
use app\models\ClientModel;

class TransactionController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-12-21
	 */
	public function actionIndex()
	{
		$this->redirect('list-transaction');
	}
	
	/**
	 * @desc 出入库明细列表
	 * @author liaojianwen
	 * @date 2016-12-21
	 */
	public function actionListTransaction()
	{
		$warehouse = WarehouseModel::model()->listWarehouse();
		$supplier = SupplierModel::model()->listSuppliers();
		$customer = ClientModel::model()->getCustomer();
		return $this->render('transactionlist',['warehouse'=>$warehouse,'vendor'=>$supplier,'cust'=>$customer]);
	}
	
	/**
	 * @desc 获取出入库列表信息
	 * @author liaojianwen
	 * @date 2016-12-21
	 */
	public function actionGetTransactions()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$result = TransactionModel::model()->getTransactions($condition, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 出入库汇总统计
	 * @author liaojianwen
	 * @date 2017-04-18
	 */
	public function actionListStockSum()
	{
// 		$warehouse = WarehouseModel::model()->listWarehouse();
// 		$supplier = SupplierModel::model()->listSuppliers();
// 		$customer = SupplierModel::model()->getCustomer();
		return $this->render('stocksummary');
	}
	
	/**
	 * @desc 获取出入库统计数据
	 * @author liaojianwen
	 * @date 2017-04-19
	 */
	public function actionGetStockSummary()
	{
		$pageInfo = [
			'page'=>CInputFilter::getInt('page',1),
			'pageSize' => CInputFilter::getInt('pageSize',10),
		];
		$condition = CInputFilter::getArray('cond', '');
		$result = TransactionModel::model()->getStockSummary($pageInfo, $condition);
		$this->renderJson($result);
	}
}