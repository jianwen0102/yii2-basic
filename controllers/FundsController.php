<?php
 namespace app\controllers;
 /**
  * @desc 往来管理
  * @author liaojianwen
  * @date 2017-02-28
  */
 use app\controllers\BaseController;
 use Yii;
 use app\enum\EnumOriginType;
 use app\models\WarehouseModel;
 use app\models\SupplierModel;
 use app\models\EmployeeModel;
use app\models\FundsModel;
use app\helpers\CInputFilter;
use app\models\DepartmentModel;
			 
 class FundsController extends BaseController
 {
 	/**
 	 * @desc 默认控制器
 	 * @author liaojianwen
 	 * @date 2017-02-28
 	 */
 	public function actionIndex()
 	{
 		$this->redirect('funds/list-payables');
 	}
 	
 	/**
 	 * @desc 付款单
 	 * @author liaojianwen
 	 * @date 2017-02-28
 	 */
 	public function actionAddSettlePayables()
 	{
 		$payNo = EnumOriginType::PAYABLE_OTHER.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$type = EnumOriginType::$payables_type;
 		return $this->render ( 'addsettlepayables', [
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'type' => $type,
 				'NO' => $payNo
 		] );
 	}
 	
 	/**
 	 * @desc 结算单列表
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function actionListSettlePayables()
 	{
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render('settlepayableslist',['supplier'=>$supplier]);
 		
//  		return $this->render('settlepayableslist');
 	}
 	/**
 	 * @desc 付款单管理
 	 * @author liaojianwen
 	 * @date 2017-02-28
 	 */
 	public function actionListPayables()
 	{
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render('payables_list',['supplier'=>$supplier]);
 	}
 	
 	/**
 	 * @desc 获取应付款单列表
 	 * @author liaojianwen
 	 * @date 2017-03-01
 	 */
 	public function actionGetPayables()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$filter = CInputFilter::getInt('filter');
 		
 		$result = FundsModel::model ()->getPayables($condition, $filter,$pageInfo);
 		$this->renderJson ( $result );
 	}
 	
 	
 	/**
 	 * @desc 根据往来单位获取应付帐单
 	 * @author liaojianwen
 	 * @date 2017-03-02
 	 */
 	public function actionGetPayablesByVendor()
 	{
 		$pageInfo = [
 			'page'=> CInputFilter::getInt('page',1),
 			'pageSize' => CInputFilter::getInt('pageSize',5),
 		];
 		$vendor = CInputFilter::getInt('vendor');
 		$condition = [
 			'starTime' => CInputFilter::getString('starTime'),
 			'endTime' => CInputFilter::getString('endTime'),
 			'name' => CInputFilter::getString('name'),
 		];
 		
 		$result = FundsModel::model()->getPayableByVendor($vendor, $pageInfo, $condition);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 保存采购应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-03
 	 */
 	public function actionSaveSettlePayables()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$result = FundsModel::model ()->saveSettlePayables ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 获取结算单列表
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function actionGetSettlePayables()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$filter = CInputFilter::getInt('filter');
 			
 		$result = FundsModel::model ()->getSettlesPayables($condition, $filter,$pageInfo);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有编辑应收结算单权限
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function actionCheckEditSettlePayables()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 打开应付账单编辑页面
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function actionEditSettlePayables()
 	{
 		$payNo = EnumOriginType::PAYABLE_OTHER.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$type = EnumOriginType::$payables_type;
 		return $this->render ( 'editsettlepayables', [
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'type' => $type,
 				'NO' => $payNo
 		] );
 	}
 	
 	/**
 	 * @desc 获取应付结算编辑页面
 	 * @author liaojianwen
 	 * @date 2017-03-06
 	 */
 	public function actionGetSettlePayablesInfo()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->getSettlePayablesInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查应付单是否被调用
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function actionCheckPayablesById()
 	{
 		$id = CInputFilter::getInt('id');
 		
 		$result = FundsModel::model()->checkPayablesById($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有确认权限（应付结算单）
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function actionCheckConfirmSettlePayables()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 确认应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function actionConfirmSettlePayables()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->confirmSettlePayables($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 查询结算单明细
 	 * @author liaojianwen
 	 * @date 2017-03-07
 	 */
 	public function actionGetSettlePayablesDet()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->getSettlePayablesDet($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有删除应付结算权限
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function actionCheckDeleteSettlePayables()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有红冲单据权限
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function actionCheckInvalidSettlePayables()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 删除应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function actionDeleteSettlePayables()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->deleteSettlePayables($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 红冲应付结算单
 	 * @author liaojianwen
 	 * @date 2017-03-08
 	 */
 	public function actionInvalidSettlePayables()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->InvalidSettlePayables($id);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 供应商货款结算单
 	 * @author liaojianwen
 	 * @date 2017-07-05
 	 */
 	
 	public function actionAddVendorPayment()
 	{
 		$No = EnumOriginType::VENDOR_PAYMENT.date('YmdHis');
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$department = DepartmentModel::model()->listDepartment();
 		return $this->render ( 'addvendorpayment', [
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $No,
 				'depart' => $department,
 		] );
 	}
 	
 	
 	/**
 	 * @desc 编辑供应商货款结算单
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	
 	public function actionEditVendorPayment()
 	{
 		$No = EnumOriginType::VENDOR_PAYMENT.date('YmdHis');
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$department = DepartmentModel::model()->listDepartment();
 		return $this->render ( 'editvendorpayment', [
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'NO' => $No,
 				'depart' => $department,
 		] );
 	}
 	
 	/**
 	 * @desc 根据供应商获取采购入库明细
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function actionGetPurchaseByVendor()
 	{
//  		$pageInfo = [
//  				'page'=> CInputFilter::getInt('page',1),
//  				'pageSize' => CInputFilter::getInt('pageSize',100),
//  		];
 		$vendor = CInputFilter::getInt('vendor');
 		$condition = [
 				'starTime' => CInputFilter::getString('starTime'),
 				'endTime' => CInputFilter::getString('endTime'),
 				'name' => CInputFilter::getString('name'),
 		];
 			
 		$result = FundsModel::model()->getPurchaseByVendor($vendor, $condition);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 根据供应商获取供应商的账户信息
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function actionGetAccountByVendor()
 	{
 		$vendor = CInputFilter::getInt('vid',0);
 		
 		$result = SupplierModel::model()->getAccountByVendor($vendor);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 保存申请单
 	 * @author liaojianwen
 	 * @date 2017-07-06
 	 */
 	public function actionSaveVendorPayment()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$sumtime = CInputFilter::getString('sumtime','');
 		$result = FundsModel::model ()->saveVendorPayment ( $head, $detail, $remove, $id, $sumtime);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 货款申请单列表
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function actionListVendorPayment()
 	{
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render('vendorpaymentlist',['supplier'=>$supplier]);
 	}
 	
 	/**
 	 * @desc 获取货款申请单信息
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function actionGetVendorPayment()
 	{
 		$pageInfo = [
 				'page' =>CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		
 		$result = FundsModel::model ()->getVendorPayment($condition,$pageInfo);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 查询请款单明细(列表)
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function actionGetVendorPaymentDet()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = FundsModel::model()->getVendorPaymentDet($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有编辑权限
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function actionCheckEditVendorPayment()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 获取请款单编辑页面信息
 	 * @author liaojianwen
 	 * @date 2017-07-07
 	 */
 	public function actionGetVendorPaymentInfo()
 	{
 		$id = CInputFilter::getInt('id', 0);
 		
 		$result = FundsModel::model()->getVendorPaymentInfo($id);
 		$this->renderJson($result);
 	}

 	/**
 	 * @desc 检查是否有删除请款单权限
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function actionCheckDeleteVendorPayment()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 删除请款单
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function actionDeleteVendorPayment()
 	{	
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 		$result = FundsModel::model()->deleteVendorPayment($ids);
 		$this->renderJson($result);
 		
 	}
 	
 	/**
 	 * @desc 检验是否有导出请款单权限
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function actionCheckExplodeVendorPayment()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 导出请款单
 	 * @author liaojianwen
 	 * @date 2017-07-10
 	 */
 	public function actionExplodeVendorPayment(){
 		$id = CInputFilter::getInt('id', 0);
		$paymentInfo = FundsModel::model()->getVendorPaymentList($id);
 		if($paymentInfo['Ack'] =='Failure'){
 			$this->renderJson('error,no data found');
 		}
 		$title = '供应商货款结算审批表';
 		$fileName = '付款审批表';
 		
 		FundsModel::model()->explodeVendorPayment($paymentInfo['Body'], $fileName,$title);
 		
 		exit;
 		
 	}
 	
 	
 	/**
 	 * @desc 检验是否有导出请款单汇总权限
 	 * @author liaojianwen
 	 * @date 2017-11-01
 	 */
 	public function actionCheckExplodePaymentSummary()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 导出请款单汇总
 	 * @author liaojianwen
 	 * @date 2017-11-01
 	 */
 	public function actionExplodePaymentSummary(){
 		$id = CInputFilter::getInt('id', 0);
 		$paymentInfo = FundsModel::model()->getPaymentSummaryList($id);
 		if($paymentInfo['Ack'] =='Failure'){
 			$this->renderJson('error,no data found');
 		}
 		$title = '供应商货款结算审批表';
 		$fileName = '付款审批表';
 			
 		FundsModel::model()->explodePaymentSummary($paymentInfo['Body'], $fileName,$title);
 			
 		exit;
 			
 	}
 }