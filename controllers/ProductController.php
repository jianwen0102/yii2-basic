<?php
namespace app\controllers;
/**
 * @desc product 控制类
 */
 
use app\controllers\BaseController;
use Yii;
use app\models\ProductModel;
use app\models\CategoryModel;
use app\models\SupplierModel;
use app\dao\UnitDAO;
use app\models\WarehouseModel;
use app\helpers\CInputFilter;
use app\enum\EnumOther;

class ProductController extends BaseController
{
	public $enableCsrfValidation = false;
	/**
	 * @desc  默认控制器
	 * @author liaojianwen
	 * @date 2016-12-05
	 */
	public function actionIndex()
	{
		$this->redirect('product/list-product');
	}
	
	/**
	 * @desc 产品列表
	 * @author liaojianwen
	 * @date 2016-12-05
	 */
	public function actionListProduct()
	{
		$category = CategoryModel::model()->getCategorys(0);
		$supplier = SupplierModel::model()->getSupplierList();
		return $this->render('productlist',['category'=>$category,'supplier'=>$supplier]);
	}
	
	/**
	 * @desc 获取产品列表数据
	 * @author liaojianwen
	 * @date 2016-12-05
	 */
	public function actionGetProducts()
	{
		$request = Yii::$app->request;
		
		$pageInfo = [
			'page'=>$request->get('page',1),
			'pageSize'=>$request->get('pageSize',10),
		];
		$cond = $request->get('cond');
		$result = ProductModel::model()->getProductList($pageInfo, $cond);
		$this->renderJson($result);
	}

	/**
	 * @desc 新增产品
	 * @author liaojianwen
	 * @date 2016-12-05
	 */
	public function actionAddProduct()
	{
		$request = Yii::$app->request;
		$id = $request->get ( 'id', 0 );
		$product = ProductModel::model ()->getProductEdit ( $id );
		$category = CategoryModel::model ()->getCategorys ( 0 );
		$supplier = SupplierModel::model ()->getSupplierList ();
		$warehouse = WarehouseModel::model ()->listWarehouse ();
		$unit = WarehouseModel::model ()->listUnits ();
		return $this->render ( 'addproduct', [ 
				'category' => $category,
				'supplier' => $supplier,
				'unit' => $unit,
				'warehouse'=> $warehouse,
				'product' => $product 
		] );
	}
	
	/**
	 * @desc 保存商品
	 * @author liaojianwen
	 * @date 2016-12-07
	 */
	public function actionSaveProduct()
	{
		$request = Yii::$app->request;
		$productInfo = $request->post();
		$result = ProductModel::model()->saveProduct($productInfo);
		$this->renderJson($result);
		
	}
	
	/**
	 * @desc 编辑页面
	 * @author liaojianwen
	 * @date 2016-12-07
	 */
	public function actionEditProduct()
	{
		$request = Yii::$app->request;
		$id = $request->get ( 'id' );
		$product = ProductModel::model ()->getProductEdit ( $id );
		$category = CategoryModel::model ()->getCategorys ( 0, $product ['category_id'] );
		$supplier = SupplierModel::model ()->getSupplierList ( $product ['supplier_id'] );
		$warehouse = WarehouseModel::model ()->listWarehouse ($product['warehouse_id']);
		$unit = WarehouseModel::model ()->listUnits ( $product ['quantity_unit'] );
		return $this->render ( 'editproduct', [ 
				'category' => $category,
				'supplier' => $supplier,
				'unit' => $unit,
				'warehouse'=> $warehouse,
				'product' => $product 
		] );
	}
	
// 	/**
// 	 * @desc 获取编辑页面的信息
// 	 * @author liaojianwen
// 	 * @date 2016-12-07
// 	 */
// 	public function actionGetProductEdit()
// 	{
// 		$request = Yii::$app->request;
// 		$id = $request->get('id');
// 		$result = ProductModel::model()->getProductEdit($id);
// 		$this->renderJson($result);
// 	}

	/**
	 * @desc 删除商品
	 * @author liaojianwen
	 * @date 2016-12-09
	 */
	public function actionDelProduct()
	{
		$ids = Yii::$app->request->get('ids');
		$result = ProductModel::model()->delProduct($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 商品的库存总表
	 * @author liaojianwen
	 * @date 2016-12-22
	 */
	public function actionListStockPile()
	{
		$category = CategoryModel::model()->getCategorys(0);
		return $this->render('stocklist',['category'=>$category]);
	}
	
	/**
	 * @desc 获取库存信息
	 * @author liaojianwen
	 * @date 2016-12-22
	 */
	public function actionGetStockPile()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter','');
		
		$result = ProductModel::model()->getStockPile($condition, $filter, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有编辑商品权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditProduct()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有新增商品权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddProduct()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有删除商品权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelProduct()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 保存辅助单位设置
	 * @author liaojianwen
	 * @date 2017-02-15
	 */
	public function actionSaveProductUnit()
	{
		$units = CInputFilter::getArray ( 'det', '' );
		$remove = CInputFilter::getArray('remove', '');
		$product_id = CInputFilter::getInt('pid',0);
		$result = ProductModel::model()->saveProductUnit($units, $remove, $product_id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据id获取辅助单位
 	 * @author liaojianwen
 	 * @date 2017-02-15
	 */
	public function actionGetProductUnitInfo()
	{
		$id = Yii::$app->request->get('id');
		
		$result = ProductModel::model()->getProductUnitInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有导出库存权限
	 * @author liaojianwen
	 * @date 2017-03-13
	 */
	public function actionCheckProductStockExport()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出库存信息（库存表）
	 * @author liaojianwen
	 * @date 2017-03-14
	 */
	public function actionExportProductStock()
	{
		$condition = [
				'name'=>CInputFilter::getString('name',''),
				'category' => CInputFilter::getString('category',''),
				'filter' => CInputFilter::getInt('filter'),
		];
		
		$pageInfo = ProductModel::model()->getProductStock($condition);
		if(empty($pageInfo['list'])){
			$this->renderJson('error,no data found');
		}
		$title = '库存报表';
		$fileName = '库存报表';
		
		ProductModel::model()->exportProductStock($pageInfo['list'],$fileName,$title);
		
		exit;
	}
	
	/**
	 * @desc 检查是否有查看成本单价权限
	 * @author liaojianwen
	 * @date 2017-03-15
	 */
	public function actionCheckProductPrice()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出商品
	 * @author liajianwen
	 * @date 2017-05-08
	 */
	public function actionCheckExplodeProduct()
	{
		$this->renderJson('');
	}
	/**
	 * @desc 导入商品
	 * @author liajianwen
	 * @date 2017-05-08
	 */
	public function actionCheckImplodeProduct()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出商品信息
	 * @author liaojianwen
	 * @date 2017-03-14
	 */
	public function actionExportProductList()
	{
		$condition = [
				'name'=>CInputFilter::getString('name',''),
				'category' => CInputFilter::getString('category',''),
				'vendor' => CInputFilter::getInt('vendor'),
		];
	
		$pageInfo = ProductModel::model()->getExplodeProduct($condition);
		if(empty($pageInfo['list'])){
			$this->renderJson('error,no data found');
		}
		$title = '商品列表';
		$fileName = '商品列表';
	
		ProductModel::model()->exportProductList($pageInfo['list'], $fileName, $title );
		
		exit ();
	}
	
	/**
	 * 导入更新供货价指导价 
	 * @author liaojianwen
	 * @date 2017-05-08
	 */
	public function actionImplodeProductList()
	{
	 if (! empty($_FILES['file_stu']['name'])) {
            $tmp_file = $_FILES['file_stu']['tmp_name'];
            $file_types = explode(".", $_FILES['file_stu']['name']);
            $file_type = $file_types[count($file_types) - 1];
            /* 判别是不是.xls文件，判别是不是excel文件 */
            if (strtolower($file_type) != "xls" && strtolower($file_type) !='xlsx') {
                $response['Ack'] = 'Error';
                $response['msg'] = '不是Excel文件，重新上传!';
                echo json_encode($response);
                return false;
            }
            /* 设置上传路径 */
            $savePath = Yii::$app->basePath.'/'.'web/uploadfile/execl/';
//             $savePath = dirname($_SERVER['PHP_SELF']) . '/' .'../uploadfile/execl';
            file_exists($savePath) ? null : mkdir($savePath);
            /* 以时间来命名上传的文件 */
            $str = date('Ymdhis');
            $file_name = $str . "." . $file_type;
            /* 是否上传成功 */
            if (! copy($tmp_file, $savePath . $file_name)) {
                $response['Ack'] = 'Error';
                $response['msg'] = '上传失败!';
                echo json_encode($response);
                return false;
            }

			$response['Ack'] = 'Success';
			$response['path'] = $savePath;
			$response['filename'] = $file_name; 
			$response['msg'] = '上传成功，请点击更新！';
			echo json_encode($response);
			exit;
		}
	}
	
	/**
	 * @desc 调用execl 更新商品的供货价，指导价
	 * @author liaojianwen
	 * @date 2017-05-11
	 */
	public function actionUpdateProductPrice()
	{
		$path = CInputFilter::getString('path','');
		$fileName = CInputFilter::getString('name','');
		
		$result = ProductModel::model()->updateProductPrice($path, $fileName);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存商品分拆信息
	 * @author liaojianwen
	 * @date 2018-04-09
	 */
	public function actionSaveProductExtend()
	{
		$pro_extend = CInputFilter::getArray ( 'det', '' );
		$remove = CInputFilter::getArray('remove', '');
		$product_id = CInputFilter::getInt('pid',0);
		$result = ProductModel::model()->saveProductExtend($pro_extend, $remove,$product_id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据id分拆商品
	 * @author liaojianwen
	 * @date 2018-04-09
	 */
	public function actionGetProductExtendInfo()
	{
		$id = Yii::$app->request->get('id');
	
		$result = ProductModel::model()->getProductExtendInfo($id);
		$this->renderJson($result);
	}
	
}