<?php
namespace app\controllers;

use app\controllers\BaseController;
use Yii;
use app\models\DepartmentModel;
use app\models\EmployeeModel;

class DepartmentController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionIndex()
	{
		$this->redirect(['department/list-depart']);
	}
	
	/**
	 * @desc 员工列表
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionListDepart()
	{
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render('department',['admin'=> $admin]);
	}
	
	
	/**
	 * @desc 获取员工列表数据
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionGetDepart()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page'=>$request->get('page',1),
				'pageSize'=>$request->get('pageSize',10),
		];
		$result = DepartmentModel::model()->getDepart($pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存部门
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionSaveDepart()
	{
		$request = Yii::$app->request;
		$name= $request->get('name','');
		$remark = $request->get('remark', '' );
		$id = $request->get ( 'id', 0);
		$master = $request->get('master',0);
		$result = DepartmentModel::model ()->saveDepart ( $name, $remark, $id, $master);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除部门
	 * @author liaojianwen
	 * @date 2016-11-23
	 */
	public function actionDelDepart()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids','');
		$result = DepartmentModel::model()->delDepart($ids);
		$this->renderJson($result);
		
	}

	/**
	 * @desc 验证是否有新增部门权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckAddDepart()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 验证是否有编辑部门权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditDepart()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 验证是否有删除部门权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelDepart()
	{
		$this->renderJson('');
	}
}