<?php
namespace app\controllers;
/**
 * @desc 类目控制器
 * @author liaojianwen
 * @date 2016-11-30
 */
 use app\controllers\BaseController;
 use app\models\CategoryModel;
 use Yii;
 
class CategoryController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-11-30
	 */
	public function actionIndex()
	{
		$this->redirect('category/list_category');
	}
	
	/**
	 * @desc 类目页面
	 * @author liaojianwen
	 * @date 2016-11-30
	 */
	public function actionListCategory()
	{
// 		$category = CategoryModel::model()->getCategorys(0);
		return $this->render('categorylist');
	}
	
	/**
	 * @desc 获取category list
	 * @author liaojianwen
	 * @date 2016-12-02
	 */
	public function actionCategoryList()
	{
		$result = $category = CategoryModel::model()->getCategoryList();
		$this->renderJson($category);
	}
	
	/**
	 * @desc 根据id 获取cateInfo
	 * @author liaojianwen
	 * @date 2016-12-02
	 */
	public function actionGetCateInfo()
	{
		$request = Yii::$app->request;
		
		$id = $request->get('id');
		$result = CategoryModel::model()->getCateInfo($id);
		$this->renderJson($result);
		
	}
	
	/**
	 * @desc 保存类目信息
	 * @author liaojianwen
	 * @date 2016-12-02
	 */
	public function actionSaveCate()
	{
		$request = Yii::$app->request;
		
		$result = CategoryModel::model()->saveCate($request->get());
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除类目
	 * @author liaojianwen
	 */
	public function actionDelCate()
	{
		$request = Yii::$app->request;
		
		$id = $request->get('id');
		$result = CategoryModel::model()->delCate($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有更新权限
	 * @author liaojianwen
	 * @date 2017-01-09
	 */
	public function actionCheckCategoryUpdate()
	{
		$this->renderJson('');
		
	}
	
	/**
	 * @desc 检查是否有删除类目权限
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckCategoryDel()
	{
		$this->renderJson('');
	}
	
}