<?php
namespace app\controllers;
/**
 * @desc 分仓库存表
 * @author liaojianwen
 * @date 2016-12-26
 */
use app\controllers\BaseController;
use Yii;
use app\helpers\Pivot;
use app\models\StockPileModel;
use app\models\WarehouseModel;

class StockPileController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-12-26
	 */
	public function actionIndex()
	{
		$this->redirect('list-stock-pile');
	}

	/**
	 * @desc 分仓库存列表
	 * @author liaojianwen
	 * @date 2016-12-26
	 */
	public function actionListStockPile()
	{
// 		$result = StockPileModel::model()->makeArr();
// 		$pp = new Pivot($data, $topPivot, $leftPivot, $measure);
		$warehouse = WarehouseModel::model()->listWarehouse();
		return $this->render('stockpilelist',['warehouse'=>$warehouse]);
	}
	
	/**
	 * @desc  获取分仓数据
	 * @author liaojianwen
	 * @date 2016-12-27
	 */
	public function actionGetStockInfo()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$result = StockPileModel::model()->getStockInfo($condition, $pageInfo);
		$this->renderJson($result);
	}
	
}