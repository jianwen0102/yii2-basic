<?php
namespace app\controllers;

/**
 * @desc 采购入库控制器
 */
use app\controllers\BaseController;
use Yii; 
use app\models\WarehouseModel;
use app\models\SupplierModel;
use app\models\EmployeeModel;
use app\enum\EnumOriginType;
use app\models\PurchaseModel;
use app\helpers\CInputFilter;

class PurchaseController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2016-12-13
	 */
	public function actionIndex()
	{
		$this->redirect('list-purchase');
	}
	
	/**
	 * @desc 采购入库列表页面
	 * @author liaojianwen
	 * @date 2016-12-13
	 */
	public function actionListPurchase()
	{
		$vendor = SupplierModel::model()->getSupplierList();
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('purchaselist',['vendor'=>$vendor, 'stock'=>$stock]);
	}
	
	/**
	 * @desc 新增采购入库单
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionAddPurchase()
	{
		$purNo = EnumOriginType::PURCHASE .date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$purchase_type;
		return $this->render ( 'addpurchase', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $purNo
		] );
	}
	
	/**
	 * @desc 保存采购入库单
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionSavePurchase()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id');
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id');
		$remove = CInputFilter::getArray('remove', '');
		$result = PurchaseModel::model ()->savePurchase( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 采购入库列表
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionGetPurchases()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		
		$result = PurchaseModel::model()->getPurchases($pageInfo, $condition);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 编辑页面
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionEditPurchase()
	{
		$purNo = EnumOriginType::PURCHASE . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		$supplier = SupplierModel::model()->listSuppliers();
		$type = EnumOriginType::$purchase_type;
		return $this->render ( 'editpurchase', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'supplier' => $supplier,
				'type' => $type,
				'NO' => $purNo
		]);
	}
	
	/**
	 * @desc 获取编辑页面采购入库单信息
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionGetPurchaseInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = PurchaseModel::model()->getPurchaseInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 导入采购订单
	 * @author liaojianwen
	 * @date 2017-05-02
	 */
	public function actionImplodeProcurementInfo()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = PurchaseModel::model()->ImplodeProcurementInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 确认采购入库单
	 * @author liaojianwen
	 * @date 2016-12-14
	 */
	public function actionConfirmPurchase()
	{
		$id = Yii::$app->request->get('id',0);
		$result = PurchaseModel::model()->confirmPurchase($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除采购订单
	 * @author liaojianwen
	 * @date 2016-12-15
	 */
	public function actionDelPurchase()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		
		$result = PurchaseModel::model()->delPurchase($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 采购入库查询可以导入的采购订单信息
	 * @author liaojianwen
	 * @date 2016-12-15
	 */
	public function actionSelectProcurement()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 5)
		];
		$condition = $request->get ( 'cond', [ ] );
		$result = PurchaseModel::model()->selectProcurement($condition, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 根据id 获取采购明细的信息
	 * @author liaojianwen
	 * @date 2016-15-15
	 */
	public function actionGetProDetail()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = PurchaseModel::model()->getProDetail($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查订单是否被其他入库单占用
	 * @author liaojianwen
	 * @date 2016-12-20
	 */
	public function actionCheckPurchaseById()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = PurchaseModel::model()->checkPurchaseById($id);
		$this->renderJson($result);
		
	}
	
	/**
	 * @desc 获取采购入库单的明细
	 * @author liaojianwen
	 * @date 2016-12-29
	 */
	public function actionGetPurchaseDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = PurchaseModel::model()->getPurchaseDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有删除采购入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckDelPur()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有编辑采购入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckEditPur()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检验是否有确认采购入库的功能
	 * @author liaojianwen
	 * @date 2017-01-13
	 */
	public function actionCheckConfirmPur()
	{
		$this->renderJson('');
	}

	/**
	 * @desc 采购统计（供应商）
	 * @author liaojianwen
	 * @date 2017-03-09
	 */
	public function actionPurchaseVendor()
	{
		$supplier = SupplierModel::model()->listSuppliers();
		return $this->render('purchase_vendor',['vendor'=>$supplier]);
	}
	
	/**
	 * @desc 查询采购统计
	 * @author liaojianwen
	 * @date 2017-03-09
	 */
	public function actionGetPurchaseCount()
	{
		$pageInfo = [
				'page' => CInputFilter::getInt( 'page', 1 ),
				'pageSize' => CInputFilter::getInt( 'pageSize', 10)
		];
		$condition = CInputFilter::getArray( 'cond', '');
		$result = PurchaseModel::model()->getPurchaseCount($condition, $pageInfo);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 查询采购付款统计是否有导出权限
	 * @author liaojianwen
	 * @date 2017-03-10
	 */
	public function actionCheckPurchaseVendorExport()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 导出采购付款统计
	 * @author liaojianwen
	 * @date 2017-03-10
	 */
	public function actionExportPurchaseVendor()
	{
		$condition = [
				'starTime'=>CInputFilter::getString('starTime'),
				'endTime' => CInputFilter::getString('endTime'),
				'vendor' => CInputFilter::getInt('vendor'),
				'id' => CInputFilter::getInt('id',0),
				'name'=> CInputFilter::getString('name'),
		];
		
		$pageInfo = PurchaseModel::model()->getPurchaseVendor($condition);
		$title = $condition['starTime'] .'至' . $condition['endTime'];
		$fileName = date('m',strtotime($condition['starTime'])) .'月进货明细表';
		
		$result = PurchaseModel::model()->exportPurchaseVendor($pageInfo['Body'], $fileName, $title);
		
		exit;
		
	}
	
}