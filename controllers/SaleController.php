<?php
 namespace app\controllers;
 
 use app\controllers\BaseController;
 use Yii;
 use app\models\WarehouseModel;
 use app\models\EmployeeModel;
 use app\enum\EnumOriginType;
 use app\enum\EnumOther;
 use app\models\SupplierModel;
 use app\models\InstoreModel;
 use app\models\SaleModel;
 use app\models\InventoryModel;
 use app\helpers\CInputFilter;
 use app\helpers\scmTokenTool;
 use app\helpers\Curl;
use app\dao\SaleOutDAO;
use app\models\ClientModel;
				 
 class SaleController extends BaseController
 {
 	/**
 	 * @desc 默认控制器
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public  function actionIndex()
 	{
 		$this->redirect(array('list-sale-out'));
 	}
 	
 	/**
 	 * @desc 出库单列表
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function actionListSaleOut()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('saleoutlist', ['stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 出库单页面
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function actionAddSaleOut()
 	{
 		$instoreNo = EnumOriginType::SALE_OUT.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		$type = EnumOriginType::$sale_type;
 		return $this->render ( 'addsaleout', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'type' => $type,
 				'NO' => $instoreNo
 		] );
 	}
 	
 	/**
 	 * @desc 出库单编辑页面
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionEditSaleOut()
 	{
 		$instoreNo = EnumOriginType::SALE_OUT.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		$type = EnumOriginType::$sale_type;
 		return $this->render ( 'editsaleout', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'type' => $type,
 				'NO' => $instoreNo
 		] );
 	}
 	
 	/**
 	 * @desc 获取产品信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function actionGetProducts()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize'=>$request->get('pageSize',5),
 		];
 		$name = $request->get('name');
 		$result = InstoreModel::model()->getProducts($pageInfo,$name);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 根据仓库获取仓库下面的商品
 	 * @author liaojianwen
 	 * @date 2017-01-19
 	 */
 	public function actionGetProductsByWare()
 	{
 		$request = Yii::$app->request;
 		$ware_id = $request->get('ware');
 	
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize'=>$request->get('pageSize',5),
 		];
 		$name = $request->get('name');
 		$result = InventoryModel::model()->getProductByWare($ware_id, $name, $pageInfo);
 		$this->renderJson($result);
 	
 	}
 	
 	/**
 	 * @desc 保存销售出库
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function actionSaveSaleOut()
 	{
//  		$request = Yii::$app->request;
//  		$head = $request->get ( 'head' );
//  		$detail = $request->get ( 'det' );
//  		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
 		$result = SaleModel::model ()->saveSaleOut ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 销售出库单列表信息
 	 * @author liaojianwen
 	 * @date 2017-02-07
 	 */
 	public function actionGetSaleOut()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 10 )
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$filter = $request->get('filter');
 	
 		$result = SaleModel::model ()->getSaleOut ( $pageInfo, $condition, $filter);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 检查是否有编辑销售发货单权限
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionCheckEditSaleOut()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有删除销售发货单权限
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionCheckDelSaleOut()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有确认销售发货单权限
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionCheckConfirmSaleOut()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 编辑页面根据id获取编辑页面信息
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionGetSaleOutInfo()
 	{
 		$id = Yii::$app->request->get('id');
 		$result = SaleModel::model()->getSaleOutInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 销售出库导入销售订单
 	 * @author liaojianwen
 	 * @date 2017-05-03
 	 */
 	public function actionImplodeSellInfo()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = SaleModel::model()->implodeSellInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 删除销售出库单
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionDelSaleOut()
 	{
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 		
 		$result = SaleModel::model()->delSaleOut($ids);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 获取销售出库单明细
 	 * @author liaojianwen
 	 * @date 2017-02-008
 	 */
 	public function actionGetSaleOutDet()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSaleOutDet($id);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 确认销售出库单
 	 * @author liaojianwen
 	 * @date 2017-02-08
 	 */
 	public function actionConfirmSaleOut()
 	{
 		$id = Yii::$app->request->get('id',0);
 		$result = SaleModel::model()->confirmSaleOut($id);
 		$this->renderJson($result);
 	}
 	
 	
 	public function actionSaleOutCust()
 	{
//  		$cust = SupplierModel::model()->getCustomer();
 		$cust = ClientModel::model()->getCustomer();
 		return $this->render('saleout_cust',['cust'=>$cust]);
 	}
 	
 	/**
 	 * @desc 获取销售统计
 	 * @author liaojianwen
 	 * @date 2017-03-29
 	 */
 	public function actionGetSaleOutCount()
 	{
 		$pageInfo = [
 				'page' => CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10)
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$result = SaleModel::model()->getSaleoutCount($condition, $pageInfo);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 导出
 	 * @author liaojianwen
 	 */
 	public function actionExportSaleOut()
 	{
 		
 	}
 	
 	/**
 	 * @desc 赠送客户信息
 	 * @author liaojianwen
 	 * @date 2017-03-29
 	 */
 	public function actionSaleOutFree()
 	{
//  		$cust = SupplierModel::model()->getCustomer();
		$cust = ClientModel::model()->getCustomer();
 		return $this->render('saleout_free', ['cust'=>$cust]);
 	}
 	
 	/**
 	 * @desc 获取赠送明细
 	 * @author liaojianwen
 	 * @date 2017-03-29
 	 */
 	public function actionGetSaleOutFree()
 	{
 		$pageInfo = [
 				'page' => CInputFilter::getInt( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10)
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$result = SaleModel::model()->getSaleoutFree($condition, $pageInfo);
 		$this->renderJson($result);
 	}
 
 	/**
 	 * @desc 检查是由导出赠送明细的权限
 	 * @author liaojianwen
 	 * @date 2017-03-30
 	 */
 	public function actionCheckSaleOutFreeExport()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 导出赠送明细
 	 * @author liaojianwen
 	 * @date 2017-03-30
 	 */
 	public function actionExportSaleOutFree() 
 	{
 		$condition = [
 				'starTime'=>CInputFilter::getString('starTime'),
 				'endTime' => CInputFilter::getString('endTime'),
 				'customer' => CInputFilter::getInt('customer'),
 				'id' => CInputFilter::getInt('id'),
 				'name'=> CInputFilter::getString('name'),
 		];
 		
 		$pageInfo = SaleModel::model()->getSaleFreeExport($condition);
 		$fileName = date('Y',strtotime($condition['starTime'])) .'年' .date('m',strtotime($condition['starTime'])) .'月商品出库明细表';
 		$title = $fileName;
 		
 		$result = SaleModel::model()->exportSaleoutFree($pageInfo['Body'], $fileName, $title);
 		
 		exit;
 	}
 	
 	
 	/**
 	 * @desc 新增报价单
 	 * @author liaojianwen
 	 * @date 2017-03-23
 	 */
 	public function actionAddQuotes()
 	{
 		$proNo = EnumOriginType::PROCUREMENT.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		return $this->render ( 'addquotes', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
//  				'type' => $type,
 				'NO' => $proNo
 		] );
 	}
 	
 	/**
 	 * @desc 编辑报价单
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function actionEditQuotes()
 	{
 		$proNo = EnumOriginType::PROCUREMENT.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
//  		$type = EnumOriginType::$instore_type;
 		return $this->render ( 'editquotes', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
//  				'type' => $type,
 				'NO' => $proNo
 		] );
 		return $this->render('editquotes');
 	}
 	
 	/**
 	 * @desc 报价单列表
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function actionListQuotes()
 	{
 		$vendor = SupplierModel::model()->getSupplierList();
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('quoteslist',['vendor'=>$vendor, 'stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 保存报价单
 	 * @author liaojianwen
 	 * @date 2017-03-24
 	 */
 	public function actionSaveQuotes()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id');
 		$remove = CInputFilter::getArray('remove','');
 		$result = SaleModel::model ()->saveQuotes ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 获取报价单列表
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function actionGetQuotes()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 10 )
 		];
 		$condition = $request->get ( 'cond', [ ] );
 	
 		$result = SaleModel::model()->getQuotes($pageInfo, $condition);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 检验是否有编辑报价单权限
 	 * @author liaojianwen
 	 */
 	public function actionCheckEditQuotes()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 获取编辑报价单明细信息
 	 * @author liaojianwen
 	 * @date 2017-03-27
 	 */
 	public function actionGetQuotesInfo()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getQuotesInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有删除报价单权限
 	 * @author liaojianwen
 	 * @date 2017-03-18
 	 */
 	public function actionCheckDelQuotes()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 删除报价单
 	 * @author liaojianwen
 	 * @date 2017-03-28
 	 */
 	public function actionDelQuotes()
 	{
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 	
 		$result = SaleModel::model()->delQuotes($ids);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 新增销售订单
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function actionAddSells()
 	{
 		$saleNo = EnumOriginType::SALE.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		return $this->render ( 'addsell', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'NO' => $saleNo
 		] );
 	}
 	
 	
 	/**
 	 * @desc 销售订单列表
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function actionListSells()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('selllist', ['stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 销售订单编辑页面
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function actionEditSells()
 	{
 		$instoreNo = EnumOriginType::SALE.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		return $this->render ( 'editsell', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'NO' => $instoreNo
 		] );
 	}
 	
 	/**
 	 * @desc 保存销售订单
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function actionSaveSell()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id');
 		$remove = CInputFilter::getArray('remove','');
 		$result = SaleModel::model ()->saveSell( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 获取销售订单列表
 	 * @author liaojianwen
 	 * @date 2017-04-13
 	 */
 	public function actionGetSells()
 	{
 		$pageInfo = [
 				'page' => CInputFilter::getInt ( 'page', 1 ),
 				'pageSize' => CInputFilter::getInt( 'pageSize', 10 )
 		];
 		$condition = CInputFilter::getArray( 'cond', '');
 		$filter =  CInputFilter::getInt('filter',0);
 		
 		$result = SaleModel::model()->getSells($pageInfo, $condition, $filter);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检验是否有编辑销售定单权限
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionCheckEditSell()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有删除报价单权限
 	 * @author liaojianwen
 	 * @date 2017-03-18
 	 */
 	public function actionCheckDelSell()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 获取编辑销售定单明细信息
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionGetSellInfo()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSellInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 销售出库查询可以导入的采购订单信息
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionSelectSells()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 5)
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$result = SaleModel::model()->selectSell($condition, $pageInfo);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 根据id 获取销售订单明细的信息
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionGetSellDetail()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSellDetail($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 获取销售出库单明细
 	 * @author liaojianwen
 	 * @date 2017-02-008
 	 */
 	public function actionGetSellDet()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSellDet($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 删除销售定单
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionDelSell()
 	{
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 	
 		$result = SaleModel::model()->delSell($ids);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查销售订单是否被销售出库单占用
 	 * @author liaojianwen
 	 * @date 2017-04-14
 	 */
 	public function actionCheckSellById()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->checkSellById($id);
 		$this->renderJson($result);
 	
 	}
 	
 	
 	/**
 	 * @desc 销售退货
 	 * @author liaojianwen
 	 * @date 2017-04-24
 	 */
 	public function actionListSaleReturn()
 	{
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('salereturnlist', ['stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 销售退货列表
 	 * @author liaojianwen
 	 * @date 2017-04-24
 	 */
 	public function actionAddSaleReturn()
 	{
 		$returnNo = EnumOriginType::SALE_RETURN.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		$type = EnumOriginType::$sale_return_type;
 		return $this->render ( 'addsalereturn', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'type' => $type,
 				'NO' => $returnNo
 		] );
 	}
 	
 	/**
 	 * @desc 销售退货单编辑页面
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionEditSaleReturn()
 	{
 		$instoreNo = EnumOriginType::SALE_RETURN.date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
//  		$customer = SupplierModel::model()->getCustomer();
 		$customer = ClientModel::model()->getCustomer();
 		$type = EnumOriginType::$sale_return_type;
 		return $this->render ( 'editsalereturn', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'customer' => $customer,
 				'type' => $type,
 				'NO' => $instoreNo
 		] );
 	}
 	
 	/**
 	 * @desc 入库查询可以导入的采购订单信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionSelectSaleOut()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 5)
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$result = SaleModel::model()->selectSaleOut($condition, $pageInfo);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 根据id 获取销售出货明细的信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionGetSaleOutDetail()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSaleOutDetail($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查销售出库单是否被销售退货单占用
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionCheckSaleOutById()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->checkSaleOutById($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 保存销售退货
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionSaveSaleReturn()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id',0);
 		$remove = CInputFilter::getArray('remove','');
 		$result = SaleModel::model ()->saveSaleReturn ( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 销售退货单列表信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionGetSaleReturn()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 10 )
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$filter = $request->get('filter');
 	
 		$result = SaleModel::model ()->getSaleReturn ( $pageInfo, $condition, $filter);
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 编辑页面根据id获取编辑页面信息(销售退货)
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionGetSaleReturnInfo()
 	{
 		$id = Yii::$app->request->get('id');
 		$result = SaleModel::model()->getSaleReturnInfo($id);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 销售退货单导入销售出库
 	 * @author liaojianwen
 	 * @date 2017-05-03
 	 */
 	public function actionImplodeSaleOutInfo()
 	{
 		$id = CInputFilter::getInt('id');
 		$result = SaleModel::model()->implodeSaleOutInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 获取销售退货单明细(列表弹窗)
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionGetSaleReturnDet()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = SaleModel::model()->getSaleReturnDet($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查是否有编辑销售退货单权限
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionCheckEditSaleReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有删除销售退货单权限
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionCheckDelSaleReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检查是否有确认销售退货单权限
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionCheckConfirmSaleReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 确认销售退货单
 	 * @author liaojianwen
 	 * @date 2017-04-26
 	 */
 	public function actionConfirmSaleReturn()
 	{
 		$id = Yii::$app->request->get('id',0);
 		$result = SaleModel::model()->confirmSaleReturn($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 删除销售退货单
 	 * @author liaojianwen
 	 * @date 2017-04-26
 	 */
 	public function actionDelSaleReturn()
 	{
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 			
 		$result = SaleModel::model()->delSaleReturn($ids);
 		$this->renderJson($result);
 	}

    /**
	 * @desc 获取某一期的订单汇总
 	 * @author lizichuan
 	 * @date 2017-07-26
	 */
    public function actionOrderSummary()
    {
    	$shopUrl = Yii::$app->params ['shopUrl'];
    	$date = Yii::$app->request->get('date');
    	$url = $shopUrl ."api/order.php?act=summary&date=". $date;
    	$key = scmTokenTool::getInstance()->getToken();
		
    	$data = Curl::curlOption($url, 'GET', '', $key);
    	echo($data);
    }
    
    /**
     * @desc 检查是否有确认销售订单权限
     * @author liaojianwen
     * @date 2017-07-28
     */
    public function actionCheckConfirmSell()
    {
    	$this->renderJson('');
    }
    
    /**
     * @desc 确认销售订单
     * @author liaojianwen
     * @date 2017-07-28
     */
    public function actionConfirmSell()
    {
    	$id = Yii::$app->request->get('id',0);
    	$result = SaleModel::model()->confirmSell($id);
    	$this->renderJson($result);
    }
}
