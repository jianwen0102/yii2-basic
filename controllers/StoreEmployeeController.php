<?php

/**
 * @desc   店员列表控制器
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\controllers;
use Yii;
use app\controllers\BaseController;
use app\models\StoreEmployeeModel;
use app\dao\StoreStoreDAO;

class StoreEmployeeController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
    public function actionIndex()
	{
	    $this->redirect('store-employee/list-store-employee');	
	}
    
    public function actionListStoreEmployee()
	{
	   return $this->render('list-store-employee');
	}

	/**
	 * @desc   获取列表数据
	 * @author hehuawei
	 * @date   2017-7-21
	 */
    public function actionGetStoreEmployeeList()
	{
	    $request = Yii::$app->request;
	    $pageInfo = [
	      'page'     => $request->get('page',1), 
		  'pageSize' => $request->get('pageSize',10)
	    ];
		$searchData = $request->get('searchData');
		$filter     = $request->get('filter');
        $storeEmployeeList  = StoreEmployeeModel::model()->getStoreEmployeeList($pageInfo,$searchData,$filter);
		if(count($storeEmployeeList['Body']['list']>0)){
		   $store_ids = $storeRs = array();
		   foreach($storeEmployeeList['Body']['list'] as &$v) $store_ids[$v['store_id']] = $v['store_id'];		   
		   if(count($store_ids)>0){
		        $storeList = StoreStoreDao::getInstance()->getStoreRs($store_ids);
				if(count($storeList)>0) foreach($storeList as $st) $storeRs[$st['store_id']] = $st['store_name'];
		   } 
		   foreach($storeEmployeeList['Body']['list'] as &$val){		   
		      $val['create_time'] = $val['create_time']>0 ? date('Y-m-d H:i:s',$val['create_time']) : '';
			  $val['store_name']  = isset($storeRs[$val['store_id']]) ?$storeRs[$val['store_id']] : '';
		   } 	      
		}
	    $this->renderJson($storeEmployeeList);
	}

	/**
	 * @desc 保存添加/编辑门店
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionSaveStoreEmployee()
	{
	    $request                = Yii::$app->request;
		$data['employee_id']    = $request->post('employee_id',0);
		$data['store_id']       = $request->post('store_id',0);
		$data['employee_name']  = $request->post('employee_name');
		$data['delete_flag']    = $request->post('delete_flag');
		$storeEmployeeList      = StoreEmployeeModel::model()->saveStoreEmployee($data);
        $this->renderJson($storeEmployeeList);
	}
		
	/**
	 * @desc 检验是否有编辑员工权限
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionCheckEditEmployee()
	{
		$request = Yii::$app->request;
		$employee_id = $request->post('employee_id',0);
		$employeeOne = StoreEmployeeModel::model()->getStoreEmployeeOne($employee_id);
		$store = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson(array('employee'=>$employeeOne['Body'],'store'=>$store));
	}


	/**
	 * @desc 检验是否有添加员工权限
	 * @author hehuawei
	 * @date 2017-01-13
	 */
	public function actionCheckAddEmployee()
	{
		$store = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson($store);
	}





}