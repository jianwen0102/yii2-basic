<?php
 namespace app\controllers;
 /**
  * @desc 采购退货控制器
  */
 use app\controllers\BaseController;
 use Yii;
 use app\helpers\CInputFilter;
 use app\models\SupplierModel;
 use app\models\WarehouseModel;
 use app\enum\EnumOriginType;
 use app\models\EmployeeModel;
 use app\models\PurchaseReturnModel;
 use app\models\InventoryModel;
	 
 class PurchaseReturnController extends BaseController 
 {
 	/**
 	 * @descs 默认控制器
 	 * @author liaojianwen
 	 * @date 2017-04-27
 	 */
 	public function actionIndex()
 	{
 		$this->redirect('list-purchase-return');
 	}
 	
 	/**
 	 * @desc 采购入库列表页面
 	 * @author liaojianwen
 	 * @date 2016-12-13
 	 */
 	public function actionListPurchaseReturn()
 	{
 		$vendor = SupplierModel::model()->getSupplierList();
 		$stock = WarehouseModel::model()->listWarehouse();
 		return $this->render('purchasereturnlist',['vendor'=>$vendor, 'stock'=>$stock]);
 	}
 	
 	/**
 	 * @desc 新增采购退货单
 	 * @author liaojianwen
 	 * @date 2017-04-27
 	 */
 	public function actionAddPurchaseReturn()
 	{
 		$purNo = EnumOriginType::PURCHASE_RETURN .date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$type = EnumOriginType::$purchase_back_type;
 		return $this->render ( 'addpurchasereturn', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'type' => $type,
 				'NO' => $purNo
 		] );
 	}
 	
 	/**
 	 * @desc 新增采购退货单
 	 * @author liaojianwen
 	 * @date 2017-04-27
 	 */
 	public function actionEditPurchaseReturn()
 	{
 		$purNo = EnumOriginType::PURCHASE_RETURN .date('YmdHis');
 		$warehouse = WarehouseModel::model()->listWarehouse();
 		$admin = EmployeeModel::model()->listEmployee();
 		$supplier = SupplierModel::model()->listSuppliers();
 		$type = EnumOriginType::$purchase_back_type;
 		return $this->render ( 'editpurchasereturn', [
 				'warehouse' => $warehouse,
 				'admin' => $admin,
 				'supplier' => $supplier,
 				'type' => $type,
 				'NO' => $purNo
 		] );
 	}
 	
 	/**
 	 * @desc 入库查询可以导入的采购订单信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionSelectPurchase()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 5)
 		];
 		$condition = $request->get ( 'cond', [ ] );
 		$result = PurchaseReturnModel::model()->selectPurchase($condition, $pageInfo);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 根据id 获取蔡国入库明细的信息
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionGetPurDetail()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = PurchaseReturnModel::model()->getPurDetail($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检查采购入库单是否被采购退货单占用
 	 * @author liaojianwen
 	 * @date 2017-04-25
 	 */
 	public function actionCheckPurchaseById()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = PurchaseReturnModel::model()->checkPurchaseById($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 保存采购退货单
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionSavePurchaseReturn()
 	{
 		$head = CInputFilter::getArray('head', '');
 		$detail = CInputFilter::getArray('det', '');
 		$id = CInputFilter::getInt('id');
 		$remove = CInputFilter::getArray('remove', '');
 		$result = PurchaseReturnModel::model ()->savePurchaseReturn( $head, $detail, $remove, $id );
 		$this->renderJson ( $result );
 	}
 	
 	/**
 	 * @desc 采购退货列表
 	 * @author liaojianwen
 	 * @date 2017-004-228
 	 */
 	public function actionGetPurchaseReturn()
 	{
 		$request = Yii::$app->request;
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize' => $request->get ( 'pageSize', 10 )
 		];
 		$condition = $request->get ( 'cond', [ ] );
 	
 		$result = PurchaseReturnModel::model()->getPurchaseReturn($pageInfo, $condition);
 		$this->renderJson($result);
 	}
 		
 	
 	/**
 	 * @desc 获取编辑页面采购入库单信息
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionGetPurchaseReturnInfo()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = PurchaseReturnModel::model()->getPurchaseReturnInfo($id);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 获取导入采购入库单信息
 	 * @author liaojianwen
 	 * @date 2017-05-03
 	 */
 	public function actionImplodePurchaseInfo()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = PurchaseReturnModel::model()->implodePurchaseInfo($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 检验是否有删除采购退货的功能
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionCheckDelPurchaseReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检验是否有编辑采购退货的功能
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionCheckEditPurchaseReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 检验是否有确认采购入库的功能
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionCheckConfirmPurchaseReturn()
 	{
 		$this->renderJson('');
 	}
 	
 	/**
 	 * @desc 获取采购退货单的明细
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionGetPurchaseReturnDet()
 	{
 		$request = Yii::$app->request;
 		$id = $request->get('id');
 		$result = PurchaseReturnModel::model()->getPurchaseReturnDet($id);
 		$this->renderJson($result);
 	}
 	
 	
 	/**
 	 * @desc 确认采购退货单
 	 * @author liaojianwen
 	 * @date 2017-04-28
 	 */
 	public function actionConfirmPurchaseReturn()
 	{
 		$id = Yii::$app->request->get('id',0);
 		$result = PurchaseReturnModel::model()->confirmPurchaseReturn($id);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 删除采购退货单
 	 * @author liaojianwen
 	 * @date 2017-05-04
 	 */
 	public function actionDelPurchaseReturn()
 	{
 		$request = Yii::$app->request;
 		$ids = $request->get('ids');
 	
 		$result = PurchaseReturnModel::model()->delPurchaseReturn($ids);
 		$this->renderJson($result);
 	}
 	
 	/**
 	 * @desc 根据仓库获取仓库下面的商品
 	 * @author liaojianwen
 	 * @date 2017-06-21
 	 */
 	public function actionGetProductsByWare()
 	{
 		$request = Yii::$app->request;
 		$ware_id = $request->get('ware');
 	
 		$pageInfo = [
 				'page' => $request->get ( 'page', 1 ),
 				'pageSize'=>$request->get('pageSize',5),
 		];
 		$name = $request->get('name');
 		$result = InventoryModel::model()->getProductByWare($ware_id, $name, $pageInfo);
 		$this->renderJson($result);
 	
 	}
 }