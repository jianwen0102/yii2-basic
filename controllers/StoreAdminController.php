<?php

/**
 * @desc   门店后台管理员控制器
 * @author hehuawei
 * @date   2017-7-21
 */
namespace app\controllers;
use Yii;
use app\controllers\BaseController;
use app\models\StoreAdminModel;
use app\dao\StoreStoreDAO;
use app\models\StorePasswordForm;

class StoreAdminController extends BaseController
{
	/**
	 * @desc 默认控制器
	 */
    public function actionIndex()
	{
	    $this->redirect('store-admin/list-store-admin');	
	}
    
    public function actionListStoreAdmin()
	{
	   return $this->render('list-store-admin');
	}

	/**
	 * @desc   获取列表数据
	 * @author hehuawei
	 * @date   2017-7-21
	 */
    public function actionGetStoreAdminList()
	{
	    $request = Yii::$app->request;
	    $pageInfo = [
	      'page'     => $request->get('page',1), 
		  'pageSize' => $request->get('pageSize',10)
	    ];
		$searchData = $request->get('searchData');
		$filter     = $request->get('filter');
        $storeAdminList  = (new StoreAdminModel())->getStoreAdminList($pageInfo,$searchData,$filter);
		if(count($storeAdminList['Body']['list']>0)){
		   $store_ids = $storeRs = array();
		   foreach($storeAdminList['Body']['list'] as &$v) $store_ids[$v['store_id']] = $v['store_id'];		   
		   if(count($store_ids)>0){
		        $storeList = StoreStoreDao::getInstance()->getStoreRs($store_ids);
				if(count($storeList)>0) foreach($storeList as $st) $storeRs[$st['store_id']] = $st['store_name'];
		   }
		   foreach($storeAdminList['Body']['list'] as &$v){
		      $v['created_at'] = $v['created_at']>0 ? date('Y-m-d H:i:s',$v['created_at']) : '';		 
		      $v['updated_at'] = $v['updated_at']>0 ? date('Y-m-d H:i:s',$v['updated_at']) : '';
			  $v['store_name']  = isset($storeRs[$v['store_id']]) ?$storeRs[$v['store_id']] : '';
		   }      
		}
	    $this->renderJson($storeAdminList);
	}

	/**
	 * @desc 编辑
	 * @author hehuawei
	 * @date 2017-07-29
	 */
    public function actionStoreAdminEdit()
	{
	   	$request = Yii::$app->request;
		$id = $request->get('id');
		$info = (new StoreAdminModel())->find()->where(['id'=>$id])->one();
		if(empty($info)){
			return false;
		}
		$storeList = StoreStoreDao::getInstance()->getStoreRs('',0);
		return $this->render('edit-store-admin',['info'=>$info,'storeList'=>$storeList]);
	}

		
	/**
	 * @desc 检验是否有编辑门店权限
	 * @author hehuawei
	 * @date 2017-07-29
	 */
	public function actionCheckEditAdmin()
	{
		$this->renderJson('');
	}

	/**
	 * @desc 检验是否有添加门店权限
	 * @author hehuawei
	 * @date 2017-07-29
	 */
	public function actionCheckAddAdmin()
	{
		$storeList = StoreStoreDao::getInstance()->getStoreRs('',0);
		$this->renderJson($storeList);
	}

	
	/**
	 * @desc 检验是否有编辑门店权限
	 * @author hehuawei
	 * @date 2017-07-29
	 */
	public function actionAddEditStore()
	{
		$this->renderJson('');
	}

	/**
	 * @desc 检查用户名是否存在
	 * @author hehuawei
	 * @date 2016-11-15
	 */
	public function actionCheckUser()
	{
		$request = Yii::$app->request;
		$type = $request->get('type', 'adminname');
		$val = $request->get($type, '');
		$id = $request->get('id','');
		if ($val){
			switch ($type){
				case 'adminname':
					if(empty($id)){
						$userInfo = StoreAdminModel::find()->where('username=:name',[':name'=>$val])->one();
					} else {
						$userInfo = StoreAdminModel::find()->where('username=:name and id != :id',[':name'=>$val,':id'=>$id])->one();
					}
					break;
				case 'email':
					if(empty($id)){
						$userInfo = StoreAdminModel::find()->where('email=:email',[':email'=>$val])->one();
					} else{
						$userInfo = StoreAdminModel::find()->where('email=:email and id != :id',[':email'=>$val,':id'=>$id])->one();
					}
					break;
			}
			if (!empty($userInfo)){
				echo 'false';
				exit;
			}
		}
		echo 'true';
		exit;
	}

	/**
	 * @desc 更改用户信息
	 * @author hehuawei
	 * @date 2017-7-29
	 */
	public function actionChangeUserPwd()
	{
		$model = new StorePasswordForm(Yii::$app->request->post());
		$result = $model->changeInfo();
		$this->renderJson($result);
	}

	/**
	 * @desc 创建新用户管理员
	 * @author hehuawei
	 * @date 2017-7-29
	 */
	public function actionCreateUser()
	{
		$userInfo = Yii::$app->request->post();
		$result = (new StoreAdminModel())->createUser($userInfo);
		$this->renderJson($result);
	}
}