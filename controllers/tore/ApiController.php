<?php
 namespace app\controllers\tore;
 /**
  * @desc 调用shop 接口
  * @author liaojianwen
  * @date 2017-03-17
  */
 
 use app\controllers\BaseController;
 use app\helpers\storeTokenTool;
 use app\helpers\CInputFilter;
 use yii\web\Controller;
 use Yii;
use app\models\SaleModel;
				 
 class ApiController extends Controller
 {
 	public $enableCsrfValidation = false;
 	/**
	 *@desc 默认控制器
	 *@author liaojianwen
	 *@date 2017-03-17
 	 */
 	public function actionIndex()
 	{
 	}
 	
 	/**
 	 * @desc 返回json数据
 	 * @param $data
 	 * @author liaojianwen
 	 * @date 2016-11-1
 	 */
 	public function renderJson($data)
 	{
 		header('Content-type: application/json');
 		echo json_encode($data);
 		exit;
 	}
 	
 	/**
 	 * @desc 检测 是否是从商城过来的数据
 	 * @author liaojianwen
 	 * @date 2017-03-21
 	 */
    private function scmStoreCheck()
    {
    	$headers = Yii::$app->request->headers;
    	$key = $headers->get('authorization');
		if(! storeTokenTool::getInstance()->verifyKey($key)){
            $result = array(
                'Ack' => 'Failure',
                'body' => 'verify error.'
            );
            $this->renderJson($result);
            Yii::$app->end(); // safe
        }
    }
    
    /**
     * @desc 店铺订单新增采购单
     * @author liaojianwen
     * @date 2017-03-21
     */
    public function actionSellInsert()
    {
    	$this->scmStoreCheck();
    	$res = Yii::$app->request;
    	$productInfo = $res->post();
		$result = SaleModel::model()->storeInsertSell($productInfo);
    	$this->renderJson($result);
    }

// 	/**
// 	 * @desc 商城新增类目
// 	 * @author liaojianwen
// 	 * @date 2017-03-21
// 	 */
//     public function actionCategoryInsert()
//     {
//     	$this->scmStoreCheck();
    	
//     	$resquest = Yii::$app->request;
//     	$categoryInfo = $resquest->post();
//     	$result = CategoryModel::model()->shopInsertCategory($categoryInfo);
//     	$this->renderJson($result);
//     }
    
    /**
     * @desc 店铺退货
     * @author liaojianwen
     * @date 2017-11-30
     */
    public function actionSaleReturnInsert()
    {
    	$this->scmStoreCheck();
    	$res = Yii::$app->request;
    	$saleReturn = $res->post();
    	$result = SaleModel::model()->storeInsertSaleReturn($saleReturn);
    	$this->renderJson($result);
    }
 }