<?php
namespace app\controllers;

/**
 * @desc 盘点控制器
 * @author liaojianwen
 * @date 2017-01-17
 */
 
use Yii;
use app\controllers\BaseController;
use app\models\WarehouseModel;
use app\models\EmployeeModel;
use app\models\InventoryModel;
use app\enum\EnumOriginType;
use app\helpers\CInputFilter;

class InventoryController extends BaseController
{
	/**
	 * @desc 默认控制器
	 * @author liaojianwen
	 * @date 2017-01-17
	 */
	public function actionIndex()
	{
		$this->redirect('list-inventory');
	}
	
	/**
	 * @desc 盘点单列表
	 * @author liaojianwen
	 * @date 2017-1-17
	 */
	public function actionListInventory()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('inventorylist',['stock'=>$stock]);
// 		return $this->render('inventorylist');
	}
	
	/**
	 * @desc 新增盘点单
	 * @author liaojianwen
	 * @date 2017-01-18
	 */
	public function actionAddInventory()
	{
		$instoreNo = EnumOriginType::INVENTORY . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'addinventory', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $instoreNo
		] );
	}
	
	/**
	 * @desc 新增盘盈单
	 * @author liaojianwen
	 * @date 2017-01-17
	 */
	public function actionAddInventoryFull()
	{
		$inventoryNo = EnumOriginType::INVENTORY_FULL . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'addinventoryfull', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $inventoryNo
		] );
	}
	
	/**
	 * @desc 盘盈单编辑
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionEditInventoryFull()
	{
		$inventoryNo = EnumOriginType::INVENTORY_FULL . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'editinventoryfull', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $inventoryNo
		] );
	}
	
	/**
	 * @desc 盘盈单列表
	 * @author liaojianwen
	 * @date 2017-01-17
	 */
	public function actionListInventoryFull()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('inventoryfull_list',['stock'=>$stock]);
	}
	
	/**
	 * @desc 盘亏单
	 * @author liaojianwen
	 * @date 2017-01-17
	 */
	public function actionAddInventoryLost()
	{
		$inventoryNo = EnumOriginType::INVENTORY_LOST . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'addinventorylost', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $inventoryNo
		] );
	}
	
	/**
	 * @desc 盘亏单编辑
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionEditInventoryLost()
	{
		$inventoryNo = EnumOriginType::INVENTORY_LOST . date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'editinventorylost', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $inventoryNo
		] );
	}
	
	/**
	 * @desc 盘亏单列表
	 * @author liaojianwen
	 * @date 2017-01-17
	 */
	public function actionListInventoryLost()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('inventorylost_list',['stock'=>$stock]);
	}
	
	
	/**
	 * @desc 根据仓库获取仓库下面的商品
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionGetProductByWare()
	{
		$request = Yii::$app->request;
		$ware_id = $request->get('ware');
		
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize'=>$request->get('pageSize',5),
		];
		$name = $request->get('name');
		$result = InventoryModel::model()->getProductByWare($ware_id, $name, $pageInfo);
		$this->renderJson($result);
		
	}
	
	/**
	 * @desc 保存盘点单
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionSaveInventory()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
		$result = InventoryModel::model ()->saveInventory ( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 获取盘点单列表
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionGetInventorys()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter');
		
		$result = InventoryModel::model ()->getInventorys ( $pageInfo, $condition, $filter);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 获取盘点明细
	 * @author liaojianwen
	 * @date 2017-02-09
	 */
	public  function actionGetInventoryDet()
	{
// 		$request = Yii::$app->request;
// 		$id = $request->get('id');
		$id = CInputFilter::getInt('id');
		$result = InventoryModel::model()->getInventoryDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 盘点单编辑页面
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionEditInventory()
	{
		$instoreNo = date('YmdHis');
		$warehouse = WarehouseModel::model()->listWarehouse();
		$admin = EmployeeModel::model()->listEmployee();
		return $this->render ( 'editinventory', [
				'warehouse' => $warehouse,
				'admin' => $admin,
				'NO' => $instoreNo
		] );
	}
	
	/**
	 * @desc 检查是否有编辑盘点单权限
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionCheckEditInventory()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 初始化编辑页面数据
	 * @author liaojianwen
	 * @date 2017-01-19
	 */
	public function actionGetInventoryInfo()
	{
		$id = Yii::$app->request->get('id');
		$result = InventoryModel::model()->getInventoryInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有删除盘点单的权限
	 * @author liaojianwen
	 * @date 2017-01-20
	 */
	public function actionCheckDelInventory()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 删除盘点单
	 * @author liaojianwen
	 * @date 2017-01-20
	 */
	public function actionDelInventory()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
		
		$result = InventoryModel::model()->delInventory($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检查是否有确认权限
	 * @author liaojianwen
	 * @date 2017-02-04
	 */
	public function actionCheckConfirmInventory()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 确认盘点单
	 * @author liaojianwen
	 * @date 2017-02-04
	 */
	public function actionConfirmInventory()
	{
		$id = Yii::$app->request->get('id',0);
		$result = InventoryModel::model()->confirmInventory($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 保存盘盈单
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionSaveInventoryFull()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
		$result = InventoryModel::model ()->saveInventoryFull ( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 盘盈单列表信息
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionGetInventoryFull()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter');
		
		$result = InventoryModel::model ()->getInventoryFull ( $pageInfo, $condition, $filter);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 盘盈编辑初始化信息
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionGetInventoryFullInfo()
	{
		$id = Yii::$app->request->get('id');
		$result = InventoryModel::model()->getInventoryFullInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取盘盈单的明细
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionGetInventoryFullDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = InventoryModel::model()->getInventoryFullDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除盘盈单
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionDelInventoryFull()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
	
		$result = InventoryModel::model()->delInventoryFull($ids);
		$this->renderJson($result);
	}
	

	
	/**
	 * @desc 确认盘盈单
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionConfirmInventoryFull()
	{
		$id = Yii::$app->request->get('id',0);
		$result = InventoryModel::model()->confirmInventoryFull($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有确认盘盈单权限
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionCheckConfirmInventoryFull()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有编辑盘盈单权限
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionCheckEditInventoryFull()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有删除盘盈单权限
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionCheckDelInventoryFull()
	{
		$this->renderJson('');
	}
	/**
	 * @desc 保存盘亏单
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionSaveInventoryLost()
	{
// 		$request = Yii::$app->request;
// 		$head = $request->get ( 'head' );
// 		$detail = $request->get ( 'det' );
// 		$id = $request->get ( 'id', 0 );
		$head = CInputFilter::getArray('head', '');
		$detail = CInputFilter::getArray('det', '');
		$id = CInputFilter::getInt('id',0);
		$remove = CInputFilter::getArray('remove','');
		$result = InventoryModel::model ()->saveInventoryLost ( $head, $detail, $remove, $id );
		$this->renderJson ( $result );
	}
	/**
	 * @desc 上传报损产品图片
	 * @author lizichuan
	 * @date 2018-6-14
	 */
	public function actionUploadInventoryLostImage()
	{
		$product_id = CInputFilter::getInt('product_id',0);
		$result = InventoryModel::model()->uploadInventoryLostImage();
		$result['Body']['product_id'] = $product_id;
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 盘亏单列表信息
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionGetInventoryLost()
	{
		$request = Yii::$app->request;
		$pageInfo = [
				'page' => $request->get ( 'page', 1 ),
				'pageSize' => $request->get ( 'pageSize', 10 )
		];
		$condition = $request->get ( 'cond', [ ] );
		$filter = $request->get('filter');
	
		$result = InventoryModel::model ()->getInventoryLost ( $pageInfo, $condition, $filter);
		$this->renderJson ( $result );
	}
	
	/**
	 * @desc 盘亏编辑初始化信息
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionGetInventoryLostInfo()
	{
		$id = Yii::$app->request->get('id');
		$result = InventoryModel::model()->getInventoryLostInfo($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 获取盘亏单的明细
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionGetInventoryLostDet()
	{
		$request = Yii::$app->request;
		$id = $request->get('id');
		$result = InventoryModel::model()->getInventoryLostDet($id);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 删除盘盈单
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionDelInventoryLost()
	{
		$request = Yii::$app->request;
		$ids = $request->get('ids');
	
		$result = InventoryModel::model()->delInventoryLost($ids);
		$this->renderJson($result);
	}
	
	/**
	 * @desc 检验是否有确认盘盈单权限
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionCheckConfirmInventoryLost()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有编辑盘亏单权限
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionCheckEditInventoryLost()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 检查是否有删除盘亏单权限
	 * @author liaojianwen
	 * @date 2017-02-06
	 */
	public function actionCheckDelInventoryLost()
	{
		$this->renderJson('');
	}
	
	/**
	 * @desc 确认盘盈单
	 * @author liaojianwen
	 * @date 2017-02-05
	 */
	public function actionConfirmInventoryLost()
	{
		$id = Yii::$app->request->get('id',0);
		$result = InventoryModel::model()->confirmInventoryLost($id);
		$this->renderJson($result);
	}
	
	
	/**
	 * @desc 报盈单汇总
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionInventoryFullSummary()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('inventoryfullsum', ['stock'=> $stock]);
	}
	
	/**
	 * @desc 获取报盈单汇总数据
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionGetInventoryFullSummary()
	{
		$pageInfo = [
				'page'=>CInputFilter::getInt('page',1),
				'pageSize' => CInputFilter::getInt('pageSize',10),
		];
		$condition = CInputFilter::getArray('cond', '');
		$result = InventoryModel::model()->getInventoryFullSummary($pageInfo, $condition);
		$this->renderJson($result);
	}
	
	
	/**
	 * @desc 报损单汇总
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionInventoryLostSummary()
	{
		$stock = WarehouseModel::model()->listWarehouse();
		return $this->render('inventorylostsum', ['stock'=> $stock]);
	}
	
	/**
	 * @desc 获取报损单汇总数据
	 * @author liaojianwen
	 * @date 2018-01-30
	 */
	public function actionGetInventoryLostSummary()
	{
		$pageInfo = [
				'page'=>CInputFilter::getInt('page',1),
				'pageSize' => CInputFilter::getInt('pageSize',10),
		];
		$condition = CInputFilter::getArray('cond', '');
		$result = InventoryModel::model()->getInventoryLostSummary($pageInfo, $condition);
		$this->renderJson($result);
	}
	
}

