<?php
namespace app\enum;
class UserEnum 
{
	//登录错误代码
	const LOGIN_CODE_SUCCESS = 0;
	const LOGIN_MSG_SUCCESS = '登录成功';
	const LOGIN_CODE_NOLOGIN = 1;
	const LOGIN_MSG_NOLOGIN = '登录状态失效';
	const LOGIN_CODE_LOGINCODE = 3;
	const LOGIN_MSG_LOGINCODE = '您输入的后台管理认证码不对，请重新输入。';
	const LOGIN_CODE_NOUSER = 4;
	const LOGIN_MSG_NOUSER = '无此帐号，请检查！';
	const LOGIN_CODE_NOPWD = 5;
	const LOGIN_MSG_NOPWD = '密码错误，请检查！';
	const LOGIN_CODE_CAPTCHA = 6;
	const LOGIN_MSG_CAPTCHA = '您输入的验证码和系统产生的不一致，请重新输入。';
	const LOGIN_CODE_NOIN = 7;
	const LOGIN_MSG_NOIN = '帐号被禁用！';
	const LOGIN_CODE_NO_SHOP = 8;
	const LOGIN_MSG_NO_SHOP = '您没有商店或没有加入商店，不能登录!';
	
	
 	
}
