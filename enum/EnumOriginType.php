<?php
namespace app\enum;
/**
 * @desc 订单类型枚举
 */

class EnumOriginType 
{
	//单据类型编号依次添加
	//入库单类型
	/**
	 * @var 采购入库
	 */
	const origin_income = 1;//采购入库
	/**
	 * @var 其他入库
	 */
	const origin_other_in = 2;//其他入库
	/**
	 * @var 采购退货
	 */
	const origin_outcome = 3;//采购退货
	/**
	 * @var 其他出库
	 */
	const origin_other_out = 4;//其他出库
	/**
	 * @var 盘盈单
	 */
	const origin_py_out  = 5;//盘盈单 （入库）
	/**
	 * @var 盘亏单
	 */
	const origin_pk_in = 6;//盘亏单（出库）
	
	/**
	 * @var 商城订单发货
	 */
	const origin_sale_out_market = 7;//商城发货单
	
	/**
	 * @var 集团发货
	 */
	const origin_sale_out_company = 8;//集团发货
	
	/**
	 * @var 付款结算
	 */
	const origin_payable_other = 9;//付款结算
	
	/**
	 * @var 采购应付单
	 */
	const origin_purchase_pay = 10;//采购应付单

	/**
	 * @var 调拨单
	 */
	const origin_transfer = 11;//调拨单
	
	/**
	 * @var 试吃出库
	 */
	const origin_eat = 12;//试吃
	
	/**
	 * @var 活动出库
	 */
	const origin_outside_activity = 13;//活动出库
	
	/**
	 * @var 活动入库
	 */
	const origin_inside_activity = 14;//活动出库
	
	/**
	 * @var 商城订单退货
	 */
	const origin_sale_return_market = 15;//商城退货单
	
	/**
	 * @var 集团退货
	 */
	const origin_sale_return_company = 16;//集团退货
	
	/**
	 * @var 采购退货单（应付款单据）
	 */
	const origin_purchase_return = 17;//采购退货单
	
	/**
	 * @desc 拆装单
	 */
	const origin_disassembly = 18;//拆装单
	
	/**
	 * @desc 店铺退货
	 */
	const origin_store_return = 19;///'店铺退货'
	
	/**
	 * @desc 报废单
	 */
	const origin_scrapped = 20;//报废单
	
	/**
	 * @var 单据类型
	 */
	public static $origin_type = array(
			self::origin_income => '采购入库',
			self::origin_other_in => '其他入库',
			self::origin_outcome =>'采购退货',
			self::origin_other_out =>'其他出库',
			self::origin_pk_in => '盘亏单',
			self::origin_py_out => '盘盈单',
			self::origin_sale_out_company =>'集团发货',
			self::origin_sale_out_market => '商城订单发货',
			self::origin_transfer => '调拨单',
			self::origin_inside_activity => '活动入库',
			self::origin_eat => '商品试吃',
			self::origin_outside_activity => '活动出库',
			self::origin_sale_return_market => '商城退货',
			self::origin_sale_return_company => '集团退货',
			self::origin_disassembly => '拆装单',
			self::origin_store_return =>'店铺退货',
			self::origin_scrapped  => '报废单',
			
	);
	
	/**
	 * @var 入库
	*/
	public static $instore_type = array(
			self::origin_other_in => '其他入库',
// 			self::origin_income => '采购入库',
			self::origin_inside_activity => '活动入库'
	);
	
	/**
	 * @var 出库
	 */
	public static $outstore_type = [
			self::origin_other_out=>'其他出库',
// 			self::origin_outcome =>'采购退货',
			self::origin_eat => '商品试吃',
			self::origin_outside_activity => '活动出库',
	];
	
	/**
	 * @var 销售出库
	 */
	public static $sale_type = [
		self::origin_sale_out_market => '商城订单发货',
		self::origin_sale_out_company =>'集团发货',
		
	];
	
	/**
	 * @var 销售退货
	 */
	public static $sale_return_type = [
		self::origin_sale_return_market => '商城订单退货',
		self::origin_sale_return_company =>'集团退货',
		self::origin_store_return =>'店铺退货',
	];
	
	/**
	 * @var 采购入库
	 */
	public static  $purchase_type = [
			self::origin_income => '采购入库',
	];
	
	/**
	 * @var 应付款来源
	 */
	public static $payable_orgin = [
		self::origin_income => '采购入库',
		self::origin_outcome => '采购退货',
	];
	
	/**
	 * @var 采购退货
	 */
	public static  $purchase_back_type = [
			self::origin_outcome => '采购退货',
	];
	
	/**
	 * @var 应付结算
	 */
	public static $payables_type = [
			self::origin_payable_other =>'付款结算',
	];
	
	/**
	 * @var 采购应付单
	 */
	public static $pay_type = [
		self::origin_purchase_pay => '采购应付单',
		self::origin_purchase_return => '采购退货单',
	];
	
	public static $transfer_type  = [
		self::origin_scrapped => '报废单',
		self::origin_transfer => '调拨单',
	];
	/**
	 * @var 盘点单前缀
	 */
	const INVENTORY = 'PDD-';
	
	/**
	 * @var 盘盈单前缀
	 */
	const INVENTORY_FULL = 'PYD-';
	
	/**
	 * @var 盘亏单前缀
	 */
	const INVENTORY_LOST = 'PKD-';
	
	/**
	 * @var 销售出库
	 */
	const SALE_OUT = 'XSCK-';
	
	/**
	 * @var 采购单
	 */
	const PROCUREMENT = 'CGD-';
	
	/**
	 * @var 采购入库
	 */
	const PURCHASE = 'CGRK-';
	
	/**
	 * @var 其他入库
	 */
	const OTHER_IN ='QTRK-';
	
	/**
	 * @var 其他出库
	 */
	const OTHER_OUT ='QTCK-';
	
	/**
	 * @var 付款结算单
	 */
	const PAYABLE_OTHER = 'FKJS-';
	
	/**
	 * @var 仓库调拨单
	 */
	const TRANSFER = 'DB-';
	
	/**
	 * @var 销售订单
	 */
	const SALE = 'XSD-';
	
	/**
	 * @var 销售退货
	 */
	const SALE_RETURN = 'XSTH-';
	
	/**
	 * @var 采购退货
	 */
	const PURCHASE_RETURN = 'CGTH-';//采购退货
	
	/**
	 * @var 供应商请款单单号
	 */
	const VENDOR_PAYMENT = 'QKD-';//请款单
	
	/**
	 * @var 成本调整单
	 */
	const PRICE_ADJUSTMENT = 'CBTZ-';//成本调整
	
	/**
	 * @desc 拆装单
	 */
	const DISASSEMBLY = 'CZD-';
}