<?php
namespace app\enum;
/**
 * @desc 权限类枚举
 * @author liaojianwen
 * @date 2017-01-08
 */

class RoleEnum
{
	const GROUP_ADMIN ='admin';
    const GROUP_CATEGORY = 'category';
    const GROUP_DEPARTMENT = 'department';
    const GROUP_EMPLOYEE = 'employee';
    const GROUP_INSTORE = 'instore';
    const GROUP_OUTSTORE = 'outstore';
    const GROUP_PERMISSION = 'permission';
    const GROUP_PROCUREMENT = 'procurement';
    const GROUP_PRODUCT = 'product';
    const GROUP_PRODUCTLOG = 'product-log';
    const GROUP_PURCHASE = 'purchase';
//     const GROUP_SITE = 'site';
    const GROUP_STOCKPILE = 'stock-pile';
    const GROUP_SUPPLIER = 'supplier';
    const GROUP_TRANSACTION = 'transaction';
    const GROUP_WAREHOUSE = 'warehouse';
    const GROUP_INVENTORY='inventory';
    const GROUP_SALE = 'sale';
    const GROUP_FUNDS ='funds';
    const GROUP_TRANSFER = 'transfer';
    const GROUP_PURCHASE_RETURN = 'purchase-return';
    const GROUP_CLIENT = 'client';
    const GROUP_PRICE = 'price';
    const GROUP_DISASSEMBLY = 'disassembly';
    
	public static $group = array (
			self::GROUP_ADMIN => '用户管理',
			self::GROUP_CATEGORY => '类目管理',
			self::GROUP_DEPARTMENT => '部门管理',
			self::GROUP_EMPLOYEE => '员工管理',
			self::GROUP_INSTORE => '其他入库管理',
			self::GROUP_OUTSTORE => '其他出库管理',
			self::GROUP_PERMISSION => '权限管理',
			self::GROUP_PROCUREMENT => '采购订单管理',
			self::GROUP_PRODUCT => '商品管理',
			self::GROUP_PRODUCTLOG => '商品更新日志管理',
			self::GROUP_PURCHASE => '采购入库管理',
// 			self::GROUP_SITE => '管理',
			self::GROUP_STOCKPILE => '分仓管理',
			self::GROUP_SUPPLIER => '供应商管理',
			self::GROUP_TRANSACTION => '出入库报表管理',
			self::GROUP_WAREHOUSE => '仓库管理',
			self::GROUP_INVENTORY => '盘点管理',
			self::GROUP_SALE =>'销售管理',
			self::GROUP_FUNDS => '往来单位',
			self::GROUP_TRANSFER => '调拨管理',
			self::GROUP_PURCHASE_RETURN => '采购退货管理',
			self::GROUP_CLIENT => '客户管理',
			self::GROUP_PRICE => '成本调整管理',
			self::GROUP_DISASSEMBLY => '拆装管理',
	);
    
	
	//个人信息
	/**
	 * @var 更改个人信息
	 */
	const ROLE_PERSONAL_CHANGEPWD = 'admin/change-pwd';//更改个人信息
	const ROLE_PERSONAL_LIST = 'admin/personal-info';//查看个人信息
	
	//管理员列表
	const ROLE_ADMIN_LIST = 'admin/list-users';//管理员列表
	const ROLE_ADMIN_ADD ='admin/check-add-user';//新增管理员
	const ROLE_ADMIN_EDIT = 'admin/check-edit-user';//编辑管理员
	const ROLE_ADMIN_DEL ='admin/check-del-user';//删除管理员
	const ROLE_ADMIN_ASSIGN = 'admin/check-assign-user';//分配角色
	
	//权限管理
	const ROLE_PERMISSION_LIST = 'permission/list-group';//权限分组列表
	const ROLE_PERMISSION_ADD = 'permission/check-add-group';//新增权限分组
	const ROLE_PERMISSION_EDIT = 'permission/check-edit-group';//编辑权限分组
	const ROLE_PERMISSION_DEL = 'permission/check-del-group';//删除权限分组
	const ROLE_PERMISSION_ASSIGN ='permission/check-assign-group';//分配权限分组
	
	//类目管理
	const ROLE_CATEGORY_LIST ='category/list-category';//类目列表
	const ROLE_CATEGORY_SAVE = 'category/check-category-update';//保存（新增编辑）
	const ROLE_CATEGORY_DEL = 'category/check-category-del';//删除类目
	
	//商品管理
	const ROLE_PRODUCT_LIST = 'product/list-product';//商品列表
	const ROLE_PRODUCT_ADD = 'product/check-add-product';//新增商品
	const ROLE_PRODUCT_EDIT = 'product/check-edit-product';//编辑商品
	const ROLE_PRODUCT_DEL = 'product/check-del-product';//删除商品 
	/**
	 * @var 检查是否有显示成本价权限
	 */
	const ROLE_PRODUCT_PRICE = 'product/check-product-price';//检查是否有显示成本价权限
	
	//部门管理
	const ROLE_DEPARTMENT_LIST = 'department/list-depart';//部门列表
	const ROLE_DEPAETMENT_ADD = 'department/check-add-depart';//新增部门
	const ROLE_DEPARTMENT_EDIT = 'department/check-edit-depart';//编辑部门
	const ROLE_DEPARTMENT_DEL = 'department/check-del-depart';//部门删除
	
	//员工管理
	const ROLE_EMPLOYEE_LIST = 'employee/list-employee';//员工列表
	const ROLE_EMPLOYEE_ADD = 'employee/check-add-emp';//新增员工
	const ROLE_EMPLOYEE_EDIT = 'employee/check-edit-emp';//编辑员工
	const ROLE_EMPLOYEE_DEL = 'employee/check-del-emp';//删除员工
	
	//供应商管理
	const ROLE_VENDOR_LIST = 'supplier/list-supplier';
	const ROLE_VENDOR_ADD = 'supplier/check-add-supplier';//新增供应商
	const ROLE_VENDOR_EDIT = 'supplier/check-edit-supplier';//编辑供应商
	const ROLE_VENDOR_DEL= 'supplier/check-del-supplier';//删除供应商
	
	//商品更新日志
	const ROLE_PRODUCTLOG_LIST = 'product-log/list-log';
	/*--采购--*/
	//采购报价
	const ROLE_QUOTES_ADD = 'sale/add-quotes';
	const ROLE_QUOTES_LIST = 'sale/list-quotes';
	const ROLE_QUOTES_EDIT = 'sale/check-edit-quotes';
	const ROLE_QUOTES_DEL = 'sale/check-del-quotes';
	
	//采购订单
	const ROLE_PROCUREMENT_ADD = 'procurement/add-procurement';//新建采购单
	const ROLE_PROCUREMENT_LIST = 'procurement/list-procurement';//采购单列表
	const ROLE_PROCUREMENT_EDIT = 'procurement/check-edit-pro';//编辑采购单
	const ROLE_PROCUREMENT_DEL = 'procurement/check-del-pro';//删除采购单
	const ROLE_PROCUREMENT_EXPORT = 'procurement/check-procurement-export';//导出采购单
	
	//采购入库单
	const ROLE_PURCHASE_ADD = 'purchase/add-purchase';//新增采购入库
	const ROLE_PURCHASE_LIST = 'purchase/list-purchase';//采购入库列表
	const ROLE_PURCHASE_DEL = 'purchase/check-del-pur';//删除采购入库
	const ROLE_PURCHASE_EDIT = 'purchase/check-edit-pur';//编辑采购入库
	const ROLE_PURCHASE_CONFIRM ='purchase/check-confirm-pur';//确认采购入库
	
	//采购统计
	const ROLE_PURCHASE_VENDOR = 'purchase/purchase-vendor';//采购统计
	const ROLE_PURCHASE_VENDOR_EXPORT = 'purchase/check-purchase-vendor-export';//导出采购统计
	

	
	//其他入库
	const ROLE_INSTORE_ADD = 'instore/instore';//新增其他入库
	const ROLE_INSTORE_LIST = 'instore/list-instore';//其他入库列表
	const ROLE_INSTORE_DEL = 'instore/check-del-instore';//删除其他入库
	const ROLE_INSTORE_EDIT = 'instore/check-edit-instore';//编辑其他入库
	const ROLE_INSTORE_CONFIRM ='instore/check-confirm-instore';//确认采购其他入库
	const ROLE_INSTORE_SUMMARY = 'instore/instore-summary';//其他入库汇总
	
	//其他出库
	const ROLE_OUTSTORE_ADD = 'outstore/outstore';//新增其他出库
	const ROLE_OUTSTORE_LIST = 'outstore/list-outstore';//其他出库列表
	const ROLE_OUTSTORE_DEL = 'outstore/check-del-outstore';//删除其他出库
	const ROLE_OUTSTORE_EDIT = 'outstore/check-edit-outstore';//编辑其他出库
	const ROLE_OUTSTORE_CONFIRM ='outstore/check-confirm-outstore';//确认采购其他出库
	const ROLE_OUTSTORE_SUMMARY = 'outstore/outstore-summary';//其他出库汇总
	
	//仓库信息
	const ROLE_WAREHOUSE_LIST = 'warehouse/list-warehouse';//仓库列表
	const ROLE_WAREHOUSE_ADD = 'warehouse/check-add-ware';//新增仓库
	const ROLE_WAREHOUSE_EDIT = 'warehouse/check-edit-ware';//仓库编辑
	const ROLE_WAREHOUSE_DEL = 'warehouse/check-del-ware';//删除仓库
	
	//单位信息
	const ROLE_UNIT_LIST = 'warehouse/list-unit';//单位列表
	const ROLE_UNIT_ADD = 'warehouse/check-add-unit';//新增单位
	const ROLE_UNIT_EDIT = 'warehouse/check-edit-unit';//编辑单位
	const ROLE_UNIT_DEL = 'warehouse/check-del-unit';//删除单位
	
	//库存信息
	const ROLE_PRODUCTSTOCK_LIST = 'product/list-stock-pile';//库存信息列表
	const ROLE_PRODUCTSTOCK_EXPORT = 'product/check-product-stock-export';//导出库存信息
	
	//出入库明细
	const ROLE_TRANSACTION_LIST = 'transaction/list-transaction';//出入库明细列表
	
	//分仓库存查询
	const ROLE_STOCKPILE_LIST = 'stock-pile/list-stock-pile';//分仓库存查询列表
	
	//盘点管理
	//盘点单
	const ROLE_INVENTORY_LIST = 'inventory/list-inventory';
	const ROLE_INVENTORY_ADD = 'inventory/add-inventory';
	const ROLE_INVENTORY_EDIT = 'inventory/check-edit-inventory';
	const ROLE_INVENTORY_DEL = 'inventory/check-del-inventory';
	const ROLE_INVENTORY_CONFIRM ='inventory/check-confirm-inventory';
	//盘盈单
	const ROLE_INVENTORY_FULL_LIST = 'inventory/list-inventory-full';
	const ROLE_INVENTORY_FULL_ADD = 'inventory/add-inventory-full';
	const ROLE_INVENTORY_FULL_EDIT = 'inventory/check-edit-inventory-full';
	const ROLE_INVENTORY_FULL_DEL = 'inventory/check-del-inventory-full';
	const ROLE_INVENTORY_FULL_CONFIRM ='inventory/check-confirm-inventory-full';
	const ROLE_INVENTORY_FULL_SUMMARY = 'inventory/inventory-full-summary';//报盈汇总
	//盘亏单
	const ROLE_INVENTORY_LOST_LIST = 'inventory/list-inventory-lost';
	const ROLE_INVENTORY_LOST_ADD = 'inventory/add-inventory-lost';
	const ROLE_INVENTORY_LOST_EDIT = 'inventory/check-edit-inventory-lost';
	const ROLE_INVENTORY_LOST_DEL = 'inventory/check-del-inventory-lost';
	const ROLE_INVENTORY_LOST_CONFIRM ='inventory/check-confirm-inventory-lost';
	const ROLE_INVENTORY_LOST_SUMMARY = 'inventory/inventory-lost-summary';//报损汇总
	
	/* --销售管理--*/
	//销售订单
	const ROLE_SELL_LIST = 'sale/list-sells';
	const ROLE_SELL_ADD = 'sale/add-sells';
	const ROLE_SELL_EDIT = 'sale/check-edit-sell';
	const ROLE_SELL_DEL = 'sale/check-del-sell';
	const ROLE_SELL_CONFIRM = 'sale/check-confirm-sell';
	//销售出库单
	const ROLE_SALE_OUT_LIST = 'sale/list-sale-out';
	const ROLE_SALE_OUT_ADD = 'sale/add-sale-out';
	const ROLE_SALE_OUT_EDIT = 'sale/check-edit-sale-out';
	const ROLE_SALE_OUT_DEL = 'sale/check-del-sale-out';
	const ROLE_SALE_OUT_CONFIRM ='sale/check-confirm-sale-out';
	
	//销售（赠送统计）
	const ROLE_SALE_OUT_FREE = 'sale/sale-out-free';//赠送统计
	const ROLE_SALE_OUT_FREE_EXP = 'sale/check-sale-out-free-export';//检查是否有导出赠送统计的权限
	
	//往来单位
	//应付单
	const ROLE_PAYABLES_LIST = 'funds/list-payables';
	//应付结算
	const ROLE_SETTLE_PAYABLES_LIST ='funds/list-settle-payables';
	const ROLE_SETTLE_PAYABLES_ADD = 'funds/add-settle-payables';
	const ROLE_SETTLE_PAYABLES_EDIT = 'funds/check-edit-settle-payables';
	const ROLE_SETTLE_PAYABLES_DEL = 'funds/check-delete-settle-payables';
	const ROLE_SETTLE_PAYABLES_CONFIRM = 'funds/check-confirm-settle-payables';
	const ROLE_SETTLE_PAYABLES_INVALID = 'funds/check-invalid-settle-payables';
	
	//调拨单
	const ROLE_TRANSFER_LIST = 'transfer/list-transfer';
	const ROLE_TRANSFER_ADD ='transfer/add-transfer';
	const ROLE_TRANSFER_EDIT ='transfer/check-edit-transfer';
	const ROLE_TRANSFER_DEL ='transfer/check-del-transfer';
	const ROLE_TRANSFER_CONFIRM ='transfer/check-confirm-transfer';
	const ROLE_TRANSFER_STATISTICS ='transfer/transfer-statistics';
	const ROLE_EXPORT_TRANSFER_STATISTICS ='transfer/check-transfer-statistics-export';
	
	
	//采购退货
	const ROLE_PURCHASE_RETURN_ADD = 'purchase-return/add-purchase-return';//新增
	const ROLE_PURCHASE_RETURN_LIST = 'purchase-return/list-purchase-return';//列表
	const ROLE_PURCHASE_RETURN_EDIT = 'purchase-return/check-edit-purchase-return';//编辑
	const ROLE_PURCHASE_RETURN_DEL = 'purchase-return/check-del-purchase-return';//删除
	const ROLE_PURCHASE_RETURN_CONFIRM = 'purchase-return/check-confirm-purchase-return';//确认
	
	//销售退货
	const ROLE_SALE_RETURN_ADD = 'sale/add-sale-return';//新增
	const ROLE_SALE_RETURN_LIST = 'sale/list-sale-return';//列表
	const ROLE_SALE_RETURN_EDIT = 'sale/check-edit-sale-out';//编辑
	const ROLE_SALE_RETURN_DEL = 'sale/check-del-sale-out';//删除
	const ROLE_SALE_RETURN_CONFIRM ='sale/check-confirm-sale-out';//确认
	
	//库存汇总
	const ROLE_STOCK_PILE_SUM  = 'transaction/list-stock-sum';//库存汇总
	
	//导入execl 更新供货价、指导价
	const ROLE_PRODUCT_IMPLOADE_EXECL = 'product/check-implode-product';
	
	//商品导出execl 
	const ROLE_PRODUCT_EXPORT_EXECL = 'product/check-explode-product';
	
	//客户管理
	const ROLE_CLIENT_LIST = 'client/list-client';
	const ROLE_CLIENT_ADD = 'client/check-add-client';//新增客户
	const ROLE_CLIENT_EDIT = 'client/check-edit-client';//编辑客户
	const ROLE_CLIENT_DEL= 'client/check-del-client';//删除客户
	
	//成本调整单
	const ROLE_PRICE_ADJUSTMENT_LIST = 'price/list-adjustment';
	const ROLE_PRICE_ADJUSTMENT_ADD ='price/add-adjustment';
	const ROLE_PRICE_ADJUSTMENT_EDIT = 'price/check-edit-adjustment';
	const ROLE_PRICE_ADJUSTMENT_CONFIRM = 'price/check-confirm-adjustment';
	const ROLE_PRICE_ADJUSTMENT_SEARCH  = 'price/list-adjustment-info';
	
	// 请款单
	const ROLE_VENDOR_PAYMENT_LIST = 'funds/list-vendor-payment';
	const ROLE_VENDOR_PAYMENT_ADD = 'funds/add-vendor-payment';
	const ROLE_VENDOR_PAYMENT_EDIT = 'funds/check-edit-vendor-payment';
	const ROLE_VENDOR_PAYMENT_DEL = 'funds/check-delete-vendor-payment';
	const ROLE_VENDOR_PAYMENT_EXPLODE = 'funds/checks-explode-vendor-payment';
	const ROLE_VENDOR_PAYMENT_SUMMARY_EXPLODE = 'funds/check-explode-payment-summary';
	
	
	//拆装单
	const ROLE_DISASSEMBLY_LIST = 'disassembly/list-dissassembly';
	const ROLE_DISASSEMBLY_ADD = 'disassembly/add-disassembly';
	const ROLE_DISASSEMBLY_EDIT = 'disassembly/check-edit-disassembly';
	const ROLE_DISASSEMBLY_DEL = 'disassembly/check-del-disassembly';
	const ROLE_DISASSEMBLY_CONFIRM ='disassembly/check-confirm-disassembly';
	
	//报表
// 	const ROLE_SALE_GROSS_PROFIT_DETAIL = 'reports/sale-gross-profit-details';//销售毛细明细表
	const ROLE_SALE_GROSS_PROFIT_SUMMARY = 'reports/sale-gross-profit-summary';//销售毛细汇总表
	const ROLE_STORE_GROSS_PROFIT_SUMMARY= 'reports/store-gross-profit-summary';//门店利润汇总表
	const ROLE_STORE_SPECIAL_GROSS_PROFIT_SUMMARY = 'reports/special-gross-profit-summary';//特定商品利润报表
	
}