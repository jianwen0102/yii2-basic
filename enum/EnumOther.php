<?php
namespace  app\enum;
/**
 * @desc  综合枚举类
 * @author liaojianwen
 * @date 2016-11-1
 */
class EnumOther
{
	 /**
     * @var 调用接口成功状态
     */
    const ACK_SUCCESS = 'Success';
    
    /**
     * @var 调用接口失败状态
     */
    const ACK_FAILURE = 'Failure';
    
    /**
     * @var 调用接口警告状态
     */
    const ACK_WARNING = 'Warning';
    
    /**
     * @var 默认显示页码
     */
    const DEFAULT_PAGE_NUM = 1;

    /**
     * @var 默认每页显示数量
     */
    const DEFAULT_PAGE_SIZE = 15;
    
    /**
     * @var 删除状态
     */
    const DELETED = 1;
    
    /**
     * @var 未删除状态
     */
    const NO_DELETE = 0;
    
    /**
     * @var 已红冲（相当于无效）
     */
    const INVALID = 2; 	
    
    /**
     * @var 出库状态
     */
    const OUT_FLAG = 0;
    
    /**
     * @var 入库状态
     */
    const IN_FLAG =1;
    
    /**
     * @var 显示库存不为零
     */
    const QUANTITY_NOT_NULL = 1;
    
    /**
     * @var 显示库存为零
     */
    const NONE_QUANTITY = 2;
  
    /**
     * @var 没有权限的显示信息
     */
    const AUTH_MSG = '你没有操作权限，如要操作，请向管理员申请';
    
    /**
     * @var 订单提醒，发货日期距离当前日期
     */
    const WARNING_TIME = 86400;//一天 24*60*60
    
    /**
     * @var transaction_log 
     */
    const TRANSACTION_LOG = 'update by ';
    
    /**
     * @var 入库单是否更改
     */
    const NO_EDIT = 0;//未修改
    const EDIT = 1;//已修改
    
    /**
     * @var 确认
     */
    const CONFIRM = 1;//确认
    
    /**
     * @var 未确认
     */
    const NO_CONFIRM = 0;//未确认
   
    /**
     * @var 单据完成
     */
    const FINISHED = 1;
    
    /**
     * @var 单据未完成
     */
    const NO_FINISHED = 0;
    
    
	/**
	 * @var 订单状态
	 */
    const OS_UNCONFIRMED = 0;// 未确认
    const OS_CONFIRMED =1;// 已确认
    const OS_CANCELED = 2;// 已取消
    const OS_INVALID = 3; // 无效
    const OS_RETURNED = 4;// 已退款
    const OS_SPLITED = 5;// 已分单
    const OS_SPLITING_PART = 6;// 部分分单
    const OS_RETURNING = 7;// 正在退款
    
    /**
     * @var配送状态
     */
    const SS_UNSHIPPED = 0;//未发货
    const SS_SHIPPED = 1;//已发货
    const SS_RECEIVED = 2;//已收货
    const SS_PREPARING = 3;//备货中
    const SS_SHIPPED_PART = 4;//已发货(部分商品)
    const SS_SHIPPED_ING = 5;//发货中(处理分单)
    const OS_SHIPPED_PART = 6;//已发货(部分商品)
    
    /**
     * @var  支付状态 
     */
	const PS_UNPAYED = 0;//未付款
	const PS_PAYING = 1;//付款中
	const PS_PAYED = 2;//已付款
	
	/**
	 * @var 地址类别分类
	 */
	const RE_COUNTRY = 0;//国家
	const RE_PROVINCE = 1;//省份
	const RE_CITY = 2;//城市
	const RE_DISTINCT = 3;//地区
	
	/**
	 * @var 供应商默认地址
	 * 
	 */
	const DEFAULT_COUNTRY = 1;//默认国家：中国
	const DEFAULT_PROVINCE = 6;//默认省份：广东
	const DEFAULT_CITY = 76;//默认城市：广州
	const DEFAULT_DISTINCT = 693;//默认区、县 ：天河区
	
	
	/*
	 * @var 公司简称
	 */
	const SN_PREFIX = 'YLV';
	
	
	/**
	 * @var 增加库存
	 */
    const PLUS = "增加库存";
    		
    /**
     * @var 减少库存
     */
    const MINUS ="减少库存";
    
    /**
     * @var 已生效
     */
    const ACTIVE =1;
    
    /**
     * @var 未生效
     */
    const NOT_ACTIVE =0;
    
    /**
     * @var 分组前缀
     */
    const PRE_GROUP = 'group_';

    /**
     * @var 分组类型
     */
    const ROLE_TYPE = 1;
    
    /**
     * @var 权限类型
     */
    const PERMISSION_TYPE = 2;
    
    /**
     * @var 供应商
     */
    const SUPPLIER = 0;
    
    /**
     * @var 客户
     */
    const CUSTOMER = 1;
    
    /**
     * @var 盘点生成盈亏单备注
     */
    const INVENTORY_REMARK = '盘点生成，盘点单号：';
    
    
    /**
     * @var 超出库存
     */
    const OVERQUANTITY ='超出了库存，不能出库';
    
    
    
    /**
     * @var 未结销
     */
    const NO_SETTELE = 0;
    
    /**
     * @var 已结销
     */
    const SETTLE = 2;
    
    /**
     * @var 部分结销
     */
    const PART_SETTLE =1;
    
    public static $settle = [
    	self::NO_SETTELE => '未结销',
    	self::SETTLE => '已结销',
    	self::PART_SETTLE=> '部分结销',	
    ];
    
    /**
     * @var 检查是否显示成本价
     */
    const CHECK_PRODUCT_PRICE ='product/check-product-price';
    
    /**
     * @desc 采购员权限
     */
    const BUY_ROLE = 'group_采购员';
    
    /**
     * @desc 市场
     */
    const SALE_ROLE = 'group_销售';
    
    /**
     * @desc 供应
     */
    const SUP_ROLE='group_供应';
    
    /**
     * @var 默认仓库
     */
    const DEFAULT_WARE = 15;//线上改为15
    
    /**
     * @var 报废仓
     */
    const SCRAPPED = 1;
    
    /**
     * @var 分拆基础商品
     */
    const BASE_GOODS = 0;
    
    //报损原因
    /**
     * @var 来料破损
     */
    const income_damage = 1;
    
    /**
     * @var 退货
     */
    const return_damage = 2;
    
    /**
     * @var 送货中
     */
    const shipping_damage =3;
    
    /**
     * @var 线下活动
     */
    const offline_damage = 4;
    
    /**
     * @var 配货中
     */
    const delivery_damage = 5;
    
    /**
     * @var 其他
     */
    const other_damage = 6;
    
    //报损原因
    public static $damage_type = [
    	self::income_damage =>'来料破损',
    	self::return_damage =>'退货',
    	self::shipping_damage => '送货中',
    	self::offline_damage => '线下活动',
    	self::delivery_damage => '配货中',
    	self::other_damage => '其他',
    ];
    
    /**
     * @var 基本单位
     */
	const DEFAULT_UNIT= 0;    
	
	public static  $accounting_method = [
			1 => '移动加权平均法',
			2 => '先进先出法'
	];
	
	/**
	 * @var 最大同步时间 提前一年半
	 */
	const INVENTORY_MAX_SYNC_DATE = 47347200;
	
	/**
	 * @var 重叠两分钟
	 */
	const OVARLAP_TIME = 60;
	
	/**
	 * @var 销售单默认客户商城发货
	 */
	const DEFAULT_CUSTOMER = '68';
}