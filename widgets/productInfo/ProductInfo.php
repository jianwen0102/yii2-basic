<?php
namespace  app\widgets\productInfo;

use yii\base\Widget;
class ProductInfo extends Widget
{
	public $msg = '';
	/**
	 * 初始化
	 * @see \yii\base\Object::init()
	 */
	public function init(){
		parent::init();
	}


	public function run(){
		return $this->render('index');
	}
}