<?php
/**
 * @desc 列表显示 商品列表
 */
?>
<div id="tableContent" style="display: none;">
	<table class="table">
		<thead>
			<tr>
				<th>#</th>
				<th>商品编号</th>
				<th>商品名称</th>
				<th>商品类目</th>
				<th>单位</th>
				<th>数量</th>
				<th>单价</th>
				<th>金额</th>
			</tr>
		</thead>
		<tbody id="webuiList">
			<tr>
				<td colspan="8" class="text-center">没有找到数据，请联系程序员</td>
			</tr>
		</tbody>
	</table>

</div>
