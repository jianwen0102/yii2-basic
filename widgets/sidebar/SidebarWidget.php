<?php
namespace app\widgets\sidebar;

/**
 * 后台siderbar插件
 */
use Yii;
use yii\base\Widget;
use yii\widgets\Menu;
use yii\helpers\Html;

class SidebarWidget extends Menu
{    
    public $submenuTemplate = "\n<ul class=\"children\">\n{items}\n</ul>\n";
    
    public $options = ['class'=>'nav nav-pills nav-stacked nav-quirk'];
    
    public $activateParents = true;
    
    public function init()
    {
        $this->items = [
//             ['label' =>'<i class="fa fa-dashboard"></i><span>仪表盘</span>','url'=>['site/index']],
            ['label' =>'<a href=""><i class="fa fa-th-list"></i><span>基础资料管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
            		['label'=>'类目管理','url'=>['category/list-category'],'items'=>[
            			]
            		],
                    ['label'=>'商品管理','url'=>['product/list-product'],'items'=>[
                        ['label'=>'创建商品','url'=>['product/add-product'],'visible'=>false],
                        ['label'=>'编辑商品','url'=>['product/edit-product'],'visible'=>false],
                        ]                       
                    ],
            		['label'=>'部门管理','url'=>['department/list-depart'],'items'=>[
//             			['label'=>'添加部门','url'=>['department/create-depart'],'visible'=>false],
//             			['label'=>'编辑部门','url'=>['department/edit-depart'],'visible'=>false],
            			]
            		],
            		['label'=>'员工管理','url'=>['employee/list-employee'],'items'=>[
//             			['label'=>'添加员工','url'=>['employee/create-supplier'],'visible'=>false],
//             			['label'=>'编辑员工','url'=>['employee/edit-supplier'],'visible'=>false],
            			]
            		],
            		['label'=>'供应商管理','url'=>['supplier/list-supplier'],'items'=>[
            				['label'=>'添加供应商','url'=>['supplier/create-supplier'],'visible'=>false],
            				['label'=>'编辑供应商','url'=>['supplier/supplier-edit'],'visible'=>false],
            			]
            		],
            		['label'=>'客户管理','url'=>['client/list-client'],'items'=>[
    						['label'=>'添加客户','url'=>['client/add-client'],'visible'=>false],
            				['label'=>'编辑客户','url'=>['client/client-edit'],'visible'=>false],
	            		]
            		],
            		['label'=>'仓库信息','url'=>['warehouse/list-warehouse'],'items'=>[
            				['label'=>'创建仓库','url'=>['warehouse/update'],'visible'=>false],
            				['label'=>'仓库详情','url'=>['warehouse/veiw'],'visible'=>false],
            			]
            		],
            		['label'=>'单位信息','url'=>['warehouse/list-unit'],'items'=>[
            			]
            		],
            		['label'=>'商品更新日志','url'=>['product-log/list-log'],'items'=>[
            			]
            		],
//                     ['label'=>'标签管理','url'=>['tag/index']],
                ]
            ],
        	['label' =>'<a href=""><i class="fa fa-send-o"></i><span>销售管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
        			['label'=>'新建报价单','url'=>['sale/add-quotes'],'items'=>[
        			
        				],
        			],
        			['label'=>'报价单列表','url'=>['sale/list-quotes'],'items'=>[
        					['label'=>'报价单编辑','url'=>['sale/edit-quotes'],'visible'=>false]
        				],
        			],
        			['label'=>'新建销售单','url'=>['sale/add-sells'],'items'=>[
        				]
        			],
        			['label'=>'销售单列表','url'=>['sale/list-sells'],'items'=>[
        						['label'=>'销售单编辑','url'=>['sale/edit-sells'],'visible'=>false],
        				]
        			],
        			['label'=>'销售出库','url'=>['sale/add-sale-out'],'items'=>[
        				]
        			],
        			['label'=>'销售出库列表','url'=>['sale/list-sale-out'],'items'=>[
        						['label'=>'销售出库编辑','url'=>['sale/edit-sale-out'],'visible'=>false],
        				]
        			],
        			['label'=>'新增销售退货','url'=>['sale/add-sale-return'],'items'=>[
	        			]
        			],
        			['label'=>'销售退货列表','url'=>['sale/list-sale-return'],'items'=>[
        					['label'=>'销售退货编辑','url'=>['sale/edit-sale-return'],'visible'=>false],
        				]
        			],
        			
//         			['label'=>'销售统计','url'=>['sale/sale-out-cust'],'items'=>[
//         				]
//         			],
        			['label'=>'销售统计','url'=>['sale/sale-out-free'],'items'=>[
        				]
        			],
        		]
        	],
            ['label' =>'<a href=""><i class="fa fa-folder"></i><span>采购管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
            		
                    ['label'=>'新建采购单','url'=>['procurement/add-procurement'],'items'=>[
                        ]                       
                    ],
            		['label'=>'采购单列表','url'=>['procurement/list-procurement'],'items'=>[
            				['label'=>'采购单编辑','url'=>['procurement/edit-procurement'],'visible'=>false],
            			]
            		],
            		['label'=>'采购入库','url'=>['purchase/add-purchase'],'items'=>[
            			]
            		],
            		['label'=>'采购入库列表','url'=>['purchase/list-purchase'],'items'=>[
            				['label'=>'采购单编辑','url'=>['purchase/edit-purchase'],'visible'=>false],
            			]
            		],
            		['label'=>'新增采购退货','url'=>['purchase-return/add-purchase-return'],'items'=>[
            			]
            		],
            		['label'=>'采购退货列表','url'=>['purchase-return/list-purchase-return'],'items'=>[
            				['label'=>'采购退货编辑','url'=>['purchase-return/edit-purchase-return'],'visible'=>false],
            			]
            		],
            		['label'=>'采购统计','url'=>['purchase/purchase-vendor'],'items'=>[
//             				['label'=>'采购单编辑','url'=>['purchase/edit-purchase'],'visible'=>false],
            			]
            		],
                ]
            ],
        	['label' =>'<a href=""><i class="fa fa-rmb"></i><span>往来管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
        			['label'=>'付款结算','url'=>['funds/add-settle-payables'],'items'=>[
        					
        				]
        			],
        			['label'=>'付款结算列表','url'=>['funds/list-settle-payables'],'items'=>[
        					['label'=>'采购单编辑','url'=>['funds/edit-settle-payables'],'visible'=>false],
        				]
        			],
        			['label'=>'应付款管理','url'=>['funds/list-payables'],'items'=>[
        					['label'=>'采购单编辑','url'=>['funds/edit-payables'],'visible'=>false],
        				]
        			],
        			['label'=>'新增请款单','url'=>['funds/add-vendor-payment'],'items'=>[
//         			    	['label'=>'新增货款申请单','url'=>['funds/edi t-vendor-payment'],'visible'=>false],
        			    ]
        			], 
        			['label'=>'请款单列表','url'=>['funds/list-vendor-payment'],'items'=>[
        					['label'=>'请款单列表','url'=>['funds/edit-vendor-payment'],'visible'=>false],
        				]
        			],
//         			['label'=>'新增付款单','url'=>['funds/list-payables'],'items'=>[
//         					['label'=>'采购单编辑','url'=>['funds/edit-payables'],'visible'=>false],
//         			]
//         			],
//         			['label'=>'收款结算','url'=>['funds/add-receivables'],'items'=>[
//         				]
//         			],
//         			['label'=>'应收款管理','url'=>['funds/list-receivables'],'items'=>[
//         					['label'=>'采购单编辑','url'=>['funds/edit-receivables'],'visible'=>false],
//         				]
//         			],
        		]
        	],
        	['label' =>'<a href=""><i class="fa fa-train"></i><span>仓库管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
        			['label'=>'其他入库单','url'=>['instore/instore'],'items'=>[
        				]
        			],
        			['label'=>'其他出库单','url'=>['outstore/outstore'],'items'=>[
        				]
        			],
        			['label'=>'入库单列表','url'=>['instore/list-instore'],'items'=>[
        					['label'=>'入库单详情','url'=>['instore/instore-detail'],'visible'=>false],
        				]
        			],
        			['label'=>'出库单列表','url'=>['outstore/list-outstore'],'items'=>[
        					['label'=>'出库单详情','url'=>['outstore/outstore-detail'],'visible'=>false],
        				]
        			],
        			['label'=>'调拨单','url'=>['transfer/add-transfer'],'items'=>[
        					
        				]
        			],
        			['label'=>'调拨单列表','url'=>['transfer/list-transfer'],'items'=>[
        					['label'=>'编辑调拨单','url'=>['transfer/edit-transfer'],'visible'=>false], 
        				]
        			],
        			['label'=>'盘点单','url'=>['inventory/add-inventory'], 'items'=>[
        					['label'=>'','url'=>[],'visible'=>false],
        				]
        			] ,
        			['label'=>'盘点单列表','url'=>['inventory/list-inventory'], 'items'=>[
        					['label'=>'编辑盘点单','url'=>['inventory/edit-inventory'],'visible'=>false],
        				]
        			] ,
        			['label'=>'盘盈单','url'=>['inventory/add-inventory-full'], 'items'=>[
        					['label'=>'','url'=>[],'visible'=>false],
        				]
        			] ,
        			['label'=>'盘盈单列表','url'=>['inventory/list-inventory-full'], 'items'=>[
        					['label'=>'编辑盘盈单','url'=>['inventory/edit-inventory-full'],'visible'=>false],
        				]
        			] ,
        			['label'=>'盘亏单','url'=>['inventory/add-inventory-lost'], 'items'=>[
        					['label'=>'','url'=>[],'visible'=>false],
        				]
        			] ,
        			['label'=>'盘亏单列表','url'=>['inventory/list-inventory-lost'], 'items'=>[
        					['label'=>'编辑盘亏单','url'=>['inventory/edit-inventory-lost'],'visible'=>false],
        				]
        			] ,
        			
//         			['label'=>'成本调整单','url'=>['price/add-adjustment'],'items'=>[
//         				]
//         			],
//         			['label'=>'成本调整单列表','url'=>['price/list-adjustment'],'items'=>[
//         					['label'=>'编辑盘亏单列表','url'=>['price/edit-adjustment'],'visible'=>false],
//         				]
//         			],
//         			['label'=>'成本调整查询','url'=>['price/list-adjustment-info'],'items'=>[
//         				]
//         			],
        			['label'=>'拆装单','url'=>['disassembly/add-disassembly'],'items'=>[
        			]
        			],
        			['label'=>'拆装单列表','url'=>['disassembly/list-disassembly'],'items'=>[
        					['label'=>'编辑拆装单','url'=>['disassembly/edit-disassembly'],'visible'=>false],
        			]
        			],
        		]
        	],
        	['label' =>'<a href=""><i class="fa fa-bar-chart "></i><span>库存管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
        			['label'=>'库存信息','url'=>['product/list-stock-pile'],'items'=>[
        				]
        			],
        			['label'=>'出入库汇总统计','url'=>['transaction/list-stock-sum'],'items'=>[
        				]
        			],
        			['label'=>'出入库明细查询','url'=>['transaction/list-transaction'],'items'=>[
        				]
        			],
        			['label'=>'分仓库存查询','url'=>['stock-pile/list-stock-pile'],'items'=>[
        				]
        			],
        			
        		]
           ],                                                      
        	['label' =>'<a href=""><i class="fa fa-line-chart "></i><span>统计分析</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
        		['label'=>'调拨汇总','url'=>['transfer/transfer-statistics'],'items'=>[
        			]
        		],
        		['label'=>'其他入库单汇总','url'=>['instore/instore-summary'],'items'=>[
        			]
        		],
        		['label'=>'其他出库单汇总','url'=>['outstore/outstore-summary'],'items'=>[
        			]
        		],
        		['label'=>'报盈单汇总','url'=>['inventory/inventory-full-summary'],'items'=>[
        			]
        		],
        		['label'=>'报损单汇总','url'=>['inventory/inventory-lost-summary'],'items'=>[
        			]
        		],
        		['label'=>'商城毛利汇总表','url'=>['reports/sale-gross-profit-summary'],'items'=>[
        			]
        		],
        		['label'=>'特定商品毛利汇总表','url'=>['reports/special-gross-profit-summary'],'items'=>[
        			]
        		],
//         		['label'=>'门店毛利汇总表','url'=>['reports/sale-gross-profit-summary'],'items'=>[
//         			]
//         		],
//         		['label'=>'公司毛利汇总表','url'=>['reports/sale-gross-profit-summary'],'items'=>[
//         			]
//         		],
        		]
        	],
        
//            ['label' =>'<a href=""><i class="fa fa-send-o"></i><span>连锁管理</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
//         			['label'=>'门店列表','url'=>['store-store/list-store-store'],'items'=>[
//         				]
//         			],
// 					['label'=>'门店管理员','url'=>['store-admin/list-store-admin'],'items'=>[
// 							['label'=>'门店管理员编辑','url'=>['store-admin/store-admin-edit'],'visible'=>false],
//         				]
//         			],
//         			['label'=>'门店店员','url'=>['store-employee/list-store-employee'],'items'=>[
//         				]
//         			],					
//         			['label'=>'门店POS机','url'=>['store-pos/list-store-pos'],'items'=>[
//         				]
//         			],
//                     ['label'=>'门店会员','url'=>['store-member/list-member'],'items'=>[
//         				]
//         			],
//         			['label'=>'门店分析','url'=>['store-stats/stats-store'],'items'=>[
// 							['label'=>'销售总额','url'=>['store-stats/stats-store'],'visible'=>false],
// 							['label'=>'订单数','url'=>['store-stats/stats-order-num'],'visible'=>false],
// 							['label'=>'商品销售额','url'=>['store-stats/stats-goods'],'visible'=>false],
// 							['label'=>'类目销售额','url'=>['store-stats/stats-category'],'visible'=>false],
// 							['label'=>'会员消费','url'=>['store-stats/stats-member-consumption'],'visible'=>false],
// 							['label'=>'会员充值','url'=>['store-stats/stats-member-recharge'],'visible'=>false],
// 							['label'=>'出货毛利','url'=>['store-stats/stats-shipment-gross-profit'],'visible'=>false],
// 							// ['label'=>'配送毛利','url'=>['store-stats/stats-delivery-gross-profit'],'visible'=>false],
// 							['label'=>'损耗','url'=>['store-stats/stats-loss'],'visible'=>false],
//         				]
//         			],
//         		]
//         	],
           ['label' =>'<a href=""><i class="fa fa-sun-o "></i><span>系统设置</span></a>','options'=>['class'=>'nav-parent'],'items'=>[
	           		['label'=>'个人信息','url'=>['admin/personal-info'],'items'=>[
	           			]
	           		],
        			['label'=>'管理员列表','url'=>['admin/list-users'],'items'=>[
        					['label'=>'管理员信息','url'=>['admin/user-info'],'visible'=>false],
        				]
        			],
	           		['label'=>'权限分组','url'=>['permission/list-group'],'items'=>[
// 	           				['label'=>'管理员信息','url'=>['admin/user-info'],'visible'=>false],
	           			]
	           		],
        		]
           ],
        ];
    }

        /**
     * Normalizes the [[items]] property to remove invisible items and activate certain items.
     * @param array $items the items to be normalized.
     * @param boolean $active whether there is an active child menu item.
     * @return array the normalized menu items
     */
    protected function normalizeItems($items, &$active)
    {
        foreach ($items as $i => $item) {
            if (!isset($item['label'])) {
                $item['label'] = '';
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $items[$i]['label'] = $encodeLabel ? Html::encode($item['label']) : $item['label'];
            $hasActiveChild = false;
            if (isset($item['items'])) {
                $items[$i]['items'] = $this->normalizeItems($item['items'], $hasActiveChild);
                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    //检查是否有权限--start
                    $auth = Yii::$app->authManager;
                    if(isset($item['url']) &&$auth->getPermission($item['url'])){//只判断在auth_item 的权限
                    	$res = $auth->checkAccess(Yii::$app->user->id, $item['url'][0]);
                    	if(!$res){
                    		unset($items[$i]);
                    		continue;
                    	}
                    }
                    //--end
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
            if (!isset($item['active'])) {
                if ($this->activateParents && $hasActiveChild || $this->activateItems && $this->isItemActive($item)) {
                    $active = $items[$i]['active'] = true;
                } else {
                    $items[$i]['active'] = false;
                }
            } elseif ($item['active']) {
                $active = true;
            }
             
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            }
        }
    
        return array_values($items);
    }
    
//     public function run()
//     {
//     	if ($this->route === null && Yii::$app->controller !== null) {
//     		$this->route = Yii::$app->controller->getRoute();
//     	}
//     	if ($this->params === null) {
//     		$this->params = Yii::$app->request->getQueryParams();
//     	}
//     	$items = $this->normalizeItems($this->items, $hasActiveChild);
//     	dd($items[0]['items']);//@todo 这个有用
//     	if (!empty($items)) {
//     		$options = $this->options;
//     		$tag = ArrayHelper::remove($options, 'tag', 'ul');
    
//     		echo Html::tag($tag, $this->renderItems($items), $options);
//     	}
//     }
}