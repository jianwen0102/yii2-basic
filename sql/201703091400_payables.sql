SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `payables` (
`pay_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`pay_time`  int(11) NOT NULL DEFAULT 0 COMMENT '应付时间' ,
`pay_type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '应付款单单据类型' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商id' ,
`pay_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记 ' ,
`origin_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '原单编号' ,
`origin_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'y原单id' ,
`origin_type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '原单类型' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '结销状态 0： 未结销  1: 半结销 2：已结销' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`pay_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '应付款' ,
`settle_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '结算金额' ,
`receive_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '已付款' ,
`left_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '余额' ,
`description`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '摘要' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
PRIMARY KEY (`pay_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;

CREATE TABLE `settle_payables` (
`settle_pay_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`settle_pay_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应付结算单号' ,
`settle_pay_type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '应付结算单据类型' ,
`settle_pay_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '结算时间' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '往来单位' ,
`settle_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '结算付款金额' ,
`discount_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '优惠金额' ,
`settle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '结算人员' ,
`create_man`  int(11) NOT NULL COMMENT '制单人' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单据创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认付款' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除 0: 未删除 1 : 已删除 2： 已红冲' ,
PRIMARY KEY (`settle_pay_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `settle_payables_detail` (
`settle_pay_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`settle_pay_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '应付结算单头id' ,
`pay_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '应付单id' ,
`pay_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '应付金额' ,
`settled_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '已结销金额' ,
`left_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '当前剩余金额' ,
`current_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '本次付款金额' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除 0: 未删除 1 : 已删除 2： 已红冲' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
PRIMARY KEY (`settle_pay_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
SET FOREIGN_KEY_CHECKS=1;