SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `procurement_detail` MODIFY COLUMN `received_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '采购订单已收到的数量' AFTER `delete_flag`;
ALTER TABLE `procurement_detail` MODIFY COLUMN `minus_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '未到货数量' AFTER `received_quantity`;
CREATE INDEX `IDx_product_name` ON `product`(`product_name`) USING BTREE ;

ALTER TABLE `purchase_detail` MODIFY COLUMN `minus_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '未到货数量' AFTER `received_quantity`;
ALTER TABLE `saleout` ADD COLUMN `sell_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一层单据id(采购订单id)' AFTER `discount_amount`;
ALTER TABLE `saleout_detail` ADD COLUMN `sell_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售订单明细id' AFTER `second_relation`;
CREATE TABLE `sell` (
`sell_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '销售订单id' ,
`sell_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '销售订单编号' ,
`sell_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单日期' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`customer_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '客户id' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 : 未完成 1：已完成' ,
`sell_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '销售订单金额' ,
`total_discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣金额' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`discount_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '整单优惠金额' ,
PRIMARY KEY (`sell_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `sell_detail` (
`sell_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '销售订单明细id' ,
`sell_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售订单单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`weight`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '重量' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:未删除 1：已删除' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单位' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '折扣金额' ,
`batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' ,
`base_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '基本数量' ,
`base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' ,
`second_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' ,
`second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' ,
`second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' ,
`shipped_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '已发出数量' ,
`minus_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '剩余数量' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 : 未完成 1：已完成' ,
PRIMARY KEY (`sell_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
SET FOREIGN_KEY_CHECKS=1;