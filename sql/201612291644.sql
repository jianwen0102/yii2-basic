SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `instore` ADD COLUMN `instore_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '入库单总金额' AFTER `confirm_flag`;
ALTER TABLE `instore_detail` MODIFY COLUMN `amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `outstore` ADD COLUMN `outstore_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '出库单总金额' AFTER `confirm_flag`;
ALTER TABLE `outstore_detail` MODIFY COLUMN `amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `procurement` MODIFY COLUMN `procurement_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '订单总金额' AFTER `create_man`;
ALTER TABLE `procurement_detail` MODIFY COLUMN `amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `product` ADD COLUMN `genexpectstart_qty`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '期初库存' AFTER `product_img`;
ALTER TABLE `product` ADD COLUMN `price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '成本单价' AFTER `genexpectstart_qty`;
ALTER TABLE `product` ADD COLUMN `amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `product` ADD COLUMN `genexpectstart_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '期初金额' AFTER `amount`;
ALTER TABLE `purchase` MODIFY COLUMN `purchase_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '订单金额' AFTER `remark`;
ALTER TABLE `purchase_detail` MODIFY COLUMN `amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
CREATE TABLE `stock_pile` (
`stock_pile_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '分仓库库存表' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
PRIMARY KEY (`stock_pile_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Fixed
DELAY_KEY_WRITE=0
;
ALTER TABLE `transaction` ADD COLUMN `quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量单位' AFTER `quantity`;
ALTER TABLE `transaction` ADD COLUMN `vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商id' AFTER `weight`;
SET FOREIGN_KEY_CHECKS=1;