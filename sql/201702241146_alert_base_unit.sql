SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `instore_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `batch_num`;
ALTER TABLE `instore_detail` ADD COLUMN `base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `instore_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `instore_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `instore_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `inventory_detail` ADD COLUMN `base_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `delete_flag`;
ALTER TABLE `inventory_detail` ADD COLUMN `base_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `inventory_detail` ADD COLUMN `second_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `inventory_detail` ADD COLUMN `second_unit`  int(11) NOT NULL AFTER `second_quantity`;
ALTER TABLE `inventory_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `inventory_full_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `inventory_det_id`;
ALTER TABLE `inventory_full_detail` ADD COLUMN `base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `inventory_full_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `inventory_full_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `inventory_full_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `inventory_det_id`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `base_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `outstore_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `batch_num`;
ALTER TABLE `outstore_detail` ADD COLUMN `base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `outstore_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `outstore_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `outstore_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `procurement_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `finish_flag`;
ALTER TABLE `procurement_detail` ADD COLUMN `base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `procurement_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `procurement_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `procurement_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `product` ADD COLUMN `warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' AFTER `genexpectstart_amount`;
ALTER TABLE `product` ADD COLUMN `unit_content`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '单位关系' AFTER `warehouse_id`;
CREATE TABLE `product_unit` (
`product_unit_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品单位关系表' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`unit_type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位类别' ,
`unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`rate`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '换算比率' ,
`format`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规格' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`product_unit_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `purchase_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `remark`;
ALTER TABLE `purchase_detail` ADD COLUMN `base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `purchase_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `purchase_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `purchase_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
ALTER TABLE `saleout_detail` ADD COLUMN `base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' AFTER `batch_num`;
ALTER TABLE `saleout_detail` ADD COLUMN `base_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '基本单位' AFTER `base_quantity`;
ALTER TABLE `saleout_detail` ADD COLUMN `second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' AFTER `base_unit`;
ALTER TABLE `saleout_detail` ADD COLUMN `second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' AFTER `second_quantity`;
ALTER TABLE `saleout_detail` ADD COLUMN `second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' AFTER `second_unit`;
CREATE DEFINER = `root`@`%` PROCEDURE `InitProductUnit`()
BEGIN 
DECLARE pre_product ,pre_unit ,no_more_trans INT DEFAULT 0;

DECLARE units CURSOR FOR SELECT product_id,quantity_unit from `product` where delete_flag = 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN units;
FETCH units into pre_product,pre_unit;
REPEAT
	INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));
FETCH units into pre_product,pre_unit;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE units;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `InsertStockPile`(IN pid INT, IN WID INT)
BEGIN 
DECLARE pre_product ,pre_unit ,pre_warehouse,no_more_trans INT DEFAULT 0;

DECLARE pro CURSOR FOR SELECT product_id,quantity_unit,WID from `product` where delete_flag = 0 and product_id = pid;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN pro;
FETCH pro into pre_product,pre_unit,pre_warehouse;
REPEAT
	INSERT INTO stock_pile (warehouse_id,product_id,quantity_unit,create_time) VALUES (pre_warehouse,pre_product,pre_unit,UNIX_TIMESTAMP(NOW()));
FETCH pro into pre_product,pre_unit,pre_warehouse;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE pro;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `instore_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT instore_det_id, unit,quantity from `instore_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE instore_detail set base_unit = pre_unit ,base_quantity =pre_quantity where instore_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_det_id, quantity_unit,quantity from `inventory_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_full_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_full_det_id, quantity_unit,quantity from `inventory_full_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_full_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_full_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_lost_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_lost_det_id, quantity_unit,quantity from `inventory_lost_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_lost_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_lost_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `outstore_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT outstore_det_id, unit,quantity from `outstore_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE outstore_detail set base_unit = pre_unit ,base_quantity =pre_quantity where outstore_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `procurement_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT procurement_det_id, quantity_unit,quantity from `procurement_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE procurement_detail set base_unit = pre_unit ,base_quantity =pre_quantity where procurement_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `purchase_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT purchase_det_id, quantity_unit,quantity from `purchase_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE purchase_detail set base_unit = pre_unit ,base_quantity =pre_quantity where purchase_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `saleout_detail_init`()
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT saleout_det_id, quantity_unit,quantity from `saleout_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE saleout_detail set base_unit = pre_unit ,base_quantity =pre_quantity where saleout_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `test`()
BEGIN 
DECLARE pre_product  ,no_more_trans INT DEFAULT 0;
DECLARE unit_content VARCHAR(100);

DECLARE units CURSOR FOR SELECT product_id from `product` where delete_flag = 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN units;
FETCH units into pre_product;
REPEAT
	-- select pre_product;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));
BEGIN
DECLARE pro_product, pro_rate, no_more_rate INT DEFAULT 0;
DECLARE pro_unit VARCHAR(50);

DECLARE rates CURSOR FOR SELECT product_id,rate,unit_name from `product_unit` p LEFT JOIN unit u on u.unit_id = p.unit where p.delete_flag = 0 and p.product_id = pre_product ORDER BY rate DESC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_rate = 1;
OPEN rates;
FETCH rates into pro_product,pro_rate,pro_unit;
REPEAT
 -- select pro_product,pro_rate,pro_unit;
		
		
select unit_content;
FETCH rates into pro_product,pro_rate,pro_unit;
UNTIL no_more_rate = 1
END REPEAT;
CLOSE rates;
END;
FETCH units into pre_product;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE units;
END;
SET FOREIGN_KEY_CHECKS=1;