/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : mooc

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2016-11-09 17:32:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `admin`
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `auth_key` varchar(32) NOT NULL COMMENT '自动登录key',
  `password_hash` varchar(255) NOT NULL COMMENT '加密密码',
  `password_reset_token` varchar(255) NOT NULL COMMENT '重置密码token',
  `email_validate_token` varchar(255) NOT NULL COMMENT '邮箱验证token',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `role` smallint(6) NOT NULL DEFAULT '10' COMMENT '角色等级',
  `status` smallint(6) NOT NULL DEFAULT '10' COMMENT '状态',
  `avatar` varchar(255) NOT NULL COMMENT '头像',
  `vip_lv` int(11) NOT NULL DEFAULT '0' COMMENT 'vip等级',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员表';

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'VtIxs8fRiNE1ULN_5HiJ07siDOTMXgh-', '$2y$13$rjIZBs3NgZ.JJYMBkBNdzehovxeRqzXFKEQk4PrRuGWC38Y.7.w1e', '', '', '11@qq.com', '10', '10', '', '0', '1478187499', '1478187499');

-- ----------------------------
-- Table structure for `generalledger`
-- ----------------------------
DROP TABLE IF EXISTS `generalledger`;
CREATE TABLE `generalledger` (
  `gen_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '库存总帐表id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `warehouse_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '仓库id',
  `gen_quantity` int(11) NOT NULL DEFAULT '0' COMMENT '现有总库存（数量）',
  `gen_weight` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '现有总库存（重量）',
  `genexpectstartqty` int(11) NOT NULL DEFAULT '0' COMMENT '期初数量',
  `genexpectstartweight` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '起初重量',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`gen_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of generalledger
-- ----------------------------

-- ----------------------------
-- Table structure for `instore`
-- ----------------------------
DROP TABLE IF EXISTS `instore`;
CREATE TABLE `instore` (
  `instore_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '入库单id',
  `instore_no` varchar(45) NOT NULL DEFAULT '' COMMENT '入库单号',
  `instore_date` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入库时间',
  `instore_type` varchar(25) NOT NULL DEFAULT '' COMMENT '入库类型',
  `warehouse_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '仓库id',
  `instore_man` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入库人员',
  `vendor_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '供应商id',
  `create_man` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '制单人',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '制单时间',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上一次修改时间',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，0否，1是',
  `remark` varchar(150) NOT NULL DEFAULT '' COMMENT '备注',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`instore_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of instore
-- ----------------------------

-- ----------------------------
-- Table structure for `instore_detail`
-- ----------------------------
DROP TABLE IF EXISTS `instore_detail`;
CREATE TABLE `instore_detail` (
  `instore_det_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '入库单明细id',
  `instore_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入库单头id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `quantity` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '入库数量',
  `weight` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '入库重量',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，0，否，1是',
  `transaction_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出入库明细id',
  PRIMARY KEY (`instore_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of instore_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `outstore`
-- ----------------------------
DROP TABLE IF EXISTS `outstore`;
CREATE TABLE `outstore` (
  `outstore_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '出库单id',
  `outstore_no` varchar(45) NOT NULL DEFAULT '' COMMENT '出库单号',
  `outstore_date` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出库时间',
  `outstore_type` varchar(25) NOT NULL DEFAULT '' COMMENT '出库类型',
  `warehouse_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '仓库id',
  `outstore_man` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出库人员',
  `vendor_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '供应商id',
  `create_man` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '制单人',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '制单时间',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上一次修改时间',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，0否，1是',
  `remark` varchar(150) NOT NULL DEFAULT '' COMMENT '备注',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`outstore_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of outstore
-- ----------------------------

-- ----------------------------
-- Table structure for `outstore_detail`
-- ----------------------------
DROP TABLE IF EXISTS `outstore_detail`;
CREATE TABLE `outstore_detail` (
  `outstore_det_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '出库单明细id',
  `outstore_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出库单头id',
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `quantity` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出库数量',
  `weight` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '出库重量',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除，0，否，1是',
  `transaction_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出入库明细id',
  PRIMARY KEY (`outstore_det_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of outstore_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `procurement`
-- ----------------------------
DROP TABLE IF EXISTS `procurement`;
CREATE TABLE `procurement` (
  `procurement_id` int(11) NOT NULL AUTO_INCREMENT,
  `procurement_no` varchar(50) NOT NULL DEFAULT '' COMMENT '采购单单号',
  `procurement_man` int(11) NOT NULL DEFAULT '0' COMMENT '采购人',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `modify_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `supplier_id` int(11) NOT NULL DEFAULT '0' COMMENT '供应商id',
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标记；0：未删除，1：删除',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`procurement_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of procurement
-- ----------------------------

-- ----------------------------
-- Table structure for `procurement_goods_relation`
-- ----------------------------
DROP TABLE IF EXISTS `procurement_goods_relation`;
CREATE TABLE `procurement_goods_relation` (
  `relation_id` int(11) NOT NULL AUTO_INCREMENT,
  `procurement_id` int(11) NOT NULL DEFAULT '0' COMMENT '采购单id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '产品id',
  `quantity` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `weight` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '重量',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `modify_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`relation_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of procurement_goods_relation
-- ----------------------------

-- ----------------------------
-- Table structure for `supplier`
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `supplier_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) NOT NULL,
  `supplier_desc` mediumtext NOT NULL,
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `country` smallint(5) unsigned NOT NULL,
  `province` smallint(5) unsigned NOT NULL,
  `city` smallint(5) unsigned NOT NULL,
  `district` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('24', '本来果坊', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('14', '茂生园', '', '1', '1', '6', '95', '844');
INSERT INTO `supplier` VALUES ('23', '黑龙江龙凤山水农业发展有限公司', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('8', '广州绿垠农业科技发展有限公司', '', '1', '1', '6', '76', '702');
INSERT INTO `supplier` VALUES ('9', '鹏鹄菌业', '', '1', '1', '6', '80', '750');
INSERT INTO `supplier` VALUES ('6', '广州市三道食品有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('21', '阳江三洲海霞绿色农业', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('15', '健之源广州专卖店', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('16', '广州酩门旺竹贸易有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('26', '广东省食品进出口集团公司', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('13', '广州市豪芮家贸易有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('17', '广东祺盛农业科技有限公司', '', '1', '1', '6', '91', '0');
INSERT INTO `supplier` VALUES ('10', '广州华农大食品科技有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('12', '惠兴米业', '', '1', '1', '6', '82', '762');
INSERT INTO `supplier` VALUES ('27', '套餐系列', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('4', '封开县智诚家禽育种有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('34', '广州市科誉有机农产品科技有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('20', '广东三洲海霞绿色农业有限公司', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('18', '广东天勤蚕业有限责任公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('25', '深圳菌香园农业发展有限公司', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('5', '深圳市六泉商贸有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('11', '客家佬豆腐花', '', '1', '1', '6', '82', '760');
INSERT INTO `supplier` VALUES ('28', '雅雅', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('30', '优之名食品有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('31', '绿草猪', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('22', '佛山顺德香云纱文化遗产保护基地', '', '1', '1', '6', '76', '0');
INSERT INTO `supplier` VALUES ('3', '东洞农场', '', '1', '1', '6', '76', '702');
INSERT INTO `supplier` VALUES ('7', '亿鲜汇社区水果', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('33', '牛运亨通', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('29', '广州市益普食品有限公司', '', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('32', '广州福音琴行', '广州福音琴行成立于2007年，是一家集各类乐器培训和中西乐器销售的专业化连锁式音乐港湾。旨在为广大音乐爱好者提供一个了解音乐学习音乐的专业平台。因其雄厚的师资和丰硕的教学成果在业内深受好评，目前已有多家连锁式分店。', '1', '1', '6', '76', '693');
INSERT INTO `supplier` VALUES ('19', '十六区酒业', '', '1', '1', '6', '76', '0');

-- ----------------------------
-- Table structure for `transaction`
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `transaction_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '产品id',
  `quantity` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数量',
  `weight` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '重量',
  `warehouse_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '仓库id',
  `origin_type` varchar(25) NOT NULL DEFAULT '' COMMENT '来源单据类型',
  `origin_id` varchar(45) NOT NULL DEFAULT '' COMMENT '来源单号',
  `origin_line_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '原单序号',
  `origin_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '出入库时间',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0： 出，1：入',
  PRIMARY KEY (`transaction_id`),
  KEY `IDX_goods_id` (`goods_id`) USING BTREE,
  KEY `IDX_warehouse_id` (`warehouse_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of transaction
-- ----------------------------

-- ----------------------------
-- Table structure for `unit`
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(25) NOT NULL DEFAULT '' COMMENT '单位名称',
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `is_active` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否生效',
  `description` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES ('1', '个', '0', '1', '哦二而扼腕认为而我', '1478513303');
INSERT INTO `unit` VALUES ('2', '箱', '1', '1', '3444', '1478514413');
INSERT INTO `unit` VALUES ('3', '箱', '0', '1', '3', '1478673022');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `auth_key` varchar(32) NOT NULL COMMENT '自动登录key',
  `password_hash` varchar(255) NOT NULL COMMENT '加密密码',
  `password_reset_token` varchar(255) NOT NULL COMMENT '重置密码token',
  `email_validate_token` varchar(255) NOT NULL COMMENT '邮箱验证token',
  `email` varchar(255) NOT NULL COMMENT '邮箱',
  `role` smallint(6) NOT NULL DEFAULT '10' COMMENT '角色等级',
  `status` smallint(6) NOT NULL DEFAULT '10' COMMENT '状态',
  `avatar` varchar(255) NOT NULL COMMENT '头像',
  `vip_lv` int(11) NOT NULL DEFAULT '0' COMMENT 'vip等级',
  `created_at` int(11) NOT NULL COMMENT '创建时间',
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'test', 'VtIxs8fRiNE1ULN_5HiJ07siDOTMXgh-', '$2y$13$rjIZBs3NgZ.JJYMBkBNdzehovxeRqzXFKEQk4PrRuGWC38Y.7.w1e', '', '', '11@qq.com', '10', '0', '', '0', '1478187499', '1478623025');
INSERT INTO `user` VALUES ('2', 'test_01', 'F_yHc7bvnVK-2yG0jJBRwXjjf8lSsED1', '$2y$13$IYUR/hMe.8V.sagshJO3UedDm1z5tvqOzHaPyI47vNcZTnSgoxyQm', '', '', '12@qq.com', '10', '10', '', '0', '1478193205', '1478193205');
INSERT INTO `user` VALUES ('3', 'admin', 'OL_iXdK10oXAb89Sm-5FWUqIsT7cdLVK', '$2y$13$J29K2O87r.2Sloi.Dngux.iuT25y.Airx67ahgte9WdnNg8MZBXFG', '', '', '111@qq.com', '10', '10', '', '0', '1478615176', '1478615176');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `last_login` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'test', 'e10adc3949ba59abbe56e057f20f883e', '0', '0');

-- ----------------------------
-- Table structure for `warehouse`
-- ----------------------------
DROP TABLE IF EXISTS `warehouse`;
CREATE TABLE `warehouse` (
  `warehouse_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `warehouse_name` varchar(25) NOT NULL DEFAULT '' COMMENT '仓库',
  `is_default` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否默认；0：不是，1：是',
  `delete_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否删除',
  `remark` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `modify_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of warehouse
-- ----------------------------
INSERT INTO `warehouse` VALUES ('1', '15553', '0', '0', '23', '1478249968', '0', '0');
INSERT INTO `warehouse` VALUES ('2', '123', '0', '1', '11', '0', '1478244658', '0');
INSERT INTO `warehouse` VALUES ('3', '1234', '0', '0', '3', '0', '1478251767', '0');
INSERT INTO `warehouse` VALUES ('4', '2', '0', '0', '233', '1478671360', '1478599336', '0');
INSERT INTO `warehouse` VALUES ('5', '2333', '0', '0', '2', '0', '1478671369', '0');
