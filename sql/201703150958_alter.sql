CREATE INDEX `IDx_product_id` ON `product`(`product_id`) USING BTREE ;
DROP PROCEDURE IF EXISTS `InitProductUnit`;
CREATE DEFINER = `root`@`%` PROCEDURE `InitProductUnit`()
    COMMENT '初始化多单位'
BEGIN 
DECLARE pre_product ,pre_unit ,no_more_trans INT DEFAULT 0;

DECLARE units CURSOR FOR SELECT product_id,quantity_unit from `product` where delete_flag = 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN units;
FETCH units into pre_product,pre_unit;
REPEAT
	INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));
FETCH units into pre_product,pre_unit;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE units;
END;
DROP PROCEDURE IF EXISTS `InsertStockPile`;
CREATE DEFINER = `root`@`%` PROCEDURE `InsertStockPile`(IN pid INT, IN WID INT)
    COMMENT '初始分仓'
BEGIN 
DECLARE pre_product ,pre_unit ,pre_warehouse,no_more_trans INT DEFAULT 0;

DECLARE pro CURSOR FOR SELECT product_id,quantity_unit,WID from `product` where delete_flag = 0 and product_id = pid;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN pro;
FETCH pro into pre_product,pre_unit,pre_warehouse;
REPEAT
	INSERT INTO stock_pile (warehouse_id,product_id,quantity_unit,create_time) VALUES (pre_warehouse,pre_product,pre_unit,UNIX_TIMESTAMP(NOW()));
FETCH pro into pre_product,pre_unit,pre_warehouse;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE pro;
END;
DROP PROCEDURE IF EXISTS `instore_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `instore_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT instore_det_id, unit,quantity from `instore_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE instore_detail set base_unit = pre_unit ,base_quantity =pre_quantity where instore_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `inventory_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_det_id, quantity_unit,quantity from `inventory_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `inventory_full_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_full_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_full_det_id, quantity_unit,quantity from `inventory_full_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_full_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_full_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `inventory_lost_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `inventory_lost_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT inventory_lost_det_id, quantity_unit,quantity from `inventory_lost_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE inventory_lost_detail set base_unit = pre_unit ,base_quantity =pre_quantity where inventory_lost_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `outstore_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `outstore_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT outstore_det_id, unit,quantity from `outstore_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE outstore_detail set base_unit = pre_unit ,base_quantity =pre_quantity where outstore_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
CREATE DEFINER = `root`@`%` PROCEDURE `payables_init`()
BEGIN 
DECLARE pre_id ,pre_type,pre_date,pre_vendor,pre_man,pre_createman, cont,no_more_trans INT DEFAULT 0;
DECLARE pre_amount DECIMAL DEFAULT 0.00;
DECLARE pre_no,pre_remark VARCHAR(255);

DECLARE purchases CURSOR FOR SELECT h.purchase_id,h.purchase_no,h.purchase_type,h.purchase_date,h.vendor_id,h.purchase_man,h.purchase_amount,h.remark,h.create_man from purchase h  where delete_flag = 0;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN purchases;
FETCH purchases into pre_id,pre_no,pre_type,pre_date,pre_vendor,pre_man,pre_amount,pre_remark,pre_createman;
REPEAT
  SELECT count(pay_id) INTO cont  from payables where delete_flag = 0 and origin_id = pre_id;

	-- INSERT INTO payables(origin_id,origin_no,origin_type,pay_time,pay_type,vendor_id,pay_man,pay_amount,settle_amount,left_amount,description,remark,create_man)	VALUES   (pre_id,pre_no,pre_type,pre_date,10,pre_vendor,pre_man,pre_amount,pre_amount,pre_amount,'应付账单，采购入库单',pre_remark,pre_createman);
		select cont;
	IF cont = 0 THEN
		INSERT INTO payables(origin_id,origin_no,origin_type,pay_time,pay_type,vendor_id,pay_man,pay_amount,settle_amount,left_amount,description,remark,create_man)	VALUES   (pre_id,pre_no,pre_type,pre_date,10,pre_vendor,pre_man,pre_amount,pre_amount,pre_amount,'应付账单，采购入库单',pre_remark,pre_createman);
  END IF;
FETCH purchases into pre_id,pre_no,pre_type,pre_date,pre_vendor,pre_man,pre_amount,pre_remark,pre_createman;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE purchases;
END;
DROP PROCEDURE IF EXISTS `procurement_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `procurement_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT procurement_det_id, quantity_unit,quantity from `procurement_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE procurement_detail set base_unit = pre_unit ,base_quantity =pre_quantity where procurement_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `purchase_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `purchase_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT purchase_det_id, quantity_unit,quantity from `purchase_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE purchase_detail set base_unit = pre_unit ,base_quantity =pre_quantity where purchase_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
DROP PROCEDURE IF EXISTS `saleout_detail_init`;
CREATE DEFINER = `root`@`%` PROCEDURE `saleout_detail_init`()
    COMMENT '初始化基本单位'
BEGIN 
DECLARE pre_det_id,pre_unit,no_more_trans INT DEFAULT 0;
DECLARE pre_quantity DECIMAL(11,2);
DECLARE unit_content VARCHAR(100);

DECLARE det CURSOR FOR SELECT saleout_det_id, quantity_unit,quantity from `saleout_detail`;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_trans = 1;
OPEN det;
FETCH det into pre_det_id,pre_unit,pre_quantity;
REPEAT
 select pre_det_id,pre_unit,pre_quantity;
	-- INSERT INTO instore_detail (base_unit,base_quantity) VALUES (pre_unit,pre_quantity) WHERE instore_det_id = pre_det_id;
	UPDATE saleout_detail set base_unit = pre_unit ,base_quantity =pre_quantity where saleout_det_id = pre_det_id;
	-- INSERT INTO product_unit (product_id,rate,unit,create_time) VALUES (pre_product,1,pre_unit,UNIX_TIMESTAMP(NOW()));

FETCH det into pre_det_id,pre_unit,pre_quantity;
UNTIL no_more_trans = 1
END REPEAT;
CLOSE det;
END;
SET FOREIGN_KEY_CHECKS=1;