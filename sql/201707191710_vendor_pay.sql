SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `price_adjustment` (
`adjustment_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`adjustment_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '调整编号' ,
`adjustment_date`  int(11) NOT NULL DEFAULT 0 COMMENT '调整时间' ,
`handle_man`  int(11) NOT NULL DEFAULT 0 COMMENT '调整人员' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`create_man`  int(11) NOT NULL DEFAULT 0 COMMENT '创建人(账户人员）' ,
`remark`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`confirm_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 确认标记（0：未完成 1：已完成）' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`adjustment_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `price_adjustment_detail` (
`adjustment_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`adjustment_id`  int(11) NOT NULL DEFAULT 0 COMMENT '调整单单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '库存数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`adjustment_price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '调后单价' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记 （0：未删除 1：已删除）' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`adjustment_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `procurement` ADD COLUMN `total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '总计金额' AFTER `create_man`;
ALTER TABLE `procurement` ADD COLUMN `freight`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '运费及其他' AFTER `procurement_amount`;
ALTER TABLE `procurement_detail` ADD COLUMN `gross_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '毛重' AFTER `goods_id`;
ALTER TABLE `procurement_detail` ADD COLUMN `gross_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '毛重单位' AFTER `gross_quantity`;
ALTER TABLE `procurement_detail` MODIFY COLUMN `quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量单位' AFTER `quantity`;
CREATE INDEX `IDX_supplier_id` ON `product`(`supplier_id`) USING BTREE ;

CREATE INDEX `IDX_product_id` ON `product_unit`(`product_id`) USING BTREE ;
ALTER TABLE `purchase_detail` ADD COLUMN `pay_finish`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 : 未做请款 1：已作请款' AFTER `finish_flag`;

ALTER TABLE `supplier` ADD COLUMN `bank`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '开户银行' AFTER `create_man`;
ALTER TABLE `supplier` ADD COLUMN `bank_account`  varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '银行账户' AFTER `bank`;
ALTER TABLE `supplier` ADD COLUMN `account_name`  varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收款账户名称' AFTER `bank_account`;
CREATE TABLE `supplier_image` (
`image_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`supplier_id`  int(11) NOT NULL DEFAULT 0 ,
`supplier_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`create_time`  int(11) NOT NULL DEFAULT 0 ,
`modify_time`  int(11) NOT NULL DEFAULT 0 ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`image_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `vendor_payment` (
`vendor_payment_id`  int(11) NOT NULL AUTO_INCREMENT ,
`vendor_payment_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '请款单单号' ,
`payment_date`  int(11) NOT NULL DEFAULT 0 COMMENT '请款时间' ,
`department`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '部门' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商' ,
`account_name`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收款账户名称' ,
`account_bank`  varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '开户银行' ,
`account`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '银行账户' ,
`money_unit`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '钱币单位' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
`finish_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0：未完成 1：已完成' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`handle_man`  int(11) NOT NULL DEFAULT 0 COMMENT '经手人' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`create_man`  int(11) NOT NULL DEFAULT 0 COMMENT '制单人' ,
PRIMARY KEY (`vendor_payment_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `vendor_payment_detail` (
`vendor_payment_det_id`  int(11) NOT NULL AUTO_INCREMENT ,
`vendor_payment_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商货款结算单头id' ,
`purchase_time`  int(11) NOT NULL DEFAULT 0 COMMENT '进货时间' ,
`product_id`  int(11) NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
`purchase_id`  int(11) NOT NULL DEFAULT 0 COMMENT '采购入库' ,
`purchase_det_id`  int(11) NOT NULL DEFAULT 0 COMMENT '采购入库明细id' ,
PRIMARY KEY (`vendor_payment_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;

SET FOREIGN_KEY_CHECKS=1;