SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `instore` MODIFY COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人//admin' AFTER `vendor_id`;
ALTER TABLE `outstore` ADD COLUMN `confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认 ，1：已确认 0：未确认' AFTER `remark`;
ALTER TABLE `outstore` DROP COLUMN `user_id`;
ALTER TABLE `outstore_detail` ADD COLUMN `unit`  int(11) UNSIGNED NULL DEFAULT 0 COMMENT '单位' AFTER `delete_flag`;
SET FOREIGN_KEY_CHECKS=1;