SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `procurement_detail` ADD COLUMN `sell_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售订单id' AFTER `second_relation`;
ALTER TABLE `procurement_detail` ADD COLUMN `sell_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售订单商品明细' AFTER `sell_id`;
ALTER TABLE `product_unit` MODIFY COLUMN `unit_type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位类别 0:基本单位， 1：辅助单位' AFTER `product_id`;
SET FOREIGN_KEY_CHECKS=1;