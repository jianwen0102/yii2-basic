CREATE TABLE `disassembly` (
`disassembly_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`disassembly_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '拆装编号' ,
`disassembly_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`warehouse_out`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调出仓库' ,
`warehouse_in`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调入仓库' ,
`disassembly_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '拆装金额' ,
`disassembly_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '拆装日期' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记 0：未删除 ，1：已删除' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未确认 1：已确认' ,
PRIMARY KEY (`disassembly_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `disassembly_detail` (
`disassembly_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`disassembly_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '拆装单id' ,
`type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 : 拆-out 1：装-in' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '进货价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`base_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '基本数量' ,
`base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' ,
`second_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '辅助数量' ,
`second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' ,
`second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除 0：未删除 1：已删除' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
PRIMARY KEY (`disassembly_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;