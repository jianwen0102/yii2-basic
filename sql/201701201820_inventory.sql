SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `inventory` (
`inventory_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '盘点单号' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' ,
`inventory_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点时间' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '盈亏总额' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 : 未确认 1：确认' ,
PRIMARY KEY (`inventory_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `inventory_detail` (
`inventory_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_id`  int(11) UNSIGNED NOT NULL ,
`product_id`  int(11) NOT NULL ,
`inventory_quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点数量' ,
`quantity_unit`  int(11) NOT NULL ,
`lost_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '盈亏数量' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '成本价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '盈亏金额' ,
`quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存数量' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) NOT NULL ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
PRIMARY KEY (`inventory_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `inventory_full` (
`inventory_full_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_full_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '报溢单号' ,
`inventory_full_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报溢日期' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报溢仓库' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:未删除 1：已删除' ,
`inventory_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点单' ,
PRIMARY KEY (`inventory_full_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `inventory_full_detail` (
`inventory_full_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_full_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘盈单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:未删除 1：已删除' ,
`inventory_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点明细id' ,
PRIMARY KEY (`inventory_full_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `inventory_lost` (
`inventory_lost_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_lost_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '报损单号' ,
`inventory_lost_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报损日期' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报损仓库' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:未删除 1：已删除' ,
`inventory_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点单id' ,
PRIMARY KEY (`inventory_lost_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `inventory_lost_detail` (
`inventory_lost_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`inventory_lost_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘亏单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`craete_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
`inventory_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '盘点点明细id' ,
PRIMARY KEY (`inventory_lost_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `stock_pile` ENGINE=InnoDB,
ROW_FORMAT=Dynamic;
ALTER TABLE `supplier` MODIFY COLUMN `delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记，0：未删除，1：删除' AFTER `district`;
ALTER TABLE `supplier` MODIFY COLUMN `handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '负责人' AFTER `delete_flag`;
ALTER TABLE `supplier` MODIFY COLUMN `create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `handle_man`;
ALTER TABLE `supplier` MODIFY COLUMN `modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更改时间' AFTER `create_time`;
ALTER TABLE `supplier` ADD COLUMN `type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位类型 0 供应商 1：客户' AFTER `modify_time`;
ALTER TABLE `supplier` ADD COLUMN `phone`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系方式' AFTER `type`;
SET FOREIGN_KEY_CHECKS=1;