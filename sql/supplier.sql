/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : mooc

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-01-04 12:05:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `supplier`
-- ----------------------------
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE `supplier` (
  `supplier_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_name` varchar(255) NOT NULL DEFAULT '',
  `supplier_desc` mediumtext NOT NULL,
  `is_check` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `country` smallint(5) unsigned NOT NULL DEFAULT '0',
  `province` smallint(5) unsigned NOT NULL DEFAULT '0',
  `city` smallint(5) unsigned NOT NULL DEFAULT '0',
  `district` smallint(5) unsigned NOT NULL DEFAULT '0',
  `delete_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标记，0：未删除，1：删除',
  `handle_man` int(11) NOT NULL DEFAULT '0' COMMENT '负责人',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `modify_time` int(11) NOT NULL DEFAULT '0' COMMENT '更改时间',
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of supplier
-- ----------------------------
INSERT INTO `supplier` VALUES ('3', '东洞农场', '552', '1', '1', '6', '76', '702', '0', '2', '0', '1479783538');
INSERT INTO `supplier` VALUES ('4', '封开县智诚家禽育种有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('5', '深圳市六泉商贸有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('6', '广州市三道食品有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('7', '亿鲜汇社区水果', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('8', '广州绿垠农业科技发展有限公司', '', '1', '1', '6', '76', '702', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('9', '鹏鹄菌业', '', '1', '1', '6', '80', '750', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('10', '广州华农大食品科技有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('11', '客家佬豆腐花', '32323232323', '1', '1', '6', '82', '760', '0', '2', '0', '1479722093');
INSERT INTO `supplier` VALUES ('12', '惠兴米业', '', '1', '1', '6', '82', '762', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('13', '广州市豪芮家贸易有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('14', '茂生园', '', '1', '1', '6', '95', '844', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('15', '健之源广州专卖店', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('16', '广州酩门旺竹贸易有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('17', '广东祺盛农业科技有限公司', '', '1', '1', '6', '91', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('18', '广东天勤蚕业有限责任公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('19', '十六区酒业', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('20', '广东三洲海霞绿色农业有限公司', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('21', '阳江三洲海霞绿色农业', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('22', '佛山顺德香云纱文化遗产保护基地', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('23', '黑龙江龙凤山水农业发展有限公司', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('24', '本来果坊', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('25', '深圳菌香园农业发展有限公司', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('26', '广东省食品进出口集团公司', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('27', '套餐系列', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('28', '雅雅', '', '1', '1', '6', '76', '0', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('29', '广州市益普食品有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('30', '优之名食品有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('31', '绿草猪', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('32', '广州福音琴行', '广州福音琴行成立于2007年，是一家集各类乐器培训和中西乐器销售的专业化连锁式音乐港湾。旨在为广大音乐爱好者提供一个了解音乐学习音乐的专业平台。因其雄厚的师资和丰硕的教学成果在业内深受好评，目前已有多家连锁式分店。', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('33', '牛运亨通', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('34', '广州市科誉有机农产品科技有限公司', '', '1', '1', '6', '76', '693', '0', '0', '0', '0');
INSERT INTO `supplier` VALUES ('35', '廖建文', 'goods supplier', '1', '1', '6', '95', '845', '0', '2', '0', '1479722219');
INSERT INTO `supplier` VALUES ('36', '2244', '66767', '1', '1', '6', '76', '695', '1', '3', '1479715183', '1479783234');
INSERT INTO `supplier` VALUES ('37', '22', '', '1', '1', '9', '121', '0', '1', '3', '1479715590', '1479783234');
INSERT INTO `supplier` VALUES ('40', 'jw', 'nihap', '1', '1', '6', '76', '693', '1', '3', '1479722709', '1479783234');
INSERT INTO `supplier` VALUES ('41', 'kk', '1121', '1', '1', '6', '83', '764', '1', '3', '1479722737', '1479783234');
INSERT INTO `supplier` VALUES ('42', 'oo', '23232', '1', '1', '6', '96', '850', '1', '2', '1479722865', '1479783234');
INSERT INTO `supplier` VALUES ('43', 'tt', 'erer', '1', '1', '6', '76', '693', '1', '1', '1479722888', '1479783234');
INSERT INTO `supplier` VALUES ('44', 'pp', '212', '1', '1', '6', '76', '693', '1', '2', '1479722914', '1479783234');
INSERT INTO `supplier` VALUES ('45', 'ii', '22323', '1', '1', '6', '76', '693', '1', '2', '1479723123', '1479781765');
INSERT INTO `supplier` VALUES ('46', 'jw', '122', '1', '1', '6', '76', '693', '0', '2', '1479784302', '0');
INSERT INTO `supplier` VALUES ('47', '海瑞', '11', '1', '1', '9', '121', '0', '0', '2', '1480046249', '0');
