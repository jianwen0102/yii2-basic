SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `department` ADD COLUMN `master`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '负责人' AFTER `modify_time`;

ALTER TABLE `product_update_log` ADD COLUMN `origin_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '原单序号' AFTER `origin_id`;
ALTER TABLE `product_update_log` ADD COLUMN `warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' AFTER `remark`;
ALTER TABLE `product_update_log` ADD COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人' AFTER `warehouse_id`;
ALTER TABLE `quotes` ADD COLUMN `customer`  varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '客户' AFTER `receive_date`;
ALTER TABLE `quotes` DROP COLUMN `vendor_id`;
ALTER TABLE `quotes` DROP COLUMN `warehouse_id`;
ALTER TABLE `supplier` MODIFY COLUMN `type`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '类型 0 供应商 1：客户' AFTER `modify_time`;
ALTER TABLE `supplier` ADD COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建人' AFTER `phone`;

SET FOREIGN_KEY_CHECKS=1;