SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `product_update_log` (
`log_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'product 更新log id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`update_num`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新数量' ,
`num_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`initial_num`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上期数量' ,
`origin_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新数量的单据id' ,
`origin_line_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新数量的单据明细id' ,
`origin_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新数量单据的类型' ,
`flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '出入库 1：入库 ；0：出库' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上次更新时间' ,
`update_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`log_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
SET FOREIGN_KEY_CHECKS=1;