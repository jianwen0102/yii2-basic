SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `admin` MODIFY COLUMN `username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名' AFTER `id`;
ALTER TABLE `admin` MODIFY COLUMN `auth_key`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自动登录key' AFTER `username`;
ALTER TABLE `admin` MODIFY COLUMN `password_hash`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '加密密码' AFTER `auth_key`;
ALTER TABLE `admin` MODIFY COLUMN `password_reset_token`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '重置密码token' AFTER `password_hash`;
ALTER TABLE `admin` MODIFY COLUMN `email_validate_token`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱验证token' AFTER `password_reset_token`;
ALTER TABLE `admin` MODIFY COLUMN `email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱' AFTER `email_validate_token`;
ALTER TABLE `admin` MODIFY COLUMN `avatar`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像' AFTER `status`;
ALTER TABLE `admin` MODIFY COLUMN `created_at`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `vip_lv`;
ALTER TABLE `admin` MODIFY COLUMN `updated_at`  int(11) NOT NULL DEFAULT 0 AFTER `created_at`;
ALTER TABLE `admin` ADD COLUMN `delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记 0未删除 1：已删除' AFTER `updated_at`;
ALTER TABLE `admin` ADD COLUMN `last_in`  int(11) NOT NULL DEFAULT 0 COMMENT '上次登录时间' AFTER `delete_flag`;
CREATE TABLE `admin_user` (
`admin_id`  int(11) NOT NULL AUTO_INCREMENT ,
`admin_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员名称' ,
`email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`password`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`last_login`  int(11) NOT NULL DEFAULT 0 COMMENT '上次登录时间' ,
`last_ip`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录ip' ,
PRIMARY KEY (`admin_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;
CREATE TABLE `department` (
`depart_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`depart_name`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称' ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
PRIMARY KEY (`depart_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `employee` (
`employee_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '员工id' ,
`employee_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '员工名称' ,
`phone`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系方式' ,
`depart_id`  int(11) NOT NULL DEFAULT 0 COMMENT '部门id' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0:未删除 1：删除' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`employee_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;
CREATE TABLE `goods` (
`goods_id`  mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品的自增id' ,
`cat_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品所属商品分类id，取值ecs_category的cat_id' ,
`sub_cat_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`goods_sn`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的唯一货号' ,
`goods_name`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的名称' ,
`goods_name_style`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '+' COMMENT '商品名称显示的样式；包括颜色和字体样式；格式如#ff00ff+strong' ,
`click_count`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品点击数' ,
`brand_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '品牌id，取值于ecs_brand 的brand_id' ,
`provider_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '供货人的名称，程序还没实现该功能' ,
`goods_number`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品库存数量' ,
`warn_number`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '商品报警数量' ,
`min_number`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 ,
`expire_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品过期时间' ,
`sales_count`  int(10) UNSIGNED NOT NULL ,
`goods_weight`  decimal(10,3) UNSIGNED NOT NULL DEFAULT 0.000 COMMENT '商品的重量，以千克为单位' ,
`weight_unit`  tinyint(3) UNSIGNED NOT NULL ,
`market_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '市场售价' ,
`shop_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '本店售价' ,
`mobile_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '手机端价格' ,
`promote_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '促销价格' ,
`promote_start_date`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '促销价格开始日期' ,
`promote_end_date`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '促销价结束日期' ,
`keywords`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品关键字，放在商品页的关键字中，为搜索引擎收录用' ,
`goods_alias`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称的简称(用于订单处理和发货)' ,
`goods_brief`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的简短描述' ,
`goods_desc`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '商品的详细描述' ,
`goods_thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品在前台显示的微缩图片，如在分类筛选时显示的小图片' ,
`goods_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的实际大小图片，如进入该商品页时介绍商品属性所显示的大图片' ,
`original_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '应该是上传的商品的原始图片' ,
`is_real`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否是实物，1，是；0，否；比如虚拟卡就为0，不是实物' ,
`extension_code`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的扩展属性，比如像虚拟卡' ,
`is_on_sale`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '该商品是否开放销售，1，是；0，否' ,
`is_alone_sale`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否能单独销售，1，是；0，否；如果不能单独销售，则只能作为某商品的配件或者赠品销售' ,
`is_shipping`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`can_shipping`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`not_coupon`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`integral`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '购买该商品可以使用的积分数量，估计应该是用积分代替金额消费；但程序好像还没有实现该功能' ,
`add_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品的添加时间' ,
`last_update`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`sort_order`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 COMMENT '应该是商品的显示顺序，不过该版程序中没实现该功能' ,
`best_sort`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 ,
`hot_sort`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 ,
`is_delete`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品是否已经删除，0，否；1，已删除' ,
`is_best`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是精品；0，否；1，是' ,
`is_new`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否是新品；0，否；1，是' ,
`is_hot`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否热销，0，否；1，是' ,
`is_promote`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否特价促销；0，否；1，是' ,
`is_tour`  tinyint(1) NOT NULL COMMENT '是否是亲子游 [0否 1是]' ,
`is_learning`  tinyint(1) UNSIGNED NOT NULL ,
`bonus_type_id`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '购买该商品所能领到的红包类型' ,
`goods_unit`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`goods_type`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品所属类型id，取值表goods_type的cat_id' ,
`seller_note`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品的商家备注，仅商家可见' ,
`give_integral`  int(11) NOT NULL DEFAULT '-1' COMMENT '购买该商品时每笔成功交易赠送的积分数量。' ,
`rank_integral`  int(11) NOT NULL DEFAULT '-1' ,
`supplier_id`  smallint(5) UNSIGNED NOT NULL ,
`is_check`  tinyint(1) UNSIGNED NOT NULL ,
`is_discount`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否允许打折[0-否 1-是]' ,
`is_summarizing`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否分开汇总 [0-否 1-是]' ,
`ready_sell_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预售时间' ,
`sold_out_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下架时间' ,
`shipping_sort`  smallint(5) UNSIGNED NOT NULL DEFAULT 1000 ,
`is_double_half`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否第二商品半价[0.否 1.是]' ,
PRIMARY KEY (`goods_id`),
INDEX `goods_sn` (`goods_sn`) USING BTREE ,
INDEX `cat_id` (`cat_id`) USING BTREE ,
INDEX `last_update` (`last_update`) USING BTREE ,
INDEX `brand_id` (`brand_id`) USING BTREE ,
INDEX `goods_weight` (`goods_weight`) USING BTREE ,
INDEX `promote_end_date` (`promote_end_date`) USING BTREE ,
INDEX `promote_start_date` (`promote_start_date`) USING BTREE ,
INDEX `goods_number` (`goods_number`) USING BTREE ,
INDEX `sort_order` (`sort_order`) USING BTREE 
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;
ALTER TABLE `instore` ADD COLUMN `confirm_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '确认状态，1：确认 0:未确认' AFTER `remark`;
ALTER TABLE `instore` DROP COLUMN `user_id`;
ALTER TABLE `instore_detail` ADD COLUMN `unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' AFTER `transaction_id`;
CREATE TABLE `product` (
`product_id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '产品自增id' ,
`product_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品名称' ,
`product_sn`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品唯一的货号' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存数量' ,
`weight`  decimal(11,3) NOT NULL DEFAULT 0.000 COMMENT '重量' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 删除标记' ,
`supplier_id`  int(11) NOT NULL DEFAULT 0 COMMENT '供应商id' ,
`warn_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存预警数量' ,
`min_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存最小数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '数量单位' ,
PRIMARY KEY (`product_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `supplier` MODIFY COLUMN `supplier_name`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' AFTER `supplier_id`;
ALTER TABLE `supplier` MODIFY COLUMN `country`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `is_check`;
ALTER TABLE `supplier` MODIFY COLUMN `province`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `country`;
ALTER TABLE `supplier` MODIFY COLUMN `city`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `province`;
ALTER TABLE `supplier` MODIFY COLUMN `district`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `city`;
ALTER TABLE `supplier` ADD COLUMN `handle_man`  int(11) NOT NULL DEFAULT 0 COMMENT '负责人' AFTER `delete_flag`;
ALTER TABLE `supplier` ADD COLUMN `create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `handle_man`;
ALTER TABLE `supplier` ADD COLUMN `modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '更改时间' AFTER `create_time`;
ALTER TABLE `unit` MODIFY COLUMN `create_time`  int(11) NOT NULL DEFAULT 0 AFTER `description`;
ALTER TABLE `unit` ENGINE=InnoDB;
DROP TABLE `user`;
DROP TABLE `users`;
SET FOREIGN_KEY_CHECKS=1;