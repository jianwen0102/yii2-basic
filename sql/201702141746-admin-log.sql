CREATE TABLE `admin_log` (
`id`  int(10) NOT NULL AUTO_INCREMENT ,
`controller`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '控制器名称' ,
`action`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作动作' ,
`route`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '操作描述' ,
`create_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '记录时间' ,
`admin_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作用户名' ,
`admin_id`  int(10) NOT NULL DEFAULT 0 COMMENT '操作用户id' ,
`admin_ip`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '操作用户IP' ,
`sql`  longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '操作语句' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;