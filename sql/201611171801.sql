
SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `admin` MODIFY COLUMN `username`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名' AFTER `id`;
ALTER TABLE `admin` MODIFY COLUMN `auth_key`  varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '自动登录key' AFTER `username`;
ALTER TABLE `admin` MODIFY COLUMN `password_hash`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '加密密码' AFTER `auth_key`;
ALTER TABLE `admin` MODIFY COLUMN `password_reset_token`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '重置密码token' AFTER `password_hash`;
ALTER TABLE `admin` MODIFY COLUMN `email_validate_token`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱验证token' AFTER `password_reset_token`;
ALTER TABLE `admin` MODIFY COLUMN `email`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '邮箱' AFTER `email_validate_token`;
ALTER TABLE `admin` MODIFY COLUMN `avatar`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像' AFTER `status`;
ALTER TABLE `admin` MODIFY COLUMN `created_at`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `vip_lv`;
ALTER TABLE `admin` MODIFY COLUMN `updated_at`  int(11) NOT NULL DEFAULT 0 AFTER `created_at`;
ALTER TABLE `admin` ADD COLUMN `delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记 0未删除 1：已删除' AFTER `updated_at`;
ALTER TABLE `admin` ADD COLUMN `last_in`  int(11) NOT NULL DEFAULT 0 COMMENT '上次登录时间' AFTER `delete_flag`;
CREATE TABLE `admin_user` (
`admin_id`  int(11) NOT NULL AUTO_INCREMENT ,
`admin_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '管理员名称' ,
`email`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`password`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`last_login`  int(11) NOT NULL DEFAULT 0 COMMENT '上次登录时间' ,
`last_ip`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上次登录ip' ,
PRIMARY KEY (`admin_id`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;

CREATE TABLE `product` (
`product_id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '产品自增id' ,
`product_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品名称' ,
`product_sn`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品唯一的货号' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存数量' ,
`weight`  decimal(11,3) NOT NULL DEFAULT 0.000 COMMENT '重量' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 删除标记' ,
`supplier_id`  int(11) NOT NULL DEFAULT 0 COMMENT '供应商id' ,
`warn_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存预警数量' ,
`min_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存最小数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '数量单位' ,
PRIMARY KEY (`product_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `unit` ENGINE=InnoDB;
DROP TABLE `user`;
DROP TABLE `users`;
SET FOREIGN_KEY_CHECKS=1;