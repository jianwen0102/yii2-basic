SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `vendor_payment_summary` (
`vendor_payment_summary_id`  int(11) NOT NULL AUTO_INCREMENT ,
`vendor_payment_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商货款结算单头id' ,
`summary_time`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '进货时间区间' ,
`product_id`  int(11) NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未删除 1：已删除' ,
PRIMARY KEY (`vendor_payment_summary_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Compact
;
SET FOREIGN_KEY_CHECKS=1;