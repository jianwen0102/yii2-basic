SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `product` ADD COLUMN `supply_price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '供货价' AFTER `attr_id`;
ALTER TABLE `product` ADD COLUMN `guide_price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '指导价' AFTER `supply_price`;

ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_id`  int(11) UNSIGNED NOT NULL COMMENT '退货单id' FIRST ;
ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '退货单号' AFTER `salereturn_id`;
ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '退货时间' AFTER `salereturn_no`;
ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售退货类型' AFTER `salereturn_date`;
ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '退货金额' AFTER `confirm_flag`;
ALTER TABLE `salereturn` MODIFY COLUMN `salereturn_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '退货单id' FIRST ;
ALTER TABLE `salereturn_detail` MODIFY COLUMN `salereturn_det_id`  int(11) UNSIGNED NOT NULL COMMENT '销售退货单明细id' FIRST ;
ALTER TABLE `salereturn_detail` MODIFY COLUMN `salereturn_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售退货单头id' AFTER `salereturn_det_id`;
ALTER TABLE `salereturn_detail` MODIFY COLUMN `salereturn_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '销售退货单明细id' FIRST ;

SET FOREIGN_KEY_CHECKS=1;