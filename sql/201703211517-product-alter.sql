ALTER TABLE `category` ADD COLUMN `shop_cat_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商城类目id' AFTER `remark`;
ALTER TABLE `product` ADD COLUMN `good_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商城商品id' AFTER `unit_content`;
ALTER TABLE `product` ADD COLUMN `attr_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商城商品规格id' AFTER `good_id`;