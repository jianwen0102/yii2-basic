SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `payables` MODIFY COLUMN `pay_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '应付款' AFTER `remark`;
ALTER TABLE `payables` MODIFY COLUMN `settle_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '结算金额' AFTER `pay_amount`;
ALTER TABLE `payables` MODIFY COLUMN `receive_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '已付款' AFTER `settle_amount`;

ALTER TABLE `purchase` ADD COLUMN `finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '退货状态 0：未完成 1：完成' AFTER `purchase_type`;
ALTER TABLE `purchase_detail` ADD COLUMN `return_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '已退数量' AFTER `amount`;
ALTER TABLE `purchase_detail` MODIFY COLUMN `minus_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '剩余数量' AFTER `return_quantity`;
ALTER TABLE `purchase_detail` ADD COLUMN `finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '退货状态 0：未完成 1：完成' AFTER `second_relation`;
ALTER TABLE `purchase_detail` DROP COLUMN `received_quantity`;
CREATE TABLE `purchase_return` (
`purchase_return_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '采购退货id' ,
`purchase_return_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '采购退货编号' ,
`purchase_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一层单据id(采购入库id)' ,
`purchase_return_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商' ,
`warehouse_id`  int(11) NOT NULL DEFAULT 0 COMMENT '仓库' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`purchase_return_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购退货时间' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认状态 0未确认 1：确认' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`purchase_return_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '订单金额' ,
`purchase_return_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单据类型' ,
PRIMARY KEY (`purchase_return_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `purchase_return_detail` (
`purchase_return_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`purchase_return_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购退货id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`weight`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '重量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' ,
`purchase_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购入库序号' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' ,
`base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' ,
`second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' ,
`second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' ,
`second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' ,
PRIMARY KEY (`purchase_return_det_id`),
INDEX `IDx_goods_id` (`product_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `saleout` MODIFY COLUMN `sell_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一层单据id(销售订单id)' AFTER `discount_amount`;
ALTER TABLE `saleout` ADD COLUMN `finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0 : 未完成 1：已完成(退货状态)' AFTER `sell_id`;
ALTER TABLE `saleout_detail` ADD COLUMN `return_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '退货日期' AFTER `sell_det_id`;
ALTER TABLE `saleout_detail` ADD COLUMN `minus_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '剩余数量（未退）' AFTER `return_quantity`;
ALTER TABLE `saleout_detail` ADD COLUMN `finish_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 : 未完成 1：已完成(退货状态)' AFTER `minus_quantity`;
CREATE TABLE `salereturn` (
`salereturn_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '退货单id' ,
`salereturn_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '退货单号' ,
`salereturn_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '退货时间' ,
`salereturn_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售退货类型' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人员' ,
`customer_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '客户id' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0否，1是' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认 ，1：已确认 0：未确认' ,
`salereturn_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '退货金额' ,
`total_discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣金额' ,
`total_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`discount_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '整单优惠金额' ,
`saleout_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一层单据id(销售出库id)' ,
PRIMARY KEY (`salereturn_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `salereturn_detail` (
`salereturn_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '销售退货单明细id' ,
`salereturn_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售退货单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '产品id' ,
`quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售出库数量' ,
`weight`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售出库重量' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0，否，1是' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '折扣金额' ,
`batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' ,
`base_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '基本单位数量' ,
`base_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '基本单位' ,
`second_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '辅助单位数量' ,
`second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' ,
`second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' ,
`saleout_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售出库明细id' ,
PRIMARY KEY (`salereturn_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
SET FOREIGN_KEY_CHECKS=1;