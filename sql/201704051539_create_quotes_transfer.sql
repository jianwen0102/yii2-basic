SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `quotes` (
`quotes_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`quotes_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '报价单单号' ,
`quotes_man`  int(11) NOT NULL DEFAULT 0 COMMENT '经手人' ,
`quotes_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报价时间' ,
`receive_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '到货时间' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`quotes_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '报价总金额' ,
`discount_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '优惠金额' ,
`total_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '合计金额' ,
`total_discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣金额' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '完成标记' ,
PRIMARY KEY (`quotes_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `quotes_detail` (
`quotes_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`quotes_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '报价单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '数量' ,
`weight`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '重量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '备注' ,
`discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '折扣' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '完成标记' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
PRIMARY KEY (`quotes_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;

ALTER TABLE `transaction` ADD COLUMN `customer_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id' AFTER `flag`;
CREATE TABLE `transfer` (
`transfer_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`transfer_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '调拨单编号' ,
`transfer_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`warehouse_out`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调出仓库' ,
`warehouse_in`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调入仓库' ,
`transfer_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '调拨金额' ,
`transfer_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调拨日期' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记 0：未删除 ，1：已删除' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0：未确认 1：已确认' ,
PRIMARY KEY (`transfer_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `transfer_detail` (
`transfer_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`transfer_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '调拨单id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '调拨数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '进货价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`base_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '基本数量' ,
`base_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '基本单位' ,
`second_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '辅助数量' ,
`second_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '辅助单位' ,
`second_relation`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '辅助单位关系' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除 0：未删除 1：已删除' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
PRIMARY KEY (`transfer_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `warehouse` ADD COLUMN `is_scrapped`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1:报废仓 0：正常仓库' AFTER `user_id`;
SET FOREIGN_KEY_CHECKS=1;