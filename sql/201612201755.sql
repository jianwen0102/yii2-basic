SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `admin_user` ENGINE=InnoDB;
CREATE TABLE `category` (
`category_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '类目Id' ,
`category_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类目名称' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`parent_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父id' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '更新时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记//1：已删除；0：未删除' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`category_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `employee` ENGINE=InnoDB;
ALTER TABLE `instore` MODIFY COLUMN `instore_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '入库类型' AFTER `instore_date`;
ALTER TABLE `instore_detail` ADD COLUMN `price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' AFTER `unit`;
ALTER TABLE `instore_detail` ADD COLUMN `amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `instore_detail` ADD COLUMN `batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' AFTER `amount`;
ALTER TABLE `instore_detail` DROP COLUMN `transaction_id`;
ALTER TABLE `outstore` MODIFY COLUMN `outstore_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '出库类型' AFTER `outstore_date`;
ALTER TABLE `outstore_detail` MODIFY COLUMN `unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' AFTER `delete_flag`;
ALTER TABLE `outstore_detail` ADD COLUMN `price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' AFTER `unit`;
ALTER TABLE `outstore_detail` ADD COLUMN `amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '金额' AFTER `price`;
ALTER TABLE `outstore_detail` ADD COLUMN `batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' AFTER `amount`;
ALTER TABLE `outstore_detail` DROP COLUMN `transaction_id`;
ALTER TABLE `procurement` ADD COLUMN `vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商' AFTER `remark`;
ALTER TABLE `procurement` ADD COLUMN `warehouse_id`  int(11) NOT NULL DEFAULT 0 COMMENT '仓库' AFTER `vendor_id`;
ALTER TABLE `procurement` ADD COLUMN `procurement_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购时间' AFTER `warehouse_id`;
ALTER TABLE `procurement` ADD COLUMN `finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购单完成状态' AFTER `procurement_date`;
ALTER TABLE `procurement` ADD COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' AFTER `finish_flag`;
ALTER TABLE `procurement` ADD COLUMN `procurement_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单总金额' AFTER `create_man`;
ALTER TABLE `procurement` DROP COLUMN `supplier_id`;
ALTER TABLE `procurement` ENGINE=InnoDB;
CREATE TABLE `procurement_detail` (
`procurement_det_id`  int(11) NOT NULL AUTO_INCREMENT ,
`procurement_id`  int(11) NOT NULL DEFAULT 0 COMMENT '采购单id' ,
`goods_id`  int(11) NOT NULL DEFAULT 0 COMMENT '产品id' ,
`quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '数量' ,
`weight`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '重量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`remark`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`received_quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购订单已收到的数量' ,
`minus_quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '未到货数量' ,
`finish_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '入库完成标记（0:未完成 1：已完成）' ,
PRIMARY KEY (`procurement_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `product` MODIFY COLUMN `product_sn`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品唯一的货号' AFTER `product_name`;
ALTER TABLE `product` MODIFY COLUMN `supplier_id`  int(11) NOT NULL DEFAULT 0 COMMENT '供应商id' AFTER `weight`;
ALTER TABLE `product` MODIFY COLUMN `warn_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存预警数量' AFTER `supplier_id`;
ALTER TABLE `product` MODIFY COLUMN `min_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存最小数量' AFTER `warn_quantity`;
ALTER TABLE `product` MODIFY COLUMN `quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '数量单位' AFTER `min_quantity`;
ALTER TABLE `product` ADD COLUMN `category_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '类目id' AFTER `quantity_unit`;
ALTER TABLE `product` MODIFY COLUMN `delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 删除标记' AFTER `category_id`;
ALTER TABLE `product` ADD COLUMN `description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述' AFTER `modify_time`;
ALTER TABLE `product` ADD COLUMN `product_img`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品大图' AFTER `description`;
CREATE TABLE `product_image` (
`image_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`product_id`  int(11) NOT NULL DEFAULT 0 ,
`origin_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`product_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`create_time`  int(11) NOT NULL DEFAULT 0 ,
`modify_time`  int(11) NOT NULL DEFAULT 0 ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`image_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `purchase` (
`purchase_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '采购入库id' ,
`purchase_no`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '采购入库编号' ,
`procurement_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一层单据id(采购订单id)' ,
`purchase_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人' ,
`vendor_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '供应商' ,
`warehouse_id`  int(11) NOT NULL DEFAULT 0 COMMENT '仓库' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`purchase_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购入库时间' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认状态 0未确认 1：确认' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`purchase_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单金额' ,
`purchase_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单据类型' ,
PRIMARY KEY (`purchase_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `purchase_detail` (
`purchase_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT ,
`purchase_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购入库id' ,
`goods_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品id' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '数量' ,
`weight`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '重量' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价' ,
`amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`received_quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '已到货数量' ,
`minus_quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '未到货数量' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '删除标记' ,
`batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' ,
`procurement_det_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '采购订单序号' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
PRIMARY KEY (`purchase_det_id`),
INDEX `IDx_goods_id` (`goods_id`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `region` ENGINE=InnoDB;
CREATE TABLE `shop_category` (
`cat_id`  smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT ,
`cat_name`  varchar(90) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`keywords`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`cat_desc`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`parent_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`sort_order`  tinyint(1) UNSIGNED NOT NULL DEFAULT 50 ,
`template_file`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`measure_unit`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`show_in_nav`  tinyint(1) NOT NULL DEFAULT 0 ,
`style`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`is_show`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
`grade`  tinyint(4) NOT NULL DEFAULT 0 ,
`filter_attr`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' ,
`cat_ico`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`cat_id`),
INDEX `parent_id` (`parent_id`) USING BTREE 
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;
CREATE TABLE `shop_goods` (
`goods_id`  mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT ,
`cat_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`sub_cat_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`goods_sn`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_name`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_name_style`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '+' ,
`click_count`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`brand_id`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`provider_name`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_number`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`warn_number`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 ,
`min_number`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 ,
`expire_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '商品过期时间' ,
`sales_count`  int(10) UNSIGNED NOT NULL ,
`goods_weight`  decimal(10,3) UNSIGNED NOT NULL DEFAULT 0.000 ,
`weight_unit`  tinyint(3) UNSIGNED NOT NULL ,
`market_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 ,
`shop_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 ,
`mobile_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '手机端价格' ,
`promote_price`  decimal(10,2) UNSIGNED NOT NULL DEFAULT 0.00 ,
`promote_start_date`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`promote_end_date`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`keywords`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_alias`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '商品名称的简称(用于订单处理和发货)' ,
`goods_brief`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_desc`  text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`goods_thumb`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`goods_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`original_img`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`is_real`  tinyint(3) UNSIGNED NOT NULL DEFAULT 1 ,
`extension_code`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`is_on_sale`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
`is_alone_sale`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 ,
`is_shipping`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`can_shipping`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`not_coupon`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`integral`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`add_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`last_update`  int(10) UNSIGNED NOT NULL DEFAULT 0 ,
`sort_order`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 ,
`best_sort`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 ,
`hot_sort`  smallint(4) UNSIGNED NOT NULL DEFAULT 100 ,
`is_delete`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`is_best`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`is_new`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`is_hot`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`is_promote`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 ,
`is_tour`  tinyint(1) NOT NULL COMMENT '是否是亲子游 [0否 1是]' ,
`is_learning`  tinyint(1) UNSIGNED NOT NULL ,
`bonus_type_id`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`goods_unit`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 ,
`goods_type`  smallint(5) UNSIGNED NOT NULL DEFAULT 0 ,
`seller_note`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
`give_integral`  int(11) NOT NULL DEFAULT '-1' ,
`rank_integral`  int(11) NOT NULL DEFAULT '-1' ,
`supplier_id`  smallint(5) UNSIGNED NOT NULL ,
`is_check`  tinyint(1) UNSIGNED NOT NULL ,
`is_discount`  tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否允许打折[0-否 1-是]' ,
`is_summarizing`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否分开汇总 [0-否 1-是]' ,
`ready_sell_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '预售时间' ,
`sold_out_time`  int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下架时间' ,
`shipping_sort`  smallint(5) UNSIGNED NOT NULL DEFAULT 1000 ,
`is_double_half`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否第二商品半价[0.否 1.是]' ,
PRIMARY KEY (`goods_id`),
INDEX `goods_sn` (`goods_sn`) USING BTREE ,
INDEX `cat_id` (`cat_id`) USING BTREE ,
INDEX `last_update` (`last_update`) USING BTREE ,
INDEX `brand_id` (`brand_id`) USING BTREE ,
INDEX `goods_weight` (`goods_weight`) USING BTREE ,
INDEX `promote_end_date` (`promote_end_date`) USING BTREE ,
INDEX `promote_start_date` (`promote_start_date`) USING BTREE ,
INDEX `goods_number` (`goods_number`) USING BTREE ,
INDEX `sort_order` (`sort_order`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `supplier` ENGINE=InnoDB;
DROP TABLE `goods`;
DROP TABLE `procurement_goods_relation`;
SET FOREIGN_KEY_CHECKS=1;