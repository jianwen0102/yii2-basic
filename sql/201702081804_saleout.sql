SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `generalledger` MODIFY COLUMN `gen_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '现有总库存（数量）' AFTER `warehouse_id`;
ALTER TABLE `generalledger` MODIFY COLUMN `genexpectstartqty`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '期初数量' AFTER `gen_weight`;
ALTER TABLE `instore_detail` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '入库数量' AFTER `goods_id`;
ALTER TABLE `inventory_detail` MODIFY COLUMN `inventory_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '盘点数量' AFTER `product_id`;
ALTER TABLE `inventory_detail` MODIFY COLUMN `lost_quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '盈亏数量' AFTER `quantity_unit`;
ALTER TABLE `inventory_detail` MODIFY COLUMN `quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '库存数量' AFTER `amount`;
ALTER TABLE `inventory_full_detail` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `product_id`;
ALTER TABLE `inventory_lost_detail` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `product_id`;
ALTER TABLE `outstore_detail` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '出库数量' AFTER `goods_id`;
ALTER TABLE `procurement_detail` MODIFY COLUMN `quantity`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `goods_id`;
ALTER TABLE `product` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '库存数量' AFTER `product_sn`;
ALTER TABLE `product_update_log` MODIFY COLUMN `update_num`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '更新数量' AFTER `product_id`;
ALTER TABLE `product_update_log` MODIFY COLUMN `initial_num`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '上期数量' AFTER `num_unit`;
ALTER TABLE `purchase_detail` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `goods_id`;
ALTER TABLE `purchase_detail` MODIFY COLUMN `received_quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '已到货数量' AFTER `amount`;
CREATE TABLE `saleout` (
`saleout_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '出库单id' ,
`saleout_no`  varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '出库单号' ,
`saleout_date`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发货时间' ,
`saleout_type`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售出库类型' ,
`warehouse_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '仓库id' ,
`handle_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经手人员' ,
`customer_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '客户id' ,
`create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '上一次修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0否，1是' ,
`remark`  varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认 ，1：已确认 0：未确认' ,
`saleout_amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '应收金额' ,
`total_discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '整单折扣金额' ,
`total_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '总金额' ,
`discount_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '整单优惠金额' ,
PRIMARY KEY (`saleout_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `saleout_detail` (
`saleout_det_id`  int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '销售出库单明细id' ,
`saleout_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '销售出库单头id' ,
`product_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '产品id' ,
`quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售出库数量' ,
`weight`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售出库重量' ,
`remark`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注' ,
`create_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`delete_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除，0，否，1是' ,
`quantity_unit`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单位' ,
`price`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '销售单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`discount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '折扣金额' ,
`batch_num`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '批号' ,
PRIMARY KEY (`saleout_det_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
ALTER TABLE `stock_pile` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `product_id`;
ALTER TABLE `transaction` MODIFY COLUMN `quantity`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量' AFTER `goods_id`;
DROP TABLE `product_copy`;
SET FOREIGN_KEY_CHECKS=1;