SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE `auth_assignment` (
`item_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`user_id`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`created_at`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`item_name`, `user_id`),
CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `auth_item` (
`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`type`  int(11) NOT NULL ,
`description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`rule_name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
`data`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created_at`  int(11) NULL DEFAULT NULL ,
`updated_at`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`name`),
CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
INDEX `rule_name` (`rule_name`) USING BTREE ,
INDEX `type` (`type`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `auth_item_child` (
`parent`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`child`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`parent`, `child`),
CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
INDEX `child` (`child`) USING BTREE 
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `auth_rule` (
`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`data`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`created_at`  int(11) NULL DEFAULT NULL ,
`updated_at`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`name`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
CREATE TABLE `product_copy` (
`product_id`  int(11) NOT NULL AUTO_INCREMENT COMMENT '产品自增id' ,
`product_name`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品名称' ,
`product_sn`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品唯一的货号' ,
`quantity`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存数量' ,
`weight`  decimal(11,3) NOT NULL DEFAULT 0.000 COMMENT '重量' ,
`supplier_id`  int(11) NOT NULL DEFAULT 0 COMMENT '供应商id' ,
`warn_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存预警数量' ,
`min_quantity`  int(11) NOT NULL DEFAULT 0 COMMENT '库存最小数量' ,
`quantity_unit`  int(11) NOT NULL DEFAULT 0 COMMENT '数量单位' ,
`category_id`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '类目id' ,
`delete_flag`  tinyint(1) NOT NULL DEFAULT 0 COMMENT ' 删除标记' ,
`create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' ,
`modify_time`  int(11) NOT NULL DEFAULT 0 COMMENT '修改时间' ,
`description`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '描述' ,
`product_img`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产品大图' ,
`genexpectstart_qty`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '期初库存' ,
`price`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '成本单价' ,
`amount`  decimal(11,2) NOT NULL DEFAULT 0.00 COMMENT '金额' ,
`genexpectstart_amount`  decimal(11,2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '期初金额' ,
PRIMARY KEY (`product_id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
ROW_FORMAT=Dynamic
;
SET FOREIGN_KEY_CHECKS=1;