SET FOREIGN_KEY_CHECKS=0;
ALTER TABLE `inventory_full` ADD COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' AFTER `inventory_id`;
ALTER TABLE `inventory_full` ADD COLUMN `confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认 0：未确认 1：已确认' AFTER `create_man`;
ALTER TABLE `inventory_lost` ADD COLUMN `confirm_flag`  tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '确认 0：未确认 1：已确认' AFTER `inventory_id`;
ALTER TABLE `inventory_lost` ADD COLUMN `create_man`  int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '制单人' AFTER `confirm_flag`;
ALTER TABLE `inventory_lost_detail` ADD COLUMN `create_time`  int(11) NOT NULL DEFAULT 0 COMMENT '创建时间' AFTER `remark`;
ALTER TABLE `inventory_lost_detail` DROP COLUMN `craete_time`;
SET FOREIGN_KEY_CHECKS=1;